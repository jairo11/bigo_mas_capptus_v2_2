"use strict";

var freeGlobal = require("./_freeGlobal");

/** Detect free variable `self`. */
var freeSelf = typeof self == "object" && self && self.Object === Object && self; // eslint-disable-line no-undef

/** Used as a reference to the global object. */
var root = freeGlobal || freeSelf || Function("return this")();

module.exports = root;
