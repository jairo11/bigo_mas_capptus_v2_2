"use strict";

var getNative = require("./_getNative");

var defineProperty = (function () {
    try {
        var func = getNative(Object, "defineProperty");
        func({}, "", {});
        return func;
    } catch (e) {
        /* eslint-disable no-empty */
    }
}());

module.exports = defineProperty;
