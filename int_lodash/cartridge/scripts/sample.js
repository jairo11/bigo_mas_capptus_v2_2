"use strict";

var arraySample = require("./base/_arraySample"),
    baseSample = require("./base/_baseSample"),
    isArray = require("./base/isArray");

/**
 * Gets a random element from `collection`.
 *
 * @static
 * @memberOf _
 * @since 2.0.0
 * @category Collection
 * @param {Array|Object} collection The collection to sample.
 * @returns {*} Returns the random element.
 * @example
 *
 * _.sample([1, 2, 3, 4]);
 * // => 2
 */
function sample(collection) {
    var func = isArray(collection) ? arraySample : baseSample;
    return func(collection);
}

module.exports = sample;
