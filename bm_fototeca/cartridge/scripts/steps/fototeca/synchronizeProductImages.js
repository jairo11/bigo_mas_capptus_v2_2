"use strict";

var Logger          = require("dw/system/Logger");
var File            = require("dw/io/File");
var ProductMgr      = require("dw/catalog/ProductMgr");
var CatalogMgr      = require("dw/catalog/CatalogMgr");

var products;
var SFTPUtils = require("~/cartridge/scripts/util/SFTPUtilsFototeca");
var sftpClient;

var imagePathSFCC = "images/";
var imagePathSFTP;
var catalogName ;
const IMAGEPATH = "default";

var masterArray= [];

module.exports.beforeStep = function (params) {
    catalogName = params.catalogName;
    imagePathSFTP = params.sftpImagePath;
    var catalog = CatalogMgr.getCatalog(catalogName);
    if (catalog) {
        sftpClient = SFTPUtils.SFTPConnect();
        products = ProductMgr.queryProductsInCatalog(catalog);
        if (!products|| !products.getCount() > 0) {
            Logger.info("El catalogo {0} no contiene productos", catalogName);
        }
    } else {
        Logger.info("No se encontro el catalogo {0}", catalogName);
    }
};

module.exports.getTotalCount = function () {
    return products.getCount();
};

module.exports.read = function () {
    if (products.hasNext()) {
        return products.next();
    }
};

module.exports.process = function (product) {
    if (product.isVariant()) {
        var pvm = product.variationModel;
        if (pvm) {
            var masterProduct = pvm.getMaster();
            if (!(masterArray.indexOf(masterProduct.ID) >= 0)) {
                getAllImages(masterProduct);
                masterArray.push(masterProduct.ID);
            }
        }
    }
};

module.exports.write = function () {
};

module.exports.afterStep = function () {
    products.close();
};

function getRelativeImageUrl(url) {
    return url.substring(url.indexOf(imagePathSFCC) + imagePathSFCC.length);
}

function getImagenFromSFTP(relativeUrl, productID) {
    var fileFromSFTP;
    var sftpFileUrl = imagePathSFTP + relativeUrl;
    if (sftpClient.getConnected()) {
        fileFromSFTP= sftpClient.getFileInfo(sftpFileUrl);
        if (fileFromSFTP) {
            Logger.info("imagen encontrada en SFTP " + sftpFileUrl);
            var sfccFileUrl = File.CATALOGS + File.SEPARATOR + catalogName + File.SEPARATOR + IMAGEPATH + File.SEPARATOR + imagePathSFCC + relativeUrl;
            var fileFromSFCC = new File(sfccFileUrl);
            if (fileFromSFCC.exists()) {
                var sftpFileDate = fileFromSFTP.modificationTime;
                var sfccFileDate = new Date(fileFromSFCC.lastModified());
                if (sftpFileDate > sfccFileDate) {
                    if (sftpClient.getBinary((imagePathSFTP + relativeUrl), fileFromSFCC)) {
                        Logger.info("Se actualiza imagen : " + fileFromSFCC.getName() + " del producto : " + productID);
                    }
                }
                Logger.info("Imagen encontrada en SFCC " + sfccFileUrl);
            } else {
                Logger.info("Imagen NO encontrada en SFCC " + sfccFileUrl);
                var sfccDirPathUrl = sfccFileUrl.replace(fileFromSFCC.getName(), "");
                var sfccDirPath = new File(sfccDirPathUrl);
                if (sfccDirPath.exists()) {
                    Logger.info("Existe directorio en SFCC " + sfccDirPathUrl);
                    if (sftpClient.getBinary((imagePathSFTP + relativeUrl), fileFromSFCC)) {
                        Logger.info("Imagen creada con exito en SFCC " + sfccFileUrl);
                    }
                } else {
                    Logger.info("No existe directorio en SFCC " + sfccDirPathUrl);
                    if (sfccDirPath.mkdir()) {
                        Logger.info("Ruta creada con exito en SFCC " + sfccDirPathUrl);
                        if (sftpClient.getBinary((imagePathSFTP + relativeUrl), fileFromSFCC)) {
                            Logger.info("Imagen creada con exito en SFCC " + sfccFileUrl);
                        }
                    }
                }
            }
        } else {
            Logger.info("imagen {0} de producto {1} NO encontrada en SFTP", sftpFileUrl, productID);
        }

    } else {
        Logger.warn("Sin conexión al servidor ");
    }
}

function getAllImages(product) {
    var imageTypes = ["large", "medium", "small", "swatch"];
    for (var index = 0; index < imageTypes.length; index++) {
        Logger.debug("ProductID -> " + product.ID + " - " + "imageSize -> " + imageTypes[index]);
        var productImageList = product.getImages(imageTypes[index]);
        if (productImageList && productImageList.length > 0) {
            var productImageListItr = productImageList.iterator();
            while (productImageListItr.hasNext()) {
                var productImage = productImageListItr.next();
                var relativeUrl = getRelativeImageUrl(productImage.url.toString());
                Logger.debug("url " + productImage.url.toString());
                getImagenFromSFTP(relativeUrl, product.ID);
            }
        }
    }
}
