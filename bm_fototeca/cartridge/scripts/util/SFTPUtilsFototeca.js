"use strict";

var SFTPClient = require("dw/net/SFTPClient");
var siteHelper = require("helpers");
var Site = require("dw/system/Site");

/**
 * @function SFTPConnect
 * @description connects to (s)ftp server
 */
exports.SFTPConnect = function () {
    var url= Site.getCurrent().getCustomPreferenceValue("sftpFototecaURL");
    var userName= Site.getCurrent().getCustomPreferenceValue("sftpFototecaUsername");
    var password= Site.getCurrent().getCustomPreferenceValue("sftpFototecaPassword");
    var sftp = new SFTPClient();
    sftp.setTimeout(siteHelper.sitePreference("sftpTimeout"));
    sftp.connect(url, userName, password);
    return sftp;
};
