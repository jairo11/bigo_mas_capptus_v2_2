"use strict";

var LocalServiceRegistry = require("dw/svc/LocalServiceRegistry");

// Global service componentnts
var svc;
var config;
var url;
var user;
var password;
var credential;

/**
 * Initialize the service components
 */
function initservice() {
    // Create a service object
    svc = LocalServiceRegistry.createService("cya.services.oms", {
        createRequest: function (_svc, args) {
            if (args) {
                return JSON.stringify(args);
            }
            return null;
        },
        parseResponse: function (_svc, client) {
            return client.text;
        }, 
        filterLogMessage: function (msg) {
            return msg;  
        }
    });
    // Configure the service related parameters
    config = svc.getConfiguration();
    credential = config.getCredential();
    user = credential.getUser();
    password = credential.getPassword();
    url = !empty(credential.getURL()) ? credential.getURL() : "";
    svc.addHeader(
        "Content-Type",
        "application/json"
    );
}

function returnOrder(requestPayload) {
    initservice();
    var req = {};
    req= requestPayload;
    req.name= user;
    req.id = password;
    url += "api/Return/ReturnOrder";
    svc.setRequestMethod("POST");
    url = encodeURI(url);
    svc.setURL(url);
    // service call
    var httpResult = svc.call(req);
    return responseFilter(httpResult);
}

function getReasonsReturn() {
    initservice();
    url += "api/Return/GetReasonsReturn";
    var req = {};
    req.name= user;
    req.id = password;
    svc.setRequestMethod("POST");
    url = encodeURI(url);
    svc.setURL(url);
    var httpResult = svc.call(req);
    return responseFilter(httpResult);
}

function getReturnStatus(){
    initservice();
    url += "api/Return/GetStatusReturn";
    var req = {};
    req.name= user;
    req.id = password;
    svc.setRequestMethod("POST");
    url = encodeURI(url);
    svc.setURL(url);
    var httpResult = svc.call(req);
    return responseFilter(httpResult);
}

function getTrackingOrder(rma){
    initservice();
    url += "api/Return/TrackingReturn";
    var req = {};
    req.name= user;
    req.id = password;
    req.rma = rma;
    svc.setRequestMethod("POST");
    url = encodeURI(url);
    svc.setURL(url);
    var httpResult = svc.call(req);
    return responseFilter(httpResult);
}


function setStatusRefund(request){
    initservice();
    url += "api/Return/UpdateStatus";
    var req = {};
    req.name= user;
    req.id = password;
    req.returnOrders= [request];
    svc.setRequestMethod("POST");
    url = encodeURI(url);
    svc.setURL(url);
    var httpResult = svc.call(req);
    return responseFilter(httpResult);
}


function responseFilter(httpResponse) {
    if (httpResponse.status !== "OK") {
        var errorResult = {
            statusCode: httpResponse.status,
            errorMessage: parseMessage(httpResponse),
            url: url
        };
        return errorResult;
    }
    // return a plain javascript object
    return JSON.parse(httpResponse.object);
}

function parseMessage(httpResponse){
    var msg = "";
    try {
        msg = JSON.parse(httpResponse.getErrorMessage())
    } catch (error) {
        msg = httpResponse.msg;
    }
    return msg;
}

module.exports = {
    returnOrder: returnOrder,
    getReasonsReturn: getReasonsReturn,
    getReturnStatus:getReturnStatus,
    getTrackingOrder: getTrackingOrder,
    setStatusRefund: setStatusRefund
};

