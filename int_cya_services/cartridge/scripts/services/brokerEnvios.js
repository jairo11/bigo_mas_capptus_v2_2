"use strict";

var LocalServiceRegistry = require("dw/svc/LocalServiceRegistry");

// Global service componentnts
var svc;
var config;
var url;
var user;
var password;
var credential;

/**
 * Initialize the service components
 */
function initservice() {
    // Create a service object
    svc = LocalServiceRegistry.createService("cya.services.brokerEnvios", {
        createRequest: function (_svc, args) {
            if (args) {
                return JSON.stringify(args);
            }
            return null;
        },
        parseResponse: function (_svc, client) {
            return client.text;
        }, 
        filterLogMessage: function (msg) {
            return msg;  
        }
    });
    // Configure the service related parameters
    config = svc.getConfiguration();
    credential = config.getCredential();
    user = credential.getUser();
    password = credential.getPassword();
    url = !empty(credential.getURL()) ? credential.getURL() : "";
    svc.addHeader(
        "Content-Type",
        "application/json"
    );
}

function getStatusOrder(orderId) {
    initservice();
    url += "api/Order/GetStatusOrder";
    svc.setRequestMethod("POST");
    url = encodeURI(url);
    svc.setURL(url);
    // service call
    var httpResult = svc.call(orderId);
    return responseFilter(httpResult);
}

function getPricesShipping(requestPayload) {
    initservice();
    url += "api/DeliveryService/GetPricesShipping";
    var req = {};
    req =requestPayload;
    req.name= user;
    req.id = password;
    svc.setRequestMethod("POST");
    url = encodeURI(url);
    svc.setURL(url);
    var httpResult = svc.call(req);
    return responseFilter(httpResult);
}

function getValidatePostalCode(requestPayload) {
    initservice();
    url += "api/DeliveryService/ValidatePostalCode";
    var req = {};
    req.postalCodes=[requestPayload];
    req.name= user;
    req.id = password;
    svc.setRequestMethod("POST");
    url = encodeURI(url);
    svc.setURL(url);
    var httpResult = svc.call(req);
    return responseFilter(httpResult);
}

function responseFilter(httpResponse) {
    if (httpResponse.status !== "OK") {
        var errorResult = {
            statusCode: httpResponse.status,
            errorMessage: parseMessage(httpResponse),
            url: url
        };
        return errorResult;
    }
    // return a plain javascript object
    return JSON.parse(httpResponse.object);
}

function parseMessage(httpResponse){
    var msg = "";
    try {
        msg = JSON.parse(httpResponse.getErrorMessage())
    } catch (error) {
        msg = httpResponse.msg;
    }
    return msg;
}

module.exports = {
    getStatusOrder: getStatusOrder,
    getPricesShipping: getPricesShipping,
    getValidatePostalCode: getValidatePostalCode
};

