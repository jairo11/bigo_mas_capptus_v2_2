'use strict';

var cyaServices = require('int_cya_services/cartridge/scripts/services/brokerEnvios');
var helper = require('*/cartridge/scripts/helpers/responseHelper');

function getShippingCost(requestPayload) {
    var res = cyaServices.getPricesShipping(requestPayload);
    if (res.ResponseCode !== 200) {
        return { priceId: 0, deliveryType: null, shippingPrice: 55 };
    }
    return helper.shippingCostResponse(res);
}

function getValidatePostalCode(requestPayload) {
    var res = cyaServices.getValidatePostalCode(requestPayload);
    if (res.ResponseCode !== 200) {
        return { error: true };
    }
    return res;
}

module.exports = {
    getShippingCost: getShippingCost,
    getValidatePostalCode: getValidatePostalCode
};

