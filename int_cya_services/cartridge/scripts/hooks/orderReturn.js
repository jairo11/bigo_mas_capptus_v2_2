'use strict';

var cyaServices = require('int_cya_services/cartridge/scripts/services/oms');
var helper = require('*/cartridge/scripts/helpers/responseHelper');
var Logger = require('dw/system/Logger');

function getReturnReasonCodes() {
    var response = cyaServices.getReasonsReturn();
    if (response.ResponseCode !== 200) {
        return '';
    }
    return response.Reasons;
}

function returnOrder(requestPayload) {
    var response = cyaServices.returnOrder(requestPayload);
    if (response.ResponseCode !== 200) {
        return '';
    }
    return helper.returnResponse(response);
}

function getReturnStatus() {
    var response = cyaServices.getReturnStatus();
    if (response.ResponseCode !== 200) {
        return '';
    }
    return response.Status;
}

function setStatusOMS(request) {
    var response = cyaServices.setStatusRefund(request);
    if (response.ResponseCode !== 200) {
        Logger.error('error: ' + response.errorMessage);
        return { error: true };
    }
    return response;
}

module.exports = {
    getReturnReasonCodes: getReturnReasonCodes,
    returnOrder: returnOrder,
    getReturnStatus: getReturnStatus,
    setStatusOMS: setStatusOMS
};
