"use strict";

function buildItems(returnItems) {
    var items = [];
    for (var i = 0; i < returnItems.length; i++) {
        var item = JSON.parse(returnItems[i].orderLineItem);
        items.push({"itemId": item.productID, "quantity" : item.selectedquantity,
            "reasonCode": item.reasonReturn});
    }
    return items;
}

function getReturnsRequestPayload(orderNumber, returnItems, idShippingPrice, shippingPrice) {
    var payload={
        priceEstimateId: idShippingPrice,
        orderNo: orderNumber,
        applicationDate: new Date(),
        expectedReceipt: new Date(),
        items : buildItems(returnItems),
        returnCost : shippingPrice
    };

    return payload;
}

function buildCustomerPayload(address, customer) {
    var customerPayload ={};
    customerPayload.street = address.address1 ;
    customerPayload.city = address.city;
    customerPayload.postalCode = address.postalCode;
    customerPayload.suburb = address.custom.suburb;
    customerPayload.contact = address.firstName
    customerPayload.email = customer.profile? customer.profile.email : "";
    customerPayload.state = address.stateCode;
    customerPayload.company = "";
    customerPayload.numExt = address.custom.numberExt;
    customerPayload.numInt = "";
    customerPayload.country = address.countryCode.value;
    customerPayload.locationReference = address.address2;
    return customerPayload;
}

function buildProductsPayload(productLineItems) {
    var products = [];
    for (var i = 0; i < productLineItems.length; i++) {
        products.push({"productId": productLineItems[i].productID, "quantity" : productLineItems[i].quantityValue});
    }
    return products;
}

function buildProductsReturnPayload(returnItems) {
    var products = [];
    for (var i = 0; i < returnItems.length; i++) {
        var item = JSON.parse(returnItems[i].orderLineItem);
        products.push({"productId": item.productID, "quantity" : item.selectedquantity});
    }
    return products;
}

function getShippingRequestPayload(shipment, address, customer, processType) {
    var payload = {
        shipmentID: shipment.UUID,
        process: processType,
        customer: buildCustomerPayload(address, customer),
        products: buildProductsPayload(shipment.productLineItems)
    };

    return payload;
}

function getShippingReturnRequestPayload(returnItems, address, customer, processType) {
    var payload = {
        process: processType,
        customer: buildCustomerPayload(address, customer),
        products: buildProductsReturnPayload(returnItems)
    };

    return payload;
}

module.exports = {
    getReturnsRequestPayload: getReturnsRequestPayload,
    getShippingRequestPayload: getShippingRequestPayload,
    getShippingReturnRequestPayload: getShippingReturnRequestPayload
};
