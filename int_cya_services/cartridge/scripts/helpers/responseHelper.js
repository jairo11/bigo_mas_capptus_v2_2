"use strict";

function shippingCostResponse(req) {
    var res ={
        priceId : req.idPrice,
        deliveryType : req.DeliveryType,
        shippingPrice : req.PriceList[0].price
    };
    return res;
}

function returnResponse(req) {
    var res ={
        returnId : req.returnId,
        company : req.company,
        guideNo : req.guideNo,
        pdf : req.pdf
    };
    // var res ={
    //     returnId : (Math.random() * (200 - 100) + 100),
    //     company : 'Fedex',
    //     guideNo : '95385126',
    //     pdf : "XS9Sb290IDEyIDAgUi9TaXplIDE0Pj4Kc3RhcnR4cmVmCjMxMTMzCiUlRU9GCg=="
    // };
    return res;
}

module.exports = {
    shippingCostResponse : shippingCostResponse,
    returnResponse: returnResponse
};
