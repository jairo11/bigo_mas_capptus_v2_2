/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./cartridges/int_paypal/cartridge/client/default/js/pdp/pdpHelper.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./cartridges/int_paypal/cartridge/client/default/js/pdp/pdpHelper.js":
/*!****************************************************************************!*\
  !*** ./cartridges/int_paypal/cartridge/client/default/js/pdp/pdpHelper.js ***!
  \****************************************************************************/
/*! exports provided: addProductForPDPbtnFlow, appendToUrl */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addProductForPDPbtnFlow", function() { return addProductForPDPbtnFlow; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "appendToUrl", function() { return appendToUrl; });
/**
 * Shows paypal PDP button/static image
 * @param {Object} $paypalPDPButton - paypal button element
 */
function showPDPButton($paypalPDPButton) {
    $paypalPDPButton.style.display = '';
}

/**
 * Hides paypal PDP button/static image
 * @param {Object} $paypalPDPButton - paypal button element
 */
function hidePDPButton($paypalPDPButton) {
    $paypalPDPButton.style.display = 'none';
}

/**
 * Adds product to basket on paypal button/static image click when PDP flow
 * @returns {Object} response with required data
 */
function addProductForPDPbtnFlow() {
    var $bundleItem = $('.bundle-item');
    /**
     * Gets options
     * @param {Object} $productContainer - product container
     * @returns {string} options
     */
    function getOptions($productContainer) {
        var options = $productContainer
            .find('.product-option')
            .map(function () {
                var $elOption = $(this).find('.options-select');
                var urlValue = $elOption.val();
                var selectedValueId = $elOption.find('option[value="' + urlValue + '"]')
                    .data('value-id');
                return {
                    optionId: $(this).data('option-id'),
                    selectedValueId: selectedValueId
                };
            }).toArray();

        return JSON.stringify(options);
    }

    var pid = $('.product-detail:not(".bundle-item")').data('pid');
    var $btn = $('.paypal_pdp_button');
    var $productContainer = $btn.closest('.product-detail');

    var form = {
        pid: pid,
        quantity: $('.quantity-select').val()
    };

    if (!$bundleItem.length) {
        form.options = getOptions($productContainer);
    } else {
        var items = $bundleItem.map(function () {
            return {
                pid: $(this).find('.product-id').text(),
                quantity: parseInt($(this).find('label.quantity').data('quantity'), 10)
            };
        });
        form.childProducts = JSON.stringify(items.toArray());
    }
    var response = $.ajax({
        url: $('.add-to-cart-url').val(),
        method: 'POST',
        async: false,
        data: form
    }).responseJSON;
    response.pid = pid;
    return response;
}

/**
 * Appends params to a url
 * @param {string} url - Original url
 * @param {Object} param - Parameters to append
 * @returns {string} result url with appended parameters
 */
function appendToUrl(url, param) {
    var newUrl = url;
    newUrl += (newUrl.indexOf('?') !== -1 ? '&' : '?') + Object.keys(param).map(function (key) {
        return key + '=' + encodeURIComponent(param[key]);
    }).join('&');

    return newUrl;
}

// Handling PDP button/static image behavior
var $paypalPDPButton = document.querySelector('.paypal_pdp_button');
if ($paypalPDPButton) {
    var $price = document.querySelector('.price .sales .value');
    var isZeroAmount = false;
    var $miniCartQuantity = document.querySelector('.minicart-quantity');
    var $addToCartButton = document.querySelector('.add-to-cart') || document.querySelector('.add-to-cart-global');

    // Check minicart quantity and hide PDPbutton if it is not empty
    if (($miniCartQuantity && parseInt($miniCartQuantity.textContent, 0) > 0)
        || ($price && $price.getAttribute('content') === '0.00')) {  // Check if product price is zero
        hidePDPButton($paypalPDPButton);
    }

    if ($addToCartButton.disabled) {
        hidePDPButton($paypalPDPButton);
    }

    $('body').on('product:afterAddToCart', function () {
        hidePDPButton($paypalPDPButton);
    });

    $('body').on('cart:update', function () {
        $miniCartQuantity = parseInt(document.querySelector('.minicart-quantity').textContent, 0);
        if ($addToCartButton.disabled) {
            hidePDPButton($paypalPDPButton);
        }
        if ($miniCartQuantity === 0 && !$addToCartButton.disabled) {
            showPDPButton($paypalPDPButton);
        }
    });

    $('body').on('product:statusUpdate', function () {
        $miniCartQuantity = parseInt(document.querySelector('.minicart-quantity').textContent, 0);
        $price = document.querySelector('.price .sales .value');
        isZeroAmount = false;
        if ($price) {
            isZeroAmount = $price.getAttribute('content') === '0.00';
        }

        if ($miniCartQuantity === 0) {
            if ($addToCartButton.disabled || isZeroAmount) {
                hidePDPButton($paypalPDPButton);
            }
            if (!$addToCartButton.disabled && !isZeroAmount) {
                showPDPButton($paypalPDPButton);
            }
        }
    });
}




/***/ })

/******/ });
//# sourceMappingURL=pdpHelper.js.map