/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./cartridges/int_paypal/cartridge/client/default/js/pdp/pdp.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./cartridges/int_paypal/cartridge/client/default/js/api.js":
/*!******************************************************************!*\
  !*** ./cartridges/int_paypal/cartridge/client/default/js/api.js ***!
  \******************************************************************/
/*! exports provided: updateOrderData, getPurchaseUnits, getBillingAgreementToken, createBillingAgreementCall, getOrderDetailsCall, returnFromCart, showCartErrorHtml, showCheckoutErrorHtml, finishLpmOrder, createCartBillingFormData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateOrderData", function() { return updateOrderData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPurchaseUnits", function() { return getPurchaseUnits; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getBillingAgreementToken", function() { return getBillingAgreementToken; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createBillingAgreementCall", function() { return createBillingAgreementCall; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getOrderDetailsCall", function() { return getOrderDetailsCall; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "returnFromCart", function() { return returnFromCart; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showCartErrorHtml", function() { return showCartErrorHtml; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showCheckoutErrorHtml", function() { return showCheckoutErrorHtml; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "finishLpmOrder", function() { return finishLpmOrder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createCartBillingFormData", function() { return createCartBillingFormData; });
var loaderInstance = __webpack_require__(/*! ./loader */ "./cartridges/int_paypal/cartridge/client/default/js/loader.js");
var $loaderContainer = document.querySelector('.paypalLoader');
var loader = loaderInstance($loaderContainer);
/**
 *  Appends error message on cart page
 *
 * @param {string} message error message
 */
function showCartErrorHtml(message) {
    $('.checkout-btn').addClass('disabled');
    $('.cart-error').append(
        `<div class="alert alert-danger alert-dismissible valid-cart-error fade show cartError" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            ${message}
        </div>`
    );
    window.scrollTo(0, 0);
}

/**
 *  Appends error message on billing checkout page
 *
 * @param {string} message error message
 */
function showCheckoutErrorHtml(message) {
    document.querySelector('.error-message-text').textContent = '';
    document.querySelector('.error-message').style.display = 'block';
    document.querySelector('.error-message-text').append(message);
    window.scrollTo(0, 0);
}

/**
 * Updates information about an order
 *
 * @returns {Object} Call handling result
 */
function updateOrderData() {
    loader.show();
    return $.ajax({
        url: window.paypalUrls.updateOrderData + '?isCartFlow=true',
        type: 'PATCH',
        success: () => {
            loader.hide();
            window.location.href = window.paypalUrls.placeOrderStage;
        },
        error: (err) => {
            loader.hide();
            var error = JSON.parse(err.responseText);
            showCartErrorHtml(error.errorMsg);
            if (error.transactionExpired) {
                location.reload();
            }
        }
    });
}

/**
 * Gets purchase units
 *
 * @returns {Object} with purchase units data
 */
function getPurchaseUnits() {
    return $.get(window.paypalUrls.getPurchaseUnit)
        .then(({ purchase_units }) => purchase_units);
}

/**
 * Gets Billing Agreement Token
 *
 * @param {boolean} isCartFlow - billing agreement flow from cart
 * @returns {string} billingToken - returns a JSON response that includes token, an approval URL
 */
function getBillingAgreementToken(isCartFlow = false) {
    return $.get(window.paypalUrls.createBillingAgreementToken + `?isCartFlow=${isCartFlow}`)
        .then((data) => data);
}

/**
 * Gets Billing Agreement
 * After buyer approval, you use a billing agreement token to create the agreement.
 *
 * @param {string} billingToken - billing agreement token
 * @returns {Object} JSON response body that includes the billing agreement ID,
 * the state of the agreement, which is ACTIVE, and information about the payer
 */
function createBillingAgreementCall(billingToken) {
    return $.ajax({
        url: window.paypalUrls.createBillingAgreement,
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({ billingToken })
    });
}

/**
 * Gets Order Details
 *
 * @param {string} orderId - billing agreement token
 * @returns {Object} JSON response body that includes payer email
 */
function getOrderDetailsCall(orderId) {
    return $.get(window.paypalUrls.getOrderDetails + `?orderId=${orderId}`)
        .then((data) => data);
}

/**
 * Create billing formData from fields data
 *
 * @param {Object} fieldsData - fields data values
 * @param {Elemtn} $paypalButton - paypal button selector
 * @returns {Object} cart billing form data
 */
function createCartBillingFormData(fieldsData, $paypalButton) {
    var cartBillingFormData = new FormData();
    if (!$paypalButton) {
        $paypalButton = document.querySelector('#paypal_pdp_image') || document.querySelector('#paypal_image');
    }
    var cartBillingFields = $paypalButton && JSON.parse($paypalButton.getAttribute('data-paypal-billing-form-fields'));

    Object.entries(cartBillingFields).forEach(entry => {
        const [key, field] = entry;
        if (typeof field === 'object') {
            cartBillingFormData.append(field.name, fieldsData && fieldsData[key] ? fieldsData[key] : field.value);
        }
    });

    return cartBillingFormData;
}

/**
 * Calls to returnFromCart endpoint, redirects to place order stage or shows error if it exists
 *
 * @returns {Object} Call handling result
 */
function returnFromCart() {
    loader.show();
    let cartBillingFormData = createCartBillingFormData();

    return $.ajax({
        url: window.paypalUrls.returnFromCart,
        type: 'POST',
        contentType: false,
        data: cartBillingFormData,
        processData: false,
        success: () => {
            loader.hide();
            window.location.href = window.paypalUrls.placeOrderStage;
        },
        error: function (err) {
            loader.hide();
            showCartErrorHtml(err.responseText);
        }
    });
}

/**
 * Call finishLpmOrder endpoint
 * @param  {Object} details billing address details
 * @returns {Promise} ajax call
 */
function finishLpmOrder(details) {
    const lpmName = document.querySelector('#usedPaymentMethod').value;
    const paypalMethodId = document.querySelector('#paypalMethodId').value;
    return $.ajax({
        url: window.paypalUrls.finishLpmOrder,
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            details,
            lpmName,
            paypalMethodId
        })
    });
}




/***/ }),

/***/ "./cartridges/int_paypal/cartridge/client/default/js/helper.js":
/*!*********************************************************************!*\
  !*** ./cartridges/int_paypal/cartridge/client/default/js/helper.js ***!
  \*********************************************************************/
/*! exports provided: getPaypalButtonStyle */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPaypalButtonStyle", function() { return getPaypalButtonStyle; });
const defaultStyle = {
    color: 'gold',
    shape: 'rect',
    layout: 'vertical',
    label: 'paypal',
    tagline: false
};

/**
 *  Gets paypal button styles
 * @param {Element} button - button element
 * @returns {Object} with button styles or if error appears with default styles
 */
function getPaypalButtonStyle(button) {
    try {
        const config = button.getAttribute('data-paypal-button-config');
        if (config) {
            const buttonConfigs = JSON.parse(config);
            return buttonConfigs.style;
        }
    } catch (error) {
        return {
            style: defaultStyle
        };
    }
}




/***/ }),

/***/ "./cartridges/int_paypal/cartridge/client/default/js/loader.js":
/*!*********************************************************************!*\
  !*** ./cartridges/int_paypal/cartridge/client/default/js/loader.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* eslint-disable require-jsdoc */
module.exports = function (containerElement) {
    function Constructor() {
        this.containerEl = containerElement;
    }
    Constructor.prototype.show = function () {
        this.containerEl.style.display = 'block';
    };
    Constructor.prototype.hide = function () {
        this.containerEl.style.display = 'none';
    };
    return new Constructor();
};


/***/ }),

/***/ "./cartridges/int_paypal/cartridge/client/default/js/pdp/initPdpButton.js":
/*!********************************************************************************!*\
  !*** ./cartridges/int_paypal/cartridge/client/default/js/pdp/initPdpButton.js ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../api */ "./cartridges/int_paypal/cartridge/client/default/js/api.js");
/* harmony import */ var _pdpHelper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./pdpHelper */ "./cartridges/int_paypal/cartridge/client/default/js/pdp/pdpHelper.js");
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../helper */ "./cartridges/int_paypal/cartridge/client/default/js/helper.js");




var loaderInstance = __webpack_require__(/*! ../loader */ "./cartridges/int_paypal/cartridge/client/default/js/loader.js");
var $loaderContainer = document.querySelector('.paypalLoader');
var loader = loaderInstance($loaderContainer);
var pid;
var uuid;
var removeFromCartUrl;

/**
 *  Gets purchase units object, creates order and returns object with data
 *
 * @param {Object} _ - arg
 * @param {Object} actions - paypal actions
 * @returns {Object} with purchase units data and application context
 */
function createOrder(_, actions) {
    loader.show();
    var res = Object(_pdpHelper__WEBPACK_IMPORTED_MODULE_1__["addProductForPDPbtnFlow"])();
    if (res.cart) {
        uuid = res.pliUUID;
        removeFromCartUrl = res.cart.actionUrls.removeProductLineItemUrl;
        pid = res.pid;
    } else {
        loader.hide();
        throw new Error(res.message || 'Error occurs');
    }

    return $
        .get(window.paypalUrls.getCartPurchaseUnit)
        .then(function ({ purchase_units }) {
            let parsedPurchaseUnit = purchase_units[0];
            if (JSON.parse(parsedPurchaseUnit.amount.value) === 0) {
                Object(_api__WEBPACK_IMPORTED_MODULE_0__["showCartErrorHtml"])('Order total 0 is not allowed for PayPal');
            }
            const application_context = {
                shipping_preference: parsedPurchaseUnit.shipping_preference
            };
            loader.hide();
            return actions.order.create({
                purchase_units,
                application_context
            });
        });
}

/**
 *  Makes post call and transfers order ID to returnFromCart endpoint, goes to checkout place order stage
 *
 * @param {Object} orderID - order id
 */
function onApprove({ orderID }) {
    let cartBillingFormData = Object(_api__WEBPACK_IMPORTED_MODULE_0__["createCartBillingFormData"])({
        paypalOrderID: orderID
    }, document.querySelector('.js_paypal_button_on_pdp_page'));

    $.ajax({
        type: 'POST',
        url: window.paypalUrls.returnFromCart,
        contentType: false,
        data: cartBillingFormData,
        processData: false,
        success: () => {
            loader.hide();
            window.location.href = window.paypalUrls.placeOrderStage;
        },
        error: function () {
            loader.hide();
            window.location.href = window.paypalUrls.cartPage;
        }
    });
}

/**
 * Hides loader on paypal widget closing without errors
 * If PDP flow, removes product from basket on cancel
 */
function onCancel() {
    var urlParams = {
        pid: pid,
        uuid: uuid
    };

    $.ajax({
        url: Object(_pdpHelper__WEBPACK_IMPORTED_MODULE_1__["appendToUrl"])(removeFromCartUrl, urlParams),
        type: 'get',
        dataType: 'json',
        success: function () {
            $.spinner().stop();
        },
        error: function () {
            $.spinner().stop();
        }
    });
    loader.hide();
}

/**
 * Shows errors if paypal widget was closed with errors
 * If PDP flow, removes product from basket on error
 */
function onError() {
    if (pid) {
        var productID = pid;
        var urlParams = {
            pid: productID,
            uuid: uuid
        };

        $.ajax({
            url: Object(_pdpHelper__WEBPACK_IMPORTED_MODULE_1__["appendToUrl"])(removeFromCartUrl, urlParams),
            type: 'get',
            dataType: 'json',
            success: function () {
                $.spinner().stop();
            },
            error: function () {
                $.spinner().stop();
            }
        });
    }
    loader.hide();
    window.location.href = window.paypalUrls.cartPage;
}

/**
 *Inits paypal button on cart page
 */
function initPaypalButton() {
    loader.show();
    window.paypal.Buttons({
        createOrder,
        onApprove,
        onCancel,
        onError,
        style: Object(_helper__WEBPACK_IMPORTED_MODULE_2__["getPaypalButtonStyle"])(document.querySelector('.js_paypal_button_on_pdp_page'))
    }).render('.paypal-pdp-button')
        .then(() => {
            loader.hide();
        });
}

/* harmony default export */ __webpack_exports__["default"] = (initPaypalButton);


/***/ }),

/***/ "./cartridges/int_paypal/cartridge/client/default/js/pdp/pdp.js":
/*!**********************************************************************!*\
  !*** ./cartridges/int_paypal/cartridge/client/default/js/pdp/pdp.js ***!
  \**********************************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _initPdpButton__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./initPdpButton */ "./cartridges/int_paypal/cartridge/client/default/js/pdp/initPdpButton.js");
/* harmony import */ var _registered_initBillingAgreementButton__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./registered/initBillingAgreementButton */ "./cartridges/int_paypal/cartridge/client/default/js/pdp/registered/initBillingAgreementButton.js");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../api */ "./cartridges/int_paypal/cartridge/client/default/js/api.js");
/* harmony import */ var _pdpHelper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pdpHelper */ "./cartridges/int_paypal/cartridge/client/default/js/pdp/pdpHelper.js");
/* eslint-disable no-nested-ternary */





const loaderInstance = __webpack_require__(/*! ../loader */ "./cartridges/int_paypal/cartridge/client/default/js/loader.js");
let $loaderContainer = document.querySelector('.paypalLoader');
let loader = loaderInstance($loaderContainer);

let $paypalPdpImage = document.querySelector('#paypal_pdp_image');
let $paypalPDPButton = document.querySelector('.js_paypal_button_on_pdp_page');
let $isBAEnabled = $paypalPDPButton && JSON.parse($paypalPDPButton.getAttribute('data-paypal-ba-enabled'));
const paypalUrls = document.querySelector('.js_paypal-content').getAttribute('data-paypal-urls');
window.paypalUrls = JSON.parse(paypalUrls);

if ($paypalPdpImage) {
    $paypalPdpImage.addEventListener('click', function () {
        loader.show();
        var res = Object(_pdpHelper__WEBPACK_IMPORTED_MODULE_3__["addProductForPDPbtnFlow"])();
        if (res.cart) {
            Object(_api__WEBPACK_IMPORTED_MODULE_2__["returnFromCart"])();
        } else {
            loader.hide();
            throw new Error(res.message || 'Error occurs');
        }
    });
} else if (window.paypal) {
    $isBAEnabled ?
            Object(_registered_initBillingAgreementButton__WEBPACK_IMPORTED_MODULE_1__["default"])() :
            Object(_initPdpButton__WEBPACK_IMPORTED_MODULE_0__["default"])();
}


/***/ }),

/***/ "./cartridges/int_paypal/cartridge/client/default/js/pdp/pdpHelper.js":
/*!****************************************************************************!*\
  !*** ./cartridges/int_paypal/cartridge/client/default/js/pdp/pdpHelper.js ***!
  \****************************************************************************/
/*! exports provided: addProductForPDPbtnFlow, appendToUrl */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addProductForPDPbtnFlow", function() { return addProductForPDPbtnFlow; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "appendToUrl", function() { return appendToUrl; });
/**
 * Shows paypal PDP button/static image
 * @param {Object} $paypalPDPButton - paypal button element
 */
function showPDPButton($paypalPDPButton) {
    $paypalPDPButton.style.display = '';
}

/**
 * Hides paypal PDP button/static image
 * @param {Object} $paypalPDPButton - paypal button element
 */
function hidePDPButton($paypalPDPButton) {
    $paypalPDPButton.style.display = 'none';
}

/**
 * Adds product to basket on paypal button/static image click when PDP flow
 * @returns {Object} response with required data
 */
function addProductForPDPbtnFlow() {
    var $bundleItem = $('.bundle-item');
    /**
     * Gets options
     * @param {Object} $productContainer - product container
     * @returns {string} options
     */
    function getOptions($productContainer) {
        var options = $productContainer
            .find('.product-option')
            .map(function () {
                var $elOption = $(this).find('.options-select');
                var urlValue = $elOption.val();
                var selectedValueId = $elOption.find('option[value="' + urlValue + '"]')
                    .data('value-id');
                return {
                    optionId: $(this).data('option-id'),
                    selectedValueId: selectedValueId
                };
            }).toArray();

        return JSON.stringify(options);
    }

    var pid = $('.product-detail:not(".bundle-item")').data('pid');
    var $btn = $('.paypal_pdp_button');
    var $productContainer = $btn.closest('.product-detail');

    var form = {
        pid: pid,
        quantity: $('.quantity-select').val()
    };

    if (!$bundleItem.length) {
        form.options = getOptions($productContainer);
    } else {
        var items = $bundleItem.map(function () {
            return {
                pid: $(this).find('.product-id').text(),
                quantity: parseInt($(this).find('label.quantity').data('quantity'), 10)
            };
        });
        form.childProducts = JSON.stringify(items.toArray());
    }
    var response = $.ajax({
        url: $('.add-to-cart-url').val(),
        method: 'POST',
        async: false,
        data: form
    }).responseJSON;
    response.pid = pid;
    return response;
}

/**
 * Appends params to a url
 * @param {string} url - Original url
 * @param {Object} param - Parameters to append
 * @returns {string} result url with appended parameters
 */
function appendToUrl(url, param) {
    var newUrl = url;
    newUrl += (newUrl.indexOf('?') !== -1 ? '&' : '?') + Object.keys(param).map(function (key) {
        return key + '=' + encodeURIComponent(param[key]);
    }).join('&');

    return newUrl;
}

// Handling PDP button/static image behavior
var $paypalPDPButton = document.querySelector('.paypal_pdp_button');
if ($paypalPDPButton) {
    var $price = document.querySelector('.price .sales .value');
    var isZeroAmount = false;
    var $miniCartQuantity = document.querySelector('.minicart-quantity');
    var $addToCartButton = document.querySelector('.add-to-cart') || document.querySelector('.add-to-cart-global');

    // Check minicart quantity and hide PDPbutton if it is not empty
    if (($miniCartQuantity && parseInt($miniCartQuantity.textContent, 0) > 0)
        || ($price && $price.getAttribute('content') === '0.00')) {  // Check if product price is zero
        hidePDPButton($paypalPDPButton);
    }

    if ($addToCartButton.disabled) {
        hidePDPButton($paypalPDPButton);
    }

    $('body').on('product:afterAddToCart', function () {
        hidePDPButton($paypalPDPButton);
    });

    $('body').on('cart:update', function () {
        $miniCartQuantity = parseInt(document.querySelector('.minicart-quantity').textContent, 0);
        if ($addToCartButton.disabled) {
            hidePDPButton($paypalPDPButton);
        }
        if ($miniCartQuantity === 0 && !$addToCartButton.disabled) {
            showPDPButton($paypalPDPButton);
        }
    });

    $('body').on('product:statusUpdate', function () {
        $miniCartQuantity = parseInt(document.querySelector('.minicart-quantity').textContent, 0);
        $price = document.querySelector('.price .sales .value');
        isZeroAmount = false;
        if ($price) {
            isZeroAmount = $price.getAttribute('content') === '0.00';
        }

        if ($miniCartQuantity === 0) {
            if ($addToCartButton.disabled || isZeroAmount) {
                hidePDPButton($paypalPDPButton);
            }
            if (!$addToCartButton.disabled && !isZeroAmount) {
                showPDPButton($paypalPDPButton);
            }
        }
    });
}




/***/ }),

/***/ "./cartridges/int_paypal/cartridge/client/default/js/pdp/registered/initBillingAgreementButton.js":
/*!********************************************************************************************************!*\
  !*** ./cartridges/int_paypal/cartridge/client/default/js/pdp/registered/initBillingAgreementButton.js ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../api */ "./cartridges/int_paypal/cartridge/client/default/js/api.js");
/* harmony import */ var _pdpHelper_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../pdpHelper.js */ "./cartridges/int_paypal/cartridge/client/default/js/pdp/pdpHelper.js");
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../helper */ "./cartridges/int_paypal/cartridge/client/default/js/helper.js");




const loaderInstance = __webpack_require__(/*! ../../loader */ "./cartridges/int_paypal/cartridge/client/default/js/loader.js");
let $loaderContainer = document.querySelector('.paypalLoader');
let loader = loaderInstance($loaderContainer);
var pid;
var uuid;
var removeFromCartUrl;

/**
 *  Create's Billing Agreement
 *
 * @returns {string} returns JSON response that includes an data token
 */
function createBillingAgreement() {
    loader.show();
    var res = Object(_pdpHelper_js__WEBPACK_IMPORTED_MODULE_1__["addProductForPDPbtnFlow"])();
    if (res.cart) {
        uuid = res.pliUUID;
        removeFromCartUrl = res.cart.actionUrls.removeProductLineItemUrl;
        pid = res.pid;
    } else {
        loader.hide();
        throw new Error(res.message || 'Error occurs');
    }
    let isCartFlow = true;
    return Object(_api__WEBPACK_IMPORTED_MODULE_0__["getBillingAgreementToken"])(isCartFlow)
        .then((data) => data.token)
        .fail(() => {
            loader.hide();
        });
}

/**
 *  Makes post call and transfers billingToken to returnFromCart endpoint, triggers checkout (stage = place order)
 *
 * @param {string} billingToken - billing agreement token
 * @returns {Object} JSON response that includes the billing agreement ID and information about the payer
 */
function onApprove({ billingToken }) {
    return Object(_api__WEBPACK_IMPORTED_MODULE_0__["createBillingAgreementCall"])(billingToken)
        .then(({ id, payer }) => {
            let cartBillingFormData = Object(_api__WEBPACK_IMPORTED_MODULE_0__["createCartBillingFormData"])({
                billingAgreementID: id,
                billingAgreementPayerEmail: payer.payer_info.email
            }, document.querySelector('.js_paypal_button_on_pdp_page'));

            return $.ajax({
                type: 'POST',
                url: window.paypalUrls.returnFromCart,
                data: cartBillingFormData,
                contentType: false,
                processData: false,
                dataType: 'json'
            });
        })
        .then(() => {
            loader.hide();
            window.location.href = window.paypalUrls.placeOrderStage;
        })
        .fail(() => {
            loader.hide();
            window.location.href = window.paypalUrls.cartPage;
        });
}

/**
 * Hides loader on paypal widget closing without errors
 * If PDP flow, removes product from basket on cancel
 */
function onCancel() {
    var urlParams = {
        pid: pid,
        uuid: uuid
    };

    $.ajax({
        url: Object(_pdpHelper_js__WEBPACK_IMPORTED_MODULE_1__["appendToUrl"])(removeFromCartUrl, urlParams),
        type: 'get',
        dataType: 'json',
        success: function () {
            $.spinner().stop();
        },
        error: function () {
            $.spinner().stop();
        }
    });
    loader.hide();
}

/**
 * Shows errors if paypal widget was closed with errors
 * If PDP flow, removes product from basket on error
 */
function onError() {
    if (pid) {
        var urlParams = {
            pid: pid,
            uuid: uuid
        };

        $.ajax({
            url: Object(_pdpHelper_js__WEBPACK_IMPORTED_MODULE_1__["appendToUrl"])(removeFromCartUrl, urlParams),
            type: 'get',
            dataType: 'json',
            success: function () {
                $.spinner().stop();
            },
            error: function () {
                $.spinner().stop();
            }
        });
    }
    loader.hide();
    window.location.href = window.paypalUrls.cartPage;
}

/**
 *Inits paypal Billing Agreement button on billing checkout page
 */
function initPaypalBAButton() {
    loader.show();
    window.paypal.Buttons({
        createBillingAgreement,
        onApprove,
        onCancel,
        onError,
        style: Object(_helper__WEBPACK_IMPORTED_MODULE_2__["getPaypalButtonStyle"])(document.querySelector('.js_paypal_button_on_pdp_page'))
    }).render('.paypal-pdp-button')
        .then(() => {
            loader.hide();
        });
}

/* harmony default export */ __webpack_exports__["default"] = (initPaypalBAButton);


/***/ })

/******/ });
//# sourceMappingURL=pdp.js.map