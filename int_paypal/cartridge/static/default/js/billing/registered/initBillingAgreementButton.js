/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./cartridges/int_paypal/cartridge/client/default/js/billing/registered/initBillingAgreementButton.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./cartridges/int_paypal/cartridge/client/default/js/api.js":
/*!******************************************************************!*\
  !*** ./cartridges/int_paypal/cartridge/client/default/js/api.js ***!
  \******************************************************************/
/*! exports provided: updateOrderData, getPurchaseUnits, getBillingAgreementToken, createBillingAgreementCall, getOrderDetailsCall, returnFromCart, showCartErrorHtml, showCheckoutErrorHtml, finishLpmOrder, createCartBillingFormData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateOrderData", function() { return updateOrderData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPurchaseUnits", function() { return getPurchaseUnits; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getBillingAgreementToken", function() { return getBillingAgreementToken; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createBillingAgreementCall", function() { return createBillingAgreementCall; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getOrderDetailsCall", function() { return getOrderDetailsCall; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "returnFromCart", function() { return returnFromCart; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showCartErrorHtml", function() { return showCartErrorHtml; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showCheckoutErrorHtml", function() { return showCheckoutErrorHtml; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "finishLpmOrder", function() { return finishLpmOrder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createCartBillingFormData", function() { return createCartBillingFormData; });
var loaderInstance = __webpack_require__(/*! ./loader */ "./cartridges/int_paypal/cartridge/client/default/js/loader.js");
var $loaderContainer = document.querySelector('.paypalLoader');
var loader = loaderInstance($loaderContainer);
/**
 *  Appends error message on cart page
 *
 * @param {string} message error message
 */
function showCartErrorHtml(message) {
    $('.checkout-btn').addClass('disabled');
    $('.cart-error').append(
        `<div class="alert alert-danger alert-dismissible valid-cart-error fade show cartError" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            ${message}
        </div>`
    );
    window.scrollTo(0, 0);
}

/**
 *  Appends error message on billing checkout page
 *
 * @param {string} message error message
 */
function showCheckoutErrorHtml(message) {
    document.querySelector('.error-message-text').textContent = '';
    document.querySelector('.error-message').style.display = 'block';
    document.querySelector('.error-message-text').append(message);
    window.scrollTo(0, 0);
}

/**
 * Updates information about an order
 *
 * @returns {Object} Call handling result
 */
function updateOrderData() {
    loader.show();
    return $.ajax({
        url: window.paypalUrls.updateOrderData + '?isCartFlow=true',
        type: 'PATCH',
        success: () => {
            loader.hide();
            window.location.href = window.paypalUrls.placeOrderStage;
        },
        error: (err) => {
            loader.hide();
            var error = JSON.parse(err.responseText);
            showCartErrorHtml(error.errorMsg);
            if (error.transactionExpired) {
                location.reload();
            }
        }
    });
}

/**
 * Gets purchase units
 *
 * @returns {Object} with purchase units data
 */
function getPurchaseUnits() {
    return $.get(window.paypalUrls.getPurchaseUnit)
        .then(({ purchase_units }) => purchase_units);
}

/**
 * Gets Billing Agreement Token
 *
 * @param {boolean} isCartFlow - billing agreement flow from cart
 * @returns {string} billingToken - returns a JSON response that includes token, an approval URL
 */
function getBillingAgreementToken(isCartFlow = false) {
    return $.get(window.paypalUrls.createBillingAgreementToken + `?isCartFlow=${isCartFlow}`)
        .then((data) => data);
}

/**
 * Gets Billing Agreement
 * After buyer approval, you use a billing agreement token to create the agreement.
 *
 * @param {string} billingToken - billing agreement token
 * @returns {Object} JSON response body that includes the billing agreement ID,
 * the state of the agreement, which is ACTIVE, and information about the payer
 */
function createBillingAgreementCall(billingToken) {
    return $.ajax({
        url: window.paypalUrls.createBillingAgreement,
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({ billingToken })
    });
}

/**
 * Gets Order Details
 *
 * @param {string} orderId - billing agreement token
 * @returns {Object} JSON response body that includes payer email
 */
function getOrderDetailsCall(orderId) {
    return $.get(window.paypalUrls.getOrderDetails + `?orderId=${orderId}`)
        .then((data) => data);
}

/**
 * Create billing formData from fields data
 *
 * @param {Object} fieldsData - fields data values
 * @param {Elemtn} $paypalButton - paypal button selector
 * @returns {Object} cart billing form data
 */
function createCartBillingFormData(fieldsData, $paypalButton) {
    var cartBillingFormData = new FormData();
    if (!$paypalButton) {
        $paypalButton = document.querySelector('#paypal_pdp_image') || document.querySelector('#paypal_image');
    }
    var cartBillingFields = $paypalButton && JSON.parse($paypalButton.getAttribute('data-paypal-billing-form-fields'));

    Object.entries(cartBillingFields).forEach(entry => {
        const [key, field] = entry;
        if (typeof field === 'object') {
            cartBillingFormData.append(field.name, fieldsData && fieldsData[key] ? fieldsData[key] : field.value);
        }
    });

    return cartBillingFormData;
}

/**
 * Calls to returnFromCart endpoint, redirects to place order stage or shows error if it exists
 *
 * @returns {Object} Call handling result
 */
function returnFromCart() {
    loader.show();
    let cartBillingFormData = createCartBillingFormData();

    return $.ajax({
        url: window.paypalUrls.returnFromCart,
        type: 'POST',
        contentType: false,
        data: cartBillingFormData,
        processData: false,
        success: () => {
            loader.hide();
            window.location.href = window.paypalUrls.placeOrderStage;
        },
        error: function (err) {
            loader.hide();
            showCartErrorHtml(err.responseText);
        }
    });
}

/**
 * Call finishLpmOrder endpoint
 * @param  {Object} details billing address details
 * @returns {Promise} ajax call
 */
function finishLpmOrder(details) {
    const lpmName = document.querySelector('#usedPaymentMethod').value;
    const paypalMethodId = document.querySelector('#paypalMethodId').value;
    return $.ajax({
        url: window.paypalUrls.finishLpmOrder,
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            details,
            lpmName,
            paypalMethodId
        })
    });
}




/***/ }),

/***/ "./cartridges/int_paypal/cartridge/client/default/js/billing/billingHelper.js":
/*!************************************************************************************!*\
  !*** ./cartridges/int_paypal/cartridge/client/default/js/billing/billingHelper.js ***!
  \************************************************************************************/
/*! exports provided: injectBillingSDK, showPaypalBlock, showPaypalBtn, hidePaypalBtn, hideContinueButton, handleTabChange, togglePaypalBtnVisibility, updateSessionAccountEmail, isNewAccountSelected, updateClientSide, showContinueButton, isLpmUsed */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "injectBillingSDK", function() { return injectBillingSDK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showPaypalBlock", function() { return showPaypalBlock; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showPaypalBtn", function() { return showPaypalBtn; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hidePaypalBtn", function() { return hidePaypalBtn; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hideContinueButton", function() { return hideContinueButton; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleTabChange", function() { return handleTabChange; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "togglePaypalBtnVisibility", function() { return togglePaypalBtnVisibility; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateSessionAccountEmail", function() { return updateSessionAccountEmail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isNewAccountSelected", function() { return isNewAccountSelected; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateClientSide", function() { return updateClientSide; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showContinueButton", function() { return showContinueButton; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isLpmUsed", function() { return isLpmUsed; });
/* harmony import */ var _guest_initBillingButton__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./guest/initBillingButton */ "./cartridges/int_paypal/cartridge/client/default/js/billing/guest/initBillingButton.js");
/* harmony import */ var _registered_initBillingAgreementButton__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./registered/initBillingAgreementButton */ "./cartridges/int_paypal/cartridge/client/default/js/billing/registered/initBillingAgreementButton.js");
/* harmony import */ var _registered_billingAgreementHelper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./registered/billingAgreementHelper */ "./cartridges/int_paypal/cartridge/client/default/js/billing/registered/billingAgreementHelper.js");




let $paypalButton = document.querySelector('.js_paypal_button_on_billing_form');
let $paypalAccountsDropdown = document.querySelector('#paypalAccountsDropdown');
const $continueButton = document.querySelector('button[value=submit-payment]');
let isRegisteredUser = document.querySelector('.data-checkout-stage') && document.querySelector('.data-checkout-stage').getAttribute('data-customer-type') === 'registered';
let $restPaypalAccountsList = document.querySelector('#restPaypalAccountsList');
let $billingButtonContainer = document.querySelector('#billing-paypal-button-container');
let isBAEnabled = $billingButtonContainer && JSON.parse($billingButtonContainer.getAttribute('data-is-ba-enabled'));

/**
 * Shows continue button if it's not visible
 */
function showContinueButton() {
    if ($continueButton.style.display !== '') {
        $continueButton.style.display = '';
    }
}

/**
 * Hides continue button if it's not hidden
 */
function hideContinueButton() {
    if ($continueButton.style.display !== 'none') {
        $continueButton.style.display = 'none';
    }
}

/**
 * Shows PayPal div container if it's not visible and hides continue button
*/
function showPaypalBtn() {
    if (!$paypalButton) {
        $paypalButton = document.querySelector('.js_paypal_button_on_billing_form');
    }
    if ($paypalButton.style.display !== 'block') {
        $paypalButton.style.display = 'block';
    }
    hideContinueButton();
}

/**
 * Hides PayPal div container if it's not hidden and shows continue button
*/
function hidePaypalBtn() {
    if (!$paypalButton) {
        $paypalButton = document.querySelector('.js_paypal_button_on_billing_form');
    }
    if ($paypalButton.style.display !== 'none') {
        $paypalButton.style.display = 'none';
    }
    showContinueButton();
}

/**
 * Shows PayPal block with accounts dropdown if it's not visible
*/
function showPaypalBlock() {
    if (!$paypalAccountsDropdown) {
        $paypalAccountsDropdown = document.querySelector('#paypalAccountsDropdown');
    }
    if ($paypalAccountsDropdown.style.display !== 'block') {
        $paypalAccountsDropdown.style.display = 'block';
    }
}

/**
 * Hides PayPal block with accounts dropdown if it's visible
*/
function hidePaypalBlock() {
    if (!$paypalAccountsDropdown) {
        $paypalAccountsDropdown = document.querySelector('#paypalAccountsDropdown');
    }
    if ($paypalAccountsDropdown.style.display !== 'none') {
        $paypalAccountsDropdown.style.display = 'none';
    }
}

/**
 * Injects SDK into billing page
*/
function injectBillingSDK() {
    var head = document.getElementsByTagName('head').item(0);
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.onload = function () {
        isBAEnabled ?
            Object(_registered_initBillingAgreementButton__WEBPACK_IMPORTED_MODULE_1__["default"])() :
            Object(_guest_initBillingButton__WEBPACK_IMPORTED_MODULE_0__["default"])();
    };
    script.src = window.paypalUrls.billingSdkUrl;
    script.setAttribute('data-partner-attribution-id', window.paypalUrls.partnerAttributionId);
    head.appendChild(script);
}

/**
 * Shows is new account selected
 * @param {Element} $accountList - $accountList element
 * @returns {boolean} value whether new account selected
*/
function isNewAccountSelected($accountList) {
    return $accountList
        .querySelector('option:checked')
        .value === 'newaccount';
}

/**
 * Changes PayPal button visibility depending on checked option of element
 * @param {Element} $accountList - $accountList element
*/
function togglePaypalBtnVisibility($accountList) {
    isNewAccountSelected($accountList) ?
        showPaypalBtn() :
        hidePaypalBtn();
}

/**
 * Handles tabs changing
 * @param {event} e - event
*/
function handleTabChange(e) {
    const isPaypalContentSelected = e.target.hash === '#paypal-content';
    if (!isPaypalContentSelected) {
        showContinueButton();
        return;
    }
    isNewAccountSelected($restPaypalAccountsList) ?
        hideContinueButton() :
        showContinueButton();
    if (isRegisteredUser && isBAEnabled) {
        Object(_registered_billingAgreementHelper__WEBPACK_IMPORTED_MODULE_2__["assignEmailForSavedBA"])();
        Object(_registered_billingAgreementHelper__WEBPACK_IMPORTED_MODULE_2__["handleCheckboxChange"])();
    }
}

/**
 * Updates session account email if it is differ from existed or email doesn't exist (for guest or disabled billing agreement)
 * @param {Object} _ - arg
 *
*/
function updateSessionAccountEmail(_, { order: { paypalPayerEmail } }) {
    if (!paypalPayerEmail) return;
    showPaypalBlock();
    const $sessionPaypalAccount = document.querySelector('#sessionPaypalAccount');
    if ($sessionPaypalAccount && $sessionPaypalAccount.value !== paypalPayerEmail) {
        $sessionPaypalAccount.value = paypalPayerEmail;
        $sessionPaypalAccount.innerText = paypalPayerEmail;
        $sessionPaypalAccount.selected = true;
        $restPaypalAccountsList.onchange();
    }
}

/**
 * Updates paypal content to initial state on client side if payment method was changed from paypal to different one
 * @param {Object} _ - arg
 * @param {Object} customer - customer data object
*/
function updateClientSide(_, customer) {
    var selectedPaymentInstruments = customer.order.billing.payment.selectedPaymentInstruments;
    var paypalPaymentMethod = document.querySelector('.nav-link.paypal-tab').parentElement.getAttribute('data-method-id');
    var giftCertTabLink = document.querySelector('.nav-link.gift-cert-tab');
    var $sessionPaypalAccount = $restPaypalAccountsList.querySelector('option[id=sessionPaypalAccount]');

    if (selectedPaymentInstruments.length > 0 && ($sessionPaypalAccount && $sessionPaypalAccount.value !== '')) {
        selectedPaymentInstruments.forEach(paymentInstr => {
            if (paymentInstr.paymentMethod !== paypalPaymentMethod) {
                if (!isBAEnabled) {
                    $sessionPaypalAccount.value = '';
                    $restPaypalAccountsList.querySelector('option:checked').value = 'newaccount';
                    hidePaypalBlock();
                    togglePaypalBtnVisibility($restPaypalAccountsList);
                } else if (isBAEnabled && $sessionPaypalAccount) {
                    Object(_registered_billingAgreementHelper__WEBPACK_IMPORTED_MODULE_2__["clearSessionOption"])();
                    var $defaultBA = $restPaypalAccountsList.querySelector('option[data-default=true]');
                    $defaultBA ? $defaultBA.selected = true : hidePaypalBlock();
                    Object(_registered_billingAgreementHelper__WEBPACK_IMPORTED_MODULE_2__["toggleBABtnVisibility"])();
                }
                injectBillingSDK();
            }
        });
    } else if (selectedPaymentInstruments.length > 0 && giftCertTabLink) {
        if (selectedPaymentInstruments.find(paymentInstr => paymentInstr.paymentMethod === giftCertTabLink.parentElement.getAttribute('data-method-id'))) {
            giftCertTabLink.click();
        }
    }
}

/**
 * Returns value whether LPM was used or not
 * @param {Element} $usedPaymentMethod - $usedPaymentMethod element
 * @returns {boolean} value whether LPM was used
*/
function isLpmUsed($usedPaymentMethod) {
    const disableFunds = [
        'sepa',
        'bancontact',
        'eps',
        'giropay',
        'ideal',
        'mybank',
        'p24',
        'sofort'
    ];
    if (disableFunds.indexOf($usedPaymentMethod.value) !== -1) {
        return true;
    }
    return false;
}




/***/ }),

/***/ "./cartridges/int_paypal/cartridge/client/default/js/billing/guest/initBillingButton.js":
/*!**********************************************************************************************!*\
  !*** ./cartridges/int_paypal/cartridge/client/default/js/billing/guest/initBillingButton.js ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../api */ "./cartridges/int_paypal/cartridge/client/default/js/api.js");
/* harmony import */ var _billingHelper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../billingHelper */ "./cartridges/int_paypal/cartridge/client/default/js/billing/billingHelper.js");
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../helper */ "./cartridges/int_paypal/cartridge/client/default/js/helper.js");
/* eslint-disable no-useless-escape */
/* eslint-disable no-control-regex */




const loaderInstance = __webpack_require__(/*! ../../loader */ "./cartridges/int_paypal/cartridge/client/default/js/loader.js");
let $loaderContainer = document.querySelector('.paypalLoader');
let loader = loaderInstance($loaderContainer);
let $usedPaymentMethod = document.querySelector('#usedPaymentMethod');
const regExpPhone = new RegExp(/^[0-9]{1,14}?$/);
const regExpEmail = new RegExp(/(?:[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+)*|(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-zA-Z0-9-]*[a-zA-Z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/);
const notEmptyString = new RegExp(/=(?!\s*$).+/);

/**
 *  Filters valid form elements
 *
 * @param {string} str - arg
 * @returns {string} with valid form element
 */
function filterValidFormElement(str) {
    return (str.indexOf('addressFields') !== -1 || str.indexOf('contactInfoFields') !== -1)
        && notEmptyString.test(str);
}

/**
 *  Parses billing fields
 *
 * @param {string} acc -
 * @param {string} str - paypal actions
 * @returns {string} decoded string
 */
function parseBillingFields(acc, str) {
    let [key, value] = str.split('=');
    key = key.split('_');
    key = key[key.length - 1];
    const inputValue = decodeURIComponent(value);
    if (key === 'phone' || key === 'email') {
        let validInput = (key === 'phone') ?
            regExpPhone.test(inputValue) :
            regExpEmail.test(inputValue);
        if (!validInput) return acc;
    }
    acc[key] = inputValue;
    return acc;
}

/**
 *  Creates billing address, serializes address into form
 *
 * @returns {Object} with created billing address
 */
function createBillingAddress() {
    return $('#dwfrm_billing')
        .serialize()
        .split('&')
        .filter(filterValidFormElement)
        .reduce(parseBillingFields, {});
}

/**
 *  Creates payer object with billing address data
 *
 * @param {Object} billingAddress - billing address
 * @returns {Object} with payer data
 */
function createPayerObject(billingAddress) {
    if (billingAddress.country && billingAddress.phone) {
        return {
            name: {
                given_name: billingAddress.firstName,
                surname: billingAddress.lastName
            },
            email_address: billingAddress.email,
            phone: {
                phone_number: {
                    national_number: billingAddress.phone
                }
            },
            address: {
                address_line_1: billingAddress.address1,
                address_line_2: billingAddress.address2 || '',
                admin_area_2: billingAddress.city,
                admin_area_1: billingAddress.stateCode,
                postal_code: billingAddress.postalCode,
                country_code: billingAddress.country
            }
        };
    }
    return false;
}

/**
 * Check for contactInfoEmail input field and saves used payment method to hidden input
 *
 * @param {Object} data - object with data
 * @param {Object} actions - actions
 * @returns {Function} reject - if incorrect email or set PaymentMethod
 */
function onClick(data, actions) {
    let $contactInfoEmail = document.querySelector('input[name=dwfrm_billing_contactInfoFields_email]');
    let errDiv = $contactInfoEmail.parentElement.querySelector('.invalid-feedback');
    let errStr = 'Please enter a valid email address';
    if ($contactInfoEmail.value.trim() !== '' &&
        !regExpEmail.test($contactInfoEmail.value)) {
        Object(_api__WEBPACK_IMPORTED_MODULE_0__["showCheckoutErrorHtml"])(errStr);
        errDiv.innerText = errStr;
        errDiv.style = 'display: block';
        $contactInfoEmail.style = 'border-color: red';

        return actions.reject();
    }

    errDiv.innerText = '';
    errDiv.style = 'display: none';
    $contactInfoEmail.style = 'border-color: rgb(206, 212, 218)';

    $usedPaymentMethod.value = data.fundingSource;
}
/**
 *  Gets purchase units object, creates order and returns object with data
 *
 * @param {Object} _ - arg
 * @param {Object} actions - paypal actions
 * @returns {Object} with purchase units, payer and application context
 */
function createOrder(_, actions) {
    loader.show();
    return Object(_api__WEBPACK_IMPORTED_MODULE_0__["getPurchaseUnits"])()
        .then(purchase_units => {
            let parsedPurchaseUnit = purchase_units[0];
            if (JSON.parse(parsedPurchaseUnit.amount.value) === 0) {
                Object(_api__WEBPACK_IMPORTED_MODULE_0__["showCheckoutErrorHtml"])('Order total 0 is not allowed for PayPal');
            }
            var payer;
            const payerObj = createPayerObject(createBillingAddress());
            if (payerObj) {
                payer = payerObj;
            }
            const application_context = {
                shipping_preference: parsedPurchaseUnit.shipping_preference
            };
            loader.hide();
            return actions.order.create({
                purchase_units,
                payer,
                application_context
            });
        });
}

/**
 * Sets orderID to hidden input, clears session account if it exists and irrelevant errors,
 * and clicks submit payment button
 *
 * @param {Object} data - object with data
 * @param {Object} actions - actions
 *
 */
function onApprove(data, actions) {
    loader.show();
    if (Object(_billingHelper__WEBPACK_IMPORTED_MODULE_1__["isLpmUsed"])($usedPaymentMethod)) {
        actions.order.capture()
            .then(_api__WEBPACK_IMPORTED_MODULE_0__["finishLpmOrder"])
            .then(({ redirectUrl }) => {
                loader.hide();
                window.location.href = redirectUrl;
            })
            .catch(function () {
                loader.hide();
            });
        return;
    }
    var $orderId = document.querySelector('#paypal_orderId');
    var $selectedAccount = document.querySelector('#sessionPaypalAccount');
    $orderId.value = data.orderID;

    if ($selectedAccount.value !== '') {
        $selectedAccount.value = '';
        $selectedAccount.innerText = '';
    }
    $selectedAccount.selected = true;
    $selectedAccount.style.display = 'block';

    var $contactInfoEmail = document.querySelector('input[name=dwfrm_billing_contactInfoFields_email]');
    if ($contactInfoEmail.value.trim() !== '') {
        document.querySelector('button.submit-payment').click();
        loader.hide();
    } else {
        Object(_api__WEBPACK_IMPORTED_MODULE_0__["getOrderDetailsCall"])(data.orderID)
            .then((orderData) => {
                $contactInfoEmail.value = orderData.payer.email_address;
                document.querySelector('button.submit-payment').click();
                loader.hide();
            })
            .fail(() => {
                loader.hide();
            });
    }
}

/**
 * Hides loader on paypal widget closing without errors
 *
 */
function onCancel() {
    loader.hide();
}

/**
 * Shows errors if paypal widget was closed with errors
 *
 */
function onError() {
    loader.hide();
    if (document.querySelector('.error-message').style.display !== 'block') {
        Object(_api__WEBPACK_IMPORTED_MODULE_0__["showCheckoutErrorHtml"])('An internal server error has occurred. \r\nRetry the request later.');
    }
}

/**
 *Inits paypal button on billing checkout page
 */
function initPaypalButton() {
    loader.show();
    window.paypal.Buttons({
        onClick,
        createOrder,
        onApprove,
        onCancel,
        onError,
        style: Object(_helper__WEBPACK_IMPORTED_MODULE_2__["getPaypalButtonStyle"])(document.querySelector('.js_paypal_button_on_billing_form'))
    }).render('.paypal-checkout-button')
        .then(() => {
            loader.hide();
        });
}

/* harmony default export */ __webpack_exports__["default"] = (initPaypalButton);


/***/ }),

/***/ "./cartridges/int_paypal/cartridge/client/default/js/billing/registered/billingAgreementHelper.js":
/*!********************************************************************************************************!*\
  !*** ./cartridges/int_paypal/cartridge/client/default/js/billing/registered/billingAgreementHelper.js ***!
  \********************************************************************************************************/
/*! exports provided: toggleBABtnVisibility, assignEmailForSavedBA, handleCheckboxChange, clearSessionOption, updateSessionOption, setBAFormValues */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "toggleBABtnVisibility", function() { return toggleBABtnVisibility; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "assignEmailForSavedBA", function() { return assignEmailForSavedBA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleCheckboxChange", function() { return handleCheckboxChange; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "clearSessionOption", function() { return clearSessionOption; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateSessionOption", function() { return updateSessionOption; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setBAFormValues", function() { return setBAFormValues; });
/* harmony import */ var _billingHelper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../billingHelper */ "./cartridges/int_paypal/cartridge/client/default/js/billing/billingHelper.js");


let $billingBAbutton = document.querySelector('.paypal-checkout-ba-button');
let $restPaypalAccountsList = document.querySelector('#restPaypalAccountsList');
let $paypalAccountSave = document.querySelector('#savePaypalAccount');
let $paypalAccountMakeDefault = document.querySelector('#paypalAccountMakeDefault');

/**
 * Sets BA id & email to form values
 * @param {string} baID - billing agreement active ID
 * @param {string} baEmail - billing agreement active email
*/
function setBAFormValues(baID, baEmail) {
    document.getElementById('billingAgreementID').value = baID;
    document.getElementById('billingAgreementPayerEmail').value = baEmail;
}

/** Shows PayPal BA button if it's not visible and hides continue button
*/
function showPaypalBABtn() {
    if (!$billingBAbutton) {
        $billingBAbutton = document.querySelector('.paypal-checkout-ba-button');
    }
    if ($billingBAbutton.style.display !== 'block') {
        $billingBAbutton.style.display = 'block';
    }
    Object(_billingHelper__WEBPACK_IMPORTED_MODULE_0__["hideContinueButton"])();
}

/** Hides PayPal BA button if it's not hidden and shows continue button
*/
function hidePaypalBABtn() {
    if (!$billingBAbutton) {
        $billingBAbutton = document.querySelector('.paypal-checkout-ba-button');
    }
    if ($billingBAbutton.style.display !== 'none') {
        $billingBAbutton.style.display = 'none';
    }
    Object(_billingHelper__WEBPACK_IMPORTED_MODULE_0__["showContinueButton"])();
}

/** Put value of checkbox makeDefault/saveAccount for backend
*/
function saveCheckboxState() {
    var $paypal_makeDefault = document.querySelector('#paypal_makeDefault');
    var $paypal_saveAccount = document.querySelector('#paypal_saveAccount');
    $paypal_makeDefault.value = $paypalAccountMakeDefault.checked;
    $paypal_saveAccount.value = $paypalAccountSave.checked;
}

/** Handle makeDefault/saveAccount checkboxes state on change
*/
function handleCheckboxChange() {
    let $selectedAccount = $restPaypalAccountsList.querySelector('option:checked');
    let isSessionAccountAppended = JSON.parse($selectedAccount.getAttribute('data-append'));
    let hasDefaultPaymentMethod = JSON.parse($restPaypalAccountsList.getAttribute('data-has-default-account'));

    if (isSessionAccountAppended || $selectedAccount.value === 'newaccount') {
        if (!$paypalAccountSave.checked) {
            $paypalAccountMakeDefault.checked = false;
            $paypalAccountMakeDefault.disabled = true;
        } else {
            $paypalAccountMakeDefault.disabled = false;
            if (!hasDefaultPaymentMethod) {
                $paypalAccountMakeDefault.checked = true;
            }
        }
    }
    saveCheckboxState();
}

/** Show/hide/check/disable checkboxes depends on selected type of account
*/
function toggleCustomCheckbox() {
    let $selectedAccount = $restPaypalAccountsList.querySelector('option:checked');
    let $paypalAccountMakeDefaultContainer = document.querySelector('#paypalAccountMakeDefaultContainer');
    let $paypalAccountSaveContainer = document.querySelector('#savePaypalAccountContainer');
    let hasPPSavedAccount = JSON.parse($restPaypalAccountsList.getAttribute('data-has-saved-account'));
    let hasDefaultPaymentMethod = JSON.parse($restPaypalAccountsList.getAttribute('data-has-default-account'));
    let isSessionAccountAppended = JSON.parse($selectedAccount.getAttribute('data-append'));
    let isBALimitReached = JSON.parse($restPaypalAccountsList.getAttribute('data-ba-limit-reached'));

    if ($paypalAccountSaveContainer) {
        if ($selectedAccount.dataset.default === 'true') {
            $paypalAccountMakeDefaultContainer.style.display = 'none';
            $paypalAccountMakeDefault.checked = true;
            $paypalAccountMakeDefault.disabled = false;
            if (hasPPSavedAccount && !hasDefaultPaymentMethod) {
                $paypalAccountSave.checked = true;
            } else {
                $paypalAccountSaveContainer.style.display = 'none';
            }
            saveCheckboxState();
        }
        var isDataSetDefault = $selectedAccount.dataset.default === 'false' || $selectedAccount.dataset.default === 'null';
        if (isDataSetDefault && !($selectedAccount.value === 'newaccount') && !isSessionAccountAppended) {
            $paypalAccountMakeDefaultContainer.style.display = 'block';
            $paypalAccountSaveContainer.style.display = 'none';
            $paypalAccountSave.checked = false;
            $paypalAccountMakeDefault.disabled = false;
        }

        if ($selectedAccount.value === 'newaccount' || isSessionAccountAppended) {
            if (!hasPPSavedAccount) {
                $paypalAccountMakeDefaultContainer.style.display = 'none';
                $paypalAccountMakeDefault.checked = true;
                $paypalAccountMakeDefault.disabled = false;
                $paypalAccountSaveContainer.style.display = 'block';
                if (($selectedAccount.value !== 'newaccount' && (hasDefaultPaymentMethod || isSessionAccountAppended)) ||
                    ($selectedAccount.value === 'newaccount' && !isSessionAccountAppended)) {
                    $paypalAccountSave.checked = true;
                } else {
                    $paypalAccountSave.checked = false;
                }

                saveCheckboxState();
                return;
            }
            handleCheckboxChange();

            if (isBALimitReached) {
                $paypalAccountSaveContainer.style.display = 'none';
                $paypalAccountMakeDefaultContainer.style.display = 'none';
            } else {
                $paypalAccountSaveContainer.style.display = 'block';
                $paypalAccountMakeDefaultContainer.style.display = 'block';
            }

            if (hasDefaultPaymentMethod) {
                return;
            }
            hasPPSavedAccount && !hasDefaultPaymentMethod ?
                $paypalAccountMakeDefaultContainer.style.display = 'none' :
                $paypalAccountMakeDefault.disabled = true;

            $paypalAccountMakeDefault.checked = true;
        }
    }
}

/** Show billing agreement btn - hide paypal btn and vise versa
*/
function toggleBABtnVisibility() {
    toggleCustomCheckbox();

    if (Object(_billingHelper__WEBPACK_IMPORTED_MODULE_0__["isNewAccountSelected"])($restPaypalAccountsList)) {
        showPaypalBABtn();
        Object(_billingHelper__WEBPACK_IMPORTED_MODULE_0__["hideContinueButton"])();
        return;
    }
    hidePaypalBABtn();
    Object(_billingHelper__WEBPACK_IMPORTED_MODULE_0__["showPaypalBlock"])();
}

/** Assign billing agreement emails on change into input field
*/
function assignEmailForSavedBA() {
    let $paypalActiveAccount = document.querySelector('#paypal_activeAccount');
    let $contractInfoeEmail = document.querySelector('input[name=dwfrm_billing_contactInfoFields_email]');
    let $selectedAccount = $restPaypalAccountsList.querySelector('option:checked');

    if (Object(_billingHelper__WEBPACK_IMPORTED_MODULE_0__["isNewAccountSelected"])($restPaypalAccountsList)) {
        $paypalActiveAccount.value = '';
        $contractInfoeEmail.value = '';
        document.getElementById('billingAgreementID').value = '';
        document.getElementById('billingAgreementPayerEmail').value = '';
    } else {
        $paypalActiveAccount.value = $restPaypalAccountsList.querySelector('option:checked').value;
        $contractInfoeEmail.value = $paypalActiveAccount.value;
        setBAFormValues($selectedAccount.dataset.baId, $selectedAccount.value);
    }
}

/**
 *  Clear element to an Existing restPaypalAccountsList Collection
 *
 */
function clearSessionOption() {
    var $option = document.querySelector('#sessionPaypalAccount');
    $option.text = '';
    $option.value = '';
    $option.setAttribute('data-append', false);
    $option.selected = false;
    $option.style.display = 'none';
    $option.setAttribute('data-ba-id', '');
    document.getElementById('billingAgreementID').value = '';
    document.getElementById('billingAgreementPayerEmail').value = '';

    toggleBABtnVisibility();
}

/**
 *  Update element under restPaypalAccountsList Collection
 *
 * @param {string} email - billing agreement email
 */
function updateSessionOption(email) {
    var $option = document.querySelector('#sessionPaypalAccount');
    $option.text = email;
    $option.value = email;
    $option.selected = 'selected';
    $option.style.display = 'block';
    $option.setAttribute('data-append', true);
    document.querySelector('#restPaypalAccountsList').value = email;

    hidePaypalBABtn();
    Object(_billingHelper__WEBPACK_IMPORTED_MODULE_0__["showPaypalBlock"])();
    Object(_billingHelper__WEBPACK_IMPORTED_MODULE_0__["showContinueButton"])();
}




/***/ }),

/***/ "./cartridges/int_paypal/cartridge/client/default/js/billing/registered/initBillingAgreementButton.js":
/*!************************************************************************************************************!*\
  !*** ./cartridges/int_paypal/cartridge/client/default/js/billing/registered/initBillingAgreementButton.js ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../api */ "./cartridges/int_paypal/cartridge/client/default/js/api.js");
/* harmony import */ var _billingAgreementHelper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./billingAgreementHelper */ "./cartridges/int_paypal/cartridge/client/default/js/billing/registered/billingAgreementHelper.js");
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../helper */ "./cartridges/int_paypal/cartridge/client/default/js/helper.js");
/* eslint-disable no-useless-escape */
/* eslint-disable no-control-regex */




const loaderInstance = __webpack_require__(/*! ../../loader */ "./cartridges/int_paypal/cartridge/client/default/js/loader.js");
const regExprEmail = new RegExp(/(?:[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+)*|(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-zA-Z0-9-]*[a-zA-Z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/);
let $loaderContainer = document.querySelector('.paypalLoader');
let loader = loaderInstance($loaderContainer);

/**
 * Check for contactInfoEmail input field if not empty
 *
 * @param {Object} _ - arg
 * @param {Object} actions - paypal actions
 * @returns {Function} reject - if incorrect email
 */
function onClick(_, actions) {
    let $contactInfoEmail = document.querySelector('input[name=dwfrm_billing_contactInfoFields_email]');
    let errDiv = $contactInfoEmail.parentElement.querySelector('.invalid-feedback');
    let errStr = 'Please enter a valid email address';
    if ($contactInfoEmail.value.trim() !== ''
        && !regExprEmail.test($contactInfoEmail.value)) {
        Object(_api__WEBPACK_IMPORTED_MODULE_0__["showCheckoutErrorHtml"])(errStr);
        errDiv.innerText = errStr;
        errDiv.style = 'display: block';
        $contactInfoEmail.style = 'border-color: red';
        return actions.reject();
    }

    errDiv.innerText = '';
    errDiv.style = 'display: none';
    $contactInfoEmail.style = 'border-color: rgb(206, 212, 218)';
}

/**
 *  Create's Billing Agreement
 *
 * @returns {string} returns JSON response that includes an data token
 */
function createBillingAgreement() {
    loader.show();
    return Object(_api__WEBPACK_IMPORTED_MODULE_0__["getBillingAgreementToken"])()
        .then((data) => data.token)
        .fail(() => {
            loader.hide();
        });
}

/**
 *  Makes post call using facilitator Access Token and transfers billingToken
 *  save's billingAgreementID & billingAgreementPayerEmail to input field
 *  and triggers checkout place order stage
 *
 * @param {string} billingToken - billing agreement token
 * @returns {Object} JSON response that includes the billing agreement ID and information about the payer
 */
function onApprove({ billingToken }) {
    return Object(_api__WEBPACK_IMPORTED_MODULE_0__["createBillingAgreementCall"])(billingToken)
        .then(({ id, payer }) => {
            let payerInfo = payer.payer_info;
            let billingAddress = payerInfo.billing_address;
            let $restPaypalAccountsList = document.querySelector('#restPaypalAccountsList');
            let hasDefaultPaymentMethod = JSON.parse($restPaypalAccountsList.getAttribute('data-has-default-account'));
            let hasPPSavedAccount = JSON.parse($restPaypalAccountsList.getAttribute('data-has-saved-account'));
            document.querySelector('#sessionPaypalAccount').setAttribute('data-ba-id', id);
            Object(_billingAgreementHelper__WEBPACK_IMPORTED_MODULE_1__["setBAFormValues"])(id, payerInfo.email);

            if (hasDefaultPaymentMethod && !hasPPSavedAccount) {
                $restPaypalAccountsList.setAttribute('data-has-default-account', false);
                $('#restPaypalAccountsList').data('data-has-default-account', false); // MFRA jquery hack
            }

            let billingAddressFields = [];
            let contactInfoFields = [].slice.call(document.querySelectorAll('.contact-info-block input[required]'));

            var keysValues = {
                email: payerInfo.email,
                phoneNumber: payerInfo.phone,
                billingFirstName: payerInfo.first_name,
                billingLastName: payerInfo.last_name,
                billingAddressOne: billingAddress.line1,
                billingCountry: billingAddress.country_code,
                billingState: billingAddress.state,
                billingAddressCity: billingAddress.city,
                billingZipCode: billingAddress.postal_code
            };
            let $billinAddressBlock = document.querySelector('.billing-address-block');
            if ($billinAddressBlock.querySelector('#billingAddressSelector option:checked').value === 'new') {
                billingAddressFields = [].slice.call($billinAddressBlock.querySelectorAll('input[required], select[required]'));
            }

            (contactInfoFields.concat(billingAddressFields)
                .filter(el => el.value.trim() === ''))
                .forEach(function (el) {
                    el.value = keysValues[el.id];
                });

            document.querySelector('button.submit-payment').click();
            var sameAttribute = [].slice.call($restPaypalAccountsList.options)
                    .find(el => el.value === payerInfo.email);

            (sameAttribute && sameAttribute.id !== 'sessionPaypalAccount') ?
                Object(_billingAgreementHelper__WEBPACK_IMPORTED_MODULE_1__["clearSessionOption"])() :
                Object(_billingAgreementHelper__WEBPACK_IMPORTED_MODULE_1__["updateSessionOption"])(payerInfo.email);

            loader.hide();
        })
        .fail(() => {
            loader.hide();
        });
}

/**
 * Hides loader on paypal widget closing without errors

 */
function onCancel() {
    loader.hide();
}

/**
 * Shows errors if paypal widget was closed with errors
 *
 */
function onError() {
    loader.hide();
    Object(_api__WEBPACK_IMPORTED_MODULE_0__["showCheckoutErrorHtml"])('An internal server error has occurred. \r\nRetry the request later.');
}

/**
 *Inits paypal Billing Agreement button on billing checkout page
 */
function initPaypalBAButton() {
    loader.show();
    window.paypal.Buttons({
        onClick,
        createBillingAgreement,
        onApprove,
        onCancel,
        onError,
        style: Object(_helper__WEBPACK_IMPORTED_MODULE_2__["getPaypalButtonStyle"])(document.querySelector('.paypal-checkout-ba-button'))
    }).render('.paypal-checkout-ba-button')
        .then(() => {
            loader.hide();
        });
}

/* harmony default export */ __webpack_exports__["default"] = (initPaypalBAButton);


/***/ }),

/***/ "./cartridges/int_paypal/cartridge/client/default/js/helper.js":
/*!*********************************************************************!*\
  !*** ./cartridges/int_paypal/cartridge/client/default/js/helper.js ***!
  \*********************************************************************/
/*! exports provided: getPaypalButtonStyle */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPaypalButtonStyle", function() { return getPaypalButtonStyle; });
const defaultStyle = {
    color: 'gold',
    shape: 'rect',
    layout: 'vertical',
    label: 'paypal',
    tagline: false
};

/**
 *  Gets paypal button styles
 * @param {Element} button - button element
 * @returns {Object} with button styles or if error appears with default styles
 */
function getPaypalButtonStyle(button) {
    try {
        const config = button.getAttribute('data-paypal-button-config');
        if (config) {
            const buttonConfigs = JSON.parse(config);
            return buttonConfigs.style;
        }
    } catch (error) {
        return {
            style: defaultStyle
        };
    }
}




/***/ }),

/***/ "./cartridges/int_paypal/cartridge/client/default/js/loader.js":
/*!*********************************************************************!*\
  !*** ./cartridges/int_paypal/cartridge/client/default/js/loader.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* eslint-disable require-jsdoc */
module.exports = function (containerElement) {
    function Constructor() {
        this.containerEl = containerElement;
    }
    Constructor.prototype.show = function () {
        this.containerEl.style.display = 'block';
    };
    Constructor.prototype.hide = function () {
        this.containerEl.style.display = 'none';
    };
    return new Constructor();
};


/***/ })

/******/ });
//# sourceMappingURL=initBillingAgreementButton.js.map