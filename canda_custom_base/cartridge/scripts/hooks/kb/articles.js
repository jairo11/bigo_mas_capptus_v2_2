'use strict';

var Rest = require('*/cartridge/scripts/services/kb/articles');

function getCategoryArticles(category) {
    return Rest.kbCategoryArticles.call(category);
}

exports.getCategoryArticles = getCategoryArticles;
