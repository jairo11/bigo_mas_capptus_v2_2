'use strict';

var Rest = require('*/cartridge/scripts/services/kb/article');

function getCategoryArticle(article) {
    return Rest.kbCategoryArticle.call(article);
}

exports.getCategoryArticle = getCategoryArticle;
