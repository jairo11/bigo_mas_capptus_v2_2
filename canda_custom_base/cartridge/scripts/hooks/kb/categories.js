'use strict';

var Rest = require('*/cartridge/scripts/services/kb/categories');

function getCategories() {
    return Rest.kbCategories.call();
}

exports.getCategories = getCategories;
