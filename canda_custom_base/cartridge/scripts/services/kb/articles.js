'use strict'

var Logger = require('dw/system/Logger');
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

var kbCategoryArticles = LocalServiceRegistry.createService('cya.kb.api.category.articles', {
    createRequest: function (svc, args) {
        var json = '{"C_A_Commerce":"' + args +'"}';
        var url = svc.configuration.credential.URL.replace('{json}', encodeURIComponent(json));
        var token = svc.configuration.credential.password;
        svc.setRequestMethod('GET');
        svc.setURL(url);
        svc.addHeader('Authorization', 'Bearer ' + token);
        svc.addHeader('Content-Type', 'application/json');
        svc.addHeader('Accept-language', 'es-MX');
    },

    parseResponse: function (svc, client) {
        return client.text
    },

    mockCall: function () {
        return {
            statusCode: 200,
            statusMessage: 'Success'
        }
    }
});

module.exports.kbCategoryArticles = kbCategoryArticles;
