"use strict";

var server = require("server");
var ContentMgr = dw.content.ContentMgr;
var Site = require("dw/system/Site");
var themeServices = require("*/cartridge/scripts/services/theme");
var themeHelper = require("*/cartridge/scripts/helpers/themeHelper");

var customizerConteinerId = Site.getCurrent().getCustomPreferenceValue(
    "themeCustomizerContainer"
);

server.post("ActivateCurrentTheme", function (req, res, next) {
    var clientToken = themeServices.getClientToken();
    var updatedTheme = JSON.parse(req.body);
    var currentAsset = ContentMgr.getContent(customizerConteinerId);
    var currentThemesAssetJSON = JSON.parse(currentAsset.custom.body.markup);
    var siteLib = ContentMgr.getSiteLibrary();
    var siteLibId = siteLib ? siteLib.ID : "";

    if (siteLibId === "Library") {//in case it's private lib
        siteLibId = Site.getCurrent().ID;
    }

    var urlEndpoint =
        "https://" +
        req.host +
        "/s/-/dw/data/v19_8/libraries/" +
        siteLibId +
        "/content/" +
        customizerConteinerId;
    var updatedThemes = JSON.stringify(
        themeHelper.updateThemes(currentThemesAssetJSON, updatedTheme, true)
    );
    var payload = JSON.stringify({
        c_body: {
            default: {
                _type: "markup_text",
                markup: updatedThemes,
                source: updatedThemes
            }
        }
    });
    var themeUpdateService = themeServices.getThemeUpdateService();

    var result = themeUpdateService.call({
        urlEndpoint: urlEndpoint,
        clientToken: clientToken,
        payload: payload
    });

    if (!result || result.errorMessage) {
        res.setStatusCode(500);
        return;
    }

    res.json({ status: "ok" });
    next();
});

server.post("SaveCurrentTheme", function (req, res, next) {
    var clientToken = themeServices.getClientToken();
    var updatedTheme = JSON.parse(req.body);
    var currentAsset = ContentMgr.getContent(customizerConteinerId);
    var currentThemesAssetJSON = JSON.parse(currentAsset.custom.body.markup);
    var siteLib = ContentMgr.getSiteLibrary();
    var siteLibId = siteLib ? siteLib.ID : "";

    if (siteLibId === "Library") {//in case it's private lib
        siteLibId = Site.getCurrent().ID;
    }

    var urlEndpoint =
        "https://" +
        req.host +
        "/s/-/dw/data/v19_8/libraries/" +
        siteLibId +
        "/content/" +
        customizerConteinerId;
    var updatedThemes = JSON.stringify(
        themeHelper.updateThemes(currentThemesAssetJSON, updatedTheme)
    );
    var payload = JSON.stringify({
        c_body: {
            default: {
                _type: "markup_text",
                markup: updatedThemes,
                source: updatedThemes
            }
        }
    });
    var service = themeServices.getThemeUpdateService();

    var result = service.call({
        urlEndpoint: urlEndpoint,
        clientToken: clientToken,
        payload: payload
    });

    if (!result || result.errorMessage) {
        res.setStatusCode(500);
        return;
    }

    res.json({ status: "ok" });
    next();
});

module.exports = server.exports();
