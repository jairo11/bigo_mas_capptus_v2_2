"use strict";

var server = require("server");
var cache = require("*/cartridge/scripts/middleware/cache");
var consentTracking = require("*/cartridge/scripts/middleware/consentTracking");
var pageMetaData = require("*/cartridge/scripts/middleware/pageMetaData");
var ExperienceHelpers = require("*/cartridge/scripts/helpers/experienceHelpers");

server.get("Show", consentTracking.consent, cache.applyDefaultCache, function (req, res, next) {
    var Site = require("dw/system/Site");
    var pageMetaHelper = require("*/cartridge/scripts/helpers/pageMetaHelper");
    ExperienceHelpers.setPageToRender(res, "homepage");
    if (!res.page) {
        pageMetaHelper.setPageMetaTags(req.pageMetaData, Site.current);
        res.render("/home/homePage");
    }
    next();
}, pageMetaData.computedPageMetaData);

server.get("ErrorNotFound", function (req, res, next) {
    ExperienceHelpers.setPageToRender(res, "404");
    if (!res.page) {
        res.setStatusCode(404);
        res.render("error/notFound");
    }
    next();
});

module.exports = server.exports();
