"use strict";

var server = require("server");

var COHelpers = require("*/cartridge/scripts/helpers/checkoutHelpers");
var csrfProtection = require("*/cartridge/scripts/middleware/csrf");
var consentTracking = require("*/cartridge/scripts/middleware/consentTracking");
var cartHelper = require("*/cartridge/scripts/helpers/cartHelpers");

/**
 * Main entry point for Checkout
 */

server.get(
    "Login",
    server.middleware.https,
    consentTracking.consent,
    csrfProtection.generateToken,
    function (req, res, next) {
        var BasketMgr = require("dw/order/BasketMgr");
        var ProductLineItemsModel = require("*/cartridge/models/productLineItems");
        var TotalsModel = require("*/cartridge/models/totals");
        var URLUtils = require("dw/web/URLUtils");
        var reportingUrlsHelper = require("*/cartridge/scripts/reportingUrls");
        var validationHelpers = require("*/cartridge/scripts/helpers/basketValidationHelpers");

        if (!require("helpers").sitePreference("enableTransactionalSite", true)) {
            res.redirect(URLUtils.url("Home-Show"));
            return next();
        }

        var currentBasket = BasketMgr.getCurrentBasket();
        var validatedProducts = validationHelpers.validateProducts(currentBasket);
        var reportingURLs;
        if (!currentBasket || validatedProducts.error) {
            res.redirect(URLUtils.url("Cart-Show"));
            return next();
        }

        if (req.currentCustomer.profile) {
            res.redirect(URLUtils.url("Checkout-Begin"));
        } else {
            var rememberMe = false;
            var userName = "";
            var actionUrl = URLUtils.url("Account-Login", "rurl", 2);
            var totalsModel = new TotalsModel(currentBasket);
            var details = {
                subTotal: totalsModel.subTotal,
                totalQuantity: ProductLineItemsModel.getTotalQuantity(
                    currentBasket.productLineItems
                )
            };

            if (req.currentCustomer.credentials) {
                rememberMe = true;
                userName = req.currentCustomer.credentials.username;
            }

            reportingURLs = reportingUrlsHelper.getCheckoutReportingURLs(
                currentBasket.UUID,
                1,
                "CheckoutMethod"
            );

            res.render("/checkout/checkoutLogin", {
                rememberMe: rememberMe,
                userName: userName,
                actionUrl: actionUrl,
                details: details,
                reportingURLs: reportingURLs,
                oAuthReentryEndpoint: 2
            });
        }

        return next();
    }
);


// Main entry point for Checkout
server.get(
    "Begin",
    server.middleware.https,
    consentTracking.consent,
    csrfProtection.generateToken,
    function (req, res, next) {
        var BasketMgr = require("dw/order/BasketMgr");
        var Transaction = require("dw/system/Transaction");
        var AccountModel = require("*/cartridge/models/account");
        var OrderModel = require("*/cartridge/models/order");
        var URLUtils = require("dw/web/URLUtils");
        var reportingUrlsHelper = require("*/cartridge/scripts/reportingUrls");
        var Locale = require("dw/util/Locale");
        var collections = require("*/cartridge/scripts/util/collections");
        var validationHelpers = require("*/cartridge/scripts/helpers/basketValidationHelpers");

        if (!require("helpers").sitePreference("enableTransactionalSite", true)) {
            res.redirect(URLUtils.url("Home-Show"));
            return next();
        }

        var currentBasket = BasketMgr.getCurrentBasket();
        if (!currentBasket) {
            res.redirect(URLUtils.url("Cart-Show"));
            return next();
        }
        var validatedProducts = validationHelpers.validateProducts(currentBasket);
        if (validatedProducts.error) {
            res.redirect(URLUtils.url("Cart-Show"));
            return next();
        }

        var currentStage = req.querystring.stage ? req.querystring.stage : "shipping";

        var billingAddress = currentBasket.billingAddress;

        var currentCustomer = req.currentCustomer.raw;
        var currentLocale = Locale.getLocale(req.locale.id);
        var preferredAddress;

        // only true if customer is registered
        if (req.currentCustomer.addressBook && req.currentCustomer.addressBook.preferredAddress) {
            var shipments = currentBasket.shipments;
            preferredAddress = req.currentCustomer.addressBook.preferredAddress;

            collections.forEach(shipments, function (shipment) {
                if (!shipment.shippingAddress) {
                    COHelpers.copyCustomerAddressToShipment(preferredAddress, shipment);
                }
            });

            if (!billingAddress) {
                COHelpers.copyCustomerAddressToBilling(preferredAddress);
            }
        }

        // Calculate the basket
        Transaction.wrap(function () {
            COHelpers.ensureNoEmptyShipments(req);
        });

        if (currentBasket.shipments.length <= 1) {
            req.session.privacyCache.set("usingMultiShipping", false);
        }

        if (currentBasket.currencyCode !== req.session.currency.currencyCode) {
            Transaction.wrap(function () {
                currentBasket.updateCurrency();
            });
        }

        COHelpers.recalculateBasket(currentBasket);

        var shippingForm = COHelpers.prepareShippingForm(currentBasket);
        var billingForm = COHelpers.prepareBillingForm(currentBasket);
        var usingMultiShipping = req.session.privacyCache.get("usingMultiShipping");

        if (preferredAddress) {
            shippingForm.copyFrom(preferredAddress);
            billingForm.copyFrom(preferredAddress);
        }

        // Loop through all shipments and make sure all are valid
        var allValid = COHelpers.ensureValidShipments(currentBasket);

        var orderModel = new OrderModel(
            currentBasket,
            {
                customer: currentCustomer,
                usingMultiShipping: usingMultiShipping,
                shippable: allValid,
                countryCode: currentLocale.country,
                containerView: "basket"
            }
        );

        // Get rid of this from top-level ... should be part of OrderModel???
        var currentYear = new Date().getFullYear();
        var creditCardExpirationYears = [];

        for (var j = 0; j < 10; j++) {
            creditCardExpirationYears.push(currentYear + j);
        }

        var accountModel = new AccountModel(req.currentCustomer);

        var reportingURLs;
        reportingURLs = reportingUrlsHelper.getCheckoutReportingURLs(
            currentBasket.UUID,
            2,
            "Shipping"
        );

        res.render("checkout/checkout", {
            order: orderModel,
            customer: accountModel,
            forms: {
                shippingForm: shippingForm,
                billingForm: billingForm
            },
            expirationYears: creditCardExpirationYears,
            currentStage: currentStage,
            reportingURLs: reportingURLs,
            cartAvailableAttributes: cartHelper.getAvailableAttributes()
        });

        return next();
    }
);

server.get(
    "PaymentCards",
    server.middleware.https,
    consentTracking.consent,
    csrfProtection.generateToken,
    function (req, res, next) {
        var PaymentMgr = require("dw/order/PaymentMgr");

        var paymentMethods = PaymentMgr.getActivePaymentMethods();

        var payments = {};
        if (paymentMethods && paymentMethods.length > 0) {
            paymentMethods.toArray().forEach(function (method) {
                method.getActivePaymentCards().toArray().reduce(function (obj, card) {
                    obj[card.name.toLowerCase()] = card.cardType;
                    return obj;
                }, payments);
            });
        }

        res.json({
            payments: payments
        });

        next();
    }
);


module.exports = server.exports();
