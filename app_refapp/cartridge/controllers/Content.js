"use strict";

var server = require("server");

var cache = require("*/cartridge/scripts/middleware/cache");
var consentTracking = require("*/cartridge/scripts/middleware/consentTracking");
var pageMetaData = require("*/cartridge/scripts/middleware/pageMetaData");

server.get("Show", cache.applyDefaultCache, consentTracking.consent, function (req, res, next) {
    var ContentMgr = require("dw/content/ContentMgr");
    var Logger = require("dw/system/Logger");
    var ContentModel = require("*/cartridge/models/content");
    var pageMetaHelper = require("*/cartridge/scripts/helpers/pageMetaHelper");

    var apiContent = ContentMgr.getContent(req.querystring.cid);
    var apiFolder = ContentMgr.getFolder(req.querystring.fid);

    if (!empty(req.querystring.cid) && apiContent) {
        var content = new ContentModel(apiContent, "content/categorizedContentAsset");
        //while parent !==null !root id===catgorisedpages
        pageMetaHelper.setPageMetaData(req.pageMetaData, content);
        pageMetaHelper.setPageMetaTags(req.pageMetaData, content);

        if (content.template) {
            res.render(content.template, { content: content });
        } else {
            Logger.warn("Content asset with ID {0} is offline", req.querystring.cid);
            res.render("/components/content/offlineContent");
        }
    }
    else if (!empty(req.querystring.fid) && apiFolder) {
        pageMetaHelper.setPageMetaData(req.pageMetaData, apiFolder);
        pageMetaHelper.setPageMetaTags(req.pageMetaData, apiFolder);

        var configs = {
            currentFolder: apiFolder,
        };

        if (apiFolder.online) {
            res.render(apiFolder.template || "content/categorizedFolder", configs);
        } else {
            Logger.warn("Content folder with ID {0} is offline", req.querystring.fid);
        }
    } else {
        Logger.warn("Content asset with ID {0} was included but not found", req.querystring.cid);
    }

    next();
}, pageMetaData.computedPageMetaData);

module.exports = server.exports();
