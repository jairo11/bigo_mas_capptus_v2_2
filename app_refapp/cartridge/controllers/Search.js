"use strict";

var server = require("server");

var cache = require("*/cartridge/scripts/middleware/cache");
var consentTracking = require("*/cartridge/scripts/middleware/consentTracking");
var pageMetaData = require("*/cartridge/scripts/middleware/pageMetaData");
var search = require("*/cartridge/scripts/middleware/search");
var PageMgr = require("dw/experience/PageMgr");

server.get("UpdateGrid", search.isEnabled, function (req, res, next) {
    var CatalogMgr = require("dw/catalog/CatalogMgr");
    var ProductSearchModel = require("dw/catalog/ProductSearchModel");
    var searchHelper = require("*/cartridge/scripts/helpers/searchHelpers");
    var ProductSearch = require("*/cartridge/models/search/productSearch");

    var apiProductSearch = new ProductSearchModel();
    apiProductSearch = searchHelper.setupSearch(apiProductSearch, req.querystring);
    var viewData = {
        apiProductSearch: apiProductSearch
    };
    res.setViewData(viewData);

    this.on("route:BeforeComplete", function (requ, resp) { // eslint-disable-line no-shadow
        apiProductSearch.search();

        if (!apiProductSearch.personalizedSort) {
            searchHelper.applyCache(resp);
        }
        var productSearch = new ProductSearch(
            apiProductSearch,
            requ.querystring,
            requ.querystring.srule,
            CatalogMgr.getSortingOptions(),
            CatalogMgr.getSiteCatalog().getRoot()
        );

        resp.render("/search/productGrid", {
            productSearch: productSearch
        });
    });

    next();
});

server.get("Refinebar", search.isEnabled, cache.applyDefaultCache, function (req, res, next) {
    var CatalogMgr = require("dw/catalog/CatalogMgr");
    var ProductSearchModel = require("dw/catalog/ProductSearchModel");
    var ProductSearch = require("*/cartridge/models/search/productSearch");
    var searchHelper = require("*/cartridge/scripts/helpers/searchHelpers");

    var apiProductSearch = new ProductSearchModel();
    apiProductSearch = searchHelper.setupSearch(apiProductSearch, req.querystring);
    apiProductSearch.search();
    var productSearch = new ProductSearch(
        apiProductSearch,
        req.querystring,
        req.querystring.srule,
        CatalogMgr.getSortingOptions(),
        CatalogMgr.getSiteCatalog().getRoot()
    );
    res.render("/search/searchRefineBar", {
        productSearch: productSearch,
        querystring: req.querystring
    });

    next();
}, pageMetaData.computedPageMetaData);

server.get("ShowAjax", search.isEnabled, cache.applyShortPromotionSensitiveCache, consentTracking.consent, function (req, res, next) {
    var searchHelper = require("*/cartridge/scripts/helpers/searchHelpers");
    var result = searchHelper.search(req, res);

    if (result.searchRedirect) {
        res.redirect(result.searchRedirect);
        return next();
    }

    res.render("search/searchResultsNoDecorator", {
        productSearch: result.productSearch,
        category: result.category,
        maxSlots: result.maxSlots,
        reportingURLs: result.reportingURLs,
        refineurl: result.refineurl
    });

    return next();
}, pageMetaData.computedPageMetaData);

server.get("Show", search.isEnabled, cache.applyShortPromotionSensitiveCache, consentTracking.consent, function (req, res, next) {
    var searchHelper = require("*/cartridge/scripts/helpers/searchHelpers");
    var template = "search/searchResults";
    var httpParameterMap = request.httpParameterMap;
    var renderPageDesignerOnThisPage = httpParameterMap.pdshow.booleanValue;

    var result = searchHelper.search(req, res);

    if (result.searchRedirect) {
        res.redirect(result.searchRedirect);
        return next();
    }

    if (result.category && result.category.custom.pageDesignerLandingPageId && renderPageDesignerOnThisPage) {
        var page = PageMgr.getPage(result.category.custom.pageDesignerLandingPageId);

        if (page && page.isVisible()) {
            res.setStatusCode(200);
            res.page(page.ID, {});
            return next();
        }
    }

    if (result.category && result.categoryTemplate) {
        template = result.categoryTemplate;
    }

    res.render(template, {
        productSearch: result.productSearch,
        maxSlots: result.maxSlots,
        reportingURLs: result.reportingURLs,
        refineurl: result.refineurl,
        category: result.category ? result.category : null,
        canonicalUrl: result.canonicalUrl,
        schemaData: result.schemaData
    });

    return next();
}, pageMetaData.computedPageMetaData);

server.get("Content", search.isEnabled, cache.applyDefaultCache, consentTracking.consent, function (req, res, next) {
    var searchHelper = require("*/cartridge/scripts/helpers/searchHelpers");

    var contentSearch = searchHelper.setupContentSearch(req.querystring);
    res.render("/search/contentGrid", {
        contentSearch: contentSearch
    });
    next();
});

server.get("ShowContent", search.isEnabled, cache.applyDefaultCache, consentTracking.consent, function (req, res, next) {
    var searchHelper = require("*/cartridge/scripts/helpers/searchHelpers");

    var contentSearch = searchHelper.setupContentSearch(req.querystring);
    res.render("/search/decoratedContentGrid", {
        contentSearch: contentSearch
    });
    next();
});

module.exports = server.exports();
