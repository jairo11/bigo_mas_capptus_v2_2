"use strict";

var server = require("server");

var cache = require("*/cartridge/scripts/middleware/cache");
var consentTracking = require("*/cartridge/scripts/middleware/consentTracking");
var pageMetaData = require("*/cartridge/scripts/middleware/pageMetaData");
var productListHelper = require("*/cartridge/scripts/features/productListHelpers");
var wishListType = require("dw/customer/ProductList").TYPE_WISH_LIST;
var ExperienceHelpers = require("*/cartridge/scripts/helpers/experienceHelpers");

/**
 * @typedef ProductDetailPageResourceMap
 * @type Object
 * @property {String} global_availability - Localized string for "Availability"
 * @property {String} label_instock - Localized string for "In Stock"
 * @property {String} global_availability - Localized string for "This item is currently not
 *     available"
 * @property {String} info_selectforstock - Localized string for "Select Styles for Availability"
 */

server.get("Show", cache.applyPromotionSensitiveCache, consentTracking.consent, function (req, res, next) {
    var productHelper = require("*/cartridge/scripts/helpers/productHelpers");
    var showProductPageHelperResult = productHelper.showProductPage(req.querystring, req.pageMetaData);
    var productType = showProductPageHelperResult.product.productType;
    if (!showProductPageHelperResult.product.online && productType !== "set" && productType !== "bundle") {
        ExperienceHelpers.setPageToRender(res, "404");
        if (!res.page) {
            res.setStatusCode(404);
            res.render("error/notFound");
        }
    } else {
        var currentWishlist = productListHelper.getList(req.currentCustomer.raw, { type: wishListType });

        res.render(showProductPageHelperResult.template, {
            product: showProductPageHelperResult.product,
            addToCartUrl: showProductPageHelperResult.addToCartUrl,
            resources: showProductPageHelperResult.resources,
            breadcrumbs: showProductPageHelperResult.breadcrumbs,
            canonicalUrl: showProductPageHelperResult.canonicalUrl,
            schemaData: showProductPageHelperResult.schemaData,
            currentWishlist: currentWishlist.items
        });
    }
    next();
}, pageMetaData.computedPageMetaData);

server.get("ShowInCategory", cache.applyPromotionSensitiveCache, function (req, res, next) {
    var productHelper = require("*/cartridge/scripts/helpers/productHelpers");
    var showProductPageHelperResult = productHelper.showProductPage(req.querystring, req.pageMetaData);
    if (!showProductPageHelperResult.product.online) {
        ExperienceHelpers.setPageToRender(res, "404");
        if (!res.page) {
            res.setStatusCode(404);
            res.render("error/notFound");
        }
    } else {
        res.render(showProductPageHelperResult.template, {
            product: showProductPageHelperResult.product,
            addToCartUrl: showProductPageHelperResult.addToCartUrl,
            resources: showProductPageHelperResult.resources,
            breadcrumbs: showProductPageHelperResult.breadcrumbs
        });
    }
    next();
});

server.get("Variation", function (req, res, next) {
    var productHelper = require("*/cartridge/scripts/helpers/productHelpers");
    var priceHelper = require("*/cartridge/scripts/helpers/pricing");
    var ProductFactory = require("*/cartridge/scripts/factories/product");
    var renderTemplateHelper = require("*/cartridge/scripts/renderTemplateHelper");

    var params = req.querystring;
    var product = ProductFactory.get(params);

    product.price.html = priceHelper.renderHtml(priceHelper.getHtmlContext(product.price));

    var attributeContext = { product: { attributes: product.attributes } };
    var attributeTemplate = "product/components/attributesPre";
    product.attributesHtml = renderTemplateHelper.getRenderedHtml(
        attributeContext,
        attributeTemplate
    );

    var promotionsContext = { product: { promotions: product.promotions } };
    var promotionsTemplate = "product/components/promotions";

    product.promotionsHtml = renderTemplateHelper.getRenderedHtml(
        promotionsContext,
        promotionsTemplate
    );

    var optionsContext = { product: { options: product.options } };
    var optionsTemplate = "product/components/options";

    product.optionsHtml = renderTemplateHelper.getRenderedHtml(
        optionsContext,
        optionsTemplate
    );

    res.json({
        product: product,
        resources: productHelper.getResources()
    });

    next();
});

server.get("ShowQuickView", cache.applyPromotionSensitiveCache, function (req, res, next) {
    var URLUtils = require("dw/web/URLUtils");
    var productHelper = require("*/cartridge/scripts/helpers/productHelpers");
    var ProductFactory = require("*/cartridge/scripts/factories/product");
    var renderTemplateHelper = require("*/cartridge/scripts/renderTemplateHelper");
    var Resource = require("dw/web/Resource");

    var params = req.querystring;
    var product = ProductFactory.get(params);
    var addToCartUrl = URLUtils.url("Cart-AddProduct");
    var template = product.productType === "set"
        ? "product/setQuickView.isml"
        : "product/quickView.isml";
    var currentWishlist = productListHelper.getList(req.currentCustomer.raw, { type: wishListType });

    var context = {
        product: product,
        addToCartUrl: addToCartUrl,
        resources: productHelper.getResources(),
        quickViewFullDetailMsg: Resource.msg("link.quickview.viewdetails", "product", null),
        closeButtonText: Resource.msg("link.quickview.close", "product", null),
        enterDialogMessage: Resource.msg("msg.enter.quickview", "product", null),
        currentWishlist: currentWishlist.items,
        template: template
    };

    // if quickview is disabled, redirect the quickview link to PDP page
    var enableQuickView = require("helpers").sitePreference("enableQuickView");

    if (enableQuickView === false) {
        res.redirect(URLUtils.url("Product-Show", "pid", product.id));
    }

    res.setViewData(context);
    // eslint-disable-next-line no-shadow
    this.on("route:BeforeComplete", function (req, res) { //NOSONAR
        var viewData = res.getViewData();
        var renderedTemplate = renderTemplateHelper.getRenderedHtml(viewData, viewData.template);

        res.json({
            renderedTemplate: renderedTemplate,
            productUrl: URLUtils.url("Product-Show", "pid", viewData.product.id).relative().toString()
        });
    });

    next();
});

server.get("SizeChart", function (req, res, next) {
    var ContentMgr = require("dw/content/ContentMgr");

    var apiContent = ContentMgr.getContent(req.querystring.cid);

    if (apiContent) {
        res.json({
            success: true,
            content: apiContent.custom.body.markup
        });
    } else {
        res.json({});
    }
    next();
});

server.get("ShowBonusProducts", function (req, res, next) {
    var Resource = require("dw/web/Resource");
    var ProductFactory = require("*/cartridge/scripts/factories/product");
    var renderTemplateHelper = require("*/cartridge/scripts/renderTemplateHelper");
    var moreUrl = null;
    var pagingModel;
    var products = [];
    var product;
    var duuid = req.querystring.DUUID;
    var collections = require("*/cartridge/scripts/util/collections");
    var BasketMgr = require("dw/order/BasketMgr");
    var currentBasket = BasketMgr.getCurrentOrNewBasket();
    var showMoreButton;
    var selectedBonusProducts;

    if (duuid) {
        var bonusDiscountLineItem = collections.find(currentBasket.getBonusDiscountLineItems(), function (item) {
            return item.UUID === duuid;
        });

        if (bonusDiscountLineItem && bonusDiscountLineItem.bonusProductLineItems.length) {
            selectedBonusProducts = collections.map(bonusDiscountLineItem.bonusProductLineItems, function (bonusProductLineItem) {
                var option = {
                    optionid: "",
                    selectedvalue: ""
                };
                if (!bonusProductLineItem.optionProductLineItems.empty) {
                    option.optionid = bonusProductLineItem.optionProductLineItems[0].optionID;
                    option.optionid = bonusProductLineItem.optionProductLineItems[0].optionValueID;
                }
                return {
                    pid: bonusProductLineItem.productID,
                    name: bonusProductLineItem.productName,
                    submittedQty: (bonusProductLineItem.quantityValue),
                    option: option
                };
            });
        } else {
            selectedBonusProducts = [];
        }

        if (req.querystring.pids) {
            var params = req.querystring.pids.split(",");
            products = params.map(function (param) {
                product = ProductFactory.get({
                    pid: param,
                    pview: "bonus",
                    duuid: duuid });
                return product;
            });
        } else {
            var URLUtils = require("dw/web/URLUtils");
            var PagingModel = require("dw/web/PagingModel");
            var pageStart = parseInt(req.querystring.pagestart, 10);
            var pageSize = parseInt(req.querystring.pagesize, 10);
            showMoreButton = true;

            var ProductSearchModel = require("dw/catalog/ProductSearchModel");
            var apiProductSearch = new ProductSearchModel();
            var productSearchHit;
            apiProductSearch.setPromotionID(bonusDiscountLineItem.promotionID);
            apiProductSearch.setPromotionProductType("bonus");
            apiProductSearch.search();
            pagingModel = new PagingModel(apiProductSearch.getProductSearchHits(), apiProductSearch.count);
            pagingModel.setStart(pageStart);
            pagingModel.setPageSize(pageSize);

            var totalProductCount = pagingModel.count;

            if (pageStart + pageSize > totalProductCount) {
                showMoreButton = false;
            }

            moreUrl = URLUtils.url("Product-ShowBonusProducts", "DUUID", duuid, "pagesize", pageSize, "pagestart", pageStart + pageSize).toString();

            var iter = pagingModel.pageElements;
            while (iter !== null && iter.hasNext()) {
                productSearchHit = iter.next();
                product = ProductFactory.get({ pid: productSearchHit.getProduct().ID, pview: "bonus", duuid: duuid });
                products.push(product);
            }
        }
    }

    var context = {
        products: products,
        selectedBonusProducts: selectedBonusProducts,
        maxPids: req.querystring.maxpids,
        moreUrl: moreUrl,
        showMoreButton: showMoreButton,
        closeButtonText: Resource.msg("link.choice.of.bonus.dialog.close", "product", null),
        enterDialogMessage: Resource.msg("msg.enter.choice.of.bonus.select.products", "product", null),
        template: "product/components/choiceOfBonusProducts/bonusProducts.isml"
    };

    res.setViewData(context);
    // eslint-disable-next-line no-shadow
    this.on("route:BeforeComplete", function (req, res) { //NOSONAR
        var viewData = res.getViewData();

        res.json({
            renderedTemplate: renderTemplateHelper.getRenderedHtml(viewData, viewData.template)
        });
    });

    next();
});

server.get("Recommendation", server.middleware.include, function (req, res, next) {
    var siteHelper = require("helpers");
    var ProductFactory = require("*/cartridge/scripts/factories/product");

    var viewData = {};
    var displayCatalogRecommendations = siteHelper.sitePreference("displayCatalogRecommendations");
    if (displayCatalogRecommendations && req.querystring.pid) {
        var product = ProductFactory.get({
            pid: req.querystring.pid
        });

        if (!empty(product) && !empty(product.recommendations)) {
            var content = product.recommendations.reduce(function (recommendations, recommendation) {
                if (recommendation.recommendationType == 1) {
                    recommendations.push(recommendation);
                }
                return recommendations;
            }, []);
            if (!empty(content)) {
                viewData.slotcontent = {content: content};
            }
        }
    }

    res.render("product/components/recommendation", viewData);
    return next();
});

server.get("Component", server.middleware.include, function (req, res, next) {
    var ProductFactory = require("*/cartridge/scripts/factories/product");
    var URLUtils = require("dw/web/URLUtils");
    var template = req.querystring.template;

    if (template) {
        var viewData = {};
        var product = ProductFactory.get({
            pid: req.querystring.pid
        });

        if (product) {
            viewData.product = product;
            var currentWishlist = productListHelper.getList(req.currentCustomer.raw, { type: wishListType });
            viewData.currentWishlist = currentWishlist.items;

            if (template === "addToCartProduct") {
                viewData.addToCartUrl = URLUtils.url("Cart-AddProduct");
                viewData.isQuickView = req.querystring.isQuickView === "true";
            }

            res.render("product/components/" + template, viewData);
        }
    }

    return next();
});

module.exports = server.exports();
