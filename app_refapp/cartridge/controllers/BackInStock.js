"use strict";

var server = require("server");
var csrfProtection = require("*/cartridge/scripts/middleware/csrf");
var backInStock = require("*/cartridge/scripts/middleware/backInStock");

server.get("Show", backInStock.isEnabled, server.middleware.https, csrfProtection.generateToken, function (req, res, next) {
    var backInStockHelper = require("*/cartridge/scripts/features/backInStockHelpers");
    var Resource = require("dw/web/Resource");
    var URLUtils = require("dw/web/URLUtils");
    var notificationModel = backInStockHelper.getNotificationModel(req);

    notificationModel.breadcrumbs = [
        {
            htmlValue: Resource.msg("global.home", "common", null),
            url: URLUtils.home().toString()
        },
        {
            htmlValue: Resource.msg("page.title.myaccount", "account", null),
            url: URLUtils.url("Account-Show").toString()
        }
    ];

    res.render("backInStock/backInStock", notificationModel);

    next();
});


server.post("Add", backInStock.isEnabledAjax, server.middleware.https, csrfProtection.generateToken, function (req, res, next) {
    var backInStockHelper = require("*/cartridge/scripts/features/backInStockHelpers");
    var Resource = require("dw/web/Resource");

    var pid = req.form.pid;
    var email = req.currentCustomer.raw.profile.email;

    var result = {
        success: true,
        msg: Resource.msgf("msg.added.backinstock", "product", null, pid)
    };

    try {
        backInStockHelper.addBackInStockNotification({pid: pid, email: email});
    } catch (e) {
        result.success = false;
        result.msg = Resource.msgf("msg.added.error.backinstock", "product", null, pid);
    }

    res.json(result);

    next();
});

server.post("AddForGuest", backInStock.isEnabledAjax, function (req, res, next) {
    var backInStockHelper = require("*/cartridge/scripts/features/backInStockHelpers");
    var UUIDUtils = require("dw/util/UUIDUtils");
    var Resource = require("dw/web/Resource");

    var pid = req.form.pid;
    var email = req.form.email;
    var uuid = UUIDUtils.createUUID();

    var result = {
        success: true,
        msg: Resource.msg("msg.added.guest.backinstock", "product", null)
    };

    try {
        backInStockHelper.addBackInStockValidation(email, pid, uuid);
    } catch (error) {
        result.success = false;
        result.msg = Resource.msgf("msg.added.error.backinstock", "product", null, pid);
    }

    res.json(result);

    next();

});

server.get("Validation", backInStock.isEnabled, function (req, res, next) {
    var backInStockHelper = require("*/cartridge/scripts/features/backInStockHelpers");
    var CustomObjectMgr = require("dw/object/CustomObjectMgr");
    var Transaction = require("dw/system/Transaction");
    var URLUtils = require("dw/web/URLUtils");
    var Resource = require("dw/web/Resource");

    var uuid = req.querystring.t;
    var result;
    var pid;
    try {
        Transaction.wrap(function () {
            var validation = CustomObjectMgr.getCustomObject("BackInStockActivations", uuid);
            if (validation) {
                var notificationToAdd = JSON.parse(validation.custom.toValidate);
                pid = notificationToAdd.pid;
                backInStockHelper.addBackInStockNotification(notificationToAdd);

                result = {
                    success: true,
                    msg: Resource.msgf("msg.added.backinstock", "product", null, notificationToAdd.pid)
                };
            }

            CustomObjectMgr.remove(validation);
        });
    } catch (error) {
        result.success = false;
        result.msg = Resource.msgf("msg.added.error.backinstock", "product", null, pid);
    }

    res.redirect(URLUtils.url("Home-Show"));

    next();

});

server.get("MoreList", backInStock.isEnabledAjax, function (req, res, next) {
    var backInStockHelper = require("*/cartridge/scripts/features/backInStockHelpers");
    var notificationModel = backInStockHelper.getNotificationModel(req);

    res.render("/backInStock/components/list", notificationModel);

    next();
});


server.get("Remove", backInStock.isEnabledAjax, server.middleware.https, csrfProtection.generateToken, function (req, res, next) {
    var backInStockHelper = require("*/cartridge/scripts/features/backInStockHelpers");
    var Resource = require("dw/web/Resource");
    var pid = req.querystring.pid;
    var fromCard = req.querystring.card;
    var email = req.currentCustomer.profile.email;
    var isEmpty = false;
    var notifications;

    var result = {
        success: true,
        msg: Resource.msg("backInStock.removefromNotification.success.msg", "account", null)
    };

    try {
        notifications = backInStockHelper.backInStockUserNotifications(req);
        isEmpty = notifications.length == 0;
        backInStockHelper.removesUserNotification(email, pid);
    } catch (error) {
        result.success = false;
        result.msg = Resource.msg("backInStock.removefromNotification.failure.msg", "account", null);
    }

    result.isEmpty = isEmpty;

    if (fromCard) {
        res.render("account/backInStock/listNotifications", {
            account: {
                backInStockNotifications: backInStockHelper.backInStockUserNotifications(req)
            }
        });
    } else {
        res.json({result: result});
    }

    next();
});


server.get("AjaxFail", function (req, res, next) {
    var URLUtils = require("dw/web/URLUtils");
    var Resource = require("dw/web/Resource");

    res.setStatusCode(500);
    res.json({ backInStockEnabled: false, redirectUrl: URLUtils.url("Home-Show").toString(), message: Resource.msg("backInStock.notifcations.disabled", "account", null)});
    next();
});


module.exports = server.exports();
