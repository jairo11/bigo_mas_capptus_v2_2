"use strict";

var server = require("server");

var csrfProtection = require("*/cartridge/scripts/middleware/csrf");


server.get("Get", server.middleware.https, function (req, res, next) {
    var BasketMgr = require("dw/order/BasketMgr");
    var AccountModel = require("*/cartridge/models/account");
    var OrderModel = require("*/cartridge/models/order");
    var Locale = require("dw/util/Locale");
    var Resource = require("dw/web/Resource");
    var URLUtils = require("dw/web/URLUtils");
    var COHelpers = require("*/cartridge/scripts/helpers/checkoutHelpers");

    if (!require("helpers").sitePreference("enableTransactionalSite", true)) {
        res.json({
            redirectUrl: URLUtils.url("Home-Show").toString(),
            error: true
        });

        return next();
    }

    var currentBasket = BasketMgr.getCurrentBasket();

    if (!currentBasket) {
        res.json({
            redirectUrl: URLUtils.url("Cart-Show").toString(),
            error: true
        });

        return next();
    }

    var usingMultiShipping = req.session.privacyCache.get("usingMultiShipping");
    if (usingMultiShipping === true && currentBasket.shipments.length < 2) {
        req.session.privacyCache.set("usingMultiShipping", false);
        usingMultiShipping = false;
    }

    var currentLocale = Locale.getLocale(req.locale.id);
    var allValid = COHelpers.ensureValidShipments(currentBasket);
    var basketModel = new OrderModel(
        currentBasket,
        { usingMultiShipping: usingMultiShipping, countryCode: currentLocale.country, containerView: "basket" }
    );

    res.json({
        order: basketModel,
        customer: new AccountModel(req.currentCustomer),
        error: !allValid,
        message: allValid ? "" : Resource.msg("error.message.shipping.addresses", "checkout", null)
    });

    return next();
});

/**
 *  Handle Ajax payment (and billing) form submit
 */
server.post(
    "SubmitPayment",
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var PaymentManager = require("dw/order/PaymentMgr");
        var HookManager = require("dw/system/HookMgr");
        var Resource = require("dw/web/Resource");
        var accountHelpers = require("*/cartridge/scripts/helpers/accountHelpers");
        var COHelpers = require("*/cartridge/scripts/helpers/checkoutHelpers");
        var URLUtils = require("dw/web/URLUtils");
        var termsAndConditionsError = {};
        var Site = require("dw/system/Site");
        var termsPageId = Site.getCurrent().getCustomPreferenceValue("termsAndConditionsPDPageId");
        var privacyPageId = Site.getCurrent().getCustomPreferenceValue("privacyPolicyPDPageId");

        if (!require("helpers").sitePreference("enableTransactionalSite", true)) {
            res.json({
                redirectUrl: URLUtils.url("Home-Show").toString(),
                error: true
            });

            return next();
        }

        var viewData = {};
        var paymentForm = server.forms.getForm("billing");

        if (termsPageId && privacyPageId) {
            if (!paymentForm.termsAndConditions.checked) {
                paymentForm.valid = false;
                paymentForm.termsAndConditions.value = false;
                paymentForm.termsAndConditions.error = Resource.msg("error.card.info.missing.termsAndConditions", "forms", null);
                termsAndConditionsError[paymentForm.termsAndConditions.htmlName] = paymentForm.termsAndConditions.error;
            }
        } else {
            delete paymentForm.termsAndConditions;
        }

        // verify billing form data
        var billingFormErrors = COHelpers.validateBillingForm(paymentForm.addressFields);
        var contactInfoFormErrors = COHelpers.validateFields(paymentForm.contactInfoFields);

        var formFieldErrors = [];
        if (Object.keys(billingFormErrors).length || Object.keys(termsAndConditionsError).length) {
            formFieldErrors.push(billingFormErrors, termsAndConditionsError);
        } else {
            viewData.address = {
                firstName: { value: paymentForm.addressFields.firstName.value },
                lastName: { value: paymentForm.addressFields.lastName.value },
                address1: { value: paymentForm.addressFields.address1.value },
                address2: { value: paymentForm.addressFields.address2.value },
                city: { value: paymentForm.addressFields.city.value },
                postalCode: { value: paymentForm.addressFields.postalCode.value },
                countryCode: { value: paymentForm.addressFields.country.value }
            };

            if (Object.prototype.hasOwnProperty.call(paymentForm.addressFields, "states")) {
                viewData.address.stateCode = { value: paymentForm.addressFields.states.stateCode.value };
            }
        }

        var nonActivatedGuestOrdersValidation = accountHelpers.allowNonActivatedGuestOrders(paymentForm.contactInfoFields.email.value);
        if (nonActivatedGuestOrdersValidation && !nonActivatedGuestOrdersValidation.allow) {
            formFieldErrors.push({nonActivatedGuestOrders: nonActivatedGuestOrdersValidation});
        }

        if (Object.keys(contactInfoFormErrors).length) {
            formFieldErrors.push(contactInfoFormErrors);
        } else {
            viewData.email = {
                value: paymentForm.contactInfoFields.email.value
            };

            viewData.phone = { value: paymentForm.contactInfoFields.phone.value };
        }

        var paymentMethodIdValue = paymentForm.paymentMethod.value;
        if (!PaymentManager.getPaymentMethod(paymentMethodIdValue).paymentProcessor) {
            throw new Error(Resource.msg(
                "error.payment.processor.missing",
                "checkout",
                null
            ));
        }

        var paymentProcessor = PaymentManager.getPaymentMethod(paymentMethodIdValue).getPaymentProcessor();

        var paymentFormResult;
        if (HookManager.hasHook("app.payment.form.processor." + paymentProcessor.ID.toLowerCase())) {
            paymentFormResult = HookManager.callHook("app.payment.form.processor." + paymentProcessor.ID.toLowerCase(),
                "processForm",
                req,
                paymentForm,
                viewData
            );
        } else {
            paymentFormResult = HookManager.callHook("app.payment.form.processor.default_form_processor", "processForm");
        }

        if (paymentFormResult.error && paymentFormResult.fieldErrors) {
            formFieldErrors.push(paymentFormResult.fieldErrors);
        }

        if (formFieldErrors.length || paymentFormResult.serverErrors) {
            // respond with form data and errors
            res.json({
                form: paymentForm,
                fieldErrors: formFieldErrors,
                serverErrors: paymentFormResult.serverErrors ? paymentFormResult.serverErrors : [],
                error: true
            });
            return next();
        }

        res.setViewData(paymentFormResult.viewData);

        // eslint-disable no-shadow
        this.on("route:BeforeComplete", function (req, res) { //NOSONAR
        // eslint-enable no-shadow
            var BasketMgr = require("dw/order/BasketMgr");
            var HookMgr = require("dw/system/HookMgr");
            var PaymentMgr = require("dw/order/PaymentMgr");
            var Transaction = require("dw/system/Transaction");
            var AccountModel = require("*/cartridge/models/account");
            var OrderModel = require("*/cartridge/models/order");
            var Locale = require("dw/util/Locale");
            var basketCalculationHelpers = require("*/cartridge/scripts/helpers/basketCalculationHelpers");
            var hooksHelper = require("*/cartridge/scripts/helpers/hooks");
            var validationHelpers = require("*/cartridge/scripts/helpers/basketValidationHelpers");

            var currentBasket = BasketMgr.getCurrentBasket();
            var validatedProducts = validationHelpers.validateProducts(currentBasket);

            var billingData = res.getViewData();

            if (!currentBasket || validatedProducts.error) {
                delete billingData.paymentInformation;

                res.json({
                    error: true,
                    cartError: true,
                    fieldErrors: [],
                    serverErrors: [],
                    redirectUrl: URLUtils.url("Cart-Show").toString()
                });
                return;
            }

            var billingAddress = currentBasket.billingAddress;
            var billingForm = server.forms.getForm("billing");
            var paymentMethodID = billingData.paymentMethod.value;
            var result;

            billingForm.creditCardFields.cardNumber.htmlValue = "";
            billingForm.creditCardFields.securityCode.htmlValue = "";

            Transaction.wrap(function () {
                if (!billingAddress) {
                    billingAddress = currentBasket.createBillingAddress();
                }

                billingAddress.setFirstName(billingData.address.firstName.value);
                billingAddress.setLastName(billingData.address.lastName.value);
                billingAddress.setAddress1(billingData.address.address1.value);
                billingAddress.setAddress2(billingData.address.address2.value);
                billingAddress.setCity(billingData.address.city.value);
                billingAddress.setPostalCode(billingData.address.postalCode.value);
                if (Object.prototype.hasOwnProperty.call(billingData.address, "stateCode")) {
                    billingAddress.setStateCode(billingData.address.stateCode.value);
                }
                billingAddress.setCountryCode(billingData.address.countryCode.value);

                if (billingData.storedPaymentUUID) {
                    billingAddress.setPhone(req.currentCustomer.profile.phone);
                    currentBasket.setCustomerEmail(req.currentCustomer.profile.email);
                } else {
                    billingAddress.setPhone(billingData.phone.value);
                    currentBasket.setCustomerEmail(billingData.email.value);
                }
            });

            // if there is no selected payment option and balance is greater than zero
            if (!paymentMethodID && currentBasket.totalGrossPrice.value > 0) {
                var noPaymentMethod = {};

                noPaymentMethod[billingData.paymentMethod.htmlName] =
                    Resource.msg("error.no.selected.payment.method", "payment", null);

                delete billingData.paymentInformation;

                res.json({
                    form: billingForm,
                    fieldErrors: [noPaymentMethod],
                    serverErrors: [],
                    error: true
                });
                return;
            }

            // check to make sure there is a payment processor
            if (!PaymentMgr.getPaymentMethod(paymentMethodID).paymentProcessor) {
                throw new Error(Resource.msg(
                    "error.payment.processor.missing",
                    "checkout",
                    null
                ));
            }

            var processor = PaymentMgr.getPaymentMethod(paymentMethodID).getPaymentProcessor();

            if (HookMgr.hasHook("app.payment.processor." + processor.ID.toLowerCase())) {
                result = HookMgr.callHook("app.payment.processor." + processor.ID.toLowerCase(),
                    "Handle",
                    currentBasket,
                    billingData.paymentInformation
                );
            } else {
                result = HookMgr.callHook("app.payment.processor.default", "Handle");
            }

            // need to invalidate credit card fields
            if (result.error) {
                delete billingData.paymentInformation;

                res.json({
                    form: billingForm,
                    fieldErrors: result.fieldErrors,
                    serverErrors: result.serverErrors,
                    error: true
                });
                return;
            }

            if (HookMgr.hasHook("app.payment.form.processor." + processor.ID.toLowerCase())) {
                HookMgr.callHook("app.payment.form.processor." + processor.ID.toLowerCase(),
                    "savePaymentInformation",
                    req,
                    currentBasket,
                    billingData
                );
            } else {
                HookMgr.callHook("app.payment.form.processor.default", "savePaymentInformation");
            }

            // Calculate the basket
            Transaction.wrap(function () {
                basketCalculationHelpers.calculateTotals(currentBasket);
            });

            // Re-calculate the payments.
            var calculatedPaymentTransaction = COHelpers.calculatePaymentTransaction(
                currentBasket
            );

            if (calculatedPaymentTransaction.error) {
                res.json({
                    form: paymentForm,
                    fieldErrors: [],
                    serverErrors: [Resource.msg("error.technical", "checkout", null)],
                    error: true
                });
                return;
            }

            var usingMultiShipping = req.session.privacyCache.get("usingMultiShipping");
            if (usingMultiShipping === true && currentBasket.shipments.length < 2) {
                req.session.privacyCache.set("usingMultiShipping", false);
                usingMultiShipping = false;
            }

            hooksHelper("app.customer.subscription", "subscribeTo", [paymentForm.subscribe.checked, paymentForm.contactInfoFields.email.htmlValue], function () {});

            var currentLocale = Locale.getLocale(req.locale.id);

            var basketModel = new OrderModel(
                currentBasket,
                { usingMultiShipping: usingMultiShipping, countryCode: currentLocale.country, containerView: "basket" }
            );

            var accountModel = new AccountModel(req.currentCustomer);
            var renderedStoredPaymentInstrument = COHelpers.getRenderedPaymentInstruments(
                req,
                accountModel
            );

            delete billingData.paymentInformation;

            var resObject = {
                renderedPaymentInstruments: renderedStoredPaymentInstrument,
                customer: accountModel,
                order: basketModel,
                form: billingForm,
                error: false
            };

            res.json(resObject);
        });

        return next();
    }
);



server.use("PlaceOrder", server.middleware.https, function (req, res, next) {
    var siteHelper = require("helpers");
    var CsEnabled = siteHelper.sitePreference("CsEnabled");

    var BasketMgr = require("dw/order/BasketMgr");
    var OrderMgr = require("dw/order/OrderMgr");
    var Resource = require("dw/web/Resource");
    var Transaction = require("dw/system/Transaction");
    var URLUtils = require("dw/web/URLUtils");
    var basketCalculationHelpers = require("*/cartridge/scripts/helpers/basketCalculationHelpers");
    var currentBasket = BasketMgr.getCurrentBasket();
    var hooksHelper = require("*/cartridge/scripts/helpers/hooks");
    var COHelpers = require("*/cartridge/scripts/helpers/checkoutHelpers");
    var validationHelpers = require("*/cartridge/scripts/helpers/basketValidationHelpers");
    var validatedProducts = validationHelpers.validateProducts(currentBasket);
    var WishlistHelpers = require("*/cartridge/scripts/features/wishlistHelpers");
    var addressHelpers = require("*/cartridge/scripts/helpers/addressHelpers");

    if (!require("helpers").sitePreference("enableTransactionalSite", true)) {
        res.json({
            redirectUrl: URLUtils.url("Home-Show").toString(),
            error: true
        });

        return next();
    }

    if (!currentBasket || validatedProducts.error) {
        res.json({
            error: true,
            cartError: true,
            fieldErrors: [],
            serverErrors: [],
            redirectUrl: URLUtils.url("Cart-Show").toString()
        });
        return next();
    }

    if (req.session.privacyCache.get("fraudDetectionStatus")) {
        res.json({
            error: true,
            cartError: true,
            redirectUrl: URLUtils.url("Error-ErrorCode", "err", "01").toString(),
            errorMessage: Resource.msg("error.technical", "checkout", null)
        });

        return next();
    }
    var validationOrderStatus = hooksHelper(
        "app.validate.order",
        "validateOrder",
        currentBasket,
        require("*/cartridge/scripts/hooks/validateOrder").validateOrder
    );
    if (validationOrderStatus.error) {
        res.json({
            error: true,
            errorMessage: validationOrderStatus.message
        });
        return next();
    }

    // Check to make sure there is a shipping address
    if (currentBasket.defaultShipment.shippingAddress === null) {
        res.json({
            error: true,
            errorStage: {
                stage: "shipping",
                step: "address"
            },
            errorMessage: Resource.msg("error.no.shipping.address", "checkout", null)
        });
        return next();
    }

    // Check to make sure billing address exists
    if (!currentBasket.billingAddress) {
        res.json({
            error: true,
            errorStage: {
                stage: "payment",
                step: "billingAddress"
            },
            errorMessage: Resource.msg("error.no.billing.address", "checkout", null)
        });
        return next();
    }

    // Calculate the basket
    Transaction.wrap(function () {
        basketCalculationHelpers.calculateTotals(currentBasket);
    });

    // Re-validates existing payment instruments
    var validPayment = COHelpers.validatePayment(req, currentBasket);
    if (validPayment.error) {
        res.json({
            error: true,
            errorStage: {
                stage: "payment",
                step: "paymentInstrument"
            },
            errorMessage: Resource.msg("error.payment.not.valid", "checkout", null)
        });
        return next();
    }

    // Re-calculate the payments.
    var calculatedPaymentTransactionTotal = COHelpers.calculatePaymentTransaction(currentBasket);
    if (calculatedPaymentTransactionTotal.error) {
        res.json({
            error: true,
            errorMessage: Resource.msg("error.technical", "checkout", null)
        });
        return next();
    }

    // Creates a new order.
    var order = COHelpers.createOrder(currentBasket);
    if (!order) {
        res.json({
            error: true,
            errorMessage: Resource.msg("error.technical", "checkout", null)
        });
        return next();
    }

    // Handles payment authorization
    var handlePaymentResult = COHelpers.handlePayments(order, order.orderNo);
    if (CsEnabled) {
        var CsSAType = siteHelper.sitePreferenceAs("CsSAType", "string");
        if (handlePaymentResult.error) {
            if (order.paymentInstrument.paymentMethod != null
                && (order.paymentInstrument.paymentMethod == Resource.msg("paymentmethodname.creditcard", "cybersource", null)
                    && (CsSAType == Resource.msg("cssatype.SA_REDIRECT", "cybersource", null)
                        || CsSAType == Resource.msg("cssatype.SA_SILENTPOST", "cybersource", null)))
                || order.paymentInstrument.paymentMethod == Resource.msg("paymentmethodname.alipay", "cybersource", null)
                || order.paymentInstrument.paymentMethod == Resource.msg("paymentmethodname.eps", "cybersource", null)
                || order.paymentInstrument.paymentMethod == Resource.msg("paymentmethodname.sof", "cybersource", null)
                || order.paymentInstrument.paymentMethod == Resource.msg("paymentmethodname.idl", "cybersource", null)
                || order.paymentInstrument.paymentMethod == Resource.msg("paymentmethodname.mch", "cybersource", null)
                || order.paymentInstrument.paymentMethod == Resource.msg("paymentmethodname.gpy", "cybersource", null)
            ) {
                res.redirect(URLUtils.https("Checkout-Begin", "stage", "placeOrder", "PlaceOrderError", Resource.msg("error.technical", "checkout", null)));
            } else {
                res.json({
                    error: true,
                    errorMessage: Resource.msg("error.technical", "checkout", null)
                });
            }
            return next();
        } else if (handlePaymentResult.returnToPage) {
            res.render("secureacceptance/secureAcceptanceIframeSummmary", {
                Order: handlePaymentResult.order
            });
            return next();
        } else if (handlePaymentResult.intermediate) {
            res.render(handlePaymentResult.renderViewPath, {
                alipayReturnUrl: handlePaymentResult.alipayReturnUrl
            });
            return next();
        } else if (handlePaymentResult.intermediateSA) {
            res.render(handlePaymentResult.renderViewPath, {
                Data: handlePaymentResult.data, FormAction: handlePaymentResult.formAction
            });
            return next();
        } else if (handlePaymentResult.intermediateSilentPost) {
            res.render(handlePaymentResult.renderViewPath, {
                requestData: handlePaymentResult.data, formAction: handlePaymentResult.formAction, cardObject: handlePaymentResult.cardObject
            });
            return next();
        } else if (handlePaymentResult.redirection) {
            res.redirect(handlePaymentResult.redirectionURL);
            return next();
        } else if (handlePaymentResult.declined) {
            session.custom.SkipTaxCalculation = false;
            Transaction.wrap(function () { OrderMgr.failOrder(order, true); });

            if (order.paymentInstrument.paymentMethod != null
                && (order.paymentInstrument.paymentMethod == Resource.msg("paymentmethodname.creditcard", "cybersource", null)
                    && (CsSAType == Resource.msg("cssatype.SA_REDIRECT", "cybersource", null)
                        || CsSAType == Resource.msg("cssatype.SA_SILENTPOST", "cybersource", null)))
                || order.paymentInstrument.paymentMethod == Resource.msg("paymentmethodname.alipay", "cybersource", null)
                || order.paymentInstrument.paymentMethod == Resource.msg("paymentmethodname.eps", "cybersource", null)
                || order.paymentInstrument.paymentMethod == Resource.msg("paymentmethodname.sof", "cybersource", null)
                || order.paymentInstrument.paymentMethod == Resource.msg("paymentmethodname.idl", "cybersource", null)
                || order.paymentInstrument.paymentMethod == Resource.msg("paymentmethodname.mch", "cybersource", null)
                || order.paymentInstrument.paymentMethod == Resource.msg("paymentmethodname.gpy", "cybersource", null)
            ) {
                res.redirect(URLUtils.https("Checkout-Begin", "stage", "placeOrder", "placeOrderError", Resource.msg("sa.billing.payment.error.declined", "cybersource", null)));
            } else {
                res.json({
                    error: true,
                    errorMessage: Resource.msg("sa.billing.payment.error.declined", "cybersource", null)
                });
            }
            return next();
        } else if (handlePaymentResult.missingPaymentInfo) {
            session.custom.SkipTaxCalculation = false;
            Transaction.wrap(function () { OrderMgr.failOrder(order, true); });

            if (order.paymentInstrument.paymentMethod != null
                && (order.paymentInstrument.paymentMethod == Resource.msg("paymentmethodname.creditcard", "cybersource", null)
                    && (CsSAType == Resource.msg("cssatype.SA_REDIRECT", "cybersource", null)
                        || CsSAType == Resource.msg("cssatype.SA_SILENTPOST", "cybersource", null)))
                || order.paymentInstrument.paymentMethod == Resource.msg("paymentmethodname.alipay", "cybersource", null)
                || order.paymentInstrument.paymentMethod == Resource.msg("paymentmethodname.eps", "cybersource", null)
                || order.paymentInstrument.paymentMethod == Resource.msg("paymentmethodname.sof", "cybersource", null)
                || order.paymentInstrument.paymentMethod == Resource.msg("paymentmethodname.idl", "cybersource", null)
                || order.paymentInstrument.paymentMethod == Resource.msg("paymentmethodname.mch", "cybersource", null)
                || order.paymentInstrument.paymentMethod == Resource.msg("paymentmethodname.gpy", "cybersource", null)
            ) {
                res.redirect(URLUtils.https("Checkout-Begin", "stage", "placeOrder", "PlaceOrderError", Resource.msg("sa.billing.payment.error.declined", "cybersource", null)));
            } else {
                res.json({
                    error: true,
                    errorMessage: Resource.msg("error.technical", "checkout", null)
                });
            }
            return next();
        } else if (handlePaymentResult.rejected) {
            Transaction.wrap(function () { OrderMgr.failOrder(order, true); });
            currentBasket = BasketMgr.getCurrentBasket();
            Transaction.wrap(function () {
                COHelpers.handlePayPal(currentBasket);
            });
            res.json({
                error: true,
                cartError: true,
                redirectUrl: URLUtils.https("Checkout-Begin", "stage", "payment", "payerAuthError", Resource.msg("payerauthentication.carderror", "cybersource", null)).toString()
            });
            return next();
        } else if (handlePaymentResult.process3DRedirection) {
            res.json({
                error: false,
                continueUrl: URLUtils.url("CheckoutServices-PayerAuthentication").toString()
            });
            return next();
        }

    } else {
        if (handlePaymentResult.error) {
            res.json({
                error: true,
                errorMessage: Resource.msg("error.technical", "checkout", null)
            });
            return next();
        }
    }

    var fraudDetectionStatus = hooksHelper("app.fraud.detection", "fraudDetection", currentBasket, require("*/cartridge/scripts/hooks/fraudDetection").fraudDetection);
    if (fraudDetectionStatus.status === "fail") {
        Transaction.wrap(function () { OrderMgr.failOrder(order, true); });

        // fraud detection failed
        req.session.privacyCache.set("fraudDetectionStatus", true);

        res.json({
            error: true,
            cartError: true,
            redirectUrl: URLUtils.url("Error-ErrorCode", "err", fraudDetectionStatus.errorCode).toString(),
            errorMessage: Resource.msg("error.technical", "checkout", null)
        });

        return next();
    }

    // Places the order
    var placeOrderResult = COHelpers.placeOrder(order, fraudDetectionStatus);
    if (placeOrderResult.error) {
        res.json({
            error: true,
            errorMessage: Resource.msg("error.technical", "checkout", null)
        });
        return next();
    }

    if (req.currentCustomer.addressBook) {
        // save all used shipping addresses to address book of the logged in customer
        var allAddresses = addressHelpers.gatherShippingAddresses(order);
        allAddresses.forEach(function (address) {
            if (!addressHelpers.checkIfAddressStored(address, req.currentCustomer.addressBook.addresses)) {
                addressHelpers.saveAddress(address, req.currentCustomer, addressHelpers.generateAddressName(address));
            }
        });
    }

    WishlistHelpers.createPurchases(order, req);

    COHelpers.sendConfirmationEmail(order, req.locale.id);

    //  Set order confirmation status to not confirmed for REVIEW orders.
    if (session.custom.CybersourceFraudDecision == "REVIEW") {
        var Order = require("dw/order/Order");
        Transaction.wrap(function () {
            order.setConfirmationStatus(Order.CONFIRMATION_STATUS_NOTCONFIRMED);
        });
    }
    //  Reset decision session variable.
    session.custom.CybersourceFraudDecision = "";
    session.custom.SkipTaxCalculation = false;
    session.custom.cartStateString = null;

    // Reset usingMultiShip after successful Order placement
    req.session.privacyCache.set("usingMultiShipping", false);
    var options = { "selectedPayment": "others" };

    // TODO: Exposing a direct route to an Order, without at least encoding the orderID
    //  is a serious PII violation.  It enables looking up every customers orders, one at a
    //  time.

    res.json({
        error: false,
        orderID: order.orderNo,
        orderToken: order.orderToken,
        continueUrl: URLUtils.url("Order-Confirm").toString(),
        options: options
    });

    return next();
});

module.exports = server.exports();
