"use strict";

/**
 * @description Easy emails testing
 */

var server = require("server");
var System = require("dw/system/System");
var Locale = require("dw/util/Locale");
var OrderMgr = require("dw/order/OrderMgr");
var Resource = require("dw/web/Resource");
var URLUtils = require("dw/web/URLUtils");
var siteHelper = require("helpers");
var StringUtils = require("dw/util/StringUtils");
var Site = require("dw/system/Site");

var OrderModel = require("*/cartridge/models/order");
var emailHelpers = require("*/cartridge/scripts/helpers/emailHelpers");
var emailTemplate = require("*/cartridge/scripts/util/emailTemplate");

var isProduction = System.instanceType === System.PRODUCTION_SYSTEM;

function devOnlyMiddleware(req, res, next) {
    if (isProduction) {
        next(new Error("Production controller use is forbidden"));
    } else {
        next();
    }
}

var emailsDict = {
    orderConfirmation: function (req) {
        var status;

        if (!req.querystring.orderId) {
            status = "Missing Order ID";
        }
        var order = OrderMgr.getOrder(req.querystring.orderId);
        var currentLocale = Locale.getLocale(req.locale.id);
        var data = new OrderModel(order, { countryCode: currentLocale.country, containerView: "order" });
        data.orderDetailsTableTemplate = emailTemplate.templatesType.orderDetailsTable;

        return {
            emailConfig: {
                to: req.querystring.email,
                subject: emailHelpers.emailSubject("orderConfirmation", Resource.msg("subject.order.confirmation.email", "order", null)),
                from: siteHelper.sitePreference("customerServiceEmail", "no-reply@salesforce.com"),
                type: emailHelpers.emailTypes.orderConfirmation
            },
            data: data,
            renderingTemplate: "checkout/confirmation/confirmationEmail",
            status: status
        };
    },
    contactUs: function (req) {
        return {
            emailConfig: {
                to: req.querystring.email,
                from: siteHelper.sitePreference("customerServiceEmail", "no-reply@salesforce.com"),
                subject: emailHelpers.emailSubject("contactUs", Resource.msg("email.subject", "contactUs", null)),
                type: emailHelpers.emailTypes.contactUs
            },
            data: {
                firstName: "MockFirstName",
                lastName: "MockLastName",
                email: req.querystring.email,
                topic: "MockTopic",
                comment: "MockComment"
            },
            renderingTemplate: "mail/feedback"
        };
    },
    backInStockSubscription: function (req) {
        return {
            emailConfig: {
                to: req.querystring.email,
                subject: emailHelpers.emailSubject("backInStockValidation", Resource.msg("subject.backinstock.validation", "product", null)),
                from: siteHelper.sitePreference("customerServiceEmail", "no-reply@salesforce.com"),
                type: emailHelpers.emailTypes.backInStockValidation
            },
            data: {
                pid: req.querystring.pid,
                url: URLUtils.https("BackInStock-Validation", "t", req.querystring.email)
            },
            renderingTemplate: "product/components/backInStockValidationEmail"
        };
    },
    productBackInStock: function (req) {
        var status;

        if (!req.querystring.pid) {
            status = "Missing product ID";
        }

        return {
            emailConfig: {
                to: req.querystring.email,
                subject: emailHelpers.emailSubject("backInStockNotification", Resource.msg("subject.backinstock.notification", "product", null)),
                from: siteHelper.sitePreference("customerServiceEmail", "no-reply@salesforce.com"),
                type: emailHelpers.emailTypes.backInStockNotification
            },
            data: {
                pid: req.querystring.pid,
                url: URLUtils.https("Product-Show", "pid", req.querystring.pid)
            },
            renderingTemplate: "product/components/backInStockNotificationEmail",
            status: status
        };
    },
    accountRegistered: function (req) {
        return {
            emailConfig: {
                to: req.querystring.email,
                subject: emailHelpers.emailSubject("registration", Resource.msg("email.subject.new.registration", "registration", null)),
                from: siteHelper.sitePreference("customerServiceEmail", "no-reply@salesforce.com"),
                type: emailHelpers.emailTypes.registration
            },
            data: {
                email: req.querystring.email,
                firstName: "MockFirstName",
                lastName: "MockLastName",
                url: URLUtils.https("Login-Show")
            },
            renderingTemplate: "checkout/confirmation/accountRegisteredEmail"
        };
    },
    passwordChanged: function (req) {
        return {
            emailConfig: {
                to: req.querystring.email,
                subject: emailHelpers.emailSubject("passwordChanged", Resource.msg("subject.profile.resetpassword.email", "login", null)),
                from: siteHelper.sitePreference("customerServiceEmail", "no-reply@salesforce.com"),
                type: emailHelpers.emailTypes.passwordChanged
            },
            data: {
                firstName: "MockFirstName",
                lastName: "MockLastName",
                url: URLUtils.https("Login-Show")
            },
            renderingTemplate: "account/password/passwordChangedEmail"
        };
    },
    passwordReset: function (req) {
        return {
            emailConfig: {
                to: req.querystring.email,
                subject: emailHelpers.emailSubject("passwordReset", Resource.msg("subject.profile.resetpassword.email", "login", null)),
                from: siteHelper.sitePreference("customerServiceEmail", "no-reply@salesforce.com"),
                type: emailHelpers.emailTypes.passwordReset
            },
            data: {
                passwordResetToken: "MockPasswordResetToken",
                firstName: "MockFirstName",
                lastName: "MockLastName",
                url: URLUtils.https("Account-SetNewPassword", "Token", "MockPasswordResetToken")
            },
            renderingTemplate: "account/password/passwordResetEmail"
        };
    },
    accountLocked: function (req) {
        return {
            emailConfig: {
                to: req.querystring.email,
                subject: emailHelpers.emailSubject("accountLocked", Resource.msg("subject.account.locked.email", "login", null)),
                from: siteHelper.sitePreference("customerServiceEmail", "no-reply@salesforce.com"),
                type: emailHelpers.emailTypes.accountLocked
            },
            data: {
                firstName: "MockFirstName",
                lastName: "MockLastName",
                url: URLUtils.https("Account-SetNewPassword", "Token", "MockPasswordResetToken")
            },
            renderingTemplate: "mail/account/accountLockedEmail"
        };
    },
    accountVerification: function (req) {
        return {
            emailConfig: {
                to: req.querystring.email,
                subject: emailHelpers.emailSubject("accountLocked", Resource.msg("email.account.validate.subject", "account", null)),
                from: Site.current.getCustomPreferenceValue("customerServiceEmail") || "no-reply@salesforce.com",
                type: emailHelpers.emailTypes.accountVerification
            },
            data: {
                firstName: "MockFirstName",
                lastName: "MockLastName",
                url: URLUtils.https("Account-ActivateAccount", "t", StringUtils.encodeBase64("customerNo"), "s", "doubleOptinUniqueID")
            },
            renderingTemplate: "account/accountVerificationEmail"
        };
    },
    accountEdited: function (req) {
        return {
            emailConfig: {
                to: req.querystring.email,
                subject: emailHelpers.emailSubject("accountEdited", Resource.msg("email.subject.account.edited", "account", null)),
                from: siteHelper.sitePreference("customerServiceEmail", "no-reply@salesforce.com"),
                type: emailHelpers.emailTypes.accountEdited
            },
            data: {
                firstName: "MockFirstName",
                lastName: "MockLastName",
                url: URLUtils.https("Login-Show")
            },
            renderingTemplate: "account/components/accountEditedEmail"
        };
    }
};

server.get("Start", devOnlyMiddleware, function (req, res, next) {
    var emailSendStatus = "Email PROBABLY have been sent, check you email box";
    var status;

    // 2 params always required, others are optional
    if (req.querystring.email && req.querystring.emailType) {
        var emailHelperParams = emailsDict[req.querystring.emailType](req);

        if (!emailHelperParams.status) {
            emailHelpers.sendEmail(emailHelperParams.emailConfig, emailHelperParams.renderingTemplate, emailHelperParams.data);
            status = emailSendStatus;
        }
    }

    if (req.querystring.emailType && !req.querystring.email) {
        status = "Email not set";
    }

    res.render("mail/emailmock", {
        status: status,
        querystring: req.querystring
    });
    next();
});

module.exports = server.exports();
