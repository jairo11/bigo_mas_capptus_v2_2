"use strict";

var backInStockHelper = require("*/cartridge/scripts/features/backInStockHelpers");

function backInStockNotifications(req) {
    return backInStockHelper.getNotificationModel(req, 2).notifications;
}


module.exports = function (object, req) {
    Object.defineProperty(object, "backInStockNotifications", {
        enumerable: true,
        value: backInStockNotifications(req)
    });
};
