"use strict";

var backInStockDecorator = require("*/cartridge/models/account/decorators/backInStock");

/**
 * Decorate an object(account model) with wishlist information
 * @param {Object} object - account Model to be decorated
 * @param {dw.system.Reques} req - current request
 *
 * @returns {Object} - Decorated account model
 */
module.exports = function backInStockAccount(object, req) {
    backInStockDecorator(object, req);
    return object;
};
