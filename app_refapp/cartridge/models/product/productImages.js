"use strict";

var collections = require("*/cartridge/scripts/util/collections");
var DIS = require("*/cartridge/scripts/util/DIS.js");

/**
 * @constructor
 * @classdesc Returns images for a given product
 * @param {dw.catalog.Product} product - product to return images for
 * @param {Object} imageConfig - configuration object with image types
 */
function Images(product, imageConfig) {
    imageConfig.types.forEach(function (type) {
        var result = {};

        if (type === "swatch") {
            var images = product.getImages(type);

            if (imageConfig.quantity === "single") {
                var firstImage = collections.first(images);
                if (firstImage) {
                    result = [{
                        alt: firstImage.alt,
                        url: encodeURI(firstImage.URL.toString()),
                        title: firstImage.title,
                        index: "0",
                        absURL: encodeURI(firstImage.absURL.toString())
                    }];
                }
            } else {
                result = collections.map(images, function (image, index) {
                    return {
                        alt: image.alt,
                        url: encodeURI(image.URL.toString()),
                        index: index.toString(),
                        title: image.title,
                        absURL: encodeURI(image.absURL.toString())
                    };
                });
            }
        } else {
            if (imageConfig.quantity === "single") {
                result = DIS.getImage(product, type);
            } else {
                result = DIS.getImages(product, type);
            }
        }

        this[type] = result;
    }, this);
}

module.exports = Images;
