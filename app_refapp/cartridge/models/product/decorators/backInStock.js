"use strict";

function canAddBackInStock(apiProduct) {
    var backInStockFeatureEnabled = require("helpers").sitePreference("enableBackInStock");

    var availabilityModel = apiProduct.getAvailabilityModel();

    return backInStockFeatureEnabled && !apiProduct.custom.disableBackInStock && !availabilityModel.inStock && availabilityModel.availabilityStatus == availabilityModel.AVAILABILITY_STATUS_NOT_AVAILABLE;

}


module.exports = function (object, apiProduct) {
    Object.defineProperty(object, "addBackInStock", {
        enumerable: true,
        value: canAddBackInStock(apiProduct)
    });
};
