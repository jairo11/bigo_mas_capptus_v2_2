"use strict";

module.exports = function (object, category) {
    Object.defineProperty(object, "primaryCategory", {
        enumerable: true,
        value: category
    });
};
