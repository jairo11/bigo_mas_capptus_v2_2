"use strict";

function getRecommendationProductIds(product) {
    var recommendations = product.getOrderableRecommendations(1);//getting cross sell recommendations
    var collections = require("*/cartridge/scripts/util/collections");
    return collections.map(recommendations, function (recommendation) {
        return recommendation.getRecommendedItemID();
    });
}

module.exports = function (object, product) {
    Object.defineProperty(object, "relatedProductIDs", {
        enumerable: true,
        value: getRecommendationProductIds(product)
    });
};
