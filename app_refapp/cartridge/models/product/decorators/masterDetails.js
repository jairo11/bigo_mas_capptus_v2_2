"use strict";

module.exports = function (object, apiProduct) {
    Object.defineProperty(object, "masterProduct", {
        enumerable: true,
        value: apiProduct.masterProduct
    });
};
