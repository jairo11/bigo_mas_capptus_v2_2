"use strict";
var siteHelper = require("helpers");

function getDisplayableDescriptionAttributes(product) {
    var typeDef = product.describe();
    var sitePdpDescriptionAttributes = siteHelper.sitePreference("pdpDescriptionAttributes");
    var productDescriptionAttributes = "descriptionAttributes" in product.custom ? product.custom.descriptionAttributes : "";
    var descriptionAttributes = productDescriptionAttributes.length > 0 ? productDescriptionAttributes : sitePdpDescriptionAttributes;
    var obj = {};

    if (descriptionAttributes.length > 0) {
        var attributeDefinition;
        var attributeValue;
        descriptionAttributes.reduce(function (result, attribute) {
            attributeDefinition = typeDef.getCustomAttributeDefinition(attribute) || typeDef.getSystemAttributeDefinition(attribute);
            if (attributeDefinition) {
                attributeValue = attributeDefinition.system ? product[attribute] : product.custom[attribute];
                result[attribute] = {
                    name: attribute,
                    value: attributeValue
                };
            }
            return result;
        }, obj);
    }

    return obj;
}

module.exports = function (object, product) {
    Object.defineProperty(object, "attributesDescriptions", {
        enumerable: true,
        value: getDisplayableDescriptionAttributes(product)
    });
};
