"use strict";

module.exports = function (object, product) {
    Object.defineProperty(object, "tabDetails", {
        enumerable: true,
        value: Object.prototype.hasOwnProperty.call(product.custom, "tabDetails") && !empty(product.custom.tabDetails) ? product.custom.tabDetails.markup : ""
    });
    Object.defineProperty(object, "tabShippingReturns", {
        enumerable: true,
        value: Object.prototype.hasOwnProperty.call(product.custom, "tabShippingReturns") && !empty(product.custom.tabShippingReturns) ? product.custom.tabShippingReturns.markup : ""
    });
};
