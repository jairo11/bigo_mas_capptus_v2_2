"use strict";
var collections = require("*/cartridge/scripts/util/collections");

module.exports = function (object, apiProduct) {
    Object.defineProperty(object, "recommendations", {
        enumerable: true,
        value: (function () {
            var arr = [];
            collections.forEach(apiProduct.recommendations, function (recommendation) {
                this.push({
                    ID: recommendation.recommendedItemID,
                    recommendationType: recommendation.recommendationType
                });
            }, arr);
            return arr;
        }())
    });
};
