"use strict";

function getDefaultImage(product) {
    var image = "";
    if (product.getImage("large")) {
        image = product.getImage("large").getAbsURL().toString();
    }

    return image;
}

module.exports = function (object, product, _config) {
    Object.defineProperty(object, "socialMediaImage", {
        enumerable: true,
        value: getDefaultImage(product),
    });
};
