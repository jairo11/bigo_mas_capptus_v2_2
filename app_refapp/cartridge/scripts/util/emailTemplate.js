"use strict";

var emailHelpers = require("*/cartridge/scripts/helpers/emailHelpers");

var templatesType = {
    orderDetails: {template: "order/components/orderdetails", type: "order"},
    payment: {template: "order/components/paymentdetails", type: "order"},
    orderDetailsTable: {template: "mail/components/orderDetailsTable", type: "order"}
};

function orderModel(order) {
    var OrderModel = require("*/cartridge/models/order");

    var orderObj = new OrderModel(order, { countryCode: order.customerLocaleID, containerView: "order" });
    return orderObj;
}

function getOrderConfirmation(order, template) {
    var orderObj = orderModel(order);
    orderObj.orderDetailsTableTemplate = templatesType.orderDetails;
    orderObj.orderPaymentTableTemplate = templatesType.payment;

    return emailHelpers.replaceAssetBody(orderObj, template);
}

function getOrder(order, assetTemplate, template) {
    var orderObj = orderModel(order);
    orderObj.orderDetailsTableTemplate = template;

    return emailHelpers.replaceAssetBody(orderObj, assetTemplate);
}

module.exports = {
    getOrder: getOrder,
    getOrderConfirmation: getOrderConfirmation,
    templatesType: templatesType
};
