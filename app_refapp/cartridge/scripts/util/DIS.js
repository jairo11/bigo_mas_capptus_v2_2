"use strict";

var Logger = require("dw/system/Logger").getLogger("DIS", "DIS");
var URLUtils = require("dw/web/URLUtils");
var Resource = require("dw/web/Resource");

/**
 * Get the first product image
 *
 * @param {string|dw.catalog.Product} productSource - Product instance or Id of the product
 * @param {string} type - View type of the image (e.g. large, medium, small)
 * @returns {Object|null}
 */
function getImage(productSource, type, host) {
    var parameters = getParameters(productSource, type);
    var image;
    var url;

    if (!parameters.scalingProperties) {
        // Return original image without resizing.
        image = parameters.product.getImage(type);
        if (image) {
            url = empty(host)
                ? image.getHttpsURL().toString()
                : image.getHttpsURL().host(host).toString();

            return {
                url: encodeURI(url),
                title: image.getTitle(),
                alt: image.getAlt(),
                absUrl: encodeURI(image.getAbsURL())
            };
        }
    } else {
        // Get the first image with referenced type.
        image = !empty(parameters.product) ? parameters.product.getImage(parameters.sourceViewType) : "";
        if (image) {
            // Resizing image with DIS.
            url = empty(host)
                ? image.getHttpsImageURL(parameters.scalingProperties).toString()
                : image.getHttpsImageURL(parameters.scalingProperties).host(host).toString();

            return {
                url: encodeURI(addScalingToRemoteURL(url, parameters.scalingProperties)),
                title: image.getTitle(),
                alt: image.getAlt()
            };
        }
    }

    return {
        url: URLUtils.staticURL("/images/noimagelarge.png"),
        title: Resource.msg("msg.no.image", "common", null),
        alt: Resource.msg("msg.image.missing", "common", null),
        noimage: true
    };
}

/**
 * Get all product images
 *
 * @param {string|dw.catalog.Product} productSource - Product instance or Id of the product
 * @param {string} type - View type of the image (e.g. large, medium, small)
 * @returns {array|null}
 */
function getImages(productSource, type) {
    var parameters = getParameters(productSource, type);
    var result     = [];
    var images;

    if (!parameters.scalingProperties) {
        // Get image type and return original image without resizing.
        images = parameters.product.getImages(type);
        if (images) {
            result = getImagesWithoutResizing(images);
        }
    } else {
        // Get product images with referenced type and scale them for the current type.
        images = parameters.product.getImages(parameters.sourceViewType);
        if (images) {
            result = transform(images, parameters.scalingProperties);
        }
    }

    return result;
}

/**
 * Extract productSource and return Product object with DIS settings
 *
 * @param {string|dw.catalog.Product} productSource - Product instance or Id of the product
 * @param {string} type - View type of the image (e.g. large, medium, small)
 * @returns {Object} Product object and DIS related configuration parameters
 */
function getParameters(productSource, type) {
    var product;
    var sourceViewType;
    var scalingProperties;

    // Get product from ProductMgr in case when productSource is string product ID.
    if (typeof productSource === "string") {
        product = require("dw/catalog/ProductMgr").getProduct(productSource);
        // Show error message if product is invalid.
        if (!product) {
            Logger.error("Invalid Product ID:" + productSource);
            return false;
        }
    } else {
        product = productSource;
    }

    // Get DIS configuration from BM.
    var disConfiguration = getConfiguration();
    if (!disConfiguration) {
        sourceViewType = false;
        scalingProperties = false;
    } else {
        if (Object.prototype.hasOwnProperty.call(disConfiguration, type)) {
            var typeConfig = disConfiguration[type];
            // Get image type and scaling properties: scalingWidth, scalingHeight, scaleMode and quality.
            sourceViewType = typeConfig.sourceViewType ? typeConfig.sourceViewType : "original";
            scalingProperties = typeConfig.scalingProperties;

            if (!Object.prototype.hasOwnProperty.call(scalingProperties, "scaleWidth")
                || !Object.prototype.hasOwnProperty.call(scalingProperties, "scaleHeight")
                || !Object.prototype.hasOwnProperty.call(scalingProperties, "scaleMode")
                || !Object.prototype.hasOwnProperty.call(scalingProperties, "quality"))
            {
                scalingProperties = false;
            }

        } else {
            scalingProperties = false;
        }
    }

    return {
        product: product,
        sourceViewType: sourceViewType,
        scalingProperties: scalingProperties
    };
}

/**
 * Retrives DIS configuration from BM
 *
 * @returns {Object} configuration object
 */
function getConfiguration() {
    var configJSON  = require("helpers").sitePreference("disScalingProperties");
    var config;

    if (!configJSON) {
        Logger.error("Configuration: Could not find disScalingProperties");
        return false;
    }

    try {
        config = JSON.parse(configJSON);
    } catch (e) {
        Logger.error("Configuration: Incorrect formed disScalingProperties, JSON format is invalid!");
        return false;
    }

    return config;
}

/**
 * Gets images and returns all links as HTTPS
 *
 * @param {dw.util.List} images - List of the product images
 */
function getImagesWithoutResizing(images) {
    var imagesLinks = [];
    var iterator    = images.iterator();

    while (iterator.hasNext()) {
        var image = iterator.next();

        // Add the current unresized image with image data to array.
        imagesLinks.push({
            url: encodeURI(image.getHttpsURL().toString()),
            title: image.getTitle(),
            alt: image.getAlt()
        });
    }

    return imagesLinks;
}

/**
 * Scales images by configuration values (width, height, mode, quality)
 *
 * @param {dw.util.List} images - List of the product images
 * @param {Object} scalingProperties - Properties for transforming
 */
function transform(images, scalingProperties) {
    var scaledImages = [];
    var iterator     = images.iterator();

    // Get images and scale them in a loop.
    while (iterator.hasNext()) {
        var image = iterator.next();

        // Add the current scaled image with image data to array.
        scaledImages.push({
            url: encodeURI(addScalingToRemoteURL(image.getHttpsImageURL(scalingProperties).toString(), scalingProperties)),
            title: image.getTitle(),
            alt: image.getAlt()
        });
    }

    return scaledImages;
}

/**
 * If the useDISRemote preference is setted up, this function will add the transform parameters in the remote url.
 * For the this functionality works, the catalog should be configurated as remote image.
 *
 * @param {String} url - The abs url that will be replaced
 * @param {Object} scalingProperties - The image scaling properties
 * @returns {String} - The replaced url.
 */
function addScalingToRemoteURL(url, scalingProperties) {
    var useDISRemote = require("helpers").sitePreference("useDISRemote");

    if (useDISRemote && !empty(scalingProperties)) {
        var urlSplited = url.split("/");
        var imageName = urlSplited[urlSplited.length -1];

        if (imageName.indexOf("?") == -1) {
            var localUrlSplited = URLUtils.imageURL(imageName, scalingProperties).toString().split("/");
            urlSplited[urlSplited.length -1] = localUrlSplited[localUrlSplited.length - 1];
            url = urlSplited.join("/");
        }
    }

    return url;
}

module.exports = {
    getImage: getImage,
    getImages: getImages
};
