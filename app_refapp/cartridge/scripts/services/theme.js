"use strict";

var LocalServiceRegistry = require("dw/svc/LocalServiceRegistry");
var Logger = require("dw/system/Logger");

function getClientToken() {
    var tokenService = LocalServiceRegistry.createService(
        "get.ocapi.client.token",
        {
            createRequest: function (service) {
                var serviceCredential = service
                    .getConfiguration()
                    .getCredential();
                var clientID = serviceCredential.getUser();
                var clientPassword = serviceCredential.getPassword();

                var Auth = dw.util.StringUtils.encodeBase64(
                    clientID + ":" + clientPassword
                );

                service.setURL(
                    "https://account.demandware.com/dwsso/oauth2/access_token"
                );
                service.setRequestMethod("POST");
                service.addHeader("Accept", "application/json");
                service.addHeader("Authorization", "Basic " + Auth);
                service.addHeader(
                    "Content-Type",
                    "application/x-www-form-urlencoded"
                );

                return "grant_type=client_credentials";
            },
            parseResponse: function (service, response) {
                //NOSONAR
                return JSON.parse(response.getText("UTF-8"));
            }
        }
    );

    var result = tokenService.call();

    if (!result || result.errorMessage) {
        Logger.error("Authentication failed: " + result.errorMessage);
        return;
    }

    return result.object.access_token;
}

function getThemeUpdateService() {
    return LocalServiceRegistry.createService(
        "theme.customizer.update",
        {
            createRequest: function (service, params) {
                var serviceCredential = service
                    .getConfiguration()
                    .getCredential();
                var clientID = serviceCredential.getUser();

                service.setURL(params.urlEndpoint);
                service.setRequestMethod("PATCH");
                service.addHeader("client_id", clientID);
                service.addHeader("Accept", "application/json");
                service.addHeader("Authorization", "Bearer " + params.clientToken);
                service.addHeader("Content-Type", "application/json");

                return params.payload;
            },
            parseResponse: function (service, response) {
                //NOSONAR
                return JSON.parse(response.getText("UTF-8"));
            }
        }
    );
}

module.exports = {
    getClientToken: getClientToken,
    getThemeUpdateService: getThemeUpdateService
};
