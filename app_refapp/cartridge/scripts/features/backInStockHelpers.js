"use strict";

var CustomObjectMgr = require("dw/object/CustomObjectMgr");
var Transaction = require("dw/system/Transaction");


function addBackInStockNotification(notification) {
    var email = notification.email;
    var pid = notification.pid;
    Transaction.wrap(function () {
        var backInStockNotification = CustomObjectMgr.getCustomObject("BackInStockNotifications", pid);
        if (!backInStockNotification) {
            backInStockNotification = CustomObjectMgr.createCustomObject("BackInStockNotifications", pid);
        }

        if (backInStockNotification.custom.emails) {
            var emails = backInStockNotification.custom.emails.split(",");
            if (emails.indexOf(email) < 0) {
                emails.push(email);
            }
            backInStockNotification.custom.emails = emails.join(",");
        } else {
            backInStockNotification.custom.emails = email;
        }
    });
}

function addBackInStockValidation(email, pid, uuid) {
    Transaction.wrap(function () {
        var backInStockActivation = CustomObjectMgr.createCustomObject("BackInStockActivations", uuid);
        backInStockActivation.custom.toValidate = JSON.stringify({pid: pid, email: email});

        sendBackInStockEmailValidation(email, uuid, pid);

    });
}

function sendBackInStockEmailValidation(email, uuid, pid) {
    var emailHelpers = require("*/cartridge/scripts/helpers/emailHelpers");
    var URLUtils = require("dw/web/URLUtils");
    var Resource = require("dw/web/Resource");
    var siteHelper = require("helpers");

    var userObject = {
        pid: pid,
        url: URLUtils.https("BackInStock-Validation", "t", uuid)
    };

    var emailObj = {
        to: email,
        subject: emailHelpers.emailSubject("backInStockValidation", Resource.msg("subject.backinstock.validation", "product", null)),
        from: siteHelper.sitePreference("customerServiceEmail", "no-reply@salesforce.com"),
        type: emailHelpers.emailTypes.backInStockValidation
    };

    emailHelpers.sendEmail(emailObj, "product/components/backInStockValidationEmail", userObject);
}

function sendBackInStockEmailNotification(email, pid) {
    var emailHelpers = require("*/cartridge/scripts/helpers/emailHelpers");
    var URLUtils = require("dw/web/URLUtils");
    var Resource = require("dw/web/Resource");
    var siteHelper = require("helpers");

    var userObject = {
        pid: pid,
        url: URLUtils.https("Product-Show", "pid", pid)
    };

    var emailObj = {
        to: email,
        subject: emailHelpers.emailSubject("backInStockNotification", Resource.msg("subject.backinstock.notification", "product", null)),
        from: siteHelper.sitePreference("customerServiceEmail", "no-reply@salesforce.com"),
        type: emailHelpers.emailTypes.backInStockNotification
    };

    emailHelpers.sendEmail(emailObj, "product/components/backInStockNotificationEmail", userObject);
}

function backInStockUserNotifications(req) {
    var ImageModel = require("*/cartridge/models/product/productImages");
    var ProductMgr = require("dw/catalog/ProductMgr");

    var email = req.currentCustomer.profile.email;
    var querystring = "custom.emails LIKE {0}";
    var sortby = "custom.productID";
    var notifications = CustomObjectMgr.queryCustomObjects("BackInStockNotifications", querystring, sortby, "*"+email+"*");
    var backInStockNotifications = [], notification, product;

    while (notifications.hasNext()) {
        notification = notifications.next();
        product = ProductMgr.getProduct(notification.custom.productID);
        backInStockNotifications.push({
            pid: product.ID,
            name: product.name,
            imageObj: new ImageModel(product, { types: ["small"], quantity: "single" })
        });
    }

    return backInStockNotifications;
}

function removesUserNotification(email, pid) {
    Transaction.wrap(function () {
        var notification = CustomObjectMgr.getCustomObject("BackInStockNotifications", pid);
        var emails = notification.custom.emails.split(",");
        var index = emails.indexOf(email);
        if (index >= 0) {
            emails.splice(index, 1);
        }
        notification.custom.emails = emails.join(",");
    });
}

function isBackInStockEnabled() {
    return require("helpers").sitePreference("enableBackInStock");
}

function getNotificationModel(req, pagesize) {
    var totalNumber = 0;
    var pageSize = pagesize || 15;
    var pageNumber = req.querystring.pageNumber || 1;
    var notifications = backInStockUserNotifications(req);

    var backInStockNotificationModel = {
        hasMore: false,
        pageNumber: pageNumber
    };

    var result = [];

    notifications.forEach(function (notification) {
        if (totalNumber < (pageSize * pageNumber)) {
            result.push(notification);
            totalNumber++;
        } else {
            totalNumber++;
        }
    });

    backInStockNotificationModel.notifications = result;
    backInStockNotificationModel.hasMore = totalNumber > pageSize * pageNumber;

    return backInStockNotificationModel;
}

module.exports = {
    addBackInStockNotification: addBackInStockNotification,
    addBackInStockValidation: addBackInStockValidation,
    sendBackInStockEmailNotification: sendBackInStockEmailNotification,
    backInStockUserNotifications: backInStockUserNotifications,
    removesUserNotification: removesUserNotification,
    isBackInStockEnabled: isBackInStockEnabled,
    getNotificationModel: getNotificationModel
};
