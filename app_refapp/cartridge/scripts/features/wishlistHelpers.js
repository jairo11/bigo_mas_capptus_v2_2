"use strict";

var productListHelper = require("*/cartridge/scripts/features/productListHelpers");
var wishListType = require("dw/customer/ProductList").TYPE_WISH_LIST;
var collections = require("*/cartridge/scripts/util/collections");

function isWishlistEnabled() {
    return require("helpers").sitePreference("enableWishlist");
}

function isProductInWishlist(wishListItems, product) {
    if (isWishlistEnabled()) {
        return !empty(collections.find(wishListItems, function (item) {
            return item.productID == product.id || (!empty(product.masterProduct) && item.productID == product.masterProduct.ID);
        }));
    }
}

function mergeGuestListToLoggedInCustomer(guestCustomer, authenticatedCustomer, req) {
    if (isWishlistEnabled()) {
        var guestWishlist = productListHelper.getList(guestCustomer, { type: wishListType });
        var listLoggedIn = productListHelper.getList(authenticatedCustomer, { type: wishListType });
        productListHelper.mergelists(listLoggedIn, guestWishlist, req, { type: wishListType });
        productListHelper.updateWishlistPrivacyCache(guestCustomer, req, { type: wishListType });
    }
}

function createPurchases(order, req) {
    if (isWishlistEnabled()) {
        collections.forEach(order.productLineItems, function (productLineItem) {
            productListHelper.createPurchase(req.currentCustomer.raw, productLineItem.productID, order.currentOrderNo, productLineItem.quantityValue, { req: req, type: wishListType, optionId: null, optionValue: null }); // get these values
        });
    }
}

module.exports = {
    isWishlistEnabled: isWishlistEnabled,
    mergeGuestListToLoggedInCustomer: mergeGuestListToLoggedInCustomer,
    createPurchases: createPurchases,
    isProductInWishlist: isProductInWishlist
};
