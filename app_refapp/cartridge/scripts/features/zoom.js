"use strict";

const Logger = require("dw/system/Logger");
const siteHelper = require("helpers");

/**
 * @function
 * @description Get Site preference for Zoom enable/disable
 * @returns {Boolean}
 */
function isZoomEnabled() {
    let enabled = siteHelper.sitePreference("zoom_enabled");
    if (enabled == null) {
        Logger.error("Zoom: Could not find zoom_enabled Site Preference.");
        return false;
    }
    return enabled;
}

/**
 * @function
 * @description Get Site preference for Zoom options. Parse preferences and return a JSON with values.
 * @see Zoom Configurations - Custom Site Preference Group for options available
 * @returns {Object}
 */
function getZoomOptions() {
    let configJSON = {};
    let configOptions = [
        "lensShape",
        "adaptive",
        "position",
        "mposition",
        "defaultScale",
        "fadeOut",
        "tint",
        "title",
        "scroll",
        "fadeIn",
        "lensOpacity",
        "tintOpacity",
        "Yoffset",
        "Xoffset",
        "openOnSmall",
        "fadeTrans",
        "lensCollision",
        "smoothZoomMove",
        "hover",
        "lens",
        "smoothLensMove",
        "smoothScale",
        "bg"
    ];

    configOptions.forEach(function (item) {
        try {
            var preference = siteHelper.sitePreference("zoom_" + item);
            if (preference && preference.value) {
                configJSON[item] = preference.value;
            } else {
                configJSON[item] = preference;
            }
        } catch (e) {
            Logger.error("Zoom: Incorrect Site Preference name or format.");
        }
    });

    try {
        configJSON = JSON.stringify(configJSON);
    } catch (e) {
        Logger.error("Zoom: Incorrect formed JSON.");
        return null;
    }

    return configJSON;
}

module.exports = {
    isZoomEnabled: isZoomEnabled,
    getZoomOptions: getZoomOptions
};
