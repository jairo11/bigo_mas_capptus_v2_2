"use strict";
var Resource = require("dw/web/Resource");
var siteHelper = require("helpers");
var emailHelper = require("*/cartridge/scripts/helpers/emailHelpers");

exports.subscribe = function (firstName, lastName, email, topic, comment) {
    var contactUsRecipients = siteHelper.sitePreference("contactUsRecipients");
    var contactUs = emailHelper.emailTypes["contactUs"];
    var contactUsAdmin = emailHelper.emailTypes["contactUsAdmin"];

    if (contactUs.enabled) {
        var contactDetails = {
            firstName: firstName,
            lastName: lastName,
            email: email,
            topic: topic,
            comment: comment
        };

        var emailObject = {
            to: email,
            from: siteHelper.sitePreference("customerServiceEmail", "no-reply@salesforce.com"),
            subject: emailHelper.emailSubject("contactUs", Resource.msg("email.subject", "contactUs", null)),
            type: contactUs
        };
        emailHelper.sendEmail(emailObject, "mail/feedback", contactDetails);

        var emailObjectAdmin = {
            to: email,
            from: siteHelper.sitePreference("customerServiceEmail", "no-reply@salesforce.com"),
            subject: emailHelper.emailSubject("contactUsAdmin", Resource.msg("email.subject", "contactUs", null)),
            type: contactUsAdmin
        };

        contactUsRecipients.forEach(function (serverEmail) {
            emailObjectAdmin.to = serverEmail;

            emailHelper.sendEmail(emailObjectAdmin, "mail/feedbackAdmin", contactDetails);
        });
    }
};
