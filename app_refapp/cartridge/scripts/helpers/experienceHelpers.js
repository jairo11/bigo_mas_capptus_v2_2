"use strict";

var PageMgr = require("dw/experience/PageMgr");
var Site = require("dw/system/Site");

var currentSiteId = Site.getCurrent().getID();

function setPageToRender(res, pagename) {
    var page = PageMgr.getPage(pagename + "-" + currentSiteId);

    if (page && page.isVisible()) {
        res.setStatusCode(200);
        res.page(page.ID, {});
    }
}

module.exports = {
    setPageToRender: setPageToRender
};
