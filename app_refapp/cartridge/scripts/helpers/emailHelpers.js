"use strict";

var renderTemplateHelper = require("*/cartridge/scripts/renderTemplateHelper");
var ContentMgr = require("dw/content/ContentMgr");
var styleHelpers = require("*/cartridge/scripts/helpers/styleHelpers");

/**
 * Helper that select a template configured in the business manager or the default template.
 * @param {Object} emailObjType - email type configuration, represents one of the values on emailTypes Enum.
 * @param {string} templateDefault - Location of the ISML default template to be rendered in the email, used only if no configuration was found for the email type selected.
 * @returns {string} template or null
 */

function templateConfiguration(emailObjType, templateDefault, context) {
    var template = templateDefault;

    if (emailObjType.enabled) {
        if (emailObjType.assets) {
            var assets = [];

            assets = Object.keys(emailObjType.assets).reduce(function (acumulator, asset) {
                acumulator.push(replaceAssetBody(context, emailObjType.assets[asset]));
                return acumulator;
            }, assets);

            if (!empty(assets)) {
                template = "mail/mailcontainer.isml";
                context = {body: assets};
            }
        }
    }

    return template ? renderTemplateHelper.getRenderedHtml(context, template) : null;
}

/**
 * Creates a regex pattern to be used as a container to be replaced inside content assets;
 * @param {String} key - key value to be replaced.
 * @returns {String}
 */
function definePatternReplace(key) {
    var pattern = "[[" + key + "]]";
    return pattern;
}

/**
 * Merge properties of two objects dynamically
 * @param {Object} to - Object where all properties will be merged
 * @param {Object} from - Reference object the values to be copied from.
 * @returns {Object}
 */
function mergeObjects(to, from) {
    for (var n in from) {
        if (typeof to[n] != "object") {
            to[n] = from[n];
        } else if (typeof from[n] == "object") {
            to[n] = mergeObjects(to[n], from[n]);
        }
    }

    return to;
}

/**
 *
 * @param {Object} templateValues - object with values to be replaced inside content asset body.
 * @param {string} assetId - content asset id, that contains a body with markups
 * @param {dw.system.Request} req - system request object
 * @retunrs string body
 */
function replaceAssetBody(templateValues, assetId) {
    var dynamicValues = templateValues || {};
    var colorValues = styleHelpers.colorsMap;
    var objValues = mergeObjects(dynamicValues, colorValues);

    var assetBody;
    if (assetId) {
        var Calendar = require("dw/util/Calendar");
        var StringUtils = require("dw/util/StringUtils");

        var asset = ContentMgr.getContent(assetId);
        var template, type, valueToPlace, templateResult, obj = {};
        if (!empty(asset)) {
            assetBody = asset.custom.body.markup;
            if (objValues && assetBody) {
                Object.keys(objValues).forEach(function (key) {
                    templateResult = null;
                    valueToPlace = objValues[key];
                    if (assetBody.indexOf(key) >= 0) {
                        if (objValues[key].hasOwnProperty("template")) { // eslint-disable-line no-prototype-builtins
                            template = objValues[key].template;
                            type = objValues[key].type;
                            obj = {};

                            if (type) {
                                obj[type] = objValues;
                            } else {
                                obj = objValues;
                            }

                            templateResult = renderTemplateHelper.getRenderedHtml(obj, template, "text/html", "UTF-8");
                        } else if (objValues[key] instanceof Date) {
                            valueToPlace = StringUtils.formatCalendar(new Calendar(objValues[key]), request.locale, Calendar.INPUT_DATE_PATTERN);
                        } else if (objValues[key] instanceof dw.web.URL) {
                            valueToPlace = objValues[key].toString();
                        }

                        assetBody = assetBody.replace(definePatternReplace(key), templateResult || valueToPlace, "g");
                    }
                });
            }
        }
    }

    return assetBody.replace(/[[\]]/g, "");
}

/**
 * Helper that sends an email to a customer. This will only get called if hook handler is not registered
 * @param {obj} emailObj - An object that contains information about email that will be sent
 * @param {string} emailObj.to - Email address to send the message to (required)
 * @param {string} emailObj.subject - Subject of the message to be sent (required)
 * @param {string} emailObj.from - Email address to be used as a "from" address in the email (required)
 * @param {string} emailObj.type - String that specifies the type of the email being sent out. See export from emailHelpers for values.
 * @param {string} template - Location of the ISML template to be rendered in the email.
 * @param {obj} context - Object with context to be passed as pdict into ISML template.
 */
function send(emailObj, template, context) {
    template = templateConfiguration(emailObj.type, template, context);
    if (template) {
        var Mail = require("dw/net/Mail");

        var email = new Mail();
        email.addTo(emailObj.to);
        email.setSubject(emailObj.subject);
        email.setFrom(emailObj.from);
        email.setContent(template, "text/html", "UTF-8");
        email.send();
    }
}

/**
 * Checks if the email value entered is correct format
 * @param {string} email - email string to check if valid
 * @returns {boolean} Whether email is valid
 */
function validateEmail(email) {
    // eslint-disable-next-line
    var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(email);
}

/**
 * @param {String} type - type of the email defined in site preferences JSON
 * @param {String} defaultValue - default value to be returned in case no explicit subject configured
 * @returns {String} - email subject text
 */
function emailSubject(type, defaultValue) {
    var emailTypes = require("helpers").sitePreferenceAs("emailsAssetsConfigurations", "object");
    return (type in emailTypes && "subject" in emailTypes[type]) ? emailTypes[type].subject : defaultValue;
}

module.exports = {
    send: send,
    sendEmail: function (emailObj, template, context) {
        var hooksHelper = require("*/cartridge/scripts/helpers/hooks");
        return hooksHelper("app.customer.email", "sendEmail", [emailObj, template, context], send);
    },
    emailTypes: require("helpers").sitePreferenceAs("emailsAssetsConfigurations", "object"),
    emailSubject: emailSubject,
    validateEmail: validateEmail,
    replaceAssetBody: replaceAssetBody
};
