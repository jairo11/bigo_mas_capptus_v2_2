"use strict";


var Site = require("dw/system/Site");
var themeCustomizer = require("~/cartridge/scripts/themeCustomizer");
var customizerConteinerId = Site.getCurrent().getCustomPreferenceValue("themeCustomizerContainer");
var themeCustomizerThemesData = themeCustomizer.getPageThemesJSON(customizerConteinerId);
var activeThemeJSON = themeCustomizerThemesData.activeThemeVarsJSON;

let customizerColorsMap = {};

/**
 * @name colorsMap
 * @description Represents a fallback values for Theme Customizer BM JSON.
 */

const colorsMap = {
    "themeCustomizer--text-primary": "#777777",
    "themeCustomizer--text-secondary": "#FFFFFF",
    "themeCustomizer--text-special": "#323232",
    "themeCustomizer--text-special-2": "#C1B17E",
    "themeCustomizer--text-disabled": "#AFAFAF",

    "themeCustomizer--success-primary": "#5CBC42",
    "themeCustomizer--error-primary": "#f00",

    "themeCustomizer--background-primary": "#fff",
    "themeCustomizer--background-secondary": "#C1B17E",
    "themeCustomizer--background-special": "#323232",
    "themeCustomizer--background-special-2": "#F5F5F5",

    "themeCustomizer--border-primary": "#e0e0e0",
    "themeCustomizer--border-secondary": "#333",
    "themeCustomizer--border-special": "#777777",
    "themeCustomizer--border-special-2": "#F6F6F6",
    "themeCustomizer--border-special-3": "#C1B17E",

    "themeCustomizer--font-primary": "'Helvetica', Arial",
    "themeCustomizer--font-icons": "'acf-font', sans-serif"
};

/**
 * @function
 * @description Get color from Site Preferences if Theme Configurator is enabled.
 * @default Fallback color.
 * @see colorsMap
 * @param {String} cssVarId - theme customizer css variable id with "themeCustomizer" prefix
 * @requires helpers/sitePreferencesHelpers
 * @returns {String} - color value
 */
function getThemeCustomizerCssVar(cssVarId) {
    if (!empty(customizerConteinerId) && activeThemeJSON) {
        var keyWithoutPrefix = cssVarId.toString().replace("themeCustomizer", "");

        return activeThemeJSON ? (activeThemeJSON[keyWithoutPrefix] ? activeThemeJSON[keyWithoutPrefix] : colorsMap[cssVarId]) : colorsMap[cssVarId];
    } else {
        return colorsMap[cssVarId];
    }
}

if (!empty(customizerConteinerId)) {
    for (let key in colorsMap) {
        customizerColorsMap[key] = getThemeCustomizerCssVar(key);
    }
} else {
    customizerColorsMap = colorsMap;
}

/**
 * @module styleHelpers
 */
module.exports = {
    getThemeCustomizerCssVar: getThemeCustomizerCssVar,
    colorsMap: customizerColorsMap
};
