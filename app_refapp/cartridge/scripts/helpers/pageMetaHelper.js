"use strict";

var Logger = require("dw/system/Logger");
var siteHelper = require("helpers");

/**
 * Set page meta Data including page title, page description and page keywords
 *
 * @param {Object} pageMetaData - Global request pageMetaData object
 * @param {Object} object - object which contains page meta data for instance product/content
 */
function setPageMetaData(pageMetaData, object) {
    var title = "";

    if (object === null) {
        return;
    }

    if ("pageTitle" in object) {
        title = object.pageTitle;
    }

    if (!title && "name" in object) {
        title = object.name;
    } else if (!title && "productName" in object) {
        title = object.productName;
    }

    pageMetaData.setTitle(title);

    if ("pageDescription" in object && object.pageDescription) {
        pageMetaData.setDescription(object.pageDescription);
    }

    if ("pageKeywords" in object && object.pageKeywords) {
        pageMetaData.setKeywords(object.pageKeywords);
    }
}

/**
 * Set page meta tags to support rule based meta data
 *
 * @param {Object} pageMetaData - Global request pageMetaData object
 * @param {Object} object - object which contains page meta tags
 */
function setPageMetaTags(pageMetaData, object) {
    if (object === null) {
        return;
    }

    if ("pageMetaTags" in object) {
        pageMetaData.addPageMetaTags(object.pageMetaTags);
    }
}

/**
 * @description
 * @param {String} pipelineEndpoint
 * @returns {Object}
 */
function getMetaDataContent(pipelineEndpoint) {
    if (pipelineEndpoint === null) {
        return;
    }

    var ContentMgr = require("dw/content/ContentMgr");
    var apiContent = null;

    try {
        var pipelinesContentMap = JSON.parse(siteHelper.sitePreference("pipelinesMetaTagsContent"));
        apiContent = ContentMgr.getContent(pipelinesContentMap[pipelineEndpoint]);
    } catch (e) {
        Logger.warn("Issues trying to parse the 'pipelinesMetaTagsContent' site preference JSON while getting the content asset for: {0}", pipelineEndpoint);
    }

    if (!apiContent) {
        Logger.warn("Did not find definition for pipeline: {0}", pipelineEndpoint);
    }

    return apiContent;
}

/**
 * @description Returns the page title for the requested action
 * @param {Obj} Request Object, current pdict
 * @returns {String} Can be title | description | keywords
 */
function getCurrentPageMeta(pdict, type) {
    if (pdict.productSearch && pdict.productSearch.isCategorySearch) {
        return pdict.CurrentPageMetaData[type];
    } else {
        var content = getMetaDataContent(pdict.action);
        if (content) {
            switch (type) {
                case "title" :
                    return content.pageTitle;
                case "description" :
                    return content.pageDescription;
                case "keywords" :
                    return content.pageKeywords;
                default : return "";
            }
        } else {
            return pdict.CurrentPageMetaData[type];
        }
    }
}

module.exports = {
    setPageMetaData: setPageMetaData,
    setPageMetaTags: setPageMetaTags,
    getMetaDataContent: getMetaDataContent,
    getCurrentPageMeta : getCurrentPageMeta
};
