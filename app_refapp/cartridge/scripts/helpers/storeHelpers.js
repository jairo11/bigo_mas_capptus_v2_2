"use strict";
var Site = require("dw/system/Site");

/**
 * Searches for stores and creates a plain object of the stores returned by the search
 * @param {string} radius - selected radius
 * @param {string} postalCode - postal code for search
 * @param {string} lat - latitude for search by latitude
 * @param {string} long - longitude for search by longitude
 * @param {Object} geolocation - geloaction object with latitude and longitude
 * @param {boolean} showMap - boolean to show map
 * @param {dw.web.URL} url - a relative url
 * @returns {Object} a plain object containing the results of the search
 */
function getStores(radius, postalCode, lat, long, geolocation, showMap, url) {
    var StoresModel = require("*/cartridge/models/stores");
    var StoreMgr = require("dw/catalog/StoreMgr");
    var siteHelper = require("helpers");
    var URLUtils = require("dw/web/URLUtils");

    var countryCode = geolocation.countryCode;
    var distanceUnit = countryCode === "US" ? "mi" : "km";
    var resolvedRadius = radius ? parseInt(radius, 10) : 15;

    var searchKey = {};
    var storeMgrResult = null;
    var location = {};

    if (postalCode && postalCode !== "") { //NOSONAR
        // find by postal code
        searchKey = postalCode;
        storeMgrResult = StoreMgr.searchStoresByPostalCode(
            countryCode,
            searchKey,
            distanceUnit,
            resolvedRadius
        );
        searchKey = { postalCode: searchKey };
    } else {
        // find by coordinates (detect location)
        location.lat = lat && long ? parseFloat(lat) : geolocation.latitude;
        location.long = long && lat ? parseFloat(long) : geolocation.longitude;

        storeMgrResult = StoreMgr.searchStoresByCoordinates(location.lat, location.long, distanceUnit, resolvedRadius);
        searchKey = { lat: location.lat, long: location.long };
    }

    var actionUrl = url || URLUtils.url("Stores-FindStores", "showMap", showMap).toString();
    var apiKey = siteHelper.sitePreference("mapAPI");
    var storeFocusRadius = distanceUnit == "mi" ? Site.current.getCustomPreferenceValue("storeFocusMilesRadius") || 100 : Site.current.getCustomPreferenceValue("storeFocusKmRadius") || 200;
    var stores = new StoresModel(storeMgrResult.keySet(), searchKey, storeFocusRadius, actionUrl, apiKey);

    return stores;
}

function getNearbyStores(targetStore, geolocation) {
    var latitude = targetStore.latitude;
    var longitude = targetStore.longitude;
    var radius = 200;
    var showMap = true;
    var nearByStores = this.getStores(radius, null, latitude, longitude, geolocation, showMap);
    var stores = [];
    nearByStores.stores.forEach(function (store) {
        if (targetStore.ID != store.ID && stores.length < 4) {
            store.storeInfoPop = createStorePopupHtml(store);
            store.storeJSON = JSON.stringify(store);
            stores.push(store);
        }
        if (stores.length > 2) {
            return;
        }
    });

    stores = stores.length > 3 ? stores.slice(0, 3) : stores;
    nearByStores.stores = stores;
    return nearByStores;

}

/**
 * create the stores info popup html
 * @param {Object} store - store information
 * @returns {string} The rendered HTML
 */
function createStorePopupHtml(store, renderTemplate) {
    var HashMap = require("dw/util/HashMap");
    var Template = require("dw/util/Template");

    var context = new HashMap();
    var object = {store: store};

    Object.keys(object).forEach(function (key) {
        context.put(key, object[key]);
    });

    var template = new Template(renderTemplate || "storeLocator/storeInfoPopup");
    return template.render(context).text;
}
/**
 * create the stores results html
 * @param {Array} storesInfo - an array of objects that contains store information
 * @returns {string} The rendered HTML
 */
function createStoresResultsHtml(storesInfo) {
    var HashMap = require("dw/util/HashMap");
    var Template = require("dw/util/Template");

    var context = new HashMap();
    var object = { stores: { stores: storesInfo } };

    Object.keys(object).forEach(function (key) {
        context.put(key, object[key]);
    });

    var template = new Template("storeLocator/storeLocatorResults");
    return template.render(context).text;
}

/**
 * @description Get center of box
 * @param {String} locale
 * @returns {Object}
 */
function getCenterBox(locale) {
    var Locale = require("dw/util/Locale");

    var currentCountry = Locale.getLocale(locale).country;
    var sitePref = Site.getCurrent().getCustomPreferenceValue("storeLocatorCenterBox");

    if (sitePref) {
        var parsedPref = JSON.parse(sitePref);
        var centerBox = parsedPref[currentCountry] || parsedPref["default"] || null;

        if (centerBox) {
            return centerBox;
        }
    }
}

module.exports = exports = {
    createStoresResultsHtml: createStoresResultsHtml,
    getStores: getStores,
    getNearbyStores: getNearbyStores,
    createStorePopupHtml: createStorePopupHtml,
    getCenterBox: getCenterBox
};
