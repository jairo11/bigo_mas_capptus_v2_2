"use strict";

function updateThemes(
    currentThemesAssetJSON,
    updatedTheme,
    activateCurrentTheme
) {
    var previousActiveThemeId = undefined;

    if (Object.keys(currentThemesAssetJSON).indexOf(updatedTheme.id) == -1) {
        //Add new key if not yet present
        currentThemesAssetJSON[updatedTheme.id] = { active: false };
    }

    Object.keys(currentThemesAssetJSON).map(function (key) {
        if (activateCurrentTheme) {
            if (updatedTheme.id == key) {
                currentThemesAssetJSON[key].active = true;
                currentThemesAssetJSON[key].colors = JSON.parse(
                    updatedTheme.colors
                );
                currentThemesAssetJSON[key].components = JSON.parse(
                    updatedTheme.components
                );
                currentThemesAssetJSON[key].fontfamilies = JSON.parse(
                    updatedTheme.fontfamilies
                );
                currentThemesAssetJSON[key].typography = JSON.parse(
                    updatedTheme.typography
                );
            } else {
                currentThemesAssetJSON[key].active = false;
            }
        } else {
            if (currentThemesAssetJSON[key].active) {
                previousActiveThemeId = key;
            }

            if (updatedTheme.id == key) {
                currentThemesAssetJSON[key].colors = JSON.parse(
                    updatedTheme.colors
                );
                currentThemesAssetJSON[key].components = JSON.parse(
                    updatedTheme.components
                );
                currentThemesAssetJSON[key].fontfamilies = JSON.parse(
                    updatedTheme.fontfamilies
                );
                currentThemesAssetJSON[key].typography = JSON.parse(
                    updatedTheme.typography
                );
            }
            currentThemesAssetJSON[key].active = false;
        }

        return key;
    });

    if (!activateCurrentTheme) {
        if (previousActiveThemeId) {
            currentThemesAssetJSON[previousActiveThemeId].active = true;
        }
    }

    return currentThemesAssetJSON;
}

module.exports = {
    updateThemes: updateThemes
};
