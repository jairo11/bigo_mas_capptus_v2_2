"use strict";
var URLUtils = require("dw/web/URLUtils");
var Site = require("dw/system/Site");
var Resource = require("dw/web/Resource");
var endpoints = require("*/cartridge/config/oAuthRenentryRedirectEndpoints");

/**
 * Creates an account model for the current customer
 * @param {string} redirectUrl - rurl of the req.querystring
 * @param {string} privacyCache - req.session.privacyCache
 * @param {boolean} newlyRegisteredUser - req.session.privacyCache
 * @returns {string} a redirect url
 */

function getLoginRedirectURL(redirectUrl, privacyCache, newlyRegisteredUser) {
    var endpoint = "Account-Show";
    var result;
    var targetEndPoint = redirectUrl ? parseInt(redirectUrl, 10) : 1;

    var registered = newlyRegisteredUser ? "submitted" : "false";

    var argsForQueryString = privacyCache.get("args");

    if (targetEndPoint && endpoints[targetEndPoint]) {
        endpoint = endpoints[targetEndPoint];
    }

    if (argsForQueryString) {
        result = URLUtils.url(endpoint, "registration", registered, "args", argsForQueryString).relative().toString();
    } else {
        result = URLUtils.url(endpoint, "registration", registered).relative().toString();
    }

    return result;
}

/**
 * Send an email that would notify the user that account was created
 * @param {obj} registeredUser - object that contains user's email address and name information.
 */
function sendCreateAccountEmail(registeredUser) {
    var emailHelpers = require("*/cartridge/scripts/helpers/emailHelpers");
    var siteHelper = require("helpers");

    var userObject = {
        email: registeredUser.email,
        firstName: registeredUser.firstName,
        lastName: registeredUser.lastName,
        url: URLUtils.https("Login-Show")
    };

    var emailObj = {
        to: registeredUser.email,
        subject: emailHelpers.emailSubject("registration", Resource.msg("email.subject.new.registration", "registration", null)),
        from: siteHelper.sitePreference("customerServiceEmail", "no-reply@salesforce.com"),
        type: emailHelpers.emailTypes.registration
    };

    emailHelpers.sendEmail(emailObj, "checkout/confirmation/accountRegisteredEmail", userObject);
}

/**
 * Gets the password reset token of a customer
 * @param {Object} customer - the customer requesting password reset token
 * @returns {string} password reset token string
 */
function getPasswordResetToken(customer) {
    var Transaction = require("dw/system/Transaction");

    var passwordResetToken;
    Transaction.wrap(function () {
        passwordResetToken = customer.profile.credentials.createResetPasswordToken();
    });
    return passwordResetToken;
}

/**
 * Sends the email with password reset instructions
 * @param {string} email - email for password reset
 * @param {Object} resettingCustomer - the customer requesting password reset
 */
function sendPasswordResetEmail(email, resettingCustomer, template) {
    var siteHelper = require("helpers");
    var emailHelpers = require("*/cartridge/scripts/helpers/emailHelpers");
    template = template || "account/password/passwordResetEmail";

    var passwordResetToken = getPasswordResetToken(resettingCustomer);
    var url = URLUtils.https("Account-SetNewPassword", "Token", passwordResetToken);
    var objectForEmail = {
        passwordResetToken: passwordResetToken,
        firstName: resettingCustomer.profile.firstName,
        lastName: resettingCustomer.profile.lastName,
        url: url
    };

    var emailObj = {
        to: email,
        subject: emailHelpers.emailSubject("passwordReset", Resource.msg("subject.profile.resetpassword.email", "login", null)),
        from: siteHelper.sitePreference("customerServiceEmail", "no-reply@salesforce.com"),
        type: emailHelpers.emailTypes.passwordReset
    };

    emailHelpers.sendEmail(emailObj, template, objectForEmail);
}

/**
 * Send an email that would notify the user that account was edited
 * @param {obj} profile - object that contains user's profile information.
 */
function sendAccountEditedEmail(profile) {
    var emailHelpers = require("*/cartridge/scripts/helpers/emailHelpers");
    var siteHelper = require("helpers");

    var userObject = {
        firstName: profile.firstName,
        lastName: profile.lastName,
        url: URLUtils.https("Login-Show")
    };

    var emailObj = {
        to: profile.email,
        subject: emailHelpers.emailSubject("accountEdited", Resource.msg("email.subject.account.edited", "account", null)),
        from: siteHelper.sitePreference("customerServiceEmail", "no-reply@salesforce.com"),
        type: emailHelpers.emailTypes.accountEdited
    };

    emailHelpers.sendEmail(emailObj, "account/components/accountEditedEmail", userObject);
}

function isAccountValidationActive() {
    return Site.getCurrent().getCustomPreferenceValue("enableDoubleOptin");
}

function allowNonActivatedGuestOrders(email) {
    if (!email) {
        return false;
    }

    var CustomerMgr = require("dw/customer/CustomerMgr");
    var enabledNonActivateGuest = Site.getCurrent().getCustomPreferenceValue("allowNonActiveGuestOrders");
    var TempCustomer = CustomerMgr.getCustomerByLogin(email);

    var result = {
        allow: true
    };

    if (isAccountValidationActive()) {
        result.allow = enabledNonActivateGuest === false ? empty(TempCustomer) || empty(TempCustomer.profile.custom.doubleOptinUniqueID) : true;
        if (empty(TempCustomer)) {
            result.link = URLUtils.https("Login-Show").toString();
        } else {
            result.link = URLUtils.https("Account-ResendValidation", "t", dw.util.StringUtils.encodeBase64(TempCustomer.profile.customerNo)).toString();
        }
    }

    return result;
}

function sendAccountVerificationEmail(profile) {
    if (isAccountValidationActive()) {
        var emailHelpers = require("*/cartridge/scripts/helpers/emailHelpers");
        var StringUtils = require("dw/util/StringUtils");

        var userObject = {
            firstName: profile.firstName,
            lastName: profile.lastName,
            url: URLUtils.https("Account-ActivateAccount", "t", StringUtils.encodeBase64(profile.customerNo), "s", profile.custom.doubleOptinUniqueID)
        };

        var emailObj = {
            to: profile.email,
            subject: emailHelpers.emailSubject("accountLocked", Resource.msg("email.account.validate.subject", "account", null)),
            from: Site.current.getCustomPreferenceValue("customerServiceEmail") || "no-reply@salesforce.com",
            type: emailHelpers.emailTypes.accountVerification
        };

        emailHelpers.sendEmail(emailObj, "account/accountVerificationEmail", userObject);
    }
}

function handleNewOptinCustomer(newCustomer) {
    var UUIDUtils = require("dw/util/UUIDUtils");
    var Transaction = require("dw/system/Transaction");
    var needsVerification = false;

    if (isAccountValidationActive()) {
        Transaction.wrap(function () {
            newCustomer.profile.custom.doubleOptinUniqueID = UUIDUtils.createUUID();
        });

        session.custom.ResendValidationLink = URLUtils.https("Account-ResendValidation", "t", dw.util.StringUtils.encodeBase64(newCustomer.profile.customerNo)).toString();
        session.custom.TargetLocation = URLUtils.https("Login-AccountValidation").toString();
        needsVerification = true;
    }

    return needsVerification;
}

function addsValidationLinksToSession(profile) {
    if (!empty(profile) && isAccountValidationActive()) {
        var CustomerMgr = require("dw/customer/CustomerMgr");
        var customerObj = CustomerMgr.getCustomerByCustomerNumber(profile.customerNo);
        if (customerObj && customerObj.profile && !empty(customerObj.profile.custom.doubleOptinUniqueID)) {
            session.custom.ResendValidationLink = dw.web.URLUtils.https("Account-ResendValidation", "t", dw.util.StringUtils.encodeBase64(profile.customerNo)).toString();
            session.custom.TargetLocation = dw.web.URLUtils.https("Login-AccountValidation").toString();
            session.custom.accountToValidate = true;
        }
    }
}

module.exports = {
    getLoginRedirectURL: getLoginRedirectURL,
    sendCreateAccountEmail: sendCreateAccountEmail,
    sendPasswordResetEmail: sendPasswordResetEmail,
    sendAccountEditedEmail: sendAccountEditedEmail,
    sendAccountVerificationEmail: sendAccountVerificationEmail,
    handleNewOptinCustomer: handleNewOptinCustomer,
    isAccountValidationActive: isAccountValidationActive,
    allowNonActivatedGuestOrders: allowNonActivatedGuestOrders,
    addsValidationLinksToSession: addsValidationLinksToSession
};
