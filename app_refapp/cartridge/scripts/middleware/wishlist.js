"use strict";

var WishlistHelpers = require("*/cartridge/scripts/features/wishlistHelpers");
var URLUtils = require("dw/web/URLUtils");

function isEnabled(req, res, next) {
    var wishlistEnabled = WishlistHelpers.isWishlistEnabled();

    if (!wishlistEnabled) {
        res.redirect(URLUtils.url("Home-ErrorNotFound"));
    }

    next();
}

function isEnabledAjax(req, res, next) {
    var wishlistEnabled = WishlistHelpers.isWishlistEnabled();

    if (!wishlistEnabled) {
        res.redirect(URLUtils.url("Wishlist-AjaxFail"));
    }

    next();
}

module.exports = {
    isEnabled: isEnabled,
    isEnabledAjax: isEnabledAjax
};
