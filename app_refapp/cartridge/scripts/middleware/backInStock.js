"use strict";

var backInStockHelper = require("*/cartridge/scripts/features/backInStockHelpers");
var URLUtils = require("dw/web/URLUtils");

function isEnabled(req, res, next) {
    var isBackInStockEnabled = backInStockHelper.isBackInStockEnabled();

    if (!isBackInStockEnabled) {
        res.redirect(URLUtils.url("Home-ErrorNotFound"));
    }

    next();
}

function isEnabledAjax(req, res, next) {
    var isBackInStockEnabled = backInStockHelper.isBackInStockEnabled();

    if (!isBackInStockEnabled) {
        res.redirect(URLUtils.url("BackInStock-AjaxFail"));
    }

    next();
}

module.exports = {
    isEnabled: isEnabled,
    isEnabledAjax: isEnabledAjax
};
