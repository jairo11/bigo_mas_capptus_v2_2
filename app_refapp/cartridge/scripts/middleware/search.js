"use strict";

var searchHelper = require("*/cartridge/scripts/helpers/searchHelpers");
var URLUtils = require("dw/web/URLUtils");

function isEnabled(req, res, next) {

    if (!searchHelper.isSearchEnabled() && req.querystring.q) {
        res.redirect(URLUtils.url("Home-Show"));
    }

    next();
}

module.exports = {
    isEnabled: isEnabled,
};
