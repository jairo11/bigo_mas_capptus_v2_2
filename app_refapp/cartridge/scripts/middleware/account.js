"use strict";

var accountHelpers = require("*/cartridge/scripts/helpers/accountHelpers");
var URLUtils = require("dw/web/URLUtils");

function isValidationEnabled(req, res, next) {
    if (!accountHelpers.isAccountValidationActive()) {
        res.redirect(URLUtils.url("Home-Show"));
    }

    next();
}

module.exports = {
    isValidationEnabled: isValidationEnabled
};
