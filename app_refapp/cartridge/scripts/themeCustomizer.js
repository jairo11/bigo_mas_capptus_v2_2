"use strict";
var ContentMgr = dw.content.ContentMgr;

function getActiveThemeId(parsedThemes) {
    var activeId = "";

    Object.keys(parsedThemes).map(function (key) {
        if (parsedThemes[key].active === true) {
            activeId = key;
        }

        return key;
    });

    return activeId;
}

function getPageThemesJSON(themesAssetId) {
    var themeAsset = ContentMgr.getContent(themesAssetId);

    if (!empty(themeAsset.custom.body)) {
        var parsedThemes = JSON.parse(themeAsset.custom.body.markup);
        var activeThemeId = getActiveThemeId(parsedThemes);
        var cssString = "";
        var activeThemeVarsJSON = {};

        if (parsedThemes[activeThemeId]) {
            Object.keys(parsedThemes[activeThemeId].fontfamilies).map(function (
                key
            ) {
                cssString +=
                    key +
                    ":" +
                    parsedThemes[activeThemeId].fontfamilies[key] +
                    ";";
                activeThemeVarsJSON[key] = parsedThemes[activeThemeId].fontfamilies[key];
                return key;
            });
            Object.keys(parsedThemes[activeThemeId].colors).map(function (key) {
                cssString +=
                    key + ":" + parsedThemes[activeThemeId].colors[key] + ";";
                activeThemeVarsJSON[key] = parsedThemes[activeThemeId].colors[key];
                return key;
            });
            Object.keys(parsedThemes[activeThemeId].typography).map(function (
                key
            ) {
                cssString +=
                    key +
                    ":" +
                    parsedThemes[activeThemeId].typography[key] +
                    ";";
                activeThemeVarsJSON[key] = parsedThemes[activeThemeId].typography[key];
                return key;
            });
        }

        return {
            stringThemes: JSON.stringify(parsedThemes),
            activeThemeVarsJSON: activeThemeVarsJSON,
            cssString: cssString
        };
    } else {
        return null;
    }
}

function getComponentStyles(componentId, themesAssetId) {
    var themeAsset = ContentMgr.getContent(themesAssetId);
    var cssString = "";

    if (!empty(themeAsset.custom.body)) {
        var parsedThemes = JSON.parse(themeAsset.custom.body.markup);
        var activeThemeId = getActiveThemeId(parsedThemes);

        if (parsedThemes[activeThemeId]) {
            if (
                parsedThemes[activeThemeId].components[componentId] &&
                parsedThemes[activeThemeId].components[componentId].fontfamilies
            ) {
                Object.keys(
                    parsedThemes[activeThemeId].components[componentId]
                        .fontfamilies
                ).map(function (key) {
                    cssString +=
                        key +
                        ":" +
                        parsedThemes[activeThemeId].components[componentId]
                            .fontfamilies[key] +
                        ";";
                    return key;
                });
            }

            if (
                parsedThemes[activeThemeId].components[componentId] &&
                parsedThemes[activeThemeId].components[componentId].colors
            ) {
                Object.keys(
                    parsedThemes[activeThemeId].components[componentId].colors
                ).map(function (key) {
                    cssString +=
                        key +
                        ":" +
                        parsedThemes[activeThemeId].components[componentId]
                            .colors[key] +
                        ";";
                    return key;
                });
            }

            if (
                parsedThemes[activeThemeId].components[componentId] &&
                parsedThemes[activeThemeId].components[componentId].typography
            ) {
                Object.keys(
                    parsedThemes[activeThemeId].components[componentId]
                        .typography
                ).map(function (key) {
                    cssString +=
                        key +
                        ":" +
                        parsedThemes[activeThemeId].components[componentId]
                            .typography[key] +
                        ";";
                    return key;
                });
            }
        }
    }

    return cssString;
}

module.exports.getPageThemesJSON = getPageThemesJSON;
module.exports.getComponentStyles = getComponentStyles;
