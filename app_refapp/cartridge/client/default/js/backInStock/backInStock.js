"use strict";

function renderNewPageOfItems(pageNumber, spinner, focusElementSelector) {
    var url = $(".backInStockItemCardsData").data("href");
    if (spinner) {
        $.spinner().start();
    }
    var scrollPosition = document.documentElement.scrollTop;
    $.ajax({
        url: url,
        method: "get",
        data: {
            pageNumber: ++pageNumber // NOSONAR
        }
    }).done(function (data) {
        $(".backInStockItemCards").empty();
        $("body .backInStockItemCards").append(data);

        if (focusElementSelector) {
            $(focusElementSelector).focus();
        } else {
            document.documentElement.scrollTop = scrollPosition;
        }
    }).fail(function () {
        $(".more-wl-items").remove();
    });
    $.spinner().stop();
}

function displayErrorMessage($elementAppendTo, msg) {
    if ($(".remove-from-backInStock-messages").length === 0) {
        $elementAppendTo.append(
            "<div class=\"remove-from-backInStock-messages \"></div>"
        );
    }
    $(".remove-from-backInStock-messages")
        .append("<div class=\"remove-from-backInStock-alert text-center alert-danger\">" + msg + "</div>");

    setTimeout(function () {
        $(".remove-from-backInStock-messages").remove();
    }, 3000);
}

module.exports = {
    removeFromWishlist: function () {
        $("body").on("click", ".remove-from-backInStock", function (e) {
            e.preventDefault();
            var url = $(this).data("url");
            var elMyAccount = $(".account-backInStock-item").length;

            if (elMyAccount > 0) {
                $(".backInStock-account-card").spinner().start();
                $.ajax({
                    url: url,
                    type: "get",
                    dataType: "html",
                    success: function (html) {
                        $(".backInStock-account-card>.card").remove();
                        $(".backInStock-account-card").append(html);
                        $(".backInStock-account-card").spinner().stop();
                    },
                    error: function () {
                        var $elToAppend = $(".backInStock-account-card");
                        $elToAppend.spinner().stop();
                        var msg = $elToAppend.data("error-msg");
                        displayErrorMessage($elToAppend, msg);
                    }
                });
            } else {
                $.spinner().start();
                $.ajax({
                    url: url,
                    type: "get",
                    dataType: "json",
                    success: function () {
                        var pageNumber = $(".backInStockItemCardsData").data("page-number") - 1;
                        renderNewPageOfItems(pageNumber, false);
                    },
                    error: function () {
                        $.spinner().stop();
                        var $elToAppendWL = $(".backInStockItemCards");
                        var msg = $elToAppendWL.data("error-msg");
                        displayErrorMessage($elToAppendWL, msg);
                    }
                });
            }
        });
    },
    moreWLItems: function () {
        $("body").on("click", ".more-backInStock-items", function () {
            var pageNumber = $(".backInStockItemCardsData").data("page-number");
            renderNewPageOfItems(pageNumber, true);
        });
    },
};
