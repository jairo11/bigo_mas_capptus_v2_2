"use strict";

var formValidation = require("../components/formValidation");
var notification = require("../components/notification");
var formatters = require("../formatters");

module.exports = {
    login: function () {
        $("form.login").submit(function (e) {
            var form = $(this);
            e.preventDefault();
            var url = form.attr("action");
            form.spinner().start();
            $("form.login").trigger("login:submit", e);
            $.ajax({
                url: url,
                type: "post",
                dataType: "json",
                data: form.serialize(),
                success: function (data) {
                    form.spinner().stop();
                    if (!data.success) {
                        if (data.error && data.error[0] && typeof data.error[0] === "object") {
                            data.error[0] = data.error[0]["msg"] +
                            "<a data-post-obj='" + JSON.stringify(data.error[0]["link"]["postObj"]) + "' href=\""+ data.error[0]["link"]["href"] +"\" class=\"js-action-"+data.error[0]["link"]["type"]+"\">" +
                                data.error[0]["link"]["text"] +
                            "</a>.";
                        }
                        formValidation(form, data);
                        $("form.login").trigger("login:error", data);
                    } else {
                        $("form.login").trigger("login:success", data);
                        location.href = data.redirectUrl;
                    }
                },
                error: function (data) {
                    if (data.responseJSON.redirectUrl) {
                        window.location.href = data.responseJSON.redirectUrl;
                    } else {
                        $("form.login").trigger("login:error", data);
                        form.spinner().stop();
                    }
                }
            });
            return false;
        });
    },

    register: function () {
        $("form.registration").submit(function (e) {
            var form = $(this);
            e.preventDefault();
            var url = form.attr("action");
            form.spinner().start();
            $("form.registration").trigger("login:register", e);
            $.ajax({
                url: url,
                type: "post",
                dataType: "json",
                data: form.serialize(),
                success: function (data) {
                    form.spinner().stop();
                    if (!data.success) {
                        formValidation(form, data);
                    } else {
                        location.href = data.redirectUrl;
                    }
                },
                error: function (err) {
                    if (err.responseJSON.redirectUrl) {
                        window.location.href = err.responseJSON.redirectUrl;
                    } else {
                        notification({
                            target: ".error-messaging",
                            message: err.responseJSON.errorMessage,
                            dismissible: true
                        });
                    }

                    form.spinner().stop();
                }
            });
            return false;
        });
    },

    resetPassword: function () {
        $(".reset-password-form").submit(function (e) {
            var form = $(this);
            e.preventDefault();
            var url = form.attr("action");
            form.spinner().start();
            $(".reset-password-form").trigger("login:register", e);
            $.ajax({
                url: url,
                type: "post",
                dataType: "json",
                data: form.serialize(),
                success: function (data) {
                    form.spinner().stop();
                    if (!data.success) {
                        formValidation(form, data);
                    } else {
                        $(".request-password-title").text(data.receivedMsgHeading);
                        $(".request-password-body").empty()
                            .append("<p>" + data.receivedMsgBody + "</p>");
                        if (!data.mobile) {
                            $("#submitEmailButton").text(data.buttonText)
                                .attr("data-dismiss", "modal");
                        } else {
                            $(".send-email-btn").empty()
                                .html("<a href=\""
                                    + data.returnUrl
                                    + "\" class=\"btn btn-primary btn-block\">"
                                    + data.buttonText + "</a>"
                                );
                        }
                    }
                },
                error: function () {
                    form.spinner().stop();
                }
            });
            return false;
        });
    },

    clearResetForm: function () {
        $("#login .modal").on("hidden.bs.modal", function () {
            $("#reset-password-email").val("");
            $(".modal-dialog .form-control.is-invalid").removeClass("is-invalid");
        });
    },

    requestResetPassword: function () {
        $("form.login").on("click", ".js-action-ajax", function (e) {
            e.preventDefault();
            var $button = $(this);
            var obj = $button.data("post-obj");
            $.ajax({
                url: $button.attr("href"),
                type: "post",
                dataType: "json",
                data: obj,
                success: function (data) {
                    if (data.redirectUrl) {
                        location.href = data.redirectUrl;
                    }
                },
                error: function (err) {
                    if (err.responseJSON.redirectUrl) {
                        window.location.href = err.responseJSON.redirectUrl;
                    }
                }
            });
            return false;
        });
    },

    initFormatters: formatters.init
};
