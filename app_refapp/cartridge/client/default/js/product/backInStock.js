"use strict";

function displayMessage(data, button) {
    $.spinner().stop();
    var status;
    if (data.success) {
        status = "alert-success";
    } else {
        status = "alert-danger";
    }

    if ($(".add-to-backInStock-messages").length === 0) {
        $("body").append(
            "<div class=\"add-to-backInStock-messages \"></div>"
        );
    }
    $(".add-to-backInStock-messages")
        .append("<div class=\"add-to-backInStock-alert text-center " + status + "\">" + data.msg + "</div>");

    setTimeout(function () {
        $(".add-to-backInStock-messages").remove();
        button.removeAttr("disabled");
    }, 5000);
}


function addBackInStockAlert(url, data, button) {
    $.ajax({
        url: url,
        type: "post",
        dataType: "json",
        data: data,
        success: function (data) { // NOSONAR
            displayMessage(data, button);
        },
        error: function (err) {
            displayMessage(err, button);
        }
    });
}

module.exports = {
    addGuestToBackInStockAlert: function () {
        $(".add-back-in-stock-alert").on("click", function () {
            $.spinner().start();
            var $form = $("#addBackInStockToGuestForm");
            var $button = $(this);
            var pid = $button.closest(".product-detail").find(".product-id").html();
            var email;
            var url;

            if (!pid) {
                return;
            }

            if ($form.length) {
                email = $form.find("input[id='email']").val().trim();
                url = $form.attr("action");
            } else {
                url = $button.data("href");
            }

            addBackInStockAlert(url, {email: email, pid: pid}, $button);

        });
    }
};
