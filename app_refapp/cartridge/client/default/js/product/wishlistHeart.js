"use strict";

const notification = require("../components/notification");

/**
 * appends params to a url
 * @param {string} data - data returned from the server's ajax call
 * @param {Object} $icon - icon that was clicked to add a product to the wishlist
 */
function displayMessage(data) {
    notification({
        message: data.msg,
        color: data.success ? "alert-success" : "alert-danger",
        classes: "add-to-wishlist-alert text-center affix",
        dismissTime: 5000
    });
}

module.exports = {
    operateWishlist: function () {
        $("body").on("click", ".wishlist-cta", function () {
            const $this = $(this);
            const pid = $this.closest(".product").data("pid");
            const optionId = $this.closest(".product-detail").find(".product-option").attr("data-option-id");
            const optionVal = $this.closest(".product-detail").find(".options-select option:selected").attr("data-value-id");
            let isInWishlist = $this.data("isInWishlist");
            const endpointUrl = isInWishlist ? $this.data("removeUrl") : $this.data("addUrl");
            // TODO: there are inconsistencies in BD code for add/remove wishlist item. Both should be POST methods.
            const requestType = isInWishlist ? "get" : "post";
            if (!endpointUrl || !pid) {
                return;
            }

            $.spinner().start();
            $.ajax({
                url: endpointUrl,
                type: requestType,
                dataType: "json",
                data: { pid, optionId, optionVal },
                success: function (data) {
                    isInWishlist = !isInWishlist;
                    displayMessage(data);
                    if (data.success) {
                        // also account for product tile duplicates in product sliders
                        // and other modules that make duplicate product tiles to appear
                        const $productTileWishlistCtas = $(`.product[data-pid="${pid}"] .wishlist-cta`);
                        const $productTileIcons = $productTileWishlistCtas.find(".wishlist-icon--js");

                        $productTileWishlistCtas.data("isInWishlist", isInWishlist);
                        $productTileIcons
                            .removeClass("acf-icon-heart acf-icon-heart-empty")
                            .addClass(isInWishlist ? "acf-icon-heart" : "acf-icon-heart-empty");
                    }
                },
                error: function (err) {
                    displayMessage(err);
                },
                complete() {
                    $.spinner().stop();
                }
            });
        });
    }
};
