"use strict";

(function () {
    var Glide = require("@glidejs/glide/dist/glide.min.js");

    if (!window.recommendationsProductsGlideInitialized) {
        const carousels = $(".up-4-products-carousel-glide");

        carousels.each(function (index, carousel) {
            const slider = new Glide("." + $(carousel).data().id, {
                type: "carousel",
                breakpoints: {
                    focusAt: "center",
                    767: { perView: 1, peek: { before: 90, after: 90 } },
                    1023: { perView: 1, peek: { before: 220, after: 220 } },
                    10000: { perView: 4, peek: 0 }
                }
            });

            slider.mount();
        });

        window.recommendationsProductsGlideInitialized = true;
    }
})();
