"use strict";

var notification = require("../components/notification");

/**
 * appends params to a url
 * @param {string} data - data returned from the server's ajax call
 * @param {Object} $button - button that was clicked to add a product to the wishlist
 */
function displayMessage(data) {
    notification({
        message: data.msg,
        color: data.success ? "alert-success" : "alert-danger",
        classes: "add-to-wishlist-alert text-center affix",
        dismissTime: 5000
    });
}

module.exports = {
    operateWishlist: function () {
        $(".pdp-wishlist-cta").on("click", function () {
            const $this = $(this);
            const $icon = $this.find(".wishlist-icon--js");
            const $ctaText = $this.find(".wishlist-cta-text--js");
            var pid = $this.find("input[name='productID']").val();
            var optionId = $this.closest(".product-detail").find(".product-option").attr("data-option-id");
            var optionVal = $this.closest(".product-detail").find(".options-select option:selected").attr("data-value-id");

            let isInWishlist = $this.data("isInWishlist");
            const endpointUrl = isInWishlist ? $this.data("removeUrl") : $this.data("addUrl");
            // TODO: there are inconsistencies in BD code for add/remove wishlist item. Both should be POST methods.
            const requestType = isInWishlist ? "get" : "post";
            if (!endpointUrl || !pid) {
                return;
            }

            $.spinner().start();
            $.ajax({
                url: endpointUrl,
                type: requestType,
                dataType: "json",
                data: {
                    pid: pid,
                    optionId: optionId,
                    optionVal: optionVal
                },
                success: function (data) {
                    isInWishlist = !isInWishlist;
                    displayMessage(data);
                    if (data.success) {
                        $this.data("isInWishlist", isInWishlist);
                        $icon
                            .removeClass("acf-icon-heart acf-icon-heart-empty")
                            .addClass(isInWishlist ? "acf-icon-heart" : "acf-icon-heart-empty");
                        $ctaText.text($this.data(isInWishlist ? "removeText" : "addText"));
                    }
                },
                error: function (err) {
                    displayMessage(err);
                },
                complete() {
                    $.spinner().stop();
                }
            });
        });
    }
};
