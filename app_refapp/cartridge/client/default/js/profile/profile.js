"use strict";

var formValidation = require("../components/formValidation");
var notification = require("../components/notification");
var formatters = require("../formatters");

module.exports = {
    submitProfile: function () {
        var $form = $("form.edit-profile-form");

        $form.submit(function (e) {
            e.preventDefault();
            var url = $form.attr("action");
            $form.spinner().start();
            $("form.edit-profile-form").trigger("profile:edit", e);
            $.ajax({
                url: url,
                type: "post",
                dataType: "json",
                data: $form.serialize(),
                success: function (data) {
                    $form.spinner().stop();
                    if (!data.success) {
                        formValidation($form, data);
                    } else {
                        notification({
                            message: $form.data("notification-success"),
                            color: "alert-success",
                            classes: "alert-top text-center",
                            dismissTime: 5000,
                            callback: function () {
                                location.href = data.redirectUrl;
                            }
                        });
                    }
                },
                error: function (err) {
                    if (err.responseJSON.redirectUrl) {
                        window.location.href = err.responseJSON.redirectUrl;
                    }
                    $form.spinner().stop();
                }
            });
            return false;
        });

        $form.find("#password").on("keyup", function (e) {
            $form.find(".btn-save").prop("disabled", !e.currentTarget.validity.valid);
        });
    },

    submitPassword: function () {
        $("form.change-password-form").submit(function (e) {
            var $form = $(this);
            e.preventDefault();
            var url = $form.attr("action");
            $form.spinner().start();
            $("form.change-password-form").trigger("password:edit", e);
            $.ajax({
                url: url,
                type: "post",
                dataType: "json",
                data: $form.serialize(),
                success: function (data) {
                    $form.spinner().stop();
                    if (!data.success) {
                        formValidation($form, data);
                    } else {
                        location.href = data.redirectUrl;
                    }
                },
                error: function (err) {
                    if (err.responseJSON.redirectUrl) {
                        window.location.href = err.responseJSON.redirectUrl;
                    }
                    $form.spinner().stop();
                }
            });
            return false;
        });
    },

    initFormatters: formatters.init
};
