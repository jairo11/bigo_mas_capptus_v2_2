"use strict";
var _ = require("lodash");

var util = {
    /**
     * appends the parameter with the given name and value to the given url and returns the changed url
     * @param {String} url the url to which the parameter will be added
     * @param {String} name the name of the parameter
     * @param {String} value the value of the parameter
     */
    appendParamToURL: function (url, name, value) {
        if (url.indexOf(name + "=") !== -1) {
            return url;
        }
        var separator = url.indexOf("?") !== -1 ? "&" : "?";
        return url + separator + name + "=" + encodeURIComponent(value);
    },

    /**
     * appends the parameters to the given url and returns the changed url
     * @param {String} url the url to which the parameters will be added
     * @param {Object} params
     */
    appendParamsToUrl: function (url, params) {
        var _url = url;
        _.each(params, function (value, name) {
            _url = this.appendParamToURL(_url, name, value);
        }.bind(this));
        return _url;
    }

};

module.exports = util;

