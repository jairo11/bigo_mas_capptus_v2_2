"use strict";

const InputMask = require("inputmask");

const formatters = {
    initPhoneNumbersFormatter() {
        const phoneFields = document.querySelectorAll("input#shippingPhoneNumberdefault, input#phoneNumber, input#phone, input#registration-form-phone, input[id*='PhoneNumber']");
        InputMask("999-999-9999").mask(phoneFields);
    },
    init() {
        formatters.initPhoneNumbersFormatter();
    }
};

module.exports = formatters;
