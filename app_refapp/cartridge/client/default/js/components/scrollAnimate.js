"use strict";

module.exports = function (element, offset) {
    var position = element && element.length ? element.offset().top : 0;
    if (offset) {
        position -= offset;
    }

    $("html, body").animate({
        scrollTop: position
    }, 500);

    if (!element) {
        $(".logo-home").focus();
    }
};
