"use strict";

// eslint-disable-next-line no-unused-vars
const xzoom = require("xzoom");

/**
 * @function
 * @description Generate options, that will allow to customize the zoom functionality.
 * @see https://github.com/payalord/xZoom/blob/master/doc/manual.md for options available
 * @returns {Object} combined development options - available only in the JS code and options that comes from Site Preferences.
 */
function getZoomOptions() {
    /* Option accessible from the code only for development purpose */
    const defaulOptions = {
        zoomWidth: "auto",
        zoomHeight: "auto",
        sourceClass: "xzoom-source",
        loadingClass: "xzoom-loading",
        lensClass: "xzoom-lens",
        zoomClass: "xzoom-preview",
        activeClass: "xactive",
        titleClass: "xzoom-caption"
    };

    /* Option accessible from BM configuration for customer customizations */
    const userOptions = ($(".zoom-container").length && $(".zoom-container").attr("data-zoom-configs")) ? JSON.parse($(".zoom-container").attr("data-zoom-configs")) : {};

    return Object.assign({}, defaulOptions, userOptions);
}

/**
 * @public
 * @function
 * @description Initialize Zoom instances. Iterate through all images to cover Product Sets/Bundles.
 */
function createZoomInstance() {
    const options = getZoomOptions();

    $(".active .zoom-image").each(function () {
        $(this).xzoom(options);
    });
}

/* When the zoom plugin is called, the images need to be loaded in order for the dimensions to be calculated */
module.exports = function () {
    createZoomInstance();

    /* Call Zoom instance after PDP is updated */
    $("body").on("product:statusUpdate, product:afterAttributeSelect, product:updateAddToCart, product:updateAvailability", createZoomInstance);

    /* Call Zoom instance after QuickView is open */
    $("body").on("shown.bs.modal", "#quickViewModal, #editProductModal", createZoomInstance);

    /**
        Call Zoom instance for active carousel slide.
        @todo ACF4 After carousel refactoring, update with chosen plugin event.
    */
    $("body").on("slid.bs.carousel", ".carousel", createZoomInstance);
};
