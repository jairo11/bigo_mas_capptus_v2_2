"use strict";

const debounce = require("lodash/debounce");
const scrollAnimate = require("./scrollAnimate");
const thresholdTime = 5;

/**
 * @function
 * @description Add to header element data-toggle="affix". Function uses offset to define when to toggle the pinning of an element.
 * @param {Object} affixElement
 * @param {Object} scrollElement
 * @param {Object} affixWrapper
 * @see https://getbootstrap.com/docs/3.4/javascript/#affix
 */

function toggleAffix(affixElement, scrollElement, affixWrapper) {
    const height = affixElement.outerHeight(),
        top = affixWrapper.offset().top;

    if (scrollElement.scrollTop() >= top) {
        affixElement.addClass("affix").trigger("on.bs.affix");
        affixWrapper.height(height);
    } else {
        affixElement.removeClass("affix").trigger("off.bs.affix");
        affixWrapper.height("auto");
    }
}

module.exports = function () {
    $("[data-toggle='affix']").each(function () {
        let $element = $(this),
            $wrapper = $("<div></div>");

        $element.before($wrapper);

        $(window).on("scroll resize", debounce(() => {
            toggleAffix($element, $(window), $wrapper);
        }, thresholdTime));

        toggleAffix($element, $(window), $wrapper);
    });

    $("[data-scroll='affix']").on("click", function (e) {
        e.preventDefault();
        let target = $(this).attr("href");
        let offset = $(this).attr("data-offset");
        scrollAnimate($(target), offset);
    });

    const urlParams = new URLSearchParams(location.search);

    if (urlParams.has("affix")) {
        let target = "#" + urlParams.get("affix");
        let offset = urlParams.has("offset") ? urlParams.get("offset") : 0;
        scrollAnimate($(target), offset);
    }
};
