"use strict";

window.jQuery = window.$ = require("jquery");
require("mq-respond");
var processInclude = require("./util");
var cssVars = require("css-vars-ponyfill");

cssVars.default({
    include: "[data-cssvars]"
});

window.mqRespond = new window.mqRes([
    {
        label: "phone",
        from: 0,
        to: 479
    },
    {
        label: "phoneLandscape",
        from: 480,
        to: 767
    },
    {
        label: "tablet",
        from: 768,
        to: 1023
    },
    {
        label: "desktop",
        from: 1024,
        to: 1679
    },
    {
        label: "desktopLarge",
        from: 1923
    }
]);

$(document).ready(function () {
    processInclude(require("./components/menu"));
    processInclude(require("./components/cookie"));
    processInclude(require("./components/consentTracking"));
    processInclude(require("./components/footer"));
    processInclude(require("./components/miniCart"));
    processInclude(require("./components/collapsibleItem"));
    processInclude(require("./components/search"));
    processInclude(require("./components/clientSideValidation"));
    processInclude(require("./components/countrySelector"));
    processInclude(require("./components/toolTip"));
    processInclude(require("./components/affix"));
});

require("./thirdParty/bootstrap");
require("./components/spinner");
