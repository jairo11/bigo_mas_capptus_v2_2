/* globals google */
"use strict";

/**
 * Uses google maps api to render a map
 */
function maps() {
    var map;
    var infowindow = new google.maps.InfoWindow();

    // Init U.S. Map in the center of the viewport
    var mapOptions = {
        zoom: 15
    };
    var fillColor = getComputedStyle(document.documentElement)
            .getPropertyValue("--background-secondary"),
        textColor = getComputedStyle(document.documentElement)
            .getPropertyValue("--text-primary");

    // Customized google map marker icon with svg format
    var markerImg = {
        path: "M9.64387 0C7.97426 0.000194842 6.3333 0.433796 4.88153 1.25838C3.42975 2.08297 2.2169 3.27029 1.36161 4.7042C0.50633 6.1381 0.0379201 7.76947 0.0022072 9.43869C-0.0335057 11.1079 0.364698 12.7578 1.15787 14.227L9.11687 28.621C9.16836 28.716 9.24456 28.7953 9.3374 28.8506C9.43025 28.9059 9.53631 28.9351 9.64437 28.9351C9.75243 28.9351 9.85849 28.9059 9.95133 28.8506C10.0442 28.7953 10.1204 28.716 10.1719 28.621L18.1339 14.221C18.9255 12.7517 19.3223 11.1021 19.2855 9.43353C19.2487 7.76495 18.7797 6.13446 17.9241 4.70143C17.0686 3.2684 15.8559 2.08186 14.4046 1.2578C12.9532 0.43374 11.3129 0.000352414 9.64387 0V0ZM9.64387 14.466C8.69021 14.4658 7.75802 14.1828 6.96518 13.6529C6.17234 13.1229 5.55445 12.3697 5.18964 11.4886C4.82483 10.6075 4.72947 9.63797 4.91564 8.70266C5.10182 7.76735 5.56115 6.90825 6.23556 6.23398C6.90997 5.55971 7.76917 5.10056 8.70452 4.91458C9.63986 4.7286 10.6094 4.82415 11.4904 5.18915C12.3715 5.55414 13.1245 6.17219 13.6543 6.96514C14.1841 7.7581 14.4669 8.69034 14.4669 9.644C14.4653 10.9226 13.9566 12.1483 13.0524 13.0523C12.1483 13.9563 10.9224 14.4647 9.64387 14.466Z",
        anchor: new google.maps.Point(0, 0),
        fillColor: fillColor,
        fillOpacity: 1,
        scale: 1,
        strokeColor: textColor,
        strokeWeight: 1,
        labelOrigin: new google.maps.Point(10, 50)
    };

    var $canvases = $(".map-canvas");
    $canvases.each(function (index) {
        var $canvas = $($canvases[index]);
        var storeData = $canvas.attr("data-store-data");

        storeData = storeData ? JSON.parse(storeData) : {};
        if (!storeData.latitude || !storeData.longitude) {
            return;
        }
        mapOptions.center = { lat: storeData.latitude, lng: storeData.longitude };
        map = new google.maps.Map($canvases[index], mapOptions);

        var label = storeData.name;

        var marker = new google.maps.Marker({
            position: {lat: storeData.latitude, lng: storeData.longitude},
            map: map,
            title: label,
            icon: markerImg,
            label: { text: label, color: textColor, fontSize: "16px", y: "100px" }
        });

        marker.addListener("click", function () {
            infowindow.setOptions({
                content: storeData.storeInfoPop || ""
            });
            infowindow.open(map, marker);
        });
    });
    //toggle display of store hours
    $(".store-hours-container").on("click", function () {
        $(this).toggleClass("accordion-opened");
    });
}

function initSlider() {
    var Glide = require("@glidejs/glide/dist/glide.min.js");

    const slider = new Glide(".nearby-stores-mobile", {
        type: "slider",
        startAt: 1,
        breakpoints: {
            focusAt: "center",
            425: { perView: 1, peek: { before: 35, after: 35 }},
            767: { perView: 1, peek: { before: 70, after: 70 }},
            10000: { perView: 1, peek: { before: 180, after: 180 } }
        }
    });

    slider.mount();
}

module.exports = {
    init: function () {
        if ($(".store-details-page").data("has-google-api")) {
            maps();
            initSlider();
        } else {
            $(".store-locator-no-apiKey").show();
        }
    }
};
