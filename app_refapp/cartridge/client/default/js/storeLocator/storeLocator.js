/* globals google */
"use strict";

var fuse = require("fuse.js");

/**
 * appends params to a url
 * @param {string} url - Original url
 * @param {Object} params - Parameters to append
 * @returns {string} result url with appended parameters
 */
function appendToUrl(url, params) {
    var newUrl = url;
    newUrl += (newUrl.indexOf("?") !== -1 ? "&" : "?") + Object.keys(params).map(function (key) {
        return key + "=" + encodeURIComponent(params[key]);
    }).join("&");

    return newUrl;
}

/**
 * Uses google maps api to render a map
 */
function maps() {
    var map;
    var infowindow = new google.maps.InfoWindow();
    var centerBox = $(".js-center-box").data("center-box");

    // Init U.S. Map in the center of the viewport
    var latlng = new google.maps.LatLng(centerBox.centerLat, centerBox.centerLng);
    var mapOptions = {
        scrollwheel: false,
        zoom: 4,
        center: latlng
    };

    map = new google.maps.Map($(".map-canvas")[0], mapOptions);
    var mapdiv = $(".map-canvas").attr("data-locations");

    mapdiv = JSON.parse(mapdiv);

    var bounds = new google.maps.LatLngBounds();
    var fillColor = getComputedStyle(document.documentElement)
            .getPropertyValue("--background-secondary"),
        textColor = getComputedStyle(document.documentElement)
            .getPropertyValue("--text-primary");

    // Customized google map marker icon with svg format
    var markerImg = {
        path: "M9.64387 0C7.97426 0.000194842 6.3333 0.433796 4.88153 1.25838C3.42975 2.08297 2.2169 3.27029 1.36161 4.7042C0.50633 6.1381 0.0379201 7.76947 0.0022072 9.43869C-0.0335057 11.1079 0.364698 12.7578 1.15787 14.227L9.11687 28.621C9.16836 28.716 9.24456 28.7953 9.3374 28.8506C9.43025 28.9059 9.53631 28.9351 9.64437 28.9351C9.75243 28.9351 9.85849 28.9059 9.95133 28.8506C10.0442 28.7953 10.1204 28.716 10.1719 28.621L18.1339 14.221C18.9255 12.7517 19.3223 11.1021 19.2855 9.43353C19.2487 7.76495 18.7797 6.13446 17.9241 4.70143C17.0686 3.2684 15.8559 2.08186 14.4046 1.2578C12.9532 0.43374 11.3129 0.000352414 9.64387 0V0ZM9.64387 14.466C8.69021 14.4658 7.75802 14.1828 6.96518 13.6529C6.17234 13.1229 5.55445 12.3697 5.18964 11.4886C4.82483 10.6075 4.72947 9.63797 4.91564 8.70266C5.10182 7.76735 5.56115 6.90825 6.23556 6.23398C6.90997 5.55971 7.76917 5.10056 8.70452 4.91458C9.63986 4.7286 10.6094 4.82415 11.4904 5.18915C12.3715 5.55414 13.1245 6.17219 13.6543 6.96514C14.1841 7.7581 14.4669 8.69034 14.4669 9.644C14.4653 10.9226 13.9566 12.1483 13.0524 13.0523C12.1483 13.9563 10.9224 14.4647 9.64387 14.466Z",
        fillColor: fillColor,
        fillOpacity: 1,
        scale: 1.1,
        strokeColor: textColor,
        strokeWeight: 1,
        anchor: new google.maps.Point(13, 30),
        labelOrigin: new google.maps.Point(12, 12)
    };

    Object.keys(mapdiv).forEach(function (key) {
        var item = mapdiv[key];
        var lable = parseInt(key, 10) + 1;
        var storeLocation = new google.maps.LatLng(item.latitude, item.longitude);
        var marker = new google.maps.Marker({
            position: storeLocation,
            map: map,
            title: item.name,
            icon: markerImg,
            label: { text: lable.toString(), color: textColor, fontSize: "16px" }
        });

        marker.addListener("click", function () {
            infowindow.setOptions({
                content: item.infoWindowHtml
            });
            infowindow.open(map, marker);
        });

        // Create a minimum bound based on a set of storeLocations
        bounds.extend(marker.position);
    });
    // Fit the all the store marks in the center of a minimum bounds when any store has been found.
    if (mapdiv && mapdiv.length !== 0) {
        map.fitBounds(bounds);
    }
}

/**
 * Renders the results of the search and updates the map
 * @param {Object} data - Response from the server
 */
function updateStoresResults(data) {
    var $resultsDiv = $(".results");
    var $mapDiv = $(".map-canvas");
    var hasResults = data.stores.length > 0;

    if (!hasResults) {
        $(".store-locator-no-results").show();
    } else {
        $(".store-locator-no-results").hide();
    }

    $resultsDiv.empty()
        .data("has-results", hasResults)
        .data("radius", data.radius)
        .data("search-key", data.searchKey);

    $mapDiv.attr("data-locations", data.locations);

    if ($mapDiv.data("has-google-api")) {
        maps();
    } else {
        $(".store-locator-no-apiKey").show();
    }

    if (data.storesResultsHtml) {
        $resultsDiv.append(data.storesResultsHtml);
    }
}

function updateStoresList(stores) {
    window.stores = stores;

    window.fuzzySearch = new fuse.default(window.stores, {
        keys: ["name", "city", "postalCode"],
        threshold: 0.4
    });
}

function listenForSearchInput() {
    $(".store-locator-search-input").on("input", function () {
        var searchResults = window.fuzzySearch.search($(this).val());

        $("#search-input-autocomlete-list").html("");

        searchResults.forEach(function (store) {
            if (store.item.name && store.item.postalCode && store.item.city) {
                $("#search-input-autocomlete-list").append($("<li data-store-id=" + store.item.ID + " data-store-longitude=" + store.item.longitude + " data-store-latitude=" + store.item.latitude + ">" + store.item.name + "(" + store.item.city + " " + store.item.postalCode + ")" + "</li>"));
            }
        });

        if (window.storesAutocomplete) {
            window.storesAutocomplete.destroy();
        }

        // eslint-disable-next-line
        window.storesAutocomplete = new Awesomplete(document.querySelector(".store-locator-search-input"), {list: "#search-input-autocomlete-list"});
        $(".store-locator-search-input").on("awesomplete-selectcomplete", function () {
            window.storesAutocomplete.close();
        });
    });
}

function getGeolocationFromFuzzySearchData($form) {
    var searchTerm = $form.find("[name=\"storeSearch\"]").val().trim(),
        $storesToCheck = $("#search-input-autocomlete-list li"),
        result = {},
        hadPerfectMatch = false;

    $storesToCheck.each(function (index, storeItem) {
        if (searchTerm == $(storeItem).html().trim()) {
            hadPerfectMatch = true;
            result.id = $(storeItem).data("store-id");
            result.lat = $(storeItem).data("store-latitude");
            result.long = $(storeItem).data("store-longitude");
        }
    });

    if (!hadPerfectMatch && $storesToCheck.length) {
        result.id = $($storesToCheck[0]).data("store-id");
        result.lat = $($storesToCheck[0]).data("store-latitude");
        result.long = $($storesToCheck[0]).data("store-longitude");
    }

    return result;
}

/**
 * Search for stores with new zip code
 * @param {HTMLElement} element - the target html element
 * @returns {boolean} false to prevent default event
 */
function search(element) {
    var dialog = element.closest(".in-store-inventory-dialog");
    var spinner = dialog.length ? dialog.spinner() : $.spinner();
    spinner.start();
    var $form = element.closest(".store-locator");

    var radius = $(".results").data("radius") || 64000;
    var url = $form.attr("action");
    var urlParams = { radius: radius };
    var geolocationData = getGeolocationFromFuzzySearchData($form);

    var payload = geolocationData;

    url = appendToUrl(url, urlParams);

    $.ajax({
        url: url,
        type: $form.attr("method"),
        data: payload,
        dataType: "json",
        success: function (data) {
            spinner.stop();
            updateStoresResults(data);
            $(".select-store").prop("disabled", true);
        }
    });
    return false;
}

module.exports = {
    init: function () {
        listenForSearchInput();

        if ($(".map-canvas[data-has-google-api]").data("has-google-api")) {
            maps();
        } else {
            $(".store-locator-no-apiKey").show();
        }

        if (!$(".results").data("has-results")) {
            $(".store-locator-no-results").show();
        }

        //toggle display of store hours
        $("body").on("click", ".store-hours-container", function () {
            $(this).toggleClass("accordion-opened");
        });
    },

    detectLocation: function () {
        // clicking on detect location.
        var $detectLocationButton = $(".detect-location");
        var url = $detectLocationButton.data("action");
        var centerBox = $(".js-center-box").data("center-box");

        var urlParams = {
            radius: $(".results").data("radius"),
        };

        $.spinner().start();
        if (!navigator.geolocation) {
            $.spinner().stop();
            return;
        }

        navigator.geolocation.getCurrentPosition(function (position) {
            urlParams.lat = position.coords.latitude;
            urlParams.long = position.coords.longitude;
        }, function () {
            urlParams.lat = centerBox.centerLat;
            urlParams.long = centerBox.centerLng;
            $.spinner().stop();
        });

        url = appendToUrl(url, urlParams);

        $.ajax({
            url: url,
            type: "get",
            dataType: "json",
            success: function (data) {
                $.spinner().stop();
                updateStoresList(data.stores);
                updateStoresResults(data);
                $(".select-store").prop("disabled", true);
            }
        });
    },

    search: function () {
        $(".store-locator-container form.store-locator").submit(function (e) {
            e.preventDefault();
            search($(this));
        });
        $(".store-locator-container .btn-storelocator-search[type=\"button\"]").click(function (e) {
            e.preventDefault();
            search($(this));
        });
    },

    changeRadius: function () {
        $(".store-locator-container .radius").change(function () {
            var radius = $(this).val();
            var searchKeys = $(".results").data("search-key");
            var url = $(this).data("action-url");
            var urlParams = {};

            if (searchKeys.postalCode) {
                urlParams = {
                    radius: radius,
                    postalCode: searchKeys.postalCode
                };
            } else if (searchKeys.lat && searchKeys.long) {
                urlParams = {
                    radius: radius,
                    lat: searchKeys.lat,
                    long: searchKeys.long
                };
            }

            url = appendToUrl(url, urlParams);
            var dialog = $(this).closest(".in-store-inventory-dialog");
            var spinner = dialog.length ? dialog.spinner() : $.spinner();
            spinner.start();

            $.ajax({
                url: url,
                type: "get",
                dataType: "json",
                success: function (data) {
                    spinner.stop();
                    updateStoresResults(data);
                    $(".select-store").prop("disabled", true);
                }
            });
        });
    },
    selectStore: function () {
        $(".store-locator-container").on("click", ".select-store", (function (e) {
            e.preventDefault();
            var selectedStore = $(":checked", ".results-card .results");
            var data = {
                storeID: selectedStore.val(),
                searchRadius: $("#radius").val(),
                searchPostalCode: $(".results").data("search-key").postalCode,
                storeDetailsHtml: selectedStore.siblings("label").find(".store-details").html(),
                event: e
            };

            $("body").trigger("store:selected", data);
        }));
    },
    updateSelectStoreButton: function () {
        $("body").on("change", ".select-store-input", (function () {
            $(".select-store").prop("disabled", false);
        }));
    }
};
