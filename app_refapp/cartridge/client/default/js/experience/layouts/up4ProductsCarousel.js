"use strict";

(function () {
    var Glide = require("@glidejs/glide/dist/glide.min.js");

    if (!window.up4ProductsGlideInitialized) {
        const carousels = $(".up-4-products-carousel-glide");

        carousels.each(function (index, carousel) {
            var selector = "." + $(carousel).data().id;
            var $slidesLength = $(selector + " .glide__slide").length;
            const slider = new Glide("." + $(carousel).data().id, {
                type: "carousel",
                breakpoints: {
                    10000: { perView: 4 }
                }
            });

            if ($slidesLength) {
                slider.mount();
            }
        });

        window.up4ProductsGlideInitialized = true;
    }


})();
