"use strict";

(function () {
    var Glide = require("@glidejs/glide/dist/glide.min.js");

    if (!window.up5ItemsGlideInitialized) {
        const carousels = $(".up-5-items-carousel-glide");

        carousels.each(function (index, carousel) {
            var selector = "." + $(carousel).data().id;
            var $slidesLength = $(selector + " .glide__slide").length;
            const slider = new Glide(selector, {
                type: "carousel",
                breakpoints: {
                    1023: { perView: 3, gap: 20 },
                    1923: { perView: 5, gap: 20 },
                    10000: { perView: 5, gap: 40 }
                }
            });

            if ($slidesLength) {
                slider.mount();
            }
        });

        window.up5ItemsGlideInitialized = true;
    }
})();
