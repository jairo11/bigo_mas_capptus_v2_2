"use strict";

const { Loader } = require("google-maps");

function getMarkerOverlayMarkup(markerData) {
    return `
        <div class="google-map__marker gmap-marker">
            <p class="gmap-marker__name">${markerData.name}</p>
            <p class="gmap-marker__phone">
                <a href="tel:${markerData.phoneFormatted}">${markerData.translations.phone}: ${markerData.phone}</a>
            </p>
        </div>
    `;
}

function initModuleInstance($root, markerImgConfig, googleLibInstance) {
    let marker;

    const infoWindow = new googleLibInstance.maps.InfoWindow();
    const mapBounds = new googleLibInstance.maps.LatLngBounds();
    const markersDataList = $root
        .find(".google-map__marker-data")
        .map(function () {
            const data = $(this).val();
            return JSON.parse(decodeURI(data));
        })
        .get();

    const gMap = new googleLibInstance.maps.Map($root.find(".google-map").get(0));

    markersDataList.forEach(markerData => {
        const markerCoords = new googleLibInstance.maps.LatLng(Number(markerData.lat), Number(markerData.lng));

        marker = new googleLibInstance.maps.Marker({
            position: markerCoords,
            map: gMap,
            title: markerData.name,
            icon: markerImgConfig,
            label: {
                text: markerData.name,
                color: textColor,
                fontSize: "16px"
            }
        });
        mapBounds.extend(marker.position);
        marker.addListener("click", function () {
            infoWindow.setOptions({
                content: getMarkerOverlayMarkup(markerData)
            });
            infoWindow.open(gMap, marker);
        });
    });

    googleLibInstance.maps.event.addListenerOnce(gMap, "idle", function () {
        gMap.fitBounds(mapBounds);
        gMap.setZoom(14);
    });
}

// api key is ensured to be present, ptherwise this js file(asset) is not included in
// cartridges/app_theme_1/cartridge/templates/default/experience/components/layouts/googleMap.isml
const gmapApiKey = $(".google-map-api-key").val();
const fillColor = getComputedStyle(document.documentElement)
    .getPropertyValue("--background-secondary");
const textColor = getComputedStyle(document.documentElement)
    .getPropertyValue("--text-primary");

const googleApiLoader = new Loader(gmapApiKey);

(async function () {
    const googleLibInstance = window.google || await googleApiLoader.load();
    // TODO: marker config is hard-coded
    const markerImgConfig = {
        path: "M9.64387 0C7.97426 0.000194842 6.3333 0.433796 4.88153 1.25838C3.42975 2.08297 2.2169 3.27029 1.36161 4.7042C0.50633 6.1381 0.0379201 7.76947 0.0022072 9.43869C-0.0335057 11.1079 0.364698 12.7578 1.15787 14.227L9.11687 28.621C9.16836 28.716 9.24456 28.7953 9.3374 28.8506C9.43025 28.9059 9.53631 28.9351 9.64437 28.9351C9.75243 28.9351 9.85849 28.9059 9.95133 28.8506C10.0442 28.7953 10.1204 28.716 10.1719 28.621L18.1339 14.221C18.9255 12.7517 19.3223 11.1021 19.2855 9.43353C19.2487 7.76495 18.7797 6.13446 17.9241 4.70143C17.0686 3.2684 15.8559 2.08186 14.4046 1.2578C12.9532 0.43374 11.3129 0.000352414 9.64387 0V0ZM9.64387 14.466C8.69021 14.4658 7.75802 14.1828 6.96518 13.6529C6.17234 13.1229 5.55445 12.3697 5.18964 11.4886C4.82483 10.6075 4.72947 9.63797 4.91564 8.70266C5.10182 7.76735 5.56115 6.90825 6.23556 6.23398C6.90997 5.55971 7.76917 5.10056 8.70452 4.91458C9.63986 4.7286 10.6094 4.82415 11.4904 5.18915C12.3715 5.55414 13.1245 6.17219 13.6543 6.96514C14.1841 7.7581 14.4669 8.69034 14.4669 9.644C14.4653 10.9226 13.9566 12.1483 13.0524 13.0523C12.1483 13.9563 10.9224 14.4647 9.64387 14.466Z",
        fillColor: fillColor,
        fillOpacity: 1,
        scale: 1.1,
        strokeColor: textColor,
        strokeWeight: 1,
        anchor: new googleLibInstance.maps.Point(13, 30),
        labelOrigin: new googleLibInstance.maps.Point(12, 40)
    };

    const $mapsList = $(".google-map__wrapper");

    $mapsList.each(function () {
        initModuleInstance($(this), markerImgConfig, googleLibInstance);
    });
})();
