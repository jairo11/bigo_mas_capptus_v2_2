"use strict";

(function () {
    var Glide = require("@glidejs/glide/dist/glide.min.js");

    if (!window.up1ItemGlideInitialized) {
        const carousels = $(".up-1-item-carousel-glide");

        carousels.each(function (index, carousel) {
            var selector = "." + $(carousel).data().id;
            var $slidesLength = $(selector + " .glide__slide").length;
            const slider = new Glide("." + $(carousel).data().id, {
                type: "carousel",
                breakpoints: {
                    10000: { perView: 1 }
                }
            });

            if ($slidesLength) {
                slider.mount();
            }
        });

        window.up1ItemGlideInitialized = true;
    }
})();
