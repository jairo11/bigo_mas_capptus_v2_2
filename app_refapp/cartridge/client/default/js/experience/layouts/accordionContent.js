"use strict";

function initAccordionInstance($root) {
    const isSingleExpanded = $root.is(".accordion-content--single-expanded");
    const $tabsList = $root.find(".accordion-content-tab");

    $root.on("click", ".accordion-content-tab__title", function () {
        const $this = $(this);
        const $currentTab = $this.closest(".accordion-content-tab");

        if (isSingleExpanded) {
            const wasCurrentTabExpanded = $currentTab.is(".accordion-content-tab--expanded");

            $tabsList.removeClass("accordion-content-tab--expanded");
            if (!wasCurrentTabExpanded) {
                $currentTab.addClass("accordion-content-tab--expanded");
            }
        } else {
            $currentTab.toggleClass("accordion-content-tab--expanded");
        }
    });
}

(function () {
    // accordion instances list
    const $accordions = $(".accordion-content");

    $accordions.each(function () {
        initAccordionInstance($(this));
    });
})();
