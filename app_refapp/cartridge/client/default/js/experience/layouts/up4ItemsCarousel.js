"use strict";

(function () {
    var Glide = require("@glidejs/glide/dist/glide.min.js");

    if (!window.up4ItemsGlideInitialized) {
        const carousels = $(".up-4-items-carousel-glide");

        carousels.each(function (index, carousel) {
            var selector = "." + $(carousel).data().id;
            var $slidesLength = $(selector + " .glide__slide").length;
            const slider = new Glide("." + $(carousel).data().id, {
                type: "carousel",
                breakpoints: {
                    1023: { perView: 2 },
                    10000: { perView: 4 }
                }
            });

            if ($slidesLength) {
                slider.mount();
            }
        });

        window.up4ItemsGlideInitialized = true;
    }
})();
