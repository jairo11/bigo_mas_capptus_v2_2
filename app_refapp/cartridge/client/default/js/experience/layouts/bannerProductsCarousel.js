"use strict";

(function () {
    var Glide = require("@glidejs/glide/dist/glide.min.js");

    if (!window.bannerProductsCarouselInitialized) {
        const carousels = $(".banner-products-carousel__carousel-wrapper");

        carousels.each(function (index, carousel) {
            var selector = "." + $(carousel).data().id;
            var $slidesLength = $(selector + " .glide__slide").length;
            const slider = new Glide("." + $(carousel).data().id, {
                type: "carousel",
                breakpoints: {
                    focusAt: "center",
                    767: { perView: 1, peek: { before: 100, after: 100 } },
                    1023: { perView: 1, peek: { before: 220, after: 220 } },
                    10000: { perView: 2, peek: 0 }
                },
                swipeThreshold: $slidesLength <= 1 ? false : true,
                dragThreshold: $slidesLength <= 1 ? false : true
            });

            if ($slidesLength) {
                slider.mount();

                if ($slidesLength <= 1) {
                    $(".banner-products-carousel__carousel-wrapper").addClass("carousel-disabled");
                    slider.destroy();
                }
            }
        });

        window.bannerProductsCarouselInitialized = true;
    }
})();
