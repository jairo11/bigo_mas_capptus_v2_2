"use strict";

/**
 * Display the returned message.
 * @param {string} data - data returned from the server's ajax call
 * @param {Object} $button - button that was clicked for contact us sign-up
 */
function displayMessage(data, $button) {
    var status = data.success ? "alert-success" : "alert-danger";

    if (!$(".contact-us-form__signup-message").length) {
        $(".contact-us-form__wrapper--js").append(
            "<div class=\"contact-us-form__signup-message contact-us-form__signup-message--js\"></div>"
        );
    }
    $(".contact-us-form__signup-message--js")
        .append("<div class=\"contact-us-form__signup-alert text-center " + status + "\" role=\"alert\">" + data.msg + "</div>");

    setTimeout(function () {
        $(".contact-us-form__signup-message--js").remove();
        $button.removeAttr("disabled");
    }, 3000);
}

(function () {
    $(".contact-us-form--js").submit(function (e) {
        e.preventDefault();
        var $form = $(this);
        var $subscribeCta = $(".contact-us-form__submit-cta--js");
        var url = $form.attr("action");

        $.spinner().start();
        $subscribeCta.attr("disabled", true);
        $.ajax({
            url: url,
            type: "post",
            dataType: "json",
            data: $form.serialize(),
            success: function (data) {
                displayMessage(data, $subscribeCta);
                if (data.success) {
                    $form.trigger("reset");
                } else if (data.freshCSRF) {
                    $form.find(".contact-us-form__csrf-token--js").val(data.freshCSRF);
                    $subscribeCta.removeAttr("disabled");
                }
            },
            error: function (err) {
                displayMessage(err, $subscribeCta);
            },
            complete: $.spinner().stop
        });
    });
})();
