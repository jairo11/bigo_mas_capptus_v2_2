"use strict";

(function () {
    var $button = $(".article .article__buttons a");

    $button.on("click", function (e) {
        e.preventDefault();
        var $article = $(this).parents(".article");

        if ($article.length) {
            $article.addClass("read-more");
        }
    });
})();
