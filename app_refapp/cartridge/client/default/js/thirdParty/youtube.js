"use strict";

/**
 * @function
 * @description load YouTube API script in case is not present on the page.
 */
function loadYoutubeAPI() {
    if (typeof YT == "undefined") {
        const firstScriptTag = document.getElementsByTagName("script")[0];
        const tag = document.createElement("script");

        tag.src = "https://www.youtube.com/iframe_api";
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }
}

/**
 * @private
 * @function
 * @description Initialize video module
 * @param {String} videoContainerId - Youtube API requires ID of element that will be replaced by iframe
 * @param {String} videoId - video code
 */
function initializeVideoModules(videoContainerId, videoId) {
    window.onPlayerReady = function (event) {
        event.target.playVideo();
    };

    if ((typeof(YT) != "undefined" && YT.Player) != false) { // eslint-disable-line no-undef
        createYoutubePlayer(videoContainerId, videoId);
    } else {
        loadYoutubeAPI();

        window.onYouTubeIframeAPIReady = window.onYouTubeIframeAPIReady || function () {
            createYoutubePlayer(videoContainerId, videoId);
        };
    }
}

/**
 * @private
 * @function
 * @description Create instance for yeach player in case multiple on the same page.
 * @param {String} videoContainerId - Youtube API requires ID of element that will be replaced by iframe
 * @param {String} videoId - video code
 */
function createYoutubePlayer(videoContainerId, videoId) {
    try {
        new YT.Player(videoContainerId, { // eslint-disable-line no-undef
            videoId: videoId,
            events: {
                "onReady": function (e) {
                    window.onPlayerReady(e);
                }
            },
            playerVars: {
                showinfo: 0,
                enablejsapi: 1,
                rel: 0
            }
        });
    } catch (err) {
        // Call recursive function when YT.Player is not loaded
        setTimeout(function () {
            initializeVideoModules(videoContainerId, videoId);
        }.bind(this), 1000);
    }
}

module.exports = {
    initializeVideoModules: initializeVideoModules
};
