"use strict";

function switchOptionalFormGroups() {
    const emailType = $("#email-type").val();

    $(".form-group--optional").hide();
    $(`.form-group--optional[data-email-type~="${emailType}"]`).show();
}

(function () {
    // $("#email-form").submit(function(e) {
    //     const $form = $(this);
    //
    //     e.preventDefault();
    //
    //     $.spinner().start();
    //
    //     $.ajax({
    //         url: $form.attr("action"),
    //         type: "get",
    //         dataType: "json",
    //         data: $form.serialize(),
    //         error: function (err) {
    //             console.error(err);
    //         },
    //         complete: $.spinner().stop
    //     });
    // });

    $("#email-type").on("change", switchOptionalFormGroups);

    switchOptionalFormGroups();
})();
