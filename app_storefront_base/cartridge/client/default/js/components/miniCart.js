'use strict';

var cart = require('../cart/cart');

var updateMiniCart = true;

module.exports = function () {
    cart();

    $('.minicart').on('count:update', function (event, count) {
        if (count && $.isNumeric(count.quantityTotal)) {
            $('.minicart .minicart-quantity').text(count.quantityTotal);
            $('.minicart .minicart-link').attr({
                'aria-label': count.minicartCountOfItems,
                title: count.minicartCountOfItems
            });
        }
    });

    $('.minicart').on('click', function () {
        if ($('.search:visible').length === 0) {
            return;
        }
        var url = $('.minicart').data('action-url');
        var count = parseInt($('.minicart .minicart-quantity').text());
        var isCart = window.location.pathname.indexOf('Cart-Show');
        if (count && isCart === -1) {
            if (!updateMiniCart) {
                $('.popover').addClass('show');
                $('body').addClass('no-scroll');
                return;
            }
            $('body').addClass('no-scroll');
            $('.page .popover').addClass('show');
            $('.page .popover').spinner().start();
            $.get(url, function (data) {
                $('.page .popover').empty();
                $('.page .popover').append(data);
                $('.close-minicart').on('click', function () {
                        $('.page .popover').removeClass('show');
                        $('body').removeClass('no-scroll');
                });
                updateMiniCart = false;
                $.spinner().stop();
            });
        }
    });
    $('body').on('touchstart click', function (e) {
        if ($('.minicart').has(e.target).length <= 0) {
            $('.minicart .popover').removeClass('show');
        }
    });

    $('.close-minicart').on('click', function (e) {
        console.log('as');
        if ($('.minicart').has(e.target).length <= 0) {
            $('.page .popover').removeClass('show');
        }
    });

    $('.minicart').on('mouseleave focusout', function (event) {
        if ((event.type === 'focusout' && $('.minicart').has(event.target).length > 0)
            || (event.type === 'mouseleave' && $(event.target).is('.minicart .quantity'))
            || $('body').hasClass('modal-open')) {
            event.stopPropagation();
            return;
        }
        $('.minicart .popover').removeClass('show');
    });
    $('body').on('change', '.minicart .quantity', function () {
        if ($(this).parents('.bonus-product-line-item').length && $('.cart-page').length) {
            location.reload();
        }
    });
    $('body').on('product:afterAddToCart', function () {
        updateMiniCart = true;
    });
    $('body').on('cart:update', function () {
        updateMiniCart = true;
    });
};
