"use strict";

var plainjs = require("int_plainjs");


/**
 * require('*'/cartridge/scripts/debug').printObjToJSON(obj)
 * @param obj Demandware object
 * @param type Java explicit class [optional]
 * @return JSON as string
 */
exports.printObjToJSON = function (obj, type) {
    return JSON.stringify(plainjs.map2Plain(obj, type));
};
