"use strict";

const typesPaths = (function () {
        const typesPaths = ["~/cartridge/scripts/types/"], //NOSONAR
            Resources = require("dw/web/Resource");

        Resources.msg("plainjs.cartridges", "plainjs", "")
            .split(",")
            .map(function (str) {return str.trim();})
            .forEach(function (cartridge) {
                if (cartridge) {
                    typesPaths.push(
                        cartridge + "/cartridge/scripts/types/"
                    );
                }
            });

        return typesPaths; //NOSONAR
    }()),
    protoToString = Object.prototype.toString;

function extend(target) {
    for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];
        for (var key in source) {
            if (Object.prototype.hasOwnProperty.call(source, key)) {
                target[key] = source[key];
            }
        }
    }
    return target;
}

function memoize(func) {
    const memoizeInner = function (key) {
        const cache = memoizeInner.cache;
        if (!cache[key]) {
            cache[key] = func.apply(this, arguments);
        }
        return cache[key];
    };
    memoizeInner.cache = {};
    return memoizeInner;
}

function get(object, path, defaults) {
    var parts = path.split("."),
        part;

    while (parts.length) {
        part = parts.shift();
        if (typeof object === "object" && object !== null && part in object) {
            object = object[part];
        } else {
            return defaults;
        }
    }
    return object;
}

const getTypeDescription = memoize(function getTypeDescription(typeClass) {
    const typePath = typeClass.split(".").join("/"),
        typeDescriptionArr = [];

    typesPaths.forEach(function (path) {
        try {
            typeDescriptionArr.push(require(path + typePath));
        } catch (e) {
            // silently die
        }
    });

    if (typeDescriptionArr.length === 1) {
        return typeDescriptionArr[0];
    } else if (typeDescriptionArr.length > 1) {
        typeDescriptionArr.unshift({});
        return extend.apply(null, typeDescriptionArr);
    } else {
        throw Error("PlainJS: Not found definition for \"" + typeClass + "\"" +
        " looked in " + JSON.stringify(typesPaths));
    }
});
/**
 * Convert JavaClass name to simple format
 * @example `[JavaClass dw.value.Money]` converts to `dw.value.Money`
 * @param  {JavaClass} obj2Map JavaClass for detection
 * @return {String} detected class name
 */
function getJavaClassName(obj2Map) {
    const javaClass = obj2Map.class + "";
    return javaClass.substr(11, javaClass.length - 12);
}
/**
 * Convert JavaClass to JavaScript Object
 * @param  {any} obj2Map JavaClass or JS Object that contains JavaClass
 * @param  {String} [explicitClass] iptional class name
 * @return {any}   JavaScript native object
 */
exports.map2Plain = function map2Plain(obj2Map, explicitClass) {
    const kind = protoToString.call(obj2Map);

    if (explicitClass || kind === "[object JavaObject]") {
        const className = explicitClass || getJavaClassName(obj2Map),
            typeDescription = getTypeDescription(className);

        return Object.keys(typeDescription).reduce(function (acc, objKey) {
            const [key, type] = objKey.split(":"),
                field = typeDescription[objKey];

            if (key === "__values") {
                acc = field(obj2Map, map2Plain);
            } else if (field === "__ignore") {
                return acc;
            } else if (typeof field === "function") {
                acc[key] = field(obj2Map, map2Plain);
            } else if (typeof field === "string") {
                acc[key] = map2Plain(get(obj2Map, field), type);
            } else {
                throw Error("Unexpected case for \"" + objKey + "\"!");
            }
            return acc;
        }, {});
        //}, {'*' : className});
    } else if (kind === "[object Object]") {
        return Object.keys(obj2Map).reduce(function (acc, key) {
            acc[key] = map2Plain(obj2Map[key]);
            return acc;
        }, {});
    } else if (kind === "[object Array]") {
        const initArr = [];

        initArr.length = obj2Map.length;
        return Object.keys(obj2Map).reduce(function (acc, key) {
            acc[key] = map2Plain(obj2Map[key]);
            return acc;
        }, initArr);
    } else {
        return obj2Map;
    }
};
