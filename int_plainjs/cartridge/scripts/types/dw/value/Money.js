"use strict";

module.exports = {
    available : "available",
    currencyCode : "currencyCode",
    value : "value",
    formatted : function (money) {
        return money.toFormattedString();
    }
};
