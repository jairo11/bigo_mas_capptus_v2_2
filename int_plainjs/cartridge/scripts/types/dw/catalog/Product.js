
"use strict";

module.exports = {
    id : "ID",
    name : "name",
    online: "__ignore",
    variants : "variants",
    //variant : 'variant',
    //master : 'master',
    price: "priceModel.price",
    imageURL: function (product, map2Plain) {
        return map2Plain(product.getImages("small"));
    },
    categories: "categories"
};
