"use strict";

exports.__values = function (hash2Plain, map2Plain) {
    return hash2Plain.keySet().toArray().reduce(function (acc, key) {
        const val = hash2Plain.get(key);
        acc[key] = map2Plain(val);
        return acc;
    }, {});
};
