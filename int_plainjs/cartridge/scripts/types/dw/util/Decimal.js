"use strict";

exports.__values = function (decimal) {
    return {
        value: decimal.get()
    };
};
