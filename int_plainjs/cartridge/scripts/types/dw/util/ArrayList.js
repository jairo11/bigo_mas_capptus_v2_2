"use strict";

exports.__values = function (array2Plain, map2Plain) {
    const dest = [];

    if (array2Plain && !array2Plain.empty) {
        const iter = array2Plain.iterator();
        while (iter.hasNext()) {
            dest.push(map2Plain(iter.next()));
        }
    }
    return dest;
};
