# PlainJS
Small library that allows map Demandware/Java objects to JavaScript native objects, so they can be serialized via `JSON.serialize` and so on.

------------------------------------------------

## Intro

The growing popularity of libraries such as Angular, Backbone, Ember and React is required review used techniques for build web. All of them are REST based (kind off). Working on Demandware we face problem with serialization JavaClasses to plain JavaScript objects. So many projects solve this via different way. So purpose of this lib is simplify such process.


## Usage

Usage is simple as whole library. Just check out:
```javascript
const jsMoney = require('int_plainjs').map2Plain(new dw.value.Money(42, 'USD'));

```
But what we got in `jsMoney`? This is a right question. It depends on your types description. Some basic descriptions can be found in `int_plainjs/cartridge/scripts/types`. There a list of folders with same hierarchy as DW classes, ex. class 'dw.value.Money' is based directory `dw/value` in file `Money.js`. The file contain simple object that shows how we should map Java object to JavaScript object.
`Money.js` looks like:
```javascript
// int_plainjs/cartridge/scripts/types/dw/value/Money.js
module.exports = {
    available : 'available',
    currencyCode : 'currencyCode',
    value : 'value',
    formatted : function (money) {
        return money.toFormattedString();
    }
}
```
Property name of exported object is property that will be created in `jsMoney` during mapping and value of property shows what field of `Money` should be read. In case when read require some complex logic, value of property can be a function that returns proper JS value. On invocation in function is passed instance of current Java class as first argument, as second argument is passed function `map2Plain` (is useful for recursive processing). Basically the function can contain some domain logic. In case there simple mapping: one value to another, type description can be saved as json file:
```javascript
// int_plainjs/cartridge/scripts/types/dw/value/Money.json
{
    "available" : "available",
    "currencyCode" : "currencyCode",
    "value" : "value"
}
```

Let's try to look a more complex object.
```javascript
// int_plainjs/cartridge/scripts/types/dw/catalog/Product.js
module.exports = {
    id : 'ID',
    name : 'name',
    online: 'online',
    variants : 'variants',
    price: 'priceModel.price',
    imageURL: function (product, map2Plain) {
        return map2Plain(product.getImages('small'));
    },
    categories: 'categories'
}
```

First of all we should consider a property `variants`, as you may know it contain `Collection` of related product. You may note it's not simple value but complex object that contains other objects. This case PlainJS handle properly and convert to array with another products (actually `Variant`).

> Please note, PlainJS works recursively in deep, in case depth is to big stack will be blown. So be careful with building cyclic objects.

Second thing we consider is property `price`. Value of this property is `priceModel.price`, this means you can read from `Product` only first level value but any value in depth. `priceModel.price.currencyCode` will return currency code of product price. In case `priceModel` or `price` is absent, property `price` will be undefined.

Third thing, take a look how is implemented function `imageURL` and note how `map2Plain` is used.

## Advanced usage

### Custom types

As far as library can detect type of JavaClass what can happen when add possibility to specify type explicitly? That's right we get custom types. This is useful in cases when you project require some specific types. For example you can build type that represent full state of your store and pass this state to client side.
```javascript
// int_plainjs/cartridge/scripts/types/custom/Store.js
module.exports = {
    basket : 'basket',
    customer : 'customer'
};
```
To use this custom type you must specify that type on invocation function `map2Plain` as second parameter.

```javascript
const basketModel = app.getModel('Cart').get();
const store = {
    customer: customer,
    basket: basketModel && basketModel.object
}

const siteState = require('int_plainjs').map2Plain(store, 'custom.Store');
```
Also some time is required to redefine type in existing JavaClass, for example you may want to use other `Product` for `productLineItem` than usually. For this case you can specify new type directly in 'dw/order/ProductLineItem':
```javascript
// int_plainjs/cartridge/scripts/types/dw/order/ProductLineItem.json
{
    "quantity" : "quantity",
    "productName" : "productName",
    "productID" : "productID",
    "adjustedPrice" : "adjustedPrice",
    "product:custom.Product" : "product"
}
```

Take a look on last property `product:custom.Product`, there is divider and first part of property is as before and second in actually type which should be used for this property. So this allows you to have more compact products in basket.

### "Inheritage"

Constantly is needed to change or extend class description but editing of library is not always great idea, for example when the same library is used for many projects.

For similar case we can use one specially provided config. You can specify cartridges where library should look for type definition. You can do this in resource file `you_cartridge/cartridge/templates/resources/plainjs.properties` with key `plainjs.cartridges` by specifying list of cartridges separated by comma.
Library cartridge should be omitted from this list because it's included by default.
As type description is plain JS object so multiple type definition is merged via `extend`. Also is you want to omit some property in destination object just assign value `__ignore`:
```javascript
// your_cartridge/cartridge/scripts/types/dw/value/Money.json
{
    "available" : "__ignore",
    "currencyCode" : "currencyCode",
    "value" : "value"
}
```
