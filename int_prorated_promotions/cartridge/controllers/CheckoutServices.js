"use strict";

var Logger = require("dw/system/Logger");
const server = require("server");
server.extend(module.superModule);

server.append("PlaceOrder", function (req, res, next) {
    var Transaction = require("dw/system/Transaction");
    var requestHelper = require("*/cartridge/scripts/helpers/requestHelper");
    var OrderMgr = require("dw/order/OrderMgr");
    var viewData = res.getViewData();
    var order = OrderMgr.getOrder(viewData.orderID);
    var HookMgr = require("dw/system/HookMgr");
    var Order = require("dw/order/Order");
    if (Object.hasOwnProperty.call(viewData, "orderID")) {
        try {
            Transaction.wrap(function () {
                var collections = require("*/cartridge/scripts/util/collections");
                // var order = OrderMgr.getOrder(viewData.orderID);

                collections.forEach(order.productLineItems, function (pli) {
                    pli.custom.proratedPrice = pli.getProratedPrice().value;
                    pli.custom.proratedUnitPrice = pli.getProratedPrice().value / pli.quantityValue;
                });

                if(order.paymentTransaction.paymentProcessor.ID === 'CASH'){
                        order.setConfirmationStatus(Order.CONFIRMATION_STATUS_NOTCONFIRMED);
                        order.setExportStatus(Order.EXPORT_STATUS_NOTEXPORTED);
                }
            });
        } catch (e) {
            Logger.error("Error trying to calculate prorated prices for items in order {0} : {1}", viewData.orderID, e.message);
        }

        Transaction.wrap(function () {
            order.billingAddress.custom.customerAddresId = null;
            order.billingAddress.custom.saveAddress = null;
            order.shipments[0].shippingAddress.custom.customerAddresId=null;
            order.shipments[0].shippingAddress.custom.saveAddress=null;
            if(order.paymentTransaction.paymentInstrument.custom  && order.paymentTransaction.paymentInstrument.custom.creditCardExpirationDate){
                order.paymentTransaction.paymentInstrument.custom.creditCardExpirationDate=null
            }
            if(order.paymentTransaction.paymentInstrument.custom  && order.paymentTransaction.paymentInstrument.custom.tempToken){
                order.paymentTransaction.paymentInstrument.custom.tempToken=null
            }
            if(order.paymentTransaction.paymentInstrument.custom  && order.paymentTransaction.paymentInstrument.custom.tempNo){
                order.paymentTransaction.paymentInstrument.custom.tempNo=null
            }
            if(order.paymentTransaction.paymentInstrument.custom  && order.paymentTransaction.paymentInstrument.custom.tempCode){
                order.paymentTransaction.paymentInstrument.custom.tempCode=null
            }
        });

        var requestPayload = requestHelper.getShippingRequestPayload(order.shipments[0], order.shipments[0].shippingAddress, order.customer, "venta");
        if (HookMgr.hasHook("app.services.cya.brokerDelivery")) {
            var response = HookMgr.callHook("app.services.cya.brokerDelivery", "getShippingCost", requestPayload);
            if(response){
                Transaction.wrap(function () {
                    order.shipments[0].custom.calculatedShippingCostCache = JSON.stringify(response);
                    order.custom.priceId = response.priceId
                });
            }
        }

        //call api order xml
        if (HookMgr.hasHook("app.services.api.orderXML") && order.paymentStatus.value === 2) {
            // if(order.paymentStatus.value === 1){
                HookMgr.callHook("app.services.api.orderXML",
                    "sendOrderXML",
                    viewData.orderID
                );
            // }
        }
    }
    next();
});

module.exports = server.exports();
