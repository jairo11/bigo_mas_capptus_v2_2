"use strict";

/**
 * The purpose of this helper is to redirect to GTM cartridge if such is used on the site
 * or to return empty objects and avoid any errors in logs caused by gtm functions being
 * used withut gtm cartridge.
 * If gtmEnabled site preference doesn't exist or it is not true, gtm cartridge won't be used.
 */

var isGtmEnabled = require("helpers").sitePreference("gtmEnabled");


function gtmProductTile(productID) {
    if (isGtmEnabled) {
        return require("int_gtm/cartridge/scripts/gtm").gtmProductTile(productID);
    } else {
        return "{}";
    }
}

function gtmQuickView(productID) {
    if (isGtmEnabled) {
        return require("int_gtm/cartridge/scripts/gtm").gtmQuickView(productID);
    } else {
        return "{}";
    }
}

function gtmProductName(productID) {
    if (isGtmEnabled) {
        return require("int_gtm/cartridge/scripts/gtm").gtmProductName(productID);
    } else {
        return "{}";
    }
}

function gtmProductPDP(productID) {
    if (isGtmEnabled) {
        return require("int_gtm/cartridge/scripts/gtm").gtmProductPDP(productID);
    } else {
        return "{}";
    }
}

function gtmAddToCart(productID, quantity, path) {
    if (isGtmEnabled) {
        return require("int_gtm/cartridge/scripts/gtm").gtmAddToCart(productID, quantity, path);
    } else {
        return "{}";
    }
}

function gtmRemoveFromCart(productID, eventTypes) {
    if (isGtmEnabled) {
        return require("int_gtm/cartridge/scripts/gtm").gtmRemoveFromCart(productID, eventTypes);
    } else {
        return "{}";
    }
}

function gtmCheckoutData(step, eventTypes) {
    if (isGtmEnabled) {
        return require("int_gtm/cartridge/scripts/gtm").gtmCheckoutData(step, eventTypes);
    } else {
        return "{}";
    }
}

function gtmOrderConfirmation(OrderNo) {
    if (isGtmEnabled) {
        return require("int_gtm/cartridge/scripts/gtm").gtmOrderConfirmation(OrderNo);
    } else {
        return "{}";
    }
}

function gtmEventData(event, action, label) {
    if (isGtmEnabled) {
        return require("int_gtm/cartridge/scripts/gtm").gtmEventData(event, action, label);
    } else {
        return "{}";
    }
}

function gtmEventListener(eventTypes, event, action, label) {
    if (isGtmEnabled) {
        return require("int_gtm/cartridge/scripts/gtm").gtmEventListener(eventTypes, event, action, label);
    } else {
        return "{}";
    }
}

function gtmForm(eventTypes, action, label) {
    if (isGtmEnabled) {
        return require("int_gtm/cartridge/scripts/gtm").gtmForm(eventTypes, action, label);
    } else {
        return "{}";
    }
}

function gtmAddDynamicData(selector, eventTypes, event, action, label) {
    if (isGtmEnabled) {
        return require("int_gtm/cartridge/scripts/gtm").gtmAddDynamicData(selector, eventTypes, event, action, label);
    } else {
        return "{}";
    }
}

function gtmIsEnabled() {
    if (isGtmEnabled) {
        return require("int_gtm/cartridge/scripts/gtm").gtmIsEnabled();
    } else {
        return false;
    }
}

function gtmSitePreferences() {
    if (isGtmEnabled) {
        return require("int_gtm/cartridge/scripts/gtm").gtmSitePreferences();
    } else {
        return {
            GTM_ENABLED : false,
            GTM_CLICK   : false,
            GTM_HOVER   : false,
            GTM_SCROLL  : false
        };
    }
}


module.exports = {
    gtmProductTile       : gtmProductTile,
    gtmQuickView         : gtmQuickView,
    gtmProductName       : gtmProductName,
    gtmProductPDP        : gtmProductPDP,
    gtmAddToCart         : gtmAddToCart,
    gtmRemoveFromCart    : gtmRemoveFromCart,
    gtmCheckoutData      : gtmCheckoutData,
    gtmOrderConfirmation : gtmOrderConfirmation,
    gtmEventData         : gtmEventData,
    gtmEventListener     : gtmEventListener,
    gtmForm              : gtmForm,
    gtmIsEnabled         : gtmIsEnabled,
    gtmSitePreferences   : gtmSitePreferences,
    gtmAddDynamicData    : gtmAddDynamicData
};

