"use strict";

var Site = require("dw/system/Site");
var StringUtils = require("dw/util/StringUtils");
var Calendar = require("dw/util/Calendar");

var types = {
    object: function (value) {
        if (empty(value)) {
            return {};
        }

        if (typeof value == "string") {
            try {
                value = JSON.parse(value);
            } catch (error) {
                value = {};
            }
        }

        return value;
    },
    string: function (value) {
        if (value) {
            if (value instanceof Array) {
                var result = [];
                result = value.reduce(function (arr, val) {
                    arr = arr.concat(val);
                    return arr;
                }, result);

                return String(result);
            }

            if (value instanceof dw.content.MarkupText) {
                return value.markup;
            }

            if (value instanceof dw.value.EnumValue) {
                return value.value;
            }

            if (value instanceof Date) {
                return StringUtils.formatCalendar(new Calendar(value));
            }

            if (value instanceof dw.content.MediaFile) {
                return value.absURL.toString();
            }

            if (typeof value == "object") {
                return JSON.stringify(value);
            }

            return String(value);
        }

        return null;
    },
    number: function (value) {
        return Number(value);
    },
    array: function (value) {
        if (empty(value)) {
            return Array();
        }

        if (value instanceof Array) {
            var result = [];
            result = value.reduce(function (arr, val) {
                arr = arr.concat(val);
                return arr;
            }, result);

            return result;
        }

        return Array(value);
    },
    boolean: function (value) {
        return Boolean(value);
    }
};

function sitePreferenceAs(customPreference, type) {
    var prefValue = preferences(customPreference);
    var choice = types[type.toLowerCase()];
    var asChoice;
    if (choice) {
        try {
            asChoice = choice(prefValue);
        } catch (error) {
            asChoice = prefValue;
        }
    }
    return asChoice;
}

function preferences(customPreference, fallbackValue) {
    if (customPreference in Site.getCurrent().getPreferences().custom && Site.getCurrent().getPreferences().custom[customPreference] !== null) {
        return Site.getCurrent().getCustomPreferenceValue(customPreference);
    } else {
        return fallbackValue;
    }
}

function printDateWithPreferredTimeZone(date, format) {
    var customerPreferredTimeZone = (customer && customer.authenticated && "preferredTimeZone" in customer.profile.custom) ? customer.profile.custom.preferredTimeZone : null;
    var calendar = new Calendar(date);
    var siteTimeZone = customerPreferredTimeZone || preferences("preferredSiteTimeZone", "GMT");
    calendar.setTimeZone(siteTimeZone);
    var result = format ? StringUtils.formatCalendar(calendar, format) : StringUtils.formatCalendar(calendar);
    return result;
}

module.exports = {
    sitePreference: preferences,
    sitePreferenceAs: sitePreferenceAs,
    printDateWithPreferredTimeZone: printDateWithPreferredTimeZone
};

