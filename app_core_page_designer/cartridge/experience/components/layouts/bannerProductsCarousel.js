"use strict";

var Template = require("dw/util/Template");
var HashMap = require("dw/util/HashMap");
var PageRenderHelper = require("*/cartridge/experience/utilities/PageRenderHelper.js");
var ImageTransformation = require("*/cartridge/experience/utilities/ImageTransformation.js");

/**
 * Render logic for the 1 Row x 1 Col (Mobile), 1 Row x 1 Col (Tablet, Desktop) layout.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var component = context.component;

    // automatically register configured regions
    model.regions = PageRenderHelper.getRegionModelRegistry(component);
    model.regions.bannerProductsCarouselTiles.setClassName("glide__slides");
    model.regions.bannerProductsCarouselTiles.setComponentClassName("glide__slide");
    model.title = context.content.title;
    model.description = context.content.description;
    model.imageMobile = "";
    model.imageTablet = "";
    model.imageDesktop = "";

    if (context.content.imageMobile) {
        model.imageMobile = ImageTransformation.getScaledImage(context.content.imageMobile, context.content.scaledImage);
    }

    if (context.content.imageTablet) {
        model.imageTablet = ImageTransformation.getScaledImage(context.content.imageTablet, context.content.scaledImage);
    }

    if (context.content.imageDesktop) {
        model.imageDesktop = ImageTransformation.getScaledImage(context.content.imageDesktop, context.content.scaledImage);
    }

    if (!model.imageTablet) {
        if (model.imageMobile) {
            model.imageTablet = model.imageMobile;
        }
    }

    if (!model.imageDesktop) {
        if (model.imageTablet) {
            model.imageDesktop = model.imageTablet;
        } else if (model.imageMobile) {
            model.imageDesktop = model.imageMobile;
        }
    }

    if (model.content) {
        switch (model.content.layoutSize) {
            case "Full Width":
                model.layoutSize = "layout-full-width";
                break;
            case "Middle Aligned":
                model.layoutSize = "layout-middle-aligned";
                break;
            case "Minor Margins":
                model.layoutSize = "layout-minor-margins";
                break;
            default:
                model.layoutSize = "layout-full-width";
        }
    }

    model.id = "banner-products-carousel-" + context.component.getID();

    model.enableTopMargin = context.content.enableTopMargin || null;

    return new Template("experience/components/layouts/bannerProductsCarousel").render(model).text;
};
