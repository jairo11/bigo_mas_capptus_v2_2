"use strict";

var Template = require("dw/util/Template");
var HashMap = require("dw/util/HashMap");
var PageRenderHelper = require("*/cartridge/experience/utilities/PageRenderHelper.js");

/**
 * Render logic for the 1 Row x 1 Col (Mobile), 1 Row x 1 Col (Tablet, Desktop) layout.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var component = context.component;

    // automatically register configured regions
    model.regions = PageRenderHelper.getRegionModelRegistry(component);
    model.regions.up4ItemsCarouselItems.setClassName("glide__slides");
    model.regions.up4ItemsCarouselItems.setComponentClassName("glide__slide");
    model.title = context.content.title || null;
    model.description = context.content.description || null;
    model.showDivider = model.title || model.description;

    model.id = "up-4-items-carousel-" + context.component.getID();

    if (context.content) {
        switch (context.content.layoutSize) {
            case "Full Width":
                model.layoutSize = "layout-full-width";
                break;
            case "Middle Aligned":
                model.layoutSize = "layout-middle-aligned";
                break;
            case "Minor Margins":
                model.layoutSize = "layout-minor-margins";
                break;
            default:
                model.layoutSize = "layout-full-width";
        }
    }

    model.enableTopMargin = context.content.enableTopMargin || null;

    return new Template("experience/components/layouts/up4ItemsCarousel").render(model).text;
};
