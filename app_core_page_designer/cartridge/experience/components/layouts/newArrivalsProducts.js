"use strict";

var Template = require("dw/util/Template");
var HashMap = require("dw/util/HashMap");
var PageRenderHelper = require("*/cartridge/experience/utilities/PageRenderHelper.js");

/**
 * Render logic for the 1 Row x 1 Col (Mobile), 1 Row x 1 Col (Tablet, Desktop) layout.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var component = context.component;

    // automatically register configured regions
    model.regions = PageRenderHelper.getRegionModelRegistry(component);
    model.title = context.content.title || "Title";
    model.description = context.content.description || "Lorem Ipsum ...";
    model.loadMore = context.content.loadMore || "+Load More";
    model.showDivider = model.title || model.description;
    model.autoGenerateNewArrivals = context.content.autoGenerateNewArrivals;

    if (context.content) {
        switch (context.content.layoutSize) {
            case "Full Width":
                model.layoutSize = "layout-full-width";
                break;
            case "Middle Aligned":
                model.layoutSize = "layout-middle-aligned";
                break;
            case "Minor Margins":
                model.layoutSize = "layout-minor-margins";
                break;
            default:
                model.layoutSize = "layout-full-width";
        }
    }

    model.enableTopMargin = context.content.enableTopMargin || null;

    return new Template("experience/components/layouts/newArrivalsProducts").render(model).text;
};
