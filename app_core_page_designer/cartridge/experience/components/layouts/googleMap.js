"use strict";

var Template = require("dw/util/Template");
var PageRenderHelper = require("*/cartridge/experience/utilities/PageRenderHelper.js");
var HashMap = require("dw/util/HashMap");

var SiteHelper = require("helpers");

var gmapApiKey = SiteHelper.sitePreference("mapAPI");

/**
 * Render logic for Google map component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var component = context.component;

    // automatically register configured regions
    model.regions = PageRenderHelper.getRegionModelRegistry(component);
    model.content = context.content;

    if (model.content) {
        switch (model.content.layoutSize) {
            case "Full Width":
                model.layoutSize = "layout-full-width";
                break;
            case "Middle Aligned":
                model.layoutSize = "layout-middle-aligned";
                break;
            case "Minor Margins":
                model.layoutSize = "layout-minor-margins";
                break;
            default:
                model.layoutSize = "layout-full-width";
        }
    }

    model.gmapApiKey = gmapApiKey;
    model.enableTopMargin = context.content.enableTopMargin || null;

    return new Template("experience/components/layouts/googleMap").render(model).text;
};
