"use strict";

var Template = require("dw/util/Template");
var HashMap = require("dw/util/HashMap");
var ImageTransformation = require("*/cartridge/experience/utilities/ImageTransformation.js");
var PageRenderHelper = require("*/cartridge/experience/utilities/PageRenderHelper.js");

/**
 * Render logic for Banner Layout.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    // automatically register configured regions
    model.regions = PageRenderHelper.getRegionModelRegistry(context.component);
    model.alt = context.content.alt || null;
    model.link = context.content.link || null;
    model.buttonText = context.content.buttonText || null;

    if (content.imageMobile) {
        model.imageMobile = ImageTransformation.getScaledImage(content.imageMobile, content.scaledImage);
    }

    if (content.imageTablet) {
        model.imageTablet = ImageTransformation.getScaledImage(content.imageTablet, content.scaledImage);
    }

    if (content.imageDesktop) {
        model.imageDesktop = ImageTransformation.getScaledImage(content.imageDesktop, content.scaledImage);
    }

    if (!model.imageTablet) {
        if (model.imageMobile) {
            model.imageTablet = model.imageMobile;
        }
    }

    if (!model.imageDesktop) {
        if (model.imageTablet) {
            model.imageDesktop = model.imageTablet;
        } else if (model.imageMobile) {
            model.imageDesktop = model.imageMobile;
        }
    }


    model.id = "imageButton-" + context.component.getID();

    return new Template("experience/components/assets/imageButton").render(model).text;
};
