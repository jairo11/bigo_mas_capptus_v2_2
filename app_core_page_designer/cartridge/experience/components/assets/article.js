"use strict";

var Template = require("dw/util/Template");
var HashMap = require("dw/util/HashMap");
var ImageTransformation = require("*/cartridge/experience/utilities/ImageTransformation.js");

/**
 * Render logic for Article Layout.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    model.title = context.content.title || null;
    model.content = context.content.content || null;
    model.image = ImageTransformation.getScaledImage(content.image, content.scaledImage);
    model.button1 = context.content.button1 || null;
    model.alt = context.content.alt || null;

    model.enableTopMargin = context.content.enableTopMargin || null;

    model.id = "article-" + context.component.getID();

    if (context.content) {
        switch (context.content.layoutSize) {
            case "Full Width":
                model.layoutSize = "layout-full-width";
                break;
            case "Middle Aligned":
                model.layoutSize = "layout-middle-aligned";
                break;
            case "Minor Margins":
                model.layoutSize = "layout-minor-margins";
                break;
            default:
                model.layoutSize = "layout-full-width";
        }
    }

    return new Template("experience/components/assets/article").render(model).text;
};
