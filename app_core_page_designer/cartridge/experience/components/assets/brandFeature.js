"use strict";

var Template = require("dw/util/Template");
var HashMap = require("dw/util/HashMap");
var ImageTransformation = require("*/cartridge/experience/utilities/ImageTransformation.js");

/**
 * Render logic for Banner Layout.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();

    model.title = context.content.title;
    model.description = context.content.description;
    model.image = ImageTransformation.getScaledImage(context.content.image, context.content.scaledImage);
    model.alt = context.content.alt;
    model.link = context.content.link;

    model.id = "brand-feature-" + context.component.getID();

    return new Template("experience/components/assets/brandFeature").render(model).text;
};
