"use strict";

var Template = require("dw/util/Template");
var HashMap = require("dw/util/HashMap");

/**
 * Render logic for 404 page main content.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();

    model.title = context.content.title || null;
    model.subTitle = context.content.subTitle || null;
    model.description = context.content.description || null;
    model.searchPlaceholder = context.content.searchPlaceholder || null;
    model.btnText = context.content.btnText || null;
    model.btnUrl = context.content.btnUrl || null;

    switch (context.content.layoutSize) {
        case "Full Width":
            model.layoutSize = "layout-full-width";
            break;
        case "Middle Aligned":
            model.layoutSize = "layout-middle-aligned";
            break;
        case "Minor Margins":
            model.layoutSize = "layout-minor-margins";
            break;
        default:
            model.layoutSize = "layout-full-width";
    }
    model.enableTopMargin = context.content.enableTopMargin || null;

    return new Template("experience/components/assets/content404").render(model).text;
};
