"use strict";

var Template = require("dw/util/Template");
var HashMap = require("dw/util/HashMap");

/**
 * Render logic for "Contact us form" module.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();

    // automatically register configured regions
    model.enableTopMargin = context.content.enableTopMargin || null;

    return new Template("experience/components/assets/contactUsForm").render(model).text;
};
