"use strict";

var Template = require("dw/util/Template");
var HashMap = require("dw/util/HashMap");

/**
 * Render logic for Banner Layout.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();

    // automatically register configured regions
    model.title = context.content.title || null;
    model.markup = context.content.markup || null;
    model.isExpanded = context.content.isExpanded || null;

    return new Template("experience/components/assets/accordionContentTab").render(model).text;
};
