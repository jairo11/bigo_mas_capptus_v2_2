"use strict";

var Template = require("dw/util/Template");
var HashMap = require("dw/util/HashMap");
var Resource = require("dw/web/Resource");

/**
 * Render logic for Banner Layout.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();

    model.markerDataString = encodeURI(JSON.stringify({
        lat: context.content.lat || null,
        lng: context.content.lng || null,
        name: context.content.name || null,
        phone: context.content.phone || null,
        phoneFormatted: context.content.phone.replace("()- ", ""),
        translations: {
            phone: Resource.msg("global.phone", "common", null)
        }
    }));

    return new Template("experience/components/assets/googleMapMarker").render(model).text;
};
