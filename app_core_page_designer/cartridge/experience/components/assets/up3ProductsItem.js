"use strict";

var Template = require("dw/util/Template");
var HashMap = require("dw/util/HashMap");
var PageRenderHelper = require("*/cartridge/experience/utilities/PageRenderHelper.js");
var URLUtils = require("dw/web/URLUtils");
var ProductFactory = require("*/cartridge/scripts/factories/product");
var ImageTransformation = require("*/cartridge/experience/utilities/ImageTransformation.js");

/**
 * Render logic for Product Tile.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;
    var product = content.product;
    var productFactoryProduct = ProductFactory.get({pid: product.ID});

    var productUrl = URLUtils.url("Product-Show", "pid", productFactoryProduct.id).relative().toString();
    var productName = productFactoryProduct.productName;

    // automatically register configured regions
    model.price = productFactoryProduct.price ? productFactoryProduct.price.sales.formatted.toString() : null;
    model.regions = PageRenderHelper.getRegionModelRegistry(context.component);
    model.title = context.content.title ? context.content.title : productName;
    model.subtitle = context.content.subtitle;

    model.id = "up3ProductsItem-" + context.component.getID();

    model.urls = {
        productUrl: productUrl
    };

    if (content.image) {
        model.urls.productImage = ImageTransformation.getScaledImage(content.image, content.scaledImage).src.mobile;
    }

    return new Template("experience/components/assets/up3ProductsItem").render(model).text;
};
