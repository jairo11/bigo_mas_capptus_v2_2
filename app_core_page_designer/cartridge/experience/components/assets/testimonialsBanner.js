"use strict";

var Template = require("dw/util/Template");
var HashMap = require("dw/util/HashMap");
var ImageTransformation = require("*/cartridge/experience/utilities/ImageTransformation.js");

/**
 * Render logic for Banner Layout.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    model.title = context.content.title || null;
    model.subtitle = context.content.subtitle || null;
    model.description = context.content.description || null;
    model.personProfesion = context.content.personProfesion || null;
    model.personName = context.content.personName || null;
    model.alt = context.content.alt || null;
    model.link = context.content.link || null;

    if (content.image) {
        model.image = ImageTransformation.getScaledImage(content.image, content.scaledImage);
    }

    model.id = "testimonialsBanner-" + context.component.getID();

    model.enableTopMargin = context.content.enableTopMargin || null;

    return new Template("experience/components/assets/testimonialsBanner").render(model).text;
};
