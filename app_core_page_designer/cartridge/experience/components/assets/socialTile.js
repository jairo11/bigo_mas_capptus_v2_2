"use strict";

var Template = require("dw/util/Template");
var HashMap = require("dw/util/HashMap");
var ImageTransformation = require("*/cartridge/experience/utilities/ImageTransformation.js");

/**
 * Render logic for Banner Layout.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    // automatically register configured regions
    model.title = context.content.title || null;
    model.subtitle = context.content.subtitle || null;
    model.image = ImageTransformation.getScaledImage(content.image, content.scaledImage);
    model.alt = context.content.alt || null;
    model.facebookLink = context.content.facebookLink || null;
    model.twitterLink = context.content.twitterLink || null;
    model.instagramLink = context.content.instagramLink || null;

    model.id = "socialTile-" + context.component.getID();

    return new Template("experience/components/assets/socialTile").render(model).text;
};
