"use strict";

var Template = require("dw/util/Template");
var HashMap = require("dw/util/HashMap");
var ImageTransformation = require("*/cartridge/experience/utilities/ImageTransformation.js");

/**
 * Render logic for Banner Layout.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    // automatically register configured regions
    model.title = context.content.title || null;
    model.subtitle = context.content.subtitle || null;
    model.description = context.content.description || null;
    model.imageMobile = "";
    model.imageTablet = "";
    model.imageDesktop = "";
    model.alt = context.content.alt || null;
    model.link = context.content.link || null;
    model.buttonText = context.content.buttonText || null;
    model.buttonTextSecondary = context.content.buttonTextSecondary || null;

    if (content.imageMobile) {
        model.imageMobile = ImageTransformation.getScaledImage(content.imageMobile, content.scaledImage);
    }

    if (content.imageTablet) {
        model.imageTablet = ImageTransformation.getScaledImage(content.imageTablet, content.scaledImage);
    }

    if (content.imageDesktop) {
        model.imageDesktop = ImageTransformation.getScaledImage(content.imageDesktop, content.scaledImage);
    }

    if (!model.imageTablet) {
        if (model.imageMobile) {
            model.imageTablet = model.imageMobile;
        }
    }

    if (!model.imageDesktop) {
        if (model.imageTablet) {
            model.imageDesktop = model.imageTablet;
        } else if (model.imageMobile) {
            model.imageDesktop = model.imageMobile;
        }
    }

    if (context.content) {
        switch (context.content.layoutSize) {
            case "Full Width":
                model.layoutSize = "layout-full-width";
                break;
            case "Middle Aligned":
                model.layoutSize = "layout-middle-aligned";
                break;
            case "Minor Margins":
                model.layoutSize = "layout-minor-margins";
                break;
            default:
                model.layoutSize = "layout-full-width";
        }
    }

    model.enableTopMargin = context.content.enableTopMargin || null;

    model.id = "up2Cta-" + context.component.getID();

    return new Template("experience/components/assets/up2CtaTile").render(model).text;
};
