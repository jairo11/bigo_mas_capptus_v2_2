"use strict";

var Template = require("dw/util/Template");
var HashMap = require("dw/util/HashMap");
var ImageTransformation = require("*/cartridge/experience/utilities/ImageTransformation.js");

/**
 * Render logic for Banner Layout.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    model.title = context.content.title || null;
    model.description = context.content.description || null;
    model.imageMobile = "";
    model.imageTablet = "";
    model.imageDesktop = "";
    model.alt = context.content.alt || null;
    model.link1 = context.content.link || null;
    model.link2 = context.content.link2 || null;
    model.button1 = context.content.button1 || null;
    model.button2 = context.content.button2 || null;

    if (content.imageMobile) {
        model.imageMobile = ImageTransformation.getScaledImage(content.imageMobile, content.scaledImage);
    }

    if (content.imageTablet) {
        model.imageTablet = ImageTransformation.getScaledImage(content.imageTablet, content.scaledImage);
    }

    if (content.imageDesktop) {
        model.imageDesktop = ImageTransformation.getScaledImage(content.imageDesktop, content.scaledImage);
    }

    if (!model.imageTablet) {
        if (model.imageMobile) {
            model.imageTablet = model.imageMobile;
        }
    }

    if (!model.imageDesktop) {
        if (model.imageTablet) {
            model.imageDesktop = model.imageTablet;
        } else if (model.imageMobile) {
            model.imageDesktop = model.imageMobile;
        }
    }

    model.enableTopMargin = context.content.enableTopMargin || null;

    model.id = "banner-" + context.component.getID();

    return new Template("experience/components/assets/banner").render(model).text;
};
