"use strict";

var Template = require("dw/util/Template");
var HashMap = require("dw/util/HashMap");
var ImageTransformation = require("*/cartridge/experience/utilities/ImageTransformation.js");
var PageRenderHelper = require("*/cartridge/experience/utilities/PageRenderHelper.js");

/**
 * Render logic for Banner Layout.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    // automatically register configured regions
    model.regions = PageRenderHelper.getRegionModelRegistry(context.component);
    model.alt = context.content.alt || null;
    model.link = context.content.link || null;

    if (content.image) {
        model.image = ImageTransformation.getScaledImage(content.image, content.scaledImage);
    }

    model.id = "up5ItemsCarouselItem-" + context.component.getID();

    return new Template("experience/components/assets/up5ItemsCarouselItem").render(model).text;
};
