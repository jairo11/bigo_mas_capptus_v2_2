"use strict";

var Template = require("dw/util/Template");
var HashMap = require("dw/util/HashMap");

/**
 * Render logic for Product Tile.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    model.product = content.productTileItem;
    model.id = "productTile-" + context.component.getID();

    return new Template("experience/components/assets/productTile").render(model).text;
};
