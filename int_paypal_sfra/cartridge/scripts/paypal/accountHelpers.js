'use strict';

/* global empty customer */

var Transaction = require('dw/system/Transaction');

var prefs = require('~/cartridge/config/paypalPreferences');
var paypalApi = require('~/cartridge/scripts/paypal/paypalApi');

var accountHelper = {};

/**
 * Determines a unique address ID for an address to be save the given
 * address book. The function first checks the city as the candidate ID
 * or appends a counter to the city (if already used as address ID) and
 * checks the existence of the resulting ID candidate. If the resulting
 * ID is unique this ID is returned, if not the counter is incremented and
 * checked again.
 *
 * @param {string} city customer city
 * @param {dw.customer.AddressBook} addressBook customer address book
 * @returns {boolean} candidateID or null
 */
function determineUniqueAddressID(city, addressBook) {
    var counter = 0;
    var existingAddress = null;

    if (city === null || empty(city)) {
        return null;
    }

    var candidateID = city;

    while (existingAddress === null) {
        existingAddress = addressBook.getAddress(candidateID);
        if (existingAddress !== null) {
            counter++;
            candidateID = city + '-' + counter;
            existingAddress = null;
        } else {
            return candidateID;
        }
    }

    return null;
}

/**
 * Returns a possible equivalent address to the given order address from the
 * address book or null, if non equivalent address was found.
 *
 * @param {dw.customer.AddressBook} addressBook customer address book
 * @param {dw.order.OrderAddress} orderAddress order shipping address
 * @returns {dw.customer.CustomerAddress} customer address
 */
function getEquivalentAddress(addressBook, orderAddress) {
    var iterator = addressBook.addresses.iterator();
    var address = null;
    while (iterator.hasNext()) {
        address = iterator.next();
        if (address.isEquivalentAddress(orderAddress)) {
            return address;
        }
    }

    return null;
}

/**
 * Return Actual Billing Agreement data
 * @param {Object} basket - Basket
 * @returns {Object} null or object with billing agreement data
 */
accountHelper.getActualBillingAgreementData = function (basket) {
    if (!customer.authenticated) {
        return null;
    }
    var currencyCode = basket.getCurrencyCode();
    var customerBillingAgreement = require('~/cartridge/scripts/paypal/paypalHelper').getCustomerBillingAgreement(currencyCode);

    if (customerBillingAgreement.hasAnyBillingAgreement && prefs.PP_BillingAgreementState !== 'DoNotCreate') {
        var referenceId = customerBillingAgreement.getDefault().id;
        var response = paypalApi.baUpdate({
            referenceId: referenceId,
            currencyCode: currencyCode
        });
        if (response.error) {
            if (response.responseData) {
                var errorCodes = [10201, // Billing Agreement was cancelled
                    10204, // User's account is closed or restricted
                    10211, // Invalid billing agreement ID
                    11451]; // Billing Agreement Id or transaction Id is not valid

                if (errorCodes.indexOf(parseInt(response.responseData.l_errorcode0, 10)) >= 0) {
                    customerBillingAgreement.remove(referenceId);
                }
            }
            return null;
        }
        return response;
    }

    return null;
};

accountHelper.createAddress = function (usedAddress, addressBook) {
    var addressID = accountHelper.determineUniqueAddressID(usedAddress.city, addressBook);

    if (empty(addressID)) {
        return null;
    }

    var address = addressBook.createAddress(addressID);
    address.setFirstName(usedAddress.firstName);
    address.setLastName(usedAddress.lastName);
    address.setAddress1(usedAddress.address1);
    address.setAddress2(usedAddress.address2);
    address.setCity(usedAddress.city);
    address.setPostalCode(usedAddress.postalCode);
    address.setStateCode(usedAddress.stateCode);
    address.setCountryCode(usedAddress.countryCode.value);
    return address;
};

/**
 * Save shipping address as default address for future PayPal transaction through Billing Agreement
 *
 * @param {dw.order.LineItemCtnr} basket Order
 */
accountHelper.saveShippingAddressToAccountFromBasket = function (basket) {
    if (!customer.authenticated) {
        return;
    }

    var addressBook = customer.profile.addressBook;
    var usedAddress = basket.defaultShipment.shippingAddress;

    if (usedAddress === null) {
        return;
    }

    Transaction.wrap(function () {
        var address = accountHelper.getEquivalentAddress(addressBook, usedAddress, addressBook);

        if (address === null) {
            address = accountHelper.createAddress(usedAddress, addressBook);
        }

        address.setPhone(usedAddress.phone);

        var iterator = addressBook.addresses.iterator();
        var addressItem = null;
        while (iterator.hasNext()) {
            addressItem = iterator.next();
            addressItem.custom.paypalDefaultShippingAddressCurrencyCode = null;
        }

        address.custom.paypalDefaultShippingAddressCurrencyCode = basket.getCurrencyCode();
    });
};

accountHelper.determineUniqueAddressID = determineUniqueAddressID;
accountHelper.getEquivalentAddress = getEquivalentAddress;
module.exports = accountHelper;
