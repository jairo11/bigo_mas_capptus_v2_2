/* eslint-disable camelcase */
var paypal = {
    FUNDING: {
        BANCONTACT: 'bancontact',
        CARD: 'card',
        CREDIT: 'credit',
        ELV: 'elv',
        EPS: 'eps',
        GIROPAY: 'giropay',
        IDEAL: 'ideal',
        MYBANK: 'mybank',
        PAYPAL: 'paypal',
        VENMO: 'venmo'
    },
    CARD: {
        AMEX: 'amex',
        CBNATIONALE: 'cbnationale',
        CETELEM: 'cetelem',
        COFIDIS: 'cofidis',
        COFINOGA: 'cofinoga',
        CUP: 'cup',
        DISCOVER: 'discover',
        ELO: 'elo',
        HIPER: 'hiper',
        JCB: 'jcb',
        MAESTRO: 'maestro',
        MASTERCARD: 'mastercard',
        SWITCH: 'switch',
        VISA: 'visa'
    }
};

var PP_Cart_Button_Config = {
    locale: 'es_MX',
    style: {
        size: 'responsive', // small, medium, large, responsive
        color: 'gold', // gold, blue, silver, black
        shape: 'pill', // pill, rect
        layout: 'vertical', // horizontal, vertical
        maxbuttons: 1 // 1-4
    },
    commit: false,
    funding: {
        allowed: [],
        disallowed: []
        // paypal.FUNDING.CREDIT
        // paypal.FUNDING.CARD
        // paypal.FUNDING.ELV
    }
};

var PP_Billing_Button_Config = {
    locale: 'es_MX',
    style: {
        size: 'responsive', // small, medium, large, responsive
        color: 'gold', // gold, blue, silver, black
        shape: 'pill', // pill, rect
        layout: 'vertical', // horizontal, vertical
        maxbuttons: 1 // 1-4
    },
    commit: false,
    funding: {
        allowed: [paypal.FUNDING.CARD, paypal.FUNDING.CREDIT],
        disallowed: []
        // paypal.FUNDING.CREDIT
        // paypal.FUNDING.CARD
        // paypal.FUNDING.ELV
    }
};

module.exports = {
    PP_Cart_Button_Config: PP_Cart_Button_Config,
    PP_Billing_Button_Config: PP_Billing_Button_Config
};
