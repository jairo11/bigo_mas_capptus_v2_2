"use strict";

var servicesApiOrder = require("*/cartridge/scripts/services/apiOrder");
var OrderMgr = require('dw/order/OrderMgr');
var Order = require('dw/order/Order');

function sendOrderXML(noOrder) {
    var Transaction = require("dw/system/Transaction");
    var transactionsHistoryTMP = null;
    var order = OrderMgr.getOrder(noOrder);
    if (order.paymentTransaction.paymentProcessor.ID === 'PAYPAL') {
        transactionsHistoryTMP = order.paymentTransaction.custom.transactionsHistory;
        order.paymentTransaction.custom.transactionsHistory = null;
    }
    var singleOrderXML = {};
    singleOrderXML = order.getOrderExportXML(null, null);
    singleOrderXML = singleOrderXML.replace('xmlns=\"http://www.demandware.com/xml/impex/order/2006-10-31\"', "")
    singleOrderXML = singleOrderXML.replace("<order", "<orders>\n<order");
    singleOrderXML = singleOrderXML.replace("</order>", "</order>\n</orders>");
    var orderXML = new XML(singleOrderXML);
    if (transactionsHistoryTMP !== null) {
        order.paymentTransaction.custom.transactionsHistory = transactionsHistoryTMP;
    }

    var res = servicesApiOrder.sendOrder(orderXML);
    if (res.ok === 'false') {
        Logger.error('Error to export order: ' + noOrder);
        Logger.error('Error -' + res);
        return res.error;
    }
    Transaction.wrap(function () {
        order.setExportStatus(Order.EXPORT_STATUS_EXPORTED);
    });
    return "Ok";
}

module.exports = {
    sendOrderXML: sendOrderXML
};

