"use strict";

var LocalServiceRegistry = require("dw/svc/LocalServiceRegistry");
var System = require("dw/system/System");


// Global service componentnts
var svc;
var config;
var url;
var user;
var password;
var credential;

/**
 * Initialize the service components
 */
function initservice() {
    // Create a service object
    svc = LocalServiceRegistry.createService("apiOrder.service", {
        createRequest: function (_svc, args) {
            if (args) {
                return args;
            }
            return null;
        },
        parseResponse: function (_svc, client) {
            return client.text;
        },
        filterLogMessage: function (msg) {
            return msg;
        }
    });
    // Configure the service related parameters
    config = svc.getConfiguration();
    credential = config.getCredential();
    user = credential.getUser();
    password = credential.getPassword();
    url = !empty(credential.getURL()) ? credential.getURL() : "";
    svc.addHeader("Content-Type", "application/xml");
    svc.addHeader("charset","utf-8");
    // svc.addHeader("Authorization", "Basic " + require('dw/util/StringUtils').encodeBase64(user + ':' + password));
}

function sendOrder(request) {
    initservice();
    var req = {};
    req= request;
    svc.setRequestMethod("POST");
    url = encodeURI(url);
    url += "v1/orders";
    svc.setURL(url);
    // service call
    var httpResult = svc.call(req);
    return httpResult;
}

// var dummiTracking={
//     "ResponseCode": 200, 
//     "ResponseMessage": "OK",
//     "company": "REDPACK",
//     "guideCompany": "12345678",
//     "currentSituation": "RECIBIDO EN OFICINA",
//     "history": [
//     {
//     "situation": "RECIBIDO EN OFICINA",
//     "date": "2021-01-30T15:20:31.000-06:00",
//     "location": "TULTITLAN, MEXICO",
//     "observations": "-"
//     },
//     {
//         "situation": "EN CAMINO",
//         "date": "2021-01-30T15:20:31.000-06:00",
//         "location": "TULTITLAN, MEXICO",
//         "observations": "-"
//         },
//     {
//     "situation": "ENTREGADO",
//     "date": "2021-01-27T13:49:11.000-06:00",
//     "location": "INDETERMINADA",
//     "observations": "-"
//     }
//     ]
//    };   

function getTrackingOrder(request) {
    var Site = require('dw/system/Site');
    initservice();
    var req = {};
    req= request;
    req.name= Site.getCurrent().getPreferences().custom.IDCredential;
    req.id = Site.getCurrent().getPreferences().custom.nameCredential;
    req = JSON.stringify(req);
    svc.addHeader("Content-Type", "application/json");
    svc.setRequestMethod("POST");
    url = encodeURI(url);
    url += "v1/shipping/track";
    svc.setURL(url);
    // service call
    var httpResult = svc.call(req);
    return responseFilter(httpResult);
    // return dummiTracking;
}

function responseFilter(httpResponse) {
    if (httpResponse.status !== "OK") {
        var errorResult = {
            statusCode: httpResponse.status,
            errorMessage: parseMessage(httpResponse),
            url: url
        };
        return errorResult;
    }
    // return a plain javascript object
    return JSON.parse(httpResponse.object);
}

function parseMessage(httpResponse) {
    var msg = "";
    try {
        msg = JSON.parse(httpResponse.getErrorMessage())
    } catch (error) {
        msg = httpResponse.msg;
    }
    return msg;
}

module.exports = {
    sendOrder: sendOrder,
    getTrackingOrder: getTrackingOrder
};

