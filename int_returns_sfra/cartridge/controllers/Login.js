"use strict";

const server = require("server");
var userLoggedIn = require("*/cartridge/scripts/middleware/userLoggedIn");
var consentTracking = require("*/cartridge/scripts/middleware/consentTracking");
var csrfProtection = require("*/cartridge/scripts/middleware/csrf");
var returnOH = require("*/cartridge/scripts/helpers/returnOrderHelper");

server.extend(module.superModule);

server.append("Show",
    consentTracking.consent,
    server.middleware.https,
    csrfProtection.generateToken,
    function (req, res, next) {
            res.setViewData({navTabReturnValue : "track"});
        next();
    });

module.exports = server.exports();