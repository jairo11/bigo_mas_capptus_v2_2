
"use strict";

var server = require('server');

server.extend(module.superModule);

var csrfProtection = require("*/cartridge/scripts/middleware/csrf");
var validate = require("*/cartridge/scripts/features/validate");

server.replace('SubmitRegistration', 
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var CustomerMgr = require("dw/customer/CustomerMgr");
        var Resource = require("dw/web/Resource");
        var WishlistHelpers = require("*/cartridge/scripts/features/wishlistHelpers");
        var formErrors = require("*/cartridge/scripts/formErrors");

        var registrationForm = server.forms.getForm("profile");
        var guestCustomer = req.currentCustomer.raw;

        // form validation
        if (registrationForm.customer.email.value.toLowerCase()
            !== registrationForm.customer.emailconfirm.value.toLowerCase()
        ) {
            registrationForm.customer.email.valid = false;
            registrationForm.customer.emailconfirm.valid = false;
            registrationForm.customer.emailconfirm.error =
                Resource.msg("error.message.mismatch.email", "forms", null);
            registrationForm.valid = false;
        }

        if (registrationForm.login.password.value
            !== registrationForm.login.passwordconfirm.value
        ) {
            registrationForm.login.password.valid = false;
            registrationForm.login.passwordconfirm.valid = false;
            registrationForm.login.passwordconfirm.error =
                Resource.msg("error.message.mismatch.password", "forms", null);
            registrationForm.valid = false;
        }

        if (!CustomerMgr.isAcceptablePassword(registrationForm.login.password.value) || /\s/g.test(registrationForm.login.password.value)) {
            registrationForm.login.password.valid = false;
            registrationForm.login.passwordconfirm.valid = false;
            registrationForm.login.password.error = validate.validatePassword(registrationForm.login.password.value);
            registrationForm.valid = false;
        }

        // setting variables for the BeforeComplete function
        var registrationFormObj = {
            firstName: registrationForm.customer.firstname.value,
            lastName: registrationForm.customer.lastname.value,
            phone: registrationForm.customer.phone.value,
            email: registrationForm.customer.email.value,
            emailConfirm: registrationForm.customer.emailconfirm.value,
            password: registrationForm.login.password.value,
            passwordConfirm: registrationForm.login.passwordconfirm.value,
            validForm: registrationForm.valid,
            form: registrationForm
        };

        if (registrationForm.valid) {
            res.setViewData(registrationFormObj);

            // eslint-disable no-shadow
            this.on("route:BeforeComplete", function (req, res) { //NOSONAR
            // eslint-enable no-shadow
                var Transaction = require("dw/system/Transaction");
                var accountHelpers = require("*/cartridge/scripts/helpers/accountHelpers");
                var returnOH = require("*/cartridge/scripts/helpers/returnOrderHelper");
                var authenticatedCustomer;
                var serverError;

                // getting variables for the BeforeComplete function
                // eslint-disable
                var registrationForm = res.getViewData(); //NOSONAR
                // eslint-enable

                if (registrationForm.validForm) {
                    var login = registrationForm.email;
                    var password = registrationForm.password;

                    // attempt to create a new user and log that user in.
                    try {
                        Transaction.wrap(function () {
                            var error = {};
                            var newCustomer = CustomerMgr.createCustomer(login, password);
                            accountHelpers.handleNewOptinCustomer(newCustomer);

                            var authenticateCustomerResult = CustomerMgr.authenticateCustomer(login, password);
                            if (authenticateCustomerResult.status !== "AUTH_OK") {
                                error = { authError: true, status: authenticateCustomerResult.status };
                                throw error;
                            }

                            authenticatedCustomer = CustomerMgr.loginCustomer(authenticateCustomerResult, false);

                            if (!authenticatedCustomer) {
                                error = { authError: true, status: authenticateCustomerResult.status };
                                throw error;
                            } else {
                                accountHelpers.addsValidationLinksToSession(authenticatedCustomer.profile);
                                WishlistHelpers.mergeGuestListToLoggedInCustomer(guestCustomer, authenticatedCustomer, req);
                                // assign values to the profile
                                var newCustomerProfile = newCustomer.getProfile();

                                newCustomerProfile.firstName = registrationForm.firstName;
                                newCustomerProfile.lastName = registrationForm.lastName;
                                newCustomerProfile.phoneHome = registrationForm.phone;
                                newCustomerProfile.email = registrationForm.email;
                                returnOH.mergeOdersToCreatedCustomer(newCustomer);
                                
                            }
                        });
                    } catch (e) {
                        if (e.authError) {
                            serverError = true;
                        } else {
                            registrationForm.validForm = false;
                            registrationForm.form.customer.email.valid = false;
                            registrationForm.form.customer.emailconfirm.valid = false;
                            registrationForm.form.customer.email.error =
                                Resource.msg("error.message.username.exists", "forms", null);
                        }
                    }
                }

                delete registrationForm.password;
                delete registrationForm.passwordConfirm;
                formErrors.removeFormValues(registrationForm.form);

                if (serverError) {
                    res.setStatusCode(500);
                    res.json({
                        success: false,
                        errorMessage: Resource.msg("error.message.unable.to.create.account", "login", null)
                    });

                    return;
                }

                if (registrationForm.validForm) {
                    // send a registration email or account verification email
                    if (!accountHelpers.isAccountValidationActive()) {
                        accountHelpers.sendCreateAccountEmail(authenticatedCustomer.profile);
                    } else {
                        accountHelpers.sendAccountVerificationEmail(authenticatedCustomer.profile);
                    }

                    res.setViewData({ authenticatedCustomer: authenticatedCustomer });
                    res.json({
                        success: true,
                        redirectUrl: accountHelpers.getLoginRedirectURL(req.querystring.rurl, req.session.privacyCache, true)
                    });

                    req.session.privacyCache.set("args", null);
                } else {
                    res.json({
                        fields: formErrors.getFormErrors(registrationForm)
                    });
                }
            });
            this.on("route:Complete", function (req, res) { //NOSONAR
                var Transaction = require("dw/system/Transaction");
                var viewData = res.getViewData();
                if (viewData.success && customer.authenticated) {
                    Transaction.wrap(function () {
                        customer.profile.custom.lastProfileChangeTimestamp = new Date();
                    });
                }
            });
        } else {
            res.json({
                fields: formErrors.getFormErrors(registrationForm)
            });
        }

    return next();
}
);

module.exports = server.exports();