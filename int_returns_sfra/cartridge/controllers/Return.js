"use strict";

var server = require("server");

var Resource = require("dw/web/Resource");
var URLUtils = require("dw/web/URLUtils");
var userLoggedIn = require("*/cartridge/scripts/middleware/userLoggedIn");
var consentTracking = require("*/cartridge/scripts/middleware/consentTracking");
//var cartHelper = require("*/cartridge/scripts/helpers/cartHelpers");
var csrfProtection = require("*/cartridge/scripts/middleware/csrf");
var returnCore = require("*/cartridge/scripts/core/returnCore");
var returnOH = require("*/cartridge/scripts/helpers/returnOrderHelper");
var CustomObjectMgr = require("dw/object/CustomObjectMgr");
var ProductMgr = require('dw/catalog/ProductMgr');

var statusReturn = [
    {id:0, valor:"En proceso"},
    {id:1, valor:"En proceso"},
    {id:2, valor:"En camino"},
    {id:3, valor:"En revisión"},
    {id:4, valor:"En revisión"},
    {id:5, valor:"Devolucion aceptada"},
    {id:6, valor:"Completada"},
    {id:7, valor:"Devolucion rechazada"},
    {id:8, valor:"Paquete devuelto"},
    {id:9, valor:"Guia expirada"},
    {id:10, valor:"Fallida"},
    {id:11, valor:"Cancelada"},
];

server.get(
    "Details",
    consentTracking.consent,
    server.middleware.https,
    userLoggedIn.validateLoggedIn,
    function (req, res, next) {
        var OrderMgr = require("dw/order/OrderMgr");
        var OrderModel = require("*/cartridge/models/order");
        var Locale = require("dw/util/Locale");

        var order = OrderMgr.getOrder(req.querystring.orderID);
        var orderCustomerNo = req.currentCustomer.profile.customerNo;
        var currentCustomerNo = order.customer.profile.customerNo;
        var breadcrumbs = [
            {
                htmlValue: Resource.msg("global.home", "common", null),
                url: URLUtils.home().toString()
            },
            {
                htmlValue: Resource.msg("page.title.myaccount", "account", null),
                url: URLUtils.url("Account-Show").toString()
            },
            {
                htmlValue: Resource.msg("label.orderhistory", "account", null),
                url: URLUtils.url("Order-History").toString()
            }
        ];

        if (order && orderCustomerNo === currentCustomerNo) {
            var config = {
                numberOfLineItems: "*"
            };

            var currentLocale = Locale.getLocale(req.locale.id);

            var orderModel = new OrderModel(
                order,
                { config: config, countryCode: currentLocale.country, containerView: "order" }
            );
            var exitLinkText = Resource.msg("link.orderdetails.orderhistory", "account", null);
            var exitLinkUrl = req.querystring.orderFilter ? URLUtils.https("Order-History", "orderFilter", req.querystring.orderFilter) : URLUtils.https("Order-History");
            var x=returnOH.getReturnPreferences();
            res.render("return/returnDetails", {
                order: orderModel,
                exitLinkText: exitLinkText,
                exitLinkUrl: exitLinkUrl,
                breadcrumbs: breadcrumbs,
                preferences: returnOH.getReturnPreferences(),
                returnOrder: returnCore.getReturnOrderDetails(order),
                returnsBeforeDate: returnOH.getReturnsBeforeDate(order)
            });
        } else {
            res.redirect(URLUtils.url("Account-Show"));
        }
        next();
    }
);


server.get(
    "NoAccount",
    consentTracking.consent,
    server.middleware.https,
    csrfProtection.validateRequest,
    csrfProtection.generateToken,
    function (req, res, next) {

        var OrderMgr = require("dw/order/OrderMgr");
        var OrderModel = require("*/cartridge/models/order");
        var Locale = require("dw/util/Locale");

        var order;
        var validForm = true;

        var profileForm = server.forms.getForm("profile");
        profileForm.clear();

        if (req.querystring.trackOrderEmail
            && req.querystring.trackOrderPostal
            && req.querystring.trackOrderNumber) {
            order = OrderMgr.getOrder(req.querystring.trackOrderNumber);
        } else {
            validForm = false;
        }

        if (!order) {
            res.render("/account/login", {
                navTabValue: "login",
                orderTrackFormError: validForm,
                profileForm: profileForm,
                userName: ""
            });
            next();
        } else {
            var config = {
                numberOfLineItems: "*"
            };

            var currentLocale = Locale.getLocale(req.locale.id);

            var orderModel = new OrderModel(
                order,
                { config: config, countryCode: currentLocale.country, containerView: "order" }
            );

            // check the email and postal code of the form
            if (req.querystring.trackOrderEmail.toLowerCase()
                    !== orderModel.orderEmail.toLowerCase()) {
                validForm = false;
            }

            if (req.querystring.trackOrderPostal
                !== orderModel.billing.billingAddress.address.postalCode) {
                validForm = false;
            }

            if (validForm) {
                var exitLinkText;
                var exitLinkUrl;

                exitLinkText = !req.currentCustomer.profile
                    ? Resource.msg("link.continue.shop", "order", null)
                    : Resource.msg("link.orderdetails.myaccount", "account", null);

                exitLinkUrl = !req.currentCustomer.profile
                    ? URLUtils.url("Home-Show")
                    : URLUtils.https("Account-Show");

                res.render("return/returnDetails", {
                    order: orderModel,
                    exitLinkText: exitLinkText,
                    exitLinkUrl: exitLinkUrl,
                    cartAvailableAttributes: cartHelper.getAvailableAttributes(),
                    returnOrder: returnCore.getReturnOrderDetails(order),
                    returnsBeforeDate: returnOH.getReturnsBeforeDate(order)
                });
            } else {
                res.render("/account/login", {
                    navTabValue: "login",
                    profileForm: profileForm,
                    orderTrackFormError: !validForm, // NOSONAR
                    userName: ""
                });
            }

            next();
        }
    }
);

server.post(
    "CalculateShippingCost",
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var Resource = require("dw/web/Resource");
        var returnOH = require("*/cartridge/scripts/helpers/returnOrderHelper");
        var requestHelpers = require("*/cartridge/scripts/helpers/requestHelper");
        var OrderMgr = require("dw/order/OrderMgr");
        var Site = require("dw/system/Site");
        var returnItems = JSON.parse(req.form.data);
        var refoundAmount = JSON.parse(req.form.refoundAmount);
        var orderNumber = req.querystring.orderId;
        var order = OrderMgr.getOrder(orderNumber);
        var shipment = order.shipments[0];
        var error = "";
        var returnShippingPrice = 0;
        var priceID = "";
        var amountToRefound = refoundAmount.amountToRefound;
        var returnCost = JSON.parse(Site.getCurrent().getCustomPreferenceValue("returnCost"));
        var minimunAmountReturn = JSON.parse(Site.getCurrent().getCustomPreferenceValue("minimunAmountReturn"));
        var amountFreeReturn = JSON.parse(Site.getCurrent().getCustomPreferenceValue("amountFreeReturn"));
        var warning = "";

        var shippingPayload = requestHelpers.getShippingReturnRequestPayload(returnItems, shipment.shippingAddress, order.customer, "devolucion");
        var shippingResponse = returnCore.getAPIBrokerDilivery(shippingPayload);
        if (shippingResponse) {
            if (returnOH.isFirstReturn(orderNumber) && (order.totalNetPrice >= amountFreeReturn)) {
                returnShippingPrice = 0;
            }else{
                returnShippingPrice = returnCost;
            }
            priceID = shippingResponse.priceId;
        } else {
            error = Resource.msg("error.message.calculated.shipping.error", "return", null);
        }

        amountToRefound = (amountToRefound - returnShippingPrice).toFixed(2);

        if(minimunAmountReturn > refoundAmount.amountToRefound){
            warning = Resource.msgf("warning.message.minimun.amout.for.return", "return", null, minimunAmountReturn);
        }

        res.json({
            success: "success",
            error: error,
            warning: warning,
            itemsCost: (parseFloat(refoundAmount.amountToRefound,10).toFixed(2)),
            amountToRefound: (parseFloat(amountToRefound,10).toFixed(2)),
            returnShippingPrice: returnShippingPrice,
            minimunAmountReturn: minimunAmountReturn,
            priceID: priceID
        });

        return next();
    });

server.post("ReturnSubmit",
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var Site = require("dw/system/Site");
        var HookMgr = require("dw/system/HookMgr");
        var OrderMgr = require("dw/order/OrderMgr");
        var requestHelpers = require("*/cartridge/scripts/helpers/requestHelper");
        var shippingResponse = '';
        var orderNumber = req.querystring.orderId;
        var returnItems = JSON.parse(req.form.data);
        var ttlItems = null;
        //var priceID = req.form.priceID;
        var returnShippingCost = (req.form.returnShippingCost? new Number(req.form.returnShippingCost): 0);
        var total = req.form.total;
        var rma = "";
        var CustomObjectMgr = require("dw/object/CustomObjectMgr");
        var Transaction = require("dw/system/Transaction");
        var order = OrderMgr.getOrder(orderNumber);
        var shipment = order.shipments[0];
        var minimunAmountReturn = JSON.parse(Site.getCurrent().getCustomPreferenceValue("minimunAmountReturn"));
        var subttl=0;
        for (var i = 0; i < returnItems.length; i++) {
            var item = JSON.parse(returnItems[i].orderLineItem);
            subttl += Number(item.adjustedPrice * item.quantity)
        }

        if(subttl<minimunAmountReturn){
            res.json({
                success: "success",
                error: Resource.msg("label.message.minimun.amout.return", "return", null)
            });

            return next();
        }

        var shippingPayload = requestHelpers.getShippingReturnRequestPayload(returnItems, shipment.shippingAddress, order.customer, "devolucion");
        shippingResponse = returnCore.getAPIBrokerDilivery(shippingPayload);

        var returnsPayload = requestHelpers.getReturnsRequestPayload(orderNumber, returnItems, shippingResponse.priceId, returnShippingCost);
        var returnResponse = returnCore.getAPIReturnOrder("returnOrder", returnsPayload);

        if (!returnResponse) {
            res.json({
                success: "success",
                error: Resource.msg("error.message.return.submit.error", "return", null)
            });

            return next();
        }
        let costPref=returnOH.getReturnPreferences();
        rma = returnResponse.returnId.toString();

        Transaction.wrap(function () {
            var itemsTotalCost = 0;
            var returnOrder = CustomObjectMgr.createCustomObject("ReturnOrder", rma);
            returnOrder.custom.trackingNumber = returnResponse.guideNo;
            returnOrder.custom.orderNO = orderNumber;
            returnOrder.custom.pdf = returnResponse.pdf;
            returnOrder.custom.status = parseInt(returnOH.ReturnStatus.CREATED, 10);
            returnOrder.custom.shippingPrice = returnShippingCost;
            returnOrder.custom.appliedRefund = false;
            returnOrder.custom.customerID = order.customerNo;
            returnOrder.custom.deliveryCompany = returnResponse.company;

            for (var i = 0; i < returnItems.length; i++) {
                var item = JSON.parse(returnItems[i].orderLineItem);
                var id = rma + "-" + item.productID;
                var returnOrderItem = CustomObjectMgr.createCustomObject("ReturnOrderItem", id);
                returnOrderItem.custom.RMA = rma;
                returnOrderItem.custom.itemRefundAmount = (item.adjustedPrice * item.selectedquantity);
                returnOrderItem.custom.productID = item.productID;
                returnOrderItem.custom.productName = item.productName;
                returnOrderItem.custom.quantity = item.selectedquantity;
                returnOrderItem.custom.status = parseInt(returnOH.ReturnStatus.CREATED, 10);
                returnOrderItem.custom.comments = item.comments;
                returnOrderItem.custom.reasonReturn = parseInt(item.reasonReturn, 10);
                itemsTotalCost += item.adjustedPrice * item.selectedquantity;
                ttlItems += item.selectedquantity
            }
            returnOrder.custom.itemsTotal = ttlItems;
            returnOrder.custom.itemsTotalCost = new Number(itemsTotalCost);
            if (costPref.amountToFreeReturn <= itemsTotalCost){
                returnOrder.custom.refundAmount = new Number(itemsTotalCost);
            }else{
                returnOrder.custom.refundAmount = new Number(itemsTotalCost - (costPref.returnCost));
            }

        });

        res.json({
            success: 'success',
            error: '',
            rma : rma,
            continueUrl: URLUtils.url("Return-Confirm").toString()
        });

        return next();
    }
);

server.get(
    "Confirm",
    consentTracking.consent,
    server.middleware.https,
    csrfProtection.generateToken,
    function (req, res, next) {
        var OrderMgr = require("dw/order/OrderMgr");
        var returnOH = require("*/cartridge/scripts/helpers/returnOrderHelper");
        var site = require("dw/system/Site");
        var URLUtils = require("dw/web/URLUtils");
        var currentSite = site.getCurrent();

        if (!require("helpers").sitePreference("enableTransactionalSite", true)) {
            res.redirect(URLUtils.url("Home-Show"));
            return next();
        }

        var lastRMA = Object.prototype.hasOwnProperty.call(req.session.raw.custom, "RMA") ? req.session.raw.custom.RMA : null;
        if (lastRMA === req.querystring.RMA) {
            res.redirect(URLUtils.url("Home-Show"));
            return next();
        }

        var rma = req.querystring.RMA;
        var returnOrder = returnOH.getReturnOrderByRMA(rma);
        var order = OrderMgr.getOrder(returnOrder.orderNO);
        returnOrder.customerEmail = order.customerEmail;
        if(order.customer.registered){
            returnOrder.firstName = order.customer.profile.firstName;
            returnOrder.lastName = order.customer.profile.lastName;
        }else{
            returnOrder.firstName = order.customerName;
        }
        var orderDetailsURL = URLUtils.url("Order-Details", "orderID", returnOrder.orderNO).relative().toString();
        var url = currentSite.httpsHostName + orderDetailsURL;
        returnOrder.url = url;

        returnOH.sendReturnConfirmationEmail(returnOrder, getReturnItems(rma));

        res.render("return/confirmation/confirmation", {
            returnOrder: returnOrder,
            returningCustomer: true
        });
        req.session.raw.custom.RMA = req.querystring.RMA; // eslint-disable-line no-param-reassign
        return next();

    });

server.get(
    "List",
    consentTracking.consent,
    server.middleware.https,
    csrfProtection.generateToken,
    function (req, res, next) {
        var OrderMgr = require("dw/order/OrderMgr");

        var queryReturnOrder= "custom.customerID = '"+req.currentCustomer.profile.customerNo+"'";
        var returnOrder = CustomObjectMgr.queryCustomObjects("ReturnOrder", queryReturnOrder, null);
        var orders=[];
        if (returnOrder.count>0) {
            var returnOrderArray = returnOrder.asList().toArray();
            let c=0;
            returnOrderArray.forEach(function (returnOrder) {
                let detailsOrder = OrderMgr.getOrder(returnOrder.custom.orderNO);
                let item = {
                    creationDate: returnOrder.creationDate,
                    custom: returnOrder.custom,
                    image: (ProductMgr.getProduct(detailsOrder.productLineItems[0].productID)).getImage('small',1)                    
                };
                if(returnOrder.custom.status.value === 3 || returnOrder.custom.status.value === 4 ){
                    item.statusDescription= statusReturn[4].valor;
                }else{
                    item.statusDescription= statusReturn[returnOrder.custom.status.value].valor;
                }
                orders[c]=item;
                c++;
            });
        };

        res.render('account/order/historyReturn', {
            orders: orders,
            filterValues: null,
            orderFilter: null,
            accountlanding: false,
            breadcrumbs: null
        });

        return next();
});

server.get(
    'DetailsReturn',
    consentTracking.consent,
    server.middleware.https,
    userLoggedIn.validateLoggedIn,
    function (req, res, next) {
        var OrderMgr = require('dw/order/OrderMgr');
        var orderHelpers = require('*/cartridge/scripts/order/orderHelpers');
        var rma=req.querystring.orderID;
        var queryReturnOrder= "custom.RMA = '"+rma+"'";

        var returnOrder = CustomObjectMgr.queryCustomObjects("ReturnOrder", queryReturnOrder, null);
        var order=[];
        if (returnOrder.count>0) {
            var returnOrderArray = returnOrder.asList().toArray();
            let c=0;
            returnOrderArray.forEach(function (returnOrder) {
                let item={};
                if(returnOrder.custom.status.value === 3 || returnOrder.custom.status.value === 4 ){
                    item.statusDescription= statusReturn[4].valor;
                }else{
                    item.statusDescription= statusReturn[returnOrder.custom.status.value].valor;
                }
                item.order=returnOrder;
                order[c]=item;
                c++;
            });
        };

        let detailsOrder = OrderMgr.getOrder(order[0].order.custom.orderNO);
        let arrayDetails = [];

        
        for (let index = 0; index < (detailsOrder.productLineItems).length; index++) {
            let product = detailsOrder.productLineItems[index].product;
            let detail ={
                color: product.custom.refinementColor.displayValue,
                size: product.custom.size,
            };
            arrayDetails[index]=detail;
        }

        var returnItems = CustomObjectMgr.queryCustomObjects("ReturnOrderItem", queryReturnOrder, null);
        var items=[];

        if (returnItems.count>0) {
            var returnOrderArray = returnItems.asList().toArray();
            let c=0;
            returnOrderArray.forEach(function (returnItems) {
                let item = {
                    creationDate: returnItems.creationDate,
                    custom: returnItems.custom,
                    image: (ProductMgr.getProduct(returnItems.custom.productID)).getImage('small',1)
                };
               
                // image: (ProductMgr.getProduct(product.ID)).getImage('small',1)
                items[c]=item;
                c++;
            });
        };

        res.render('account/orderDetailsReturn', {
            orders: items,
            order: order,
            details: arrayDetails,
            exitLinkText: null,
            exitLinkUrl: null,
            breadcrumbs: null
        });
        
        next();
    }
);


/**
 * Order-Details : This endpoint is called to get Track Order Details
 * @name Base/Order-Details
 * @function
 * @memberof Order
 * @param {middleware} - consentTracking.consent
 * @param {middleware} - server.middleware.https
 * @param {middleware} - userLoggedIn.validateLoggedIn
 * @param {querystringparameter} - orderID - Order ID
 * @param {querystringparameter} - orderFilter - Order Filter ID
 * @param {category} - sensitive
 * @param {serverfunction} - get
 */
server.get(
    'Tracking',
    consentTracking.consent,
    server.middleware.https,
    function (req, res, next) {
        var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
        var serviceOrderTracking = require("*/cartridge/scripts/services/oms");
        var collections = require('*/cartridge/scripts/util/collections');
        var URLUtils = require('dw/web/URLUtils');
        let rma=req.querystring.rma;
        var service = serviceOrderTracking.getTrackingOrder(rma);

        let element =[]

        if(service.history){
            for (let index = 0; index < service.history.length; index++) {
                let item={
                    date: (service.history[index].date).substring(0,10),
                    time: (service.history[index].date).substring(11,19),
                    observations: service.history[index].observations ? service.history[index].observations : '',
                    statusDescription: statusReturn[service.history[index].status].valor
                };
                element[index] = item;
            }
        }else{
            return "";
        }

        service.history=element;
        var queryReturnOrder= "custom.RMA = '"+rma+"'";
        var returnOrder = CustomObjectMgr.queryCustomObjects("ReturnOrder", queryReturnOrder, null);
        if (returnOrder.count>0) {
            var returnOrderArray = returnOrder.asList().toArray();
        };

        if(service.statusCode === "ERROR"){
            res.json({
                error: true,
                fieldErrors: [],
                serverErrors: []
            });
            next();
        }

        res.render('account/order/returnTracking', {
            tracking: service,
            orderReturn: returnOrderArray[0]
        });
        next();
    }
);

function getReturnItems(rma) {
    var ProductMgr = require('dw/catalog/ProductMgr');
    var OrderMgr = require('dw/order/OrderMgr');
        var orderHelpers = require('*/cartridge/scripts/order/orderHelpers');
        var queryReturnOrder= "custom.RMA = '"+rma+"'";

        var returnOrder = CustomObjectMgr.queryCustomObjects("ReturnOrder", queryReturnOrder, null);
        var order=[];
        if (returnOrder.count>0) {
            var returnOrderArray = returnOrder.asList().toArray();
            let c=0;
            returnOrderArray.forEach(function (returnOrder) {
                order[c]=returnOrder;
                c++;
            });
        };

        let detailsOrder = OrderMgr.getOrder(order[0].custom.orderNO);
        let arrayDetails = [];

        
        for (let index = 0; index < (detailsOrder.productLineItems).length; index++) {
            let product = detailsOrder.productLineItems[index].product;
            let detail ={
                color: product.custom.refinementColor.displayValue,
                size: product.custom.size,
                id: product.ID
            };
            arrayDetails[index]=detail;
        }

        var returnItems = CustomObjectMgr.queryCustomObjects("ReturnOrderItem", queryReturnOrder, null);
        var items=[];

        if (returnItems.count>0) {
            var returnOrderArray = returnItems.asList().toArray();
            let c=0;
            returnOrderArray.forEach(function (returnItems) {
                let item={};
                //loop for all items of order, and compare id for get size and color
                arrayDetails.forEach(function (element) {
                    if (returnItems.custom.productID === element.id){
                        item = {color: element.color,
                                size: element.size
                        }
                    }
                });

                item.creationDate= returnItems.creationDate;
                item.custom= returnItems.custom;
                item.image= (ProductMgr.getProduct(returnItems.custom.productID)).getImage('small',0);
                let detailItem=ProductMgr.getProduct(returnItems.custom.productID);
                items[c]=item;
                c++;
            });
        };

    let ordeReturn={
        name: detailsOrder.customerName,
        order: order,
        items: items
    };

   return  ordeReturn ;
}


module.exports = server.exports();
