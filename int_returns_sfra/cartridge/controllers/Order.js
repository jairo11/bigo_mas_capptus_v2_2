"use strict";

const server = require("server");

server.extend(module.superModule);

var userLoggedIn = require("*/cartridge/scripts/middleware/userLoggedIn");
var consentTracking = require("*/cartridge/scripts/middleware/consentTracking");
var URLUtils = require("dw/web/URLUtils");
var csrfProtection = require("*/cartridge/scripts/middleware/csrf");
var Resource = require("dw/web/Resource");
var validate = require("*/cartridge/scripts/features/validate");



server.append("History",
    consentTracking.consent,
    server.middleware.https,
    userLoggedIn.validateLoggedIn,
    function (req, res, next) {
        var returnOH = require("*/cartridge/scripts/helpers/returnOrderHelper");
        if (returnOH.isReturnsEnabled()) {
            res.setViewData({isReturnsEnabled : true});
        }
        next();
    });

server.append("Track",
    consentTracking.consent,
    server.middleware.https,
    csrfProtection.validateRequest,
    csrfProtection.generateToken,
    function (req, res, next) {
        var returnOH = require("*/cartridge/scripts/helpers/returnOrderHelper");
        var returnOrders = returnOH.getReturnsOrders(req.querystring.trackOrderNumber);
        res.setViewData({returnOrders : returnOrders});
        next();
    });

server.append("Details",
    consentTracking.consent,
    server.middleware.https,
    userLoggedIn.validateLoggedIn,
    function (req, res, next) {
        var returnOH = require("*/cartridge/scripts/helpers/returnOrderHelper");
        var returnOrders = returnOH.getReturnsOrders(req.querystring.orderID);
        res.setViewData({returnOrders : returnOrders});
        next();
    });

server.replace(
    "CreateAccount",
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var OrderMgr = require("dw/order/OrderMgr");
        var CustomerMgr = require("dw/customer/CustomerMgr");
        var formErrors = require("*/cartridge/scripts/formErrors");
        var wishlistHelpers = require("*/cartridge/scripts/features/wishlistHelpers");

        var guestCustomer = req.currentCustomer.raw;

        var passwordForm = server.forms.getForm("newPasswords");
        var newPassword = passwordForm.newpassword.htmlValue;
        var confirmPassword = passwordForm.newpasswordconfirm.htmlValue;
        if (newPassword !== confirmPassword) {
            passwordForm.valid = false;
            passwordForm.newpasswordconfirm.valid = false;
            passwordForm.newpasswordconfirm.error =
                Resource.msg("error.message.mismatch.newpassword", "forms", null);
        }

        if (!CustomerMgr.isAcceptablePassword(newPassword) || /\s/g.test(newPassword)) {
            passwordForm.newpassword.valid = false;
            passwordForm.newpasswordconfirm.valid = false;
            passwordForm.newpassword.error = validate.validatePassword(newPassword);
            passwordForm.valid = false;
        }

        var order = OrderMgr.getOrder(req.querystring.ID);
        if (!order || order.customer.ID !== req.currentCustomer.raw.ID || order.getUUID() !== req.querystring.UUID) {
            res.json({ error: [Resource.msg("error.message.unable.to.create.account", "login", null)] });
            return next();
        }
        res.setViewData({ orderID: req.querystring.ID });
        var registrationObj = {
            firstName: order.billingAddress.firstName,
            lastName: order.billingAddress.lastName,
            phone: order.billingAddress.phone,
            email: order.customerEmail,
            password: newPassword
        };

        if (passwordForm.valid) {
            res.setViewData(registrationObj);
            // eslint-disable no-shadow
            this.on("route:BeforeComplete", function (req, res) { //NOSONAR
            // eslint-enable no-shadow
                var Transaction = require("dw/system/Transaction");
                var accountHelpers = require("*/cartridge/scripts/helpers/accountHelpers");
                var addressHelpers = require("*/cartridge/scripts/helpers/addressHelpers");
                var returnOH = require("*/cartridge/scripts/helpers/returnOrderHelper");

                var registrationData = res.getViewData();

                var login = registrationData.email;
                var password = registrationData.password;
                var newCustomer;
                var authenticatedCustomer;
                var newCustomerProfile;
                var errorObj = {};

                delete registrationData.email;
                delete registrationData.password;

                // attempt to create a new user and log that user in.
                try {
                    Transaction.wrap(function () {
                        var error = {};
                        newCustomer = CustomerMgr.createCustomer(login, password);

                        var authenticateCustomerResult = CustomerMgr.authenticateCustomer(login, password);
                        if (authenticateCustomerResult.status !== "AUTH_OK") {
                            error = { authError: true, status: authenticateCustomerResult.status };
                            throw error;
                        }

                        authenticatedCustomer = CustomerMgr.loginCustomer(authenticateCustomerResult, false);

                        if (!authenticatedCustomer) {
                            error = { authError: true, status: authenticateCustomerResult.status };
                            throw error;
                        } else {
                            wishlistHelpers.mergeGuestListToLoggedInCustomer(guestCustomer, authenticatedCustomer, req);
                            // assign values to the profile
                            newCustomerProfile = newCustomer.getProfile();

                            newCustomerProfile.firstName = registrationData.firstName;
                            newCustomerProfile.lastName = registrationData.lastName;
                            newCustomerProfile.phoneHome = registrationData.phone;
                            newCustomerProfile.email = login;

                            order.setCustomer(newCustomer);

                            // save all used shipping addresses to address book of the logged in customer
                            var allAddresses = addressHelpers.gatherShippingAddresses(order);
                            allAddresses.forEach(function (address) {
                                addressHelpers.saveAddress(address, { raw: newCustomer }, addressHelpers.generateAddressName(address));
                            });

                            returnOH.mergeOdersToCreatedCustomer(newCustomer);
                        }
                    });
                } catch (e) {
                    errorObj.error = true;
                    errorObj.errorMessage = e.authError
                        ? Resource.msg("error.message.unable.to.create.account", "login", null)
                        : Resource.msg("error.message.username.invalid", "forms", null);
                }

                if (errorObj.error) {
                    res.json({ error: [errorObj.errorMessage] });

                    return;
                }

                accountHelpers.sendCreateAccountEmail(authenticatedCustomer.profile);

                res.json({
                    success: true,
                    redirectUrl: URLUtils.url("Account-Show", "registration", "submitted").toString()
                });
            });
        } else {
            res.json({
                fields: formErrors.getFormErrors(passwordForm)
            });
        }

        return next();
    }
);

module.exports = server.exports();
