<isdecorate template="common/layout/page">
    <isscript>
        var assets = require('*/cartridge/scripts/assets.js');
        assets.addCss('/css/account/orderDetails.css');
        assets.addJs('/js/orderDetails.js');
        assets.addJs('/js/returnProduct.js');
    </isscript>

    <isset name="isAccountOrderDetails" value="${true}" scope="page" />

    <isslot id="account-orderreturns-banner" description="Account Order Detail Banner" context="global" />

    <div class="container receipt">
        <!---Breadcrumbs--->
        <isinclude template="components/breadcrumbs/pageBreadcrumbs"/>
        <div class="row justify-content-center mb-8">
            <div class="confirmation-details-wrapper ${isAccountOrderDetails ? 'col-12 col-sm-8 col-md-6': ''}">
                <div class="card confirm-details">
                    <div class="card-header">
                        <h2 class="card-header-custom">${Resource.msg('title.receipt', 'return', null)}</h2>
                    </div>
                    <div class="card-body">
                        <p>
                            <span class="summary-section-label order-number-label">${Resource.msg('label.order.number', 'confirmation', null)}</span>
                            <span class="summary-details order-number">${pdict.order.orderNumber}</span>
                            <br/>
                            <span class="summary-section-label order-date-label">${Resource.msg('label.order.date', 'confirmation', null)}</span>
                            <span class="summary-details order-date"><isprint value="${pdict.order.creationDate}" style="DATE_SHORT"/></span>
                            <br/>
                            <span class="summary-section-label order-returns-before-date-label">${Resource.msg('label.returns.before.date', 'return', null)}</span>
                            <span class="summary-details order-returns-before-date"><isprint value="${pdict.returnsBeforeDate}" style="DATE_SHORT"/></span>
                        </p>
                    </div>
                </div>

                <isset name="miniCart" value="${false}" scope="page" />
                <div class="card order-product-summary">
                    <div class="card-body">
                        <div class="row leading-lines">
                            <div class="col-6 start-lines">
                                <span class="order-receipt-label grand-total-label">${Resource.msgf('label.number.items.in.cart','cart', null, pdict.order.items.totalQuantity)}</span>
                            </div>
                            <div class="col-6 text-right end-lines">
                                <span class="grand-total-price">${pdict.order.totals.grandTotal}</span>
                            </div>
                            <hr class="col-12 p-0"/>
                        </div>
                        <div class="product-summary-block">
                            <isloop items="${pdict.order.shipping}" var="shippingModel">
                                <isloop items="${shippingModel.productLineItems.items}" var="lineItem">
                                    <isif condition="${lineItem.bonusProductLineItemUUID === 'bonus'}">
                                        <div class="card ${miniCart ? 'bonus-product-line-item' : ''} uuid-${lineItem.UUID}">
                                            <div class="card-body">
                                                <isinclude template="checkout/productCard/bonusProductCard" />
                                            </div>
                                        </div>
                                    <iselse/>
                                        <isinclude template="return/productCard" />
                                    </isif>
                                </isloop>
                            </isloop>
                        </div>
                    </div>
                </div>
            </div>
            <div class="order-totals-wrapper ${isAccountOrderDetails ? 'col-12 col-sm-8 col-md-6': ''}">
                <div class="card checkout-order-total-summary">
                    <div class="card-body order-total-summary">
                        <div class="panel panel-default">
                            <div class="panel-heading"></div>
                            <div class="panel-body">
                                <div class="row item-list">
                                    <div class="col-sm-4"><strong>${Resource.msg('return.order.item', 'return', null)}</strong></div>
                                    <div class="col-sm-4"><strong>${Resource.msg('return.order.quantity', 'return', null)}</strong></div>
                                    <div class="col-sm-4"><strong>${Resource.msg('return.order.grossPrice', 'return', null)}</strong></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <isinclude template="return/productLevelReturnButton" />
            </div>
            <div class="col-12 my-account text-center">
                <a href="${pdict.exitLinkUrl}" title="${Resource.msg('link.orderdetails.myaccount','account',null)}" aria-label="${Resource.msg('link.orderdetails.myaccount','account',null)}">
                    ${pdict.exitLinkText}
                </a>
            </div>
        </div>
    </div>
</isdecorate>
