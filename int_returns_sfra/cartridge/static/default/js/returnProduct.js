/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./cartridges/int_returns_sfra/cartridge/client/default/js/returnProduct.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./cartridges/app_storefront_base/cartridge/client/default/js/components/scrollAnimate.js":
/*!************************************************************************************************!*\
  !*** ./cartridges/app_storefront_base/cartridge/client/default/js/components/scrollAnimate.js ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (element) {
    var position = element && element.length ? element.offset().top : 0;
    $('html, body').animate({
        scrollTop: position
    }, 500);
    if (!element) {
        $('.logo-home').focus();
    }
};


/***/ }),

/***/ "./cartridges/int_returns_sfra/cartridge/client/default/js/returnProduct.js":
/*!**********************************************************************************!*\
  !*** ./cartridges/int_returns_sfra/cartridge/client/default/js/returnProduct.js ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var scrollAnimate = __webpack_require__(/*! base/components/scrollAnimate */ "./cartridges/app_storefront_base/cartridge/client/default/js/components/scrollAnimate.js");
var total = 0;
var subtotalReturn = 0;
$(document).ready(function () {
    /**
    * Procesa la confirmacion de la devolucion
    */
    $(document).on("click", ".btn-request-return-submit", function () {
        console.log("btn-request-return-submit------->");
        var isChecked = $("#checkReturnPolicy").is(":checked");
        $("#checkReturnPolicy").removeClass("is-invalid");
        if (!isChecked) {
            //defer.reject({ error: true, message: 'Debe aceptar terminos y condiciones' });
            $("#checkReturnPolicy").addClass("is-invalid");
            return;
        }
        if(processData()){
           return;
        }
        $.spinner().start();

        var form = {
            data: $("#returnData").val(),
            returnShippingCost: $("#returnShippingCost").val(),
            total: total,
            csrf_token: $(this).closest("form").find("input[name=\"csrf_token\"]").val()
        };

        $.ajax({
            url: $(this).attr("data-url"),
            method: "POST",
            data: form,
            success: function (data) {
                if (!data.error) {
                    var continueUrl = data.continueUrl;
                    var urlParams = {
                        RMA: data.rma
                    };

                    continueUrl += (continueUrl.indexOf("?") !== -1 ? "&" : "?") +
                        Object.keys(urlParams).map(function (key) {
                            return key + "=" + encodeURIComponent(urlParams[key]);
                        }).join("&");

                    $.spinner().stop();
                    window.location.href = continueUrl;
                } else {
                    $('.error-message').show();
                    $('.error-message-text').text(data.error);
                    scrollAnimate($('.error-message'));
                    /*createErrorNotification(data.error);
                    $(".btn-product-return-submit").prop("disabled", true);*/
                    $.spinner().stop();
                }
            },
            error: function () {
                // createErrorNotification("LA SOLICITUD DE DEVOLUCION NO SE PUDO PROCESAR");
                // $(".btn-product-return-submit").prop("disabled", true);
                // $.spinner().stop();
                $('.error-message').show();
                $('.error-message-text').text("LA SOLICITUD DE DEVOLUCION NO SE PUDO PROCESAR");
                scrollAnimate($('.error-message'));
                /*createErrorNotification(data.error);
                $(".btn-product-return-submit").prop("disabled", true);*/
                $.spinner().stop();
            }
        });
    });

    $(document).on("change", ".form-control", function () {
        console.log("--> form-control");
        //var checked = document.getElementById("checkbox-"+ this.dataset.uuid).checked;
        var subtotalLineItem = 0;
        if (this.id.startsWith("returnQuantity") && document.getElementById("checkbox-"+ this.dataset.uuid).checked) {
            var unitPrice = document.getElementById("checkbox-"+ this.dataset.uuid).dataset.unitprice;
            var tempSelectedQuantity = document.getElementById("checkbox-"+ this.dataset.uuid).dataset.selectedquantity;
            var returnshippingcost = document.getElementById("checkbox-"+ this.dataset.uuid).dataset.returnshippingcost;
            subtotalLineItem = (unitPrice * tempSelectedQuantity);
            subtotalReturn -= subtotalLineItem;
            var selectedQuantity = parseInt(this.options[this.selectedIndex].value, 10);
            subtotalLineItem = (unitPrice * selectedQuantity);
            $(".item-subtotal-" + this.dataset.uuid).text("$" + subtotalLineItem.toFixed(2));
            subtotalReturn += subtotalLineItem;
            $(".return-sub-total").text("$" + subtotalReturn.toFixed(2));
            document.getElementById("checkbox-"+ this.dataset.uuid).dataset.selectedquantity = parseInt(selectedQuantity, 10);
            calculateTotal(returnshippingcost);
            $(".return-grand-total-sum").text("$" + total.toFixed(2));
        }
    });

    $(document).on("click", ".returnCheckBox", function () {
        console.log("--> returnCheckBox");
        var length = $("input[class='returnCheckBox']:checked").length;
        var unitPrice;
        var selectValue = "";
        var subtotalLineItem = 0;
        var shippingCost=this.dataset.returnshippingcost;
        var itemUuid=this.dataset.uuid;

        if (this.checked) {
            if (this.dataset.selecteablequantity>1) {
                selectValue= parseInt($("#" + "returnQuantity-" + this.dataset.uuid +" option:selected").val(), 10);
            } else if (this.dataset.selecteablequantity == 1) {
                selectValue= 1;
            }
            this.dataset.selectedquantity = selectValue;
            unitPrice = this.dataset.unitprice;
            subtotalLineItem = (unitPrice * selectValue);
            subtotalReturn += subtotalLineItem;
            // var shippingCost=this.dataset.returnshippingcost;
            if(subtotalReturn >= this.dataset.amountfree){
                shippingCost="0.00";
            }
            $('.return-shipping-total-cost').text("$"+parseFloat(shippingCost).toFixed(2));
            if(subtotalLineItem < 0) subtotalLineItem=0;
            $(".item-subtotal-" + this.dataset.uuid).text("$" + subtotalLineItem.toFixed(2));
            console.log("subtotalReturn devolucion despues--> " + subtotalReturn);
            $(".return-sub-total").text("$" + subtotalReturn.toFixed(2));

            manageItemClasses([document.getElementById(`returnQuantity-${itemUuid}`), document.getElementById(`returnReason-${itemUuid}`)], "remove", 'is-disabled');
            // manageItemClasses(document.getElementsByClassName("arrow-select"), "remove", 'disabled-arrow');

        } else {
            selectValue= parseInt($("#" + "returnQuantity-" + this.dataset.uuid +" option:selected").val(), 10);
            unitPrice = this.dataset.unitprice;
            subtotalLineItem = (unitPrice * selectValue);
            subtotalReturn -= subtotalLineItem;
            if(subtotalReturn >= this.dataset.amountfree){
                shippingCost="0.00";
            }
            $('.return-shipping-total-cost').text("$"+ parseFloat(shippingCost).toFixed(2));
            $(".item-subtotal-" + this.dataset.uuid).text("$0.00");
            this.dataset.selectedquantity = 0;
            console.log("subtotalReturn--> " + subtotalReturn);
            $(".return-sub-total").text("$" + subtotalReturn.toFixed(2));

            manageItemClasses([document.getElementById(`returnQuantity-${itemUuid}`), document.getElementById(`returnReason-${itemUuid}`)], "add", 'is-disabled');
            // manageItemClasses(document.getElementsByClassName("arrow-select"), "add", 'disabled-arrow');

        }
        calculateTotal(shippingCost, this.dataset.amountfree);
        $(".return-grand-total-sum").text("$" + total.toFixed(2));
        if (length > 0) {
            document.getElementById("return-product-button").disabled = false;
        } else {
            document.getElementById("return-product-button").disabled = true;

        }
    });

    $(document).on("hidden.bs.modal", "#returnModal-product", function () {
        $(".lineitems-modal").remove();
        $(".products-price").remove();
	    $(".alert-warning").remove();
        $(".shipping-price").remove();
        $(".total-refound").remove();
        $(".alert-danger").remove();
        $(".btn-product-return-submit").prop("disabled", false);
    });

    $(document).on("show.bs.modal", "#returnModal-product", function (e) {
        var button = e.relatedTarget;
        if ($(button).hasClass("no-modal")) {
            e.stopPropagation();
        }
    });

    $("#myModal").on("show.bs.modal", function (e) {
        var button = e.relatedTarget;
        if ($(button).hasClass("no-modal")) {
            e.stopPropagation();
        }
    });

    $(document).on("click", ".btn-product-return-view", function () {

        console.log("--> btn-product-return-view");
        //$(".modal-body").spinner().start();
        $.spinner().start();
        var returnSubmitUrl = $(this).attr("data-url");

        var requiredInputFlag= showModalReturnResume();
        var returnData = $("#returnData").val();
        var refoundAmount = $("#refoundAmount").val();
        var form = {
            data: returnData,
            refoundAmount: refoundAmount,
            csrf_token: $(this).closest("form").find("input[name=\"csrf_token\"]").val()
        };
        if (!requiredInputFlag) {
            $.ajax({
                url: returnSubmitUrl,
                method: "POST",
                data: form,
                success: function (data) {
                    console.log("create");
                    if (!data.error) {
                        $("#" + "paragraph-products-price").append("<span class=\"summary-details products-price\"> $ " + data.itemsCost + "</span>");
                        $("#" + "paragraph-shipping-price").append("<span class=\"summary-details shipping-price\">  $ " + data.returnShippingPrice + "</span>");
                        $("#" + "paragraph-total-refound").append("<span class=\"summary-details total-refound\">  $ " + data.amountToRefound + "</span>");
                        $("#priceID").val(data.priceID);
                        $("#returnShippingPrice").val(data.returnShippingPrice);
                        $("#amountToRefound").val(data.amountToRefound);
                        if(data.itemsCost < data.minimunAmountReturn){
                            createWarningNotification(data.warning);
                            $(".btn-product-return-submit").prop("disabled", true);
                        }
                        $.spinner().stop();
                    } else {
                        $.spinner().stop();
                        createErrorNotification(data.error);
                        $(".btn-product-return-submit").prop("disabled", true);
                    }
                },
                error: function () {
                    $.spinner().stop();
                    createErrorNotification("LA SOLICITUD DE DEVOLUCION NO SE PUDO PROCESAR");
                    $(".btn-product-return-submit").prop("disabled", true);
                }
            });
        } else {
            $.spinner().stop();
            createErrorNotification("Por favor llene los campos requeridos");
            $(".btn-product-return-submit").prop("disabled", true);
        }
    });

    function processData() {
        $("#returnData").val("");
        var chbox = $(".returnCheckBox");
        var returnData = [];
        //var amountToRefound = 0;
        var requireInputsdFlag= false;
        for (var i = 0; i < chbox.length; i++) {
            if (chbox[i].checked === true) {

                var orderLineItem = JSON.parse(chbox[i].dataset.selecteditem);
                //var reasonReturnTxt = $("#" + "returnReason-" + orderLineItem.UUID +" option:selected").attr("label");
                var orderLineItemReturn={};
                orderLineItemReturn.selectedquantity = parseInt(chbox[i].dataset.selectedquantity, 10);
                orderLineItemReturn.quantity = parseFloat(chbox[i].dataset.itemsquantity).toFixed(2);
                orderLineItemReturn.reasonReturn = parseInt($("#" + "returnReason-" + orderLineItem.UUID +" option:selected").val(), 10);
                //orderLineItemReturn.comments = $.trim($("#"+ "comment-"+ orderLineItem.UUID).val());
                if (validateElement(orderLineItemReturn.reasonReturn, "returnReason-" + orderLineItem.UUID) |
                    validateElement(orderLineItemReturn.selectedquantity, "returnQuantity-" + orderLineItem.UUID)
                        //|validateElement(orderLineItemReturn.comments, "comment-"+ orderLineItem.UUID)
                        )
                    {
                    requireInputsdFlag = true;
                }

                orderLineItemReturn.productID = orderLineItem.productID;
                orderLineItemReturn.productName = orderLineItem.productName;
                orderLineItemReturn.adjustedPrice = parseFloat(chbox[i].dataset.unitprice).toFixed(2);
                // $(".modal-item-list").append("<div id=\"modal-list-name" + "\" class=\"col-sm-4 lineitems-modal\">" + orderLineItem.productName + "</div>");
                // $(".modal-item-list").append("<div id=\"modal-list-quantity" + "\" class=\"col-sm-2 lineitems-modal\">" + (!orderLineItemReturn.selectedquantity?"":orderLineItemReturn.selectedquantity) + "</div>");
                // $(".modal-item-list").append("<div id=\"modal-list-reason" + "\" class=\"col-sm-2 lineitems-modal\">" + (reasonReturnTxt == "Selecciona"?"":reasonReturnTxt) + "</div>");
                // $(".modal-item-list").append("<div id=\"modal-list-comments" + "\" class=\"col-sm-2 lineitems-modal\">" + orderLineItemReturn.comments + "</div>");
                // $(".modal-item-list").append("<div id=\"modal-list-price" + "\" class=\"col-sm-2 lineitems-modal\">" + (!orderLineItemReturn.adjustedPrice? "": "$ " + (orderLineItemReturn.adjustedPrice).toFixed(2)) + "</div>");
                //amountToRefound += orderLineItemReturn.adjustedPrice;
                var lineItemObj = {
                    orderLineItem: JSON.stringify(orderLineItemReturn)
                };

                returnData.push(lineItemObj);
            }
        }
        //var amountToRefoundData = {amountToRefound : JSON.stringify(amountToRefound)};
        $("#returnData").val(JSON.stringify(returnData));
        //$("#refoundAmount").val(JSON.stringify(total));
        return requireInputsdFlag;
    }

    function validateElement(element, id) {
        var flag = false;
        if (!element || element == "") {
            addError(id);
            flag = true;
        } else {
            removeError(id);
        }
        return flag;
    }

    function addError(id) {
        var element = $("#"+id);
        if (!element.hasClass("is-invalid")) {
            element.addClass("is-invalid");
            element.after("<div class=\"invalid-feedback "+id+"-error"+"\">Este campo es requerido.</div>");
        }
    }

    function removeError(id) {
        var element = $("#"+id);
        element.removeClass("is-invalid");
        $("."+ id+"-error").remove();

    }

    function createErrorNotification(message) {
        var errorHtml = "<div class=\"alert alert-danger alert-dismissible valid-cart-error " +
            "fade show\" role=\"alert\">" +
            "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">" +
            "<span aria-hidden=\"true\">&times;</span>" +
            "</button>" + message + "</div>";
        $(".modal-error").append(errorHtml);
    }

    function createWarningNotification(message) {
	    var errorHtml = "<div class=\"alert alert-warning alert-dismissible valid-cart-error " +
            "fade show\" role=\"alert\">" +
            "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">" +
            "<span aria-hidden=\"true\">&times;</span>" +
            "</button>" + message + "</div>";
	    $(".modal-error").append(errorHtml);
	  }

    function calculateIndividualItemPrice(productLinePrice, productLineQuantity, selectedQuantity) {
        var itemsPrice = productLinePrice/ productLineQuantity;
        var selectedItemsPrice = itemsPrice * selectedQuantity;
        return selectedItemsPrice;
    }

    function calculateTotal(returnShippingCost, amountFree){
        if(subtotalReturn > amountFree){
            total = subtotalReturn;
        }else{
            total = subtotalReturn - new Number(returnShippingCost);
        }
        if (total < 0) total=0;
    }

    function manageItemClasses(elements, action, className) {
        if(elements.length > 0 && action && className) {
            for(let element of elements) {
                (action == "add")
                    ? element.classList.add(className)
                    : element.classList.remove(className);
            }
        }
    }


});


/***/ })

/******/ });
//# sourceMappingURL=returnProduct.js.map