"use strict";

var Site = require("dw/system/Site");
var CustomObjectMgr = require("dw/object/CustomObjectMgr");
var returnCore = require("*/cartridge/scripts/core/returnCore");

var templatesType = {
    returnOrderDetailsTable: {template: "mail/components/returnOrderDetailsTable", type: "returnOrder"}
};

const ReturnStatus = {
    CREATED: 1,
    WAITING: 2,
    RECEIVED: 3,
    ACCEPTED: 4,
    REFUNDED: 5,
    COMPLETED: 6,
    REJECTED: 7,
    PARCEL_RETURN: 8,
    EXPIRED_SHIPPING: 9
};

function sendReturnConfirmationEmail(returnOrder, modelReturn) {
    var emailHelpers = require("*/cartridge/scripts/helpers/emailHelpers");
    var Resource = require("dw/web/Resource");
    var siteHelper = require("helpers");

    var emailObj = {
        to: returnOrder.customerEmail,
        subject: Resource.msg('subject.return.confirmation.email', 'order', null),
        from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@testorganization.com',
        type: emailHelpers.emailTypes.orderConfirmation
    };
    emailHelpers.sendEmail(emailObj, 'email/confirmedReturnEmail', modelReturn);

}

function getQuantitySelectBoxOptions(quantity) {
    var selectQuantityOptions=[];
    for (var i = 0; i<quantity; i++) {
        selectQuantityOptions.push({"value":  parseInt(i+1, 10), "label": "" + (i+1) });
    }
    return selectQuantityOptions;
}

function getAdjustedPrice(lineItem, order) {
    var price;
    if (order.getPriceAdjustments().length > 0 && lineItem.getProratedPrice() != null) {
        var val = lineItem.getProratedPrice().value + (lineItem.getProratedPrice().value * lineItem.getTaxRate());
        price = Math.round(val * 100) / 100;
    } else {
        price = lineItem.getAdjustedGrossPrice().value;
    }
    return price;
}

function getReturInfo(orderID, productID, itemsQuantity) {
    var queryReturnOrder= "custom.orderNO = '" + orderID + "' and custom.status != " + ReturnStatus.EXPIRED_SHIPPING;
    var returnOrder = CustomObjectMgr.queryCustomObjects("ReturnOrder", queryReturnOrder, null);
    var returnedQuantity=0;
    var isReturnable = false;
    if (returnOrder.count>0) {
        var returnOrderArray = returnOrder.asList().toArray();
        returnOrderArray.forEach(function (returnOrder) {
            var queryReturnOrderItem = "custom.RMA = '" + returnOrder.custom.RMA + "' AND custom.productID = '" + productID + "' and custom.status != " + ReturnStatus.EXPIRED_SHIPPING;
            var returnOrderItem = CustomObjectMgr.queryCustomObjects("ReturnOrderItem", queryReturnOrderItem, null);
            if (returnOrderItem.count>0) {
                var returnOrderItemArray =  returnOrderItem.asList().toArray();
                returnOrderItemArray.forEach(function (returnOrderItem) {
                    returnedQuantity += returnOrderItem.custom.quantity;
                });
            } else {
                isReturnable = true;
            }
        });
        if (returnedQuantity >0) {
            isReturnable = returnedQuantity<itemsQuantity;
        }
    } else {
        isReturnable = true;
    }

    return {"isReturnable": isReturnable, "quantityToReturn": (itemsQuantity - returnedQuantity)};

}

function getReturnsOrders(orderID) {
    var reasonsCodes = returnCore.getAPIReturnOrder("getReturnReasonCodes", "");
    var returnStatus = returnCore.getAPIReturnOrder("getReturnStatus", "");
    var returnOrders =[];
    var queryReturnOrder= "custom.orderNO = '" + orderID + "'";
    var returnOrder = CustomObjectMgr.queryCustomObjects("ReturnOrder", queryReturnOrder, null);
    var returnOrderArray = returnOrder.asList().toArray();
    returnOrderArray.forEach(function (returnOrder) {
        returnOrder = parseReturnOrder(returnOrder);
        setStatusLabel(returnStatus, returnOrder);
        var queryReturnOrderItem = "custom.RMA = '" + returnOrder.RMA + "'";
        var returnOrderItem = CustomObjectMgr.queryCustomObjects("ReturnOrderItem", queryReturnOrderItem, null);
        var returnOrderItemArray =  returnOrderItem.asList().toArray();
        var returnOrderItems =[];
        returnOrderItemArray.forEach(function (returnOrderItem) {
            returnOrderItem= parseReturnOrderItem(returnOrderItem);
            setReasonLabel(reasonsCodes, returnOrderItem);
            returnOrderItems.push(returnOrderItem);
        });
        returnOrder.returnOrderItems= returnOrderItems;
        returnOrders.push(returnOrder);
    });
    return returnOrders;
}

function setStatusLabel(returnStatus, returnOrder) {
    for (var i=0;i<returnStatus.length;i++) {
        if (returnOrder.status == returnStatus[i].id) {
            returnOrder.statusLabel = returnStatus[i].statusCode;
        }
    }
}

function setReasonLabel(reasonsCodes, returnOrderItem) {
    for (var i=0;i<reasonsCodes.length;i++) {
        if (returnOrderItem.reasonReturn == reasonsCodes[i].id) {
            returnOrderItem.reasonLabel = reasonsCodes[i].reasonCode;
        }
    }
}

function getReturnOrderByRMA(RMA) {
    var returnOrder = CustomObjectMgr.getCustomObject("ReturnOrder", RMA);
    returnOrder = parseReturnOrder(returnOrder);
    var queryReturnOrderItem = "custom.RMA = '" + returnOrder.RMA + "'";
    var returnOrderItem = CustomObjectMgr.queryCustomObjects("ReturnOrderItem", queryReturnOrderItem, null);
    var returnOrderItemArray =  returnOrderItem.asList().toArray();
    var returnOrderItems=[];
    returnOrderItemArray.forEach(function (returnOrderItem) {
        returnOrderItems.push(parseReturnOrderItem(returnOrderItem));
    });
    returnOrder.returnOrderItems = returnOrderItems;
    return returnOrder;
}

function reasonCodesToSelectBox(reasons) {
    var reasonCodes =[];
    if(!reasons){return []}
    for (var i=0;i < reasons.length;i++) {
        var reason = reasons[i];
        reasonCodes.push({value: reason.id, label: reason.reasonCode});
    }
    return reasonCodes;
}

function parseReturnOrder(returnOrder) {
    var parsedReturnOrder={};
    parsedReturnOrder.RMA=returnOrder.custom.RMA;
    parsedReturnOrder.appliedRefund=returnOrder.custom.appliedRefund;
    parsedReturnOrder.creationDate=returnOrder.creationDate;
    parsedReturnOrder.dateCompleted=returnOrder.custom.dateCompleted;
    parsedReturnOrder.itemsTotalCost=returnOrder.custom.itemsTotalCost;
    parsedReturnOrder.lastModified=returnOrder.lastModified;
    parsedReturnOrder.orderNO=returnOrder.custom.orderNO;
    parsedReturnOrder.pdf=returnOrder.custom.pdf;
    parsedReturnOrder.refundAmount=returnOrder.custom.refundAmount;
    parsedReturnOrder.shippingPrice=returnOrder.custom.shippingPrice;
    parsedReturnOrder.status=returnOrder.custom.status;
    parsedReturnOrder.trackingNumber=returnOrder.custom.trackingNumber;
    return parsedReturnOrder;
}

function parseReturnOrderItem(returnOrderItem) {
    var parsedReturnOrderItem={};
    parsedReturnOrderItem.ID = returnOrderItem.custom.ID;
    parsedReturnOrderItem.RMA = returnOrderItem.custom.RMA;
    parsedReturnOrderItem.comments = returnOrderItem.custom.comments;
    parsedReturnOrderItem.creationDate = returnOrderItem.creationDate;
    parsedReturnOrderItem.dateCompleted = returnOrderItem.custom.dateCompleted;
    parsedReturnOrderItem.itemRefundAmount	 = returnOrderItem.custom.itemRefundAmount;
    parsedReturnOrderItem.lastModified = returnOrderItem.lastModified;
    parsedReturnOrderItem.productID = returnOrderItem.custom.productID;
    parsedReturnOrderItem.productName = returnOrderItem.custom.productName;
    parsedReturnOrderItem.quantity = returnOrderItem.custom.quantity;
    parsedReturnOrderItem.reasonReturn = returnOrderItem.custom.reasonReturn;
    parsedReturnOrderItem.status = returnOrderItem.custom.status;
    return parsedReturnOrderItem;
}

function isReturnsEnabled() {
    if (Site.getCurrent().getCustomPreferenceValue("enableReturns")) {
        return JSON.parse(Site.getCurrent().getCustomPreferenceValue("enableReturns"));
    }
    return false;
}

function checkReturnDate(order) {
    var creationDate = order.creationDate;
    var returnDate = new Date();
    var returnsWindow = 0;
    if (Site.getCurrent().getCustomPreferenceValue("returnsWindowOrders")) {
        returnsWindow = Site.getCurrent().getCustomPreferenceValue("returnsWindowOrders");
    }
    returnDate.setDate(creationDate.getDate() + returnsWindow);
    if (Date.now() <= returnDate.getTime()) {
        return true;
    }
    return false;
}

function getReturnsBeforeDate(order) {
    var creationDate = order.getCreationDate();
    var returnsWindow = 0;
    if (Site.getCurrent().getCustomPreferenceValue("returnsWindowOrders")) {
        returnsWindow = Site.getCurrent().getCustomPreferenceValue("returnsWindowOrders");
    }
    creationDate.setDate(creationDate.getDate() + returnsWindow);
    return creationDate;
}

function isFirstReturn(orderID) {
    var queryReturnOrder= "custom.orderNO = '" + orderID + "' and custom.status != " + ReturnStatus.EXPIRED_SHIPPING;
    var returnOrder = CustomObjectMgr.queryCustomObjects("ReturnOrder", queryReturnOrder, null);
    return !(returnOrder.count > 0);
}

function mergeOdersToCreatedCustomer(newCustomer){
    var OrderMgr = require("dw/order/OrderMgr");

    var query = "customerEmail LIKE {0}";
    var orders = OrderMgr.queryOrders( query, "creationDate desc",newCustomer.profile.email);
    if(orders.getCount()>0){
        var orderArray = orders.asList().toArray();
        orderArray.forEach(function (o) {
            o.setCustomer(newCustomer);
        });
    }
}

function getReturnPreferences(){
    return returnPreferences ={
        returnCost: Site.getCurrent().getCustomPreferenceValue("returnCost"),
        minimunAmountForReturn:  Site.getCurrent().getCustomPreferenceValue("minimunAmountReturn"),
        amountToFreeReturn: Site.getCurrent().getCustomPreferenceValue("amountFreeReturn")
    };
}

function getReturnShippingCost(order){
    var Site = require("dw/system/Site");
    var returnCost = JSON.parse(Site.getCurrent().getCustomPreferenceValue("returnCost"));
    var amountFreeReturn = JSON.parse(Site.getCurrent().getCustomPreferenceValue("amountFreeReturn"));
    if (isFirstReturn(order.orderNo) && (order.totalNetPrice >= amountFreeReturn)) {
        return 0;
    }else{
        return returnCost;
    }
}

module.exports = {
    sendReturnConfirmationEmail: sendReturnConfirmationEmail,
    getAdjustedPrice: getAdjustedPrice,
    getQuantitySelectBoxOptions: getQuantitySelectBoxOptions,
    getReturInfo: getReturInfo,
    isReturnsEnabled: isReturnsEnabled,
    checkReturnDate: checkReturnDate,
    getReturnsBeforeDate: getReturnsBeforeDate,
    getReturnsOrders: getReturnsOrders,
    getReturnOrderByRMA: getReturnOrderByRMA,
    ReturnStatus: ReturnStatus,
    reasonCodesToSelectBox: reasonCodesToSelectBox,
    isFirstReturn: isFirstReturn,
    mergeOdersToCreatedCustomer: mergeOdersToCreatedCustomer,
    getReturnPreferences: getReturnPreferences,
    getReturnShippingCost: getReturnShippingCost
};
