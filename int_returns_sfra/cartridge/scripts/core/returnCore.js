"use strict";

var returnOH = require("*/cartridge/scripts/helpers/returnOrderHelper");
var hookMgr = require("dw/system/HookMgr");

function getReturnOrderDetails(order) {
    var products = order.productLineItems;
    var items={};
    var returnReasons = returnOH.reasonCodesToSelectBox(getAPIReturnOrder("getReturnReasonCodes", ""));
    for (var i=0; i<products.length;i++) {
        var item={};
        var product = products[i];
        var returInfo = returnOH.getReturInfo(order.orderNo, product.productID, product.quantity.value);
        item.productID = product.productID;
        item.productName = product.productName;
        item.UUID = product.UUID;
        item.adjustedPrice = (product.getProratedPrice().value).toFixed(2);
        item.quantity = product.quantity.value;
        item.unitPrice = item.adjustedPrice / item.quantity;
        item.quantitySelectBox = returnOH.getQuantitySelectBoxOptions(returInfo.quantityToReturn);
        item.selecteableQuantity = parseInt(returInfo.quantityToReturn, 10);
        item.returnReasonSelectBox = returnReasons;
        item.isReturnable = returInfo.isReturnable;
        items[product.productID] = item;
    }

    var viewData = {};
    viewData.shippingStatus = order.shippingStatus.displayValue;
    viewData.returnShippingCost = returnOH.getReturnShippingCost(order);
    viewData.items = items;
    return viewData;
}

function getAPIBrokerDilivery(requestPayload) {
    var response;
    if (hookMgr.hasHook("app.services.cya.brokerDelivery")) {
        response = hookMgr.callHook("app.services.cya.brokerDelivery", "getShippingCost", requestPayload);
    }
    return response;
}

function getAPIReturnOrder(method, requestPayload) {
    var response;
    if (hookMgr.hasHook("app.services.cya.orderReturn")) {
        response = hookMgr.callHook("app.services.cya.orderReturn", method, requestPayload);
    }
    return response;
}

function setStatusReturnOrder(requestPayload) {
    var response;
    if (hookMgr.hasHook("app.services.cya.orderReturn")) {
        response = hookMgr.callHook("app.services.cya.orderReturn", "setStatusOMS", requestPayload);
    }
    return response;
}

module.exports = {
    getReturnOrderDetails: getReturnOrderDetails,
    getAPIBrokerDilivery: getAPIBrokerDilivery,
    getAPIReturnOrder: getAPIReturnOrder,
    setStatusReturnOrder: setStatusReturnOrder
};
