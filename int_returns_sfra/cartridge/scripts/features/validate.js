"use strict";

var Resource = require("dw/web/Resource");
var CustomerMgr = require("dw/customer/CustomerMgr");

/**
 * @description Validates input comparing with the constraints of customer passwords.
 * @see dw.customer.CustomerPasswordConstraints
 * @param {String} value - the string value to be validated
 * @returns {String} error message
 */
function validatePassword(value) {
    var constraintsPassword = CustomerMgr.getPasswordConstraints();
    var error = "";

    switch (true) {
        case (constraintsPassword.forceMixedCase && !(/[A-Z]/.test(value))):
            error = Resource.msg("error.uppercase", "forms", null);
            break;

        case (constraintsPassword.forceMixedCase && !(/[a-z]/.test(value))):
            error = Resource.msg("error.lowercase", "forms", null);
            break;

        case (constraintsPassword.forceNumbers && !(/\d/.test(value))):
            error = Resource.msg("error.numbers", "forms", null);
            break;

        case (/\s/g.test(value)):
            error = Resource.msg("error.spaces", "forms", null);
            break;

        default:
            error = Resource.msg("error.special.characters", "forms", null);
            break;
    }

    return error;
}

module.exports = {
    validatePassword: validatePassword
};
