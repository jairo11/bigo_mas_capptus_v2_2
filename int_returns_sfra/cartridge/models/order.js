"use strict";

var Order = require('dw/order/Order');

var base = module.superModule;

var DEFAULT_MODEL_CONFIG = {
    numberOfLineItems: "*"
};

/**
 * Order class that represents the current order
 * @param {dw.order.LineItemCtnr} lineItemContainer - Current users's basket/order
 * @param {Object} options - The current order's line items
 * @param {Object} options.config - Object to help configure the orderModel
 * @param {string} options.config.numberOfLineItems - helps determine the number of lineitems needed
 * @param {string} options.countryCode - the current request country code
 * @constructor
 */
function OrderModel(lineItemContainer, options) {
    // Initialize the base model prior
    base.call(this, lineItemContainer, options);
    if(lineItemContainer && Object.hasOwnProperty.call(lineItemContainer, 'shippingStatus') && Object.hasOwnProperty.call(lineItemContainer, 'status')){
        this.enableReturnButton = lineItemContainer.shippingStatus == Order.SHIPPING_STATUS_SHIPPED  && lineItemContainer.status == Order.ORDER_STATUS_COMPLETED;
    }
    // if (lineItemContainer) {
    //     var safeOptions = options || {};
    //     var modelConfig = safeOptions.config || DEFAULT_MODEL_CONFIG;
    //     if (modelConfig.numberOfLineItems === "single") {
    //         this.shippingStatus = lineItemContainer.shippingStatus.displayValue;
    //     }
    // }
}

module.exports = OrderModel;
