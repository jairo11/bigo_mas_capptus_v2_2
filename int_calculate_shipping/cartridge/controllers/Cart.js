"use strict";

const server = require("server");
server.extend(module.superModule);

server.append("Show", function (req, res, next) {
    var error = req.querystring.error ? req.querystring.error : false;
    if (error) {
        res.setViewData({ calculatedShipping:{error: error, message : req.querystring.errorMessage}});
    }
    return next();
});

module.exports = server.exports();
