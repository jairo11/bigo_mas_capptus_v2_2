"use strict";

var Logger = require("dw/system/Logger");
const server = require("server");
server.extend(module.superModule);

server.prepend("PlaceOrder", function (req, res, next) {
    var BasketMgr = require("dw/order/BasketMgr");
    var currentBasket = BasketMgr.getCurrentBasket();
    var calculatedShippingCostCache;
    if (currentBasket.shipments && currentBasket.shipments.length > 0) {
        calculatedShippingCostCache = currentBasket.shipments[0].custom.calculatedShippingCostCache;
    }
    res.setViewData({ calculatedShippingCostCache: calculatedShippingCostCache });
    next();
});

server.append("PlaceOrder", function (req, res, next) {
    var Transaction = require("dw/system/Transaction");
    var OrderMgr = require("dw/order/OrderMgr");
    var viewData = res.getViewData();
    try {
        if (Object.hasOwnProperty.call(viewData, "orderID") && Object.hasOwnProperty.call(viewData, "calculatedShippingCostCache")) {
            var calculatedShippingCostCache = JSON.parse(viewData.calculatedShippingCostCache);

            Transaction.wrap(function () {
                var order = OrderMgr.getOrder(viewData.orderID);
                order.custom.priceId = calculatedShippingCostCache.priceId;
            });
        }
    } catch (e) {
        Logger.error("Error trying to write order.custom.priceId to the order {0} at PlaceOrder : {1}", viewData.orderID, e);
    }
    next();
});

module.exports = server.exports();
