/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./cartridges/int_calculate_shipping/cartridge/client/default/js/checkout.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./cartridges/app_refapp/cartridge/client/default/js/checkout/address.js":
/*!*******************************************************************************!*\
  !*** ./cartridges/app_refapp/cartridge/client/default/js/checkout/address.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * Populate the Billing Address Summary View
 * @param {string} parentSelector - the top level DOM selector for a unique address summary
 * @param {Object} address - the address data
 */

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function populateAddressSummary(parentSelector, address) {
  $.each(address, function (attr) {
    var val = address[attr];
    $("." + attr, parentSelector).text(val || "");
  });
}
/**
 * returns a formed <option /> element
 * @param {Object} shipping - the shipping object (shipment model)
 * @param {boolean} selected - current shipping is selected (for PLI)
 * @param {order} order - the Order model
 * @param {Object} [options] - options
 * @returns {Object} - the jQuery / DOMElement
 */


function optionValueForAddress(shipping, selected, order, options) {
  var safeOptions = options || {};
  var isBilling = safeOptions.type && safeOptions.type === "billing";
  var className = safeOptions.className || "";
  var isSelected = selected;
  var isNew = !shipping;

  if (typeof shipping === "string") {
    return $("<option class=\"" + className + "\" disabled>" + shipping + "</option>");
  }

  var safeShipping = shipping || {};
  var shippingAddress = safeShipping.shippingAddress || {};

  if (isBilling && isNew && !order.billing.matchingAddressId) {
    shippingAddress = order.billing.billingAddress.address || {};
    isNew = false;
    isSelected = true;
    safeShipping.UUID = "manual-entry";
  }

  var uuid = safeShipping.UUID ? safeShipping.UUID : "new";
  var optionEl = $("<option class=\"" + className + "\" />");
  optionEl.val(uuid);
  var title;

  if (isNew) {
    title = order.resources.addNewAddress;
  } else {
    title = [];

    if (shippingAddress.firstName) {
      title.push(shippingAddress.firstName);
    }

    if (shippingAddress.lastName) {
      title.push(shippingAddress.lastName);
    }

    if (shippingAddress.address1) {
      title.push(shippingAddress.address1);
    }

    if (shippingAddress.address2) {
      title.push(shippingAddress.address2);
    }

    if (shippingAddress.city) {
      if (shippingAddress.state) {
        title.push(shippingAddress.city + ",");
      } else {
        title.push(shippingAddress.city);
      }
    }

    if (shippingAddress.stateCode) {
      title.push(shippingAddress.stateCode);
    }

    if (shippingAddress.postalCode) {
      title.push(shippingAddress.postalCode);
    }

    if (!isBilling && safeShipping.selectedShippingMethod) {
      title.push("-");
      title.push(safeShipping.selectedShippingMethod.displayName);
    }

    if (title.length > 2) {
      title = title.join(" ");
    } else {
      title = order.resources.newAddress;
    }
  }

  optionEl.text(title);
  var keyMap = {
    "data-first-name": "firstName",
    "data-last-name": "lastName",
    "data-address1": "address1",
    "data-address2": "address2",
    "data-city": "city",
    "data-state-code": "stateCode",
    "data-postal-code": "postalCode",
    "data-country-code": "countryCode",
    "data-phone": "phone"
  };
  $.each(keyMap, function (key) {
    var mappedKey = keyMap[key];
    var mappedValue = shippingAddress[mappedKey]; // In case of country code

    if (mappedValue && _typeof(mappedValue) === "object") {
      mappedValue = mappedValue.value;
    }

    optionEl.attr(key, mappedValue || "");
  });
  var giftObj = {
    "data-is-gift": "isGift",
    "data-gift-message": "giftMessage"
  };
  $.each(giftObj, function (key) {
    var mappedKey = giftObj[key];
    var mappedValue = safeShipping[mappedKey];
    optionEl.attr(key, mappedValue || "");
  });

  if (isSelected) {
    optionEl.attr("selected", true);
  }

  return optionEl;
}
/**
 * returns address properties from a UI form
 * @param {Form} form - the Form element
 * @returns {Object} - a JSON object with all values
 */


function getAddressFieldsFromUI(form) {
  var address = {
    firstName: $("input[name$=_firstName]", form).val(),
    lastName: $("input[name$=_lastName]", form).val(),
    address1: $("input[name$=_address1]", form).val(),
    address2: $("input[name$=_address2]", form).val(),
    city: $("input[name$=_city]", form).val(),
    postalCode: $("input[name$=_postalCode]", form).val(),
    stateCode: $("select[name$=_stateCode],input[name$=_stateCode]", form).val(),
    countryCode: $("select[name$=_country]", form).val(),
    phone: $("input[name$=_phone]", form).val()
  };
  return address;
}

module.exports = {
  methods: {
    populateAddressSummary: populateAddressSummary,
    optionValueForAddress: optionValueForAddress,
    getAddressFieldsFromUI: getAddressFieldsFromUI
  },
  showDetails: function showDetails() {
    $(".btn-show-details").on("click", function () {
      var form = $(this).closest("form");
      form.attr("data-address-mode", "details");
      form.find(".multi-ship-address-actions").removeClass("d-none");
      form.find(".multi-ship-action-buttons .col-12.btn-save-multi-ship").addClass("d-none");
    });
  },
  addNewAddress: function addNewAddress() {
    $(".btn-add-new").on("click", function () {
      var $el = $(this);

      if ($el.parents("#dwfrm_billing").length > 0) {
        // Handle billing address case
        $("body").trigger("checkout:clearBillingForm");
        var $option = $($el.parents("form").find(".addressSelector option")[0]);
        $option.attr("value", "new");
        var $newTitle = $("#dwfrm_billing input[name=localizedNewAddressTitle]").val();
        $option.text($newTitle);
        $option.prop("selected", "selected");
        $el.parents("[data-address-mode]").attr("data-address-mode", "new");
      } else {
        // Handle shipping address case
        var $newEl = $el.parents("form").find(".addressSelector option[value=new]");
        $newEl.prop("selected", "selected");
        $newEl.parent().trigger("change");
      }
    });
  }
};

/***/ }),

/***/ "./cartridges/app_refapp/cartridge/client/default/js/checkout/billing.js":
/*!*******************************************************************************!*\
  !*** ./cartridges/app_refapp/cartridge/client/default/js/checkout/billing.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var addressHelpers = __webpack_require__(/*! ./address */ "./cartridges/app_refapp/cartridge/client/default/js/checkout/address.js");

var cleave = __webpack_require__(/*! ../components/cleave */ "./cartridges/app_refapp/cartridge/client/default/js/components/cleave.js");
/**
 * updates the billing address selector within billing forms
 * @param {Object} order - the order model
 * @param {Object} customer - the customer model
 */


function updateBillingAddressSelector(order, customer) {
  var shippings = order.shipping;
  var form = $("form[name$=billing]")[0];
  var $billingAddressSelector = $(".addressSelector", form);
  var hasSelectedAddress = false;

  if ($billingAddressSelector && $billingAddressSelector.length === 1) {
    $billingAddressSelector.empty(); // Add New Address option

    $billingAddressSelector.append(addressHelpers.methods.optionValueForAddress(null, false, order, {
      type: "billing"
    })); // Separator -

    $billingAddressSelector.append(addressHelpers.methods.optionValueForAddress(order.resources.shippingAddresses, false, order, {
      // className: 'multi-shipping',
      type: "billing"
    }));
    shippings.forEach(function (aShipping) {
      var isSelected = order.billing.matchingAddressId === aShipping.UUID;
      hasSelectedAddress = hasSelectedAddress || isSelected; // Shipping Address option

      $billingAddressSelector.append(addressHelpers.methods.optionValueForAddress(aShipping, isSelected, order, {
        // className: 'multi-shipping',
        type: "billing"
      }));
    });

    if (customer.addresses && customer.addresses.length > 0) {
      $billingAddressSelector.append(addressHelpers.methods.optionValueForAddress(order.resources.accountAddresses, false, order));
      customer.addresses.forEach(function (address) {
        var isSelected = order.billing.matchingAddressId === address.ID;
        hasSelectedAddress = hasSelectedAddress || isSelected; // Customer Address option

        $billingAddressSelector.append(addressHelpers.methods.optionValueForAddress({
          UUID: "ab_" + address.ID,
          shippingAddress: address
        }, isSelected, order, {
          type: "billing"
        }));
      });
    }
  }

  if (hasSelectedAddress || !order.billing.matchingAddressId && order.billing.billingAddress.address) {
    // show
    $(form).attr("data-address-mode", "edit");
  } else {
    $(form).attr("data-address-mode", "new");
  }

  $billingAddressSelector.show();
}
/**
 * updates the billing address form values within payment forms
 * @param {Object} order - the order model
 */


function updateBillingAddressFormValues(order) {
  var billing = order.billing;
  if (!billing.billingAddress || !billing.billingAddress.address) return;
  var form = $("form[name=dwfrm_billing]");
  if (!form) return;
  $("input[name$=_firstName]", form).val(billing.billingAddress.address.firstName);
  $("input[name$=_lastName]", form).val(billing.billingAddress.address.lastName);
  $("input[name$=_address1]", form).val(billing.billingAddress.address.address1);
  $("input[name$=_address2]", form).val(billing.billingAddress.address.address2);
  $("input[name$=_city]", form).val(billing.billingAddress.address.city);
  $("input[name$=_postalCode]", form).val(billing.billingAddress.address.postalCode);
  $("select[name$=_stateCode],input[name$=_stateCode]", form).val(billing.billingAddress.address.stateCode);
  $("select[name$=_country]", form).val(billing.billingAddress.address.countryCode.value);
  $("input[name$=_phone]", form).val(billing.billingAddress.address.phone);
  $("input[name$=_email]", form).val(order.orderEmail);

  if (billing.payment && billing.payment.selectedPaymentInstruments && billing.payment.selectedPaymentInstruments.length > 0) {
    var instrument = billing.payment.selectedPaymentInstruments[0];
    $("select[name$=expirationMonth]", form).val(instrument.expirationMonth);
    $("select[name$=expirationYear]", form).val(instrument.expirationYear); // Force security code and card number clear

    $("input[name$=securityCode]", form).val("");
    $("input[name$=cardNumber]").data("cleave").setRawValue("");
  }
}
/**
 * clears the billing address form values
 */


function clearBillingAddressFormValues() {
  updateBillingAddressFormValues({
    billing: {
      billingAddress: {
        address: {
          countryCode: {}
        }
      }
    },
    orderEmail: $("input[name$=email]").val()
  });
}
/**
 * Updates the billing information in checkout, based on the supplied order model
 * @param {Object} order - checkout model to use as basis of new truth
 * @param {Object} customer - customer model to use as basis of new truth
 * @param {Object} [options] - options
 */


function updateBillingInformation(order, customer) {
  updateBillingAddressSelector(order, customer); // update billing address form

  updateBillingAddressFormValues(order); // update billing address summary

  addressHelpers.methods.populateAddressSummary(".billing .address-summary", order.billing.billingAddress.address); // update billing parts of order summary

  $(".order-summary-email").text(order.orderEmail);

  if (order.billing.billingAddress.address) {
    $(".order-summary-phone").text(order.billing.billingAddress.address.phone);
  }
}
/**
 * Updates the payment information in checkout, based on the supplied order model
 * @param {Object} order - checkout model to use as basis of new truth
 */


function updatePaymentInformation(order) {
  // update payment details
  var $paymentSummary = $(".payment-details");
  var htmlToAppend = "";

  if (order.billing.payment && order.billing.payment.selectedPaymentInstruments && order.billing.payment.selectedPaymentInstruments.length > 0) {
    htmlToAppend += "<span>" + order.resources.cardType + " " + order.billing.payment.selectedPaymentInstruments[0].type + "</span><div>" + order.billing.payment.selectedPaymentInstruments[0].maskedCreditCardNumber + "</div>";

    if (order.billing.payment.selectedPaymentInstruments[0].expirationMonth && order.billing.payment.selectedPaymentInstruments[0].expirationYear) {
      htmlToAppend += "<div><span>" + order.resources.cardEnding + " " + order.billing.payment.selectedPaymentInstruments[0].expirationMonth + "/" + order.billing.payment.selectedPaymentInstruments[0].expirationYear + "</span></div>";
    }
  }

  $paymentSummary.empty().append(htmlToAppend);
}
/**
 * clears the credit card form
 */


function clearCreditCardForm() {
  $("input[name$=\"_cardNumber\"]").data("cleave").setRawValue("");
  $("select[name$=\"_expirationMonth\"]").val("");
  $("select[name$=\"_expirationYear\"]").val("");
  $("input[name$=\"_securityCode\"]").val("");
}

module.exports = {
  methods: {
    updateBillingAddressSelector: updateBillingAddressSelector,
    updateBillingAddressFormValues: updateBillingAddressFormValues,
    clearBillingAddressFormValues: clearBillingAddressFormValues,
    updateBillingInformation: updateBillingInformation,
    updatePaymentInformation: updatePaymentInformation,
    clearCreditCardForm: clearCreditCardForm
  },
  showBillingDetails: function showBillingDetails() {
    $(".btn-show-billing-details").on("click", function () {
      $(this).parents("[data-address-mode]").attr("data-address-mode", "new");
    });
  },
  hideBillingDetails: function hideBillingDetails() {
    $(".btn-hide-billing-details").on("click", function () {
      $(this).parents("[data-address-mode]").attr("data-address-mode", "shipment");
    });
  },
  selectBillingAddress: function selectBillingAddress() {
    $(".payment-form .addressSelector").on("change", function () {
      var form = $(this).parents("form")[0];
      var selectedOption = $("option:selected", this);
      var optionID = selectedOption[0].value;

      if (optionID === "new") {
        // Show Address
        $(form).attr("data-address-mode", "new");
      } else {
        // Hide Address
        $(form).attr("data-address-mode", "shipment");
      } // Copy fields


      var attrs = selectedOption.data();
      var element;
      Object.keys(attrs).forEach(function (attr) {
        element = attr === "countryCode" ? "country" : attr;

        if (element === "cardNumber") {
          $(".cardNumber").data("cleave").setRawValue(attrs[attr]);
        } else {
          $("[name$=" + element + "]", form).val(attrs[attr]);
        }
      });
    });
  },
  handleCreditCardNumber: function handleCreditCardNumber() {
    cleave.handleCreditCardNumber(".cardNumber", "#cardType");
  },
  santitizeForm: function santitizeForm() {
    $("body").on("checkout:serializeBilling", function (e, data) {
      var serializedForm = cleave.serializeData(data.form);
      data.callback(serializedForm);
    });
  },
  selectSavedPaymentInstrument: function selectSavedPaymentInstrument() {
    $(document).on("click", ".saved-payment-instrument", function (e) {
      e.preventDefault();
      $(".saved-payment-security-code").val("");
      $(".saved-payment-instrument").removeClass("selected-payment");
      $(this).addClass("selected-payment");
      $(".saved-payment-instrument .card-image").removeClass("checkout-hidden");
      $(".saved-payment-instrument .security-code-input").addClass("checkout-hidden");
      $(".saved-payment-instrument.selected-payment" + " .card-image").addClass("checkout-hidden");
      $(".saved-payment-instrument.selected-payment " + ".security-code-input").removeClass("checkout-hidden");
    });
  },
  addNewPaymentInstrument: function addNewPaymentInstrument() {
    $(".btn.add-payment").on("click", function (e) {
      e.preventDefault();
      $(".payment-information").data("is-new-payment", true);
      clearCreditCardForm();
      $(".credit-card-form").removeClass("checkout-hidden");
      $(".user-payment-instruments").addClass("checkout-hidden");
    });
  },
  cancelNewPayment: function cancelNewPayment() {
    $(".cancel-new-payment").on("click", function (e) {
      e.preventDefault();
      $(".payment-information").data("is-new-payment", false);
      clearCreditCardForm();
      $(".user-payment-instruments").removeClass("checkout-hidden");
      $(".credit-card-form").addClass("checkout-hidden");
    });
  },
  clearBillingForm: function clearBillingForm() {
    $("body").on("checkout:clearBillingForm", function () {
      clearBillingAddressFormValues();
    });
  },
  paymentTabs: function paymentTabs() {
    $(".payment-options .nav-item").on("click", function (e) {
      e.preventDefault();
      var methodID = $(this).data("method-id");
      $(".payment-information").data("payment-method-id", methodID);
    });
  }
};

/***/ }),

/***/ "./cartridges/app_refapp/cartridge/client/default/js/checkout/checkout.js":
/*!********************************************************************************!*\
  !*** ./cartridges/app_refapp/cartridge/client/default/js/checkout/checkout.js ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var addressHelpers = __webpack_require__(/*! ./address */ "./cartridges/app_refapp/cartridge/client/default/js/checkout/address.js");

var shippingHelpers = __webpack_require__(/*! ./shipping */ "./cartridges/app_refapp/cartridge/client/default/js/checkout/shipping.js");

var billingHelpers = __webpack_require__(/*! ./billing */ "./cartridges/app_refapp/cartridge/client/default/js/checkout/billing.js");

var summaryHelpers = __webpack_require__(/*! ./summary */ "./cartridges/app_refapp/cartridge/client/default/js/checkout/summary.js");

var formHelpers = __webpack_require__(/*! ./formErrors */ "./cartridges/app_refapp/cartridge/client/default/js/checkout/formErrors.js");

var scrollAnimate = __webpack_require__(/*! ../components/scrollAnimate */ "./cartridges/app_refapp/cartridge/client/default/js/components/scrollAnimate.js");

var formatters = __webpack_require__(/*! ../formatters */ "./cartridges/app_refapp/cartridge/client/default/js/formatters.js");
/**
 * Function to toggle the email account validation error message
 * @param {Object} data - data returned from the server's ajax call that have the supposed state of the message
 */


function toggleEmailErrorMessage(data) {
  if (data) {
    if (!data.allow) {
      $("#link").attr("href", data.link);
      $("body").trigger("checkout:disableButton", ".next-step-button button");
      $("#emailToValidate").removeClass("d-none");
    } else {
      $("#emailToValidate").addClass("d-none");
      $("body").trigger("checkout:enableButton", ".next-step-button button");
    }
  }
}
/**
 * Create the jQuery Checkout Plugin.
 *
 * This jQuery plugin will be registered on the dom element in checkout.isml with the
 * id of "checkout-main".
 *
 * The checkout plugin will handle the different state the user interface is in as the user
 * progresses through the varying forms such as shipping and payment.
 *
 * Billing info and payment info are used a bit synonymously in this code.
 *
 */


(function ($) {
  $.fn.checkout = function () {
    // eslint-disable-line
    var plugin = this; //
    // Collect form data from user input
    //

    var formData = {
      // Shipping Address
      shipping: {},
      // Billing Address
      billing: {},
      // Payment
      payment: {},
      // Gift Codes
      giftCode: {}
    }; //
    // The different states/stages of checkout
    //

    var checkoutStages = ["shipping", "payment", "placeOrder", "submitted"];
    /**
     * Updates the URL to determine stage
     * @param {number} currentStage - The current stage the user is currently on in the checkout
     */

    function updateUrl(currentStage) {
      history.pushState(checkoutStages[currentStage], document.title, location.pathname + "?stage=" + checkoutStages[currentStage] + "#" + checkoutStages[currentStage]);
    } //
    // Local member methods of the Checkout plugin
    //


    var members = {
      // initialize the currentStage variable for the first time
      currentStage: 0,

      /**
       * Set or update the checkout stage (AKA the shipping, billing, payment, etc... steps)
       * @returns {Object} a promise
       */
      updateStage: function updateStage() {
        var stage = checkoutStages[members.currentStage];
        var defer = $.Deferred(); // eslint-disable-line

        if (stage === "shipping") {
          //
          // Clear Previous Errors
          //
          formHelpers.clearPreviousErrors(".shipping-form"); //
          // Submit the Shipping Address Form
          //

          var isMultiShip = $("#checkout-main").hasClass("multi-ship");
          var formSelector = isMultiShip ? ".multi-shipping .active form" : ".single-shipping .shipping-form";
          var form = $(formSelector);

          if (isMultiShip && form.length === 0) {
            // disable the next:Payment button here
            $("body").trigger("checkout:disableButton", ".next-step-button button"); // in case the multi ship form is already submitted

            var url = $("#checkout-main").attr("data-checkout-get-url");
            $.ajax({
              url: url,
              method: "GET",
              success: function success(data) {
                // enable the next:Payment button here
                $("body").trigger("checkout:enableButton", ".next-step-button button");

                if (!data.error) {
                  $("body").trigger("checkout:updateCheckoutView", {
                    order: data.order,
                    customer: data.customer
                  });
                  defer.resolve();
                } else if (data.message && $(".shipping-error .alert-danger").length < 1) {
                  var errorMsg = data.message;
                  var errorHtml = "<div class=\"alert alert-danger alert-dismissible valid-cart-error " + "fade show\" role=\"alert\">" + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">" + "<span aria-hidden=\"true\">&times;</span>" + "</button>" + errorMsg + "</div>";
                  $(".shipping-error").append(errorHtml);
                  scrollAnimate($(".shipping-error"));
                  defer.reject();
                } else if (data.redirectUrl) {
                  window.location.href = data.redirectUrl;
                }
              },
              error: function error() {
                // enable the next:Payment button here
                $("body").trigger("checkout:enableButton", ".next-step-button button"); // Server error submitting form

                defer.reject();
              }
            });
          } else {
            var shippingFormData = form.serialize();
            $("body").trigger("checkout:serializeShipping", {
              form: form,
              data: shippingFormData,
              callback: function callback(data) {
                shippingFormData = data;
              }
            }); // disable the next:Payment button here

            $("body").trigger("checkout:disableButton", ".next-step-button button");
            $.ajax({
              url: form.attr("action"),
              type: "post",
              data: shippingFormData,
              success: function success(data) {
                // enable the next:Payment button here
                $("body").trigger("checkout:enableButton", ".next-step-button button");
                shippingHelpers.methods.shippingFormResponse(defer, data);
              },
              error: function error(err) {
                // enable the next:Payment button here
                $("body").trigger("checkout:enableButton", ".next-step-button button");

                if (err.responseJSON && err.responseJSON.redirectUrl) {
                  window.location.href = err.responseJSON.redirectUrl;
                } // Server error submitting form


                defer.reject(err.responseJSON);
              }
            });
          }

          return defer;
        } else if (stage === "payment") {
          //
          // Submit the Billing Address Form
          //
          formHelpers.clearPreviousErrors(".payment-form");
          var billingAddressForm = $("#dwfrm_billing .billing-address-block :input").serialize();
          $("body").trigger("checkout:serializeBilling", {
            form: $("#dwfrm_billing .billing-address-block"),
            data: billingAddressForm,
            callback: function callback(data) {
              if (data) {
                billingAddressForm = data;
              }
            }
          });
          var contactInfoForm = $("#dwfrm_billing .contact-info-block :input").serialize();
          $("body").trigger("checkout:serializeBilling", {
            form: $("#dwfrm_billing .contact-info-block"),
            data: contactInfoForm,
            callback: function callback(data) {
              if (data) {
                contactInfoForm = data;
              }
            }
          });
          var activeTabId = $(".tab-pane.active").attr("id");
          var paymentInfoSelector = "#dwfrm_billing ." + activeTabId + " .payment-form-fields :input";
          var paymentInfoForm = $(paymentInfoSelector).serialize();
          $("body").trigger("checkout:serializeBilling", {
            form: $(paymentInfoSelector),
            data: paymentInfoForm,
            callback: function callback(data) {
              if (data) {
                paymentInfoForm = data;
              }
            }
          });
          var $termsConditionsInput = $("input[name$='termsAndConditions']");
          var termsCheckboxStr = $termsConditionsInput.attr("name") + "=" + $termsConditionsInput.is(":checked");
          var paymentForm = billingAddressForm + "&" + contactInfoForm + "&" + paymentInfoForm + "&" + termsCheckboxStr;

          if ($(".data-checkout-stage").data("customer-type") === "registered") {
            // if payment method is credit card
            if ($(".payment-information").data("payment-method-id") === "CREDIT_CARD") {
              if (!$(".payment-information").data("is-new-payment")) {
                var cvvCode = $(".saved-payment-instrument." + "selected-payment .saved-payment-security-code").val();
                !$termsConditionsInput.is(":checked") && $termsConditionsInput.addClass("is-invalid");

                if (cvvCode === "") {
                  var cvvElement = $(".saved-payment-instrument." + "selected-payment " + ".form-control");
                  cvvElement.addClass("is-invalid");
                  scrollAnimate(cvvElement);
                  defer.reject();
                  return defer;
                }

                var $savedPaymentInstrument = $(".saved-payment-instrument" + ".selected-payment");
                paymentForm += "&storedPaymentUUID=" + $savedPaymentInstrument.data("uuid");
                paymentForm += "&securityCode=" + cvvCode;
              }
            }
          } // disable the next:Place Order button here


          $("body").trigger("checkout:disableButton", ".next-step-button button");
          $.ajax({
            url: $("#dwfrm_billing").attr("action"),
            method: "POST",
            data: paymentForm,
            success: function success(data) {
              // enable the next:Place Order button here
              $("body").trigger("checkout:enableButton", ".next-step-button button"); // look for field validation errors

              if (data.error) {
                if (data.fieldErrors.length) {
                  data.fieldErrors.forEach(function (error) {
                    if (Object.keys(error).length) {
                      if (error.nonActivatedGuestOrders) {
                        toggleEmailErrorMessage(error.nonActivatedGuestOrders);
                      } else {
                        formHelpers.loadFormErrors(".payment-form", error);
                      }
                    }
                  });
                }

                if (data.serverErrors.length) {
                  data.serverErrors.forEach(function (error) {
                    $(".error-message").show();
                    $(".error-message-text").text(error);
                    scrollAnimate($(".error-message"));
                  });
                }

                if (data.cartError) {
                  window.location.href = data.redirectUrl;
                }

                defer.reject();
              } else if (data.externalRedirect) {
                window.location.href = data.externalUrl;
              } else {
                //
                // Populate the Address Summary
                //
                $("body").trigger("checkout:updateCheckoutView", {
                  order: data.order,
                  customer: data.customer
                });

                if (data.renderedPaymentInstruments) {
                  $(".stored-payments").empty().html(data.renderedPaymentInstruments);
                }

                if (data.customer.registeredUser && data.customer.customerPaymentInstruments.length) {
                  $(".cancel-new-payment").removeClass("checkout-hidden");
                }

                scrollAnimate();
                defer.resolve(data);
              }
            },
            error: function error(err) {
              // enable the next:Place Order button here
              $("body").trigger("checkout:enableButton", ".next-step-button button");

              if (err.responseJSON && err.responseJSON.redirectUrl) {
                window.location.href = err.responseJSON.redirectUrl;
              }
            }
          });
          return defer;
        } else if (stage === "placeOrder") {
          // disable the placeOrder button here
          $("body").trigger("checkout:disableButton", ".next-step-button button");
          $.ajax({
            url: $(".place-order").data("action"),
            method: "POST",
            success: function success(data) {
              // enable the placeOrder button here
              $("body").trigger("checkout:enableButton", ".next-step-button button");

              if (data.error) {
                if (data.cartError) {
                  window.location.href = data.redirectUrl;
                  defer.reject();
                } else {
                  // go to appropriate stage and display error message
                  defer.reject(data);
                }
              } else {
                var continueUrl = data.continueUrl;
                var urlParams = {
                  ID: data.orderID,
                  token: data.orderToken
                };
                continueUrl += (continueUrl.indexOf("?") !== -1 ? "&" : "?") + Object.keys(urlParams).map(function (key) {
                  return key + "=" + encodeURIComponent(urlParams[key]);
                }).join("&");
                window.location.href = continueUrl;
                defer.resolve(data);
              }
            },
            error: function error() {
              // enable the placeOrder button here
              $("body").trigger("checkout:enableButton", $(".next-step-button button"));
            }
          });
          return defer;
        }

        var p = $('<div>').promise(); // eslint-disable-line

        setTimeout(function () {
          p.done(); // eslint-disable-line
        }, 500);
        return p; // eslint-disable-line
      },

      /**
       * Initialize the checkout stage.
       *
       * TODO: update this to allow stage to be set from server?
       */
      initialize: function initialize() {
        $(".checkAccountValidationNeeded").on("blur", function () {
          var contactInfoForm = $("#dwfrm_billing .contact-info-block :input").serialize();
          $.ajax({
            url: $(this).data("allow-non-active-guest"),
            method: "POST",
            data: contactInfoForm
          }).done(function (data) {
            toggleEmailErrorMessage(data.result);
          });
        });
        var $termsConditionsInput = $("input[name$='termsAndConditions']");
        $termsConditionsInput.on("change", function () {
          $(this).toggleClass("is-invalid", !$(this).is(":checked"));
        });
        $("#link").on("click", function (e) {
          var contactInfoForm = $("#dwfrm_billing .contact-info-block :input").serialize();
          e.preventDefault();
          $.ajax({
            url: $(this).attr("href"),
            method: "GET",
            data: contactInfoForm,
            success: function success() {// this just prevent rendering json
            }
          });
        });
        $("#checkValidation").on("click", function (e) {
          e.preventDefault();
          $(".checkAccountValidationNeeded").trigger("blur");
        }); // set the initial state of checkout

        members.currentStage = checkoutStages.indexOf($(".data-checkout-stage").data("checkout-stage"));
        $(plugin).attr("data-checkout-stage", checkoutStages[members.currentStage]); //
        // Handle Payment option selection
        //

        $("input[name$=\"paymentMethod\"]", plugin).on("change", function () {
          $(".credit-card-form").toggle($(this).val() === "CREDIT_CARD");
        }); //
        // Handle Next State button click
        //

        $(plugin).on("click", ".next-step-button button", function () {
          members.nextStage();
        }); //
        // Handle Edit buttons on shipping and payment summary cards
        //

        $(".shipping-summary .edit-button", plugin).on("click", function () {
          if (!$("#checkout-main").hasClass("multi-ship")) {
            $("body").trigger("shipping:selectSingleShipping");
          }

          members.gotoStage("shipping");
        });
        $(".payment-summary .edit-button", plugin).on("click", function () {
          members.gotoStage("payment");
        }); //
        // remember stage (e.g. shipping)
        //

        updateUrl(members.currentStage); //
        // Listen for foward/back button press and move to correct checkout-stage
        //

        $(window).on("popstate", function (e) {
          //
          // Back button when event state less than current state in ordered
          // checkoutStages array.
          //
          if (e.state === null || checkoutStages.indexOf(e.state) < members.currentStage) {
            members.handlePrevStage(false);
          } else if (checkoutStages.indexOf(e.state) > members.currentStage) {
            // Forward button  pressed
            members.handleNextStage(false);
          }
        }); //
        // Set the form data
        //

        plugin.data("formData", formData);
      },

      /**
       * The next checkout state step updates the css for showing correct buttons etc...
       */
      nextStage: function nextStage() {
        var promise = members.updateStage();
        promise.done(function () {
          // Update UI with new stage
          members.handleNextStage(true);
        });
        promise.fail(function (data) {
          // show errors
          if (data) {
            if (data.errorStage) {
              members.gotoStage(data.errorStage.stage);

              if (data.errorStage.step === "billingAddress") {
                var $billingAddressSameAsShipping = $("input[name$=\"_shippingAddressUseAsBillingAddress\"]");

                if ($billingAddressSameAsShipping.is(":checked")) {
                  $billingAddressSameAsShipping.prop("checked", false);
                }
              }
            }

            if (data.errorMessage) {
              $(".error-message").show();
              $(".error-message-text").text(data.errorMessage);
            }
          }
        });
      },

      /**
       * The next checkout state step updates the css for showing correct buttons etc...
       *
       * @param {boolean} bPushState - boolean when true pushes state using the history api.
       */
      handleNextStage: function handleNextStage(bPushState) {
        if (members.currentStage < checkoutStages.length - 1) {
          // move stage forward
          members.currentStage++; //
          // show new stage in url (e.g.payment)
          //

          if (bPushState) {
            updateUrl(members.currentStage);
          }
        } // Set the next stage on the DOM


        $(plugin).attr("data-checkout-stage", checkoutStages[members.currentStage]);
      },

      /**
       * Previous State
       */
      handlePrevStage: function handlePrevStage() {
        if (members.currentStage > 0) {
          // move state back
          members.currentStage--;
          updateUrl(members.currentStage);
        }

        $(plugin).attr("data-checkout-stage", checkoutStages[members.currentStage]);
      },

      /**
       * Use window history to go to a checkout stage
       * @param {string} stageName - the checkout state to goto
       */
      gotoStage: function gotoStage(stageName) {
        members.currentStage = checkoutStages.indexOf(stageName);
        updateUrl(members.currentStage);
        $(plugin).attr("data-checkout-stage", checkoutStages[members.currentStage]);
      }
    }; //
    // Initialize the checkout
    //

    members.initialize();
    return this;
  };
})(jQuery);

var exports = {
  initialize: function initialize() {
    $("#checkout-main").checkout();
  },
  updateCheckoutView: function updateCheckoutView() {
    $("body").on("checkout:updateCheckoutView", function (e, data) {
      shippingHelpers.methods.updateMultiShipInformation(data.order);
      summaryHelpers.updateTotals(data.order.totals);
      data.order.shipping.forEach(function (shipping) {
        shippingHelpers.methods.updateShippingInformation(shipping, data.order, data.customer, data.options);
      });
      billingHelpers.methods.updateBillingInformation(data.order, data.customer, data.options);
      billingHelpers.methods.updatePaymentInformation(data.order, data.options);
      summaryHelpers.updateOrderProductSummaryInformation(data.order, data.options);
      $("body").trigger("checkout:afterUpdateCheckoutView", data);
    });
  },
  disableButton: function disableButton() {
    $("body").on("checkout:disableButton", function (e, button) {
      $(button).prop("disabled", true);
    });
  },
  enableButton: function enableButton() {
    $("body").on("checkout:enableButton", function (e, button) {
      $(button).prop("disabled", false);
    });
  },
  initFormatters: formatters.init
};
[billingHelpers, shippingHelpers, addressHelpers].forEach(function (library) {
  Object.keys(library).forEach(function (item) {
    if (_typeof(library[item]) === "object") {
      exports[item] = $.extend({}, exports[item], library[item]);
    } else {
      exports[item] = library[item];
    }
  });
});
module.exports = exports;

/***/ }),

/***/ "./cartridges/app_refapp/cartridge/client/default/js/checkout/formErrors.js":
/*!**********************************************************************************!*\
  !*** ./cartridges/app_refapp/cartridge/client/default/js/checkout/formErrors.js ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var scrollAnimate = __webpack_require__(/*! ../components/scrollAnimate */ "./cartridges/app_refapp/cartridge/client/default/js/components/scrollAnimate.js");
/**
 * Display error messages and highlight form fields with errors.
 * @param {string} parentSelector - the form which contains the fields
 * @param {Object} fieldErrors - the fields with errors
 */


function loadFormErrors(parentSelector, fieldErrors) {
  // eslint-disable-line
  // Display error messages and highlight form fields with errors.
  $.each(fieldErrors, function (attr) {
    $("*[name=" + attr + "]", parentSelector).addClass("is-invalid").siblings(".invalid-feedback").html(fieldErrors[attr]);
  }); // Animate to top of form that has errors

  scrollAnimate($(parentSelector));
}
/**
 * Clear the form errors.
 * @param {string} parentSelector - the parent form selector.
 */


function clearPreviousErrors(parentSelector) {
  $(parentSelector).find(".form-control.is-invalid").removeClass("is-invalid");
  $(".error-message").hide();
}

module.exports = {
  loadFormErrors: loadFormErrors,
  clearPreviousErrors: clearPreviousErrors
};

/***/ }),

/***/ "./cartridges/app_refapp/cartridge/client/default/js/checkout/shipping.js":
/*!********************************************************************************!*\
  !*** ./cartridges/app_refapp/cartridge/client/default/js/checkout/shipping.js ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var addressHelpers = __webpack_require__(/*! ./address */ "./cartridges/app_refapp/cartridge/client/default/js/checkout/address.js");

var formHelpers = __webpack_require__(/*! ./formErrors */ "./cartridges/app_refapp/cartridge/client/default/js/checkout/formErrors.js");

var scrollAnimate = __webpack_require__(/*! ../components/scrollAnimate */ "./cartridges/app_refapp/cartridge/client/default/js/components/scrollAnimate.js");
/**
 * updates the shipping address selector within shipping forms
 * @param {Object} productLineItem - the productLineItem model
 * @param {Object} shipping - the shipping (shipment model) model
 * @param {Object} order - the order model
 * @param {Object} customer - the customer model
 */


function updateShippingAddressSelector(productLineItem, shipping, order, customer) {
  var uuidEl = $("input[value=" + productLineItem.UUID + "]");
  var shippings = order.shipping;
  var form;
  var $shippingAddressSelector;
  var hasSelectedAddress = false;

  if (uuidEl && uuidEl.length > 0) {
    form = uuidEl[0].form;
    $shippingAddressSelector = $(".addressSelector", form);
  }

  if ($shippingAddressSelector && $shippingAddressSelector.length === 1) {
    $shippingAddressSelector.empty(); // Add New Address option

    $shippingAddressSelector.append(addressHelpers.methods.optionValueForAddress(null, false, order));

    if (customer.addresses && customer.addresses.length > 0) {
      $shippingAddressSelector.append(addressHelpers.methods.optionValueForAddress(order.resources.accountAddresses, false, order));
      customer.addresses.forEach(function (address) {
        var isSelected = shipping.matchingAddressId === address.ID;
        $shippingAddressSelector.append(addressHelpers.methods.optionValueForAddress({
          UUID: "ab_" + address.ID,
          shippingAddress: address
        }, isSelected, order));
      });
    } // Separator -


    $shippingAddressSelector.append(addressHelpers.methods.optionValueForAddress(order.resources.shippingAddresses, false, order, {
      className: "multi-shipping"
    }));
    shippings.forEach(function (aShipping) {
      var isSelected = shipping.UUID === aShipping.UUID;
      hasSelectedAddress = hasSelectedAddress || isSelected;
      var addressOption = addressHelpers.methods.optionValueForAddress(aShipping, isSelected, order, {
        className: "multi-shipping"
      });
      var newAddress = addressOption.html() === order.resources.addNewAddress;
      var matchingUUID = aShipping.UUID === shipping.UUID;

      if (newAddress && matchingUUID || !newAddress && matchingUUID || !newAddress && !matchingUUID) {
        //NOSONAR
        $shippingAddressSelector.append(addressOption);
      }

      if (newAddress && !matchingUUID) {
        $(addressOption[0]).remove();
      }
    });
  }

  if (!hasSelectedAddress) {
    // show
    $(form).addClass("hide-details");
  } else {
    $(form).removeClass("hide-details");
  }

  $("body").trigger("shipping:updateShippingAddressSelector", {
    productLineItem: productLineItem,
    shipping: shipping,
    order: order,
    customer: customer
  });
}
/**
 * updates the shipping address form values within shipping forms
 * @param {Object} shipping - the shipping (shipment model) model
 */


function updateShippingAddressFormValues(shipping) {
  var addressObject = $.extend({}, shipping.shippingAddress);

  if (!addressObject) {
    addressObject = {
      firstName: null,
      lastName: null,
      address1: null,
      address2: null,
      city: null,
      postalCode: null,
      stateCode: null,
      countryCode: null,
      phone: null
    };
  }

  addressObject.isGift = shipping.isGift;
  addressObject.giftMessage = shipping.giftMessage;
  $("input[value=" + shipping.UUID + "]").each(function (formIndex, el) {
    var form = el.form;
    if (!form) return;
    var countryCode = addressObject.countryCode;
    $("input[name$=_firstName]", form).val(addressObject.firstName);
    $("input[name$=_lastName]", form).val(addressObject.lastName);
    $("input[name$=_address1]", form).val(addressObject.address1);
    $("input[name$=_address2]", form).val(addressObject.address2);
    $("input[name$=_city]", form).val(addressObject.city);
    $("input[name$=_postalCode]", form).val(addressObject.postalCode);
    $("select[name$=_stateCode],input[name$=_stateCode]", form).val(addressObject.stateCode);

    if (countryCode && _typeof(countryCode) === "object") {
      $("select[name$=_country]", form).val(addressObject.countryCode.value);
    } else {
      $("select[name$=_country]", form).val(addressObject.countryCode);
    }

    $("input[name$=_phone]", form).val(addressObject.phone);
    $("input[name$=_isGift]", form).prop("checked", addressObject.isGift);
    $("textarea[name$=_giftMessage]", form).val(addressObject.isGift && addressObject.giftMessage ? addressObject.giftMessage : "");
  });
  $("body").trigger("shipping:updateShippingAddressFormValues", {
    shipping: shipping
  });
}
/**
 * updates the shipping method radio buttons within shipping forms
 * @param {Object} shipping - the shipping (shipment model) model
 */


function updateShippingMethods(shipping) {
  var uuidEl = $("input[value=" + shipping.UUID + "]");

  if (uuidEl && uuidEl.length > 0) {
    $.each(uuidEl, function (shipmentIndex, el) {
      var form = el.form;
      if (!form) return;
      var $shippingMethodList = $(".shipping-method-list", form);

      if ($shippingMethodList && $shippingMethodList.length > 0) {
        $shippingMethodList.empty();
        var shippingMethods = shipping.applicableShippingMethods;
        var selected = shipping.selectedShippingMethod || {};
        var shippingMethodFormID = form.name + "_shippingAddress_shippingMethodID"; //
        // Create the new rows for each shipping method
        //

        $.each(shippingMethods, function (methodIndex, shippingMethod) {
          var tmpl = $("#shipping-method-template").clone(); // set input

          $("input", tmpl).prop("id", "shippingMethod-" + shippingMethod.ID + "-" + shipping.UUID).prop("name", shippingMethodFormID).prop("value", shippingMethod.ID).attr("checked", shippingMethod.ID === selected.ID);
          $("label", tmpl).prop("for", "shippingMethod-" + shippingMethod.ID + "-" + shipping.UUID); // set shipping method name

          $(".display-name", tmpl).text(shippingMethod.displayName); // set or hide arrival time

          if (shippingMethod.estimatedArrivalTime) {
            $(".arrival-time", tmpl).text("(" + shippingMethod.estimatedArrivalTime + ")").show();
          } // set shipping cost


          $(".shipping-cost", tmpl).text(shippingMethod.shippingCost);
          $shippingMethodList.append(tmpl.html());
        });
      }
    });
  }

  $("body").trigger("shipping:updateShippingMethods", {
    shipping: shipping
  });
}
/**
 * Update list of available shipping methods whenever user modifies shipping address details.
 * @param {jQuery} $shippingForm - current shipping form
 */


function updateShippingMethodList($shippingForm) {
  // delay for autocomplete!
  setTimeout(function () {
    var $shippingMethodList = $shippingForm.find(".shipping-method-list");
    var urlParams = addressHelpers.methods.getAddressFieldsFromUI($shippingForm);
    var shipmentUUID = $shippingForm.find("[name=shipmentUUID]").val();
    var url = $shippingMethodList.data("actionUrl");
    urlParams.shipmentUUID = shipmentUUID;
    $shippingMethodList.spinner().start();
    $.ajax({
      url: url,
      type: "post",
      dataType: "json",
      data: urlParams,
      success: function success(data) {
        if (data.error) {
          window.location.href = data.redirectUrl;
        } else {
          $("body").trigger("checkout:updateCheckoutView", {
            order: data.order,
            customer: data.customer,
            options: {
              keepOpen: true
            }
          });
          $shippingMethodList.spinner().stop();
        }
      }
    });
  }, 300);
}
/**
 * updates the order shipping summary for an order shipment model
 * @param {Object} shipping - the shipping (shipment model) model
 * @param {Object} order - the order model
 */


function updateShippingSummaryInformation(shipping, order) {
  $("[data-shipment-summary=" + shipping.UUID + "]").each(function (i, el) {
    var $container = $(el);
    var $shippingAddressLabel = $container.find(".shipping-addr-label");
    var $addressContainer = $container.find(".address-summary");
    var $shippingPhone = $container.find(".shipping-phone");
    var $methodTitle = $container.find(".shipping-method-title");
    var $methodArrivalTime = $container.find(".shipping-method-arrival-time");
    var $methodPrice = $container.find(".shipping-method-price");
    var $shippingSummaryLabel = $container.find(".shipping-method-label");
    var $summaryDetails = $container.find(".row.summary-details");
    var giftMessageSummary = $container.find(".gift-summary");
    var address = shipping.shippingAddress;
    var selectedShippingMethod = shipping.selectedShippingMethod;
    var isGift = shipping.isGift;
    addressHelpers.methods.populateAddressSummary($addressContainer, address);

    if (address && address.phone) {
      $shippingPhone.text(address.phone);
    } else {
      $shippingPhone.empty();
    }

    if (selectedShippingMethod) {
      $("body").trigger("shipping:updateAddressLabelText", {
        selectedShippingMethod: selectedShippingMethod,
        resources: order.resources,
        shippingAddressLabel: $shippingAddressLabel
      });
      $shippingSummaryLabel.show();
      $summaryDetails.show();
      $methodTitle.text(selectedShippingMethod.displayName);

      if (selectedShippingMethod.estimatedArrivalTime) {
        $methodArrivalTime.text("( " + selectedShippingMethod.estimatedArrivalTime + " )");
      } else {
        $methodArrivalTime.empty();
      }

      $methodPrice.text(selectedShippingMethod.shippingCost);
    }

    if (isGift) {
      giftMessageSummary.find(".gift-message-summary").text(shipping.giftMessage);
      giftMessageSummary.removeClass("d-none");
    } else {
      giftMessageSummary.addClass("d-none");
    }
  });
  $("body").trigger("shipping:updateShippingSummaryInformation", {
    shipping: shipping,
    order: order
  });
}
/**
 * Update the read-only portion of the shipment display (per PLI)
 * @param {Object} productLineItem - the productLineItem model
 * @param {Object} shipping - the shipping (shipment model) model
 * @param {Object} order - the order model
 * @param {Object} [options] - options for updating PLI summary info
 * @param {Object} [options.keepOpen] - if true, prevent changing PLI view mode to 'view'
 */


function updatePLIShippingSummaryInformation(productLineItem, shipping, order, options) {
  var $pli = $("input[value=" + productLineItem.UUID + "]");
  var form = $pli && $pli.length > 0 ? $pli[0].form : null;
  if (!form) return;
  var $viewBlock = $(".view-address-block", form);
  var address = shipping.shippingAddress || {};
  var selectedMethod = shipping.selectedShippingMethod;
  var nameLine = address.firstName ? address.firstName + " " : "";
  if (address.lastName) nameLine += address.lastName;
  var address1Line = address.address1;
  var address2Line = address.address2;
  var phoneLine = address.phone;
  var shippingCost = selectedMethod ? selectedMethod.shippingCost : "";
  var methodNameLine = selectedMethod ? selectedMethod.displayName : "";
  var methodArrivalTime = selectedMethod && selectedMethod.estimatedArrivalTime ? "(" + selectedMethod.estimatedArrivalTime + ")" : "";
  var tmpl = $("#pli-shipping-summary-template").clone();
  $(".ship-to-name", tmpl).text(nameLine);
  $(".ship-to-address1", tmpl).text(address1Line);
  $(".ship-to-address2", tmpl).text(address2Line);
  $(".ship-to-city", tmpl).text(address.city);

  if (address.stateCode) {
    $(".ship-to-st", tmpl).text(address.stateCode);
  }

  $(".ship-to-zip", tmpl).text(address.postalCode);
  $(".ship-to-phone", tmpl).text(phoneLine);

  if (!address2Line) {
    $(".ship-to-address2", tmpl).hide();
  }

  if (!phoneLine) {
    $(".ship-to-phone", tmpl).hide();
  }

  if (shipping.selectedShippingMethod) {
    $(".display-name", tmpl).text(methodNameLine);
    $(".arrival-time", tmpl).text(methodArrivalTime);
    $(".price", tmpl).text(shippingCost);
  }

  if (shipping.isGift) {
    $(".gift-message-summary", tmpl).text(shipping.giftMessage);
    var shipment = $(".gift-message-" + shipping.UUID);
    $(shipment).val(shipping.giftMessage);
  } else {
    $(".gift-summary", tmpl).addClass("d-none");
  } // checking h5 title shipping to or pickup


  var $shippingAddressLabel = $(".shipping-header-text", tmpl);
  $("body").trigger("shipping:updateAddressLabelText", {
    selectedShippingMethod: selectedMethod,
    resources: order.resources,
    shippingAddressLabel: $shippingAddressLabel
  });
  $viewBlock.html(tmpl.html());
  $("body").trigger("shipping:updatePLIShippingSummaryInformation", {
    productLineItem: productLineItem,
    shipping: shipping,
    order: order,
    options: options
  });
}
/**
 * Update the hidden form values that associate shipping info with product line items
 * @param {Object} productLineItem - the productLineItem model
 * @param {Object} shipping - the shipping (shipment model) model
 */


function updateProductLineItemShipmentUUIDs(productLineItem, shipping) {
  $("input[value=" + productLineItem.UUID + "]").each(function (key, pli) {
    var form = pli.form;
    $("[name=shipmentUUID]", form).val(shipping.UUID);
    $("[name=originalShipmentUUID]", form).val(shipping.UUID);
    $(form).closest(".card").attr("data-shipment-uuid", shipping.UUID);
  });
  $("body").trigger("shipping:updateProductLineItemShipmentUUIDs", {
    productLineItem: productLineItem,
    shipping: shipping
  });
}
/**
 * Update the shipping UI for a single shipping info (shipment model)
 * @param {Object} shipping - the shipping (shipment model) model
 * @param {Object} order - the order/basket model
 * @param {Object} customer - the customer model
 * @param {Object} [options] - options for updating PLI summary info
 * @param {Object} [options.keepOpen] - if true, prevent changing PLI view mode to 'view'
 */


function updateShippingInformation(shipping, order, customer, options) {
  // First copy over shipmentUUIDs from response, to each PLI form
  order.shipping.forEach(function (aShipping) {
    aShipping.productLineItems.items.forEach(function (productLineItem) {
      updateProductLineItemShipmentUUIDs(productLineItem, aShipping);
    });
  }); // Now update shipping information, based on those associations

  updateShippingMethods(shipping);
  updateShippingAddressFormValues(shipping);
  updateShippingSummaryInformation(shipping, order); // And update the PLI-based summary information as well

  shipping.productLineItems.items.forEach(function (productLineItem) {
    updateShippingAddressSelector(productLineItem, shipping, order, customer);
    updatePLIShippingSummaryInformation(productLineItem, shipping, order, options);
  });
  $("body").trigger("shipping:updateShippingInformation", {
    order: order,
    shipping: shipping,
    customer: customer,
    options: options
  });
}
/**
 * Update the checkout state (single vs. multi-ship)
 * @param {Object} order - checkout model to use as basis of new truth
 */


function updateMultiShipInformation(order) {
  var $checkoutMain = $("#checkout-main");
  var $checkbox = $("[name=usingMultiShipping]");
  var $submitShippingBtn = $("button.submit-shipping");
  $(".shipping-error .alert-danger").remove();

  if (order.usingMultiShipping) {
    $checkoutMain.addClass("multi-ship");
    $checkbox.prop("checked", true);
  } else {
    $checkoutMain.removeClass("multi-ship");
    $checkbox.prop("checked", null);
    $submitShippingBtn.prop("disabled", null);
  }

  $("body").trigger("shipping:updateMultiShipInformation", {
    order: order
  });
}
/**
  * Create an alert to display the error message
  * @param {Object} message - Error message to display
  */


function createErrorNotification(message) {
  var errorHtml = "<div class=\"alert alert-danger alert-dismissible valid-cart-error " + "fade show\" role=\"alert\">" + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">" + "<span aria-hidden=\"true\">&times;</span>" + "</button>" + message + "</div>";
  $(".shipping-error").append(errorHtml);
  scrollAnimate($(".shipping-error"));
}
/**
 * Handle response from the server for valid or invalid form fields.
 * @param {Object} defer - the deferred object which will resolve on success or reject.
 * @param {Object} data - the response data with the invalid form fields or
 *  valid model data.
 */


function shippingFormResponse(defer, data) {
  var isMultiShip = $("#checkout-main").hasClass("multi-ship");
  var formSelector = isMultiShip ? ".multi-shipping .active form" : ".single-shipping form"; // highlight fields with errors

  if (data.error) {
    if (data.fieldErrors.length) {
      data.fieldErrors.forEach(function (error) {
        if (Object.keys(error).length) {
          formHelpers.loadFormErrors(formSelector, error);
        }
      });
      defer.reject(data);
    }

    if (data.serverErrors && data.serverErrors.length) {
      $.each(data.serverErrors, function (index, element) {
        createErrorNotification(element);
      });
      defer.reject(data);
    }

    if (data.cartError) {
      window.location.href = data.redirectUrl;
      defer.reject();
    }
  } else {
    // Populate the Address Summary
    $("body").trigger("checkout:updateCheckoutView", {
      order: data.order,
      customer: data.customer
    });
    scrollAnimate($(".payment-form"));
    defer.resolve(data);
  }
}
/**
 * Clear out all the shipping form values and select the new address in the drop down
 * @param {Object} order - the order object
 */


function clearShippingForms(order) {
  order.shipping.forEach(function (shipping) {
    $("input[value=" + shipping.UUID + "]").each(function (formIndex, el) {
      var form = el.form;
      if (!form) return;
      $("input[name$=_firstName]", form).val("");
      $("input[name$=_lastName]", form).val("");
      $("input[name$=_address1]", form).val("");
      $("input[name$=_address2]", form).val("");
      $("input[name$=_city]", form).val("");
      $("input[name$=_postalCode]", form).val("");
      $("select[name$=_stateCode],input[name$=_stateCode]", form).val("");
      $("select[name$=_country]", form).val("");
      $("input[name$=_phone]", form).val("");
      $("input[name$=_isGift]", form).prop("checked", false);
      $("textarea[name$=_giftMessage]", form).val("");
      $(form).find(".gift-message").addClass("d-none");
      $(form).attr("data-address-mode", "new");
      var addressSelectorDropDown = $(".addressSelector option[value=new]", form);
      $(addressSelectorDropDown).prop("selected", true);
    });
  });
  $("body").trigger("shipping:clearShippingForms", {
    order: order
  });
}
/**
 * Does Ajax call to create a server-side shipment w/ pliUUID & URL
 * @param {string} url - string representation of endpoint URL
 * @param {Object} shipmentData - product line item UUID
 * @returns {Object} - promise value for async call
 */


function createNewShipment(url, shipmentData) {
  $.spinner().start();
  return $.ajax({
    url: url,
    type: "post",
    dataType: "json",
    data: shipmentData
  });
}
/**
 * Does Ajax call to select shipping method
 * @param {string} url - string representation of endpoint URL
 * @param {Object} urlParams - url params
 * @param {Object} el - element that triggered this call
 */


function selectShippingMethodAjax(url, urlParams, el) {
  $.spinner().start();
  $.ajax({
    url: url,
    type: "post",
    dataType: "json",
    data: urlParams
  }).done(function (data) {
    if (data.error) {
      window.location.href = data.redirectUrl;
    } else {
      $("body").trigger("checkout:updateCheckoutView", {
        order: data.order,
        customer: data.customer,
        options: {
          keepOpen: true
        },
        urlParams: urlParams
      });
      $("body").trigger("checkout:postUpdateCheckoutView", {
        el: el
      });
    }

    $.spinner().stop();
  }).fail(function () {
    $.spinner().stop();
  });
}
/**
 * Hide and show to appropriate elements to show the multi ship shipment cards in the enter view
 * @param {jQuery} element - The shipping content
 */


function enterMultishipView(element) {
  element.find(".btn-enter-multi-ship").removeClass("d-none");
  element.find(".view-address-block").addClass("d-none");
  element.find(".shipping-address").addClass("d-none");
  element.find(".btn-save-multi-ship.save-shipment").addClass("d-none");
  element.find(".btn-edit-multi-ship").addClass("d-none");
  element.find(".multi-ship-address-actions").addClass("d-none");
}
/**
 * Hide and show to appropriate elements to show the multi ship shipment cards in the view mode
 * @param {jQuery} element - The shipping content
 */


function viewMultishipAddress(element) {
  element.find(".view-address-block").removeClass("d-none");
  element.find(".btn-edit-multi-ship").removeClass("d-none");
  element.find(".shipping-address").addClass("d-none");
  element.find(".btn-save-multi-ship.save-shipment").addClass("d-none");
  element.find(".btn-enter-multi-ship").addClass("d-none");
  element.find(".multi-ship-address-actions").addClass("d-none");
}
/**
 * Hide and show to appropriate elements that allows the user to edit multi ship address information
 * @param {jQuery} element - The shipping content
 */


function editMultiShipAddress(element) {
  // Show
  element.find(".shipping-address").removeClass("d-none");
  element.find(".btn-save-multi-ship.save-shipment").removeClass("d-none"); // Hide

  element.find(".view-address-block").addClass("d-none");
  element.find(".btn-enter-multi-ship").addClass("d-none");
  element.find(".btn-edit-multi-ship").addClass("d-none");
  element.find(".multi-ship-address-actions").addClass("d-none");
  $("body").trigger("shipping:editMultiShipAddress", {
    element: element,
    form: element.find(".shipping-form")
  });
}
/**
 * perform the proper actions once a user has clicked enter address or edit address for a shipment
 * @param {jQuery} element - The shipping content
 * @param {string} mode - the address mode
 */


function editOrEnterMultiShipInfo(element, mode) {
  var form = $(element).closest("form");
  var root = $(element).closest(".shipping-content");
  $("body").trigger("shipping:updateDataAddressMode", {
    form: form,
    mode: mode
  });
  editMultiShipAddress(root);
  var addressInfo = addressHelpers.methods.getAddressFieldsFromUI(form);
  var savedState = {
    UUID: $("input[name=shipmentUUID]", form).val(),
    shippingAddress: addressInfo
  };
  root.data("saved-state", JSON.stringify(savedState));
}

module.exports = {
  methods: {
    updateShippingAddressSelector: updateShippingAddressSelector,
    updateShippingAddressFormValues: updateShippingAddressFormValues,
    updateShippingMethods: updateShippingMethods,
    updateShippingSummaryInformation: updateShippingSummaryInformation,
    updatePLIShippingSummaryInformation: updatePLIShippingSummaryInformation,
    updateProductLineItemShipmentUUIDs: updateProductLineItemShipmentUUIDs,
    updateShippingInformation: updateShippingInformation,
    updateMultiShipInformation: updateMultiShipInformation,
    shippingFormResponse: shippingFormResponse,
    createNewShipment: createNewShipment,
    selectShippingMethodAjax: selectShippingMethodAjax,
    updateShippingMethodList: updateShippingMethodList,
    clearShippingForms: clearShippingForms,
    editMultiShipAddress: editMultiShipAddress,
    editOrEnterMultiShipInfo: editOrEnterMultiShipInfo,
    createErrorNotification: createErrorNotification,
    viewMultishipAddress: viewMultishipAddress
  },
  selectShippingMethod: function selectShippingMethod() {
    var baseObj = this;
    $(".shipping-method-list").change(function () {
      var $shippingForm = $(this).parents("form");
      var methodID = $(":checked", this).val();
      var shipmentUUID = $shippingForm.find("[name=shipmentUUID]").val();
      var urlParams = addressHelpers.methods.getAddressFieldsFromUI($shippingForm);
      urlParams.shipmentUUID = shipmentUUID;
      urlParams.methodID = methodID;
      urlParams.isGift = $shippingForm.find(".gift").prop("checked");
      urlParams.giftMessage = $shippingForm.find("textarea[name$=_giftMessage]").val();
      var url = $(this).data("select-shipping-method-url");

      if (baseObj.methods && baseObj.methods.selectShippingMethodAjax) {
        baseObj.methods.selectShippingMethodAjax(url, urlParams, $(this));
      } else {
        selectShippingMethodAjax(url, urlParams, $(this));
      }
    });
  },
  toggleMultiship: function toggleMultiship() {
    var baseObj = this;
    $("input[name=\"usingMultiShipping\"]").on("change", function () {
      var url = $(".multi-shipping-checkbox-block form").attr("action");
      var usingMultiShip = this.checked;
      $.ajax({
        url: url,
        type: "post",
        dataType: "json",
        data: {
          usingMultiShip: usingMultiShip
        },
        success: function success(response) {
          if (response.error) {
            window.location.href = response.redirectUrl;
          } else {
            $("body").trigger("checkout:updateCheckoutView", {
              order: response.order,
              customer: response.customer
            });

            if ($("#checkout-main").data("customer-type") === "guest") {
              if (baseObj.methods && baseObj.methods.clearShippingForms) {
                baseObj.methods.clearShippingForms(response.order);
              } else {
                clearShippingForms(response.order);
              }
            } else {
              response.order.shipping.forEach(function (shipping) {
                $("input[value=" + shipping.UUID + "]").each(function (formIndex, el) {
                  var form = el.form;
                  if (!form) return;
                  $(form).attr("data-address-mode", "edit");
                  var addressSelectorDropDown = $(form).find(".addressSelector option[value=\"ab_" + shipping.matchingAddressId + "\"]");
                  $(addressSelectorDropDown).prop("selected", true);
                  $("input[name$=_isGift]", form).prop("checked", false);
                  $("textarea[name$=_giftMessage]", form).val("");
                  $(form).find(".gift-message").addClass("d-none");
                });
              });
            }

            if (usingMultiShip) {
              $("body").trigger("shipping:selectMultiShipping", {
                data: response
              });
            } else {
              $("body").trigger("shipping:selectSingleShipping", {
                data: response
              });
            }
          }

          $.spinner().stop();
        },
        error: function error() {
          $.spinner().stop();
        }
      });
    });
  },
  selectSingleShipping: function selectSingleShipping() {
    $("body").on("shipping:selectSingleShipping", function () {
      $(".single-shipping .shipping-address").removeClass("d-none");
    });
  },
  selectMultiShipping: function selectMultiShipping() {
    var baseObj = this;
    $("body").on("shipping:selectMultiShipping", function (e, data) {
      $(".multi-shipping .shipping-address").addClass("d-none");
      data.data.order.shipping.forEach(function (shipping) {
        var element = $(".multi-shipping .card[data-shipment-uuid=\"" + shipping.UUID + "\"]");

        if (shipping.shippingAddress) {
          if (baseObj.methods && baseObj.methods.viewMultishipAddress) {
            baseObj.methods.viewMultishipAddress($(element));
          } else {
            viewMultishipAddress($(element));
          }
        } else {
          /* eslint-disable no-lonely-if */
          if (baseObj.methods && baseObj.methods.enterMultishipView) {
            baseObj.methods.enterMultishipView($(element));
          } else {
            enterMultishipView($(element));
          }
          /* eslint-enable no-lonely-if */

        }
      });
    });
  },
  selectSingleShipAddress: function selectSingleShipAddress() {
    $(".single-shipping .addressSelector").on("change", function () {
      var form = $(this).parents("form")[0];
      var selectedOption = $("option:selected", this);
      var attrs = selectedOption.data();
      var shipmentUUID = selectedOption[0].value;
      var originalUUID = $("input[name=shipmentUUID]", form).val();
      var element;
      Object.keys(attrs).forEach(function (attr) {
        element = attr === "countryCode" ? "country" : attr;
        $("[name$=" + element + "]", form).val(attrs[attr]);
      });
      $("[name$=stateCode]", form).trigger("change");

      if (shipmentUUID === "new") {
        $(form).attr("data-address-mode", "new");
        $(form).find(".shipping-address-block").removeClass("d-none");
      } else if (shipmentUUID === originalUUID) {
        $(form).attr("data-address-mode", "shipment");
      } else if (shipmentUUID.indexOf("ab_") === 0) {
        $(form).attr("data-address-mode", "customer");
      } else {
        $(form).attr("data-address-mode", "edit");
      }
    });
  },
  selectMultiShipAddress: function selectMultiShipAddress() {
    var baseObj = this;
    $(".multi-shipping .addressSelector").on("change", function () {
      var form = $(this).closest("form");
      var selectedOption = $("option:selected", this);
      var attrs = selectedOption.data();
      var shipmentUUID = selectedOption[0].value;
      var originalUUID = $("input[name=shipmentUUID]", form).val();
      var pliUUID = $("input[name=productLineItemUUID]", form).val();
      var createNewShipmentScoped = baseObj.methods && baseObj.methods.createNewShipment ? baseObj.methods.createNewShipment : createNewShipment;
      var element;
      Object.keys(attrs).forEach(function (attr) {
        if (attr === "isGift") {
          $("[name$=" + attr + "]", form).prop("checked", attrs[attr]);
          $("[name$=" + attr + "]", form).trigger("change");
        } else {
          element = attr === "countryCode" ? "country" : attr;
          $("[name$=" + element + "]", form).val(attrs[attr]);
        }
      });

      if (shipmentUUID === "new" && pliUUID) {
        var createShipmentUrl = $(this).attr("data-create-shipment-url");
        createNewShipmentScoped(createShipmentUrl, {
          productLineItemUUID: pliUUID
        }).done(function (response) {
          $.spinner().stop();

          if (response.error) {
            if (response.redirectUrl) {
              window.location.href = response.redirectUrl;
            }

            return;
          }

          $("body").trigger("checkout:updateCheckoutView", {
            order: response.order,
            customer: response.customer,
            options: {
              keepOpen: true
            }
          });
          $(form).attr("data-address-mode", "new");
        }).fail(function () {
          $.spinner().stop();
        });
      } else if (shipmentUUID === originalUUID) {
        $("select[name$=stateCode]", form).trigger("change");
        $(form).attr("data-address-mode", "shipment");
      } else if (shipmentUUID.indexOf("ab_") === 0) {
        var url = $(form).attr("action");
        var serializedData = $(form).serialize();
        createNewShipmentScoped(url, serializedData).done(function (response) {
          $.spinner().stop();

          if (response.error) {
            if (response.redirectUrl) {
              window.location.href = response.redirectUrl;
            }

            return;
          }

          $("body").trigger("checkout:updateCheckoutView", {
            order: response.order,
            customer: response.customer,
            options: {
              keepOpen: true
            }
          });
          $(form).attr("data-address-mode", "customer");
          var $rootEl = $(form).closest(".shipping-content");
          editMultiShipAddress($rootEl);
        }).fail(function () {
          $.spinner().stop();
        });
      } else {
        var updatePLIShipmentUrl = $(form).attr("action");
        var serializedAddress = $(form).serialize();
        createNewShipmentScoped(updatePLIShipmentUrl, serializedAddress).done(function (response) {
          $.spinner().stop();

          if (response.error) {
            if (response.redirectUrl) {
              window.location.href = response.redirectUrl;
            }

            return;
          }

          $("body").trigger("checkout:updateCheckoutView", {
            order: response.order,
            customer: response.customer,
            options: {
              keepOpen: true
            }
          });
          $(form).attr("data-address-mode", "edit");
        }).fail(function () {
          $.spinner().stop();
        });
      }
    });
  },
  updateShippingList: function updateShippingList() {
    var baseObj = this;
    $("select[name$=\"shippingAddress_addressFields_states_stateCode\"]").on("change", function (e) {
      if (baseObj.methods && baseObj.methods.updateShippingMethodList) {
        baseObj.methods.updateShippingMethodList($(e.currentTarget.form));
      } else {
        updateShippingMethodList($(e.currentTarget.form));
      }
    });
  },
  updateDataAddressMode: function updateDataAddressMode() {
    $("body").on("shipping:updateDataAddressMode", function (e, data) {
      $(data.form).attr("data-address-mode", data.mode);
    });
  },
  enterMultiShipInfo: function enterMultiShipInfo() {
    var baseObj = this;
    $(".btn-enter-multi-ship").on("click", function (e) {
      e.preventDefault();

      if (baseObj.methods && baseObj.methods.editOrEnterMultiShipInfo) {
        baseObj.methods.editOrEnterMultiShipInfo($(this), "new");
      } else {
        editOrEnterMultiShipInfo($(this), "new");
      }
    });
  },
  editMultiShipInfo: function editMultiShipInfo() {
    var baseObj = this;
    $(".btn-edit-multi-ship").on("click", function (e) {
      e.preventDefault();

      if (baseObj.methods && baseObj.methods.editOrEnterMultiShipInfo) {
        baseObj.methods.editOrEnterMultiShipInfo($(this), "edit");
      } else {
        editOrEnterMultiShipInfo($(this), "edit");
      }
    });
  },
  saveMultiShipInfo: function saveMultiShipInfo() {
    var baseObj = this;
    $(".btn-save-multi-ship").on("click", function (e) {
      e.preventDefault(); // Save address to checkoutAddressBook

      var form = $(this).closest("form");
      var $rootEl = $(this).closest(".shipping-content");
      var data = $(form).serialize();
      var url = $(form).attr("action");
      $rootEl.spinner().start();
      $.ajax({
        url: url,
        type: "post",
        dataType: "json",
        data: data
      }).done(function (response) {
        formHelpers.clearPreviousErrors(form);

        if (response.error) {
          if (response.fieldErrors && response.fieldErrors.length) {
            response.fieldErrors.forEach(function (error) {
              if (Object.keys(error).length) {
                formHelpers.loadFormErrors(form, error);
              }
            });
          } else if (response.serverErrors && response.serverErrors.length) {
            $.each(response.serverErrors, function (index, element) {
              createErrorNotification(element);
            });
          } else if (response.redirectUrl) {
            window.location.href = response.redirectUrl;
          }
        } else {
          // Update UI from response
          $("body").trigger("checkout:updateCheckoutView", {
            order: response.order,
            customer: response.customer
          });

          if (baseObj.methods && baseObj.methods.viewMultishipAddress) {
            baseObj.methods.viewMultishipAddress($rootEl);
          } else {
            viewMultishipAddress($rootEl);
          }
        }

        if (response.order && response.order.shippable) {
          $("button.submit-shipping").attr("disabled", null);
        }

        $rootEl.spinner().stop();
      }).fail(function (err) {
        if (err.responseJSON.redirectUrl) {
          window.location.href = err.responseJSON.redirectUrl;
        }

        $rootEl.spinner().stop();
      });
      return false;
    });
  },
  cancelMultiShipAddress: function cancelMultiShipAddress() {
    var baseObj = this;
    $(".btn-cancel-multi-ship-address").on("click", function (e) {
      e.preventDefault();
      var form = $(this).closest("form");
      var $rootEl = $(this).closest(".shipping-content");
      var restoreState = $rootEl.data("saved-state"); // Should clear out changes / restore previous state

      if (restoreState) {
        var restoreStateObj = JSON.parse(restoreState);
        var originalStateCode = restoreStateObj.shippingAddress.stateCode;
        var stateCode = $("[name$=_stateCode]", form).val();

        if (baseObj.methods && baseObj.methods.updateShippingAddressFormValues) {
          baseObj.methods.updateShippingAddressFormValues(restoreStateObj);
        } else {
          updateShippingAddressFormValues(restoreStateObj);
        }

        if (stateCode !== originalStateCode) {
          $("[data-action=save]", form).trigger("click");
        } else {
          $(form).attr("data-address-mode", "edit");

          if (baseObj.methods && baseObj.methods.editMultiShipAddress) {
            baseObj.methods.editMultiShipAddress($rootEl);
          } else {
            editMultiShipAddress($rootEl);
          }
        }
      }

      return false;
    });
  },
  isGift: function isGift() {
    $(".gift").on("change", function (e) {
      e.preventDefault();
      var form = $(this).closest("form");

      if (this.checked) {
        $(form).find(".gift-message").removeClass("d-none");
      } else {
        $(form).find(".gift-message").addClass("d-none");
        $(form).find(".gift-message").val("");
      }
    });
  }
};

/***/ }),

/***/ "./cartridges/app_refapp/cartridge/client/default/js/checkout/summary.js":
/*!*******************************************************************************!*\
  !*** ./cartridges/app_refapp/cartridge/client/default/js/checkout/summary.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * updates the totals summary
 * @param {Array} totals - the totals data
 */

function updateTotals(totals) {
  $(".shipping-total-cost").text(totals.totalShippingCost);
  $(".tax-total").text(totals.totalTax);
  $(".sub-total").text(totals.subTotal);
  $(".grand-total-sum").text(totals.grandTotal);

  if (totals.orderLevelDiscountTotal.value > 0) {
    $(".order-discount").removeClass("hide-order-discount");
    $(".order-discount-total").text("- " + totals.orderLevelDiscountTotal.formatted);
  } else {
    $(".order-discount").addClass("hide-order-discount");
  }

  if (totals.shippingLevelDiscountTotal.value > 0) {
    $(".shipping-discount").removeClass("hide-shipping-discount");
    $(".shipping-discount-total").text("- " + totals.shippingLevelDiscountTotal.formatted);
  } else {
    $(".shipping-discount").addClass("hide-shipping-discount");
  }
}
/**
 * updates the order product shipping summary for an order model
 * @param {Object} order - the order model
 */


function updateOrderProductSummaryInformation(order) {
  var $productSummary = $("<div />");
  order.shipping.forEach(function (shipping) {
    shipping.productLineItems.items.forEach(function (lineItem) {
      var pli = $("[data-product-line-item=" + lineItem.UUID + "]");
      $productSummary.append(pli);
    });
    var address = shipping.shippingAddress || {};
    var selectedMethod = shipping.selectedShippingMethod;
    var nameLine = address.firstName ? address.firstName + " " : "";
    if (address.lastName) nameLine += address.lastName;
    var address1Line = address.address1;
    var address2Line = address.address2;
    var phoneLine = address.phone;
    var shippingCost = selectedMethod ? selectedMethod.shippingCost : "";
    var methodNameLine = selectedMethod ? selectedMethod.displayName : "";
    var methodArrivalTime = selectedMethod && selectedMethod.estimatedArrivalTime ? "( " + selectedMethod.estimatedArrivalTime + " )" : "";
    var tmpl = $("#pli-shipping-summary-template").clone();

    if (shipping.productLineItems.items && shipping.productLineItems.items.length > 1) {
      $("h5 > span").text(" - " + shipping.productLineItems.items.length + " " + order.resources.items);
    } else {
      $("h5 > span").text("");
    }

    var stateRequiredAttr = $("#shippingState").attr("required");
    var isRequired = stateRequiredAttr !== undefined && stateRequiredAttr !== false;
    var stateExists = shipping.shippingAddress && shipping.shippingAddress.stateCode ? shipping.shippingAddress.stateCode : false;
    var stateBoolean = false;

    if (isRequired && stateExists || !isRequired) {
      stateBoolean = true;
    }

    var shippingForm = $(".multi-shipping input[name=\"shipmentUUID\"][value=\"" + shipping.UUID + "\"]").parent();

    if (shipping.shippingAddress && shipping.shippingAddress.firstName && shipping.shippingAddress.address1 && shipping.shippingAddress.city && stateBoolean && shipping.shippingAddress.countryCode && (shipping.shippingAddress.phone || shipping.productLineItems.items[0].fromStoreId)) {
      $(".ship-to-name", tmpl).text(nameLine);
      $(".ship-to-address1", tmpl).text(address1Line);
      $(".ship-to-address2", tmpl).text(address2Line);
      $(".ship-to-city", tmpl).text(address.city);

      if (address.stateCode) {
        $(".ship-to-st", tmpl).text(address.stateCode);
      }

      $(".ship-to-zip", tmpl).text(address.postalCode);
      $(".ship-to-phone", tmpl).text(phoneLine);

      if (!address2Line) {
        $(".ship-to-address2", tmpl).hide();
      }

      if (!phoneLine) {
        $(".ship-to-phone", tmpl).hide();
      }

      shippingForm.find(".ship-to-message").text("");
    } else {
      shippingForm.find(".ship-to-message").text(order.resources.addressIncomplete);
    }

    if (shipping.isGift) {
      $(".gift-message-summary", tmpl).text(shipping.giftMessage);
    } else {
      $(".gift-summary", tmpl).addClass("d-none");
    } // checking h5 title shipping to or pickup


    var $shippingAddressLabel = $(".shipping-header-text", tmpl);
    $("body").trigger("shipping:updateAddressLabelText", {
      selectedShippingMethod: selectedMethod,
      resources: order.resources,
      shippingAddressLabel: $shippingAddressLabel
    });

    if (shipping.selectedShippingMethod) {
      $(".display-name", tmpl).text(methodNameLine);
      $(".arrival-time", tmpl).text(methodArrivalTime);
      $(".price", tmpl).text(shippingCost);
    }

    var $shippingSummary = $("<div class=\"multi-shipping\" data-shipment-summary=\"" + shipping.UUID + "\" />");
    $shippingSummary.html(tmpl.html());
    $productSummary.append($shippingSummary);
  });
  $(".product-summary-block").html($productSummary.html()); // Also update the line item prices, as they might have been altered

  $(".grand-total-price").text(order.totals.subTotal);
  order.items.items.forEach(function (item) {
    if (item.priceTotal && item.priceTotal.renderedPrice) {
      $(".item-total-" + item.UUID).empty().append(item.priceTotal.renderedPrice);
    }
  });
}

module.exports = {
  updateTotals: updateTotals,
  updateOrderProductSummaryInformation: updateOrderProductSummaryInformation
};

/***/ }),

/***/ "./cartridges/app_refapp/cartridge/client/default/js/components/cleave.js":
/*!********************************************************************************!*\
  !*** ./cartridges/app_refapp/cartridge/client/default/js/components/cleave.js ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Cleave = __webpack_require__(/*! cleave.js */ "./node_modules/cleave.js/dist/cleave-esm.js")["default"];

module.exports = {
  handleCreditCardNumber: function handleCreditCardNumber(cardFieldSelector, cardTypeSelector) {
    var cleave = new Cleave(cardFieldSelector, {
      creditCard: true,
      onCreditCardTypeChanged: function onCreditCardTypeChanged(type) {
        window.ccType = type;
        var creditCardTypes = {
          visa: "Visa",
          mastercard: "Master Card",
          amex: "Amex",
          discover: "Discover",
          maestro: "Maestro",
          unknown: "Unknown"
        };
        var cardType = creditCardTypes[Object.keys(creditCardTypes).indexOf(type) > -1 ? type : "unknown"];
        $(cardTypeSelector).val(cardType);
        $(".card-number-wrapper").attr("data-type", type);

        if (type === "visa" || type === "mastercard" || type === "discover") {
          $("#securityCode").attr("maxlength", 3);
        } else {
          $("#securityCode").attr("maxlength", 4);
        }
      }
    });

    if ($("li[data-method-id='CREDIT_CARD']").attr("data-sa-type") != "SA_FLEX") {
      $(cardFieldSelector).data("cleave", cleave);
    }
  },
  serializeData: function serializeData(form) {
    var serializedArray = form.serializeArray();
    serializedArray.forEach(function (item) {
      if (item.name.indexOf("cardNumber") > -1) {
        item.value = $('#cardNumber').data('cleave').getRawValue(); // eslint-disable-line
      }
    });
    return $.param(serializedArray);
  }
};

/***/ }),

/***/ "./cartridges/app_refapp/cartridge/client/default/js/components/scrollAnimate.js":
/*!***************************************************************************************!*\
  !*** ./cartridges/app_refapp/cartridge/client/default/js/components/scrollAnimate.js ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (element, offset) {
  var position = element && element.length ? element.offset().top : 0;

  if (offset) {
    position -= offset;
  }

  $("html, body").animate({
    scrollTop: position
  }, 500);

  if (!element) {
    $(".logo-home").focus();
  }
};

/***/ }),

/***/ "./cartridges/app_refapp/cartridge/client/default/js/formatters.js":
/*!*************************************************************************!*\
  !*** ./cartridges/app_refapp/cartridge/client/default/js/formatters.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var InputMask = __webpack_require__(/*! inputmask */ "./node_modules/inputmask/index.js");

var formatters = {
  initPhoneNumbersFormatter: function initPhoneNumbersFormatter() {
    var phoneFields = document.querySelectorAll("input#shippingPhoneNumberdefault, input#phoneNumber, input#phone, input#registration-form-phone, input[id*='PhoneNumber']");
    InputMask("999-999-9999").mask(phoneFields);
  },
  init: function init() {
    formatters.initPhoneNumbersFormatter();
  }
};
module.exports = formatters;

/***/ }),

/***/ "./cartridges/app_refapp/cartridge/client/default/js/util.js":
/*!*******************************************************************!*\
  !*** ./cartridges/app_refapp/cartridge/client/default/js/util.js ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

module.exports = function (include) {
  if (typeof include === "function") {
    include();
  } else if (_typeof(include) === "object") {
    Object.keys(include).forEach(function (key) {
      if (typeof include[key] === "function") {
        include[key]();
      }
    });
  }
};

/***/ }),

/***/ "./cartridges/int_calculate_shipping/cartridge/client/default/js/checkout.js":
/*!***********************************************************************************!*\
  !*** ./cartridges/int_calculate_shipping/cartridge/client/default/js/checkout.js ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var processInclude = __webpack_require__(/*! @refapp/js/util */ "./cartridges/app_refapp/cartridge/client/default/js/util.js");

$(document).ready(function () {
  processInclude(__webpack_require__(/*! ./checkout/checkout */ "./cartridges/int_calculate_shipping/cartridge/client/default/js/checkout/checkout.js"));
  processInclude(__webpack_require__(/*! ./checkout/mercadoPago */ "./cartridges/int_calculate_shipping/cartridge/client/default/js/checkout/mercadoPago.js"));
});

/***/ }),

/***/ "./cartridges/int_calculate_shipping/cartridge/client/default/js/checkout/billing.js":
/*!*******************************************************************************************!*\
  !*** ./cartridges/int_calculate_shipping/cartridge/client/default/js/checkout/billing.js ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var base = __webpack_require__(/*! @refapp/js/checkout/billing */ "./cartridges/app_refapp/cartridge/client/default/js/checkout/billing.js");
/**
 * @function updatePaymentInformation
 * @description Update payment details summary based on payment method
 * @param {Object} order - checkout model to use as basis of new truth
 */


base.methods.updatePaymentInformation = function (order) {
  // update payment details
  var $paymentSummary = $(".payment-details");
  var htmlToAppend = "";

  if (order.billing.payment && order.billing.payment.selectedPaymentInstruments && order.billing.payment.selectedPaymentInstruments.length > 0) {
    if (order.billing.payment.selectedPaymentInstruments[0].paymentMethod == "CREDIT_CARD") {
      htmlToAppend += "<span>" + order.resources.cardType + " " + order.billing.payment.selectedPaymentInstruments[0].type + "</span><div>" + order.billing.payment.selectedPaymentInstruments[0].maskedCreditCardNumber + "</div><div><span>" + order.resources.cardEnding + " " + order.billing.payment.selectedPaymentInstruments[0].expirationMonth + "/" + order.billing.payment.selectedPaymentInstruments[0].expirationYear + "</span></div>";
    } else if (order.billing.payment.selectedPaymentInstruments[0].paymentMethod == "MercadoPago") {
      var paymentMethods = $("[data-mp-available-payment-methods]").data("mpAvailablePaymentMethods");
      var paymentInstrumentType = order.billing.payment.selectedPaymentInstruments[0].type;
      var paymentMethod = paymentMethods.find(function (method) {
        return paymentInstrumentType == method.id;
      });
      var paymentMethodName = paymentMethod ? paymentMethod.name : paymentInstrumentType;
      htmlToAppend += "<span>" + order.billing.payment.selectedPaymentInstruments[0].paymentMethod + " " + paymentMethodName + "</span>";
    }
  }

  $paymentSummary.empty().append(htmlToAppend);
};
/**
 * @function handlePaymentOptionChange
 * @description Handle payment option change
 */


base.methods.handlePaymentOptionChange = function () {
  var $activeTab = $(this);
  var activeTabId = $activeTab.attr("href");
  var $paymentInformation = $(".payment-information");
  var isNewPayment = $(".user-payment-instruments").hasClass("checkout-hidden");
  $(".payment-options [role=tab]").each(function (i, tab) {
    var otherTab = $(tab);
    var otherTabId = otherTab.attr("href");
    $(otherTabId).find("input, select").prop("disabled", otherTabId !== activeTabId);
  });

  if (activeTabId === "#credit-card-content") {
    // Restore
    $paymentInformation.data("is-new-payment", isNewPayment);
  } else {
    // Prevent rejection during payment submit
    $paymentInformation.data("is-new-payment", true);
  }
};
/**
 * @function useSameMailPhoneAsAddress
 * @description fill user information for payment data
 */


base.useSameMailPhoneAsAddress = function () {
  var fillSameFields = function fillSameFields() {
    $("[data-mp-phone]").val($("#phoneNumber").val());
    $("[data-mp-email]").val($("#email").val());
  };

  $("#useSameMailPhoneAsAddress").change(function () {
    $("[data-mail-phone-container]").toggleClass("checkout-hidden", this.checked);

    if (this.checked) {
      fillSameFields();
      $("#phoneNumber").on("change.usesame", fillSameFields);
      $("#email").on("change.usesame", fillSameFields);
    } else {
      $("[data-mp-phone]").val("");
      $("[data-mp-email]").val("");
      $("#phoneNumber").off("change.usesame");
      $("#email").off("change.usesame");
    }
  });
};
/**
 * @function changePaymentOption
 * @description Change payment option
 */


base.changePaymentOption = function () {
  $(".payment-options [role=tab]").on("click", base.methods.handlePaymentOptionChange); // By click
};
/**
 * @function initPaymentOption
 * @description Initiate payment option
 */


base.initPaymentOption = function () {
  // Initial
  $(".payment-options [role=tab].enabled").trigger("click");
  base.methods.handlePaymentOptionChange.call($(".payment-options [role=tab].active"));
};

module.exports = base;

/***/ }),

/***/ "./cartridges/int_calculate_shipping/cartridge/client/default/js/checkout/checkout.js":
/*!********************************************************************************************!*\
  !*** ./cartridges/int_calculate_shipping/cartridge/client/default/js/checkout/checkout.js ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var billingHelpers = __webpack_require__(/*! ./billing */ "./cartridges/int_calculate_shipping/cartridge/client/default/js/checkout/billing.js");

var shippingHelpers = __webpack_require__(/*! ./shipping */ "./cartridges/int_calculate_shipping/cartridge/client/default/js/checkout/shipping.js");

var base = __webpack_require__(/*! @refapp/js/checkout/checkout */ "./cartridges/app_refapp/cartridge/client/default/js/checkout/checkout.js");

var formatters = __webpack_require__(/*! @refapp/js/formatters */ "./cartridges/app_refapp/cartridge/client/default/js/formatters.js");

[billingHelpers, shippingHelpers].forEach(function (library) {
  Object.keys(library).forEach(function (item) {
    if (_typeof(library[item]) === "object") {
      base[item] = $.extend({}, base[item], library[item]);
    } else {
      base[item] = library[item];
    }
  });
});
base.formatters = formatters.init;
module.exports = base;

/***/ }),

/***/ "./cartridges/int_calculate_shipping/cartridge/client/default/js/checkout/mercadoPago.js":
/*!***********************************************************************************************!*\
  !*** ./cartridges/int_calculate_shipping/cartridge/client/default/js/checkout/mercadoPago.js ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* eslint-disable no-undef */


(function ($) {
  /**
   * @constructor
   * @classdesc Integration class
   */
  function MercadoPago() {
    var that = this;
    var $content = $("[data-mp-content]");
    var $form = $content.find("[data-mp-form]");
    var $elements = {
      paymentOptionTab: $("[data-payment-option-tab]"),
      paymentTypeButton: $content.find("[data-toggle-payment-type]"),
      customerCardsContainer: $content.find("[data-mp-customer-cards]"),
      customerCard: $content.find("[data-mp-customer-card]")
    }; // Regular fields

    var fields = {
      cardType: {
        $el: $form.find("[data-mp-card-type]"),
        disable: {
          other: false,
          stored: false
        },
        hide: {
          other: false,
          stored: true
        },
        errors: []
      },
      cardHolder: {
        $el: $form.find("[data-mp-card-holder]"),
        disable: {
          other: true,
          stored: true
        },
        hide: {
          other: true,
          stored: true
        },
        errors: ["221", "316"]
      },
      cardNumber: {
        $el: $form.find("[data-mp-card-number]"),
        disable: {
          other: true,
          stored: true
        },
        hide: {
          other: true,
          stored: true
        },
        errors: ["205", "E301", "ECYA301"]
      },
      cardMonth: {
        $el: $form.find("[data-mp-card-month]"),
        disable: {
          other: true,
          stored: true
        },
        hide: {
          other: true,
          stored: true
        },
        errors: ["208", "325"]
      },
      cardYear: {
        $el: $form.find("[data-mp-card-year]"),
        disable: {
          other: true,
          stored: true
        },
        hide: {
          other: true,
          stored: true
        },
        errors: ["209", "326"]
      },
      securityCode: {
        $el: $form.find("[data-mp-security-code]"),
        disable: {
          other: true,
          stored: false
        },
        hide: {
          other: true,
          stored: false
        },
        errors: ["224", "E302", "E203"]
      },
      email: {
        $el: $form.find("[data-mp-email]"),
        disable: {
          other: false,
          stored: true
        },
        hide: {
          other: false,
          stored: true
        },
        errors: ["email"]
      },
      phone: {
        $el: $form.find("[data-mp-phone]"),
        disable: {
          other: false,
          stored: true
        },
        hide: {
          other: false,
          stored: true
        },
        errors: ["phone"]
      },
      saveCard: {
        $el: $form.find("[data-mp-save-card]"),
        disable: {
          other: true,
          stored: true
        },
        hide: {
          other: true,
          stored: true
        },
        errors: []
      },
      useSameMailPhoneAsAddress: {
        $el: $form.find("[data-mp-use-same]"),
        disable: {
          other: false,
          stored: true
        },
        hide: {
          other: false,
          stored: true
        },
        errors: []
      }
    }; // Extended fields

    fields.issuer = {
      $el: $form.find("[data-mp-issuer]"),
      disable: {
        other: true,
        stored: false
      },
      hide: {
        other: true,
        stored: false
      },
      errors: ["issuer"]
    };
    fields.installments = {
      $el: $form.find("[data-mp-installments]"),
      disable: {
        other: true,
        stored: false
      },
      hide: {
        other: true,
        stored: false
      },
      errors: ["installments"]
    };
    fields.docType = {
      $el: $form.find("[data-mp-doc-type]"),
      disable: {
        other: false,
        stored: false
      },
      hide: {
        other: false,
        stored: false
      },
      errors: ["212", "322"]
    };
    fields.docNumber = {
      $el: $form.find("[data-mp-doc-number]"),
      $wrapper: $form.find("[data-mp-doc-wrapper]"),
      $label: $form.find("[data-mp-doc-label]"),
      $tooltip: $form.find("[data-mp-doc-tooltip]"),
      disable: {
        other: false,
        stored: false
      },
      hide: {
        other: false,
        stored: false
      },
      errors: ["214", "324"]
    };
    fields.installmentsContainer = {
      $el: $form.find("[data-mp-installments-container]"),
      disable: {
        other: false,
        stored: false
      },
      hide: {
        other: false,
        stored: true
      },
      errors: []
    }, // Hidden fields
    Object.defineProperty(fields, "cardId", {
      enumerable: false,
      value: {
        $el: $form.find("[data-mp-card-id]")
      }
    });
    Object.defineProperty(fields, "token", {
      enumerable: false,
      value: {
        $el: $form.find("[data-mp-token]")
      }
    });
    var methods = {
      paymentOption: {
        /**
         * @function handleChange
         * @description Handle change of payment method and set initial state of payment tab
         */
        handleChange: function handleChange() {
          var $activeTab = $(this);
          var methodId = $activeTab.closest("[data-method-id]").data("methodId");
          var initialState = $form.data("mpInitial");
          methodId === that.configuration.paymentMethodId && methods.paymentOption.setInitialState[initialState + "Payment"]();
        },
        setInitialState: {
          /**
           * @function newPayment
           * @description Set initial state for new payment section
           */
          newPayment: function newPayment() {
            var paymentMethodInput = fields.cardType.$el.filter(function () {
              return this.value === that.configuration.defaultCardType;
            }); // Check default card type

            paymentMethodInput.prop("checked", true);
            methods.card.handleTypeChange.call(paymentMethodInput[0], {
              data: {
                handleOther: true
              }
            });
          },

          /**
           * @function storedPayment
           * @description Set initial state for stored payment section
           */
          storedPayment: function storedPayment() {
            var firstCard = $elements.customerCard.filter(":first"); // Select first card

            methods.registeredCustomer.selectCustomerCard.call(firstCard[0]); // Toggle payment type to stored

            $elements.paymentTypeButton.data("togglePaymentType", "stored");
            methods.registeredCustomer.togglePaymentType.call($elements.paymentTypeButton[0]);
          },

          /**
           * @function restoreStoredPayment
           * @description Restore stored payment section
           */
          restoreStoredPayment: function restoreStoredPayment() {
            var firstCard = $elements.customerCard.filter(":first"); // Select first card

            methods.registeredCustomer.selectCustomerCard.call(firstCard[0]); // Show and set disabled to false for stored payment fields

            for (var field in fields) {
              !fields[field].hide.stored && fields[field].$el.closest("[data-mp-container]").removeClass("checkout-hidden");
              !fields[field].disable.stored && fields[field].$el.prop("disabled", false);
            }
          }
        }
      },
      token: {
        /**
         * @function populate
         * @description Create token and populate field with value during submit
         * @param {Event} event
         * @param {Object} eventData
         */
        populate: function populate(event, eventData) {
          // Continue default flow
          if (eventData && eventData["continue"]) {
            return;
          } // Stop default flow


          event.stopImmediatePropagation();
          var isOtherPaymentMethod = fields.cardType.$el.filter(":checked").data("mpCardType") === that.configuration.otherPaymentMethod;
          var isMercadoPago = $("input[name$=\"billing_paymentMethod\"]:enabled").val() === that.configuration.paymentMethodId;
          var isCyaType = fields.cardType.$el.filter(":checked").val() == 'bradescard';

          if (isOtherPaymentMethod || !isMercadoPago) {
            fields.token.$el.val("");
            $(".next-step-button .submit-payment").trigger("click", {
              "continue": true
            });
            return;
          }

          if (isCyaType && !methods.token.isBradesCard()) {
            methods.token.errorResponse(fields.cardNumber.errors[2]);
            return;
          }

          Mercadopago.createToken($form, function (status, serviceResponse) {
            var validForm = true;
            Object.keys(fields).forEach(function (index) {
              var field = fields[index];

              if (field.$el.attr("required") && field.$el.is(":visible") && field.$el.is(":enabled")) {
                if (field.$el.val().length > 0) {
                  field.$el.next(".invalid-feedback").hide();
                } else if (field.errors && field.errors.indexOf(index) != -1) {
                  methods.token.errorResponse(index);
                  validForm = false;
                }
              }
            });

            if ((status === 200 || status === 201) && validForm) {
              methods.token.successResponse(serviceResponse);
            } else {
              if (fields.cardHolder.$el.attr("required") && fields.cardHolder.$el.is(":visible") && fields.cardHolder.$el.is(":enabled") && fields.cardHolder.$el.val().length === 0) {
                methods.token.errorResponse(fields.cardHolder.errors[0]);
              }

              if (fields.securityCode.$el.attr("required") && fields.securityCode.$el.is(":visible") && fields.securityCode.$el.is(":enabled") && fields.securityCode.$el.val().length === 0) {
                methods.token.errorResponse(fields.securityCode.errors[0]);
              }

              if (fields.docNumber.$el.attr("required") && fields.docNumber.$el.is(":visible") && fields.docNumber.$el.is(":enabled") && fields.docNumber.$el.val().length === 0) {
                methods.token.errorResponse(fields.docNumber.errors[0]);
              }

              if (serviceResponse.cause) {
                serviceResponse.cause.forEach(function (cause) {
                  methods.token.errorResponse(cause.code);
                });
              }
            }
          });
        },

        /**
         * @function successResponse
         * @description Success callback for token creation
         * @param {Object} serviceResponse
         */
        successResponse: function successResponse(serviceResponse) {
          Object.keys(fields).forEach(function (fieldKey) {
            var field = fields[fieldKey];
            field.$el.next(".invalid-feedback").hide();
          });
          fields.token.$el.val(serviceResponse.id);
          $(".next-step-button .submit-payment").trigger("click", {
            "continue": true
          });
        },

        /**
         * @function errorResponse
         * @description Error callback for token creation
         * @param {String} errorCode
         */
        errorResponse: function errorResponse(errorCode) {
          var errorMessages = $form.data("mpErrorMessages");
          var errorField; // Set error code message if found, otherwise set default error message

          var errorMessage = errorMessages[errorCode] ? errorMessages[errorCode] : errorMessages["default"];
          Object.keys(fields).forEach(function (index) {
            var field = fields[index];

            if (field.errors && field.errors.indexOf(errorCode) !== -1) {
              errorField = field;
              return true;
            }
          });

          if (errorField) {
            errorField.$el.next(".invalid-feedback").focus().show().text(errorMessage);
          } else {
            $(".error-message").show();
            $(".error-message-text").text(errorMessage);
          }
        },

        /**
        * @function isBradesCard
         * @description Check if is bradescard
         */
        isBradesCard: function isBradesCard() {
          var valid = false;
          var bradescardBines = JSON.parse(that.preferences.bradescardBines);
          var data = fields.cardNumber.$el.val().replace(/\s/g, '');
          var bin = parseInt(data.substr(0, 6), 10);

          if (bradescardBines.hasOwnProperty("binesRange")) {
            var binesRange = bradescardBines.binesRange;

            for (var i = 0; binesRange.length > i; i++) {
              var bines = binesRange[i].bines;
              var binFrom = parseInt(bines.substring(0, bines.indexOf('-')), 10);
              var binTo = parseInt(bines.substring(bines.indexOf('-') + 1, bines.length), 10);

              if (bin >= binFrom && bin <= binTo) {
                valid = true;
              }
            }
          }

          if (bradescardBines.hasOwnProperty("singleBines")) {
            var singleBines = bradescardBines.singleBines;

            for (var i = 0; singleBines.length > i; i++) {
              var singleBin = parseInt(singleBines[i].bin);

              if (bin == singleBin) {
                valid = true;
              }
            }
          }

          return valid;
        }
      },
      card: {
        /**
         * @function handleTypeChange
         * @description Handle credit card type change
         * @param {Event} e
         */
        handleTypeChange: function handleTypeChange(e) {
          var $el = $(this);
          var issuerMandatory = $el.data("mpIssuerRequired");
          var isOtherType = $el.data("mpCardType") === that.configuration.otherPaymentMethod;
          var isBradescard = fields.cardType.$el.filter(":checked").val() == 'bradescard'; // Handle fields for other payment method

          e.data.handleOther && methods.card.handleOtherType(isOtherType);

          if ((that.preferences.enableInstallments === true && !isBradescard || that.preferences.bradescardEnableInstallments === true && isBradescard) && !isOtherType) {
            fields.installmentsContainer.$el.show();
            methods.installment.set($el.val()); // Set issuer info

            if (issuerMandatory) {
              methods.issuer.set($el);
              fields.issuer.$el.prop("disabled", false);
              fields.issuer.$el.off("change").on("change", methods.installment.setByIssuerId);
            } else {
              fields.issuer.$el.prop("disabled", true);
            }
          } else {
            fields.issuer.$el.prop("disabled", true);
            fields.installments.$el.prop("disabled", true);
            fields.installmentsContainer.$el.hide();
          }
        },

        /**
         * @function handleOtherType
         * @description Toggle other payment method
         * @param {Boolean} isOtherType
         */
        handleOtherType: function handleOtherType(isOtherType) {
          for (var field in fields) {
            fields[field].hide.other && fields[field].$el.closest("[data-mp-container]").toggleClass("checkout-hidden", isOtherType);
            fields[field].disable.other && fields[field].$el.prop("disabled", isOtherType);
          }
        }
      },
      installment: {
        /**
         * @function set
         * @description Set installments
         * @param {String} paymentMethodId
         */
        set: function set(paymentMethodId) {
          // Set installments info
          paymentMethodId = paymentMethodId == "bradescard" ? "visa" : paymentMethodId;
          Mercadopago.getInstallments({
            payment_method_id: paymentMethodId,
            amount: $form.data("mpCartTotal")
          }, methods.installment.handleServiceResponse);
        },

        /**
         * @function handleServiceResponse
         * @description Callback for installments
         * @param {Number} status
         * @param {Array} response
         */
        handleServiceResponse: function handleServiceResponse(status, response) {
          fields.installments.$el.find("option").remove();

          if (status != 200 && status != 201) {
            return;
          }

          var payment_method_id = fields.cardType.$el.filter(":checked").val();
          var amount = parseInt($form.data("mpCartTotal"));
          var $defaultOption = $(new Option(that.resourceMessages.defaultInstallments, ""));
          fields.installments.$el.append($defaultOption);

          if (payment_method_id == "bradescard" && !(amount >= that.preferences.bradescardMinimumAmountForInstallment)) {
            fields.issuer.$el.prop("disabled", true);
            fields.installments.$el.prop("disabled", true);
            return;
          } else if (payment_method_id != "bradescard" && !(amount >= that.preferences.minimumAmountForInstallment)) {
            fields.issuer.$el.prop("disabled", true);
            fields.installments.$el.prop("disabled", true);
            return;
          }

          $.each(response[0].payer_costs, function (i, item) {
            if (payment_method_id == "bradescard" && item.installments > that.preferences.bradescardNumberOfInstallments) {
              return;
            } else if (payment_method_id != "bradescard" && item.installments > that.preferences.numberOfInstallments) {
              return;
            }

            fields.installments.$el.append($("<option>", {
              value: item.installments,
              text: item.recommended_message || item.installments
            }));

            if (fields.installments.$el.val() !== "" && fields.installments.$el.val() === item.installments) {
              fields.installments.$el.val(item.installments);
            }
          });
        },

        /**
         * @function handleServiceResponse
         * @description Set installments using issuer ID
         */
        setByIssuerId: function setByIssuerId() {
          var issuerId = $(this).val();

          if (!issuerId || issuerId === "-1") {
            return;
          }

          Mercadopago.getInstallments({
            payment_method_id: fields.cardType.$el.filter(":checked").val(),
            amount: $form.data("mpCartTotal"),
            issuer_id: issuerId
          }, methods.installment.handleServiceResponse);
        }
      },
      issuer: {
        /**
         * @function set
         * @description Set issuer
         * @param {jQuery} element
         */
        set: function set($element) {
          Mercadopago.getIssuers($element.val(), methods.issuer.handleServiceResponse);
        },

        /**
         * @function handleServiceResponse
         * @description Callback for issuer
         * @param {Number} status
         * @param {Array} response
         */
        handleServiceResponse: function handleServiceResponse(status, response) {
          fields.issuer.$el.find("option").remove();

          if (status != 200 && status != 201) {
            return;
          }

          var $defaultOption = $(new Option(that.resourceMessages.defaultIssuer, ""));
          fields.issuer.$el.append($defaultOption);
          $.each(response, function (i, item) {
            fields.issuer.$el.append($("<option>", {
              value: item.id,
              text: item.name !== "default" ? item.name : that.configuration.defaultIssuer
            }));

            if (fields.issuer.$el.val() !== "" && fields.issuer.$el.val() === item.id) {
              fields.issuer.$el.val(item.id);
            }
          });
        }
      },
      docType: {
        /**
         * @function init
         * @description Init identification document type
         */
        init: function init() {
          Mercadopago.getIdentificationTypes(methods.docType.handleServiceResponse);
        },

        /**
         * @function handleServiceResponse
         * @description Callback for identification document type
         * @param {Number} status
         * @param {Array} response
         */
        handleServiceResponse: function handleServiceResponse(status, response) {
          fields.docType.$el.find("option").remove();

          if (status != 200 && status != 201) {
            return;
          }

          $.each(response, function (i, item) {
            fields.docType.$el.append($("<option>", {
              value: item.id,
              text: item.name,
              "data-min-length": item.min_length,
              "data-max-length": item.max_length
            }));
          });
          fields.docType.$el.trigger("change");
        }
      },
      docNumber: {
        /**
         * @function setRange
         * @description Set range identification document number
         */
        setRange: function setRange() {
          var $selectedOption = $(this).find("option:selected");
          var minLength = $selectedOption.data("minLength");
          var maxLength = $selectedOption.data("maxLength"); // Set label

          var labelSecondPart = $selectedOption.val() === that.configuration.docTypeDNI ? that.resourceMessages.docNumberLabelDNI : that.resourceMessages.docNumberLabelOther;
          fields.docNumber.$label.text(that.resourceMessages.docNumberLabel + " " + labelSecondPart); // Set range

          fields.docNumber.$wrapper.addClass("required");
          fields.docNumber.$el.attr("maxlength", maxLength);
          fields.docNumber.$el.attr("minlength", minLength); // Set tooltip

          fields.docNumber.$tooltip.text(that.resourceMessages.docNumberTooltip.replace("{0}", minLength).replace("{1}", maxLength));
        }
      },
      registeredCustomer: {
        /**
         * @function togglePaymentType
         * @description Toggle payment type (new or stored)
         * @param {Event} event
         */
        togglePaymentType: function togglePaymentType(event) {
          var $el = $(this);
          var isNew = $el.data("togglePaymentType") === "new";
          $elements.customerCardsContainer.toggleClass("checkout-hidden", isNew); // Disable and remove value to properly create token

          fields.cardId.$el.prop("disabled", isNew);
          isNew && fields.cardId.$el.val("");

          for (var field in fields) {
            fields[field].hide.stored && fields[field].$el.closest("[data-mp-container]").toggleClass("checkout-hidden", !isNew);
            fields[field].disable.stored && fields[field].$el.prop("disabled", !isNew);
          } // Set initial states


          isNew && methods.paymentOption.setInitialState.newPayment(); // Only when triggered from event (to avoid recursion)

          event && !isNew && methods.paymentOption.setInitialState.restoreStoredPayment(); // Change to opposite

          $el.data("togglePaymentType", isNew ? "stored" : "new");
          $el.text($el.data((isNew ? "stored" : "new") + "PaymentText"));
        },

        /**
         * @function selectCustomerCard
         * @description Select store credit card
         */
        selectCustomerCard: function selectCustomerCard() {
          var $el = $(this);
          $elements.customerCard.removeClass("selected-payment");
          $el.addClass("selected-payment");
          fields.cardId.$el.val($el.data("mpCustomerCard"));
          var paymentMethodInput = fields.cardType.$el.filter(function () {
            return this.value === $el.data("mpMethodId");
          });
          paymentMethodInput.prop("checked", true);
          methods.card.handleTypeChange.call(paymentMethodInput[0], {
            data: {
              handleOther: false
            }
          });
        }
      }
    };
    /**
     * @function initSDK
     * @description Init MercadoPago JS SDK by setting public key
     */

    function initSDK() {
      window.Mercadopago.setPublishableKey(that.preferences.publicKey);
    }
    /**
     * @function events
     * @description Init events
     */


    function events() {
      $elements.paymentOptionTab.on("click", methods.paymentOption.handleChange); // By click

      fields.cardType.$el.on("change", {
        handleOther: true
      }, methods.card.handleTypeChange);
      fields.docType.$el.on("change", methods.docNumber.setRange);
      $elements.paymentTypeButton.on("click", methods.registeredCustomer.togglePaymentType);
      $elements.customerCard.on("click", methods.registeredCustomer.selectCustomerCard);
      $(".next-step-button .submit-payment").on("click", methods.token.populate);
    }

    this.preferences = $form.data("mpPreferences");
    this.resourceMessages = $form.data("mpResourceMessages");
    this.configuration = $form.data("mpConfiguration");
    /**
     * @function init
     * @description Init integration
     */

    this.init = function () {
      if ($content.length > 0) {
        initSDK();
        that.preferences.enableDocTypeNumber && methods.docType.init();
        events();
        methods.paymentOption.handleChange.call($elements.paymentOptionTab.filter(".enabled")); // Initial
      }
    };
  }

  $(document).ready(new MercadoPago().init);
})(jQuery);

/***/ }),

/***/ "./cartridges/int_calculate_shipping/cartridge/client/default/js/checkout/shipping.js":
/*!********************************************************************************************!*\
  !*** ./cartridges/int_calculate_shipping/cartridge/client/default/js/checkout/shipping.js ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var base = __webpack_require__(/*! @refapp/js/checkout/shipping */ "./cartridges/app_refapp/cartridge/client/default/js/checkout/shipping.js");

base.updateShippingList = function () {
  var baseObj = this;
  $("select[name$=\"shippingAddress_addressFields_states_stateCode\"]").on("change", function (e) {
    console.info("updateShippingList - select --->");

    if (baseObj.methods && baseObj.methods.updateShippingMethodList) {
      baseObj.methods.updateShippingMethodList($(e.currentTarget.form));
    } else {
      updateShippingMethodList($(e.currentTarget.form));
    }
  });
  $("input[name$=\"shippingAddress_addressFields_city\"]").on("blur", function (e) {
    console.info("updateShippingList - city --->");

    if (baseObj.methods && baseObj.methods.updateShippingMethodList) {
      baseObj.methods.updateShippingMethodList($(e.currentTarget.form));
    } else {
      updateShippingMethodList($(e.currentTarget.form));
    }
  });
  $("input[name$=\"shippingAddress_addressFields_postalCode\"]").on("blur", function (e) {
    console.info("updateShippingList - postalCode --->");

    if (baseObj.methods && baseObj.methods.updateShippingMethodList) {
      baseObj.methods.updateShippingMethodList($(e.currentTarget.form));
    } else {
      updateShippingMethodList($(e.currentTarget.form));
    }
  });
};

module.exports = base;

/***/ }),

/***/ "./node_modules/cleave.js/dist/cleave-esm.js":
/*!***************************************************!*\
  !*** ./node_modules/cleave.js/dist/cleave-esm.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var commonjsGlobal = typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

var NumeralFormatter = function NumeralFormatter(numeralDecimalMark, numeralIntegerScale, numeralDecimalScale, numeralThousandsGroupStyle, numeralPositiveOnly, stripLeadingZeroes, prefix, signBeforePrefix, tailPrefix, delimiter) {
  var owner = this;
  owner.numeralDecimalMark = numeralDecimalMark || '.';
  owner.numeralIntegerScale = numeralIntegerScale > 0 ? numeralIntegerScale : 0;
  owner.numeralDecimalScale = numeralDecimalScale >= 0 ? numeralDecimalScale : 2;
  owner.numeralThousandsGroupStyle = numeralThousandsGroupStyle || NumeralFormatter.groupStyle.thousand;
  owner.numeralPositiveOnly = !!numeralPositiveOnly;
  owner.stripLeadingZeroes = stripLeadingZeroes !== false;
  owner.prefix = prefix || prefix === '' ? prefix : '';
  owner.signBeforePrefix = !!signBeforePrefix;
  owner.tailPrefix = !!tailPrefix;
  owner.delimiter = delimiter || delimiter === '' ? delimiter : ',';
  owner.delimiterRE = delimiter ? new RegExp('\\' + delimiter, 'g') : '';
};

NumeralFormatter.groupStyle = {
  thousand: 'thousand',
  lakh: 'lakh',
  wan: 'wan',
  none: 'none'
};
NumeralFormatter.prototype = {
  getRawValue: function getRawValue(value) {
    return value.replace(this.delimiterRE, '').replace(this.numeralDecimalMark, '.');
  },
  format: function format(value) {
    var owner = this,
        parts,
        partSign,
        partSignAndPrefix,
        partInteger,
        partDecimal = ''; // strip alphabet letters

    value = value.replace(/[A-Za-z]/g, '') // replace the first decimal mark with reserved placeholder
    .replace(owner.numeralDecimalMark, 'M') // strip non numeric letters except minus and "M"
    // this is to ensure prefix has been stripped
    .replace(/[^\dM-]/g, '') // replace the leading minus with reserved placeholder
    .replace(/^\-/, 'N') // strip the other minus sign (if present)
    .replace(/\-/g, '') // replace the minus sign (if present)
    .replace('N', owner.numeralPositiveOnly ? '' : '-') // replace decimal mark
    .replace('M', owner.numeralDecimalMark); // strip any leading zeros

    if (owner.stripLeadingZeroes) {
      value = value.replace(/^(-)?0+(?=\d)/, '$1');
    }

    partSign = value.slice(0, 1) === '-' ? '-' : '';

    if (typeof owner.prefix != 'undefined') {
      if (owner.signBeforePrefix) {
        partSignAndPrefix = partSign + owner.prefix;
      } else {
        partSignAndPrefix = owner.prefix + partSign;
      }
    } else {
      partSignAndPrefix = partSign;
    }

    partInteger = value;

    if (value.indexOf(owner.numeralDecimalMark) >= 0) {
      parts = value.split(owner.numeralDecimalMark);
      partInteger = parts[0];
      partDecimal = owner.numeralDecimalMark + parts[1].slice(0, owner.numeralDecimalScale);
    }

    if (partSign === '-') {
      partInteger = partInteger.slice(1);
    }

    if (owner.numeralIntegerScale > 0) {
      partInteger = partInteger.slice(0, owner.numeralIntegerScale);
    }

    switch (owner.numeralThousandsGroupStyle) {
      case NumeralFormatter.groupStyle.lakh:
        partInteger = partInteger.replace(/(\d)(?=(\d\d)+\d$)/g, '$1' + owner.delimiter);
        break;

      case NumeralFormatter.groupStyle.wan:
        partInteger = partInteger.replace(/(\d)(?=(\d{4})+$)/g, '$1' + owner.delimiter);
        break;

      case NumeralFormatter.groupStyle.thousand:
        partInteger = partInteger.replace(/(\d)(?=(\d{3})+$)/g, '$1' + owner.delimiter);
        break;
    }

    if (owner.tailPrefix) {
      return partSign + partInteger.toString() + (owner.numeralDecimalScale > 0 ? partDecimal.toString() : '') + owner.prefix;
    }

    return partSignAndPrefix + partInteger.toString() + (owner.numeralDecimalScale > 0 ? partDecimal.toString() : '');
  }
};
var NumeralFormatter_1 = NumeralFormatter;

var DateFormatter = function DateFormatter(datePattern, dateMin, dateMax) {
  var owner = this;
  owner.date = [];
  owner.blocks = [];
  owner.datePattern = datePattern;
  owner.dateMin = dateMin.split('-').reverse().map(function (x) {
    return parseInt(x, 10);
  });
  if (owner.dateMin.length === 2) owner.dateMin.unshift(0);
  owner.dateMax = dateMax.split('-').reverse().map(function (x) {
    return parseInt(x, 10);
  });
  if (owner.dateMax.length === 2) owner.dateMax.unshift(0);
  owner.initBlocks();
};

DateFormatter.prototype = {
  initBlocks: function initBlocks() {
    var owner = this;
    owner.datePattern.forEach(function (value) {
      if (value === 'Y') {
        owner.blocks.push(4);
      } else {
        owner.blocks.push(2);
      }
    });
  },
  getISOFormatDate: function getISOFormatDate() {
    var owner = this,
        date = owner.date;
    return date[2] ? date[2] + '-' + owner.addLeadingZero(date[1]) + '-' + owner.addLeadingZero(date[0]) : '';
  },
  getBlocks: function getBlocks() {
    return this.blocks;
  },
  getValidatedDate: function getValidatedDate(value) {
    var owner = this,
        result = '';
    value = value.replace(/[^\d]/g, '');
    owner.blocks.forEach(function (length, index) {
      if (value.length > 0) {
        var sub = value.slice(0, length),
            sub0 = sub.slice(0, 1),
            rest = value.slice(length);

        switch (owner.datePattern[index]) {
          case 'd':
            if (sub === '00') {
              sub = '01';
            } else if (parseInt(sub0, 10) > 3) {
              sub = '0' + sub0;
            } else if (parseInt(sub, 10) > 31) {
              sub = '31';
            }

            break;

          case 'm':
            if (sub === '00') {
              sub = '01';
            } else if (parseInt(sub0, 10) > 1) {
              sub = '0' + sub0;
            } else if (parseInt(sub, 10) > 12) {
              sub = '12';
            }

            break;
        }

        result += sub; // update remaining string

        value = rest;
      }
    });
    return this.getFixedDateString(result);
  },
  getFixedDateString: function getFixedDateString(value) {
    var owner = this,
        datePattern = owner.datePattern,
        date = [],
        dayIndex = 0,
        monthIndex = 0,
        yearIndex = 0,
        dayStartIndex = 0,
        monthStartIndex = 0,
        yearStartIndex = 0,
        day,
        month,
        year,
        fullYearDone = false; // mm-dd || dd-mm

    if (value.length === 4 && datePattern[0].toLowerCase() !== 'y' && datePattern[1].toLowerCase() !== 'y') {
      dayStartIndex = datePattern[0] === 'd' ? 0 : 2;
      monthStartIndex = 2 - dayStartIndex;
      day = parseInt(value.slice(dayStartIndex, dayStartIndex + 2), 10);
      month = parseInt(value.slice(monthStartIndex, monthStartIndex + 2), 10);
      date = this.getFixedDate(day, month, 0);
    } // yyyy-mm-dd || yyyy-dd-mm || mm-dd-yyyy || dd-mm-yyyy || dd-yyyy-mm || mm-yyyy-dd


    if (value.length === 8) {
      datePattern.forEach(function (type, index) {
        switch (type) {
          case 'd':
            dayIndex = index;
            break;

          case 'm':
            monthIndex = index;
            break;

          default:
            yearIndex = index;
            break;
        }
      });
      yearStartIndex = yearIndex * 2;
      dayStartIndex = dayIndex <= yearIndex ? dayIndex * 2 : dayIndex * 2 + 2;
      monthStartIndex = monthIndex <= yearIndex ? monthIndex * 2 : monthIndex * 2 + 2;
      day = parseInt(value.slice(dayStartIndex, dayStartIndex + 2), 10);
      month = parseInt(value.slice(monthStartIndex, monthStartIndex + 2), 10);
      year = parseInt(value.slice(yearStartIndex, yearStartIndex + 4), 10);
      fullYearDone = value.slice(yearStartIndex, yearStartIndex + 4).length === 4;
      date = this.getFixedDate(day, month, year);
    } // mm-yy || yy-mm


    if (value.length === 4 && (datePattern[0] === 'y' || datePattern[1] === 'y')) {
      monthStartIndex = datePattern[0] === 'm' ? 0 : 2;
      yearStartIndex = 2 - monthStartIndex;
      month = parseInt(value.slice(monthStartIndex, monthStartIndex + 2), 10);
      year = parseInt(value.slice(yearStartIndex, yearStartIndex + 2), 10);
      fullYearDone = value.slice(yearStartIndex, yearStartIndex + 2).length === 2;
      date = [0, month, year];
    } // mm-yyyy || yyyy-mm


    if (value.length === 6 && (datePattern[0] === 'Y' || datePattern[1] === 'Y')) {
      monthStartIndex = datePattern[0] === 'm' ? 0 : 4;
      yearStartIndex = 2 - 0.5 * monthStartIndex;
      month = parseInt(value.slice(monthStartIndex, monthStartIndex + 2), 10);
      year = parseInt(value.slice(yearStartIndex, yearStartIndex + 4), 10);
      fullYearDone = value.slice(yearStartIndex, yearStartIndex + 4).length === 4;
      date = [0, month, year];
    }

    date = owner.getRangeFixedDate(date);
    owner.date = date;
    var result = date.length === 0 ? value : datePattern.reduce(function (previous, current) {
      switch (current) {
        case 'd':
          return previous + (date[0] === 0 ? '' : owner.addLeadingZero(date[0]));

        case 'm':
          return previous + (date[1] === 0 ? '' : owner.addLeadingZero(date[1]));

        case 'y':
          return previous + (fullYearDone ? owner.addLeadingZeroForYear(date[2], false) : '');

        case 'Y':
          return previous + (fullYearDone ? owner.addLeadingZeroForYear(date[2], true) : '');
      }
    }, '');
    return result;
  },
  getRangeFixedDate: function getRangeFixedDate(date) {
    var owner = this,
        datePattern = owner.datePattern,
        dateMin = owner.dateMin || [],
        dateMax = owner.dateMax || [];
    if (!date.length || dateMin.length < 3 && dateMax.length < 3) return date;
    if (datePattern.find(function (x) {
      return x.toLowerCase() === 'y';
    }) && date[2] === 0) return date;
    if (dateMax.length && (dateMax[2] < date[2] || dateMax[2] === date[2] && (dateMax[1] < date[1] || dateMax[1] === date[1] && dateMax[0] < date[0]))) return dateMax;
    if (dateMin.length && (dateMin[2] > date[2] || dateMin[2] === date[2] && (dateMin[1] > date[1] || dateMin[1] === date[1] && dateMin[0] > date[0]))) return dateMin;
    return date;
  },
  getFixedDate: function getFixedDate(day, month, year) {
    day = Math.min(day, 31);
    month = Math.min(month, 12);
    year = parseInt(year || 0, 10);

    if (month < 7 && month % 2 === 0 || month > 8 && month % 2 === 1) {
      day = Math.min(day, month === 2 ? this.isLeapYear(year) ? 29 : 28 : 30);
    }

    return [day, month, year];
  },
  isLeapYear: function isLeapYear(year) {
    return year % 4 === 0 && year % 100 !== 0 || year % 400 === 0;
  },
  addLeadingZero: function addLeadingZero(number) {
    return (number < 10 ? '0' : '') + number;
  },
  addLeadingZeroForYear: function addLeadingZeroForYear(number, fullYearMode) {
    if (fullYearMode) {
      return (number < 10 ? '000' : number < 100 ? '00' : number < 1000 ? '0' : '') + number;
    }

    return (number < 10 ? '0' : '') + number;
  }
};
var DateFormatter_1 = DateFormatter;

var TimeFormatter = function TimeFormatter(timePattern, timeFormat) {
  var owner = this;
  owner.time = [];
  owner.blocks = [];
  owner.timePattern = timePattern;
  owner.timeFormat = timeFormat;
  owner.initBlocks();
};

TimeFormatter.prototype = {
  initBlocks: function initBlocks() {
    var owner = this;
    owner.timePattern.forEach(function () {
      owner.blocks.push(2);
    });
  },
  getISOFormatTime: function getISOFormatTime() {
    var owner = this,
        time = owner.time;
    return time[2] ? owner.addLeadingZero(time[0]) + ':' + owner.addLeadingZero(time[1]) + ':' + owner.addLeadingZero(time[2]) : '';
  },
  getBlocks: function getBlocks() {
    return this.blocks;
  },
  getTimeFormatOptions: function getTimeFormatOptions() {
    var owner = this;

    if (String(owner.timeFormat) === '12') {
      return {
        maxHourFirstDigit: 1,
        maxHours: 12,
        maxMinutesFirstDigit: 5,
        maxMinutes: 60
      };
    }

    return {
      maxHourFirstDigit: 2,
      maxHours: 23,
      maxMinutesFirstDigit: 5,
      maxMinutes: 60
    };
  },
  getValidatedTime: function getValidatedTime(value) {
    var owner = this,
        result = '';
    value = value.replace(/[^\d]/g, '');
    var timeFormatOptions = owner.getTimeFormatOptions();
    owner.blocks.forEach(function (length, index) {
      if (value.length > 0) {
        var sub = value.slice(0, length),
            sub0 = sub.slice(0, 1),
            rest = value.slice(length);

        switch (owner.timePattern[index]) {
          case 'h':
            if (parseInt(sub0, 10) > timeFormatOptions.maxHourFirstDigit) {
              sub = '0' + sub0;
            } else if (parseInt(sub, 10) > timeFormatOptions.maxHours) {
              sub = timeFormatOptions.maxHours + '';
            }

            break;

          case 'm':
          case 's':
            if (parseInt(sub0, 10) > timeFormatOptions.maxMinutesFirstDigit) {
              sub = '0' + sub0;
            } else if (parseInt(sub, 10) > timeFormatOptions.maxMinutes) {
              sub = timeFormatOptions.maxMinutes + '';
            }

            break;
        }

        result += sub; // update remaining string

        value = rest;
      }
    });
    return this.getFixedTimeString(result);
  },
  getFixedTimeString: function getFixedTimeString(value) {
    var owner = this,
        timePattern = owner.timePattern,
        time = [],
        secondIndex = 0,
        minuteIndex = 0,
        hourIndex = 0,
        secondStartIndex = 0,
        minuteStartIndex = 0,
        hourStartIndex = 0,
        second,
        minute,
        hour;

    if (value.length === 6) {
      timePattern.forEach(function (type, index) {
        switch (type) {
          case 's':
            secondIndex = index * 2;
            break;

          case 'm':
            minuteIndex = index * 2;
            break;

          case 'h':
            hourIndex = index * 2;
            break;
        }
      });
      hourStartIndex = hourIndex;
      minuteStartIndex = minuteIndex;
      secondStartIndex = secondIndex;
      second = parseInt(value.slice(secondStartIndex, secondStartIndex + 2), 10);
      minute = parseInt(value.slice(minuteStartIndex, minuteStartIndex + 2), 10);
      hour = parseInt(value.slice(hourStartIndex, hourStartIndex + 2), 10);
      time = this.getFixedTime(hour, minute, second);
    }

    if (value.length === 4 && owner.timePattern.indexOf('s') < 0) {
      timePattern.forEach(function (type, index) {
        switch (type) {
          case 'm':
            minuteIndex = index * 2;
            break;

          case 'h':
            hourIndex = index * 2;
            break;
        }
      });
      hourStartIndex = hourIndex;
      minuteStartIndex = minuteIndex;
      second = 0;
      minute = parseInt(value.slice(minuteStartIndex, minuteStartIndex + 2), 10);
      hour = parseInt(value.slice(hourStartIndex, hourStartIndex + 2), 10);
      time = this.getFixedTime(hour, minute, second);
    }

    owner.time = time;
    return time.length === 0 ? value : timePattern.reduce(function (previous, current) {
      switch (current) {
        case 's':
          return previous + owner.addLeadingZero(time[2]);

        case 'm':
          return previous + owner.addLeadingZero(time[1]);

        case 'h':
          return previous + owner.addLeadingZero(time[0]);
      }
    }, '');
  },
  getFixedTime: function getFixedTime(hour, minute, second) {
    second = Math.min(parseInt(second || 0, 10), 60);
    minute = Math.min(minute, 60);
    hour = Math.min(hour, 60);
    return [hour, minute, second];
  },
  addLeadingZero: function addLeadingZero(number) {
    return (number < 10 ? '0' : '') + number;
  }
};
var TimeFormatter_1 = TimeFormatter;

var PhoneFormatter = function PhoneFormatter(formatter, delimiter) {
  var owner = this;
  owner.delimiter = delimiter || delimiter === '' ? delimiter : ' ';
  owner.delimiterRE = delimiter ? new RegExp('\\' + delimiter, 'g') : '';
  owner.formatter = formatter;
};

PhoneFormatter.prototype = {
  setFormatter: function setFormatter(formatter) {
    this.formatter = formatter;
  },
  format: function format(phoneNumber) {
    var owner = this;
    owner.formatter.clear(); // only keep number and +

    phoneNumber = phoneNumber.replace(/[^\d+]/g, ''); // strip non-leading +

    phoneNumber = phoneNumber.replace(/^\+/, 'B').replace(/\+/g, '').replace('B', '+'); // strip delimiter

    phoneNumber = phoneNumber.replace(owner.delimiterRE, '');
    var result = '',
        current,
        validated = false;

    for (var i = 0, iMax = phoneNumber.length; i < iMax; i++) {
      current = owner.formatter.inputDigit(phoneNumber.charAt(i)); // has ()- or space inside

      if (/[\s()-]/g.test(current)) {
        result = current;
        validated = true;
      } else {
        if (!validated) {
          result = current;
        } // else: over length input
        // it turns to invalid number again

      }
    } // strip ()
    // e.g. US: 7161234567 returns (716) 123-4567


    result = result.replace(/[()]/g, ''); // replace library delimiter with user customized delimiter

    result = result.replace(/[\s-]/g, owner.delimiter);
    return result;
  }
};
var PhoneFormatter_1 = PhoneFormatter;
var CreditCardDetector = {
  blocks: {
    uatp: [4, 5, 6],
    amex: [4, 6, 5],
    diners: [4, 6, 4],
    discover: [4, 4, 4, 4],
    mastercard: [4, 4, 4, 4],
    dankort: [4, 4, 4, 4],
    instapayment: [4, 4, 4, 4],
    jcb15: [4, 6, 5],
    jcb: [4, 4, 4, 4],
    maestro: [4, 4, 4, 4],
    visa: [4, 4, 4, 4],
    mir: [4, 4, 4, 4],
    unionPay: [4, 4, 4, 4],
    general: [4, 4, 4, 4]
  },
  re: {
    // starts with 1; 15 digits, not starts with 1800 (jcb card)
    uatp: /^(?!1800)1\d{0,14}/,
    // starts with 34/37; 15 digits
    amex: /^3[47]\d{0,13}/,
    // starts with 6011/65/644-649; 16 digits
    discover: /^(?:6011|65\d{0,2}|64[4-9]\d?)\d{0,12}/,
    // starts with 300-305/309 or 36/38/39; 14 digits
    diners: /^3(?:0([0-5]|9)|[689]\d?)\d{0,11}/,
    // starts with 51-55/2221–2720; 16 digits
    mastercard: /^(5[1-5]\d{0,2}|22[2-9]\d{0,1}|2[3-7]\d{0,2})\d{0,12}/,
    // starts with 5019/4175/4571; 16 digits
    dankort: /^(5019|4175|4571)\d{0,12}/,
    // starts with 637-639; 16 digits
    instapayment: /^63[7-9]\d{0,13}/,
    // starts with 2131/1800; 15 digits
    jcb15: /^(?:2131|1800)\d{0,11}/,
    // starts with 2131/1800/35; 16 digits
    jcb: /^(?:35\d{0,2})\d{0,12}/,
    // starts with 50/56-58/6304/67; 16 digits
    maestro: /^(?:5[0678]\d{0,2}|6304|67\d{0,2})\d{0,12}/,
    // starts with 22; 16 digits
    mir: /^220[0-4]\d{0,12}/,
    // starts with 4; 16 digits
    visa: /^4\d{0,15}/,
    // starts with 62/81; 16 digits
    unionPay: /^(62|81)\d{0,14}/
  },
  getStrictBlocks: function getStrictBlocks(block) {
    var total = block.reduce(function (prev, current) {
      return prev + current;
    }, 0);
    return block.concat(19 - total);
  },
  getInfo: function getInfo(value, strictMode) {
    var blocks = CreditCardDetector.blocks,
        re = CreditCardDetector.re; // Some credit card can have up to 19 digits number.
    // Set strictMode to true will remove the 16 max-length restrain,
    // however, I never found any website validate card number like
    // this, hence probably you don't want to enable this option.

    strictMode = !!strictMode;

    for (var key in re) {
      if (re[key].test(value)) {
        var matchedBlocks = blocks[key];
        return {
          type: key,
          blocks: strictMode ? this.getStrictBlocks(matchedBlocks) : matchedBlocks
        };
      }
    }

    return {
      type: 'unknown',
      blocks: strictMode ? this.getStrictBlocks(blocks.general) : blocks.general
    };
  }
};
var CreditCardDetector_1 = CreditCardDetector;
var Util = {
  noop: function noop() {},
  strip: function strip(value, re) {
    return value.replace(re, '');
  },
  getPostDelimiter: function getPostDelimiter(value, delimiter, delimiters) {
    // single delimiter
    if (delimiters.length === 0) {
      return value.slice(-delimiter.length) === delimiter ? delimiter : '';
    } // multiple delimiters


    var matchedDelimiter = '';
    delimiters.forEach(function (current) {
      if (value.slice(-current.length) === current) {
        matchedDelimiter = current;
      }
    });
    return matchedDelimiter;
  },
  getDelimiterREByDelimiter: function getDelimiterREByDelimiter(delimiter) {
    return new RegExp(delimiter.replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1'), 'g');
  },
  getNextCursorPosition: function getNextCursorPosition(prevPos, oldValue, newValue, delimiter, delimiters) {
    // If cursor was at the end of value, just place it back.
    // Because new value could contain additional chars.
    if (oldValue.length === prevPos) {
      return newValue.length;
    }

    return prevPos + this.getPositionOffset(prevPos, oldValue, newValue, delimiter, delimiters);
  },
  getPositionOffset: function getPositionOffset(prevPos, oldValue, newValue, delimiter, delimiters) {
    var oldRawValue, newRawValue, lengthOffset;
    oldRawValue = this.stripDelimiters(oldValue.slice(0, prevPos), delimiter, delimiters);
    newRawValue = this.stripDelimiters(newValue.slice(0, prevPos), delimiter, delimiters);
    lengthOffset = oldRawValue.length - newRawValue.length;
    return lengthOffset !== 0 ? lengthOffset / Math.abs(lengthOffset) : 0;
  },
  stripDelimiters: function stripDelimiters(value, delimiter, delimiters) {
    var owner = this; // single delimiter

    if (delimiters.length === 0) {
      var delimiterRE = delimiter ? owner.getDelimiterREByDelimiter(delimiter) : '';
      return value.replace(delimiterRE, '');
    } // multiple delimiters


    delimiters.forEach(function (current) {
      current.split('').forEach(function (letter) {
        value = value.replace(owner.getDelimiterREByDelimiter(letter), '');
      });
    });
    return value;
  },
  headStr: function headStr(str, length) {
    return str.slice(0, length);
  },
  getMaxLength: function getMaxLength(blocks) {
    return blocks.reduce(function (previous, current) {
      return previous + current;
    }, 0);
  },
  // strip prefix
  // Before type  |   After type    |     Return value
  // PEFIX-...    |   PEFIX-...     |     ''
  // PREFIX-123   |   PEFIX-123     |     123
  // PREFIX-123   |   PREFIX-23     |     23
  // PREFIX-123   |   PREFIX-1234   |     1234
  getPrefixStrippedValue: function getPrefixStrippedValue(value, prefix, prefixLength, prevResult, delimiter, delimiters, noImmediatePrefix, tailPrefix, signBeforePrefix) {
    // No prefix
    if (prefixLength === 0) {
      return value;
    }

    if (signBeforePrefix && value.slice(0, 1) == '-') {
      var prev = prevResult.slice(0, 1) == '-' ? prevResult.slice(1) : prevResult;
      return '-' + this.getPrefixStrippedValue(value.slice(1), prefix, prefixLength, prev, delimiter, delimiters, noImmediatePrefix, tailPrefix, signBeforePrefix);
    } // Pre result prefix string does not match pre-defined prefix


    if (prevResult.slice(0, prefixLength) !== prefix && !tailPrefix) {
      // Check if the first time user entered something
      if (noImmediatePrefix && !prevResult && value) return value;
      return '';
    } else if (prevResult.slice(-prefixLength) !== prefix && tailPrefix) {
      // Check if the first time user entered something
      if (noImmediatePrefix && !prevResult && value) return value;
      return '';
    }

    var prevValue = this.stripDelimiters(prevResult, delimiter, delimiters); // New value has issue, someone typed in between prefix letters
    // Revert to pre value

    if (value.slice(0, prefixLength) !== prefix && !tailPrefix) {
      return prevValue.slice(prefixLength);
    } else if (value.slice(-prefixLength) !== prefix && tailPrefix) {
      return prevValue.slice(0, -prefixLength - 1);
    } // No issue, strip prefix for new value


    return tailPrefix ? value.slice(0, -prefixLength) : value.slice(prefixLength);
  },
  getFirstDiffIndex: function getFirstDiffIndex(prev, current) {
    var index = 0;

    while (prev.charAt(index) === current.charAt(index)) {
      if (prev.charAt(index++) === '') {
        return -1;
      }
    }

    return index;
  },
  getFormattedValue: function getFormattedValue(value, blocks, blocksLength, delimiter, delimiters, delimiterLazyShow) {
    var result = '',
        multipleDelimiters = delimiters.length > 0,
        currentDelimiter; // no options, normal input

    if (blocksLength === 0) {
      return value;
    }

    blocks.forEach(function (length, index) {
      if (value.length > 0) {
        var sub = value.slice(0, length),
            rest = value.slice(length);

        if (multipleDelimiters) {
          currentDelimiter = delimiters[delimiterLazyShow ? index - 1 : index] || currentDelimiter;
        } else {
          currentDelimiter = delimiter;
        }

        if (delimiterLazyShow) {
          if (index > 0) {
            result += currentDelimiter;
          }

          result += sub;
        } else {
          result += sub;

          if (sub.length === length && index < blocksLength - 1) {
            result += currentDelimiter;
          }
        } // update remaining string


        value = rest;
      }
    });
    return result;
  },
  // move cursor to the end
  // the first time user focuses on an input with prefix
  fixPrefixCursor: function fixPrefixCursor(el, prefix, delimiter, delimiters) {
    if (!el) {
      return;
    }

    var val = el.value,
        appendix = delimiter || delimiters[0] || ' ';

    if (!el.setSelectionRange || !prefix || prefix.length + appendix.length <= val.length) {
      return;
    }

    var len = val.length * 2; // set timeout to avoid blink

    setTimeout(function () {
      el.setSelectionRange(len, len);
    }, 1);
  },
  // Check if input field is fully selected
  checkFullSelection: function checkFullSelection(value) {
    try {
      var selection = window.getSelection() || document.getSelection() || {};
      return selection.toString().length === value.length;
    } catch (ex) {// Ignore
    }

    return false;
  },
  setSelection: function setSelection(element, position, doc) {
    if (element !== this.getActiveElement(doc)) {
      return;
    } // cursor is already in the end


    if (element && element.value.length <= position) {
      return;
    }

    if (element.createTextRange) {
      var range = element.createTextRange();
      range.move('character', position);
      range.select();
    } else {
      try {
        element.setSelectionRange(position, position);
      } catch (e) {
        // eslint-disable-next-line
        console.warn('The input element type does not support selection');
      }
    }
  },
  getActiveElement: function getActiveElement(parent) {
    var activeElement = parent.activeElement;

    if (activeElement && activeElement.shadowRoot) {
      return this.getActiveElement(activeElement.shadowRoot);
    }

    return activeElement;
  },
  isAndroid: function isAndroid() {
    return navigator && /android/i.test(navigator.userAgent);
  },
  // On Android chrome, the keyup and keydown events
  // always return key code 229 as a composition that
  // buffers the user’s keystrokes
  // see https://github.com/nosir/cleave.js/issues/147
  isAndroidBackspaceKeydown: function isAndroidBackspaceKeydown(lastInputValue, currentInputValue) {
    if (!this.isAndroid() || !lastInputValue || !currentInputValue) {
      return false;
    }

    return currentInputValue === lastInputValue.slice(0, -1);
  }
};
var Util_1 = Util;
/**
 * Props Assignment
 *
 * Separate this, so react module can share the usage
 */

var DefaultProperties = {
  // Maybe change to object-assign
  // for now just keep it as simple
  assign: function assign(target, opts) {
    target = target || {};
    opts = opts || {}; // credit card

    target.creditCard = !!opts.creditCard;
    target.creditCardStrictMode = !!opts.creditCardStrictMode;
    target.creditCardType = '';

    target.onCreditCardTypeChanged = opts.onCreditCardTypeChanged || function () {}; // phone


    target.phone = !!opts.phone;
    target.phoneRegionCode = opts.phoneRegionCode || 'AU';
    target.phoneFormatter = {}; // time

    target.time = !!opts.time;
    target.timePattern = opts.timePattern || ['h', 'm', 's'];
    target.timeFormat = opts.timeFormat || '24';
    target.timeFormatter = {}; // date

    target.date = !!opts.date;
    target.datePattern = opts.datePattern || ['d', 'm', 'Y'];
    target.dateMin = opts.dateMin || '';
    target.dateMax = opts.dateMax || '';
    target.dateFormatter = {}; // numeral

    target.numeral = !!opts.numeral;
    target.numeralIntegerScale = opts.numeralIntegerScale > 0 ? opts.numeralIntegerScale : 0;
    target.numeralDecimalScale = opts.numeralDecimalScale >= 0 ? opts.numeralDecimalScale : 2;
    target.numeralDecimalMark = opts.numeralDecimalMark || '.';
    target.numeralThousandsGroupStyle = opts.numeralThousandsGroupStyle || 'thousand';
    target.numeralPositiveOnly = !!opts.numeralPositiveOnly;
    target.stripLeadingZeroes = opts.stripLeadingZeroes !== false;
    target.signBeforePrefix = !!opts.signBeforePrefix;
    target.tailPrefix = !!opts.tailPrefix; // others

    target.numericOnly = target.creditCard || target.date || !!opts.numericOnly;
    target.uppercase = !!opts.uppercase;
    target.lowercase = !!opts.lowercase;
    target.prefix = target.creditCard || target.date ? '' : opts.prefix || '';
    target.noImmediatePrefix = !!opts.noImmediatePrefix;
    target.prefixLength = target.prefix.length;
    target.rawValueTrimPrefix = !!opts.rawValueTrimPrefix;
    target.copyDelimiter = !!opts.copyDelimiter;
    target.initValue = opts.initValue !== undefined && opts.initValue !== null ? opts.initValue.toString() : '';
    target.delimiter = opts.delimiter || opts.delimiter === '' ? opts.delimiter : opts.date ? '/' : opts.time ? ':' : opts.numeral ? ',' : opts.phone ? ' ' : ' ';
    target.delimiterLength = target.delimiter.length;
    target.delimiterLazyShow = !!opts.delimiterLazyShow;
    target.delimiters = opts.delimiters || [];
    target.blocks = opts.blocks || [];
    target.blocksLength = target.blocks.length;
    target.root = _typeof(commonjsGlobal) === 'object' && commonjsGlobal ? commonjsGlobal : window;
    target.document = opts.document || target.root.document;
    target.maxLength = 0;
    target.backspace = false;
    target.result = '';

    target.onValueChanged = opts.onValueChanged || function () {};

    return target;
  }
};
var DefaultProperties_1 = DefaultProperties;
/**
 * Construct a new Cleave instance by passing the configuration object
 *
 * @param {String | HTMLElement} element
 * @param {Object} opts
 */

var Cleave = function Cleave(element, opts) {
  var owner = this;
  var hasMultipleElements = false;

  if (typeof element === 'string') {
    owner.element = document.querySelector(element);
    hasMultipleElements = document.querySelectorAll(element).length > 1;
  } else {
    if (typeof element.length !== 'undefined' && element.length > 0) {
      owner.element = element[0];
      hasMultipleElements = element.length > 1;
    } else {
      owner.element = element;
    }
  }

  if (!owner.element) {
    throw new Error('[cleave.js] Please check the element');
  }

  if (hasMultipleElements) {
    try {
      // eslint-disable-next-line
      console.warn('[cleave.js] Multiple input fields matched, cleave.js will only take the first one.');
    } catch (e) {// Old IE
    }
  }

  opts.initValue = owner.element.value;
  owner.properties = Cleave.DefaultProperties.assign({}, opts);
  owner.init();
};

Cleave.prototype = {
  init: function init() {
    var owner = this,
        pps = owner.properties; // no need to use this lib

    if (!pps.numeral && !pps.phone && !pps.creditCard && !pps.time && !pps.date && pps.blocksLength === 0 && !pps.prefix) {
      owner.onInput(pps.initValue);
      return;
    }

    pps.maxLength = Cleave.Util.getMaxLength(pps.blocks);
    owner.isAndroid = Cleave.Util.isAndroid();
    owner.lastInputValue = '';
    owner.onChangeListener = owner.onChange.bind(owner);
    owner.onKeyDownListener = owner.onKeyDown.bind(owner);
    owner.onFocusListener = owner.onFocus.bind(owner);
    owner.onCutListener = owner.onCut.bind(owner);
    owner.onCopyListener = owner.onCopy.bind(owner);
    owner.element.addEventListener('input', owner.onChangeListener);
    owner.element.addEventListener('keydown', owner.onKeyDownListener);
    owner.element.addEventListener('focus', owner.onFocusListener);
    owner.element.addEventListener('cut', owner.onCutListener);
    owner.element.addEventListener('copy', owner.onCopyListener);
    owner.initPhoneFormatter();
    owner.initDateFormatter();
    owner.initTimeFormatter();
    owner.initNumeralFormatter(); // avoid touch input field if value is null
    // otherwise Firefox will add red box-shadow for <input required />

    if (pps.initValue || pps.prefix && !pps.noImmediatePrefix) {
      owner.onInput(pps.initValue);
    }
  },
  initNumeralFormatter: function initNumeralFormatter() {
    var owner = this,
        pps = owner.properties;

    if (!pps.numeral) {
      return;
    }

    pps.numeralFormatter = new Cleave.NumeralFormatter(pps.numeralDecimalMark, pps.numeralIntegerScale, pps.numeralDecimalScale, pps.numeralThousandsGroupStyle, pps.numeralPositiveOnly, pps.stripLeadingZeroes, pps.prefix, pps.signBeforePrefix, pps.tailPrefix, pps.delimiter);
  },
  initTimeFormatter: function initTimeFormatter() {
    var owner = this,
        pps = owner.properties;

    if (!pps.time) {
      return;
    }

    pps.timeFormatter = new Cleave.TimeFormatter(pps.timePattern, pps.timeFormat);
    pps.blocks = pps.timeFormatter.getBlocks();
    pps.blocksLength = pps.blocks.length;
    pps.maxLength = Cleave.Util.getMaxLength(pps.blocks);
  },
  initDateFormatter: function initDateFormatter() {
    var owner = this,
        pps = owner.properties;

    if (!pps.date) {
      return;
    }

    pps.dateFormatter = new Cleave.DateFormatter(pps.datePattern, pps.dateMin, pps.dateMax);
    pps.blocks = pps.dateFormatter.getBlocks();
    pps.blocksLength = pps.blocks.length;
    pps.maxLength = Cleave.Util.getMaxLength(pps.blocks);
  },
  initPhoneFormatter: function initPhoneFormatter() {
    var owner = this,
        pps = owner.properties;

    if (!pps.phone) {
      return;
    } // Cleave.AsYouTypeFormatter should be provided by
    // external google closure lib


    try {
      pps.phoneFormatter = new Cleave.PhoneFormatter(new pps.root.Cleave.AsYouTypeFormatter(pps.phoneRegionCode), pps.delimiter);
    } catch (ex) {
      throw new Error('[cleave.js] Please include phone-type-formatter.{country}.js lib');
    }
  },
  onKeyDown: function onKeyDown(event) {
    var owner = this,
        pps = owner.properties,
        charCode = event.which || event.keyCode,
        Util = Cleave.Util,
        currentValue = owner.element.value; // if we got any charCode === 8, this means, that this device correctly
    // sends backspace keys in event, so we do not need to apply any hacks

    owner.hasBackspaceSupport = owner.hasBackspaceSupport || charCode === 8;

    if (!owner.hasBackspaceSupport && Util.isAndroidBackspaceKeydown(owner.lastInputValue, currentValue)) {
      charCode = 8;
    }

    owner.lastInputValue = currentValue; // hit backspace when last character is delimiter

    var postDelimiter = Util.getPostDelimiter(currentValue, pps.delimiter, pps.delimiters);

    if (charCode === 8 && postDelimiter) {
      pps.postDelimiterBackspace = postDelimiter;
    } else {
      pps.postDelimiterBackspace = false;
    }
  },
  onChange: function onChange() {
    this.onInput(this.element.value);
  },
  onFocus: function onFocus() {
    var owner = this,
        pps = owner.properties;
    Cleave.Util.fixPrefixCursor(owner.element, pps.prefix, pps.delimiter, pps.delimiters);
  },
  onCut: function onCut(e) {
    if (!Cleave.Util.checkFullSelection(this.element.value)) return;
    this.copyClipboardData(e);
    this.onInput('');
  },
  onCopy: function onCopy(e) {
    if (!Cleave.Util.checkFullSelection(this.element.value)) return;
    this.copyClipboardData(e);
  },
  copyClipboardData: function copyClipboardData(e) {
    var owner = this,
        pps = owner.properties,
        Util = Cleave.Util,
        inputValue = owner.element.value,
        textToCopy = '';

    if (!pps.copyDelimiter) {
      textToCopy = Util.stripDelimiters(inputValue, pps.delimiter, pps.delimiters);
    } else {
      textToCopy = inputValue;
    }

    try {
      if (e.clipboardData) {
        e.clipboardData.setData('Text', textToCopy);
      } else {
        window.clipboardData.setData('Text', textToCopy);
      }

      e.preventDefault();
    } catch (ex) {//  empty
    }
  },
  onInput: function onInput(value) {
    var owner = this,
        pps = owner.properties,
        Util = Cleave.Util; // case 1: delete one more character "4"
    // 1234*| -> hit backspace -> 123|
    // case 2: last character is not delimiter which is:
    // 12|34* -> hit backspace -> 1|34*
    // note: no need to apply this for numeral mode

    var postDelimiterAfter = Util.getPostDelimiter(value, pps.delimiter, pps.delimiters);

    if (!pps.numeral && pps.postDelimiterBackspace && !postDelimiterAfter) {
      value = Util.headStr(value, value.length - pps.postDelimiterBackspace.length);
    } // phone formatter


    if (pps.phone) {
      if (pps.prefix && (!pps.noImmediatePrefix || value.length)) {
        pps.result = pps.prefix + pps.phoneFormatter.format(value).slice(pps.prefix.length);
      } else {
        pps.result = pps.phoneFormatter.format(value);
      }

      owner.updateValueState();
      return;
    } // numeral formatter


    if (pps.numeral) {
      // Do not show prefix when noImmediatePrefix is specified
      // This mostly because we need to show user the native input placeholder
      if (pps.prefix && pps.noImmediatePrefix && value.length === 0) {
        pps.result = '';
      } else {
        pps.result = pps.numeralFormatter.format(value);
      }

      owner.updateValueState();
      return;
    } // date


    if (pps.date) {
      value = pps.dateFormatter.getValidatedDate(value);
    } // time


    if (pps.time) {
      value = pps.timeFormatter.getValidatedTime(value);
    } // strip delimiters


    value = Util.stripDelimiters(value, pps.delimiter, pps.delimiters); // strip prefix

    value = Util.getPrefixStrippedValue(value, pps.prefix, pps.prefixLength, pps.result, pps.delimiter, pps.delimiters, pps.noImmediatePrefix, pps.tailPrefix, pps.signBeforePrefix); // strip non-numeric characters

    value = pps.numericOnly ? Util.strip(value, /[^\d]/g) : value; // convert case

    value = pps.uppercase ? value.toUpperCase() : value;
    value = pps.lowercase ? value.toLowerCase() : value; // prevent from showing prefix when no immediate option enabled with empty input value

    if (pps.prefix && (!pps.noImmediatePrefix || value.length)) {
      if (pps.tailPrefix) {
        value = value + pps.prefix;
      } else {
        value = pps.prefix + value;
      } // no blocks specified, no need to do formatting


      if (pps.blocksLength === 0) {
        pps.result = value;
        owner.updateValueState();
        return;
      }
    } // update credit card props


    if (pps.creditCard) {
      owner.updateCreditCardPropsByValue(value);
    } // strip over length characters


    value = Util.headStr(value, pps.maxLength); // apply blocks

    pps.result = Util.getFormattedValue(value, pps.blocks, pps.blocksLength, pps.delimiter, pps.delimiters, pps.delimiterLazyShow);
    owner.updateValueState();
  },
  updateCreditCardPropsByValue: function updateCreditCardPropsByValue(value) {
    var owner = this,
        pps = owner.properties,
        Util = Cleave.Util,
        creditCardInfo; // At least one of the first 4 characters has changed

    if (Util.headStr(pps.result, 4) === Util.headStr(value, 4)) {
      return;
    }

    creditCardInfo = Cleave.CreditCardDetector.getInfo(value, pps.creditCardStrictMode);
    pps.blocks = creditCardInfo.blocks;
    pps.blocksLength = pps.blocks.length;
    pps.maxLength = Util.getMaxLength(pps.blocks); // credit card type changed

    if (pps.creditCardType !== creditCardInfo.type) {
      pps.creditCardType = creditCardInfo.type;
      pps.onCreditCardTypeChanged.call(owner, pps.creditCardType);
    }
  },
  updateValueState: function updateValueState() {
    var owner = this,
        Util = Cleave.Util,
        pps = owner.properties;

    if (!owner.element) {
      return;
    }

    var endPos = owner.element.selectionEnd;
    var oldValue = owner.element.value;
    var newValue = pps.result;
    endPos = Util.getNextCursorPosition(endPos, oldValue, newValue, pps.delimiter, pps.delimiters); // fix Android browser type="text" input field
    // cursor not jumping issue

    if (owner.isAndroid) {
      window.setTimeout(function () {
        owner.element.value = newValue;
        Util.setSelection(owner.element, endPos, pps.document, false);
        owner.callOnValueChanged();
      }, 1);
      return;
    }

    owner.element.value = newValue;
    Util.setSelection(owner.element, endPos, pps.document, false);
    owner.callOnValueChanged();
  },
  callOnValueChanged: function callOnValueChanged() {
    var owner = this,
        pps = owner.properties;
    pps.onValueChanged.call(owner, {
      target: {
        name: owner.element.name,
        value: pps.result,
        rawValue: owner.getRawValue()
      }
    });
  },
  setPhoneRegionCode: function setPhoneRegionCode(phoneRegionCode) {
    var owner = this,
        pps = owner.properties;
    pps.phoneRegionCode = phoneRegionCode;
    owner.initPhoneFormatter();
    owner.onChange();
  },
  setRawValue: function setRawValue(value) {
    var owner = this,
        pps = owner.properties;
    value = value !== undefined && value !== null ? value.toString() : '';

    if (pps.numeral) {
      value = value.replace('.', pps.numeralDecimalMark);
    }

    pps.postDelimiterBackspace = false;
    owner.element.value = value;
    owner.onInput(value);
  },
  getRawValue: function getRawValue() {
    var owner = this,
        pps = owner.properties,
        Util = Cleave.Util,
        rawValue = owner.element.value;

    if (pps.rawValueTrimPrefix) {
      rawValue = Util.getPrefixStrippedValue(rawValue, pps.prefix, pps.prefixLength, pps.result, pps.delimiter, pps.delimiters, pps.noImmediatePrefix, pps.tailPrefix, pps.signBeforePrefix);
    }

    if (pps.numeral) {
      rawValue = pps.numeralFormatter.getRawValue(rawValue);
    } else {
      rawValue = Util.stripDelimiters(rawValue, pps.delimiter, pps.delimiters);
    }

    return rawValue;
  },
  getISOFormatDate: function getISOFormatDate() {
    var owner = this,
        pps = owner.properties;
    return pps.date ? pps.dateFormatter.getISOFormatDate() : '';
  },
  getISOFormatTime: function getISOFormatTime() {
    var owner = this,
        pps = owner.properties;
    return pps.time ? pps.timeFormatter.getISOFormatTime() : '';
  },
  getFormattedValue: function getFormattedValue() {
    return this.element.value;
  },
  destroy: function destroy() {
    var owner = this;
    owner.element.removeEventListener('input', owner.onChangeListener);
    owner.element.removeEventListener('keydown', owner.onKeyDownListener);
    owner.element.removeEventListener('focus', owner.onFocusListener);
    owner.element.removeEventListener('cut', owner.onCutListener);
    owner.element.removeEventListener('copy', owner.onCopyListener);
  },
  toString: function toString() {
    return '[Cleave Object]';
  }
};
Cleave.NumeralFormatter = NumeralFormatter_1;
Cleave.DateFormatter = DateFormatter_1;
Cleave.TimeFormatter = TimeFormatter_1;
Cleave.PhoneFormatter = PhoneFormatter_1;
Cleave.CreditCardDetector = CreditCardDetector_1;
Cleave.Util = Util_1;
Cleave.DefaultProperties = DefaultProperties_1; // for angular directive

(_typeof(commonjsGlobal) === 'object' && commonjsGlobal ? commonjsGlobal : window)['Cleave'] = Cleave; // CommonJS

var Cleave_1 = Cleave;
/* harmony default export */ __webpack_exports__["default"] = (Cleave_1);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/inputmask/dist/inputmask.js":
/*!**************************************************!*\
  !*** ./node_modules/inputmask/dist/inputmask.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;function _typeof2(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof2 = function _typeof2(obj) { return typeof obj; }; } else { _typeof2 = function _typeof2(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof2(obj); }

/*!
 * dist/inputmask
 * https://github.com/RobinHerbots/Inputmask
 * Copyright (c) 2010 - 2020 Robin Herbots
 * Licensed under the MIT license
 * Version: 5.0.3
 */
!function webpackUniversalModuleDefinition(root, factory) {
  if ("object" == ( false ? undefined : _typeof2(exports)) && "object" == ( false ? undefined : _typeof2(module))) module.exports = factory();else if (true) !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));else { var i, a; }
}(window, function () {
  return modules = [function (module) {
    module.exports = JSON.parse('{"BACKSPACE":8,"BACKSPACE_SAFARI":127,"DELETE":46,"DOWN":40,"END":35,"ENTER":13,"ESCAPE":27,"HOME":36,"INSERT":45,"LEFT":37,"PAGE_DOWN":34,"PAGE_UP":33,"RIGHT":39,"SPACE":32,"TAB":9,"UP":38,"X":88,"CONTROL":17}');
  }, function (module, exports, __webpack_require__) {
    "use strict";

    function _typeof(obj) {
      return _typeof = "function" == typeof Symbol && "symbol" == _typeof2(Symbol.iterator) ? function _typeof(obj) {
        return _typeof2(obj);
      } : function _typeof(obj) {
        return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof2(obj);
      }, _typeof(obj);
    }

    var $ = __webpack_require__(2),
        window = __webpack_require__(3),
        document = window.document,
        generateMaskSet = __webpack_require__(4).generateMaskSet,
        analyseMask = __webpack_require__(4).analyseMask,
        maskScope = __webpack_require__(7);

    function Inputmask(alias, options, internal) {
      if (!(this instanceof Inputmask)) return new Inputmask(alias, options, internal);
      this.el = void 0, this.events = {}, this.maskset = void 0, this.refreshValue = !1, !0 !== internal && ($.isPlainObject(alias) ? options = alias : (options = options || {}, alias && (options.alias = alias)), this.opts = $.extend(!0, {}, this.defaults, options), this.noMasksCache = options && void 0 !== options.definitions, this.userOptions = options || {}, resolveAlias(this.opts.alias, options, this.opts), this.isRTL = this.opts.numericInput);
    }

    function resolveAlias(aliasStr, options, opts) {
      var aliasDefinition = Inputmask.prototype.aliases[aliasStr];
      return aliasDefinition ? (aliasDefinition.alias && resolveAlias(aliasDefinition.alias, void 0, opts), $.extend(!0, opts, aliasDefinition), $.extend(!0, opts, options), !0) : (null === opts.mask && (opts.mask = aliasStr), !1);
    }

    function importAttributeOptions(npt, opts, userOptions, dataAttribute) {
      function importOption(option, optionData) {
        optionData = void 0 !== optionData ? optionData : npt.getAttribute(dataAttribute + "-" + option), null !== optionData && ("string" == typeof optionData && (0 === option.indexOf("on") ? optionData = window[optionData] : "false" === optionData ? optionData = !1 : "true" === optionData && (optionData = !0)), userOptions[option] = optionData);
      }

      if (!0 === opts.importDataAttributes) {
        var attrOptions = npt.getAttribute(dataAttribute),
            option,
            dataoptions,
            optionData,
            p;
        if (attrOptions && "" !== attrOptions && (attrOptions = attrOptions.replace(/'/g, '"'), dataoptions = JSON.parse("{" + attrOptions + "}")), dataoptions) for (p in optionData = void 0, dataoptions) {
          if ("alias" === p.toLowerCase()) {
            optionData = dataoptions[p];
            break;
          }
        }

        for (option in importOption("alias", optionData), userOptions.alias && resolveAlias(userOptions.alias, userOptions, opts), opts) {
          if (dataoptions) for (p in optionData = void 0, dataoptions) {
            if (p.toLowerCase() === option.toLowerCase()) {
              optionData = dataoptions[p];
              break;
            }
          }
          importOption(option, optionData);
        }
      }

      return $.extend(!0, opts, userOptions), "rtl" !== npt.dir && !opts.rightAlign || (npt.style.textAlign = "right"), "rtl" !== npt.dir && !opts.numericInput || (npt.dir = "ltr", npt.removeAttribute("dir"), opts.isRTL = !0), Object.keys(userOptions).length;
    }

    Inputmask.prototype = {
      dataAttribute: "data-inputmask",
      defaults: {
        _maxTestPos: 500,
        placeholder: "_",
        optionalmarker: ["[", "]"],
        quantifiermarker: ["{", "}"],
        groupmarker: ["(", ")"],
        alternatormarker: "|",
        escapeChar: "\\",
        mask: null,
        regex: null,
        oncomplete: $.noop,
        onincomplete: $.noop,
        oncleared: $.noop,
        repeat: 0,
        greedy: !1,
        autoUnmask: !1,
        removeMaskOnSubmit: !1,
        clearMaskOnLostFocus: !0,
        insertMode: !0,
        insertModeVisual: !0,
        clearIncomplete: !1,
        alias: null,
        onKeyDown: $.noop,
        onBeforeMask: null,
        onBeforePaste: function onBeforePaste(pastedValue, opts) {
          return $.isFunction(opts.onBeforeMask) ? opts.onBeforeMask.call(this, pastedValue, opts) : pastedValue;
        },
        onBeforeWrite: null,
        onUnMask: null,
        showMaskOnFocus: !0,
        showMaskOnHover: !0,
        onKeyValidation: $.noop,
        skipOptionalPartCharacter: " ",
        numericInput: !1,
        rightAlign: !1,
        undoOnEscape: !0,
        radixPoint: "",
        _radixDance: !1,
        groupSeparator: "",
        keepStatic: null,
        positionCaretOnTab: !0,
        tabThrough: !1,
        supportsInputType: ["text", "tel", "url", "password", "search"],
        ignorables: [8, 9, 19, 27, 33, 34, 35, 36, 37, 38, 39, 40, 45, 46, 93, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 0, 229],
        isComplete: null,
        preValidation: null,
        postValidation: null,
        staticDefinitionSymbol: void 0,
        jitMasking: !1,
        nullable: !0,
        inputEventOnly: !1,
        noValuePatching: !1,
        positionCaretOnClick: "lvp",
        casing: null,
        inputmode: "text",
        importDataAttributes: !0,
        shiftPositions: !0
      },
      definitions: {
        9: {
          validator: "[0-9\uFF11-\uFF19]",
          definitionSymbol: "*"
        },
        a: {
          validator: "[A-Za-z\u0410-\u044F\u0401\u0451\xC0-\xFF\xB5]",
          definitionSymbol: "*"
        },
        "*": {
          validator: "[0-9\uFF11-\uFF19A-Za-z\u0410-\u044F\u0401\u0451\xC0-\xFF\xB5]"
        }
      },
      aliases: {},
      masksCache: {},
      mask: function mask(elems) {
        var that = this;
        return "string" == typeof elems && (elems = document.getElementById(elems) || document.querySelectorAll(elems)), elems = elems.nodeName ? [elems] : elems, $.each(elems, function (ndx, el) {
          var scopedOpts = $.extend(!0, {}, that.opts);

          if (importAttributeOptions(el, scopedOpts, $.extend(!0, {}, that.userOptions), that.dataAttribute)) {
            var maskset = generateMaskSet(scopedOpts, that.noMasksCache);
            void 0 !== maskset && (void 0 !== el.inputmask && (el.inputmask.opts.autoUnmask = !0, el.inputmask.remove()), el.inputmask = new Inputmask(void 0, void 0, !0), el.inputmask.opts = scopedOpts, el.inputmask.noMasksCache = that.noMasksCache, el.inputmask.userOptions = $.extend(!0, {}, that.userOptions), el.inputmask.isRTL = scopedOpts.isRTL || scopedOpts.numericInput, el.inputmask.el = el, el.inputmask.maskset = maskset, $.data(el, "_inputmask_opts", scopedOpts), maskScope.call(el.inputmask, {
              action: "mask"
            }));
          }
        }), elems && elems[0] && elems[0].inputmask || this;
      },
      option: function option(options, noremask) {
        return "string" == typeof options ? this.opts[options] : "object" === _typeof(options) ? ($.extend(this.userOptions, options), this.el && !0 !== noremask && this.mask(this.el), this) : void 0;
      },
      unmaskedvalue: function unmaskedvalue(value) {
        return this.maskset = this.maskset || generateMaskSet(this.opts, this.noMasksCache), maskScope.call(this, {
          action: "unmaskedvalue",
          value: value
        });
      },
      remove: function remove() {
        return maskScope.call(this, {
          action: "remove"
        });
      },
      getemptymask: function getemptymask() {
        return this.maskset = this.maskset || generateMaskSet(this.opts, this.noMasksCache), maskScope.call(this, {
          action: "getemptymask"
        });
      },
      hasMaskedValue: function hasMaskedValue() {
        return !this.opts.autoUnmask;
      },
      isComplete: function isComplete() {
        return this.maskset = this.maskset || generateMaskSet(this.opts, this.noMasksCache), maskScope.call(this, {
          action: "isComplete"
        });
      },
      getmetadata: function getmetadata() {
        return this.maskset = this.maskset || generateMaskSet(this.opts, this.noMasksCache), maskScope.call(this, {
          action: "getmetadata"
        });
      },
      isValid: function isValid(value) {
        return this.maskset = this.maskset || generateMaskSet(this.opts, this.noMasksCache), maskScope.call(this, {
          action: "isValid",
          value: value
        });
      },
      format: function format(value, metadata) {
        return this.maskset = this.maskset || generateMaskSet(this.opts, this.noMasksCache), maskScope.call(this, {
          action: "format",
          value: value,
          metadata: metadata
        });
      },
      setValue: function setValue(value) {
        this.el && $(this.el).trigger("setvalue", [value]);
      },
      analyseMask: analyseMask
    }, Inputmask.extendDefaults = function (options) {
      $.extend(!0, Inputmask.prototype.defaults, options);
    }, Inputmask.extendDefinitions = function (definition) {
      $.extend(!0, Inputmask.prototype.definitions, definition);
    }, Inputmask.extendAliases = function (alias) {
      $.extend(!0, Inputmask.prototype.aliases, alias);
    }, Inputmask.format = function (value, options, metadata) {
      return Inputmask(options).format(value, metadata);
    }, Inputmask.unmask = function (value, options) {
      return Inputmask(options).unmaskedvalue(value);
    }, Inputmask.isValid = function (value, options) {
      return Inputmask(options).isValid(value);
    }, Inputmask.remove = function (elems) {
      "string" == typeof elems && (elems = document.getElementById(elems) || document.querySelectorAll(elems)), elems = elems.nodeName ? [elems] : elems, $.each(elems, function (ndx, el) {
        el.inputmask && el.inputmask.remove();
      });
    }, Inputmask.setValue = function (elems, value) {
      "string" == typeof elems && (elems = document.getElementById(elems) || document.querySelectorAll(elems)), elems = elems.nodeName ? [elems] : elems, $.each(elems, function (ndx, el) {
        el.inputmask ? el.inputmask.setValue(value) : $(el).trigger("setvalue", [value]);
      });
    };
    var escapeRegexRegex = new RegExp("(\\" + ["/", ".", "*", "+", "?", "|", "(", ")", "[", "]", "{", "}", "\\", "$", "^"].join("|\\") + ")", "gim");
    Inputmask.escapeRegex = function (str) {
      return str.replace(escapeRegexRegex, "\\$1");
    }, Inputmask.dependencyLib = $, window.Inputmask = Inputmask, module.exports = Inputmask;
  }, function (module, exports, __webpack_require__) {
    "use strict";

    function _typeof(obj) {
      return _typeof = "function" == typeof Symbol && "symbol" == _typeof2(Symbol.iterator) ? function _typeof(obj) {
        return _typeof2(obj);
      } : function _typeof(obj) {
        return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof2(obj);
      }, _typeof(obj);
    }

    var window = __webpack_require__(3),
        document = window.document;

    function indexOf(list, elem) {
      for (var i = 0, len = list.length; i < len; i++) {
        if (list[i] === elem) return i;
      }

      return -1;
    }

    function isWindow(obj) {
      return null != obj && obj === obj.window;
    }

    function isArraylike(obj) {
      var length = "length" in obj && obj.length,
          ltype = _typeof(obj);

      return "function" !== ltype && !isWindow(obj) && (!(1 !== obj.nodeType || !length) || "array" === ltype || 0 === length || "number" == typeof length && 0 < length && length - 1 in obj);
    }

    function isValidElement(elem) {
      return elem instanceof Element;
    }

    function DependencyLib(elem) {
      return elem instanceof DependencyLib ? elem : this instanceof DependencyLib ? void (null != elem && elem !== window && (this[0] = elem.nodeName ? elem : void 0 !== elem[0] && elem[0].nodeName ? elem[0] : document.querySelector(elem), void 0 !== this[0] && null !== this[0] && (this[0].eventRegistry = this[0].eventRegistry || {}))) : new DependencyLib(elem);
    }

    DependencyLib.prototype = {
      on: function on(events, handler) {
        function addEvent(ev, namespace) {
          elem.addEventListener ? elem.addEventListener(ev, handler, !1) : elem.attachEvent && elem.attachEvent("on" + ev, handler), eventRegistry[ev] = eventRegistry[ev] || {}, eventRegistry[ev][namespace] = eventRegistry[ev][namespace] || [], eventRegistry[ev][namespace].push(handler);
        }

        if (isValidElement(this[0])) for (var eventRegistry = this[0].eventRegistry, elem = this[0], _events = events.split(" "), endx = 0; endx < _events.length; endx++) {
          var nsEvent = _events[endx].split("."),
              ev = nsEvent[0],
              namespace = nsEvent[1] || "global";

          addEvent(ev, namespace);
        }
        return this;
      },
      off: function off(events, handler) {
        var eventRegistry, elem;

        function removeEvent(ev, namespace, handler) {
          if (ev in eventRegistry == !0) if (elem.removeEventListener ? elem.removeEventListener(ev, handler, !1) : elem.detachEvent && elem.detachEvent("on" + ev, handler), "global" === namespace) for (var nmsp in eventRegistry[ev]) {
            eventRegistry[ev][nmsp].splice(eventRegistry[ev][nmsp].indexOf(handler), 1);
          } else eventRegistry[ev][namespace].splice(eventRegistry[ev][namespace].indexOf(handler), 1);
        }

        function resolveNamespace(ev, namespace) {
          var evts = [],
              hndx,
              hndL;
          if (0 < ev.length) {
            if (void 0 === handler) for (hndx = 0, hndL = eventRegistry[ev][namespace].length; hndx < hndL; hndx++) {
              evts.push({
                ev: ev,
                namespace: namespace && 0 < namespace.length ? namespace : "global",
                handler: eventRegistry[ev][namespace][hndx]
              });
            } else evts.push({
              ev: ev,
              namespace: namespace && 0 < namespace.length ? namespace : "global",
              handler: handler
            });
          } else if (0 < namespace.length) for (var evNdx in eventRegistry) {
            for (var nmsp in eventRegistry[evNdx]) {
              if (nmsp === namespace) if (void 0 === handler) for (hndx = 0, hndL = eventRegistry[evNdx][nmsp].length; hndx < hndL; hndx++) {
                evts.push({
                  ev: evNdx,
                  namespace: nmsp,
                  handler: eventRegistry[evNdx][nmsp][hndx]
                });
              } else evts.push({
                ev: evNdx,
                namespace: nmsp,
                handler: handler
              });
            }
          }
          return evts;
        }

        if (isValidElement(this[0])) {
          eventRegistry = this[0].eventRegistry, elem = this[0];

          for (var _events = events.split(" "), endx = 0; endx < _events.length; endx++) {
            for (var nsEvent = _events[endx].split("."), offEvents = resolveNamespace(nsEvent[0], nsEvent[1]), i = 0, offEventsL = offEvents.length; i < offEventsL; i++) {
              removeEvent(offEvents[i].ev, offEvents[i].namespace, offEvents[i].handler);
            }
          }
        }

        return this;
      },
      trigger: function trigger(events, argument_1) {
        if (isValidElement(this[0])) for (var eventRegistry = this[0].eventRegistry, elem = this[0], _events = "string" == typeof events ? events.split(" ") : [events.type], endx = 0; endx < _events.length; endx++) {
          var nsEvent = _events[endx].split("."),
              ev = nsEvent[0],
              namespace = nsEvent[1] || "global";

          if (void 0 !== document && "global" === namespace) {
            var evnt,
                i,
                params = {
              bubbles: !0,
              cancelable: !0,
              detail: argument_1
            };

            if (document.createEvent) {
              try {
                evnt = new CustomEvent(ev, params);
              } catch (e) {
                evnt = document.createEvent("CustomEvent"), evnt.initCustomEvent(ev, params.bubbles, params.cancelable, params.detail);
              }

              events.type && DependencyLib.extend(evnt, events), elem.dispatchEvent(evnt);
            } else evnt = document.createEventObject(), evnt.eventType = ev, evnt.detail = argument_1, events.type && DependencyLib.extend(evnt, events), elem.fireEvent("on" + evnt.eventType, evnt);
          } else if (void 0 !== eventRegistry[ev]) if (events = events.type ? events : DependencyLib.Event(events), events.detail = arguments.slice(1), "global" === namespace) for (var nmsp in eventRegistry[ev]) {
            for (i = 0; i < eventRegistry[ev][nmsp].length; i++) {
              eventRegistry[ev][nmsp][i].apply(elem, arguments);
            }
          } else for (i = 0; i < eventRegistry[ev][namespace].length; i++) {
            eventRegistry[ev][namespace][i].apply(elem, arguments);
          }
        }
        return this;
      }
    }, DependencyLib.isFunction = function (obj) {
      return "function" == typeof obj;
    }, DependencyLib.noop = function () {}, DependencyLib.isArray = Array.isArray, DependencyLib.inArray = function (elem, arr, i) {
      return null == arr ? -1 : indexOf(arr, elem, i);
    }, DependencyLib.valHooks = void 0, DependencyLib.isPlainObject = function (obj) {
      return "object" === _typeof(obj) && !obj.nodeType && !isWindow(obj) && !(obj.constructor && !Object.hasOwnProperty.call(obj.constructor.prototype, "isPrototypeOf"));
    }, DependencyLib.extend = function () {
      var options,
          name,
          src,
          copy,
          copyIsArray,
          clone,
          target = arguments[0] || {},
          i = 1,
          length = arguments.length,
          deep = !1;

      for ("boolean" == typeof target && (deep = target, target = arguments[i] || {}, i++), "object" === _typeof(target) || DependencyLib.isFunction(target) || (target = {}), i === length && (target = this, i--); i < length; i++) {
        if (null != (options = arguments[i])) for (name in options) {
          src = target[name], copy = options[name], target !== copy && (deep && copy && (DependencyLib.isPlainObject(copy) || (copyIsArray = DependencyLib.isArray(copy))) ? (clone = copyIsArray ? (copyIsArray = !1, src && DependencyLib.isArray(src) ? src : []) : src && DependencyLib.isPlainObject(src) ? src : {}, target[name] = DependencyLib.extend(deep, clone, copy)) : void 0 !== copy && (target[name] = copy));
        }
      }

      return target;
    }, DependencyLib.each = function (obj, callback) {
      var value,
          i = 0;
      if (isArraylike(obj)) for (var length = obj.length; i < length && (value = callback.call(obj[i], i, obj[i]), !1 !== value); i++) {
        ;
      } else for (i in obj) {
        if (value = callback.call(obj[i], i, obj[i]), !1 === value) break;
      }
      return obj;
    }, DependencyLib.data = function (owner, key, value) {
      if (void 0 === value) return owner.__data ? owner.__data[key] : null;
      owner.__data = owner.__data || {}, owner.__data[key] = value;
    }, "function" == typeof window.CustomEvent ? DependencyLib.Event = window.CustomEvent : (DependencyLib.Event = function (event, params) {
      params = params || {
        bubbles: !1,
        cancelable: !1,
        detail: void 0
      };
      var evt = document.createEvent("CustomEvent");
      return evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail), evt;
    }, DependencyLib.Event.prototype = window.Event.prototype), module.exports = DependencyLib;
  }, function (module, exports, __webpack_require__) {
    "use strict";

    var __WEBPACK_AMD_DEFINE_RESULT__;

    function _typeof(obj) {
      return _typeof = "function" == typeof Symbol && "symbol" == _typeof2(Symbol.iterator) ? function _typeof(obj) {
        return _typeof2(obj);
      } : function _typeof(obj) {
        return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof2(obj);
      }, _typeof(obj);
    }

    __WEBPACK_AMD_DEFINE_RESULT__ = function () {
      return "undefined" != typeof window ? window : new (eval("require('jsdom').JSDOM"))("").window;
    }.call(exports, __webpack_require__, exports, module), void 0 === __WEBPACK_AMD_DEFINE_RESULT__ || (module.exports = __WEBPACK_AMD_DEFINE_RESULT__);
  }, function (module, exports, __webpack_require__) {
    "use strict";

    var $ = __webpack_require__(2);

    function generateMaskSet(opts, nocache) {
      var ms;

      function generateMask(mask, metadata, opts) {
        var regexMask = !1,
            masksetDefinition,
            maskdefKey;

        if (null !== mask && "" !== mask || (regexMask = null !== opts.regex, mask = regexMask ? (mask = opts.regex, mask.replace(/^(\^)(.*)(\$)$/, "$2")) : (regexMask = !0, ".*")), 1 === mask.length && !1 === opts.greedy && 0 !== opts.repeat && (opts.placeholder = ""), 0 < opts.repeat || "*" === opts.repeat || "+" === opts.repeat) {
          var repeatStart = "*" === opts.repeat ? 0 : "+" === opts.repeat ? 1 : opts.repeat;
          mask = opts.groupmarker[0] + mask + opts.groupmarker[1] + opts.quantifiermarker[0] + repeatStart + "," + opts.repeat + opts.quantifiermarker[1];
        }

        return maskdefKey = regexMask ? "regex_" + opts.regex : opts.numericInput ? mask.split("").reverse().join("") : mask, !1 !== opts.keepStatic && (maskdefKey = "ks_" + maskdefKey), void 0 === Inputmask.prototype.masksCache[maskdefKey] || !0 === nocache ? (masksetDefinition = {
          mask: mask,
          maskToken: Inputmask.prototype.analyseMask(mask, regexMask, opts),
          validPositions: {},
          _buffer: void 0,
          buffer: void 0,
          tests: {},
          excludes: {},
          metadata: metadata,
          maskLength: void 0,
          jitOffset: {}
        }, !0 !== nocache && (Inputmask.prototype.masksCache[maskdefKey] = masksetDefinition, masksetDefinition = $.extend(!0, {}, Inputmask.prototype.masksCache[maskdefKey]))) : masksetDefinition = $.extend(!0, {}, Inputmask.prototype.masksCache[maskdefKey]), masksetDefinition;
      }

      if ($.isFunction(opts.mask) && (opts.mask = opts.mask(opts)), $.isArray(opts.mask)) {
        if (1 < opts.mask.length) {
          null === opts.keepStatic && (opts.keepStatic = !0);
          var altMask = opts.groupmarker[0];
          return $.each(opts.isRTL ? opts.mask.reverse() : opts.mask, function (ndx, msk) {
            1 < altMask.length && (altMask += opts.groupmarker[1] + opts.alternatormarker + opts.groupmarker[0]), void 0 === msk.mask || $.isFunction(msk.mask) ? altMask += msk : altMask += msk.mask;
          }), altMask += opts.groupmarker[1], generateMask(altMask, opts.mask, opts);
        }

        opts.mask = opts.mask.pop();
      }

      return null === opts.keepStatic && (opts.keepStatic = !1), ms = opts.mask && void 0 !== opts.mask.mask && !$.isFunction(opts.mask.mask) ? generateMask(opts.mask.mask, opts.mask, opts) : generateMask(opts.mask, opts.mask, opts), ms;
    }

    function analyseMask(mask, regexMask, opts) {
      var tokenizer = /(?:[?*+]|\{[0-9+*]+(?:,[0-9+*]*)?(?:\|[0-9+*]*)?\})|[^.?*+^${[]()|\\]+|./g,
          regexTokenizer = /\[\^?]?(?:[^\\\]]+|\\[\S\s]?)*]?|\\(?:0(?:[0-3][0-7]{0,2}|[4-7][0-7]?)?|[1-9][0-9]*|x[0-9A-Fa-f]{2}|u[0-9A-Fa-f]{4}|c[A-Za-z]|[\S\s]?)|\((?:\?[:=!]?)?|(?:[?*+]|\{[0-9]+(?:,[0-9]*)?\})\??|[^.?*+^${[()|\\]+|./g,
          escaped = !1,
          currentToken = new MaskToken(),
          match,
          m,
          openenings = [],
          maskTokens = [],
          openingToken,
          currentOpeningToken,
          alternator,
          lastMatch,
          closeRegexGroup = !1;

      function MaskToken(isGroup, isOptional, isQuantifier, isAlternator) {
        this.matches = [], this.openGroup = isGroup || !1, this.alternatorGroup = !1, this.isGroup = isGroup || !1, this.isOptional = isOptional || !1, this.isQuantifier = isQuantifier || !1, this.isAlternator = isAlternator || !1, this.quantifier = {
          min: 1,
          max: 1
        };
      }

      function insertTestDefinition(mtoken, element, position) {
        position = void 0 !== position ? position : mtoken.matches.length;
        var prevMatch = mtoken.matches[position - 1];
        if (regexMask) 0 === element.indexOf("[") || escaped && /\\d|\\s|\\w]/i.test(element) || "." === element ? mtoken.matches.splice(position++, 0, {
          fn: new RegExp(element, opts.casing ? "i" : ""),
          "static": !1,
          optionality: !1,
          newBlockMarker: void 0 === prevMatch ? "master" : prevMatch.def !== element,
          casing: null,
          def: element,
          placeholder: void 0,
          nativeDef: element
        }) : (escaped && (element = element[element.length - 1]), $.each(element.split(""), function (ndx, lmnt) {
          prevMatch = mtoken.matches[position - 1], mtoken.matches.splice(position++, 0, {
            fn: /[a-z]/i.test(opts.staticDefinitionSymbol || lmnt) ? new RegExp("[" + (opts.staticDefinitionSymbol || lmnt) + "]", opts.casing ? "i" : "") : null,
            "static": !0,
            optionality: !1,
            newBlockMarker: void 0 === prevMatch ? "master" : prevMatch.def !== lmnt && !0 !== prevMatch["static"],
            casing: null,
            def: opts.staticDefinitionSymbol || lmnt,
            placeholder: void 0 !== opts.staticDefinitionSymbol ? lmnt : void 0,
            nativeDef: (escaped ? "'" : "") + lmnt
          });
        })), escaped = !1;else {
          var maskdef = (opts.definitions ? opts.definitions[element] : void 0) || Inputmask.prototype.definitions[element];
          maskdef && !escaped ? mtoken.matches.splice(position++, 0, {
            fn: maskdef.validator ? "string" == typeof maskdef.validator ? new RegExp(maskdef.validator, opts.casing ? "i" : "") : new function () {
              this.test = maskdef.validator;
            }() : new RegExp("."),
            "static": maskdef["static"] || !1,
            optionality: !1,
            newBlockMarker: void 0 === prevMatch ? "master" : prevMatch.def !== (maskdef.definitionSymbol || element),
            casing: maskdef.casing,
            def: maskdef.definitionSymbol || element,
            placeholder: maskdef.placeholder,
            nativeDef: element,
            generated: maskdef.generated
          }) : (mtoken.matches.splice(position++, 0, {
            fn: /[a-z]/i.test(opts.staticDefinitionSymbol || element) ? new RegExp("[" + (opts.staticDefinitionSymbol || element) + "]", opts.casing ? "i" : "") : null,
            "static": !0,
            optionality: !1,
            newBlockMarker: void 0 === prevMatch ? "master" : prevMatch.def !== element && !0 !== prevMatch["static"],
            casing: null,
            def: opts.staticDefinitionSymbol || element,
            placeholder: void 0 !== opts.staticDefinitionSymbol ? element : void 0,
            nativeDef: (escaped ? "'" : "") + element
          }), escaped = !1);
        }
      }

      function verifyGroupMarker(maskToken) {
        maskToken && maskToken.matches && $.each(maskToken.matches, function (ndx, token) {
          var nextToken = maskToken.matches[ndx + 1];
          (void 0 === nextToken || void 0 === nextToken.matches || !1 === nextToken.isQuantifier) && token && token.isGroup && (token.isGroup = !1, regexMask || (insertTestDefinition(token, opts.groupmarker[0], 0), !0 !== token.openGroup && insertTestDefinition(token, opts.groupmarker[1]))), verifyGroupMarker(token);
        });
      }

      function defaultCase() {
        if (0 < openenings.length) {
          if (currentOpeningToken = openenings[openenings.length - 1], insertTestDefinition(currentOpeningToken, m), currentOpeningToken.isAlternator) {
            alternator = openenings.pop();

            for (var mndx = 0; mndx < alternator.matches.length; mndx++) {
              alternator.matches[mndx].isGroup && (alternator.matches[mndx].isGroup = !1);
            }

            0 < openenings.length ? (currentOpeningToken = openenings[openenings.length - 1], currentOpeningToken.matches.push(alternator)) : currentToken.matches.push(alternator);
          }
        } else insertTestDefinition(currentToken, m);
      }

      function reverseTokens(maskToken) {
        function reverseStatic(st) {
          return st === opts.optionalmarker[0] ? st = opts.optionalmarker[1] : st === opts.optionalmarker[1] ? st = opts.optionalmarker[0] : st === opts.groupmarker[0] ? st = opts.groupmarker[1] : st === opts.groupmarker[1] && (st = opts.groupmarker[0]), st;
        }

        for (var match in maskToken.matches = maskToken.matches.reverse(), maskToken.matches) {
          if (Object.prototype.hasOwnProperty.call(maskToken.matches, match)) {
            var intMatch = parseInt(match);

            if (maskToken.matches[match].isQuantifier && maskToken.matches[intMatch + 1] && maskToken.matches[intMatch + 1].isGroup) {
              var qt = maskToken.matches[match];
              maskToken.matches.splice(match, 1), maskToken.matches.splice(intMatch + 1, 0, qt);
            }

            void 0 !== maskToken.matches[match].matches ? maskToken.matches[match] = reverseTokens(maskToken.matches[match]) : maskToken.matches[match] = reverseStatic(maskToken.matches[match]);
          }
        }

        return maskToken;
      }

      function groupify(matches) {
        var groupToken = new MaskToken(!0);
        return groupToken.openGroup = !1, groupToken.matches = matches, groupToken;
      }

      function closeGroup() {
        if (openingToken = openenings.pop(), openingToken.openGroup = !1, void 0 !== openingToken) {
          if (0 < openenings.length) {
            if (currentOpeningToken = openenings[openenings.length - 1], currentOpeningToken.matches.push(openingToken), currentOpeningToken.isAlternator) {
              alternator = openenings.pop();

              for (var mndx = 0; mndx < alternator.matches.length; mndx++) {
                alternator.matches[mndx].isGroup = !1, alternator.matches[mndx].alternatorGroup = !1;
              }

              0 < openenings.length ? (currentOpeningToken = openenings[openenings.length - 1], currentOpeningToken.matches.push(alternator)) : currentToken.matches.push(alternator);
            }
          } else currentToken.matches.push(openingToken);
        } else defaultCase();
      }

      function groupQuantifier(matches) {
        var lastMatch = matches.pop();
        return lastMatch.isQuantifier && (lastMatch = groupify([matches.pop(), lastMatch])), lastMatch;
      }

      for (regexMask && (opts.optionalmarker[0] = void 0, opts.optionalmarker[1] = void 0); match = regexMask ? regexTokenizer.exec(mask) : tokenizer.exec(mask);) {
        if (m = match[0], regexMask) switch (m.charAt(0)) {
          case "?":
            m = "{0,1}";
            break;

          case "+":
          case "*":
            m = "{" + m + "}";
            break;

          case "|":
            if (0 === openenings.length) {
              var altRegexGroup = groupify(currentToken.matches);
              altRegexGroup.openGroup = !0, openenings.push(altRegexGroup), currentToken.matches = [], closeRegexGroup = !0;
            }

            break;
        }
        if (escaped) defaultCase();else switch (m.charAt(0)) {
          case "(?=":
            break;

          case "(?!":
            break;

          case "(?<=":
            break;

          case "(?<!":
            break;

          case opts.escapeChar:
            escaped = !0, regexMask && defaultCase();
            break;

          case opts.optionalmarker[1]:
          case opts.groupmarker[1]:
            closeGroup();
            break;

          case opts.optionalmarker[0]:
            openenings.push(new MaskToken(!1, !0));
            break;

          case opts.groupmarker[0]:
            openenings.push(new MaskToken(!0));
            break;

          case opts.quantifiermarker[0]:
            var quantifier = new MaskToken(!1, !1, !0);
            m = m.replace(/[{}]/g, "");
            var mqj = m.split("|"),
                mq = mqj[0].split(","),
                mq0 = isNaN(mq[0]) ? mq[0] : parseInt(mq[0]),
                mq1 = 1 === mq.length ? mq0 : isNaN(mq[1]) ? mq[1] : parseInt(mq[1]);
            "*" !== mq0 && "+" !== mq0 || (mq0 = "*" === mq1 ? 0 : 1), quantifier.quantifier = {
              min: mq0,
              max: mq1,
              jit: mqj[1]
            };
            var matches = 0 < openenings.length ? openenings[openenings.length - 1].matches : currentToken.matches;

            if (match = matches.pop(), match.isAlternator) {
              matches.push(match), matches = match.matches;
              var groupToken = new MaskToken(!0),
                  tmpMatch = matches.pop();
              matches.push(groupToken), matches = groupToken.matches, match = tmpMatch;
            }

            match.isGroup || (match = groupify([match])), matches.push(match), matches.push(quantifier);
            break;

          case opts.alternatormarker:
            if (0 < openenings.length) {
              currentOpeningToken = openenings[openenings.length - 1];
              var subToken = currentOpeningToken.matches[currentOpeningToken.matches.length - 1];
              lastMatch = currentOpeningToken.openGroup && (void 0 === subToken.matches || !1 === subToken.isGroup && !1 === subToken.isAlternator) ? openenings.pop() : groupQuantifier(currentOpeningToken.matches);
            } else lastMatch = groupQuantifier(currentToken.matches);

            if (lastMatch.isAlternator) openenings.push(lastMatch);else if (lastMatch.alternatorGroup ? (alternator = openenings.pop(), lastMatch.alternatorGroup = !1) : alternator = new MaskToken(!1, !1, !1, !0), alternator.matches.push(lastMatch), openenings.push(alternator), lastMatch.openGroup) {
              lastMatch.openGroup = !1;
              var alternatorGroup = new MaskToken(!0);
              alternatorGroup.alternatorGroup = !0, openenings.push(alternatorGroup);
            }
            break;

          default:
            defaultCase();
        }
      }

      for (closeRegexGroup && closeGroup(); 0 < openenings.length;) {
        openingToken = openenings.pop(), currentToken.matches.push(openingToken);
      }

      return 0 < currentToken.matches.length && (verifyGroupMarker(currentToken), maskTokens.push(currentToken)), (opts.numericInput || opts.isRTL) && reverseTokens(maskTokens[0]), maskTokens;
    }

    module.exports = {
      generateMaskSet: generateMaskSet,
      analyseMask: analyseMask
    };
  }, function (module, exports, __webpack_require__) {
    "use strict";

    __webpack_require__(6), __webpack_require__(8), __webpack_require__(9), __webpack_require__(10), module.exports = __webpack_require__(1);
  }, function (module, exports, __webpack_require__) {
    "use strict";

    var Inputmask = __webpack_require__(1);

    Inputmask.extendDefinitions({
      A: {
        validator: "[A-Za-z\u0410-\u044F\u0401\u0451\xC0-\xFF\xB5]",
        casing: "upper"
      },
      "&": {
        validator: "[0-9A-Za-z\u0410-\u044F\u0401\u0451\xC0-\xFF\xB5]",
        casing: "upper"
      },
      "#": {
        validator: "[0-9A-Fa-f]",
        casing: "upper"
      }
    });
    var ipValidatorRegex = new RegExp("25[0-5]|2[0-4][0-9]|[01][0-9][0-9]");

    function ipValidator(chrs, maskset, pos, strict, opts) {
      return chrs = -1 < pos - 1 && "." !== maskset.buffer[pos - 1] ? (chrs = maskset.buffer[pos - 1] + chrs, -1 < pos - 2 && "." !== maskset.buffer[pos - 2] ? maskset.buffer[pos - 2] + chrs : "0" + chrs) : "00" + chrs, ipValidatorRegex.test(chrs);
    }

    Inputmask.extendAliases({
      cssunit: {
        regex: "[+-]?[0-9]+\\.?([0-9]+)?(px|em|rem|ex|%|in|cm|mm|pt|pc)"
      },
      url: {
        regex: "(https?|ftp)//.*",
        autoUnmask: !1
      },
      ip: {
        mask: "i[i[i]].j[j[j]].k[k[k]].l[l[l]]",
        definitions: {
          i: {
            validator: ipValidator
          },
          j: {
            validator: ipValidator
          },
          k: {
            validator: ipValidator
          },
          l: {
            validator: ipValidator
          }
        },
        onUnMask: function onUnMask(maskedValue, unmaskedValue, opts) {
          return maskedValue;
        },
        inputmode: "numeric"
      },
      email: {
        mask: "*{1,64}[.*{1,64}][.*{1,64}][.*{1,63}]@-{1,63}.-{1,63}[.-{1,63}][.-{1,63}]",
        greedy: !1,
        casing: "lower",
        onBeforePaste: function onBeforePaste(pastedValue, opts) {
          return pastedValue = pastedValue.toLowerCase(), pastedValue.replace("mailto:", "");
        },
        definitions: {
          "*": {
            validator: "[0-9\uFF11-\uFF19A-Za-z\u0410-\u044F\u0401\u0451\xC0-\xFF\xB5!#$%&'*+/=?^_`{|}~-]"
          },
          "-": {
            validator: "[0-9A-Za-z-]"
          }
        },
        onUnMask: function onUnMask(maskedValue, unmaskedValue, opts) {
          return maskedValue;
        },
        inputmode: "email"
      },
      mac: {
        mask: "##:##:##:##:##:##"
      },
      vin: {
        mask: "V{13}9{4}",
        definitions: {
          V: {
            validator: "[A-HJ-NPR-Za-hj-npr-z\\d]",
            casing: "upper"
          }
        },
        clearIncomplete: !0,
        autoUnmask: !0
      },
      ssn: {
        mask: "999-99-9999",
        postValidation: function postValidation(buffer, pos, c, currentResult, opts, maskset, strict) {
          return /^(?!219-09-9999|078-05-1120)(?!666|000|9.{2}).{3}-(?!00).{2}-(?!0{4}).{4}$/.test(buffer.join(""));
        }
      }
    }), module.exports = Inputmask;
  }, function (module, exports, __webpack_require__) {
    "use strict";

    function _typeof(obj) {
      return _typeof = "function" == typeof Symbol && "symbol" == _typeof2(Symbol.iterator) ? function _typeof(obj) {
        return _typeof2(obj);
      } : function _typeof(obj) {
        return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof2(obj);
      }, _typeof(obj);
    }

    var $ = __webpack_require__(2),
        window = __webpack_require__(3),
        document = window.document,
        ua = window.navigator && window.navigator.userAgent || "",
        ie = 0 < ua.indexOf("MSIE ") || 0 < ua.indexOf("Trident/"),
        mobile = "ontouchstart" in window,
        iemobile = /iemobile/i.test(ua),
        iphone = /iphone/i.test(ua) && !iemobile,
        keyCode = __webpack_require__(0);

    module.exports = function maskScope(actionObj, maskset, opts) {
      maskset = maskset || this.maskset, opts = opts || this.opts;
      var inputmask = this,
          el = this.el,
          isRTL = this.isRTL || (this.isRTL = opts.numericInput),
          undoValue,
          $el,
          skipKeyPressEvent = !1,
          skipInputEvent = !1,
          validationEvent = !1,
          ignorable = !1,
          maxLength,
          mouseEnter = !1,
          originalPlaceholder = void 0;

      function getMaskTemplate(baseOnInput, minimalPos, includeMode, noJit, clearOptionalTail) {
        var greedy = opts.greedy;
        clearOptionalTail && (opts.greedy = !1), minimalPos = minimalPos || 0;
        var maskTemplate = [],
            ndxIntlzr,
            pos = 0,
            test,
            testPos,
            jitRenderStatic;

        do {
          if (!0 === baseOnInput && maskset.validPositions[pos]) testPos = clearOptionalTail && !0 === maskset.validPositions[pos].match.optionality && void 0 === maskset.validPositions[pos + 1] && (!0 === maskset.validPositions[pos].generatedInput || maskset.validPositions[pos].input == opts.skipOptionalPartCharacter && 0 < pos) ? determineTestTemplate(pos, getTests(pos, ndxIntlzr, pos - 1)) : maskset.validPositions[pos], test = testPos.match, ndxIntlzr = testPos.locator.slice(), maskTemplate.push(!0 === includeMode ? testPos.input : !1 === includeMode ? test.nativeDef : getPlaceholder(pos, test));else {
            testPos = getTestTemplate(pos, ndxIntlzr, pos - 1), test = testPos.match, ndxIntlzr = testPos.locator.slice();
            var jitMasking = !0 !== noJit && (!1 !== opts.jitMasking ? opts.jitMasking : test.jit);
            jitRenderStatic = jitRenderStatic && test["static"] && test.def !== opts.groupSeparator && null === test.fn || maskset.validPositions[pos - 1] && test["static"] && test.def !== opts.groupSeparator && null === test.fn, jitRenderStatic || !1 === jitMasking || void 0 === jitMasking || "number" == typeof jitMasking && isFinite(jitMasking) && pos < jitMasking ? maskTemplate.push(!1 === includeMode ? test.nativeDef : getPlaceholder(pos, test)) : jitRenderStatic = !1;
          }
          pos++;
        } while ((void 0 === maxLength || pos < maxLength) && (!0 !== test["static"] || "" !== test.def) || pos < minimalPos);

        return "" === maskTemplate[maskTemplate.length - 1] && maskTemplate.pop(), !1 === includeMode && void 0 !== maskset.maskLength || (maskset.maskLength = pos - 1), opts.greedy = greedy, maskTemplate;
      }

      function resetMaskSet(soft) {
        maskset.buffer = void 0, !0 !== soft && (maskset.validPositions = {}, maskset.p = 0);
      }

      function getLastValidPosition(closestTo, strict, validPositions) {
        var before = -1,
            after = -1,
            valids = validPositions || maskset.validPositions;

        for (var posNdx in void 0 === closestTo && (closestTo = -1), valids) {
          var psNdx = parseInt(posNdx);
          valids[psNdx] && (strict || !0 !== valids[psNdx].generatedInput) && (psNdx <= closestTo && (before = psNdx), closestTo <= psNdx && (after = psNdx));
        }

        return -1 === before || before == closestTo ? after : -1 == after ? before : closestTo - before < after - closestTo ? before : after;
      }

      function getDecisionTaker(tst) {
        var decisionTaker = tst.locator[tst.alternation];
        return "string" == typeof decisionTaker && 0 < decisionTaker.length && (decisionTaker = decisionTaker.split(",")[0]), void 0 !== decisionTaker ? decisionTaker.toString() : "";
      }

      function getLocator(tst, align) {
        var locator = (null != tst.alternation ? tst.mloc[getDecisionTaker(tst)] : tst.locator).join("");
        if ("" !== locator) for (; locator.length < align;) {
          locator += "0";
        }
        return locator;
      }

      function determineTestTemplate(pos, tests) {
        pos = 0 < pos ? pos - 1 : 0;

        for (var altTest = getTest(pos), targetLocator = getLocator(altTest), tstLocator, closest, bestMatch, ndx = 0; ndx < tests.length; ndx++) {
          var tst = tests[ndx];
          tstLocator = getLocator(tst, targetLocator.length);
          var distance = Math.abs(tstLocator - targetLocator);
          (void 0 === closest || "" !== tstLocator && distance < closest || bestMatch && !opts.greedy && bestMatch.match.optionality && "master" === bestMatch.match.newBlockMarker && (!tst.match.optionality || !tst.match.newBlockMarker) || bestMatch && bestMatch.match.optionalQuantifier && !tst.match.optionalQuantifier) && (closest = distance, bestMatch = tst);
        }

        return bestMatch;
      }

      function getTestTemplate(pos, ndxIntlzr, tstPs) {
        return maskset.validPositions[pos] || determineTestTemplate(pos, getTests(pos, ndxIntlzr ? ndxIntlzr.slice() : ndxIntlzr, tstPs));
      }

      function getTest(pos, tests) {
        return maskset.validPositions[pos] ? maskset.validPositions[pos] : (tests || getTests(pos))[0];
      }

      function positionCanMatchDefinition(pos, testDefinition, opts) {
        for (var valid = !1, tests = getTests(pos), tndx = 0; tndx < tests.length; tndx++) {
          if (tests[tndx].match && (!(tests[tndx].match.nativeDef !== testDefinition.match[opts.shiftPositions ? "def" : "nativeDef"] || opts.shiftPositions && testDefinition.match["static"]) || tests[tndx].match.nativeDef === testDefinition.match.nativeDef)) {
            valid = !0;
            break;
          }

          if (tests[tndx].match && tests[tndx].match.def === testDefinition.match.nativeDef) {
            valid = void 0;
            break;
          }
        }

        return !1 === valid && void 0 !== maskset.jitOffset[pos] && (valid = positionCanMatchDefinition(pos + maskset.jitOffset[pos], testDefinition, opts)), valid;
      }

      function getTests(pos, ndxIntlzr, tstPs) {
        var maskTokens = maskset.maskToken,
            testPos = ndxIntlzr ? tstPs : 0,
            ndxInitializer = ndxIntlzr ? ndxIntlzr.slice() : [0],
            matches = [],
            insertStop = !1,
            latestMatch,
            cacheDependency = ndxIntlzr ? ndxIntlzr.join("") : "";

        function resolveTestFromToken(maskToken, ndxInitializer, loopNdx, quantifierRecurse) {
          function handleMatch(match, loopNdx, quantifierRecurse) {
            function isFirstMatch(latestMatch, tokenGroup) {
              var firstMatch = 0 === $.inArray(latestMatch, tokenGroup.matches);
              return firstMatch || $.each(tokenGroup.matches, function (ndx, match) {
                if (!0 === match.isQuantifier ? firstMatch = isFirstMatch(latestMatch, tokenGroup.matches[ndx - 1]) : Object.prototype.hasOwnProperty.call(match, "matches") && (firstMatch = isFirstMatch(latestMatch, match)), firstMatch) return !1;
              }), firstMatch;
            }

            function resolveNdxInitializer(pos, alternateNdx, targetAlternation) {
              var bestMatch, indexPos;

              if ((maskset.tests[pos] || maskset.validPositions[pos]) && $.each(maskset.tests[pos] || [maskset.validPositions[pos]], function (ndx, lmnt) {
                if (lmnt.mloc[alternateNdx]) return bestMatch = lmnt, !1;
                var alternation = void 0 !== targetAlternation ? targetAlternation : lmnt.alternation,
                    ndxPos = void 0 !== lmnt.locator[alternation] ? lmnt.locator[alternation].toString().indexOf(alternateNdx) : -1;
                (void 0 === indexPos || ndxPos < indexPos) && -1 !== ndxPos && (bestMatch = lmnt, indexPos = ndxPos);
              }), bestMatch) {
                var bestMatchAltIndex = bestMatch.locator[bestMatch.alternation],
                    locator = bestMatch.mloc[alternateNdx] || bestMatch.mloc[bestMatchAltIndex] || bestMatch.locator;
                return locator.slice((void 0 !== targetAlternation ? targetAlternation : bestMatch.alternation) + 1);
              }

              return void 0 !== targetAlternation ? resolveNdxInitializer(pos, alternateNdx) : void 0;
            }

            function isSubsetOf(source, target) {
              function expand(pattern) {
                for (var expanded = [], start = -1, end, i = 0, l = pattern.length; i < l; i++) {
                  if ("-" === pattern.charAt(i)) for (end = pattern.charCodeAt(i + 1); ++start < end;) {
                    expanded.push(String.fromCharCode(start));
                  } else start = pattern.charCodeAt(i), expanded.push(pattern.charAt(i));
                }

                return expanded.join("");
              }

              return source.match.def === target.match.nativeDef || !(!(opts.regex || source.match.fn instanceof RegExp && target.match.fn instanceof RegExp) || !0 === source.match["static"] || !0 === target.match["static"]) && -1 !== expand(target.match.fn.toString().replace(/[[\]/]/g, "")).indexOf(expand(source.match.fn.toString().replace(/[[\]/]/g, "")));
            }

            function staticCanMatchDefinition(source, target) {
              return !0 === source.match["static"] && !0 !== target.match["static"] && target.match.fn.test(source.match.def, maskset, pos, !1, opts, !1);
            }

            function setMergeLocators(targetMatch, altMatch) {
              var alternationNdx = targetMatch.alternation,
                  shouldMerge = void 0 === altMatch || alternationNdx === altMatch.alternation && -1 === targetMatch.locator[alternationNdx].toString().indexOf(altMatch.locator[alternationNdx]);
              if (!shouldMerge && alternationNdx > altMatch.alternation) for (var i = altMatch.alternation; i < alternationNdx; i++) {
                if (targetMatch.locator[i] !== altMatch.locator[i]) {
                  alternationNdx = i, shouldMerge = !0;
                  break;
                }
              }

              if (shouldMerge) {
                targetMatch.mloc = targetMatch.mloc || {};
                var locNdx = targetMatch.locator[alternationNdx];

                if (void 0 !== locNdx) {
                  if ("string" == typeof locNdx && (locNdx = locNdx.split(",")[0]), void 0 === targetMatch.mloc[locNdx] && (targetMatch.mloc[locNdx] = targetMatch.locator.slice()), void 0 !== altMatch) {
                    for (var ndx in altMatch.mloc) {
                      "string" == typeof ndx && (ndx = ndx.split(",")[0]), void 0 === targetMatch.mloc[ndx] && (targetMatch.mloc[ndx] = altMatch.mloc[ndx]);
                    }

                    targetMatch.locator[alternationNdx] = Object.keys(targetMatch.mloc).join(",");
                  }

                  return !0;
                }

                targetMatch.alternation = void 0;
              }

              return !1;
            }

            function isSameLevel(targetMatch, altMatch) {
              if (targetMatch.locator.length !== altMatch.locator.length) return !1;

              for (var locNdx = targetMatch.alternation + 1; locNdx < targetMatch.locator.length; locNdx++) {
                if (targetMatch.locator[locNdx] !== altMatch.locator[locNdx]) return !1;
              }

              return !0;
            }

            if (testPos > opts._maxTestPos && void 0 !== quantifierRecurse) throw "Inputmask: There is probably an error in your mask definition or in the code. Create an issue on github with an example of the mask you are using. " + maskset.mask;
            if (testPos === pos && void 0 === match.matches) return matches.push({
              match: match,
              locator: loopNdx.reverse(),
              cd: cacheDependency,
              mloc: {}
            }), !0;

            if (void 0 !== match.matches) {
              if (match.isGroup && quantifierRecurse !== match) {
                if (match = handleMatch(maskToken.matches[$.inArray(match, maskToken.matches) + 1], loopNdx, quantifierRecurse), match) return !0;
              } else if (match.isOptional) {
                var optionalToken = match,
                    mtchsNdx = matches.length;

                if (match = resolveTestFromToken(match, ndxInitializer, loopNdx, quantifierRecurse), match) {
                  if ($.each(matches, function (ndx, mtch) {
                    mtchsNdx <= ndx && (mtch.match.optionality = !0);
                  }), latestMatch = matches[matches.length - 1].match, void 0 !== quantifierRecurse || !isFirstMatch(latestMatch, optionalToken)) return !0;
                  insertStop = !0, testPos = pos;
                }
              } else if (match.isAlternator) {
                var alternateToken = match,
                    malternateMatches = [],
                    maltMatches,
                    currentMatches = matches.slice(),
                    loopNdxCnt = loopNdx.length,
                    altIndex = 0 < ndxInitializer.length ? ndxInitializer.shift() : -1;

                if (-1 === altIndex || "string" == typeof altIndex) {
                  var currentPos = testPos,
                      ndxInitializerClone = ndxInitializer.slice(),
                      altIndexArr = [],
                      amndx;
                  if ("string" == typeof altIndex) altIndexArr = altIndex.split(",");else for (amndx = 0; amndx < alternateToken.matches.length; amndx++) {
                    altIndexArr.push(amndx.toString());
                  }

                  if (void 0 !== maskset.excludes[pos]) {
                    for (var altIndexArrClone = altIndexArr.slice(), i = 0, exl = maskset.excludes[pos].length; i < exl; i++) {
                      var excludeSet = maskset.excludes[pos][i].toString().split(":");
                      loopNdx.length == excludeSet[1] && altIndexArr.splice(altIndexArr.indexOf(excludeSet[0]), 1);
                    }

                    0 === altIndexArr.length && (delete maskset.excludes[pos], altIndexArr = altIndexArrClone);
                  }

                  (!0 === opts.keepStatic || isFinite(parseInt(opts.keepStatic)) && currentPos >= opts.keepStatic) && (altIndexArr = altIndexArr.slice(0, 1));

                  for (var unMatchedAlternation = !1, ndx = 0; ndx < altIndexArr.length; ndx++) {
                    amndx = parseInt(altIndexArr[ndx]), matches = [], ndxInitializer = "string" == typeof altIndex && resolveNdxInitializer(testPos, amndx, loopNdxCnt) || ndxInitializerClone.slice(), alternateToken.matches[amndx] && handleMatch(alternateToken.matches[amndx], [amndx].concat(loopNdx), quantifierRecurse) ? match = !0 : 0 === ndx && (unMatchedAlternation = !0), maltMatches = matches.slice(), testPos = currentPos, matches = [];

                    for (var ndx1 = 0; ndx1 < maltMatches.length; ndx1++) {
                      var altMatch = maltMatches[ndx1],
                          dropMatch = !1;
                      altMatch.match.jit = altMatch.match.jit || unMatchedAlternation, altMatch.alternation = altMatch.alternation || loopNdxCnt, setMergeLocators(altMatch);

                      for (var ndx2 = 0; ndx2 < malternateMatches.length; ndx2++) {
                        var altMatch2 = malternateMatches[ndx2];

                        if ("string" != typeof altIndex || void 0 !== altMatch.alternation && -1 !== $.inArray(altMatch.locator[altMatch.alternation].toString(), altIndexArr)) {
                          if (altMatch.match.nativeDef === altMatch2.match.nativeDef) {
                            dropMatch = !0, setMergeLocators(altMatch2, altMatch);
                            break;
                          }

                          if (isSubsetOf(altMatch, altMatch2)) {
                            setMergeLocators(altMatch, altMatch2) && (dropMatch = !0, malternateMatches.splice(malternateMatches.indexOf(altMatch2), 0, altMatch));
                            break;
                          }

                          if (isSubsetOf(altMatch2, altMatch)) {
                            setMergeLocators(altMatch2, altMatch);
                            break;
                          }

                          if (staticCanMatchDefinition(altMatch, altMatch2)) {
                            isSameLevel(altMatch, altMatch2) || void 0 !== el.inputmask.userOptions.keepStatic ? setMergeLocators(altMatch, altMatch2) && (dropMatch = !0, malternateMatches.splice(malternateMatches.indexOf(altMatch2), 0, altMatch)) : opts.keepStatic = !0;
                            break;
                          }
                        }
                      }

                      dropMatch || malternateMatches.push(altMatch);
                    }
                  }

                  matches = currentMatches.concat(malternateMatches), testPos = pos, insertStop = 0 < matches.length, match = 0 < malternateMatches.length, ndxInitializer = ndxInitializerClone.slice();
                } else match = handleMatch(alternateToken.matches[altIndex] || maskToken.matches[altIndex], [altIndex].concat(loopNdx), quantifierRecurse);

                if (match) return !0;
              } else if (match.isQuantifier && quantifierRecurse !== maskToken.matches[$.inArray(match, maskToken.matches) - 1]) for (var qt = match, qndx = 0 < ndxInitializer.length ? ndxInitializer.shift() : 0; qndx < (isNaN(qt.quantifier.max) ? qndx + 1 : qt.quantifier.max) && testPos <= pos; qndx++) {
                var tokenGroup = maskToken.matches[$.inArray(qt, maskToken.matches) - 1];

                if (match = handleMatch(tokenGroup, [qndx].concat(loopNdx), tokenGroup), match) {
                  if (latestMatch = matches[matches.length - 1].match, latestMatch.optionalQuantifier = qndx >= qt.quantifier.min, latestMatch.jit = (qndx || 1) * tokenGroup.matches.indexOf(latestMatch) >= qt.quantifier.jit, latestMatch.optionalQuantifier && isFirstMatch(latestMatch, tokenGroup)) {
                    insertStop = !0, testPos = pos;
                    break;
                  }

                  return latestMatch.jit && (maskset.jitOffset[pos] = tokenGroup.matches.length - tokenGroup.matches.indexOf(latestMatch)), !0;
                }
              } else if (match = resolveTestFromToken(match, ndxInitializer, loopNdx, quantifierRecurse), match) return !0;
            } else testPos++;
          }

          for (var tndx = 0 < ndxInitializer.length ? ndxInitializer.shift() : 0; tndx < maskToken.matches.length; tndx++) {
            if (!0 !== maskToken.matches[tndx].isQuantifier) {
              var match = handleMatch(maskToken.matches[tndx], [tndx].concat(loopNdx), quantifierRecurse);
              if (match && testPos === pos) return match;
              if (pos < testPos) break;
            }
          }
        }

        function mergeLocators(pos, tests) {
          var locator = [];
          return $.isArray(tests) || (tests = [tests]), 0 < tests.length && (void 0 === tests[0].alternation || !0 === opts.keepStatic ? (locator = determineTestTemplate(pos, tests.slice()).locator.slice(), 0 === locator.length && (locator = tests[0].locator.slice())) : $.each(tests, function (ndx, tst) {
            if ("" !== tst.def) if (0 === locator.length) locator = tst.locator.slice();else for (var i = 0; i < locator.length; i++) {
              tst.locator[i] && -1 === locator[i].toString().indexOf(tst.locator[i]) && (locator[i] += "," + tst.locator[i]);
            }
          })), locator;
        }

        if (-1 < pos && (void 0 === maxLength || pos < maxLength)) {
          if (void 0 === ndxIntlzr) {
            for (var previousPos = pos - 1, test; void 0 === (test = maskset.validPositions[previousPos] || maskset.tests[previousPos]) && -1 < previousPos;) {
              previousPos--;
            }

            void 0 !== test && -1 < previousPos && (ndxInitializer = mergeLocators(previousPos, test), cacheDependency = ndxInitializer.join(""), testPos = previousPos);
          }

          if (maskset.tests[pos] && maskset.tests[pos][0].cd === cacheDependency) return maskset.tests[pos];

          for (var mtndx = ndxInitializer.shift(); mtndx < maskTokens.length; mtndx++) {
            var match = resolveTestFromToken(maskTokens[mtndx], ndxInitializer, [mtndx]);
            if (match && testPos === pos || pos < testPos) break;
          }
        }

        return 0 !== matches.length && !insertStop || matches.push({
          match: {
            fn: null,
            "static": !0,
            optionality: !1,
            casing: null,
            def: "",
            placeholder: ""
          },
          locator: [],
          mloc: {},
          cd: cacheDependency
        }), void 0 !== ndxIntlzr && maskset.tests[pos] ? $.extend(!0, [], matches) : (maskset.tests[pos] = $.extend(!0, [], matches), maskset.tests[pos]);
      }

      function getBufferTemplate() {
        return void 0 === maskset._buffer && (maskset._buffer = getMaskTemplate(!1, 1), void 0 === maskset.buffer && (maskset.buffer = maskset._buffer.slice())), maskset._buffer;
      }

      function getBuffer(noCache) {
        return void 0 !== maskset.buffer && !0 !== noCache || (maskset.buffer = getMaskTemplate(!0, getLastValidPosition(), !0), void 0 === maskset._buffer && (maskset._buffer = maskset.buffer.slice())), maskset.buffer;
      }

      function refreshFromBuffer(start, end, buffer) {
        var i,
            p,
            skipOptionalPartCharacter = opts.skipOptionalPartCharacter,
            bffr = isRTL ? buffer.slice().reverse() : buffer;
        if (opts.skipOptionalPartCharacter = "", !0 === start) resetMaskSet(), maskset.tests = {}, start = 0, end = buffer.length, p = determineNewCaretPosition({
          begin: 0,
          end: 0
        }, !1).begin;else {
          for (i = start; i < end; i++) {
            delete maskset.validPositions[i];
          }

          p = start;
        }
        var keypress = new $.Event("keypress");

        for (i = start; i < end; i++) {
          keypress.which = bffr[i].toString().charCodeAt(0), ignorable = !1;
          var valResult = EventHandlers.keypressEvent.call(el, keypress, !0, !1, !1, p);
          !1 !== valResult && (p = valResult.forwardPosition);
        }

        opts.skipOptionalPartCharacter = skipOptionalPartCharacter;
      }

      function casing(elem, test, pos) {
        switch (opts.casing || test.casing) {
          case "upper":
            elem = elem.toUpperCase();
            break;

          case "lower":
            elem = elem.toLowerCase();
            break;

          case "title":
            var posBefore = maskset.validPositions[pos - 1];
            elem = 0 === pos || posBefore && posBefore.input === String.fromCharCode(keyCode.SPACE) ? elem.toUpperCase() : elem.toLowerCase();
            break;

          default:
            if ($.isFunction(opts.casing)) {
              var args = Array.prototype.slice.call(arguments);
              args.push(maskset.validPositions), elem = opts.casing.apply(this, args);
            }

        }

        return elem;
      }

      function checkAlternationMatch(altArr1, altArr2, na) {
        for (var altArrC = opts.greedy ? altArr2 : altArr2.slice(0, 1), isMatch = !1, naArr = void 0 !== na ? na.split(",") : [], naNdx, i = 0; i < naArr.length; i++) {
          -1 !== (naNdx = altArr1.indexOf(naArr[i])) && altArr1.splice(naNdx, 1);
        }

        for (var alndx = 0; alndx < altArr1.length; alndx++) {
          if (-1 !== $.inArray(altArr1[alndx], altArrC)) {
            isMatch = !0;
            break;
          }
        }

        return isMatch;
      }

      function alternate(maskPos, c, strict, fromIsValid, rAltPos, selection) {
        var validPsClone = $.extend(!0, {}, maskset.validPositions),
            tstClone = $.extend(!0, {}, maskset.tests),
            lastAlt,
            alternation,
            isValidRslt = !1,
            returnRslt = !1,
            altPos,
            prevAltPos,
            i,
            validPos,
            decisionPos,
            lAltPos = void 0 !== rAltPos ? rAltPos : getLastValidPosition(),
            nextPos,
            input,
            begin,
            end;
        if (selection && (begin = selection.begin, end = selection.end, selection.begin > selection.end && (begin = selection.end, end = selection.begin)), -1 === lAltPos && void 0 === rAltPos) lastAlt = 0, prevAltPos = getTest(lastAlt), alternation = prevAltPos.alternation;else for (; 0 <= lAltPos; lAltPos--) {
          if (altPos = maskset.validPositions[lAltPos], altPos && void 0 !== altPos.alternation) {
            if (prevAltPos && prevAltPos.locator[altPos.alternation] !== altPos.locator[altPos.alternation]) break;
            lastAlt = lAltPos, alternation = maskset.validPositions[lastAlt].alternation, prevAltPos = altPos;
          }
        }

        if (void 0 !== alternation) {
          decisionPos = parseInt(lastAlt), maskset.excludes[decisionPos] = maskset.excludes[decisionPos] || [], !0 !== maskPos && maskset.excludes[decisionPos].push(getDecisionTaker(prevAltPos) + ":" + prevAltPos.alternation);
          var validInputs = [],
              resultPos = -1;

          for (i = decisionPos; i < getLastValidPosition(void 0, !0) + 1; i++) {
            -1 === resultPos && maskPos <= i && void 0 !== c && (validInputs.push(c), resultPos = validInputs.length - 1), validPos = maskset.validPositions[i], validPos && !0 !== validPos.generatedInput && (void 0 === selection || i < begin || end <= i) && validInputs.push(validPos.input), delete maskset.validPositions[i];
          }

          for (-1 === resultPos && void 0 !== c && (validInputs.push(c), resultPos = validInputs.length - 1); void 0 !== maskset.excludes[decisionPos] && maskset.excludes[decisionPos].length < 10;) {
            for (maskset.tests = {}, resetMaskSet(!0), isValidRslt = !0, i = 0; i < validInputs.length && (nextPos = isValidRslt.caret || getLastValidPosition(void 0, !0) + 1, input = validInputs[i], isValidRslt = isValid(nextPos, input, !1, fromIsValid, !0)); i++) {
              i === resultPos && (returnRslt = isValidRslt), 1 == maskPos && isValidRslt && (returnRslt = {
                caretPos: i
              });
            }

            if (isValidRslt) break;

            if (resetMaskSet(), prevAltPos = getTest(decisionPos), maskset.validPositions = $.extend(!0, {}, validPsClone), maskset.tests = $.extend(!0, {}, tstClone), !maskset.excludes[decisionPos]) {
              returnRslt = alternate(maskPos, c, strict, fromIsValid, decisionPos - 1, selection);
              break;
            }

            var decisionTaker = getDecisionTaker(prevAltPos);

            if (-1 !== maskset.excludes[decisionPos].indexOf(decisionTaker + ":" + prevAltPos.alternation)) {
              returnRslt = alternate(maskPos, c, strict, fromIsValid, decisionPos - 1, selection);
              break;
            }

            for (maskset.excludes[decisionPos].push(decisionTaker + ":" + prevAltPos.alternation), i = decisionPos; i < getLastValidPosition(void 0, !0) + 1; i++) {
              delete maskset.validPositions[i];
            }
          }
        }

        return returnRslt && !1 === opts.keepStatic || delete maskset.excludes[decisionPos], returnRslt;
      }

      function isValid(pos, c, strict, fromIsValid, fromAlternate, validateOnly) {
        function isSelection(posObj) {
          return isRTL ? 1 < posObj.begin - posObj.end || posObj.begin - posObj.end == 1 : 1 < posObj.end - posObj.begin || posObj.end - posObj.begin == 1;
        }

        strict = !0 === strict;
        var maskPos = pos;

        function processCommandObject(commandObj) {
          if (void 0 !== commandObj) {
            if (void 0 !== commandObj.remove && ($.isArray(commandObj.remove) || (commandObj.remove = [commandObj.remove]), $.each(commandObj.remove.sort(function (a, b) {
              return b.pos - a.pos;
            }), function (ndx, lmnt) {
              revalidateMask({
                begin: lmnt,
                end: lmnt + 1
              });
            }), commandObj.remove = void 0), void 0 !== commandObj.insert && ($.isArray(commandObj.insert) || (commandObj.insert = [commandObj.insert]), $.each(commandObj.insert.sort(function (a, b) {
              return a.pos - b.pos;
            }), function (ndx, lmnt) {
              "" !== lmnt.c && isValid(lmnt.pos, lmnt.c, void 0 === lmnt.strict || lmnt.strict, void 0 !== lmnt.fromIsValid ? lmnt.fromIsValid : fromIsValid);
            }), commandObj.insert = void 0), commandObj.refreshFromBuffer && commandObj.buffer) {
              var refresh = commandObj.refreshFromBuffer;
              refreshFromBuffer(!0 === refresh ? refresh : refresh.start, refresh.end, commandObj.buffer), commandObj.refreshFromBuffer = void 0;
            }

            void 0 !== commandObj.rewritePosition && (maskPos = commandObj.rewritePosition, commandObj = !0);
          }

          return commandObj;
        }

        function _isValid(position, c, strict) {
          var rslt = !1;
          return $.each(getTests(position), function (ndx, tst) {
            var test = tst.match;

            if (getBuffer(!0), rslt = null != test.fn ? test.fn.test(c, maskset, position, strict, opts, isSelection(pos)) : (c === test.def || c === opts.skipOptionalPartCharacter) && "" !== test.def && {
              c: getPlaceholder(position, test, !0) || test.def,
              pos: position
            }, !1 !== rslt) {
              var elem = void 0 !== rslt.c ? rslt.c : c,
                  validatedPos = position;
              return elem = elem === opts.skipOptionalPartCharacter && !0 === test["static"] ? getPlaceholder(position, test, !0) || test.def : elem, rslt = processCommandObject(rslt), !0 !== rslt && void 0 !== rslt.pos && rslt.pos !== position && (validatedPos = rslt.pos), !0 !== rslt && void 0 === rslt.pos && void 0 === rslt.c ? !1 : (!1 === revalidateMask(pos, $.extend({}, tst, {
                input: casing(elem, test, validatedPos)
              }), fromIsValid, validatedPos) && (rslt = !1), !1);
            }
          }), rslt;
        }

        void 0 !== pos.begin && (maskPos = isRTL ? pos.end : pos.begin);
        var result = !0,
            positionsClone = $.extend(!0, {}, maskset.validPositions);
        if (!1 === opts.keepStatic && void 0 !== maskset.excludes[maskPos] && !0 !== fromAlternate && !0 !== fromIsValid) for (var i = maskPos; i < (isRTL ? pos.begin : pos.end); i++) {
          void 0 !== maskset.excludes[i] && (maskset.excludes[i] = void 0, delete maskset.tests[i]);
        }

        if ($.isFunction(opts.preValidation) && !0 !== fromIsValid && !0 !== validateOnly && (result = opts.preValidation.call(el, getBuffer(), maskPos, c, isSelection(pos), opts, maskset, pos, strict || fromAlternate), result = processCommandObject(result)), !0 === result) {
          if (void 0 === maxLength || maskPos < maxLength) {
            if (result = _isValid(maskPos, c, strict), (!strict || !0 === fromIsValid) && !1 === result && !0 !== validateOnly) {
              var currentPosValid = maskset.validPositions[maskPos];

              if (!currentPosValid || !0 !== currentPosValid.match["static"] || currentPosValid.match.def !== c && c !== opts.skipOptionalPartCharacter) {
                if (opts.insertMode || void 0 === maskset.validPositions[seekNext(maskPos)] || pos.end > maskPos) {
                  var skip = !1;
                  if (maskset.jitOffset[maskPos] && void 0 === maskset.validPositions[seekNext(maskPos)] && (result = isValid(maskPos + maskset.jitOffset[maskPos], c, !0), !1 !== result && (!0 !== fromAlternate && (result.caret = maskPos), skip = !0)), pos.end > maskPos && (maskset.validPositions[maskPos] = void 0), !skip && !isMask(maskPos, opts.keepStatic)) for (var nPos = maskPos + 1, snPos = seekNext(maskPos); nPos <= snPos; nPos++) {
                    if (result = _isValid(nPos, c, strict), !1 !== result) {
                      result = trackbackPositions(maskPos, void 0 !== result.pos ? result.pos : nPos) || result, maskPos = nPos;
                      break;
                    }
                  }
                }
              } else result = {
                caret: seekNext(maskPos)
              };
            }
          } else result = !1;

          !1 !== result || !opts.keepStatic || !isComplete(getBuffer()) && 0 !== maskPos || strict || !0 === fromAlternate ? isSelection(pos) && maskset.tests[maskPos] && 1 < maskset.tests[maskPos].length && opts.keepStatic && !strict && !0 !== fromAlternate && (result = alternate(!0)) : result = alternate(maskPos, c, strict, fromIsValid, void 0, pos), !0 === result && (result = {
            pos: maskPos
          });
        }

        if ($.isFunction(opts.postValidation) && !0 !== fromIsValid && !0 !== validateOnly) {
          var postResult = opts.postValidation.call(el, getBuffer(!0), void 0 !== pos.begin ? isRTL ? pos.end : pos.begin : pos, c, result, opts, maskset, strict);
          void 0 !== postResult && (result = !0 === postResult ? result : postResult);
        }

        result && void 0 === result.pos && (result.pos = maskPos), !1 === result || !0 === validateOnly ? (resetMaskSet(!0), maskset.validPositions = $.extend(!0, {}, positionsClone)) : trackbackPositions(void 0, maskPos, !0);
        var endResult = processCommandObject(result);
        return endResult;
      }

      function trackbackPositions(originalPos, newPos, fillOnly) {
        if (void 0 === originalPos) for (originalPos = newPos - 1; 0 < originalPos && !maskset.validPositions[originalPos]; originalPos--) {
          ;
        }

        for (var ps = originalPos; ps < newPos; ps++) {
          if (void 0 === maskset.validPositions[ps] && !isMask(ps, !0)) {
            var vp = 0 == ps ? getTest(ps) : maskset.validPositions[ps - 1];

            if (vp) {
              var tests = getTests(ps).slice();
              "" === tests[tests.length - 1].match.def && tests.pop();
              var bestMatch = determineTestTemplate(ps, tests),
                  np;

              if (bestMatch && (!0 !== bestMatch.match.jit || "master" === bestMatch.match.newBlockMarker && (np = maskset.validPositions[ps + 1]) && !0 === np.match.optionalQuantifier) && (bestMatch = $.extend({}, bestMatch, {
                input: getPlaceholder(ps, bestMatch.match, !0) || bestMatch.match.def
              }), bestMatch.generatedInput = !0, revalidateMask(ps, bestMatch, !0), !0 !== fillOnly)) {
                var cvpInput = maskset.validPositions[newPos].input;
                return maskset.validPositions[newPos] = void 0, isValid(newPos, cvpInput, !0, !0);
              }
            }
          }
        }
      }

      function revalidateMask(pos, validTest, fromIsValid, validatedPos) {
        function IsEnclosedStatic(pos, valids, selection) {
          var posMatch = valids[pos];
          if (void 0 === posMatch || !0 !== posMatch.match["static"] || !0 === posMatch.match.optionality || void 0 !== valids[0] && void 0 !== valids[0].alternation) return !1;
          var prevMatch = selection.begin <= pos - 1 ? valids[pos - 1] && !0 === valids[pos - 1].match["static"] && valids[pos - 1] : valids[pos - 1],
              nextMatch = selection.end > pos + 1 ? valids[pos + 1] && !0 === valids[pos + 1].match["static"] && valids[pos + 1] : valids[pos + 1];
          return prevMatch && nextMatch;
        }

        var offset = 0,
            begin = void 0 !== pos.begin ? pos.begin : pos,
            end = void 0 !== pos.end ? pos.end : pos;

        if (pos.begin > pos.end && (begin = pos.end, end = pos.begin), validatedPos = void 0 !== validatedPos ? validatedPos : begin, begin !== end || opts.insertMode && void 0 !== maskset.validPositions[validatedPos] && void 0 === fromIsValid || void 0 === validTest) {
          var positionsClone = $.extend(!0, {}, maskset.validPositions),
              lvp = getLastValidPosition(void 0, !0),
              i;

          for (maskset.p = begin, i = lvp; begin <= i; i--) {
            delete maskset.validPositions[i], void 0 === validTest && delete maskset.tests[i + 1];
          }

          var valid = !0,
              j = validatedPos,
              posMatch = j,
              t,
              canMatch;

          for (validTest && (maskset.validPositions[validatedPos] = $.extend(!0, {}, validTest), posMatch++, j++), i = validTest ? end : end - 1; i <= lvp; i++) {
            if (void 0 !== (t = positionsClone[i]) && !0 !== t.generatedInput && (end <= i || begin <= i && IsEnclosedStatic(i, positionsClone, {
              begin: begin,
              end: end
            }))) {
              for (; "" !== getTest(posMatch).match.def;) {
                if (!1 !== (canMatch = positionCanMatchDefinition(posMatch, t, opts)) || "+" === t.match.def) {
                  "+" === t.match.def && getBuffer(!0);
                  var result = isValid(posMatch, t.input, "+" !== t.match.def, "+" !== t.match.def);
                  if (valid = !1 !== result, j = (result.pos || posMatch) + 1, !valid && canMatch) break;
                } else valid = !1;

                if (valid) {
                  void 0 === validTest && t.match["static"] && i === pos.begin && offset++;
                  break;
                }

                if (!valid && posMatch > maskset.maskLength) break;
                posMatch++;
              }

              "" == getTest(posMatch).match.def && (valid = !1), posMatch = j;
            }

            if (!valid) break;
          }

          if (!valid) return maskset.validPositions = $.extend(!0, {}, positionsClone), resetMaskSet(!0), !1;
        } else validTest && getTest(validatedPos).match.cd === validTest.match.cd && (maskset.validPositions[validatedPos] = $.extend(!0, {}, validTest));

        return resetMaskSet(!0), offset;
      }

      function isMask(pos, strict, fuzzy) {
        var test = getTestTemplate(pos).match;
        if ("" === test.def && (test = getTest(pos).match), !0 !== test["static"]) return test.fn;
        if (!0 === fuzzy && void 0 !== maskset.validPositions[pos] && !0 !== maskset.validPositions[pos].generatedInput) return !0;

        if (!0 !== strict && -1 < pos) {
          if (fuzzy) {
            var tests = getTests(pos);
            return tests.length > 1 + ("" === tests[tests.length - 1].match.def ? 1 : 0);
          }

          var testTemplate = determineTestTemplate(pos, getTests(pos)),
              testPlaceHolder = getPlaceholder(pos, testTemplate.match);
          return testTemplate.match.def !== testPlaceHolder;
        }

        return !1;
      }

      function seekNext(pos, newBlock, fuzzy) {
        void 0 === fuzzy && (fuzzy = !0);

        for (var position = pos + 1; "" !== getTest(position).match.def && (!0 === newBlock && (!0 !== getTest(position).match.newBlockMarker || !isMask(position, void 0, !0)) || !0 !== newBlock && !isMask(position, void 0, fuzzy));) {
          position++;
        }

        return position;
      }

      function seekPrevious(pos, newBlock) {
        var position = pos,
            tests;
        if (position <= 0) return 0;

        for (; 0 < --position && (!0 === newBlock && !0 !== getTest(position).match.newBlockMarker || !0 !== newBlock && !isMask(position, void 0, !0) && (tests = getTests(position), tests.length < 2 || 2 === tests.length && "" === tests[1].match.def));) {
          ;
        }

        return position;
      }

      function writeBuffer(input, buffer, caretPos, event, triggerEvents) {
        if (event && $.isFunction(opts.onBeforeWrite)) {
          var result = opts.onBeforeWrite.call(inputmask, event, buffer, caretPos, opts);

          if (result) {
            if (result.refreshFromBuffer) {
              var refresh = result.refreshFromBuffer;
              refreshFromBuffer(!0 === refresh ? refresh : refresh.start, refresh.end, result.buffer || buffer), buffer = getBuffer(!0);
            }

            void 0 !== caretPos && (caretPos = void 0 !== result.caret ? result.caret : caretPos);
          }
        }

        if (void 0 !== input && (input.inputmask._valueSet(buffer.join("")), void 0 === caretPos || void 0 !== event && "blur" === event.type || caret(input, caretPos, void 0, void 0, void 0 !== event && "keydown" === event.type && (event.keyCode === keyCode.DELETE || event.keyCode === keyCode.BACKSPACE)), !0 === triggerEvents)) {
          var $input = $(input),
              nptVal = input.inputmask._valueGet();

          skipInputEvent = !0, $input.trigger("input"), setTimeout(function () {
            nptVal === getBufferTemplate().join("") ? $input.trigger("cleared") : !0 === isComplete(buffer) && $input.trigger("complete");
          }, 0);
        }
      }

      function getPlaceholder(pos, test, returnPL) {
        if (test = test || getTest(pos).match, void 0 !== test.placeholder || !0 === returnPL) return $.isFunction(test.placeholder) ? test.placeholder(opts) : test.placeholder;
        if (!0 !== test["static"]) return opts.placeholder.charAt(pos % opts.placeholder.length);

        if (-1 < pos && void 0 === maskset.validPositions[pos]) {
          var tests = getTests(pos),
              staticAlternations = [],
              prevTest;
          if (tests.length > 1 + ("" === tests[tests.length - 1].match.def ? 1 : 0)) for (var i = 0; i < tests.length; i++) {
            if ("" !== tests[i].match.def && !0 !== tests[i].match.optionality && !0 !== tests[i].match.optionalQuantifier && (!0 === tests[i].match["static"] || void 0 === prevTest || !1 !== tests[i].match.fn.test(prevTest.match.def, maskset, pos, !0, opts)) && (staticAlternations.push(tests[i]), !0 === tests[i].match["static"] && (prevTest = tests[i]), 1 < staticAlternations.length && /[0-9a-bA-Z]/.test(staticAlternations[0].match.def))) return opts.placeholder.charAt(pos % opts.placeholder.length);
          }
        }

        return test.def;
      }

      function HandleNativePlaceholder(npt, value) {
        if (ie) {
          if (npt.inputmask._valueGet() !== value && (npt.placeholder !== value || "" === npt.placeholder)) {
            var buffer = getBuffer().slice(),
                nptValue = npt.inputmask._valueGet();

            if (nptValue !== value) {
              var lvp = getLastValidPosition();
              -1 === lvp && nptValue === getBufferTemplate().join("") ? buffer = [] : -1 !== lvp && clearOptionalTail(buffer), writeBuffer(npt, buffer);
            }
          }
        } else npt.placeholder !== value && (npt.placeholder = value, "" === npt.placeholder && npt.removeAttribute("placeholder"));
      }

      function determineNewCaretPosition(selectedCaret, tabbed) {
        function doRadixFocus(clickPos) {
          if ("" !== opts.radixPoint && 0 !== opts.digits) {
            var vps = maskset.validPositions;

            if (void 0 === vps[clickPos] || vps[clickPos].input === getPlaceholder(clickPos)) {
              if (clickPos < seekNext(-1)) return !0;
              var radixPos = $.inArray(opts.radixPoint, getBuffer());

              if (-1 !== radixPos) {
                for (var vp in vps) {
                  if (vps[vp] && radixPos < vp && vps[vp].input !== getPlaceholder(vp)) return !1;
                }

                return !0;
              }
            }
          }

          return !1;
        }

        if (tabbed && (isRTL ? selectedCaret.end = selectedCaret.begin : selectedCaret.begin = selectedCaret.end), selectedCaret.begin === selectedCaret.end) {
          switch (opts.positionCaretOnClick) {
            case "none":
              break;

            case "select":
              selectedCaret = {
                begin: 0,
                end: getBuffer().length
              };
              break;

            case "ignore":
              selectedCaret.end = selectedCaret.begin = seekNext(getLastValidPosition());
              break;

            case "radixFocus":
              if (doRadixFocus(selectedCaret.begin)) {
                var radixPos = getBuffer().join("").indexOf(opts.radixPoint);
                selectedCaret.end = selectedCaret.begin = opts.numericInput ? seekNext(radixPos) : radixPos;
                break;
              }

            default:
              var clickPosition = selectedCaret.begin,
                  lvclickPosition = getLastValidPosition(clickPosition, !0),
                  lastPosition = seekNext(-1 !== lvclickPosition || isMask(0) ? lvclickPosition : 0);
              if (clickPosition < lastPosition) selectedCaret.end = selectedCaret.begin = isMask(clickPosition, !0) || isMask(clickPosition - 1, !0) ? clickPosition : seekNext(clickPosition);else {
                var lvp = maskset.validPositions[lvclickPosition],
                    tt = getTestTemplate(lastPosition, lvp ? lvp.match.locator : void 0, lvp),
                    placeholder = getPlaceholder(lastPosition, tt.match);

                if ("" !== placeholder && getBuffer()[lastPosition] !== placeholder && !0 !== tt.match.optionalQuantifier && !0 !== tt.match.newBlockMarker || !isMask(lastPosition, opts.keepStatic) && tt.match.def === placeholder) {
                  var newPos = seekNext(lastPosition);
                  (newPos <= clickPosition || clickPosition === lastPosition) && (lastPosition = newPos);
                }

                selectedCaret.end = selectedCaret.begin = lastPosition;
              }
          }

          return selectedCaret;
        }
      }

      var EventRuler = {
        on: function on(input, eventName, eventHandler) {
          var ev = function ev(e) {
            e.originalEvent && (e = e.originalEvent || e, arguments[0] = e);
            var that = this,
                args;

            if (void 0 === that.inputmask && "FORM" !== this.nodeName) {
              var imOpts = $.data(that, "_inputmask_opts");
              imOpts ? new Inputmask(imOpts).mask(that) : EventRuler.off(that);
            } else {
              if ("setvalue" === e.type || "FORM" === this.nodeName || !(that.disabled || that.readOnly && !("keydown" === e.type && e.ctrlKey && 67 === e.keyCode || !1 === opts.tabThrough && e.keyCode === keyCode.TAB))) {
                switch (e.type) {
                  case "input":
                    if (!0 === skipInputEvent || e.inputType && "insertCompositionText" === e.inputType) return skipInputEvent = !1, e.preventDefault();
                    break;

                  case "keydown":
                    skipKeyPressEvent = !1, skipInputEvent = !1;
                    break;

                  case "keypress":
                    if (!0 === skipKeyPressEvent) return e.preventDefault();
                    skipKeyPressEvent = !0;
                    break;

                  case "click":
                  case "focus":
                    return validationEvent ? (validationEvent = !1, input.blur(), HandleNativePlaceholder(input, (isRTL ? getBufferTemplate().slice().reverse() : getBufferTemplate()).join("")), setTimeout(function () {
                      input.focus();
                    }, 3e3)) : (args = arguments, setTimeout(function () {
                      input.inputmask && eventHandler.apply(that, args);
                    }, 0)), !1;
                }

                var returnVal = eventHandler.apply(that, arguments);
                return !1 === returnVal && (e.preventDefault(), e.stopPropagation()), returnVal;
              }

              e.preventDefault();
            }
          };

          input.inputmask.events[eventName] = input.inputmask.events[eventName] || [], input.inputmask.events[eventName].push(ev), -1 !== $.inArray(eventName, ["submit", "reset"]) ? null !== input.form && $(input.form).on(eventName, ev) : $(input).on(eventName, ev);
        },
        off: function off(input, event) {
          var events;
          input.inputmask && input.inputmask.events && (event ? (events = [], events[event] = input.inputmask.events[event]) : events = input.inputmask.events, $.each(events, function (eventName, evArr) {
            for (; 0 < evArr.length;) {
              var ev = evArr.pop();
              -1 !== $.inArray(eventName, ["submit", "reset"]) ? null !== input.form && $(input.form).off(eventName, ev) : $(input).off(eventName, ev);
            }

            delete input.inputmask.events[eventName];
          }));
        }
      },
          EventHandlers = {
        keydownEvent: function keydownEvent(e) {
          var input = this,
              $input = $(input),
              k = e.keyCode,
              pos = caret(input),
              kdResult = opts.onKeyDown.call(this, e, getBuffer(), pos, opts);
          if (void 0 !== kdResult) return kdResult;
          if (k === keyCode.BACKSPACE || k === keyCode.DELETE || iphone && k === keyCode.BACKSPACE_SAFARI || e.ctrlKey && k === keyCode.X && !("oncut" in input)) e.preventDefault(), handleRemove(input, k, pos), writeBuffer(input, getBuffer(!0), maskset.p, e, input.inputmask._valueGet() !== getBuffer().join(""));else if (k === keyCode.END || k === keyCode.PAGE_DOWN) {
            e.preventDefault();
            var caretPos = seekNext(getLastValidPosition());
            caret(input, e.shiftKey ? pos.begin : caretPos, caretPos, !0);
          } else k === keyCode.HOME && !e.shiftKey || k === keyCode.PAGE_UP ? (e.preventDefault(), caret(input, 0, e.shiftKey ? pos.begin : 0, !0)) : (opts.undoOnEscape && k === keyCode.ESCAPE || 90 === k && e.ctrlKey) && !0 !== e.altKey ? (checkVal(input, !0, !1, undoValue.split("")), $input.trigger("click")) : !0 === opts.tabThrough && k === keyCode.TAB ? (!0 === e.shiftKey ? (!0 === getTest(pos.begin).match["static"] && (pos.begin = seekNext(pos.begin)), pos.end = seekPrevious(pos.begin, !0), pos.begin = seekPrevious(pos.end, !0)) : (pos.begin = seekNext(pos.begin, !0), pos.end = seekNext(pos.begin, !0), pos.end < maskset.maskLength && pos.end--), pos.begin < maskset.maskLength && (e.preventDefault(), caret(input, pos.begin, pos.end))) : e.shiftKey || opts.insertModeVisual && !1 === opts.insertMode && (k === keyCode.RIGHT ? setTimeout(function () {
            var caretPos = caret(input);
            caret(input, caretPos.begin);
          }, 0) : k === keyCode.LEFT && setTimeout(function () {
            var caretPos_begin = translatePosition(input.inputmask.caretPos.begin),
                caretPos_end = translatePosition(input.inputmask.caretPos.end);
            caret(input, isRTL ? caretPos_begin + (caretPos_begin === maskset.maskLength ? 0 : 1) : caretPos_begin - (0 === caretPos_begin ? 0 : 1));
          }, 0));
          ignorable = -1 !== $.inArray(k, opts.ignorables);
        },
        keypressEvent: function keypressEvent(e, checkval, writeOut, strict, ndx) {
          var input = this,
              $input = $(input),
              k = e.which || e.charCode || e.keyCode;
          if (!(!0 === checkval || e.ctrlKey && e.altKey) && (e.ctrlKey || e.metaKey || ignorable)) return k === keyCode.ENTER && undoValue !== getBuffer().join("") && (undoValue = getBuffer().join(""), setTimeout(function () {
            $input.trigger("change");
          }, 0)), skipInputEvent = !0, !0;

          if (k) {
            44 !== k && 46 !== k || 3 !== e.location || "" === opts.radixPoint || (k = opts.radixPoint.charCodeAt(0));
            var pos = checkval ? {
              begin: ndx,
              end: ndx
            } : caret(input),
                forwardPosition,
                c = String.fromCharCode(k);
            maskset.writeOutBuffer = !0;
            var valResult = isValid(pos, c, strict);

            if (!1 !== valResult && (resetMaskSet(!0), forwardPosition = void 0 !== valResult.caret ? valResult.caret : seekNext(valResult.pos.begin ? valResult.pos.begin : valResult.pos), maskset.p = forwardPosition), forwardPosition = opts.numericInput && void 0 === valResult.caret ? seekPrevious(forwardPosition) : forwardPosition, !1 !== writeOut && (setTimeout(function () {
              opts.onKeyValidation.call(input, k, valResult);
            }, 0), maskset.writeOutBuffer && !1 !== valResult)) {
              var buffer = getBuffer();
              writeBuffer(input, buffer, forwardPosition, e, !0 !== checkval);
            }

            if (e.preventDefault(), checkval) return !1 !== valResult && (valResult.forwardPosition = forwardPosition), valResult;
          }
        },
        pasteEvent: function pasteEvent(e) {
          var input = this,
              inputValue = this.inputmask._valueGet(!0),
              caretPos = caret(this),
              tempValue;

          isRTL && (tempValue = caretPos.end, caretPos.end = caretPos.begin, caretPos.begin = tempValue);
          var valueBeforeCaret = inputValue.substr(0, caretPos.begin),
              valueAfterCaret = inputValue.substr(caretPos.end, inputValue.length);
          if (valueBeforeCaret == (isRTL ? getBufferTemplate().slice().reverse() : getBufferTemplate()).slice(0, caretPos.begin).join("") && (valueBeforeCaret = ""), valueAfterCaret == (isRTL ? getBufferTemplate().slice().reverse() : getBufferTemplate()).slice(caretPos.end).join("") && (valueAfterCaret = ""), window.clipboardData && window.clipboardData.getData) inputValue = valueBeforeCaret + window.clipboardData.getData("Text") + valueAfterCaret;else {
            if (!e.clipboardData || !e.clipboardData.getData) return !0;
            inputValue = valueBeforeCaret + e.clipboardData.getData("text/plain") + valueAfterCaret;
          }
          var pasteValue = inputValue;

          if ($.isFunction(opts.onBeforePaste)) {
            if (pasteValue = opts.onBeforePaste.call(inputmask, inputValue, opts), !1 === pasteValue) return e.preventDefault();
            pasteValue = pasteValue || inputValue;
          }

          return checkVal(this, !1, !1, pasteValue.toString().split("")), writeBuffer(this, getBuffer(), seekNext(getLastValidPosition()), e, undoValue !== getBuffer().join("")), e.preventDefault();
        },
        inputFallBackEvent: function inputFallBackEvent(e) {
          function ieMobileHandler(input, inputValue, caretPos) {
            if (iemobile) {
              var inputChar = inputValue.replace(getBuffer().join(""), "");

              if (1 === inputChar.length) {
                var iv = inputValue.split("");
                iv.splice(caretPos.begin, 0, inputChar), inputValue = iv.join("");
              }
            }

            return inputValue;
          }

          function analyseChanges(inputValue, buffer, caretPos) {
            for (var frontPart = inputValue.substr(0, caretPos.begin).split(""), backPart = inputValue.substr(caretPos.begin).split(""), frontBufferPart = buffer.substr(0, caretPos.begin).split(""), backBufferPart = buffer.substr(caretPos.begin).split(""), fpl = frontPart.length >= frontBufferPart.length ? frontPart.length : frontBufferPart.length, bpl = backPart.length >= backBufferPart.length ? backPart.length : backBufferPart.length, bl, i, action = "", data = [], marker = "~", placeholder; frontPart.length < fpl;) {
              frontPart.push("~");
            }

            for (; frontBufferPart.length < fpl;) {
              frontBufferPart.push("~");
            }

            for (; backPart.length < bpl;) {
              backPart.unshift("~");
            }

            for (; backBufferPart.length < bpl;) {
              backBufferPart.unshift("~");
            }

            var newBuffer = frontPart.concat(backPart),
                oldBuffer = frontBufferPart.concat(backBufferPart);

            for (i = 0, bl = newBuffer.length; i < bl; i++) {
              switch (placeholder = getPlaceholder(translatePosition(i)), action) {
                case "insertText":
                  oldBuffer[i - 1] === newBuffer[i] && caretPos.begin == newBuffer.length - 1 && data.push(newBuffer[i]), i = bl;
                  break;

                case "insertReplacementText":
                  "~" === newBuffer[i] ? caretPos.end++ : i = bl;
                  break;

                case "deleteContentBackward":
                  "~" === newBuffer[i] ? caretPos.end++ : i = bl;
                  break;

                default:
                  newBuffer[i] !== oldBuffer[i] && ("~" !== newBuffer[i + 1] && newBuffer[i + 1] !== placeholder && void 0 !== newBuffer[i + 1] || (oldBuffer[i] !== placeholder || "~" !== oldBuffer[i + 1]) && "~" !== oldBuffer[i] ? "~" === oldBuffer[i + 1] && oldBuffer[i] === newBuffer[i + 1] ? (action = "insertText", data.push(newBuffer[i]), caretPos.begin--, caretPos.end--) : newBuffer[i] !== placeholder && "~" !== newBuffer[i] && ("~" === newBuffer[i + 1] || oldBuffer[i] !== newBuffer[i] && oldBuffer[i + 1] === newBuffer[i + 1]) ? (action = "insertReplacementText", data.push(newBuffer[i]), caretPos.begin--) : "~" === newBuffer[i] ? (action = "deleteContentBackward", !isMask(translatePosition(i), !0) && oldBuffer[i] !== opts.radixPoint || caretPos.end++) : i = bl : (action = "insertText", data.push(newBuffer[i]), caretPos.begin--, caretPos.end--));
                  break;
              }
            }

            return {
              action: action,
              data: data,
              caret: caretPos
            };
          }

          var input = this,
              inputValue = input.inputmask._valueGet(!0),
              buffer = (isRTL ? getBuffer().slice().reverse() : getBuffer()).join(""),
              caretPos = caret(input, void 0, void 0, !0);

          if (buffer !== inputValue) {
            inputValue = ieMobileHandler(input, inputValue, caretPos);
            var changes = analyseChanges(inputValue, buffer, caretPos);

            switch ((input.inputmask.shadowRoot || document).activeElement !== input && input.focus(), writeBuffer(input, getBuffer()), caret(input, caretPos.begin, caretPos.end, !0), changes.action) {
              case "insertText":
              case "insertReplacementText":
                $.each(changes.data, function (ndx, entry) {
                  var keypress = new $.Event("keypress");
                  keypress.which = entry.charCodeAt(0), ignorable = !1, EventHandlers.keypressEvent.call(input, keypress);
                }), setTimeout(function () {
                  $el.trigger("keyup");
                }, 0);
                break;

              case "deleteContentBackward":
                var keydown = new $.Event("keydown");
                keydown.keyCode = keyCode.BACKSPACE, EventHandlers.keydownEvent.call(input, keydown);
                break;

              default:
                applyInputValue(input, inputValue);
                break;
            }

            e.preventDefault();
          }
        },
        compositionendEvent: function compositionendEvent(e) {
          $el.trigger("input");
        },
        setValueEvent: function setValueEvent(e, argument_1, argument_2) {
          var input = this,
              value = e && e.detail ? e.detail[0] : argument_1;
          void 0 === value && (value = this.inputmask._valueGet(!0)), applyInputValue(this, value), (e.detail && void 0 !== e.detail[1] || void 0 !== argument_2) && caret(this, e.detail ? e.detail[1] : argument_2);
        },
        focusEvent: function focusEvent(e) {
          var input = this,
              nptValue = this.inputmask._valueGet();

          opts.showMaskOnFocus && nptValue !== getBuffer().join("") && writeBuffer(this, getBuffer(), seekNext(getLastValidPosition())), !0 !== opts.positionCaretOnTab || !1 !== mouseEnter || isComplete(getBuffer()) && -1 !== getLastValidPosition() || EventHandlers.clickEvent.apply(this, [e, !0]), undoValue = getBuffer().join("");
        },
        invalidEvent: function invalidEvent(e) {
          validationEvent = !0;
        },
        mouseleaveEvent: function mouseleaveEvent() {
          var input = this;
          mouseEnter = !1, opts.clearMaskOnLostFocus && (this.inputmask.shadowRoot || document).activeElement !== this && HandleNativePlaceholder(this, originalPlaceholder);
        },
        clickEvent: function clickEvent(e, tabbed) {
          var input = this;

          if ((this.inputmask.shadowRoot || document).activeElement === this) {
            var newCaretPosition = determineNewCaretPosition(caret(this), tabbed);
            void 0 !== newCaretPosition && caret(this, newCaretPosition);
          }
        },
        cutEvent: function cutEvent(e) {
          var input = this,
              pos = caret(this),
              clipboardData = window.clipboardData || e.clipboardData,
              clipData = isRTL ? getBuffer().slice(pos.end, pos.begin) : getBuffer().slice(pos.begin, pos.end);
          clipboardData.setData("text", isRTL ? clipData.reverse().join("") : clipData.join("")), document.execCommand && document.execCommand("copy"), handleRemove(this, keyCode.DELETE, pos), writeBuffer(this, getBuffer(), maskset.p, e, undoValue !== getBuffer().join(""));
        },
        blurEvent: function blurEvent(e) {
          var $input = $(this),
              input = this;

          if (this.inputmask) {
            HandleNativePlaceholder(this, originalPlaceholder);

            var nptValue = this.inputmask._valueGet(),
                buffer = getBuffer().slice();

            "" !== nptValue && (opts.clearMaskOnLostFocus && (-1 === getLastValidPosition() && nptValue === getBufferTemplate().join("") ? buffer = [] : clearOptionalTail(buffer)), !1 === isComplete(buffer) && (setTimeout(function () {
              $input.trigger("incomplete");
            }, 0), opts.clearIncomplete && (resetMaskSet(), buffer = opts.clearMaskOnLostFocus ? [] : getBufferTemplate().slice())), writeBuffer(this, buffer, void 0, e)), undoValue !== getBuffer().join("") && (undoValue = getBuffer().join(""), $input.trigger("change"));
          }
        },
        mouseenterEvent: function mouseenterEvent() {
          var input = this;
          mouseEnter = !0, (this.inputmask.shadowRoot || document).activeElement !== this && (null == originalPlaceholder && this.placeholder !== originalPlaceholder && (originalPlaceholder = this.placeholder), opts.showMaskOnHover && HandleNativePlaceholder(this, (isRTL ? getBufferTemplate().slice().reverse() : getBufferTemplate()).join("")));
        },
        submitEvent: function submitEvent() {
          undoValue !== getBuffer().join("") && $el.trigger("change"), opts.clearMaskOnLostFocus && -1 === getLastValidPosition() && el.inputmask._valueGet && el.inputmask._valueGet() === getBufferTemplate().join("") && el.inputmask._valueSet(""), opts.clearIncomplete && !1 === isComplete(getBuffer()) && el.inputmask._valueSet(""), opts.removeMaskOnSubmit && (el.inputmask._valueSet(el.inputmask.unmaskedvalue(), !0), setTimeout(function () {
            writeBuffer(el, getBuffer());
          }, 0));
        },
        resetEvent: function resetEvent() {
          el.inputmask.refreshValue = !0, setTimeout(function () {
            applyInputValue(el, el.inputmask._valueGet(!0));
          }, 0);
        }
      },
          valueBuffer;

      function checkVal(input, writeOut, strict, nptvl, initiatingEvent) {
        var inputmask = this || input.inputmask,
            inputValue = nptvl.slice(),
            charCodes = "",
            initialNdx = -1,
            result = void 0;

        function isTemplateMatch(ndx, charCodes) {
          for (var targetTemplate = getMaskTemplate(!0, 0).slice(ndx, seekNext(ndx)).join("").replace(/'/g, ""), charCodeNdx = targetTemplate.indexOf(charCodes); 0 < charCodeNdx && " " === targetTemplate[charCodeNdx - 1];) {
            charCodeNdx--;
          }

          var match = 0 === charCodeNdx && !isMask(ndx) && (getTest(ndx).match.nativeDef === charCodes.charAt(0) || !0 === getTest(ndx).match["static"] && getTest(ndx).match.nativeDef === "'" + charCodes.charAt(0) || " " === getTest(ndx).match.nativeDef && (getTest(ndx + 1).match.nativeDef === charCodes.charAt(0) || !0 === getTest(ndx + 1).match["static"] && getTest(ndx + 1).match.nativeDef === "'" + charCodes.charAt(0)));

          if (!match && 0 < charCodeNdx && !isMask(ndx, !1, !0)) {
            var nextPos = seekNext(ndx);
            inputmask.caretPos.begin < nextPos && (inputmask.caretPos = {
              begin: nextPos
            });
          }

          return match;
        }

        resetMaskSet(), maskset.tests = {}, initialNdx = opts.radixPoint ? determineNewCaretPosition({
          begin: 0,
          end: 0
        }).begin : 0, maskset.p = initialNdx, inputmask.caretPos = {
          begin: initialNdx
        };
        var staticMatches = [],
            prevCaretPos = inputmask.caretPos;

        if ($.each(inputValue, function (ndx, charCode) {
          if (void 0 !== charCode) if (void 0 === maskset.validPositions[ndx] && inputValue[ndx] === getPlaceholder(ndx) && isMask(ndx, !0) && !1 === isValid(ndx, inputValue[ndx], !0, void 0, void 0, !0)) maskset.p++;else {
            var keypress = new $.Event("_checkval");
            keypress.which = charCode.toString().charCodeAt(0), charCodes += charCode;
            var lvp = getLastValidPosition(void 0, !0);
            isTemplateMatch(initialNdx, charCodes) ? result = EventHandlers.keypressEvent.call(input, keypress, !0, !1, strict, lvp + 1) : (result = EventHandlers.keypressEvent.call(input, keypress, !0, !1, strict, inputmask.caretPos.begin), result && (initialNdx = inputmask.caretPos.begin + 1, charCodes = "")), result ? (void 0 !== result.pos && maskset.validPositions[result.pos] && !0 === maskset.validPositions[result.pos].match["static"] && void 0 === maskset.validPositions[result.pos].alternation && (staticMatches.push(result.pos), isRTL || (result.forwardPosition = result.pos + 1)), writeBuffer(void 0, getBuffer(), result.forwardPosition, keypress, !1), inputmask.caretPos = {
              begin: result.forwardPosition,
              end: result.forwardPosition
            }, prevCaretPos = inputmask.caretPos) : inputmask.caretPos = prevCaretPos;
          }
        }), 0 < staticMatches.length) {
          var sndx,
              validPos,
              nextValid = seekNext(-1, void 0, !1);
          if (!isComplete(getBuffer()) && staticMatches.length <= nextValid || isComplete(getBuffer()) && 0 < staticMatches.length && staticMatches.length !== nextValid && 0 === staticMatches[0]) for (var nextSndx = nextValid; void 0 !== (sndx = staticMatches.shift());) {
            var keypress = new $.Event("_checkval");
            if (validPos = maskset.validPositions[sndx], validPos.generatedInput = !0, keypress.which = validPos.input.charCodeAt(0), result = EventHandlers.keypressEvent.call(input, keypress, !0, !1, strict, nextSndx), result && void 0 !== result.pos && result.pos !== sndx && maskset.validPositions[result.pos] && !0 === maskset.validPositions[result.pos].match["static"]) staticMatches.push(result.pos);else if (!result) break;
            nextSndx++;
          } else for (; sndx = staticMatches.pop();) {
            validPos = maskset.validPositions[sndx], validPos && (validPos.generatedInput = !0);
          }
        }

        if (writeOut) for (var vndx in writeBuffer(input, getBuffer(), result ? result.forwardPosition : void 0, initiatingEvent || new $.Event("checkval"), initiatingEvent && "input" === initiatingEvent.type), maskset.validPositions) {
          !0 !== maskset.validPositions[vndx].match.generated && delete maskset.validPositions[vndx].generatedInput;
        }
      }

      function unmaskedvalue(input) {
        if (input) {
          if (void 0 === input.inputmask) return input.value;
          input.inputmask && input.inputmask.refreshValue && applyInputValue(input, input.inputmask._valueGet(!0));
        }

        var umValue = [],
            vps = maskset.validPositions;

        for (var pndx in vps) {
          vps[pndx] && vps[pndx].match && (1 != vps[pndx].match["static"] || !0 !== vps[pndx].generatedInput) && umValue.push(vps[pndx].input);
        }

        var unmaskedValue = 0 === umValue.length ? "" : (isRTL ? umValue.reverse() : umValue).join("");

        if ($.isFunction(opts.onUnMask)) {
          var bufferValue = (isRTL ? getBuffer().slice().reverse() : getBuffer()).join("");
          unmaskedValue = opts.onUnMask.call(inputmask, bufferValue, unmaskedValue, opts);
        }

        return unmaskedValue;
      }

      function translatePosition(pos) {
        return !isRTL || "number" != typeof pos || opts.greedy && "" === opts.placeholder || !el || (pos = el.inputmask._valueGet().length - pos), pos;
      }

      function caret(input, begin, end, notranslate, isDelete) {
        var range;
        if (void 0 === begin) return "selectionStart" in input && "selectionEnd" in input ? (begin = input.selectionStart, end = input.selectionEnd) : window.getSelection ? (range = window.getSelection().getRangeAt(0), range.commonAncestorContainer.parentNode !== input && range.commonAncestorContainer !== input || (begin = range.startOffset, end = range.endOffset)) : document.selection && document.selection.createRange && (range = document.selection.createRange(), begin = 0 - range.duplicate().moveStart("character", -input.inputmask._valueGet().length), end = begin + range.text.length), {
          begin: notranslate ? begin : translatePosition(begin),
          end: notranslate ? end : translatePosition(end)
        };

        if ($.isArray(begin) && (end = isRTL ? begin[0] : begin[1], begin = isRTL ? begin[1] : begin[0]), void 0 !== begin.begin && (end = isRTL ? begin.begin : begin.end, begin = isRTL ? begin.end : begin.begin), "number" == typeof begin) {
          begin = notranslate ? begin : translatePosition(begin), end = notranslate ? end : translatePosition(end), end = "number" == typeof end ? end : begin;
          var scrollCalc = parseInt(((input.ownerDocument.defaultView || window).getComputedStyle ? (input.ownerDocument.defaultView || window).getComputedStyle(input, null) : input.currentStyle).fontSize) * end;
          if (input.scrollLeft = scrollCalc > input.scrollWidth ? scrollCalc : 0, input.inputmask.caretPos = {
            begin: begin,
            end: end
          }, opts.insertModeVisual && !1 === opts.insertMode && begin === end && (isDelete || end++), input === (input.inputmask.shadowRoot || document).activeElement) if ("setSelectionRange" in input) input.setSelectionRange(begin, end);else if (window.getSelection) {
            if (range = document.createRange(), void 0 === input.firstChild || null === input.firstChild) {
              var textNode = document.createTextNode("");
              input.appendChild(textNode);
            }

            range.setStart(input.firstChild, begin < input.inputmask._valueGet().length ? begin : input.inputmask._valueGet().length), range.setEnd(input.firstChild, end < input.inputmask._valueGet().length ? end : input.inputmask._valueGet().length), range.collapse(!0);
            var sel = window.getSelection();
            sel.removeAllRanges(), sel.addRange(range);
          } else input.createTextRange && (range = input.createTextRange(), range.collapse(!0), range.moveEnd("character", end), range.moveStart("character", begin), range.select());
        }
      }

      function determineLastRequiredPosition(returnDefinition) {
        var buffer = getMaskTemplate(!0, getLastValidPosition(), !0, !0),
            bl = buffer.length,
            pos,
            lvp = getLastValidPosition(),
            positions = {},
            lvTest = maskset.validPositions[lvp],
            ndxIntlzr = void 0 !== lvTest ? lvTest.locator.slice() : void 0,
            testPos;

        for (pos = lvp + 1; pos < buffer.length; pos++) {
          testPos = getTestTemplate(pos, ndxIntlzr, pos - 1), ndxIntlzr = testPos.locator.slice(), positions[pos] = $.extend(!0, {}, testPos);
        }

        var lvTestAlt = lvTest && void 0 !== lvTest.alternation ? lvTest.locator[lvTest.alternation] : void 0;

        for (pos = bl - 1; lvp < pos && (testPos = positions[pos], (testPos.match.optionality || testPos.match.optionalQuantifier && testPos.match.newBlockMarker || lvTestAlt && (lvTestAlt !== positions[pos].locator[lvTest.alternation] && 1 != testPos.match["static"] || !0 === testPos.match["static"] && testPos.locator[lvTest.alternation] && checkAlternationMatch(testPos.locator[lvTest.alternation].toString().split(","), lvTestAlt.toString().split(",")) && "" !== getTests(pos)[0].def)) && buffer[pos] === getPlaceholder(pos, testPos.match)); pos--) {
          bl--;
        }

        return returnDefinition ? {
          l: bl,
          def: positions[bl] ? positions[bl].match : void 0
        } : bl;
      }

      function clearOptionalTail(buffer) {
        buffer.length = 0;

        for (var template = getMaskTemplate(!0, 0, !0, void 0, !0), lmnt; void 0 !== (lmnt = template.shift());) {
          buffer.push(lmnt);
        }

        return buffer;
      }

      function isComplete(buffer) {
        if ($.isFunction(opts.isComplete)) return opts.isComplete(buffer, opts);

        if ("*" !== opts.repeat) {
          var complete = !1,
              lrp = determineLastRequiredPosition(!0),
              aml = seekPrevious(lrp.l);

          if (void 0 === lrp.def || lrp.def.newBlockMarker || lrp.def.optionality || lrp.def.optionalQuantifier) {
            complete = !0;

            for (var i = 0; i <= aml; i++) {
              var test = getTestTemplate(i).match;

              if (!0 !== test["static"] && void 0 === maskset.validPositions[i] && !0 !== test.optionality && !0 !== test.optionalQuantifier || !0 === test["static"] && buffer[i] !== getPlaceholder(i, test)) {
                complete = !1;
                break;
              }
            }
          }

          return complete;
        }
      }

      function handleRemove(input, k, pos, strict, fromIsValid) {
        if ((opts.numericInput || isRTL) && (k === keyCode.BACKSPACE ? k = keyCode.DELETE : k === keyCode.DELETE && (k = keyCode.BACKSPACE), isRTL)) {
          var pend = pos.end;
          pos.end = pos.begin, pos.begin = pend;
        }

        var lvp = getLastValidPosition(void 0, !0),
            offset;

        if (pos.end >= getBuffer().length && lvp >= pos.end && (pos.end = lvp + 1), k === keyCode.BACKSPACE ? pos.end - pos.begin < 1 && (pos.begin = seekPrevious(pos.begin)) : k === keyCode.DELETE && pos.begin === pos.end && (pos.end = isMask(pos.end, !0, !0) ? pos.end + 1 : seekNext(pos.end) + 1), !1 !== (offset = revalidateMask(pos))) {
          if (!0 !== strict && !1 !== opts.keepStatic || null !== opts.regex && -1 !== getTest(pos.begin).match.def.indexOf("|")) {
            var result = alternate(!0);

            if (result) {
              var newPos = void 0 !== result.caret ? result.caret : result.pos ? seekNext(result.pos.begin ? result.pos.begin : result.pos) : getLastValidPosition(-1, !0);
              (k !== keyCode.DELETE || pos.begin > newPos) && pos.begin;
            }
          }

          !0 !== strict && (maskset.p = k === keyCode.DELETE ? pos.begin + offset : pos.begin);
        }
      }

      function applyInputValue(input, value) {
        input.inputmask.refreshValue = !1, $.isFunction(opts.onBeforeMask) && (value = opts.onBeforeMask.call(inputmask, value, opts) || value), value = value.toString().split(""), checkVal(input, !0, !1, value), undoValue = getBuffer().join(""), (opts.clearMaskOnLostFocus || opts.clearIncomplete) && input.inputmask._valueGet() === getBufferTemplate().join("") && -1 === getLastValidPosition() && input.inputmask._valueSet("");
      }

      function mask(elem) {
        function isElementTypeSupported(input, opts) {
          function patchValueProperty(npt) {
            var valueGet, valueSet;

            function patchValhook(type) {
              if ($.valHooks && (void 0 === $.valHooks[type] || !0 !== $.valHooks[type].inputmaskpatch)) {
                var valhookGet = $.valHooks[type] && $.valHooks[type].get ? $.valHooks[type].get : function (elem) {
                  return elem.value;
                },
                    valhookSet = $.valHooks[type] && $.valHooks[type].set ? $.valHooks[type].set : function (elem, value) {
                  return elem.value = value, elem;
                };
                $.valHooks[type] = {
                  get: function get(elem) {
                    if (elem.inputmask) {
                      if (elem.inputmask.opts.autoUnmask) return elem.inputmask.unmaskedvalue();
                      var result = valhookGet(elem);
                      return -1 !== getLastValidPosition(void 0, void 0, elem.inputmask.maskset.validPositions) || !0 !== opts.nullable ? result : "";
                    }

                    return valhookGet(elem);
                  },
                  set: function set(elem, value) {
                    var result = valhookSet(elem, value);
                    return elem.inputmask && applyInputValue(elem, value), result;
                  },
                  inputmaskpatch: !0
                };
              }
            }

            function getter() {
              return this.inputmask ? this.inputmask.opts.autoUnmask ? this.inputmask.unmaskedvalue() : -1 !== getLastValidPosition() || !0 !== opts.nullable ? (this.inputmask.shadowRoot || document.activeElement) === this && opts.clearMaskOnLostFocus ? (isRTL ? clearOptionalTail(getBuffer().slice()).reverse() : clearOptionalTail(getBuffer().slice())).join("") : valueGet.call(this) : "" : valueGet.call(this);
            }

            function setter(value) {
              valueSet.call(this, value), this.inputmask && applyInputValue(this, value);
            }

            function installNativeValueSetFallback(npt) {
              EventRuler.on(npt, "mouseenter", function () {
                var input = this,
                    value = this.inputmask._valueGet(!0);

                value !== (isRTL ? getBuffer().reverse() : getBuffer()).join("") && applyInputValue(this, value);
              });
            }

            if (!npt.inputmask.__valueGet) {
              if (!0 !== opts.noValuePatching) {
                if (Object.getOwnPropertyDescriptor) {
                  "function" != typeof Object.getPrototypeOf && (Object.getPrototypeOf = "object" === _typeof("test".__proto__) ? function (object) {
                    return object.__proto__;
                  } : function (object) {
                    return object.constructor.prototype;
                  });
                  var valueProperty = Object.getPrototypeOf ? Object.getOwnPropertyDescriptor(Object.getPrototypeOf(npt), "value") : void 0;
                  valueProperty && valueProperty.get && valueProperty.set ? (valueGet = valueProperty.get, valueSet = valueProperty.set, Object.defineProperty(npt, "value", {
                    get: getter,
                    set: setter,
                    configurable: !0
                  })) : "input" !== npt.tagName.toLowerCase() && (valueGet = function valueGet() {
                    return this.textContent;
                  }, valueSet = function valueSet(value) {
                    this.textContent = value;
                  }, Object.defineProperty(npt, "value", {
                    get: getter,
                    set: setter,
                    configurable: !0
                  }));
                } else document.__lookupGetter__ && npt.__lookupGetter__("value") && (valueGet = npt.__lookupGetter__("value"), valueSet = npt.__lookupSetter__("value"), npt.__defineGetter__("value", getter), npt.__defineSetter__("value", setter));

                npt.inputmask.__valueGet = valueGet, npt.inputmask.__valueSet = valueSet;
              }

              npt.inputmask._valueGet = function (overruleRTL) {
                return isRTL && !0 !== overruleRTL ? valueGet.call(this.el).split("").reverse().join("") : valueGet.call(this.el);
              }, npt.inputmask._valueSet = function (value, overruleRTL) {
                valueSet.call(this.el, null == value ? "" : !0 !== overruleRTL && isRTL ? value.split("").reverse().join("") : value);
              }, void 0 === valueGet && (valueGet = function valueGet() {
                return this.value;
              }, valueSet = function valueSet(value) {
                this.value = value;
              }, patchValhook(npt.type), installNativeValueSetFallback(npt));
            }
          }

          "textarea" !== input.tagName.toLowerCase() && opts.ignorables.push(keyCode.ENTER);
          var elementType = input.getAttribute("type"),
              isSupported = "input" === input.tagName.toLowerCase() && -1 !== $.inArray(elementType, opts.supportsInputType) || input.isContentEditable || "textarea" === input.tagName.toLowerCase();
          if (!isSupported) if ("input" === input.tagName.toLowerCase()) {
            var el = document.createElement("input");
            el.setAttribute("type", elementType), isSupported = "text" === el.type, el = null;
          } else isSupported = "partial";
          return !1 !== isSupported ? patchValueProperty(input) : input.inputmask = void 0, isSupported;
        }

        EventRuler.off(elem);
        var isSupported = isElementTypeSupported(elem, opts);

        if (!1 !== isSupported) {
          el = elem, $el = $(el), originalPlaceholder = el.placeholder, maxLength = void 0 !== el ? el.maxLength : void 0, -1 === maxLength && (maxLength = void 0), "inputMode" in el && null === el.getAttribute("inputmode") && (el.inputMode = opts.inputmode, el.setAttribute("inputmode", opts.inputmode)), !0 === isSupported && (opts.showMaskOnFocus = opts.showMaskOnFocus && -1 === ["cc-number", "cc-exp"].indexOf(el.autocomplete), iphone && (opts.insertModeVisual = !1), EventRuler.on(el, "submit", EventHandlers.submitEvent), EventRuler.on(el, "reset", EventHandlers.resetEvent), EventRuler.on(el, "blur", EventHandlers.blurEvent), EventRuler.on(el, "focus", EventHandlers.focusEvent), EventRuler.on(el, "invalid", EventHandlers.invalidEvent), EventRuler.on(el, "click", EventHandlers.clickEvent), EventRuler.on(el, "mouseleave", EventHandlers.mouseleaveEvent), EventRuler.on(el, "mouseenter", EventHandlers.mouseenterEvent), EventRuler.on(el, "paste", EventHandlers.pasteEvent), EventRuler.on(el, "cut", EventHandlers.cutEvent), EventRuler.on(el, "complete", opts.oncomplete), EventRuler.on(el, "incomplete", opts.onincomplete), EventRuler.on(el, "cleared", opts.oncleared), mobile || !0 === opts.inputEventOnly ? el.removeAttribute("maxLength") : (EventRuler.on(el, "keydown", EventHandlers.keydownEvent), EventRuler.on(el, "keypress", EventHandlers.keypressEvent)), EventRuler.on(el, "input", EventHandlers.inputFallBackEvent), EventRuler.on(el, "compositionend", EventHandlers.compositionendEvent)), EventRuler.on(el, "setvalue", EventHandlers.setValueEvent), undoValue = getBufferTemplate().join("");
          var activeElement = (el.inputmask.shadowRoot || document).activeElement;

          if ("" !== el.inputmask._valueGet(!0) || !1 === opts.clearMaskOnLostFocus || activeElement === el) {
            applyInputValue(el, el.inputmask._valueGet(!0), opts);
            var buffer = getBuffer().slice();
            !1 === isComplete(buffer) && opts.clearIncomplete && resetMaskSet(), opts.clearMaskOnLostFocus && activeElement !== el && (-1 === getLastValidPosition() ? buffer = [] : clearOptionalTail(buffer)), (!1 === opts.clearMaskOnLostFocus || opts.showMaskOnFocus && activeElement === el || "" !== el.inputmask._valueGet(!0)) && writeBuffer(el, buffer), activeElement === el && caret(el, seekNext(getLastValidPosition()));
          }
        }
      }

      if (void 0 !== actionObj) switch (actionObj.action) {
        case "isComplete":
          return el = actionObj.el, isComplete(getBuffer());

        case "unmaskedvalue":
          return void 0 !== el && void 0 === actionObj.value || (valueBuffer = actionObj.value, valueBuffer = ($.isFunction(opts.onBeforeMask) && opts.onBeforeMask.call(inputmask, valueBuffer, opts) || valueBuffer).split(""), checkVal.call(this, void 0, !1, !1, valueBuffer), $.isFunction(opts.onBeforeWrite) && opts.onBeforeWrite.call(inputmask, void 0, getBuffer(), 0, opts)), unmaskedvalue(el);

        case "mask":
          mask(el);
          break;

        case "format":
          return valueBuffer = ($.isFunction(opts.onBeforeMask) && opts.onBeforeMask.call(inputmask, actionObj.value, opts) || actionObj.value).split(""), checkVal.call(this, void 0, !0, !1, valueBuffer), actionObj.metadata ? {
            value: isRTL ? getBuffer().slice().reverse().join("") : getBuffer().join(""),
            metadata: maskScope.call(this, {
              action: "getmetadata"
            }, maskset, opts)
          } : isRTL ? getBuffer().slice().reverse().join("") : getBuffer().join("");

        case "isValid":
          actionObj.value ? (valueBuffer = ($.isFunction(opts.onBeforeMask) && opts.onBeforeMask.call(inputmask, actionObj.value, opts) || actionObj.value).split(""), checkVal.call(this, void 0, !0, !1, valueBuffer)) : actionObj.value = isRTL ? getBuffer().slice().reverse().join("") : getBuffer().join("");

          for (var buffer = getBuffer(), rl = determineLastRequiredPosition(), lmib = buffer.length - 1; rl < lmib && !isMask(lmib); lmib--) {
            ;
          }

          return buffer.splice(rl, lmib + 1 - rl), isComplete(buffer) && actionObj.value === (isRTL ? getBuffer().slice().reverse().join("") : getBuffer().join(""));

        case "getemptymask":
          return getBufferTemplate().join("");

        case "remove":
          if (el && el.inputmask) {
            $.data(el, "_inputmask_opts", null), $el = $(el);
            var cv = opts.autoUnmask ? unmaskedvalue(el) : el.inputmask._valueGet(opts.autoUnmask),
                valueProperty;
            cv !== getBufferTemplate().join("") ? el.inputmask._valueSet(cv, opts.autoUnmask) : el.inputmask._valueSet(""), EventRuler.off(el), Object.getOwnPropertyDescriptor && Object.getPrototypeOf ? (valueProperty = Object.getOwnPropertyDescriptor(Object.getPrototypeOf(el), "value"), valueProperty && el.inputmask.__valueGet && Object.defineProperty(el, "value", {
              get: el.inputmask.__valueGet,
              set: el.inputmask.__valueSet,
              configurable: !0
            })) : document.__lookupGetter__ && el.__lookupGetter__("value") && el.inputmask.__valueGet && (el.__defineGetter__("value", el.inputmask.__valueGet), el.__defineSetter__("value", el.inputmask.__valueSet)), el.inputmask = void 0;
          }

          return el;

        case "getmetadata":
          if ($.isArray(maskset.metadata)) {
            var maskTarget = getMaskTemplate(!0, 0, !1).join("");
            return $.each(maskset.metadata, function (ndx, mtdt) {
              if (mtdt.mask === maskTarget) return maskTarget = mtdt, !1;
            }), maskTarget;
          }

          return maskset.metadata;
      }
    };
  }, function (module, exports, __webpack_require__) {
    "use strict";

    function _typeof(obj) {
      return _typeof = "function" == typeof Symbol && "symbol" == _typeof2(Symbol.iterator) ? function _typeof(obj) {
        return _typeof2(obj);
      } : function _typeof(obj) {
        return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof2(obj);
      }, _typeof(obj);
    }

    var Inputmask = __webpack_require__(1),
        $ = Inputmask.dependencyLib,
        keyCode = __webpack_require__(0),
        formatCode = {
      d: ["[1-9]|[12][0-9]|3[01]", Date.prototype.setDate, "day", Date.prototype.getDate],
      dd: ["0[1-9]|[12][0-9]|3[01]", Date.prototype.setDate, "day", function () {
        return pad(Date.prototype.getDate.call(this), 2);
      }],
      ddd: [""],
      dddd: [""],
      m: ["[1-9]|1[012]", Date.prototype.setMonth, "month", function () {
        return Date.prototype.getMonth.call(this) + 1;
      }],
      mm: ["0[1-9]|1[012]", Date.prototype.setMonth, "month", function () {
        return pad(Date.prototype.getMonth.call(this) + 1, 2);
      }],
      mmm: [""],
      mmmm: [""],
      yy: ["[0-9]{2}", Date.prototype.setFullYear, "year", function () {
        return pad(Date.prototype.getFullYear.call(this), 2);
      }],
      yyyy: ["[0-9]{4}", Date.prototype.setFullYear, "year", function () {
        return pad(Date.prototype.getFullYear.call(this), 4);
      }],
      h: ["[1-9]|1[0-2]", Date.prototype.setHours, "hours", Date.prototype.getHours],
      hh: ["0[1-9]|1[0-2]", Date.prototype.setHours, "hours", function () {
        return pad(Date.prototype.getHours.call(this), 2);
      }],
      hx: [function (x) {
        return "[0-9]{".concat(x, "}");
      }, Date.prototype.setHours, "hours", function (x) {
        return Date.prototype.getHours;
      }],
      H: ["1?[0-9]|2[0-3]", Date.prototype.setHours, "hours", Date.prototype.getHours],
      HH: ["0[0-9]|1[0-9]|2[0-3]", Date.prototype.setHours, "hours", function () {
        return pad(Date.prototype.getHours.call(this), 2);
      }],
      Hx: [function (x) {
        return "[0-9]{".concat(x, "}");
      }, Date.prototype.setHours, "hours", function (x) {
        return function () {
          return pad(Date.prototype.getHours.call(this), x);
        };
      }],
      M: ["[1-5]?[0-9]", Date.prototype.setMinutes, "minutes", Date.prototype.getMinutes],
      MM: ["0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]", Date.prototype.setMinutes, "minutes", function () {
        return pad(Date.prototype.getMinutes.call(this), 2);
      }],
      s: ["[1-5]?[0-9]", Date.prototype.setSeconds, "seconds", Date.prototype.getSeconds],
      ss: ["0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]", Date.prototype.setSeconds, "seconds", function () {
        return pad(Date.prototype.getSeconds.call(this), 2);
      }],
      l: ["[0-9]{3}", Date.prototype.setMilliseconds, "milliseconds", function () {
        return pad(Date.prototype.getMilliseconds.call(this), 3);
      }],
      L: ["[0-9]{2}", Date.prototype.setMilliseconds, "milliseconds", function () {
        return pad(Date.prototype.getMilliseconds.call(this), 2);
      }],
      t: ["[ap]"],
      tt: ["[ap]m"],
      T: ["[AP]"],
      TT: ["[AP]M"],
      Z: [""],
      o: [""],
      S: [""]
    },
        formatAlias = {
      isoDate: "yyyy-mm-dd",
      isoTime: "HH:MM:ss",
      isoDateTime: "yyyy-mm-dd'T'HH:MM:ss",
      isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
    };

    function formatcode(match) {
      var dynMatches = new RegExp("\\d+$").exec(match[0]);

      if (dynMatches && void 0 !== dynMatches[0]) {
        var fcode = formatCode[match[0][0] + "x"].slice("");
        return fcode[0] = fcode[0](dynMatches[0]), fcode[3] = fcode[3](dynMatches[0]), fcode;
      }

      if (formatCode[match[0]]) return formatCode[match[0]];
    }

    function getTokenizer(opts) {
      if (!opts.tokenizer) {
        var tokens = [],
            dyntokens = [];

        for (var ndx in formatCode) {
          if (/\.*x$/.test(ndx)) {
            var dynToken = ndx[0] + "\\d+";
            -1 === dyntokens.indexOf(dynToken) && dyntokens.push(dynToken);
          } else -1 === tokens.indexOf(ndx[0]) && tokens.push(ndx[0]);
        }

        opts.tokenizer = "(" + (0 < dyntokens.length ? dyntokens.join("|") + "|" : "") + tokens.join("+|") + ")+?|.", opts.tokenizer = new RegExp(opts.tokenizer, "g");
      }

      return opts.tokenizer;
    }

    function isValidDate(dateParts, currentResult) {
      return (!isFinite(dateParts.rawday) || "29" == dateParts.day && !isFinite(dateParts.rawyear) || new Date(dateParts.date.getFullYear(), isFinite(dateParts.rawmonth) ? dateParts.month : dateParts.date.getMonth() + 1, 0).getDate() >= dateParts.day) && currentResult;
    }

    function isDateInRange(dateParts, opts) {
      var result = !0;

      if (opts.min) {
        if (dateParts.rawyear) {
          var rawYear = dateParts.rawyear.replace(/[^0-9]/g, ""),
              minYear = opts.min.year.substr(0, rawYear.length);
          result = minYear <= rawYear;
        }

        dateParts.year === dateParts.rawyear && opts.min.date.getTime() == opts.min.date.getTime() && (result = opts.min.date.getTime() <= dateParts.date.getTime());
      }

      return result && opts.max && opts.max.date.getTime() == opts.max.date.getTime() && (result = opts.max.date.getTime() >= dateParts.date.getTime()), result;
    }

    function parse(format, dateObjValue, opts, raw) {
      var mask = "",
          match,
          fcode;

      for (getTokenizer(opts).lastIndex = 0; match = getTokenizer(opts).exec(format);) {
        if (void 0 === dateObjValue) {
          if (fcode = formatcode(match)) mask += "(" + fcode[0] + ")";else switch (match[0]) {
            case "[":
              mask += "(";
              break;

            case "]":
              mask += ")?";
              break;

            default:
              mask += Inputmask.escapeRegex(match[0]);
          }
        } else if (fcode = formatcode(match)) {
          if (!0 !== raw && fcode[3]) {
            var getFn = fcode[3];
            mask += getFn.call(dateObjValue.date);
          } else fcode[2] ? mask += dateObjValue["raw" + fcode[2]] : mask += match[0];
        } else mask += match[0];
      }

      return mask;
    }

    function pad(val, len) {
      for (val = String(val), len = len || 2; val.length < len;) {
        val = "0" + val;
      }

      return val;
    }

    function analyseMask(maskString, format, opts) {
      var dateObj = {
        date: new Date(1, 0, 1)
      },
          targetProp,
          mask = maskString,
          match,
          dateOperation;

      function extendProperty(value) {
        var correctedValue = value.replace(/[^0-9]/g, "0");
        return correctedValue;
      }

      function setValue(dateObj, value, opts) {
        dateObj[targetProp] = extendProperty(value), dateObj["raw" + targetProp] = value, void 0 !== dateOperation && dateOperation.call(dateObj.date, "month" == targetProp ? parseInt(dateObj[targetProp]) - 1 : dateObj[targetProp]);
      }

      if ("string" == typeof mask) {
        for (getTokenizer(opts).lastIndex = 0; match = getTokenizer(opts).exec(format);) {
          var value = mask.slice(0, match[0].length);
          formatCode.hasOwnProperty(match[0]) && (targetProp = formatCode[match[0]][2], dateOperation = formatCode[match[0]][1], setValue(dateObj, value, opts)), mask = mask.slice(value.length);
        }

        return dateObj;
      }

      if (mask && "object" === _typeof(mask) && mask.hasOwnProperty("date")) return mask;
    }

    function importDate(dateObj, opts) {
      var match,
          date = "";

      for (getTokenizer(opts).lastIndex = 0; match = getTokenizer(opts).exec(opts.inputFormat);) {
        "d" === match[0].charAt(0) ? date += pad(dateObj.getDate(), match[0].length) : "m" === match[0].charAt(0) ? date += pad(dateObj.getMonth() + 1, match[0].length) : "yyyy" === match[0] ? date += dateObj.getFullYear().toString() : "y" === match[0].charAt(0) && (date += pad(dateObj.getYear(), match[0].length));
      }

      return date;
    }

    function getTokenMatch(pos, opts) {
      var calcPos = 0,
          targetMatch,
          match,
          matchLength = 0;

      for (getTokenizer(opts).lastIndex = 0; match = getTokenizer(opts).exec(opts.inputFormat);) {
        var dynMatches = new RegExp("\\d+$").exec(match[0]);

        if (matchLength = dynMatches ? parseInt(dynMatches[0]) : match[0].length, calcPos += matchLength, pos <= calcPos) {
          targetMatch = match, match = getTokenizer(opts).exec(opts.inputFormat);
          break;
        }
      }

      return {
        targetMatchIndex: calcPos - matchLength,
        nextMatch: match,
        targetMatch: targetMatch
      };
    }

    Inputmask.extendAliases({
      datetime: {
        mask: function mask(opts) {
          return opts.numericInput = !1, formatCode.S = opts.i18n.ordinalSuffix.join("|"), opts.inputFormat = formatAlias[opts.inputFormat] || opts.inputFormat, opts.displayFormat = formatAlias[opts.displayFormat] || opts.displayFormat || opts.inputFormat, opts.outputFormat = formatAlias[opts.outputFormat] || opts.outputFormat || opts.inputFormat, opts.placeholder = "" !== opts.placeholder ? opts.placeholder : opts.inputFormat.replace(/[[\]]/, ""), opts.regex = parse(opts.inputFormat, void 0, opts), opts.min = analyseMask(opts.min, opts.inputFormat, opts), opts.max = analyseMask(opts.max, opts.inputFormat, opts), null;
        },
        placeholder: "",
        inputFormat: "isoDateTime",
        displayFormat: void 0,
        outputFormat: void 0,
        min: null,
        max: null,
        skipOptionalPartCharacter: "",
        i18n: {
          dayNames: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
          monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
          ordinalSuffix: ["st", "nd", "rd", "th"]
        },
        preValidation: function preValidation(buffer, pos, c, isSelection, opts, maskset, caretPos, strict) {
          if (strict) return !0;

          if (isNaN(c) && buffer[pos] !== c) {
            var tokenMatch = getTokenMatch(pos, opts);

            if (tokenMatch.nextMatch && tokenMatch.nextMatch[0] === c && 1 < tokenMatch.targetMatch[0].length) {
              var validator = formatCode[tokenMatch.targetMatch[0]][0];
              if (new RegExp(validator).test("0" + buffer[pos - 1])) return buffer[pos] = buffer[pos - 1], buffer[pos - 1] = "0", {
                fuzzy: !0,
                buffer: buffer,
                refreshFromBuffer: {
                  start: pos - 1,
                  end: pos + 1
                },
                pos: pos + 1
              };
            }
          }

          return !0;
        },
        postValidation: function postValidation(buffer, pos, c, currentResult, opts, maskset, strict) {
          if (strict) return !0;
          var tokenMatch, validator;
          if (!1 === currentResult) return tokenMatch = getTokenMatch(pos + 1, opts), tokenMatch.targetMatch && tokenMatch.targetMatchIndex === pos && 1 < tokenMatch.targetMatch[0].length && void 0 !== formatCode[tokenMatch.targetMatch[0]] && (validator = formatCode[tokenMatch.targetMatch[0]][0], new RegExp(validator).test("0" + c)) ? {
            insert: [{
              pos: pos,
              c: "0"
            }, {
              pos: pos + 1,
              c: c
            }],
            pos: pos + 1
          } : currentResult;

          if (currentResult.fuzzy && (buffer = currentResult.buffer, pos = currentResult.pos), tokenMatch = getTokenMatch(pos, opts), tokenMatch.targetMatch && tokenMatch.targetMatch[0] && void 0 !== formatCode[tokenMatch.targetMatch[0]]) {
            validator = formatCode[tokenMatch.targetMatch[0]][0];
            var part = buffer.slice(tokenMatch.targetMatchIndex, tokenMatch.targetMatchIndex + tokenMatch.targetMatch[0].length);
            !1 === new RegExp(validator).test(part.join("")) && 2 === tokenMatch.targetMatch[0].length && maskset.validPositions[tokenMatch.targetMatchIndex] && maskset.validPositions[tokenMatch.targetMatchIndex + 1] && (maskset.validPositions[tokenMatch.targetMatchIndex + 1].input = "0");
          }

          var result = currentResult,
              dateParts = analyseMask(buffer.join(""), opts.inputFormat, opts);
          return result && dateParts.date.getTime() == dateParts.date.getTime() && (result = isValidDate(dateParts, result), result = result && isDateInRange(dateParts, opts)), pos && result && currentResult.pos !== pos ? {
            buffer: parse(opts.inputFormat, dateParts, opts).split(""),
            refreshFromBuffer: {
              start: pos,
              end: currentResult.pos
            }
          } : result;
        },
        onKeyDown: function onKeyDown(e, buffer, caretPos, opts) {
          var input = this;
          e.ctrlKey && e.keyCode === keyCode.RIGHT && (this.inputmask._valueSet(importDate(new Date(), opts)), $(this).trigger("setvalue"));
        },
        onUnMask: function onUnMask(maskedValue, unmaskedValue, opts) {
          return unmaskedValue ? parse(opts.outputFormat, analyseMask(maskedValue, opts.inputFormat, opts), opts, !0) : unmaskedValue;
        },
        casing: function casing(elem, test, pos, validPositions) {
          return 0 == test.nativeDef.indexOf("[ap]") ? elem.toLowerCase() : 0 == test.nativeDef.indexOf("[AP]") ? elem.toUpperCase() : elem;
        },
        onBeforeMask: function onBeforeMask(initialValue, opts) {
          return "[object Date]" === Object.prototype.toString.call(initialValue) && (initialValue = importDate(initialValue, opts)), initialValue;
        },
        insertMode: !1,
        shiftPositions: !1,
        keepStatic: !1,
        inputmode: "numeric"
      }
    }), module.exports = Inputmask;
  }, function (module, exports, __webpack_require__) {
    "use strict";

    var Inputmask = __webpack_require__(1),
        $ = Inputmask.dependencyLib,
        keyCode = __webpack_require__(0);

    function autoEscape(txt, opts) {
      for (var escapedTxt = "", i = 0; i < txt.length; i++) {
        Inputmask.prototype.definitions[txt.charAt(i)] || opts.definitions[txt.charAt(i)] || opts.optionalmarker[0] === txt.charAt(i) || opts.optionalmarker[1] === txt.charAt(i) || opts.quantifiermarker[0] === txt.charAt(i) || opts.quantifiermarker[1] === txt.charAt(i) || opts.groupmarker[0] === txt.charAt(i) || opts.groupmarker[1] === txt.charAt(i) || opts.alternatormarker === txt.charAt(i) ? escapedTxt += "\\" + txt.charAt(i) : escapedTxt += txt.charAt(i);
      }

      return escapedTxt;
    }

    function alignDigits(buffer, digits, opts, force) {
      if (0 < buffer.length && 0 < digits && (!opts.digitsOptional || force)) {
        var radixPosition = $.inArray(opts.radixPoint, buffer);
        -1 === radixPosition && (buffer.push(opts.radixPoint), radixPosition = buffer.length - 1);

        for (var i = 1; i <= digits; i++) {
          isFinite(buffer[radixPosition + i]) || (buffer[radixPosition + i] = "0");
        }
      }

      return buffer;
    }

    function findValidator(symbol, maskset) {
      var posNdx = 0;

      if ("+" === symbol) {
        for (posNdx in maskset.validPositions) {
          ;
        }

        posNdx = parseInt(posNdx);
      }

      for (var tstNdx in maskset.tests) {
        if (tstNdx = parseInt(tstNdx), posNdx <= tstNdx) for (var ndx = 0, ndxl = maskset.tests[tstNdx].length; ndx < ndxl; ndx++) {
          if ((void 0 === maskset.validPositions[tstNdx] || "-" === symbol) && maskset.tests[tstNdx][ndx].match.def === symbol) return tstNdx + (void 0 !== maskset.validPositions[tstNdx] && "-" !== symbol ? 1 : 0);
        }
      }

      return posNdx;
    }

    function findValid(symbol, maskset) {
      var ret = -1;
      return $.each(maskset.validPositions, function (ndx, tst) {
        if (tst && tst.match.def === symbol) return ret = parseInt(ndx), !1;
      }), ret;
    }

    function parseMinMaxOptions(opts) {
      void 0 === opts.parseMinMaxOptions && (null !== opts.min && (opts.min = opts.min.toString().replace(new RegExp(Inputmask.escapeRegex(opts.groupSeparator), "g"), ""), "," === opts.radixPoint && (opts.min = opts.min.replace(opts.radixPoint, ".")), opts.min = isFinite(opts.min) ? parseFloat(opts.min) : NaN, isNaN(opts.min) && (opts.min = Number.MIN_VALUE)), null !== opts.max && (opts.max = opts.max.toString().replace(new RegExp(Inputmask.escapeRegex(opts.groupSeparator), "g"), ""), "," === opts.radixPoint && (opts.max = opts.max.replace(opts.radixPoint, ".")), opts.max = isFinite(opts.max) ? parseFloat(opts.max) : NaN, isNaN(opts.max) && (opts.max = Number.MAX_VALUE)), opts.parseMinMaxOptions = "done");
    }

    function genMask(opts) {
      opts.repeat = 0, opts.groupSeparator === opts.radixPoint && opts.digits && "0" !== opts.digits && ("." === opts.radixPoint ? opts.groupSeparator = "," : "," === opts.radixPoint ? opts.groupSeparator = "." : opts.groupSeparator = ""), " " === opts.groupSeparator && (opts.skipOptionalPartCharacter = void 0), 1 < opts.placeholder.length && (opts.placeholder = opts.placeholder.charAt(0)), "radixFocus" === opts.positionCaretOnClick && "" === opts.placeholder && (opts.positionCaretOnClick = "lvp");
      var decimalDef = "0",
          radixPointDef = opts.radixPoint;
      !0 === opts.numericInput && void 0 === opts.__financeInput ? (decimalDef = "1", opts.positionCaretOnClick = "radixFocus" === opts.positionCaretOnClick ? "lvp" : opts.positionCaretOnClick, opts.digitsOptional = !1, isNaN(opts.digits) && (opts.digits = 2), opts._radixDance = !1, radixPointDef = "," === opts.radixPoint ? "?" : "!", "" !== opts.radixPoint && void 0 === opts.definitions[radixPointDef] && (opts.definitions[radixPointDef] = {}, opts.definitions[radixPointDef].validator = "[" + opts.radixPoint + "]", opts.definitions[radixPointDef].placeholder = opts.radixPoint, opts.definitions[radixPointDef]["static"] = !0, opts.definitions[radixPointDef].generated = !0)) : (opts.__financeInput = !1, opts.numericInput = !0);
      var mask = "[+]",
          altMask;

      if (mask += autoEscape(opts.prefix, opts), "" !== opts.groupSeparator ? (void 0 === opts.definitions[opts.groupSeparator] && (opts.definitions[opts.groupSeparator] = {}, opts.definitions[opts.groupSeparator].validator = "[" + opts.groupSeparator + "]", opts.definitions[opts.groupSeparator].placeholder = opts.groupSeparator, opts.definitions[opts.groupSeparator]["static"] = !0, opts.definitions[opts.groupSeparator].generated = !0), mask += opts._mask(opts)) : mask += "9{+}", void 0 !== opts.digits && 0 !== opts.digits) {
        var dq = opts.digits.toString().split(",");
        isFinite(dq[0]) && dq[1] && isFinite(dq[1]) ? mask += radixPointDef + decimalDef + "{" + opts.digits + "}" : (isNaN(opts.digits) || 0 < parseInt(opts.digits)) && (opts.digitsOptional ? (altMask = mask + radixPointDef + decimalDef + "{0," + opts.digits + "}", opts.keepStatic = !0) : mask += radixPointDef + decimalDef + "{" + opts.digits + "}");
      }

      return mask += autoEscape(opts.suffix, opts), mask += "[-]", altMask && (mask = [altMask + autoEscape(opts.suffix, opts) + "[-]", mask]), opts.greedy = !1, parseMinMaxOptions(opts), mask;
    }

    function hanndleRadixDance(pos, c, radixPos, maskset, opts) {
      return opts._radixDance && opts.numericInput && c !== opts.negationSymbol.back && pos <= radixPos && (0 < radixPos || c == opts.radixPoint) && (void 0 === maskset.validPositions[pos - 1] || maskset.validPositions[pos - 1].input !== opts.negationSymbol.back) && (pos -= 1), pos;
    }

    function decimalValidator(chrs, maskset, pos, strict, opts) {
      var radixPos = maskset.buffer ? maskset.buffer.indexOf(opts.radixPoint) : -1,
          result = -1 !== radixPos && new RegExp("[0-9\uFF11-\uFF19]").test(chrs);
      return opts._radixDance && result && null == maskset.validPositions[radixPos] ? {
        insert: {
          pos: radixPos === pos ? radixPos + 1 : radixPos,
          c: opts.radixPoint
        },
        pos: pos
      } : result;
    }

    function checkForLeadingZeroes(buffer, opts) {
      var numberMatches = new RegExp("(^" + ("" !== opts.negationSymbol.front ? Inputmask.escapeRegex(opts.negationSymbol.front) + "?" : "") + Inputmask.escapeRegex(opts.prefix) + ")(.*)(" + Inputmask.escapeRegex(opts.suffix) + ("" != opts.negationSymbol.back ? Inputmask.escapeRegex(opts.negationSymbol.back) + "?" : "") + "$)").exec(buffer.slice().reverse().join("")),
          number = numberMatches ? numberMatches[2] : "",
          leadingzeroes = !1;
      return number && (number = number.split(opts.radixPoint.charAt(0))[0], leadingzeroes = new RegExp("^[0" + opts.groupSeparator + "]*").exec(number)), !(!leadingzeroes || !(1 < leadingzeroes[0].length || 0 < leadingzeroes[0].length && leadingzeroes[0].length < number.length)) && leadingzeroes;
    }

    Inputmask.extendAliases({
      numeric: {
        mask: genMask,
        _mask: function _mask(opts) {
          return "(" + opts.groupSeparator + "999){+|1}";
        },
        digits: "*",
        digitsOptional: !0,
        enforceDigitsOnBlur: !1,
        radixPoint: ".",
        positionCaretOnClick: "radixFocus",
        _radixDance: !0,
        groupSeparator: "",
        allowMinus: !0,
        negationSymbol: {
          front: "-",
          back: ""
        },
        prefix: "",
        suffix: "",
        min: null,
        max: null,
        step: 1,
        unmaskAsNumber: !1,
        roundingFN: Math.round,
        inputmode: "numeric",
        shortcuts: {
          k: "000",
          m: "000000"
        },
        placeholder: "0",
        greedy: !1,
        rightAlign: !0,
        insertMode: !0,
        autoUnmask: !1,
        skipOptionalPartCharacter: "",
        definitions: {
          0: {
            validator: decimalValidator
          },
          1: {
            validator: decimalValidator,
            definitionSymbol: "9"
          },
          "+": {
            validator: function validator(chrs, maskset, pos, strict, opts) {
              return opts.allowMinus && ("-" === chrs || chrs === opts.negationSymbol.front);
            }
          },
          "-": {
            validator: function validator(chrs, maskset, pos, strict, opts) {
              return opts.allowMinus && chrs === opts.negationSymbol.back;
            }
          }
        },
        preValidation: function preValidation(buffer, pos, c, isSelection, opts, maskset, caretPos, strict) {
          if (!1 !== opts.__financeInput && c === opts.radixPoint) return !1;
          var pattern;

          if (pattern = opts.shortcuts && opts.shortcuts[c]) {
            if (1 < pattern.length) for (var inserts = [], i = 0; i < pattern.length; i++) {
              inserts.push({
                pos: pos + i,
                c: pattern[i],
                strict: !1
              });
            }
            return {
              insert: inserts
            };
          }

          var radixPos = $.inArray(opts.radixPoint, buffer),
              initPos = pos;

          if (pos = hanndleRadixDance(pos, c, radixPos, maskset, opts), "-" === c || c === opts.negationSymbol.front) {
            if (!0 !== opts.allowMinus) return !1;
            var isNegative = !1,
                front = findValid("+", maskset),
                back = findValid("-", maskset);
            return -1 !== front && (isNegative = [front, back]), !1 !== isNegative ? {
              remove: isNegative,
              caret: initPos
            } : {
              insert: [{
                pos: findValidator("+", maskset),
                c: opts.negationSymbol.front,
                fromIsValid: !0
              }, {
                pos: findValidator("-", maskset),
                c: opts.negationSymbol.back,
                fromIsValid: void 0
              }],
              caret: initPos + opts.negationSymbol.back.length
            };
          }

          if (strict) return !0;
          if (-1 !== radixPos && !0 === opts._radixDance && !1 === isSelection && c === opts.radixPoint && void 0 !== opts.digits && (isNaN(opts.digits) || 0 < parseInt(opts.digits)) && radixPos !== pos) return {
            caret: opts._radixDance && pos === radixPos - 1 ? radixPos + 1 : radixPos
          };
          if (!1 === opts.__financeInput) if (isSelection) {
            if (opts.digitsOptional) return {
              rewritePosition: caretPos.end
            };

            if (!opts.digitsOptional) {
              if (caretPos.begin > radixPos && caretPos.end <= radixPos) return c === opts.radixPoint ? {
                insert: {
                  pos: radixPos + 1,
                  c: "0",
                  fromIsValid: !0
                },
                rewritePosition: radixPos
              } : {
                rewritePosition: radixPos + 1
              };
              if (caretPos.begin < radixPos) return {
                rewritePosition: caretPos.begin - 1
              };
            }
          } else if (!opts.showMaskOnHover && !opts.showMaskOnFocus && !opts.digitsOptional && 0 < opts.digits && "" === this.inputmask.__valueGet.call(this)) return {
            rewritePosition: radixPos
          };
          return {
            rewritePosition: pos
          };
        },
        postValidation: function postValidation(buffer, pos, c, currentResult, opts, maskset, strict) {
          if (!1 === currentResult) return currentResult;
          if (strict) return !0;

          if (null !== opts.min || null !== opts.max) {
            var unmasked = opts.onUnMask(buffer.slice().reverse().join(""), void 0, $.extend({}, opts, {
              unmaskAsNumber: !0
            }));
            if (null !== opts.min && unmasked < opts.min && (unmasked.toString().length >= opts.min.toString().length || unmasked < 0)) return !1;
            if (null !== opts.max && unmasked > opts.max) return !1;
          }

          return currentResult;
        },
        onUnMask: function onUnMask(maskedValue, unmaskedValue, opts) {
          if ("" === unmaskedValue && !0 === opts.nullable) return unmaskedValue;
          var processValue = maskedValue.replace(opts.prefix, "");
          return processValue = processValue.replace(opts.suffix, ""), processValue = processValue.replace(new RegExp(Inputmask.escapeRegex(opts.groupSeparator), "g"), ""), "" !== opts.placeholder.charAt(0) && (processValue = processValue.replace(new RegExp(opts.placeholder.charAt(0), "g"), "0")), opts.unmaskAsNumber ? ("" !== opts.radixPoint && -1 !== processValue.indexOf(opts.radixPoint) && (processValue = processValue.replace(Inputmask.escapeRegex.call(this, opts.radixPoint), ".")), processValue = processValue.replace(new RegExp("^" + Inputmask.escapeRegex(opts.negationSymbol.front)), "-"), processValue = processValue.replace(new RegExp(Inputmask.escapeRegex(opts.negationSymbol.back) + "$"), ""), Number(processValue)) : processValue;
        },
        isComplete: function isComplete(buffer, opts) {
          var maskedValue = (opts.numericInput ? buffer.slice().reverse() : buffer).join("");
          return maskedValue = maskedValue.replace(new RegExp("^" + Inputmask.escapeRegex(opts.negationSymbol.front)), "-"), maskedValue = maskedValue.replace(new RegExp(Inputmask.escapeRegex(opts.negationSymbol.back) + "$"), ""), maskedValue = maskedValue.replace(opts.prefix, ""), maskedValue = maskedValue.replace(opts.suffix, ""), maskedValue = maskedValue.replace(new RegExp(Inputmask.escapeRegex(opts.groupSeparator) + "([0-9]{3})", "g"), "$1"), "," === opts.radixPoint && (maskedValue = maskedValue.replace(Inputmask.escapeRegex(opts.radixPoint), ".")), isFinite(maskedValue);
        },
        onBeforeMask: function onBeforeMask(initialValue, opts) {
          var radixPoint = opts.radixPoint || ",";
          isFinite(opts.digits) && (opts.digits = parseInt(opts.digits)), "number" != typeof initialValue && "number" !== opts.inputType || "" === radixPoint || (initialValue = initialValue.toString().replace(".", radixPoint));
          var valueParts = initialValue.split(radixPoint),
              integerPart = valueParts[0].replace(/[^\-0-9]/g, ""),
              decimalPart = 1 < valueParts.length ? valueParts[1].replace(/[^0-9]/g, "") : "",
              forceDigits = 1 < valueParts.length;
          initialValue = integerPart + ("" !== decimalPart ? radixPoint + decimalPart : decimalPart);
          var digits = 0;

          if ("" !== radixPoint && (digits = opts.digitsOptional ? opts.digits < decimalPart.length ? opts.digits : decimalPart.length : opts.digits, "" !== decimalPart || !opts.digitsOptional)) {
            var digitsFactor = Math.pow(10, digits || 1);
            initialValue = initialValue.replace(Inputmask.escapeRegex(radixPoint), "."), isNaN(parseFloat(initialValue)) || (initialValue = (opts.roundingFN(parseFloat(initialValue) * digitsFactor) / digitsFactor).toFixed(digits)), initialValue = initialValue.toString().replace(".", radixPoint);
          }

          if (0 === opts.digits && -1 !== initialValue.indexOf(radixPoint) && (initialValue = initialValue.substring(0, initialValue.indexOf(radixPoint))), null !== opts.min || null !== opts.max) {
            var numberValue = initialValue.toString().replace(radixPoint, ".");
            null !== opts.min && numberValue < opts.min ? initialValue = opts.min.toString().replace(".", radixPoint) : null !== opts.max && numberValue > opts.max && (initialValue = opts.max.toString().replace(".", radixPoint));
          }

          return alignDigits(initialValue.toString().split(""), digits, opts, forceDigits).join("");
        },
        onBeforeWrite: function onBeforeWrite(e, buffer, caretPos, opts) {
          function stripBuffer(buffer, stripRadix) {
            if (!1 !== opts.__financeInput || stripRadix) {
              var position = $.inArray(opts.radixPoint, buffer);
              -1 !== position && buffer.splice(position, 1);
            }

            if ("" !== opts.groupSeparator) for (; -1 !== (position = buffer.indexOf(opts.groupSeparator));) {
              buffer.splice(position, 1);
            }
            return buffer;
          }

          var result,
              leadingzeroes = checkForLeadingZeroes(buffer, opts);

          if (leadingzeroes) {
            var buf = buffer.slice().reverse(),
                caretNdx = buf.join("").indexOf(leadingzeroes[0]);
            buf.splice(caretNdx, leadingzeroes[0].length);
            var newCaretPos = buf.length - caretNdx;
            stripBuffer(buf), result = {
              refreshFromBuffer: !0,
              buffer: buf.reverse(),
              caret: caretPos < newCaretPos ? caretPos : newCaretPos
            };
          }

          if (e) switch (e.type) {
            case "blur":
            case "checkval":
              if (null !== opts.min) {
                var unmasked = opts.onUnMask(buffer.slice().reverse().join(""), void 0, $.extend({}, opts, {
                  unmaskAsNumber: !0
                }));
                if (null !== opts.min && unmasked < opts.min) return {
                  refreshFromBuffer: !0,
                  buffer: alignDigits(opts.min.toString().replace(".", opts.radixPoint).split(""), opts.digits, opts).reverse()
                };
              }

              if (buffer[buffer.length - 1] === opts.negationSymbol.front) {
                var nmbrMtchs = new RegExp("(^" + ("" != opts.negationSymbol.front ? Inputmask.escapeRegex(opts.negationSymbol.front) + "?" : "") + Inputmask.escapeRegex(opts.prefix) + ")(.*)(" + Inputmask.escapeRegex(opts.suffix) + ("" != opts.negationSymbol.back ? Inputmask.escapeRegex(opts.negationSymbol.back) + "?" : "") + "$)").exec(stripBuffer(buffer.slice(), !0).reverse().join("")),
                    number = nmbrMtchs ? nmbrMtchs[2] : "";
                0 == number && (result = {
                  refreshFromBuffer: !0,
                  buffer: [0]
                });
              } else "" !== opts.radixPoint && buffer[0] === opts.radixPoint && (result && result.buffer ? result.buffer.shift() : (buffer.shift(), result = {
                refreshFromBuffer: !0,
                buffer: stripBuffer(buffer)
              }));

              if (opts.enforceDigitsOnBlur) {
                result = result || {};
                var bffr = result && result.buffer || buffer.slice().reverse();
                result.refreshFromBuffer = !0, result.buffer = alignDigits(bffr, opts.digits, opts, !0).reverse();
              }

          }
          return result;
        },
        onKeyDown: function onKeyDown(e, buffer, caretPos, opts) {
          var $input = $(this),
              bffr;
          if (e.ctrlKey) switch (e.keyCode) {
            case keyCode.UP:
              return this.inputmask.__valueSet.call(this, parseFloat(this.inputmask.unmaskedvalue()) + parseInt(opts.step)), $input.trigger("setvalue"), !1;

            case keyCode.DOWN:
              return this.inputmask.__valueSet.call(this, parseFloat(this.inputmask.unmaskedvalue()) - parseInt(opts.step)), $input.trigger("setvalue"), !1;
          }

          if (!e.shiftKey && (e.keyCode === keyCode.DELETE || e.keyCode === keyCode.BACKSPACE || e.keyCode === keyCode.BACKSPACE_SAFARI) && caretPos.begin !== buffer.length) {
            if (buffer[e.keyCode === keyCode.DELETE ? caretPos.begin - 1 : caretPos.end] === opts.negationSymbol.front) return bffr = buffer.slice().reverse(), "" !== opts.negationSymbol.front && bffr.shift(), "" !== opts.negationSymbol.back && bffr.pop(), $input.trigger("setvalue", [bffr.join(""), caretPos.begin]), !1;

            if (!0 === opts._radixDance) {
              var radixPos = $.inArray(opts.radixPoint, buffer);

              if (opts.digitsOptional) {
                if (0 === radixPos) return bffr = buffer.slice().reverse(), bffr.pop(), $input.trigger("setvalue", [bffr.join(""), caretPos.begin >= bffr.length ? bffr.length : caretPos.begin]), !1;
              } else if (-1 !== radixPos && (caretPos.begin < radixPos || caretPos.end < radixPos || e.keyCode === keyCode.DELETE && caretPos.begin === radixPos)) return caretPos.begin !== caretPos.end || e.keyCode !== keyCode.BACKSPACE && e.keyCode !== keyCode.BACKSPACE_SAFARI || caretPos.begin++, bffr = buffer.slice().reverse(), bffr.splice(bffr.length - caretPos.begin, caretPos.begin - caretPos.end + 1), bffr = alignDigits(bffr, opts.digits, opts).join(""), $input.trigger("setvalue", [bffr, caretPos.begin >= bffr.length ? radixPos + 1 : caretPos.begin]), !1;
            }
          }
        }
      },
      currency: {
        prefix: "",
        groupSeparator: ",",
        alias: "numeric",
        digits: 2,
        digitsOptional: !1
      },
      decimal: {
        alias: "numeric"
      },
      integer: {
        alias: "numeric",
        digits: 0
      },
      percentage: {
        alias: "numeric",
        min: 0,
        max: 100,
        suffix: " %",
        digits: 0,
        allowMinus: !1
      },
      indianns: {
        alias: "numeric",
        _mask: function _mask(opts) {
          return "(" + opts.groupSeparator + "99){*|1}(" + opts.groupSeparator + "999){1|1}";
        },
        groupSeparator: ",",
        radixPoint: ".",
        placeholder: "0",
        digits: 2,
        digitsOptional: !1
      }
    }), module.exports = Inputmask;
  }, function (module, exports, __webpack_require__) {
    "use strict";

    var _inputmask = _interopRequireDefault(__webpack_require__(1));

    function _typeof(obj) {
      return _typeof = "function" == typeof Symbol && "symbol" == _typeof2(Symbol.iterator) ? function _typeof(obj) {
        return _typeof2(obj);
      } : function _typeof(obj) {
        return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof2(obj);
      }, _typeof(obj);
    }

    function _classCallCheck(instance, Constructor) {
      if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
    }

    function _possibleConstructorReturn(self, call) {
      return !call || "object" !== _typeof(call) && "function" != typeof call ? _assertThisInitialized(self) : call;
    }

    function _assertThisInitialized(self) {
      if (void 0 === self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
      return self;
    }

    function _inherits(subClass, superClass) {
      if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function");
      subClass.prototype = Object.create(superClass && superClass.prototype, {
        constructor: {
          value: subClass,
          writable: !0,
          configurable: !0
        }
      }), superClass && _setPrototypeOf(subClass, superClass);
    }

    function _wrapNativeSuper(Class) {
      var _cache = "function" == typeof Map ? new Map() : void 0;

      return _wrapNativeSuper = function _wrapNativeSuper(Class) {
        if (null === Class || !_isNativeFunction(Class)) return Class;
        if ("function" != typeof Class) throw new TypeError("Super expression must either be null or a function");

        if ("undefined" != typeof _cache) {
          if (_cache.has(Class)) return _cache.get(Class);

          _cache.set(Class, Wrapper);
        }

        function Wrapper() {
          return _construct(Class, arguments, _getPrototypeOf(this).constructor);
        }

        return Wrapper.prototype = Object.create(Class.prototype, {
          constructor: {
            value: Wrapper,
            enumerable: !1,
            writable: !0,
            configurable: !0
          }
        }), _setPrototypeOf(Wrapper, Class);
      }, _wrapNativeSuper(Class);
    }

    function isNativeReflectConstruct() {
      if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
      if (Reflect.construct.sham) return !1;
      if ("function" == typeof Proxy) return !0;

      try {
        return Date.prototype.toString.call(Reflect.construct(Date, [], function () {})), !0;
      } catch (e) {
        return !1;
      }
    }

    function _construct(Parent, args, Class) {
      return _construct = isNativeReflectConstruct() ? Reflect.construct : function _construct(Parent, args, Class) {
        var a = [null];
        a.push.apply(a, args);
        var Constructor = Function.bind.apply(Parent, a),
            instance = new Constructor();
        return Class && _setPrototypeOf(instance, Class.prototype), instance;
      }, _construct.apply(null, arguments);
    }

    function _isNativeFunction(fn) {
      return -1 !== Function.toString.call(fn).indexOf("[native code]");
    }

    function _setPrototypeOf(o, p) {
      return _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
        return o.__proto__ = p, o;
      }, _setPrototypeOf(o, p);
    }

    function _getPrototypeOf(o) {
      return _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
        return o.__proto__ || Object.getPrototypeOf(o);
      }, _getPrototypeOf(o);
    }

    function _interopRequireDefault(obj) {
      return obj && obj.__esModule ? obj : {
        "default": obj
      };
    }

    if (document.head.createShadowRoot || document.head.attachShadow) {
      var InputmaskElement = function (_HTMLElement) {
        function InputmaskElement() {
          var _this;

          _classCallCheck(this, InputmaskElement), _this = _possibleConstructorReturn(this, _getPrototypeOf(InputmaskElement).call(this));

          var attributeNames = _this.getAttributeNames(),
              shadow = _this.attachShadow({
            mode: "closed"
          }),
              input = document.createElement("input");

          for (var attr in input.type = "text", shadow.appendChild(input), attributeNames) {
            Object.prototype.hasOwnProperty.call(attributeNames, attr) && input.setAttribute("data-inputmask-" + attributeNames[attr], _this.getAttribute(attributeNames[attr]));
          }

          return new _inputmask["default"]().mask(input), input.inputmask.shadowRoot = shadow, _this;
        }

        return _inherits(InputmaskElement, _HTMLElement), InputmaskElement;
      }(_wrapNativeSuper(HTMLElement));

      customElements.define("input-mask", InputmaskElement);
    }
  }], installedModules = {}, __webpack_require__.m = modules, __webpack_require__.c = installedModules, __webpack_require__.d = function (exports, name, getter) {
    __webpack_require__.o(exports, name) || Object.defineProperty(exports, name, {
      enumerable: !0,
      get: getter
    });
  }, __webpack_require__.r = function (exports) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(exports, Symbol.toStringTag, {
      value: "Module"
    }), Object.defineProperty(exports, "__esModule", {
      value: !0
    });
  }, __webpack_require__.t = function (value, mode) {
    if (1 & mode && (value = __webpack_require__(value)), 8 & mode) return value;
    if (4 & mode && "object" == _typeof2(value) && value && value.__esModule) return value;
    var ns = Object.create(null);
    if (__webpack_require__.r(ns), Object.defineProperty(ns, "default", {
      enumerable: !0,
      value: value
    }), 2 & mode && "string" != typeof value) for (var key in value) {
      __webpack_require__.d(ns, key, function (key) {
        return value[key];
      }.bind(null, key));
    }
    return ns;
  }, __webpack_require__.n = function (module) {
    var getter = module && module.__esModule ? function getDefault() {
      return module["default"];
    } : function getModuleExports() {
      return module;
    };
    return __webpack_require__.d(getter, "a", getter), getter;
  }, __webpack_require__.o = function (object, property) {
    return Object.prototype.hasOwnProperty.call(object, property);
  }, __webpack_require__.p = "", __webpack_require__(__webpack_require__.s = 5);

  function __webpack_require__(moduleId) {
    if (installedModules[moduleId]) return installedModules[moduleId].exports;
    var module = installedModules[moduleId] = {
      i: moduleId,
      l: !1,
      exports: {}
    };
    return modules[moduleId].call(module.exports, module, module.exports, __webpack_require__), module.l = !0, module.exports;
  }

  var modules, installedModules;
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../webpack/buildin/module.js */ "./node_modules/webpack/buildin/module.js")(module)))

/***/ }),

/***/ "./node_modules/inputmask/index.js":
/*!*****************************************!*\
  !*** ./node_modules/inputmask/index.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./dist/inputmask */ "./node_modules/inputmask/dist/inputmask.js");

/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var g; // This works in non-strict mode

g = function () {
  return this;
}();

try {
  // This works if eval is allowed (see CSP)
  g = g || new Function("return this")();
} catch (e) {
  // This works if the window reference is available
  if ((typeof window === "undefined" ? "undefined" : _typeof(window)) === "object") g = window;
} // g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}


module.exports = g;

/***/ }),

/***/ "./node_modules/webpack/buildin/module.js":
/*!***********************************!*\
  !*** (webpack)/buildin/module.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function (module) {
  if (!module.webpackPolyfill) {
    module.deprecate = function () {};

    module.paths = []; // module.parent = undefined by default

    if (!module.children) module.children = [];
    Object.defineProperty(module, "loaded", {
      enumerable: true,
      get: function get() {
        return module.l;
      }
    });
    Object.defineProperty(module, "id", {
      enumerable: true,
      get: function get() {
        return module.i;
      }
    });
    module.webpackPolyfill = 1;
  }

  return module;
};

/***/ })

/******/ });
//# sourceMappingURL=checkout.js.map