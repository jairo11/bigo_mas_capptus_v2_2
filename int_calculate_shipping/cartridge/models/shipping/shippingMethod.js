"use strict";

var base = module.superModule;

var shippingCostHelper = require("*/cartridge/scripts/helpers/shippingCostHelper");

/**
 * Plain JS object that represents a DW Script API dw.order.ShippingMethod object
 * @param {dw.order.ShippingMethod} shippingMethod - the default shipment of the current basket
 * @param {dw.order.Shipment} [shipment] - a Shipment
 */
function ShippingMethodModel(shippingMethod, shipment) {
    // Initialize the base model prior
    base.call(this, shippingMethod, shipment);

    if (shipment && session.custom.containerView && session.custom.containerView == "basket") {
        shippingCostHelper.applyShippingCostData(this, shippingMethod, shipment);
    }
}

module.exports = ShippingMethodModel;
