"use strict";

var base = module.superModule;

var shippingCostHelper = require("*/cartridge/scripts/helpers/shippingCostHelper");

/**
 * Plain JS object that represents a DW Script API dw.order.ShippingMethod object
 * @param {dw.order.ShippingMethod} shippingMethod - the default shipment of the current basket
 * @param {dw.order.Shipment} [shipment] - a Shipment
 */
function ShippingModel(shipment, address, customer, containerView) {
    // Initialize the base model prior
    base.call(this, shipment, address, customer, containerView);
    session.custom.containerView = containerView;
}

module.exports = ShippingModel;
