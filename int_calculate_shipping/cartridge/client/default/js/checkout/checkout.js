"use strict";

var billingHelpers = require("./billing");
var shippingHelpers = require("./shipping");
var base = require("@refapp/js/checkout/checkout");
var formatters = require("@refapp/js/formatters");

[billingHelpers, shippingHelpers].forEach(function (library) {
    Object.keys(library).forEach(function (item) {
        if (typeof library[item] === "object") {
            base[item] = $.extend({}, base[item], library[item]);
        } else {
            base[item] = library[item];
        }
    });
});

base.formatters = formatters.init;

module.exports = base;
