"use strict";

var base = require("@refapp/js/checkout/shipping");

base.updateShippingList = function () {
    var baseObj = this;

    $("select[name$=\"shippingAddress_addressFields_states_stateCode\"]")
        .on("change", function (e) {
            console.info("updateShippingList - select --->");
            if (baseObj.methods && baseObj.methods.updateShippingMethodList) {
                baseObj.methods.updateShippingMethodList($(e.currentTarget.form));
            } else {
                updateShippingMethodList($(e.currentTarget.form));
            }
        });
    $("input[name$=\"shippingAddress_addressFields_city\"]")
        .on("blur", function (e) {
            console.info("updateShippingList - city --->");
            if (baseObj.methods && baseObj.methods.updateShippingMethodList) {
                baseObj.methods.updateShippingMethodList($(e.currentTarget.form));
            } else {
                updateShippingMethodList($(e.currentTarget.form));
            }
        });
    $("input[name$=\"shippingAddress_addressFields_postalCode\"]")
        .on("blur", function (e) {
            console.info("updateShippingList - postalCode --->");
            if (baseObj.methods && baseObj.methods.updateShippingMethodList) {
                baseObj.methods.updateShippingMethodList($(e.currentTarget.form));
            } else {
                updateShippingMethodList($(e.currentTarget.form));
            }
        });
}

module.exports = base;
