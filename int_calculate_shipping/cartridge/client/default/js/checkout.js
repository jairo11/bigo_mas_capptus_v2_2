"use strict";

var processInclude = require("@refapp/js/util");

$(document).ready(function () {
    processInclude(require("./checkout/checkout"));
    processInclude(require("./checkout/mercadoPago"));
});
