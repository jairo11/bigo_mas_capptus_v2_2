"use strict";

/* API Includes */
var Logger = require("dw/system/Logger");
var ShippingLineItem = require("dw/order/ShippingLineItem");
var ShippingMgr = require("dw/order/ShippingMgr");
var Status = require("dw/system/Status");

/* Local Includes */
var collections = require("*/cartridge/scripts/util/collections");
var shippingCostHelper = require("*/cartridge/scripts/helpers/shippingCostHelper");

function updateEligibleShipment(shipment) {
    if (!shipment || !shipment.shippingAddress) return false;

    if (!shipment.shippingMethod || !shipment.shippingMethod.ID) return false;

    if (!shippingCostHelper.isCalculatedShippingCostMethod(shipment.shippingMethod)) return false;

    return true;
}

function validShippingCost(shippingCost) {
    var valid = shippingCost;

    if (!valid) {
        Logger.warn("Shipping cost unavailable for the selected shipping method.");
    }

    return valid;
}

function updateShippingLineItem(shippingLineItem, shippingCost) {
    shippingLineItem.setPriceValue(shippingCost);
}

function updateShipment(shipment) {
    if (updateEligibleShipment(shipment)) {
        var lineItemContainer = this;
        var shippingCost = shippingCostHelper.getShippingCost(shipment.shippingMethod, shipment, shipment.shippingAddress, lineItemContainer.customer);
        var shippingLineItem = shipment.getShippingLineItem(ShippingLineItem.STANDARD_SHIPPING_ID);
        if (validShippingCost(shippingCost)) {
            updateShippingLineItem(shippingLineItem, shippingCost);
        }
    }
}

function applyCalculatedShippingCost(lineItemContainer) {
    collections.forEach(lineItemContainer.shipments, updateShipment, lineItemContainer);
}

function calculateShipping(lineItemContainer) {
    ShippingMgr.applyShippingCost(lineItemContainer);
    applyCalculatedShippingCost(lineItemContainer);
    return new Status(Status.OK);
}

exports.calculateShipping = calculateShipping;
