"use strict";

/* API Includes */
var Bytes = require("dw/util/Bytes");
var CacheMgr = require("dw/system/CacheMgr");
var MessageDigest = require("dw/crypto/MessageDigest");

function getCacheKey(keyObject) {
    var string = JSON.stringify(keyObject);
    var bytes = new Bytes(string);

    return new MessageDigest(MessageDigest.DIGEST_SHA_256).digest(bytes);
}

function getCustomCache() {
    return CacheMgr.getCache("ShippingCostInfo");
}

function putCache(keyObject, valueObject) {
    var cache = getCustomCache();
    var key = getCacheKey(keyObject);

    cache.put(key, valueObject);
}

function invalidateCache(keyObject) {
    var cache = getCustomCache();
    var key = getCacheKey(keyObject);

    cache.invalidate(key);
}

function getCache(keyObject, loaderFunction) {
    var cache = getCustomCache();
    var key = getCacheKey(keyObject);
    var callback = (typeof loaderFunction === "function") ? loaderFunction : null;

    return cache.get(key, callback);
}

module.exports = {
    putCache: putCache,
    invalidateCache: invalidateCache,
    getCache: getCache
};
