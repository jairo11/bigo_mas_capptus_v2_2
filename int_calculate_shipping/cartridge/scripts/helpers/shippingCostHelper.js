"use strict";

var Logger = require("dw/system/Logger");

var requestHelper = require("*/cartridge/scripts/helpers/requestHelper");
var cacheHelper = require("*/cartridge/scripts/helpers/cacheHelper");

var hookMgr = require("dw/system/HookMgr");

function isCalculatedShippingCostMethod(shippingMethod) {
    return shippingMethod.custom.calculateShippingCost === true;
}

function isOrderPlaced(shipment) {
    return shipment.shipmentNo !== null;
}

function hasStoredShippingCostInfo(shipment) {
    return shipment.custom.calculatedShippingCostCache !== null;
}

function setStoredCalculatedShippingCost(shipment, response) {
    try {
        // eslint-disable-next-line no-param-reassign
        if (Object.keys(response).length) shipment.custom.calculatedShippingCostCache = JSON.stringify(response);
    } catch (e) {
        Logger.error("Stringifying response for setStoredCalculatedShippingCost: {0}", e);
    }
}

function getStoredShippingCostInfo(shipment) {
    var shippingCostInfo;
    var shippingCostInfoString = shipment.custom.calculatedShippingCostCache;

    try {
        shippingCostInfo = JSON.parse(shippingCostInfoString);
    } catch (e) {
        Logger.error("Error parsing shipping cost info in getStoredShippingCostInfo : {0}", e);
    }

    return shippingCostInfo;
}

function getAPIBrokerDilivery(requestPayload) {
    var response;
    if (hookMgr.hasHook("app.services.cya.brokerDelivery")) {
        response = hookMgr.callHook("app.services.cya.brokerDelivery", "getShippingCost", requestPayload);
    }
    return response;
}

function getShippingCostInfo(shipment, address, customer) {
    var shippingCostInfo;
    var orderPlaced = isOrderPlaced(shipment);

    if (orderPlaced && hasStoredShippingCostInfo(shipment)) {
        shippingCostInfo = getStoredShippingCostInfo(shipment);
    }

    if (!orderPlaced) {
        var shippingAddress = address;
        if (!shippingAddress && shipment && shipment.shippingAddress) {
            shippingAddress = shipment.shippingAddress;
        }

        if (shippingAddress) {
            try {
                var requestPayload = requestHelper.getShippingRequestPayload(shipment, shippingAddress, customer, "venta");

                shippingCostInfo = cacheHelper.getCache(requestPayload, function () {
                    if (Object.hasOwnProperty.call(requestPayload, "shipmentID")) {
                        delete requestPayload.shipmentID;
                    }
                    var response = getAPIBrokerDilivery(requestPayload);
                    if (response) {
                        setStoredCalculatedShippingCost(shipment, response);
                    }
                    return response;
                });
            } catch (e) {
                Logger.error("Error retrieving shippingCostInfo from BrokerDelivery: {0}", e);
            }
        }
    }

    return shippingCostInfo || {};
}

function getCalculatedShippingCost(shippingMethod, shipment, address, customer) {
    var shippingCost;
    if (isCalculatedShippingCostMethod(shippingMethod)) {
        var shippingCostInfo = getShippingCostInfo(shipment, address, customer);
        if (Object.hasOwnProperty.call(shippingCostInfo, "priceId")) shippingCost = shippingCostInfo;
    }

    return shippingCost;
}

function returnToCart(shippingMethod, shipment, address, customer) {
    if (isCalculatedShippingCostMethod(shippingMethod)) {
        var shippingCost = getShippingCostInfo(shipment, address, customer);
        if (Object.hasOwnProperty.call(shippingCost, "priceId")) {
            return false;
        } else {
            return true;
        }

    } else {
        return false;
    }
}

function getShippingCost(shippingMethod, shipment, address, customer) {
    var shippingCostInfo = getCalculatedShippingCost(shippingMethod, shipment, address, customer);

    return shippingCostInfo ? shippingCostInfo.shippingPrice : null;
}

function formatShippingCost(value, currencyCode) {
    var formatCurrency = require("*/cartridge/scripts/util/formatting").formatCurrency;

    return formatCurrency(value, currencyCode);
}

function applyShippingCostData(shippingMethodModel, shippingMethod, shipment) {
    var calculatedShippingCost = getCalculatedShippingCost(shippingMethod, shipment, shipment.shippingAddress, customer);

    var shqShippingMethod = shippingMethodModel;

    if (calculatedShippingCost) {
        shqShippingMethod.shippingCost = formatShippingCost(calculatedShippingCost.shippingPrice, "MXN");
    }
}

module.exports = {
    applyShippingCostData: applyShippingCostData,
    getShippingCost: getShippingCost,
    isOrderPlaced: isOrderPlaced,
    isCalculatedShippingCostMethod: isCalculatedShippingCostMethod,
    returnToCart : returnToCart
};
