'use strict';

var FileReader = require('dw/io/FileReader');
var XMLStreamReader = require('dw/io/XMLStreamReader');
var Status = require('dw/system/Status');
var File = require('dw/io/File');
var Logger = require('dw/system/Logger');
var OrderMgr = require('dw/order/OrderMgr');
var Order = require('dw/order/Order');
var Transaction = require('dw/system/Transaction');
var WebDAVUtils = require('~/cartridge/scripts/util/WebDAVUtils');
var XMLStreamConstants = require('dw/io/XMLStreamConstants');
/**
 * Update using a XML file stored on webdav in Sites/Impex/src/ folder.
 */
module.exports.execute = function(params) {
    var folder = File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + params.folder;
    var filePattern = params.filePattern;
    var afterImportOption = params.afterImportOption;

    Logger.info("Order Import - Getting local file");
    var files = WebDAVUtils.getFilesInLocalFolder(folder, filePattern);
    if (empty(files)) {
        return new Status(Status.OK, 'NO_FILES', 'No files to import in folder ' + folder);
    }

    var errorsCount = 0;

    files.toArray().forEach(function(file) {
        try {
            Logger.info("Order Import - Working with file " + file.name);
            var order = null;
            var shipments = null;
            var shipmentIndex = 0;
            var fileReader = new FileReader(file, "UTF-8");
            var xmlStreamReader = new XMLStreamReader(fileReader);
            var cancelDescription = '';
            while (xmlStreamReader.hasNext()) {
                Transaction.begin();
                xmlStreamReader.next();
                if (xmlStreamReader.getEventType() === XMLStreamConstants.START_ELEMENT) {
                    if (xmlStreamReader.getLocalName() === "order") {
                        var orderNumber = xmlStreamReader.getAttributeValue(null, "order-no");
                        if (!empty(orderNumber)) {
                            order = OrderMgr.getOrder(orderNumber);
                            shipmentIndex = 0;
                            shipments = order.getShipments();
                        }
                        if (!order) {
                            Logger.error("Couldn't find order with order number ", orderNumber);
                            return new Status(Status.ERROR, 'ERROR', 'Could not find order ' + orderNumber);
                        }
                    } else if (!empty(order) && xmlStreamReader.getLocalName() === "cancel-description") {
                        cancelDescription = xmlStreamReader.getElementText();
                    } else if (!empty(order) && xmlStreamReader.getLocalName() === "shipping-status") {
                        var shippingStatus =  xmlStreamReader.getElementText();
                        order.setShippingStatus(getShippingStatusValue(shippingStatus));
                    } else if (!empty(order) && xmlStreamReader.getLocalName() === "order-status") {
                        var orderStatus =  xmlStreamReader.getElementText();
                        if(getOrderStatusValue(orderStatus) === Order.ORDER_STATUS_CANCELLED){
                            var orderCancelationResult = OrderMgr.cancelOrder(order);
                            if (orderCancelationResult.status === Status.OK) {
                                order.setCancelCode(Order.ORDER_STATUS_CANCELLED);
                                order.setCancelDescription(cancelDescription);
                                cancelDescription = '';
                            } else {
                                Transaction.rollback();
                                return renderJSON({
                                    success: false,
                                    errorMsg: orderCancelationResult.message
                                });
                            }
                        }else{
                            order.setStatus(getOrderStatusValue(orderStatus));
                        }
                    } else if (!empty(order) && !empty(shipments) && xmlStreamReader.getLocalName() === "shipment") {
                        var shipmentObject = xmlStreamReader.readXMLObject();
                        var ns = shipmentObject.namespace();
                        var shipment = shipments[shipmentIndex++];

                        var shippingStatusXML = shipmentObject.ns::status;
                        var trackingNumber = '';
                        var customAttributes = shipmentObject.ns::["custom-attributes"]
                        var guideNo = '';
                        var shippingCompany = '';
                        if(customAttributes){
                            var elements =customAttributes.elements();
                            for (var i = 0; i < elements.length(); i++) {
                                if (elements[i].localName() == "custom-attribute" && elements[i].attributes("xml:lang") == "guideNo") {
                                    guideNo = elements[i].toString();
                                }else if (elements[i].localName() == "custom-attribute" && elements[i].attributes("xml:lang") == "shippingCompany")
                                    shippingCompany = elements[i].toString();
                            }
                        }
                        var shippingStatusString = shippingStatusXML.ns::["shipping-status"].toString();

                        if (!empty(shippingStatusString)) {
                            shipment.setShippingStatus(getShippingStatusValue(shippingStatusString));
                        }

                        if (!empty(guideNo)) {
                            shipment.setTrackingNumber(guideNo);
                            shipment.custom.guideNo = guideNo;
                        }

                        if (!empty(shippingCompany)) {
                            shipment.custom.shippingCompany = shippingCompany;
                        }
                    }
                }

                Transaction.commit();

            }
            if(order){
                changeStatusEmail(order);
            }

        } catch (e) {
            Logger.error("Order Import - " + e.toString());
            Transaction.rollback();
            errorsCount++;
        } finally {
            xmlStreamReader.close();
            fileReader.close();
        }

        WebDAVUtils.processFile(afterImportOption, file, folder);
    });

    if (errorsCount > 0) {
        return new Status(Status.ERROR, 'ERROR', 'Errors during file(s) processing');
    }

    return new Status(Status.OK, 'OK', 'Orders import has been successfully finished');
}

function getShippingStatusValue(status) {
    switch(status) {
        case "NOT_SHIPPED":
            return Order.SHIPPING_STATUS_NOTSHIPPED;
        case "PART_SHIPPED":
            return Order.SHIPPING_STATUS_PARTSHIPPED;
        case "SHIPPED":
            return Order.SHIPPING_STATUS_SHIPPED;
        default:
            return '';
    }
}

function getOrderStatusValue(status) {
    switch(status) {
        case "CREATED":
            return Order.ORDER_STATUS_CREATED;
        case "NEW":
            return Order.ORDER_STATUS_NEW;
        case "OPEN":
            return Order.ORDER_STATUS_OPEN;
        case "CANCELLED":
            return Order.ORDER_STATUS_CANCELLED;
        case "COMPLETED":
            return Order.ORDER_STATUS_COMPLETED;
        case "REPLACED":
            return Order.ORDER_STATUS_REPLACED;
        case "FAILED":
            return Order.ORDER_STATUS_FAILED;
        default:
            return '';
    }
}

function changeStatusEmail(order){
    var Resource = require('dw/web/Resource');
    var Site = require('dw/system/Site');
    var OrderModel = require('*/cartridge/models/order');
    var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');
    var Locale = require('dw/util/Locale');
    var currentLocale = Locale.getLocale('es_MX');
    var templateType = '';
    var orderModel = new OrderModel(order, { countryCode: currentLocale.country, containerView: 'order' });
    var orderObject = { order: orderModel };
    var emailObj = {
        to: order.customerEmail,
        subject: Resource.msg('subject.order.confirmation.email', 'order', null),
        from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@testorganization.com',
        type: emailHelpers.emailTypes.orderConfirmation
    };

    if(order.getStatus().getValue() === Order.ORDER_STATUS_COMPLETED && order.getShippingStatus().getValue() === Order.SHIPPING_STATUS_SHIPPED){
        templateType = 'email/deliveredOrderEmail';
        emailObj.subject = Resource.msgf('subject.order.in.arrived', 'order', null); // change resource message properties
        emailHelpers.sendEmail(emailObj, templateType, orderObject);
    }else if (order.getStatus().getValue() === Order.ORDER_STATUS_CANCELLED){
        templateType = 'email/canceledOrderEmail';
        emailObj.subject = Resource.msgf('subject.cancelled.order.title', 'order', null, orderModel.orderNumber); // change resource message properties
        emailHelpers.sendEmail(emailObj, templateType, orderObject);
    }else if (order.getStatus().getValue() !== Order.ORDER_STATUS_COMPLETED && order.getShippingStatus().getValue() === Order.SHIPPING_STATUS_PARTSHIPPED){
        templateType = 'email/processedOrderEmail';
        emailObj.subject = Resource.msgf('subject.order.processed.email', 'order', null, orderModel.orderNumber); // change resource
        emailHelpers.sendEmail(emailObj, templateType, orderObject);
    }else if (order.getStatus().getValue() !== Order.ORDER_STATUS_COMPLETED && order.getShippingStatus().getValue() === Order.SHIPPING_STATUS_SHIPPED){
        templateType = 'email/onRouteOrderEmail';
        emailObj.subject = Resource.msgf('subject.order.in.process', 'order', null, orderModel.orderNumber); // change resource message properties
        emailHelpers.sendEmail(emailObj, templateType, orderObject);
    }
}
