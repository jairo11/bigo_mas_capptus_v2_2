"use strict";

var Status = require("dw/system/Status");
var File = require("dw/io/File");
var Logger = require("dw/system/Logger");
var Pipeline = require("dw/system/Pipeline");

var WebDAVUtils = require("~/cartridge/scripts/util/WebDAVUtils");


/**
 * Update using a XML file stored on webdav in Sites/Impex/src/ folder.
 */
module.exports.execute = function (params) {
    var invalidFilesCount = 0;

    var rootPath = File.IMPEX + File.SEPARATOR + "src" + File.SEPARATOR;
    var folder = rootPath + params.folderPath;
    var filePattern = params.filePattern;

    var files = WebDAVUtils.getFilesInLocalFolder(folder, filePattern);
    if (empty(files)) {
        return new Status(Status.OK, "NO_FILES", "No files to validate in folder " + folder);
    }

    var fileType = params.fileType;
    var notValidFileProcessOption = params.notValidFileProcessOption;

    for (var i in files) {
        var file = files[i];
        var fileValidation = validateXMLFile(file, fileType, rootPath);
        if (fileValidation.Status.error) {
            var statusDetails = fileValidation.Status.details;
            invalidFilesCount++;
            Logger.error("File {0} is not valid. Error: {1}; ErrorsCount: {2}; Log file: {3}", file.name, statusDetails.error, statusDetails.ValidationErrorCount, statusDetails.LogFileName);

            WebDAVUtils.processFile(notValidFileProcessOption, file, folder);
        }
    }

    if (invalidFilesCount > 0) {
        return new Status(Status.ERROR, "ERROR", "Invalid files found in folder " + folder);
    }
};

function validateXMLFile(file, fileType, rootPath) {
    var filePath = file.fullPath.slice(rootPath.length);

    return Pipeline.execute("PipeletWrapper-ValidateXMLFile", {
        File : filePath,
        Schema : getXMLSchema(fileType)
    });
}

function getXMLSchema(fileType) {
    switch (fileType) {
        case "Catalog":
            return "catalog.xsd";
        case "Pricebook":
            return "pricebook.xsd";
        case "Stores":
            return "store.xsd";
        case "Inventory":
            return "inventory.xsd";
        case "Customers":
            return "customer.xsd";
        default:
            return "xml.xsd";
    }
}
