"use strict";

module.exports.execute = function () {
    var accountHelpers = require("*/cartridge/scripts/helpers/accountHelpers");
    dw.customer.CustomerMgr.processProfiles(function (profile) {
        accountHelpers.sendPasswordResetEmail(profile.email, profile.customer, "account/password/requestPasswordResetEmail");
    }, "custom.changePasswordRequired = true AND email != NULL");
};
