"use strict";

var File = require("dw/io/File");
var Calendar = require("dw/util/Calendar");

exports.processFile = function (action, file, folder) {
    // delete source file
    if (action === "DELETE") {
        file.remove();
    // archive source file
    } else if (action === "ARCHIVE") {
        var archiveFolder = new File(folder + File.SEPARATOR + "archive");
        if (!archiveFolder.exists()) {
            archiveFolder.mkdirs();
        }
        var archiveFile = new File(archiveFolder.fullPath + File.SEPARATOR + file.name);
        file.renameTo(archiveFile);
    // zip and archive source file
    } else if (action === "ZIP") {
        var zipArchiveFolder = new File(folder + File.SEPARATOR + "archive");
        if (!zipArchiveFolder.exists()) {
            zipArchiveFolder.mkdirs();
        }
        var archiveZipFile = new File(zipArchiveFolder.fullPath + File.SEPARATOR + file.name + ".zip");
        file.zip(archiveZipFile);
        file.remove();
    }
};

exports.getFilesInLocalFolder = function (path, filePattern) {
    var directory = new File(path);
    if (!directory.exists()) {
        return null;
    }
    var regExp = new RegExp(filePattern);
    var files = directory.listFiles(function (file) {
        return file.isFile() && regExp.test(file.name);
    });
    return sortByFiledate(files);
};

function sortByFiledate(files) {
    files.sort(function (a, b) {
        var aFileDate = a.name.replace(".xml", "").split("_").splice(-1).join(" ");
        var bFileDate = b.name.replace(".xml", "").split("_").splice(-1).join(" ");
        var aDate = new Calendar();
        var bDate = new Calendar();
        aDate.parseByFormat(aFileDate, "yyyyMMddHHmmss");
        bDate.parseByFormat(bFileDate, "yyyyMMddHHmmss");

        return aDate.compareTo(bDate);
    });
    return files;
}
