"use strict";

var backInStockHelper = require("app_refapp/cartridge/scripts/features/backInStockHelpers");
var CustomObjectMgr = require("dw/object/CustomObjectMgr");
var ProductMgr = require("dw/catalog/ProductMgr");
var Logger = require("dw/system/Logger");

module.exports.execute = function () {
    var notifications = CustomObjectMgr.getAllCustomObjects("BackInStockNotifications");
    var notification, product, emails;

    while (notifications.hasNext()) {
        try {
            notification = notifications.next();

            product = ProductMgr.getProduct(notification.custom.productID);

            if (product && product.availabilityModel && product.availabilityModel.inStock) {
                emails = notification.custom.emails.split(",");

                emails.forEach(function (email) {
                    backInStockHelper.sendBackInStockEmailNotification(email, product.ID);
                });
            }
        } catch (error) {
            Logger.error("[BackInStockNotification.js] script crashed on line {0}, notifications for product - {1}. ERROR: {2}", error.lineNumber, notification.custom.productID, error.message);
        }
    }
};
