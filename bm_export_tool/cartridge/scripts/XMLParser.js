/**
* Description of the Controller and the logic it provides
*
* @module  controllers/XMLParser
*/

"use strict";

var FileReader = require("dw/io/FileReader");
var XMLStreamReader = require("dw/io/XMLStreamReader");
var XMLStreamConstants = require("dw/io/XMLStreamConstants");
var Logger = require("dw/system/Logger").getLogger("ExportTool", "contentExport");

module.exports = {
    /**
    * Gets xml content object by ID and writes it to file
    *
    * @param {Object} params - httpParameterMap
    * @param {Object} XMLObject - XML Object for current loop iteration
    * @param {Array} contentRequested - array of requested IDs/Names/RegExps for asset/folder search
    * @param {Array} contentFound - array of objects with content IDs/Names with attributes that were not found
    * @param {Object} writer - XMLStreamWriter for export file
    */
    getAssetByID : function (exportSettings, XMLObject, contentFound, writer) {
        var XMLString = XMLObject.toXMLString();
        var assetID = XMLObject.attribute("content-id").toString();
        var contentRequested = exportSettings.contentRequested.map(function (object) { return object.designation; });
        if (contentRequested.indexOf(assetID) !== -1) {
            var asset = {
                designation: assetID,
                attributesNotFound: []
            };
            var attributesRequested = exportSettings.contentRequested[contentRequested.indexOf(assetID)].attributes;
            if (!empty(attributesRequested)) {
                var result = this.setCustomAttributes(attributesRequested, XMLObject);
                XMLString = result.XMLObject.toXMLString();
                asset.attributesNotFound = result.attributesNotFound;
            }
            writer.writeRaw(XMLString);
            contentFound.push(asset);
        }
    },
    /**
    * Gets xml content object by Name and writes it to file
    *
    * @param {Object} params - httpParameterMap
    * @param {Object} XMLObject - XML Object for current loop iteration
    * @param {Array} contentRequested - array of requested IDs/Names/RegExps for asset/folder search
    * @param {Array} contentFound - array of objects with content IDs/Names with attributes that were not found
    * @param {Object} writer - XMLStreamWriter for export file
    */
    getAssetByName : function (exportSettings, XMLObject, contentFound, writer) {
        var XMLString = XMLObject.toXMLString();
        var elements = XMLObject.elements();
        var contentRequested = exportSettings.contentRequested.map(function (object) { return object.designation; });
        for (var i = 0; i < contentRequested.length; i++) {
            for (var j = 0; j < elements.length(); j++) {
                if (elements[j].localName() == "display-name" &&
                    elements[j].toString() == contentRequested[i] &&
                    elements[j].attributes("xml:lang") == "x-default") {
                    var assetName = elements[j].toString();
                    var assetIndex = contentRequested.indexOf(assetName);
                    var asset = {
                        designation: elements[j].toString(),
                        attributesNotFound: []
                    };
                    var attributesRequested = exportSettings.contentRequested[assetIndex].attributes;
                    if (!empty(attributesRequested)) {
                        var result = this.setCustomAttributes(attributesRequested, XMLObject);
                        XMLString = result.XMLObject.toXMLString();
                        asset.attributesNotFound = result.attributesNotFound;
                    }
                    writer.writeRaw(XMLString);
                    contentFound.push(asset);
                }
            }
        }
    },
    /**
    * Gets xml content object by regular expression and writes it to file
    *
    * @param {Object} params - httpParameterMap
    * @param {Object} XMLObject - XML Object for current loop iteration
    * @param {Array} contentRequested - array of requested IDs/Names/RegExps for asset/folder search
    * @param {Array} contentFound - array of objects with content IDs/Names with attributes that were not found
    * @param {Object} writer - XMLStreamWriter for export file
    */
    getAssetByRegExp : function (exportSettings, XMLObject, contentFound, writer) {
        var match;
        var XMLString = XMLObject.toXMLString();
        var self = this;
        var contentRequested = exportSettings.contentRequested.map(function (object) { return object.designation; });
        var assetID = XMLObject.attribute("content-id").toString();
        contentRequested.forEach(function (item, index) {
            match = XMLString.match(new RegExp("content-id=\""+ item, "gi"));
            if (match && XMLObject.localName() == "content") {
                var asset = {
                    designation: assetID,
                    attributesNotFound: []
                };
                var attributesRequested = exportSettings.contentRequested[index].attributes;
                if (!empty(attributesRequested)) {
                    var result = self.setCustomAttributes(attributesRequested, XMLObject);
                    XMLString = result.XMLObject.toXMLString();
                    asset.attributesNotFound = result.attributesNotFound;
                }
                writer.writeRaw(XMLString);
                contentFound.push(asset);
            }
        });
    },
    /**
    * Gets xml folder object by ID and writes it to file in case of uploaded source library
    *
    * @param {Object} params - httpParameterMap
    * @param {Object} XMLObject - XML Object for current loop iteration
    * @param {Array} contentRequested - array of requested IDs/Names/RegExps for asset/folder search
    * @param {Array} contentFound - array of objects with content IDs/Names with attributes that were not found
    * @param {Array} folderIDs - array of folder IDs to export
    * @param {Object} library - current library used for XML processing, stored in session
    * @param {Object} writer - XMLStreamWriter for export file
    */
    getFolderByID : function (exportSettings, XMLObject, contentFound, folderIDs, library, writer) {
        var folderID = XMLObject.attribute("folder-id").toString();
        var XMLString = XMLObject.toXMLString();
        var contentRequested = exportSettings.contentRequested.map(function (object) { return object.designation; });
        if (contentRequested.indexOf(folderID) !== -1) {
            var folder = {
                ID: folderID,
                designation: folderID,
                attributesNotFound: []
            };
            var attributesRequested = exportSettings.contentRequested[contentRequested.indexOf(folderID)].attributes;
            if (!empty(attributesRequested)) {
                var result = this.setCustomAttributes(attributesRequested, XMLObject);
                XMLString = result.XMLObject.toXMLString();
                folder.attributesFound = result.attributesFound;
                folder.attributesNotFound = result.attributesNotFound;
            }
            contentFound.push(folder);
            folderIDs.push(folderID);
            if (library.isUploadedLibrary) {
                writer.writeRaw(XMLString);
            }
        }
    },
    /**
    * Gets xml folder object by Name and writes it to file in case of uploaded source library
    *
    * @param {Object} params - httpParameterMap
    * @param {Object} XMLObject - XML Object for current loop iteration
    * @param {Array} contentRequested - array of requested IDs/Names/RegExps for asset/folder search
    * @param {Array} contentFound - array of objects with content IDs/Names with attributes that were not found
    * @param {Array} folderIDs - array of folder IDs to export
    * @param {Object} library - current library used for XML processing, stored in session
    * @param {Object} writer - XMLStreamWriter for export file
    */
    getFolderByName : function (exportSettings, XMLObject, contentFound, folderIDs, library, writer) {
        var folderID = XMLObject.attribute("folder-id").toString();
        var XMLString = XMLObject.toXMLString();
        var elements = XMLObject.elements();
        var contentRequested = exportSettings.contentRequested.map(function (object) { return object.designation; });
        for (var i = 0; i < contentRequested.length; i++) {
            for (var j = 0; j < elements.length(); j++) {
                if (elements[j].localName() == "display-name" &&
                    elements[j].toString() == contentRequested[i] &&
                    elements[j].attributes("xml:lang") == "x-default" &&
                    XMLObject.localName() == "folder") {
                    var folderName = elements[j].toString();
                    var folderIndex = contentRequested.indexOf(folderName);
                    var folder = {
                        ID: folderID,
                        designation: folderName,
                        attributesNotFound: []
                    };
                    var attributesRequested = exportSettings.contentRequested[folderIndex].attributes;
                    if (!empty(attributesRequested)) {
                        var result = this.setCustomAttributes(attributesRequested, XMLObject);
                        XMLString = result.XMLObject.toXMLString();
                        folder.attributesFound = result.attributesFound;
                        folder.attributesNotFound = result.attributesNotFound;
                    }
                    folderIDs.push(folderID);
                    contentFound.push(folder);
                    if (library.isUploadedLibrary) {
                        writer.writeRaw(XMLString);
                    }
                }
            }
        }
    },
    /**
    * Gets xml folder object by regular expression and writes it to file in case of uploaded source library
    *
    * @param {Object} params - httpParameterMap
    * @param {Object} XMLObject - XML Object for current loop iteration
    * @param {Array} contentRequested - array of requested IDs/Names/RegExps for asset/folder search
    * @param {Array} contentFound - array of objects with content IDs/Names with attributes that were not found
    * @param {Array} folderIDs - array of folder IDs to export
    * @param {Object} library - current library used for XML processing, stored in session
    * @param {Object} writer - XMLStreamWriter for export file
    */
    getFolderByRegExp : function (exportSettings, XMLObject, contentFound, folderIDs, library, writer) {
        var folderID = XMLObject.attribute("folder-id").toString();
        var XMLString = XMLObject.toXMLString();
        var self = this;
        var match;
        var contentRequested = exportSettings.contentRequested.map(function (object) { return object.designation; });
        contentRequested.forEach(function (item, index) {
            match = XMLString.match(new RegExp("folder-id=\""+ item, "gi"));
            if (match && XMLObject.localName() == "folder") {
                var folder = {
                    ID: folderID,
                    designation: folderID,
                    attributesNotFound: []
                };
                var attributesRequested = exportSettings.contentRequested[index].attributes;
                if (!empty(attributesRequested)) {
                    var result = self.setCustomAttributes(attributesRequested, XMLObject);
                    XMLString = result.XMLObject.toXMLString();
                    folder.attributesFound = result.attributesFound;
                    folder.attributesNotFound = result.attributesNotFound;
                }
                folderIDs.push(folderID);
                contentFound.push(folder);
                if (library.isUploadedLibrary) {
                    writer.writeRaw(XMLString);
                }
            }
        });
    },
    /**
    * Gets xml subfolders of requested folder and writes it to file in case of uploaded source library
    *
    * @param {Object} params - httpParameterMap
    * @param {Object} XMLObject - XML Object for current loop iteration
    * @param {Array} contentRequested - array of requested IDs/Names/RegExps for asset/folder search
    * @param {Array} contentFound - array of objects with content IDs/Names with attributes that were not found
    * @param {Array} folderIDs - array of folder IDs to export
    * @param {Object} library - current library used for XML processing, stored in session
    * @param {Object} writer - XMLStreamWriter for export file
    */
    getFolderSubFolders : function (exportSettings, XMLObject, contentFound, folderIDs, library, writer) {
        var folderID = XMLObject.attribute("folder-id").toString();
        var XMLString = XMLObject.toXMLString();
        var elements = XMLObject.elements();
        for (var i = 0; i < folderIDs.length; i++) {
            for (var j = 0; j < elements.length(); j++) {
                if (elements[j].localName() == "parent" &&
                     elements[j].toString() == folderIDs[i] &&
                     XMLObject.localName() == "folder") {
                    var folderDesigantion = exportSettings.folderSearchMode == "byName" ? elements[0].toString() : folderID;
                    var folder = {
                        ID: folderID,
                        designation: folderDesigantion,
                        attributesNotFound: []
                    };
                    var attributesRequested = [];
                    if (exportSettings.exportType_UI !== TOOL_CONSTANTS.MANUAL_UI) {
                        attributesRequested = exportSettings.contentRequested[contentRequested.indexOf(folderID)].attributes;
                    } else {
                        attributesRequested = exportSettings.contentRequested[contentRequested.indexOf(0)].attributes;
                    }
                    if (!empty(attributesRequested)) {
                        var result = this.setCustomAttributes(attributesRequested, XMLObject);
                        XMLString = result.XMLObject.toXMLString();
                        folder.attributesFound = result.attributesFound;
                        folder.attributesNotFound = result.attributesNotFound;
                    }
                    if (library.isUploadedLibrary) {
                        writer.writeRaw(XMLString);
                    }
                    folderIDs.push(folderID);
                    contentFound.push(folder);
                }
            }
        }
    },
    /**
    * Finds and sets requested attributes to the XML object if they exist
    *
    * @param {String} attributesRequested - string of attributes provided by user separated by coma
    * @param {Object} XMLObject - XML Object for current loop iteration
    *
    * @return {Object} - return object that contains {Object} XMLObject with attributes set and {Array} attributesNotFound with attributes that were not found
    */
    setCustomAttributes: function (attributesRequested, XMLObject) {
        var attributesFound = [];
        var attributesNotFound = [];
        var XMLAttributes = new XML();
        var elements = XMLObject.elements();
        for (var i = 0; i < attributesRequested.length; i++) {
            for (var j = 0; j < elements.length(); j++) {
                if (elements[j].localName() == attributesRequested[i]) {
                    XMLAttributes += elements[j];
                    attributesFound.push(elements[j].localName());
                }
            }
        }
        for (var i = 0; i < attributesRequested.length; i++) {
            if (attributesFound.indexOf(attributesRequested[i]) == -1) {
                attributesNotFound.push(attributesRequested[i]);
            }
        }
        return {
            XMLObject : XMLObject.setChildren(XMLAttributes),
            attributesFound: attributesFound,
            attributesNotFound: attributesNotFound
        };
    },
    /**
     * Finds static content file paths in provided xml
     *
     * @param {Object} file - Exported xml file
     *
     * @return {Array} - array of static content file paths
     */
    getStaticContentPaths: function (file) {
        var fileReader = new FileReader(file, "UTF-8");
        var xmlStreamReader = new XMLStreamReader(fileReader);
        var filePaths = [];
        var regex = new RegExp(/([a-zA-Z0-9\/\.\_\-]+?)\?\$staticlink\$/gm);
        while (xmlStreamReader.hasNext()) {
            if (xmlStreamReader.next() == XMLStreamConstants.START_ELEMENT) {
                try {
                    var localName = xmlStreamReader.getLocalName();
                    if (localName == "folder" || localName == "content") {
                        var XMLObject = xmlStreamReader.readXMLObject();
                        var elements = XMLObject.elements();
                        for (var i = 0; i < elements.length(); i++) {
                            var name = elements[i].localName();
                            if (name == "custom-attributes") {
                                var elementsCA = elements.elements();
                                for (var j = 0; j < elementsCA.length(); j++) {
                                    var attrText = elementsCA[j].text().toString();
                                    var matches = attrText.match(regex);
                                    matches.forEach(function (filePath) {
                                        filePath = filePath.replace("?$staticlink$", "");
                                        filePaths.push(filePath);
                                    });
                                }
                            }
                        }
                    }
                } catch (error) {
                    Logger.error("[Export Tool :: getStaticContentPaths] An error occured: {0} In file: {1}, at line: {2}", error.message, error.fileName, error.lineNumber);
                }
            }
        }
        return filePaths;
    }
};
