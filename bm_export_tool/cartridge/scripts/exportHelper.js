"use strict";

var StringUtils = require("dw/util/StringUtils");
var Calendar = require("dw/util/Calendar");
var timeFormat = "'D'yyyy-MM-dd'T'hh:mm:ss";
var File = require("dw/io/File");
var httpsHostName = dw.system.Site.getCurrent().getHttpsHostName();
var siteLink = "https://" + httpsHostName + "/on/demandware.servlet/webdav/Sites";

module.exports = {
    /**
    * Returns formatted name with timestamp and file extension.
    *
    * @param {String} fileName - file name provided by user
    *
    * @return {String} - formatted name
    */
    formatName: function (fileName) {
        var timestamp = StringUtils.formatCalendar(new Calendar(), timeFormat);
        if (fileName.indexOf(".xml") !== -1) {
            fileName = fileName.replace(".xml", "");
        }
        return fileName + "-" + timestamp + ".xml";
    },
    /**
     * Uploads file and puts it to the session
     *
     * @return {Object} response - JSON result of execution
     */
    uploadFile: function (type, workingFolder) {
        var parameterMap = request.httpParameterMap;
        var self = this;
        var fileMap = parameterMap.processMultipart(function (field, contentType, fileName) {
            if (fileName == null || fileName == "") {
                return null;
            }
            return new File(workingFolder, self.formatName(fileName));
        });
        session.privacy[type] = {
            file: fileMap.uploadFile,
            isUploaded: true
        };
        var url = siteLink + fileMap.uploadFile.getFullPath();
        return {
            type: type + "-upload",
            success: true,
            exportMsg: "File uploaded.",
            url: url
        };
    },
    getAssetFromArrayByID: function (array, ID) {
        return array[array.map(function (asset) { return asset.designation; }).indexOf(ID)];
    },
    /**
     * Recursive function to delete a local folder and all of its children files and folders
     *
     * @param {Object} folder
     */
    deleteFolder: function (folder) {
        while (folder.listFiles().size() > 0) {
            var temp = folder.listFiles().pop();
            if (temp.isDirectory() && temp.listFiles().size() > 0) {
                this.deleteFolder(temp);
            } else {
                temp.remove();
            }
        }
        if (folder.listFiles().size() == 0) {
            folder.remove();
        }
        return;
    }
};
