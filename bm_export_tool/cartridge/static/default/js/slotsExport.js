(function () {
    jQuery(document).ready(function(){
        var exportSiteDataLoader = jQuery("#exportSiteDataLoader");
        var uploadLoader = jQuery("#uploadLoader");
        var customExportLoader = jQuery("#customExportLoader");
        var exportSiteDataMsg = jQuery("#exportSiteDataMsg");
        var fileUploadMsg = jQuery("#fileUploadMsg");
        var customExportMsg = jQuery("#customExportMsg");
        var exportSiteDataInputs = jQuery('#exportSiteDataTable input');
        var exportSiteDataButtons = jQuery('#exportSiteDataTable button');
        var customExportFormInputs = jQuery('#customExportForm input');
        var customExportFormButtons = jQuery('#customExportForm button');
        var customExportFormTextareas = jQuery('#customExportForm textarea');
        var customExportTable = jQuery('#customExportTable');
        var selectSlotsUI = jQuery('#exportUI_SelectSlots');
        var slotsFoundUI = jQuery('#exportUI_SlotsFound');
        var exportedSlots;
        var slotsBlock = jQuery('.slots .slot-tr');
        var configurationsBlock = jQuery('.slots .configurations-tr');
        var objectsQuantity = slotsBlock.length;
        var pageNumber = 1;
        var pageSize = parseInt(app.preferences.slotsPageSize, 10);
        var expandedSlots = {1:[]};

        jQuery('#exportSiteDataForm').on('submit', exportSiteData);
        jQuery('#fileUploadForm').on('submit', fileUploadForm);
        jQuery('#customExportForm').on('submit', customExport);
        jQuery('input[name="uploadFile"]').on('change', setUploadFileName);
        jQuery('input[name="exportSiteDataFileName"]').on('keyup', setExportSiteDataFileName);
        jQuery('.slot-tr').on('click', showHideSlotConfigurations);
        jQuery('.slot-tr, .configurations-tr').on('click', checkRelatedSlotItems);
        jQuery('#exportedSlotsTR').on('click', showHideExportedSlots);
        jQuery('#page-buttons').on('click', getPage);
        orginizeItemsIntoPages();
        /**
         * Organizes slots into fixed-sized blocks with pagination
         */
        function orginizeItemsIntoPages() {
            displayItemsBlock();
            if (pageSize < objectsQuantity) {
                createPageButtons(pageSize, objectsQuantity);
                setNextPrevButtonsState(true, false);
                setSelectedPage(null, pageNumber);
            }
        }
        /**
         * Displays a fixed-size block of slots
         */
        function displayItemsBlock() {
            slotsBlock.hide();
            configurationsBlock.hide();
            var startPoint = (pageNumber - 1) * pageSize;
            var endPoint = startPoint + (pageSize > objectsQuantity ? objectsQuantity : pageSize);
            for (var i = startPoint; i < endPoint; i++) {
                var slotID = slotsBlock[i].attributes['data-slotID'].value;
                showExpandedSlotConfigurations(slotID);
                slotsBlock[i].show();
            }
        }
        /**
         * Adds autocheck for slots:
         *     1 - Check/uncheck all slot's configurations selectboxes when slot is checked/unchecked.
         *     2 - Semi-check slot's selectbox if one or several configurations checked.
         *     3 - Check slot's selectbox when all configurations are checked.
         *     4 - Uncheck slot's selectbox if configuration's select boxes unchecked.
         */
        function checkRelatedSlotItems(e) {
            if (e.target.tagName === 'INPUT') {
                var checkbox = jQuery(e.target);
                var checked = checkbox.prop('checked');
                var checkBoxType = checkbox.attr('name');
                var slotTR = jQuery(this);
                var slotID = slotTR.attr('data-slotID');
                var configurationsTR = jQuery('#' + slotID + '-configurations');
                if (checkBoxType === 'configurationSelected') {
                    var slotCheckBox = jQuery('#' + slotID + '-slot').find('input[type="checkbox"]');
                    var configurationsAmount = configurationsTR.find('input:checkbox').length;
                    var checkedConfigurationsAmount = configurationsTR.find('input:checkbox:checked').length;
                    if (checked) {
                        if (checkedConfigurationsAmount === configurationsAmount) {
                            slotCheckBox.prop('indeterminate', false);
                            slotCheckBox.prop('checked', true);
                        } else {
                            slotCheckBox.prop('indeterminate', true);
                        }
                    } else {
                        if (checkedConfigurationsAmount === 0) {
                            slotCheckBox.prop('indeterminate', false);
                            slotCheckBox.prop('checked', false);
                        } else {
                            slotCheckBox.prop('indeterminate', true);
                        }
                    }
                } else {
                    configurationsTR.find('input[type="checkbox"]').prop('checked', checked);
                }
            }
        }
        /**
         * Display expanded slot's configuration for current page
         *
         * @param {String} slotID
         */
        function showExpandedSlotConfigurations(slotID) {
            if (expandedSlots[pageNumber].indexOf(slotID) !== -1) {
                jQuery('#' + slotID + '-slot').find('.minimize').show();
                jQuery('#' + slotID + '-configurations').show();
                expandedSlots[pageNumber].push(slotID);
            }
        }
        /**
         * Expands and minimizes slot's configurations by user click and stores the expanded slot IDs for each page
         *
         * @param {Object} e - event object
         */
        function showHideSlotConfigurations(e) {
            if (e.target.tagName === 'IMG') {
                var slotTR = jQuery(this);
                var image = jQuery(e.target);
                var slotID = slotTR.attr('data-slotID');
                image.hide();
                if (image.hasClass('expand')) {
                    slotTR.find('.minimize').show();
                    jQuery('#' + slotID + '-configurations').show();
                    expandedSlots[pageNumber].push(slotID);
                } else {
                    slotTR.find('.expand').show();
                    jQuery('#' + slotID + '-configurations').hide();
                    expandedSlots[pageNumber].splice(expandedSlots[pageNumber].indexOf(slotID), 1);
                }
            }
        }
        /**
         * Expands and minimizes exported slots info
         *
         * @param {Object} e - event object
         */
        function showHideExportedSlots(e) {
            if (e.target.tagName === 'IMG') {
                var exportedSlotsTR = jQuery(this);
                var exportedConfigurationsTR = exportedSlotsTR.find('.configurations-tr');
                var target = jQuery(e.target);
                var expandIcon = exportedSlotsTR.find('.expand');
                var minimizeIcon = exportedSlotsTR.find('.minimize');
                if (target.hasClass('expand')) {
                    expandIcon.hide();
                    minimizeIcon.show();
                    exportedConfigurationsTR.show();
                } else {
                    expandIcon.show();
                    minimizeIcon.hide();
                    exportedConfigurationsTR.hide();
                }
            }
        }
        /**
         * Displays the block of slots for requested page
         *
         * @param {Object} e - event object
         */
        function getPage(e) {
            switch(e.target.id) {
                case 'next':
                    pageNumber++;
                    updateNextPrevButtonsState();
                    setSelectedPage(e, pageNumber);
                    displayItemsBlock();
                break;
                case 'prev':
                    pageNumber--;
                    updateNextPrevButtonsState();
                    setSelectedPage(e, pageNumber);
                    displayItemsBlock();
                break;
                default:
                    if (e.target.tagName == 'BUTTON') {
                        pageNumber = e.target.innerText;
                        updateNextPrevButtonsState();
                        setSelectedPage(e);
                        displayItemsBlock();
                    }
                break;
            }
        }
        /**
         * Get the number of pages depending on the slots quantity
         *
         * @param {Number} pageSize - number of slots per page
         * @param {Number} objectsQuantity - number of slots found
         */
        function getPageCount(pageSize, objectsQuantity) {
            return Math.ceil(objectsQuantity/pageSize);
        }
        /**
         * Set selected page styles (for page number)
         *
         * @param {Object} e - event object
         * @param {Number} page - page number
         */
        function setSelectedPage(e, page) {
            const buttons = document.querySelectorAll('.button.page-number');
            for (let i = 0; i < buttons.length; i++) {
                buttons[i].style.color = '#17a2b8';
                buttons[i].style.backgroundColor = 'white';
            }
            if (page) {
                buttons[page-1].style.color = 'white';
                buttons[page-1].style.backgroundColor = '#17a2b8';
            } else {
                e.target.style.color = 'white';
                e.target.style.backgroundColor = '#17a2b8';
            }
        }
        /**
         * Update availability status for next and previous page buttons
         */
        function updateNextPrevButtonsState() {
            if (pageNumber == getPageCount(pageSize, objectsQuantity)) {
                setNextPrevButtonsState(false, true)
            } else if (pageNumber == 1) {
                setNextPrevButtonsState(true, false);
            } else {
                setNextPrevButtonsState(false, false)
            };
        }
        /**
         * Set availability status for next and previous page buttons
         *
         * @param {Boolean} prevValue
         * @param {Boolean} nextValue
         */
        function setNextPrevButtonsState(prevValue, nextValue) {
            document.getElementById('prev').disabled = prevValue;
            document.getElementById('next').disabled = nextValue;
        }
        /**
         * Initialize object to store info about expanded slots for each page
         *
         * @param {Number} pageQuantity
         */
        function initializeExpandedSlotsObject(pageQuantity) {
            for (let i = 0; i < pageQuantity; i++) {
                expandedSlots[i+1] = [];
            }
        }
        /**
         * Create navigation buttons for page numbers and next/previous page
         *
         * @param {Number} pageSize - number of slots per page
         * @param {Number} objectsQuantity - number of slots found
         */
        function createPageButtons(pageSize, objectsQuantity) {
            var slotsBlock = jQuery('#selectSlots tbody');
            var buttons = jQuery('#page-buttons');
            buttons.empty();
            var pageNavigation = jQuery('<div class="page-navigation"></div>');
            var pageNumbers = jQuery('<div class="page-numbers"></div>');
            buttons.append(pageNavigation);
            buttons.append(pageNumbers);
            var pageQuantity = getPageCount(pageSize, objectsQuantity);
            initializeExpandedSlotsObject(pageQuantity);
            jQuery('<button/>', {
                id: 'prev',
                class: 'button page-navigation',
                type: 'button',
                text: '<'
            }).appendTo(pageNavigation);
            jQuery('<button/>', {
                id: 'next',
                class: 'button page-navigation',
                type: 'button',
                text: '>'
            }).appendTo(pageNavigation);
            for (let i = 0; i < pageQuantity; i++) {
                jQuery('<button/>', {
                    class: 'button page-number',
                    type: 'button',
                    text: i+1
                }).appendTo(pageNumbers);
            }
        }
        /**
         * Export all site slots
         *
         * @param {Object} e - event object
         */
        function exportSiteData(e) {
            e.preventDefault();
            var form = jQuery(this);
            var data = form.serialize();
            var fileName = jQuery('input[name="exportSiteDataFileName"]').val();
            if (fileName != '') {
               jQuery.ajax({
                    type: 'POST',
                    url: form.attr('action'),
                    data: data,
                    beforeSend: beforeSiteDataExport
                })
                .done(displaySiteSlots);
            } else {
                setMessage({
                    exportMsg: 'Please set export file name.'
                }, exportSiteDataMsg);
            }
        }
        /**
         * Upload slots from file
         *
         * @param {Object} e - event object
         */
        function fileUploadForm(e) {
            e.preventDefault();
            var form = jQuery(this);
            var data = new FormData(form[0]);
            var fileName = jQuery('input[name="uploadFile"]').val();
            if (fileName != '') {
                jQuery.ajax({
                    type: 'POST',
                    url: form.attr('action'),
                    data: data,
                    processData: false,
                    contentType: false,
                    beforeSend: beforeDataUpload
                })
                .done(displaySiteSlots);
            } else {
                setMessage({
                    exportMsg: 'Please select upload file.'
                }, fileUploadMsg);
            }
        }
        /**
         * Export selected slots
         *
         * @param {Object} e - event object
         */
        function customExport(e) {
            e.preventDefault();
            var configurationInputs = configurationsBlock.find('input[name="configurationSelected"]:checked');
            var slots = [];
            var exportFileName = jQuery('input[name="exportFileName"]').val();
            var form = jQuery(this);
            jQuery.each(configurationInputs, function (index, input) {
                slots.push({
                    slotID: jQuery(input).parents('.configurations-tr').attr('data-slotID'),
                    configurationID: input.value,
                    context: input.attributes['data-context'].value,
                    contextID: input.attributes['data-contextID'].value,
                    assignedToSite: input.attributes['data-assignedToSite'].value
                });
            });
            if (slots.length) {
                jQuery.ajax({
                    type: 'POST',
                    url: form.attr('action'),
                    dataType: 'json',
                    contentType: 'application/json',
                    data: JSON.stringify({slots: slots, exportFileName: exportFileName}),
                    beforeSend: beforeCustomExport
                })
                .done(displaySlotsFound);
            } else {
                setMessage({
                    exportMsg: 'Please select slots to export.'
                }, customExportMsg);
            }
        }
        /**
         * Display all site slots after data export
         *
         * @param {Object} dataMsg - response object from exportSiteData execution
         */
        function displaySiteSlots(dataMsg) {
            jQuery.ajax({
                type: 'GET',
                url: app.urls.displaySiteSlots
            })
            .done(function(response) {
                if (dataMsg.type === 'slots-export') {
                    afterSiteDataExport(dataMsg);
                }
                if (dataMsg.type === 'slots-upload') {
                    afterDataUpload(dataMsg);
                }
                selectSlotsUI.html(response);
                updateSlotsPageData();
            });
        }
        /**
         * Display selected slots after custom export
         *
         * @param {Object} dataMsg - response object from customExport execution
         */
        function displaySlotsFound(dataMsg) {
            jQuery.ajax({
                type: 'GET',
                url: app.urls.displaySlotsFound
            })
            .done(function(response) {
                afterCustomExport(dataMsg);
                slotsFoundUI.show();
                slotsFoundUI.html(response);
                updateSlotsPageData();
            });
        }
        /**
         * Display the file path and file name for the export file
         */
        function setUploadFileName(e) {
            var fileNameElement = jQuery("#exportSiteDataFilePath .fileName");
            var fileName = e.target.files[0].name;
            if (fileName.indexOf('.xml') !== -1) {
                fileName = fileName.replace('.xml', '');
            }
            fileNameElement.html(fileName + '-timestamp.xml');
        }
        /**
         * Display the file path and file name for the uploaded file
         */
        function setExportSiteDataFileName() {
            var fileNameElement = jQuery("#exportSiteDataFilePath .fileName");
            var inputValue = jQuery(this).val();
            fileNameElement.html(inputValue + '-timestamp.xml');
            if (inputValue == '') {
                fileNameElement.empty();
            }
            if (inputValue.indexOf('.xml') !== -1) {
                fileNameElement.html(inputValue.replace('.xml', '') + '-timestamp.xml');
            }
        }
        /**
         * Refresh slots data after exporting and displaying new slots table
         */
        function updateSlotsPageData() {
            slotsBlock = jQuery('.slots .slot-tr');
            configurationsBlock = jQuery('.slots .configurations-tr');
            objectsQuantity = slotsBlock.length;
            pageNumber = 1;
            expandedSlots = {1:[]};
            jQuery('.slot-tr').on('click', showHideSlotConfigurations);
            jQuery('.slot-tr, .configurations-tr').on('click', checkRelatedSlotItems);
            orginizeItemsIntoPages();
        }

        function setMessage(data, element) {
            element.empty();
            var exportedSlotsTR = jQuery('#exportedSlotsTR');
            if(data.success) {
                element.append('<span>'+data.exportMsg+'</span><br>');
                element.append('<p>File path: ' + '<a href="'+encodeURI(data.url)+'" target="_blank">'+data.url+'</a></p>');
                if(data.type !== 'upload') {
                    element.append('<a href="'+encodeURI(data.url)+'" download><img src="'+app.urls.downloadIcon+'" class="download-icon"></a><br>');
                }
                if (data.type === 'slots-export' || data.type ===  'upload') {
                    jQuery('#slotsUsed').html('Slots used: <a href="'+encodeURI(data.url)+'" target="_blank">'+data.url+'</a>');
                }
            } else {
                element.append('<p>'+data.exportMsg+'</p>');
            }
           element.show();
        }

        function beforeSiteDataExport() {
            exportSiteDataLoader.show();
            exportSiteDataMsg.empty();
            exportSiteDataInputs.prop('disabled', true);
            exportSiteDataButtons.prop('disabled', true);
            customExportFormInputs.prop('disabled', true);
            customExportFormButtons.prop('disabled', true);
            customExportFormTextareas.prop('disabled', true);
        }

        function afterSiteDataExport(data) {
            exportSiteDataLoader.hide();
            exportSiteDataInputs.prop('disabled', false);
            exportSiteDataButtons.prop('disabled', false);
            customExportFormInputs.prop('disabled', false);
            customExportFormButtons.prop('disabled', false);
            customExportFormTextareas.prop('disabled', false);
            customExportTable.show();
            setMessage(data, exportSiteDataMsg);
        }

        function beforeDataUpload() {
            uploadLoader.show();
            fileUploadMsg.empty();
            exportSiteDataInputs.prop('disabled', true);
            exportSiteDataButtons.prop('disabled', true);
            customExportFormInputs.prop('disabled', true);
            customExportFormButtons.prop('disabled', true);
            customExportFormTextareas.prop('disabled', true);
        }

        function afterDataUpload(data) {
            uploadLoader.hide();
            exportSiteDataInputs.prop('disabled', false);
            exportSiteDataButtons.prop('disabled', false);
            customExportFormInputs.prop('disabled', false);
            customExportFormButtons.prop('disabled', false);
            customExportFormTextareas.prop('disabled', false);
            customExportTable.show();
            setMessage(data, fileUploadMsg);
        }

        function beforeCustomExport() {
            customExportLoader.show();
            customExportMsg.empty();
            customExportFormInputs.prop('disabled', true);
            customExportFormButtons.prop('disabled', true);
            customExportFormTextareas.prop('disabled', true);
        }

        function afterCustomExport(data) {
            customExportLoader.hide();
            customExportFormInputs.prop('disabled', false);
            customExportFormButtons.prop('disabled', false);
            customExportFormTextareas.prop('disabled', false);
            setMessage(data, customExportMsg);
        }
    });
})();