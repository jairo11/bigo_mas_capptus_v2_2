jQuery(document).ready(function(){
    jQuery('input[name="exportFileName"]').on('keyup', setExportFileName);
    /**
     * Displays an name format for the future export file
     */
    function setExportFileName () {
        fileNameElement = jQuery("#exportFilePath .fileName");
        var inputValue = jQuery(this).val();
        fileNameElement.html(inputValue + '-timestamp.xml');
        if (inputValue == '') {
            fileNameElement.empty();
        }
        if (inputValue.indexOf('.xml') !== -1) {
            fileNameElement.html(inputValue.replace('.xml', '') + '-timestamp.xml');
        }
    }
});

/**
 * Exports folders by provided array of IDs
 *
 * @param {Object} data - result returned by method
 * @param {Object} element - element for the message display
 *
 */
function setMessage(data, element) {
    element.empty();
    if(data.success) {
        element.append('<span>'+data.exportMsg+'</span><br>');
        element.append('<p>File path: ' + '<a href='+encodeURI(data.url)+' target="_blank">'+data.url+'</a></p>');
        if (data.type === 'library-export' || data.type === 'library-upload') {
            jQuery('#libraryUsed').html('Library used: <a href="'+encodeURI(data.url)+'" target="_blank">'+data.url+'</a>');
        }
        if (data.type === 'metadata-export' || data.type === 'metadata-upload') {
            jQuery('#metadataUsed').html('Metadata used: <a href="'+encodeURI(data.url)+'" target="_blank">'+data.url+'</a>');
        }
        if(data.type !== 'upload') {
            element.append('<a href="'+encodeURI(data.url)+'" download><img src="'+app.urls.downloadIcon+'" class="download-icon"></a><br>');
        }
        if (data.type == 'content') {
            if (data.staticContent) {
                element.append('<span>' + data.staticContent.exportMsg + '</span><br>');
                if (data.staticContent.success) {
                    element.append('<p> File path: <a href="'+encodeURI(data.staticContent.url)+'" target="_blank">'+data.staticContent.url+'</a></p>');
                    element.append('<a href="'+encodeURI(data.staticContent.url)+'" download><img src="'+app.urls.downloadIcon+'" class="download-icon"></a></br>');
                }
            }
            if (data.contentFound.length) {
                var contentFound = '';
                data.contentFound.forEach(function(object){
                    contentFound += '<p><strong>'+object.designation+'</strong></p>';
                    if (object.attributesNotFound.length) {
                        contentFound += '<span>Attribute/s not found: </span>';
                        contentFound += '<ul>';
                        object.attributesNotFound.forEach(function(attr) {
                            contentFound += '<li>' + attr + ' </li>';
                        });
                        contentFound += '</ul>';
                    }
                    if (object.groupDefinitionsNotFound) {
                        if (object.groupDefinitionsNotFound.length) {
                            contentFound += '<span>Group definition/s not found: </span>';
                            contentFound += '<ul>';
                            object.groupDefinitionsNotFound.forEach(function(group) {
                                contentFound += '<li>' + group + ' </li>';
                            });
                            contentFound += '</ul>';
                        }
                    }
                });
                element.append('<p><strong style="color:green">Exported: </strong></p>'+contentFound);
            }
            if (data.contentNotFound.length) {
                var contentNotFound = '';
                data.contentNotFound.forEach(function(content){
                    contentNotFound += '<span>'+content+'</span>';
                });
                element.append('<p><strong style="color:red">Not found: </strong></p>'+contentNotFound);
            }
        }
    } else {
        element.append('<p>'+data.exportMsg+'</p>');
    }
   element.show();
}