(function (app, $) {
    /**
     * Provides a tab switch functionality for UI
     *
     * @param {Object} e - eventObject
     */
    function openTab(e) {
        if (e.target.tagName === 'BUTTON') {
            var UI = jQuery(e.target).attr('data-UIID');
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(UI).style.display = "block";
            e.target.className += " active";
        }
    }
    /**
     * Get the number of pages depending on the slots quantity
     *
     * @param {Number} pageSize - number of slots per page
     * @param {Number} objectsQuantity - number of slots found
     */
    function getPageCount(pageSize, objectsQuantity) {
        return Math.ceil(objectsQuantity/pageSize);
    }
    /**
     * Update availability status for next and previous page buttons
     * @param {Number} pageSize - number of items per page
     * @param {Number} objectsQuantity - number of items found
     * @param {Number} pageNumber - current page number
     * @param {Number} pageSize - number of items per page
     */
    function updateNextPrevButtonsState(itemsTable, objectsQuantity, pageNumber, pageSize) {
        if (pageNumber == getPageCount(pageSize, objectsQuantity)) {
            setNextPrevButtonsState(itemsTable, false, true);
        } else if (pageNumber == 1) {
            setNextPrevButtonsState(itemsTable, true, false);
        } else {
            setNextPrevButtonsState(itemsTable, false, false);
        };
    }
    /**
     * Set availability status for next and previous page buttons
     *
     * @param {Boolean} prevValue
     * @param {Boolean} nextValue
     */
    function setNextPrevButtonsState(itemsTable, prevValue, nextValue) {
        itemsTable.find('.button.prev').prop('disabled', prevValue);
        itemsTable.find('.button.next').prop('disabled', nextValue);
    }
    /**
     * Initialize object to store info about expanded items for each page
     *
     * @param {Number} pageQuantity
     */
    function initializeExpandedItemsObject(expandedItems, pageQuantity) {
        for (var i = 0; i < pageQuantity; i++) {
            expandedItems[i+1] = [];
        }
    }
    /**
     * Organizes items into fixed-sized blocks with pagination
     *
     * @param {Object} itemsTable - table that contains all items and sub items to be paginated
     * @param {Number} objectsQuantity - number of slots found
     * @param {Number} pageNumber - current page number
     * @param {Number} expandedItems - list of expanded objects for each page
     */
    function orginizeItemsIntoPages(itemsTable, objectsQuantity, pageNumber, pageSize, expandedItems) {
        if (pageSize < objectsQuantity) {
            createPageButtons(itemsTable, pageSize, objectsQuantity);
            setNextPrevButtonsState(itemsTable, true, false);
            setSelectedPage(null, pageNumber, itemsTable);
            initializeExpandedItemsObject(expandedItems, getPageCount(pageSize, objectsQuantity));
        }
    }
    /**
     * Set selected page styles (for page number)
     *
     * @param {Object} e - event object
     * @param {Number} page - page number
     */
    function setSelectedPage(e, page, itemsTable) {
        var buttons = itemsTable.find('.button.page-number');
        for (var i = 0; i < buttons.length; i++) {
            buttons[i].classList.remove('page-number-selected');
        }
        if (page) {
            buttons[page-1].classList.add('page-number-selected');
        } else {
            e.target.classList.remove('page-number-selected');
        }
    }
    /**
     * Create navigation buttons for page numbers and next/previous page
     *
     * @param {Object} itemsTable - table that contains all items and sub items to be paginated
     * @param {Number} pageSize - number of slots per page
     * @param {Number} objectsQuantity - number of slots found
     */
    function createPageButtons(itemsTable, pageSize, objectsQuantity) {
        var pageNavigation = itemsTable.find('.navigation-buttons .page-navigation');
        var pageNumbers = itemsTable.find('.navigation-buttons .page-numbers');
        pageNavigation.empty();
        pageNumbers.empty();
        var pageQuantity = getPageCount(pageSize, objectsQuantity);
        jQuery('<button/>', {
            'data-buttonType': 'prev',
            'class': 'button prev',
            'type': 'button',
            'text': '<'
        }).appendTo(pageNavigation);
        jQuery('<button/>', {
            'data-buttonType': 'next',
            'class': 'button next',
            'type': 'button',
            'text': '>'
        }).appendTo(pageNavigation);
        for (var i = 0; i < pageQuantity; i++) {
            jQuery('<button/>', {
                'data-buttonType': 'page-number',
                'class': 'button page-number',
                'type': 'button',
                'text': i+1
            }).appendTo(pageNumbers);
        }
    }
    app.components = app.components || {};
    app.components.UINavigation = {
        openTab: openTab,
        updateNextPrevButtonsState: updateNextPrevButtonsState,
        orginizeItemsIntoPages: orginizeItemsIntoPages,
        setSelectedPage: setSelectedPage
    };
}(window.app = window.app || {}, jQuery));