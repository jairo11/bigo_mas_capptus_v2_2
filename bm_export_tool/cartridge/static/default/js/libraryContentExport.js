(function (app) {
    jQuery(document).ready(function(){
        var libraryExportLoader = jQuery("#libraryExportLoader");
        var libraryUploadLoader = jQuery("#libraryUploadLoader");
        var contentLoader = jQuery("#contentLoader");
        var libraryExportMsg =  jQuery("#libraryExportMsg");
        var libraryUploadMsg = jQuery("#libraryUploadMsg");
        var exportContentMsg = jQuery("#exportContentMsg");
        var contentExportInputs = jQuery('#contentExport input');
        var contentExportButtons = jQuery('#contentExport button');
        var contentExportTextareas = jQuery('#contentExport textarea');
        var libraryInputs = jQuery('#library input');
        var libraryButtons = jQuery('#library button');
        var contentExportElement = jQuery('#content-export');
        var selectContentUI = jQuery('#contentExportUI_SelectContent');
        var selectAssetsTable = jQuery('#selectAssetsTable tbody');
        var selectFoldersTable = jQuery('#selectFoldersTable tbody');
        var assetsQuantity = selectAssetsTable.find('.items-tr').length;
        var foldersQuantity = selectFoldersTable.find('.items-tr').length;
        var pageNumberAssets = 1;
        var pageNumberFolders = 1;
        var pageSize = parseInt(app.preferences.libraryContentPageSize, 10);
        var expandedAssets = {1:[]};
        var expandedFolders = {1:[]};
        var UINavigation = app.components.UINavigation;
        var assetUIError = jQuery('#assetUIError');

        jQuery('#libraryExport').on('submit', libraryExport);
        jQuery('#libraryUpload').on('submit', libraryUpload);
        jQuery('#contentExport').on('submit', contentExport);
        jQuery('input[name="uploadFile"]').on('change', setLibraryUploadFileName);
        jQuery('input[name="libraryExportFileName"]').on('keyup', setLibraryExportFileName);
        jQuery('.searchValue').on('click', checkSearchModeRadio);
        jQuery('.searchMode').on('click', checkExportTypeRadio);
        jQuery('.tab').on('click', UINavigation.openTab);
        initPageNavigationEvents();

        displayItemsBlock(selectFoldersTable, foldersQuantity, pageNumberFolders);
        UINavigation.orginizeItemsIntoPages(selectFoldersTable, foldersQuantity, pageNumberFolders, pageSize, expandedFolders);
        organizeAssetsPageNavigation();

        /**
         * Displays a fixed-size block of items
          *
          * @param {Object} itemsTable - table that contains all items and sub items to be paginated
          * @param {Number} objectsQuantity - number of slots found
          * @param {Number} pageNumber - current page number
          */
        function displayItemsBlock(itemsTable, objectsQuantity, pageNumber) {
            var itemsBlock = itemsTable.find('.items-tr');
            var detailsBlock = itemsTable.find('.details-tr');
            itemsBlock.hide();
            detailsBlock.hide();
            var startPoint = (pageNumber - 1) * pageSize;
            var endPoint;
            if (objectsQuantity - startPoint < pageSize) {
                endPoint = startPoint + objectsQuantity - startPoint;
            } else {
                endPoint = startPoint + (pageSize > objectsQuantity ? objectsQuantity : pageSize);
            }
            for (var i = startPoint; i < endPoint; i++) {
                var objectID = itemsBlock[i].attributes['data-objectID'].value;
                var objectType = itemsBlock[i].attributes['data-objectType'].value;
                showExpandedObjectDetails(objectID, objectType);
                itemsBlock[i].show();
            }
        }
        /**
         * Adds autocheck for items:
         *     1 - Check/uncheck all item deails selectboxes when detail item is checked/unchecked.
         *     2 - Semi-check item's selectbox if one or several detail items checked.
         *     3 - Check item's selectbox when all detail items are checked.
         *     4 - Uncheck item's selectbox if detail item select boxes unchecked.
         */
        function checkRelatedItems(e) {
            if (e.target.tagName === 'INPUT') {
                var checkBox = jQuery(e.target);
                var checked = checkBox.prop('checked');
                var checkBoxType = checkBox.attr('data-checkBoxType');
                var objectTR = jQuery(this);
                var objectID = objectTR.attr('data-objectID');
                var objectType = objectTR.attr('data-objectType');
                var objectIDDashed = objectID.replace(' ', '-');
                var objectSelector = '#'+objectIDDashed+'-'+objectType;
                var detailsSelector = '#'+objectIDDashed+'-'+objectType+'-details';
                var detailsTR = jQuery(detailsSelector);
                var objectCheckBox = jQuery(objectSelector).find('input[type="checkbox"]');
                var detailsAmount = detailsTR.find('input:checkbox').length;
                var checkedDetailsAmount = detailsTR.find('input:checkbox:checked').length;
                if (checkBoxType === 'subItem') {
                    if (checked) {
                        if (checkedDetailsAmount === detailsAmount) {
                            objectCheckBox.prop('indeterminate', false);
                            objectCheckBox.prop('checked', true);
                        } else {
                            objectCheckBox.prop('indeterminate', true);
                        }
                    } else {
                        if (checkedDetailsAmount === 0) {
                            objectCheckBox.prop('indeterminate', false);
                            objectCheckBox.prop('checked', false);
                        } else {
                            objectCheckBox.prop('indeterminate', true);
                        }
                    }
                } else {
                    detailsTR.find('input[type="checkbox"]').prop('checked', checked);
                }
                if (objectType === 'asset') {
                    addRemoveCheckedAssetToSession(objectID, detailsTR, checked || objectCheckBox.prop('indeterminate'));
                }
            }
        }
        /**
         * Expands and minimizes slot's configurations by user click and stores the expanded slot IDs for each page
         *
         * @param {Object} e - event object
         */
        function showHideItemDetails(e) {
            if (e.target.tagName === 'IMG') {
                var objectTR = jQuery(this);
                var image = jQuery(e.target);
                var objectID = objectTR.attr('data-objectID');
                var objectType = objectTR.attr('data-objectType');
                var detailsSelector = '#'+objectID.replace(' ', '-')+'-'+objectType+'-details';
                image.hide();
                var pageNumber = objectType == 'asset' ? pageNumberAssets : pageNumberFolders;
                if (image.hasClass('expand')) {
                    objectTR.find('.minimize').show();
                    jQuery(detailsSelector).show();
                    objectType == 'asset' ?
                        addRemoveExpandedAssetToSession(objectID, 'add') :
                        expandedFolders[pageNumberFolders].push(objectID) ;
                } else {
                    objectTR.find('.expand').show();
                    jQuery(detailsSelector).hide();
                    objectType == 'asset' ?
                        addRemoveExpandedAssetToSession(objectID, 'remove') :
                        expandedFolders[pageNumberFolders].splice(expandedFolders[pageNumberFolders].indexOf(objectID), 1);
                }
            }
        }
        function addRemoveExpandedAssetToSession(assetID, action) {
            jQuery.ajax({
                type: 'GET',
                url: app.urls.addRemoveExpandedAssetToSession,
                data: {
                    assetID: assetID,
                    action: action,
                }
            }).done(function(data){
                if (!data.success) {
                    assetUIError.append(app.resources.expandAssetError.replace('{0}', assetID));
                }
            });
        }
        function addRemoveCheckedAssetToSession(objectID, detailsTR, checked) {
            var assetAttributesInputs = detailsTR.find('input[name="assetAttributeCheck"]:checked');
            var assetAttributes = [];
            jQuery.each(assetAttributesInputs, function (index, input) {
                assetAttributes.push(input.value);
            });
            jQuery.ajax({
                type: 'GET',
                url: app.urls.addRemoveCheckedAssetToSession,
                data: {
                    assetID: objectID,
                    assetAttributes: assetAttributes.join(),
                    action: checked ? 'add' : 'remove',
                }
            }).done(function(data){
                if (!data.success) {
                    assetUIError.append(app.resources.checkAssetError.replace('{0}', objectID));
                }
            });
        }
        /**
         * Display expanded slot's configuration for current page
         *
         * @param {String} slotID
         */
        function showExpandedObjectDetails(objectID, objectType) {
            var objectIDDashed = objectID.replace(' ', '-');
            if (expandedFolders[pageNumberFolders].indexOf(objectID) !== -1) {
                jQuery('#'+objectIDDashed+'-'+objectType).find('.minimize').show();
                jQuery('#'+objectIDDashed+'-'+objectType+'-details').show();
                expandedFolders[pageNumberFolders].push(objectID);
            }
        }
        /**
         * Display fixed-size block of folders depending on different types of navigation interaction
         */
        function getFoldersPage(e) {
            var itemsTable = this.itemsTable;
            var objectsQuantity = this.objectsQuantity;
            var type = jQuery(e.target).closest('.navigation-buttons').attr('data-buttonType');
            var pageNumber = type == 'asset' ? pageNumberAssets : pageNumberFolders;
            var buttonType = jQuery(e.target).attr('data-buttonType');
            switch(buttonType) {
                case 'next':
                    pageNumber++;
                    type == 'asset' ? pageNumberAssets++ : pageNumberFolders++ ;
                    UINavigation.updateNextPrevButtonsState(itemsTable, objectsQuantity, pageNumber, pageSize);
                    UINavigation.setSelectedPage(e, pageNumber, itemsTable);
                    displayItemsBlock(itemsTable, objectsQuantity, pageNumber);
                break;
                case 'prev':
                    pageNumber--;
                    type == 'asset' ? pageNumberAssets-- : pageNumberFolders--;
                    UINavigation.updateNextPrevButtonsState(itemsTable, objectsQuantity, pageNumber, pageSize);
                    UINavigation.setSelectedPage(e, pageNumber, itemsTable);
                    displayItemsBlock(itemsTable, objectsQuantity, pageNumber);
                break;
                case 'page-number':
                    pageNumber = e.target.innerText;
                    type == 'asset' ? (pageNumberAssets = e.target.innerText) : (pageNumberFolders = e.target.innerText);
                    UINavigation.updateNextPrevButtonsState(itemsTable, objectsQuantity, pageNumber, pageSize);
                    UINavigation.setSelectedPage(e, pageNumber, itemsTable);
                    displayItemsBlock(itemsTable, objectsQuantity, pageNumber);
                break;
            }
        }
        /**
         * Get fixed-size block of assets depending on different types of navigation interaction
         */
        function getAssetsPage(e) {
            if (e.type === 'click' && e.target.tagName === 'BUTTON') {
                var $target = jQuery(e.target);
                var buttonType = $target.attr('data-buttonType');
                pageNumberAssets = parseInt(jQuery('#page-buttons-assets').find('.page-numbers .button.page-number.page-number-selected').text(), 10);
                switch(buttonType) {
                    case 'next':
                        pageNumberAssets++;
                        getCurrentPageAssets(pageNumberAssets);
                        break;
                    case 'prev':
                        pageNumberAssets--;
                        getCurrentPageAssets(pageNumberAssets);
                        break;
                    case 'page-number':
                        pageNumberAssets = $target.text();
                        getCurrentPageAssets(pageNumberAssets);
                        break;
                }
            }
            if (e.type === 'change' && e.target.tagName === 'SELECT') {
                pageNumberAssets = jQuery('#selectAssetsPage option:selected').val();
                getCurrentPageAssets(pageNumberAssets);
            }
        }
        /**
         * Retrieves an assets block for current page from session
         */
        function getCurrentPageAssets(pageNumber) {
            var selectAssetsContent_UI = jQuery('#contentExportUI_SelectAssets');
            jQuery.ajax({
                type: 'GET',
                url: app.urls.getCurrentPageAssets,
                data: {
                    pageNumberAssets: pageNumber,
                    pageSize: pageSize
                }
            })
            .done(function(response) {
                selectAssetsContent_UI.html(response);
                initPageNavigationEvents();
                organizeAssetsPageNavigation();
            });
        }
        /**
         * Organizes assets into fixed-sized blocks with pagination
         */
        function organizeAssetsPageNavigation() {
            var pageNumbersBlock = jQuery('#page-buttons-assets .page-numbers');
            var pageNumberButtons = pageNumbersBlock.find('button');
            var numberOfPages = pageNumberButtons.length;
            var activePageButton = pageNumbersBlock.find('button.page-number-selected');
            if (numberOfPages > 10) {
                pageNumberButtons.hide();
                for (let i = 0; i < 5; i++) {
                    pageNumberButtons[i].show();
                }
                if (activePageButton.text() > 5) {
                    activePageButton.show();
                    if (activePageButton.text() > 6 && activePageButton.text() < numberOfPages - 4) {
                        activePageButton.before('<span>...  </span>');
                    }
                }
                if (activePageButton.text() != (numberOfPages - 5)) {
                    jQuery(pageNumberButtons[numberOfPages - 5]).before('<span>...  </span>');
                }
                for (let i = numberOfPages - 5; i < numberOfPages; i++) {
                    pageNumberButtons[i].show();
                }
            }
        }
        /**
         * Refresh slots data after exporting and displaying new slots table
         */
        function updatePageData() {
            selectAssetsTable = jQuery('#selectAssetsTable tbody');
            selectFoldersTable = jQuery('#selectFoldersTable tbody');
            assetsQuantity = selectAssetsTable.find('.items-tr').length;
            foldersQuantity = selectFoldersTable.find('.items-tr').length;
            pageNumberAssets = 1;
            pageNumberFolders = 1;
            expandedAssets = {1:[]};
            expandedFolders = {1:[]};
            initPageNavigationEvents();
            displayItemsBlock(selectFoldersTable, foldersQuantity, pageNumberFolders);
            UINavigation.orginizeItemsIntoPages(selectFoldersTable, foldersQuantity, pageNumberFolders, pageSize, expandedFolders);
            organizeAssetsPageNavigation();
            var exportType_UI = jQuery('.tab .tablinks.active').attr('data-uiid');
            jQuery('#'+exportType_UI).show();
        }
        /**
         * Initialize page navigation event listeners
         */
        function initPageNavigationEvents() {
            jQuery('.items-tr').on('click', showHideItemDetails);
            jQuery('.items-tr, .details-tr').on('click', checkRelatedItems);
            jQuery('#page-buttons-assets').on('click', getAssetsPage);
            jQuery('#selectAssetsPage').on('change', getAssetsPage);
            jQuery('#page-buttons-folders').on('click', getFoldersPage.bind({
                itemsTable: selectFoldersTable,
                objectsQuantity: foldersQuantity
            }));
        }
        /**
         * Displays an name format for the future uploaded file
         */
        function setLibraryUploadFileName(e) {
            var fileNameElement = jQuery("#libraryFilePath .fileName");
            var fileName = e.target.files[0].name;
            if (fileName.indexOf('.xml') !== -1) {
                fileName = fileName.replace('.xml', '');
            }
            fileNameElement.html(fileName + '-timestamp.xml');
        }
        /**
         * Displays an name format for the site library export file
         */
        function setLibraryExportFileName() {
            var fileNameElement = jQuery("#libraryFilePath .fileName");
            var inputValue = jQuery(this).val();
            fileNameElement.html(inputValue + '-timestamp.xml');
            if (inputValue == '') {
                fileNameElement.empty();
            }
            if (inputValue.indexOf('.xml') !== -1) {
                fileNameElement.html(inputValue.replace('.xml', '') + '-timestamp.xml');
            }
        }
        /**
         * Check corresponding search mode type depending on user interaction with page
         */
        function checkSearchModeRadio(e) {
            var parent = e.target.parentElement;
            var radio = parent.querySelector('input[type="radio"]');
            radio.checked = true;
            checkExportTypeRadio(e);
        }
        /**
         * Check corresponding export type depending on user interaction with page
         */
        function checkExportTypeRadio(e) {
            if (e.target.classList.contains('assetSearch')){
                jQuery('#exportTypeAsset').prop('checked', true);
            }
            if (e.target.classList.contains('folderSearch')){
                jQuery('#exportTypeFolder').prop('checked', true);
            }
        }
        /**
         * Export current site library for further use in export
         *
         * @param {Object} e - event object
         */
        function libraryExport(e) {
            e.preventDefault();
            var form = jQuery(this);
            var data = form.serialize();
            var fileName = jQuery('input[name="libraryExportFileName"]').val();
            if (fileName != '') {
                jQuery.ajax({
                    type: 'POST',
                    url: form.attr('action'),
                    data: data,
                    beforeSend: beforeLibraryExport
                })
                .done(displaySiteLibraryContent);
            } else {
                setMessage({
                    exportMsg: 'Please set export file name.'
                }, libraryExportMsg);
            }
        }
        /**
         * Upload external library for further use in export
         *
         * @param {Object} e - event object
         */
        function libraryUpload(e) {
            e.preventDefault();
            var form = jQuery(this);
            var data = new FormData(form[0]);
            var fileName = jQuery('input[name="uploadFile"]').val();
            if (fileName != '') {
                jQuery.ajax({
                    type: 'POST',
                    url: form.attr('action'),
                    data: data,
                    processData: false,
                    contentType: false,
                    beforeSend: beforeLibraryUpload
                })
                .done(displaySiteLibraryContent);
            } else {
                setMessage({
                    exportMsg: 'Please select upload file.'
                }, libraryUploadMsg);
            }
        }
        /**
         * Calls custom export logic from BD for different types of UI
         *
         * @param {Object} e - event object
         */
        function contentExport(e) {
            e.preventDefault();
            var exportType_UI = jQuery('.tab .tablinks.active').attr('data-uiid');
            if (jQuery('input[name="exportFileName"]').val() == '') {
                setMessage({
                    exportMsg: 'Please set an export file name.'
                }, exportContentMsg);
            } else if (exportType_UI === 'contentExportUI_Manual') {
                var assetSearchMode = jQuery('.assetSearch:checked');
                var folderSearchMode = jQuery('.folderSearch:checked');
                var assetSearchValue = jQuery(assetSearchMode.data('associated')).val();
                var folderSearchValue = jQuery(folderSearchMode.data('associated')).val();
                var isAssetExport = jQuery('#exportTypeAsset').is(':checked');
                var isFolderExport = jQuery('#exportTypeFolder').is(':checked');
                if (!jQuery('input[name="exportType"]').is(':checked')) {
                    setMessage({
                        exportMsg: 'Please select an export type.'
                    }, exportContentMsg);
                } else if (isAssetExport && !jQuery('input[name="assetSearchMode"]').is(':checked') ||
                    isFolderExport && !jQuery('input[name="folderSearchMode"]').is(':checked')) {
                    setMessage({
                        exportMsg: 'Please select a search mode.'
                    }, exportContentMsg);
                } else if (isAssetExport && assetSearchValue === '' || isFolderExport && folderSearchValue === '') {
                    setMessage({
                        exportMsg: 'Please specify a search value.'
                    }, exportContentMsg);
                }  else {
                    var form = jQuery(this);
                    var data = form.serialize();
                    jQuery.ajax({
                        type: 'POST',
                        url: form.attr('action'),
                        data: data,
                        beforeSend: beforeContentExport
                    })
                    .done(afterContentExport);
                }
            } else if (exportType_UI === 'contentExportUI_SelectAssets') {
                var exportFileName = jQuery('input[name="exportFileName"]').val();
                var form = jQuery(this);
                jQuery.ajax({
                    type: 'POST',
                    url: form.attr('action'),
                    dataType: 'json',
                    contentType: 'application/json',
                    data: JSON.stringify({
                        exportFileName: exportFileName,
                        exportType_UI: exportType_UI,
                        exportStaticContent: jQuery('#exportStaticContentCheck_SelectAssetsUI').prop('checked')
                    }),
                    beforeSend: beforeContentExport
                })
                .done(afterContentExport);
            } else if (exportType_UI === 'contentExportUI_SelectFolders') {
                var folders = [];
                var objectsBlock = selectFoldersTable.find('input[name="folderSelected"]:checked, input[name="folderSelected"]:indeterminate');
                var exportFileName = jQuery('input[name="exportFileName"]').val();
                var folderExportSettings = jQuery('#folderExportSettings_SelectUI').find('input[name="folderExportSettings"]:checked').val();
                var form = jQuery(this);
                jQuery.each(objectsBlock, function (index, input) {
                    var objectID = input.value;
                    var detailsBlock = selectFoldersTable.find('tr[data-objectID="'+objectID+'"]');
                    var foldersAttributesInputs = detailsBlock.find('input[name="folderAttributeCheck"]:checked');
                    var folderAttributes = [];
                    jQuery.each(foldersAttributesInputs, function (index, input) {
                        folderAttributes.push(input.value);
                    });
                    folders.push({
                        designation: objectID,
                        attributes: folderAttributes
                    });
                });
                if (folders.length) {
                    jQuery.ajax({
                        type: 'POST',
                        url: form.attr('action'),
                        dataType: 'json',
                        contentType: 'application/json',
                        data: JSON.stringify({
                            folders: folders,
                            exportFileName: exportFileName,
                            exportType_UI: exportType_UI,
                            folderExportSettings: folderExportSettings,
                            exportStaticContent: jQuery('#exportStaticContentCheck_SelectFoldersUI').prop('checked')
                        }),
                        beforeSend: beforeContentExport
                    })
                    .done(afterContentExport);
                } else {
                    setMessage({
                        exportMsg: 'Please select folders to export.'
                    }, exportContentMsg);
                }
            }
        }
        /**
         * Display site library content after data export
         *
         * @param {Object} dataMsg - response object from libraryExport execution
         */
        function displaySiteLibraryContent(dataMsg) {
            var exportType_UI = jQuery('.tab .tablinks.active').attr('data-uiid');
            jQuery.ajax({
                type: 'GET',
                url: app.urls.displaySiteLibraryContent,
                data: {
                    pageNumber: pageNumberAssets,
                    pageSize: pageSize,
                    type: exportType_UI
                }
            })
            .done(function(response) {
                if (dataMsg.type === 'library-export') {
                    afterLibraryExport(dataMsg);
                }
                if (dataMsg.type === 'library-upload') {
                    afterLibraryUpload(dataMsg);
                }
                selectContentUI.html(response);
                updatePageData();
            });
        }

        function beforeLibraryExport() {
            libraryExportLoader.show();
            libraryExportMsg.empty();
            libraryInputs.prop('disabled', true);
            libraryButtons.prop('disabled', true);
            contentExportInputs.prop('disabled', true);
            contentExportButtons.prop('disabled', true);
            contentExportTextareas.prop('disabled', true);
        }

        function afterLibraryExport(data) {
            libraryExportLoader.hide();
            libraryInputs.prop('disabled', false);
            libraryButtons.prop('disabled', false);
            contentExportInputs.prop('disabled', false);
            contentExportButtons.prop('disabled', false);
            contentExportTextareas.prop('disabled', false);
            contentExportElement.show();
            setMessage(data, libraryExportMsg);
        }

        function beforeLibraryUpload() {
            libraryUploadLoader.show();
            libraryUploadMsg.empty();
            libraryInputs.prop('disabled', true);
            libraryButtons.prop('disabled', true);
            contentExportInputs.prop('disabled', true);
            contentExportButtons.prop('disabled', true);
            contentExportTextareas.prop('disabled', true);
        }

        function afterLibraryUpload(data) {
            libraryUploadLoader.hide();
            libraryInputs.prop('disabled', false);
            libraryButtons.prop('disabled', false);
            contentExportInputs.prop('disabled', false);
            contentExportButtons.prop('disabled', false);
            contentExportTextareas.prop('disabled', false);
            contentExportElement.show();
            setMessage(data, libraryUploadMsg);
        }

        function beforeContentExport() {
            contentLoader.show();
            exportContentMsg.empty();
            contentExportInputs.prop('disabled', true);
            contentExportButtons.prop('disabled', true);
            contentExportTextareas.prop('disabled', true);
        }

        function afterContentExport (data) {
            contentLoader.hide();
            contentExportInputs.prop('disabled', false);
            contentExportButtons.prop('disabled', false);
            contentExportTextareas.prop('disabled', false);
            setMessage(data, exportContentMsg);
        }
    });
})(window.app = window.app || {});