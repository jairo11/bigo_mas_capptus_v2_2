(function (app) {
    jQuery(document).ready(function(){
        var metadataExportLoader = jQuery("#metadataExportLoader");
        var metadataUploadLoader = jQuery("#metadataUploadLoader");
        var contentLoader = jQuery("#contentLoader");
        var metadataExportMsg = jQuery("#metadataExportMsg");
        var metadataUploadMsg = jQuery("#metadataUploadMsg");
        var customExportMsg = jQuery("#customExportMsg");
        var metadataInputs = jQuery('#metadata input');
        var metadataButtons = jQuery('#metadata button');
        var exportObjectsInputs = jQuery('#exportObjects input');
        var exportObjectsButtons = jQuery('#exportObjects button');
        var exportObjectsTextareas = jQuery('#exportObjects textarea');
        var selectObjectsUI = jQuery('#objectsExportUI_SelectObj');
        var selectSystemObjectsTable = jQuery('#selectSystemObjectsTable tbody');
        var selectCustomObjectsTable = jQuery('#selectCustomObjectsTable tbody');
        var systemObjectsQuantity = selectSystemObjectsTable.find('.items-tr').length;
        var customObjectsQuantity = selectCustomObjectsTable.find('.items-tr').length;
        var pageNumberSysObj = 1;
        var pageNumberCustObj = 1;
        var pageSize = parseInt(app.preferences.metadataPageSize, 10);
        var expandedSystemObjects = {1:[]};
        var expandedCustomObjects = {1:[]};
        var UINavigation = app.components.UINavigation;

        jQuery('#metadataExport').on('submit', metadataExport);
        jQuery('#metadataUpload').on('submit', metadataUpload);
        jQuery('#exportObjects').on('submit', exportObjects);
        jQuery('input[name="uploadFile"]').on('change', setMetadataUploadFileName);
        jQuery('input[name="metadataExportFileName"]').on('keyup', setMetadataExportFileName);
        jQuery('.tab').on('click', UINavigation.openTab);
        initPageNavigationEvents();

        displayItemsBlock(selectSystemObjectsTable, systemObjectsQuantity, pageNumberSysObj);
        displayItemsBlock(selectCustomObjectsTable, customObjectsQuantity, pageNumberCustObj);
        UINavigation.orginizeItemsIntoPages(selectSystemObjectsTable, systemObjectsQuantity, pageNumberSysObj, pageSize, expandedSystemObjects);
        UINavigation.orginizeItemsIntoPages(selectCustomObjectsTable, customObjectsQuantity, pageNumberCustObj, pageSize, expandedCustomObjects);
        /**
         * Search for attributes and attribute groups in object details
         */
        function searchSubItems() {
            var input = jQuery(this);
            var subItemsType = input.attr('data-subItemsType');
            var relatedSubItems = input.closest('tr.details-tr').find('.subItems.' + subItemsType + ' tr');
            var val = input.val().toLowerCase();
            relatedSubItems.hide();
            jQuery(relatedSubItems).each(function(){
            var text = jQuery(this).text().toLowerCase();
                if(text.indexOf(val) != -1) {
                    jQuery(this).show();
                }
            });
        }
        /**
         * Displays a fixed-size block of items
          *
          * @param {Object} itemsTable - table that contains all items and sub items to be paginated
          * @param {Number} objectsQuantity - number of slots found
          * @param {Number} pageNumber - current page number
          */
        function displayItemsBlock(itemsTable, objectsQuantity, pageNumber) {
            var itemsBlock = itemsTable.find('.items-tr');
            var detailsBlock = itemsTable.find('.details-tr');
            itemsBlock.hide();
            detailsBlock.hide();
            var startPoint = (pageNumber - 1) * pageSize;
            var endPoint;
            if (objectsQuantity - startPoint < pageSize) {
                endPoint = startPoint + objectsQuantity - startPoint;
            } else {
                endPoint = startPoint + (pageSize > objectsQuantity ? objectsQuantity : pageSize);
            }
            for (var i = startPoint; i < endPoint; i++) {
                var objectID = itemsBlock[i].attributes['data-objectID'].value;
                showExpandedObjectDetails(objectID);
                itemsBlock[i].show();
            }
        }
        /**
         * Adds autocheck for items:
         *     1 - Check/uncheck all item deails selectboxes when detail item is checked/unchecked.
         *     2 - Semi-check item's selectbox if one or several detail items checked.
         *     3 - Check item's selectbox when all detail items are checked.
         *     4 - Uncheck item's selectbox if detail item select boxes unchecked.
         */
        function checkRelatedItems(e) {
            if (e.target.tagName === 'INPUT') {
                var checkBox = jQuery(e.target);
                var checked = checkBox.prop('checked');
                var checkBoxType = checkBox.attr('data-checkBoxType');
                var objectTR = jQuery(this);
                var objectID = objectTR.attr('data-objectID');
                var detailsTR = jQuery('#' + objectID + '-details');
                if (checkBoxType === 'subItem') {
                    var objectCheckBox = jQuery('#' + objectID + '-object').find('input[type="checkbox"]');
                    var detailsAmount = detailsTR.find('input:checkbox').length;
                    var checkedDetailsAmount = detailsTR.find('input:checkbox:checked').length;
                    if (checked) {
                        if (checkedDetailsAmount === detailsAmount) {
                            objectCheckBox.prop('indeterminate', false);
                            objectCheckBox.prop('checked', true);
                        } else {
                            objectCheckBox.prop('indeterminate', true);
                        }
                    } else {
                        if (checkedDetailsAmount === 0) {
                            objectCheckBox.prop('indeterminate', false);
                            objectCheckBox.prop('checked', false);
                        } else {
                            objectCheckBox.prop('indeterminate', true);
                        }
                    }
                } else {
                    detailsTR.find('input[type="checkbox"]').prop('checked', checked);
                }
            }
        }
        /**
         * Expands and minimizes slot's configurations by user click and stores the expanded slot IDs for each page
         *
         * @param {Object} e - event object
         */
        function showHideObjectDetails(e) {
            if (e.target.tagName === 'IMG') {
                var objectTR = jQuery(this);
                var image = jQuery(e.target);
                var objectID = objectTR.attr('data-objectID');
                var objectType = objectTR.attr('data-objectType');
                var detailsSelector = '#' + objectID + '-details';
                image.hide();
                var pageNumber = objectType == 'system' ? pageNumberSysObj : pageNumberCustObj;
                if (image.hasClass('expand')) {
                    objectTR.find('.minimize').show();
                    jQuery(detailsSelector).show();
                    objectType == 'system' ?
                        expandedSystemObjects[pageNumberSysObj].push(objectID) :
                        expandedCustomObjects[pageNumberCustObj].push(objectID) ;
                } else {
                    objectTR.find('.expand').show();
                    jQuery(detailsSelector).hide();
                    objectType == 'system' ?
                        expandedSystemObjects[pageNumberSysObj].splice(expandedSystemObjects[pageNumberSysObj].indexOf(objectID), 1) :
                        expandedCustomObjects[pageNumberCustObj].splice(expandedCustomObjects[pageNumberCustObj].indexOf(objectID), 1);
                }
            }
        }
        /**
         * Display expanded slot's configuration for current page
         *
         * @param {String} slotID
         */
        function showExpandedObjectDetails(objectID) {
            var objectSelector = '#' + objectID + '-object';
            var detailsSelector = '#' + objectID + '-details';
            if (expandedSystemObjects[pageNumberSysObj].indexOf(objectID) !== -1) {
                jQuery(objectSelector).find('.minimize').show();
                jQuery(detailsSelector).show();
                expandedSystemObjects[pageNumberSysObj].push(objectID);
            }
            if (expandedCustomObjects[pageNumberCustObj].indexOf(objectID) !== -1) {
                jQuery(objectSelector).find('.minimize').show();
                jQuery(detailsSelector).show();
                expandedCustomObjects[pageNumberCustObj].push(objectID);
            }
        }
        /**
         * Displays the block of objects for requested page
         *
         * @param {Object} e - event object
         */
        function getPage(e) {
            var itemsTable = this.itemsTable;
            var objectsQuantity = this.objectsQuantity;
            var type = jQuery(e.target).closest('.navigation-buttons').attr('data-buttonType');
            var pageNumber = type == 'system' ? pageNumberSysObj : pageNumberCustObj;
            var buttonType = jQuery(e.target).attr('data-buttonType');
            switch(buttonType) {
                case 'next':
                    pageNumber++;
                    type == 'system' ? pageNumberSysObj++ : pageNumberCustObj++ ;
                    UINavigation.updateNextPrevButtonsState(itemsTable, objectsQuantity, pageNumber, pageSize);
                    UINavigation.setSelectedPage(e, pageNumber, itemsTable);
                    displayItemsBlock(itemsTable, objectsQuantity, pageNumber);
                break;
                case 'prev':
                    pageNumber--;
                    type == 'system' ? pageNumberSysObj-- : pageNumberCustObj--;
                    UINavigation.updateNextPrevButtonsState(itemsTable, objectsQuantity, pageNumber, pageSize);
                    UINavigation.setSelectedPage(e, pageNumber, itemsTable);
                    displayItemsBlock(itemsTable, objectsQuantity, pageNumber);
                break;
                case 'page-number':
                    pageNumber = e.target.innerText;
                    type == 'system' ? (pageNumberSysObj = e.target.innerText) : (pageNumberCustObj = e.target.innerText);
                    UINavigation.updateNextPrevButtonsState(itemsTable, objectsQuantity, pageNumber, pageSize);
                    UINavigation.setSelectedPage(e, pageNumber, itemsTable);
                    displayItemsBlock(itemsTable, objectsQuantity, pageNumber);
                break;
            }
        }
        /**
         * Export current site data for further use in export
         *
         * @param {Object} e - event object
         */
        function metadataExport(e) {
            e.preventDefault();
            var form = jQuery(this);
            var data = form.serialize();
            var fileName = jQuery('input[name="metadataExportFileName"]').val();
            if (fileName != '') {
               jQuery.ajax({
                    type: 'POST',
                    url: form.attr('action'),
                    data: data,
                    beforeSend: beforeMetadataExport
                })
                .done(displaySiteObjects);
            } else {
                setMessage({
                    exportMsg: 'Please set export file name.'
                }, metadataExportMsg);
            }
        }
        /**
         * Upload external data for further use in export
         *
         * @param {Object} e - event object
         */
        function metadataUpload(e) {
            e.preventDefault();
            var form = jQuery(this);
            var data = new FormData(form[0]);
            var fileName = jQuery('input[name="uploadFile"]').val();
            if (fileName != '') {
                jQuery.ajax({
                    type: 'POST',
                    url: form.attr('action'),
                    data: data,
                    processData: false,
                    contentType: false,
                    beforeSend: beforeMetadataUpload
                })
                .done(displaySiteObjects);
            } else {
                setMessage({
                    exportMsg: 'Please select upload file.'
                }, metadataUploadMsg);
            }
        }
        /**
         * Calls custom export logic from BD for different types of UI
         *
         * @param {Object} e - event object
         */
        function exportObjects(e) {
            e.preventDefault();
            var exportType_UI = jQuery('.tab .tablinks.active').attr('data-uiid');
            if (jQuery('input[name="exportFileName"]').val() == '') {
                setMessage({
                    exportMsg: 'Please set an export file name.'
                }, customExportMsg);
            } else if (exportType_UI === 'objectsExportUI_Manual') {
                if (!jQuery('input[name="exportType"]').is(':checked')) {
                    setMessage({
                        exportMsg: 'Please select an export type.'
                    }, customExportMsg);
                } else if (jQuery('input[value="systemSpecific"][name="exportType"]').is(':checked') && jQuery("#systemObjectIDs").val() == '' ||
                        jQuery('input[value="customSpecific"][name="exportType"]').is(':checked') && jQuery("#customObjectIDs").val() == '') {
                    setMessage({
                        exportMsg: 'Please specify a search value.'
                    }, customExportMsg);
                } else {
                    var form = jQuery(this);
                    var data = form.serialize();
                    jQuery.ajax({
                        type: 'POST',
                        url: form.attr('action'),
                        data: data,
                        beforeSend: beforeExportObjects
                    })
                    .done(afterExportObjects);
                }
            } else if (exportType_UI === 'objectsExportUI_SelectSysObj') {
                var systemObjects = [];
                var objectsBlock = selectSystemObjectsTable.find('input[name="systemObjectSelected"]:checked, input[name="systemObjectSelected"]:indeterminate');
                var exportFileName = jQuery('input[name="exportFileName"]').val();
                var form = jQuery(this);
                jQuery.each(objectsBlock, function (index, input) {
                    var objectID = input.value;
                    var detailsBlock = selectSystemObjectsTable.find('tr[data-objectID="'+objectID+'"]');
                    var systemAttributesInputs = detailsBlock.find('input[name="sys_systemAttributesCheck"]:checked');
                    var customAttributesInputs = detailsBlock.find('input[name="sys_customAttributesCheck"]:checked');
                    var customAttributeGroupsInputs = detailsBlock.find('input[name="sys_attributeGroupsCheck"]:checked');
                    var customAttributes = [];
                    var attributeGroups = [];
                    jQuery.each(customAttributesInputs, function (index, input) {
                        customAttributes.push(input.value);
                    });
                    jQuery.each(customAttributeGroupsInputs, function (index, input) {
                        attributeGroups.push(input.value);
                    });
                    systemObjects.push({
                        ID: objectID,
                        systemAttributes: systemAttributesInputs.length == 1,
                        customAttributes: customAttributes,
                        attributeGroups: attributeGroups
                    });
                });
                if (systemObjects.length) {
                    jQuery.ajax({
                        type: 'POST',
                        url: form.attr('action'),
                        dataType: 'json',
                        contentType: 'application/json',
                        data: JSON.stringify({systemObjects: systemObjects, exportFileName: exportFileName, exportType_UI: exportType_UI}),
                        beforeSend: beforeExportObjects
                    })
                    .done(afterExportObjects);
                } else {
                    setMessage({
                        exportMsg: 'Please select objects to export.'
                    }, customExportMsg);
                }
            } else if (exportType_UI === 'objectsExportUI_SelectCustObj') {
                var customObjects = [];
                var objectsBlock = selectCustomObjectsTable.find('input[name="customObjectSelected"]:checked, input[name="customObjectSelected"]:indeterminate');
                var exportFileName = jQuery('input[name="exportFileName"]').val();
                var form = jQuery(this);
                jQuery.each(objectsBlock, function (index, input) {
                    var objectID = input.value;
                    var detailsBlock = selectCustomObjectsTable.find('tr[data-objectID="'+objectID+'"]');
                    var customAttributesInputs = detailsBlock.find('input[name="cust_AttributesCheck"]:checked');
                    var customAttributeGroupsInputs = detailsBlock.find('input[name="cust_attributeGroupsCheck"]:checked');
                    var customAttributes = [];
                    var attributeGroups = [];
                    jQuery.each(customAttributesInputs, function (index, input) {
                        customAttributes.push(input.value);
                    });
                    jQuery.each(customAttributeGroupsInputs, function (index, input) {
                        attributeGroups.push(input.value);
                    });
                    customObjects.push({
                        ID: objectID,
                        customAttributes: customAttributes,
                        attributeGroups: attributeGroups
                    });
                });
                if (customObjects.length) {
                    jQuery.ajax({
                        type: 'POST',
                        url: form.attr('action'),
                        dataType: 'json',
                        contentType: 'application/json',
                        data: JSON.stringify({customObjects: customObjects, exportFileName: exportFileName, exportType_UI: exportType_UI}),
                        beforeSend: beforeExportObjects
                    })
                    .done(afterExportObjects);
                } else {
                    setMessage({
                        exportMsg: 'Please select objects to export.'
                    }, customExportMsg);
                }
            }
        }
        /**
         * Refresh slots data after exporting and displaying new slots table
         */
        function updatePageData() {
            selectSystemObjectsTable = jQuery('#selectSystemObjectsTable tbody');
            selectCustomObjectsTable = jQuery('#selectCustomObjectsTable tbody');
            systemObjectsQuantity = selectSystemObjectsTable.find('.items-tr').length;
            customObjectsQuantity = selectCustomObjectsTable.find('.items-tr').length;
            pageNumberSysObj = 1;
            pageNumberCustObj = 1;
            expandedSystemObjects = {1:[]};
            expandedCustomObjects = {1:[]};
            initPageNavigationEvents();
            displayItemsBlock(selectSystemObjectsTable, systemObjectsQuantity, pageNumberSysObj);
            displayItemsBlock(selectCustomObjectsTable, customObjectsQuantity, pageNumberCustObj);
            UINavigation.orginizeItemsIntoPages(selectSystemObjectsTable, systemObjectsQuantity, pageNumberSysObj, pageSize, expandedSystemObjects);
            UINavigation.orginizeItemsIntoPages(selectCustomObjectsTable, customObjectsQuantity, pageNumberCustObj, pageSize, expandedCustomObjects);
        }
        /**
         * Initialize page navigation event listeners
         */
        function initPageNavigationEvents() {
            jQuery('.items-tr').on('click', showHideObjectDetails);
            jQuery('.items-tr, .details-tr').on('click', checkRelatedItems);
            jQuery('input[name="searchSubItems"]').on('keyup', searchSubItems);
            jQuery('#page-buttons-system-obj').on('click', getPage.bind({
                itemsTable: selectSystemObjectsTable,
                objectsQuantity: systemObjectsQuantity
            }));
            jQuery('#page-buttons-custom-obj').on('click', getPage.bind({
                itemsTable: selectCustomObjectsTable,
                objectsQuantity: customObjectsQuantity
            }));
        }
        /**
         * Displays an name format for the future uploaded file
         */
        function setMetadataUploadFileName(e) {
            var fileNameElement = jQuery("#metadataFilePath .fileName");
            var fileName = e.target.files[0].name;
            if (fileName.indexOf('.xml') !== -1) {
                fileName = fileName.replace('.xml', '');
            }
            fileNameElement.html(fileName + '-timestamp.xml');
        }
        /**
         * Displays an name format for the site library export file
         */
        function setMetadataExportFileName() {
            var fileNameElement = jQuery("#metadataFilePath .fileName");
            var inputValue = jQuery(this).val();
            fileNameElement.html(inputValue + '-timestamp.xml');
            if (inputValue == '') {
                fileNameElement.empty();
            }
            if (inputValue.indexOf('.xml') !== -1) {
                fileNameElement.html(inputValue.replace('.xml', '') + '-timestamp.xml');
            }
        }
        /**
         * Display site system & custom objects after data export
         *
         * @param {Object} dataMsg - response object from metadataExport execution
         */
        function displaySiteObjects(dataMsg) {
            jQuery.ajax({
                type: 'GET',
                url: app.urls.displaySiteObjects
            })
            .done(function(response) {
                if (dataMsg.type === 'metadata-export') {
                    afterMetadataExport(dataMsg);
                }
                if (dataMsg.type === 'metadata-upload') {
                    afterMetadataUpload(dataMsg);
                }
                selectObjectsUI.html(response);
                jQuery('#customExportTable').show();
                updatePageData();
            });
        }

        function beforeMetadataExport() {
            metadataExportLoader.show();
            metadataExportMsg.empty();
            metadataInputs.prop('disabled', true);
            metadataButtons.prop('disabled', true);
            exportObjectsInputs.prop('disabled', true);
            exportObjectsButtons.prop('disabled', true);
            exportObjectsTextareas.prop('disabled', true);
        }

        function afterMetadataExport(data) {
            metadataExportLoader.hide();
            metadataInputs.prop('disabled', false);
            metadataButtons.prop('disabled', false);
            exportObjectsInputs.prop('disabled', false);
            exportObjectsButtons.prop('disabled', false);
            exportObjectsTextareas.prop('disabled', false);
            setMessage(data, metadataExportMsg);
        }

        function beforeMetadataUpload() {
            metadataUploadLoader.show();
            metadataUploadMsg.empty();
            metadataInputs.prop('disabled', true);
            metadataButtons.prop('disabled', true);
            exportObjectsInputs.prop('disabled', true);
            exportObjectsButtons.prop('disabled', true);
            exportObjectsTextareas.prop('disabled', true);
        }

        function afterMetadataUpload(data) {
            metadataUploadLoader.hide();
            metadataInputs.prop('disabled', false);
            metadataButtons.prop('disabled', false);
            exportObjectsInputs.prop('disabled', false);
            exportObjectsButtons.prop('disabled', false);
            exportObjectsTextareas.prop('disabled', false);
            setMessage(data, metadataUploadMsg);
        }

        function beforeExportObjects() {
            contentLoader.show();
            customExportMsg.empty();
            exportObjectsInputs.prop('disabled', true);
            exportObjectsButtons.prop('disabled', true);
            exportObjectsTextareas.prop('disabled', true);
        }

        function afterExportObjects(data) {
            contentLoader.hide();
            exportObjectsInputs.prop('disabled', false);
            exportObjectsButtons.prop('disabled', false);
            exportObjectsTextareas.prop('disabled', false);
            setMessage(data, customExportMsg);
        }
    });
})(window.app = window.app || {});