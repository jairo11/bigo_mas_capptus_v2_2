/**
* Description of the Controller and the logic it provides
*
* @module  controllers/LibraryExport
*/

"use strict";

// HINT: do not put all require statements at the top of the file
// unless you really need them for all functions
/* SFCC API */
var File = require("dw/io/File");
var FileWriter = require("dw/io/FileWriter");
var FileReader = require("dw/io/FileReader");
var Logger = require("dw/system/Logger").getLogger("ExportTool", "contentExport");
var Pipelet = require("dw/system/Pipelet");

var XMLStreamReader = require("dw/io/XMLStreamReader");
var XMLStreamConstants = require("dw/io/XMLStreamConstants");
var XMLIndentingStreamWriter = require("dw/io/XMLIndentingStreamWriter");
var ArrayList = require("dw/util/ArrayList");
var ISML = require("dw/template/ISML");
/* Script Modules */
var guard = require("~/cartridge/scripts/guard");
var response = require("~/cartridge/scripts/util/Response");
var XMLParser = require("~/cartridge/scripts/XMLParser");
var exportHelper = require("~/cartridge/scripts/exportHelper");
/* Global variables */
var site = dw.system.Site.getCurrent();
var httpsHostName = site.getHttpsHostName();
var siteLink = "https://" + httpsHostName + "/on/demandware.servlet/webdav/Sites";
var WORKING_FOLDER = [File.IMPEX, "src", "export-tool", "slots-export"].join(File.SEPARATOR);
var workingFolder = new File(WORKING_FOLDER);
if (!workingFolder.exists()) {
    workingFolder.mkdirs();
}
/**
 * Shows the content export main panel.
 */
function show() {
    var slots = session.privacy.slots ? session.privacy.slots : null;
    var siteSlots = slots ? slots.content : getSlots();
    slotsFilePath = slots ? siteLink + slots.file.fullPath : null;
    ISML.renderTemplate("slotsexport", {
        slotsFilePath: slotsFilePath,
        slots: siteSlots
    });
}
/**
 * Render the select slots UI
 */
function displaySiteSlots() {
    var siteSlots = session.privacy.slots.content;
    ISML.renderTemplate("slotsexport/selectslots", {
        slots: siteSlots
    });
}
/**
 * Render the slots found UI
 */
function displaySlotsFound() {
    var contentFound = session.privacy.slots.contentFound;
    ISML.renderTemplate("slotsexport/slotsfound", {
        contentFound: contentFound
    });
}
/**
 * Exports current site slots and puts it in the session
 *
 * @return {Object} response - JSON result of execution
 */
function exportSiteData() {
    var params = request.httpParameterMap;
    var fileName = exportHelper.formatName(params.exportSiteDataFileName.stringValue);
    var file = new File(workingFolder, fileName);
    var exportFileRelPath = ["export-tool", workingFolder.name, fileName].join(File.SEPARATOR);
    var result = {};

    if (!empty(fileName)) {
        var ExportSlotsResult = new Pipelet("ExportSlots").execute({
            ExportFile: exportFileRelPath
        });
        if (ExportSlotsResult.result === PIPELET_NEXT) {
            session.privacy.slots = {
                file: file,
                isUploaded: false
            };
            session.privacy.slots.content = getSlots();
            var url = siteLink + file.getFullPath();
            result = {
                type: "slots-export",
                success: true,
                exportMsg: "Export successfully finished.",
                url: url
            };
        }
        if (ExportSlotsResult.result === PIPELET_ERROR) {
            result = {
                success: false,
                exportMsg: "Failed to export files."
            };
        }
    } else {
        result = {
            success: false,
            exportMsg: "Please set an export file name.",
        };
    }
    return response.renderJSON(result);
}
/**
 * Uploads a slots file and puts it to the session
 *
 * @return {Object} response - JSON result of execution
 */
function uploadSlots() {
    var result = exportHelper.uploadFile("slots", workingFolder);
    session.privacy.slots.content = getSlots();
    return response.renderJSON(result);
}
/**
 * Provides slots export based on user preference
 *
 * @return {Object} response - JSON result of execution
 */
function customSlotsExport() {
    var contentRequested = [];
    var contentFound = [];
    var slotIDs = [];
    var params = request.httpParameterMap;
    try {
        var requestBodyAsString = JSON.parse(params.requestBodyAsString);
        var contentRequested = requestBodyAsString.slots;
        var exportFileName = requestBodyAsString.exportFileName;
    } catch (error) {
        Logger.error("[Export Tool :: customSlotsExport] An error occured: {0} In file: {1}, at line: {2}", error.message, error.fileName, error.lineNumber);
    }
    var slotsExportData = request.session.privacy.slots;
    if (!slotsExportData.file) {
        return response.renderJSON({
            success: false,
            exportMsg: "Export or upload a library at first."
        });
    }
    if (empty(exportFileName)) {
        return response.renderJSON({
            success: false,
            exportMsg: "Export file name is required."
        });
    }
    var fileReader = new FileReader(slotsExportData.file, "UTF-8");
    var xmlStreamReader = new XMLStreamReader(fileReader);
    var file = new File(workingFolder, exportHelper.formatName(exportFileName));
    var writer = new FileWriter(file);
    var Iwriter = new XMLIndentingStreamWriter(writer);
    Iwriter.writeStartDocument("UTF-8", "1.0");
    Iwriter.writeStartElement("slot-configurations");
    Iwriter.writeAttribute("xmlns", "http://www.demandware.com/xml/impex/slot/2008-09-08");
    Iwriter.writeCharacters("");
    while (xmlStreamReader.hasNext()) {
        if (xmlStreamReader.next() == XMLStreamConstants.START_ELEMENT) {
            try {
                if (xmlStreamReader.getLocalName() === "slot-configuration") {
                    var XMLObject = xmlStreamReader.readXMLObject();
                    var currentSlot = {
                        slotID: XMLObject.attribute("slot-id").toString(),
                        configurationID: XMLObject.attribute("configuration-id").toString(),
                        context: XMLObject.attribute("context").toString(),
                        contextID: XMLObject.attribute("context-id").toString(),
                        assignedToSite: XMLObject.attribute("assigned-to-site").toString()
                    };
                    contentRequested.forEach(function (slot) {
                        if (currentSlot.slotID === slot.slotID &&
                                currentSlot.configurationID === slot.configurationID &&
                                currentSlot.context === slot.context &&
                                currentSlot.contextID === slot.contextID &&
                                currentSlot.assignedToSite === slot.assignedToSite) {
                            Iwriter.writeRaw(XMLObject.toXMLString());
                            contentFound.push(currentSlot);
                            slotIDs.push(slot.slotID);
                        }
                    });
                }
            } catch (error) {
                Logger.error("[Export Tool :: customSlotsExport] An error occured: {0} In file: {1}, at line: {2}", error.message, error.fileName, error.lineNumber);
            }
        }
    }
    Iwriter.writeEndElement();
    Iwriter.writeEndDocument();
    Iwriter.flush();
    Iwriter.close();

    for (var i = 0; i < contentRequested.length; i++) {
        if (slotIDs.indexOf(contentRequested[i].slotID) == -1) {
            contentNotFound.push(contentRequested[i]);
        }
    }
    var url = siteLink + file.getFullPath();
    request.session.privacy.slots.contentFound = contentFound;
    return response.renderJSON({
        success: true,
        url: url,
        exportMsg: "Export successfully completed."
    });
}
/**
 * Get a list of slots
 */
function getSlots() {
    var slots = session.privacy.slots ? session.privacy.slots : [];
    var slotIDs = [];
    var slotObjects = [];
    try {
        var fileReader = new FileReader(slots.file, "UTF-8");
        var xmlStreamReader = new XMLStreamReader(fileReader);
        while (xmlStreamReader.hasNext()) {
            if (xmlStreamReader.next() == XMLStreamConstants.START_ELEMENT) {
                if (xmlStreamReader.getLocalName() === "slot-configuration") {
                    var XMLObject = xmlStreamReader.readXMLObject();
                    var slotID = XMLObject.attribute("slot-id").toString();
                    var context = XMLObject.attribute("context").toString();
                    var contextID = XMLObject.attribute("context-id").toString();
                    var configurationID = XMLObject.attribute("configuration-id").toString();
                    var assignedToSite = XMLObject.attribute("assigned-to-site").toString();
                    var slot = {
                        slotID: slotID,
                        configurationID: configurationID,
                        context: context,
                        contextID: contextID,
                        assignedToSite: assignedToSite
                    };
                    if (slotIDs.indexOf(slotID) === -1) {
                        slotIDs.push(slotID);
                    }
                    slotObjects.push(slot);
                }
            }
        }
    } catch (error) {
        Logger.error("[Export Tool :: getSlots] An error occured: {0} In file: {1}, at line: {2}", error.message, error.fileName, error.lineNumber);
    }
    return transformSlotObjects(slotObjects, slotIDs);
}
/**
 * Sorts a slots object by ID in alphabetical order
 *
 * @return {Object} slotObjects - JSON result of execution
 * @return {Object} slotIDs - JSON result of execution
 */
function transformSlotObjects(slotObjects, slotIDs) {
    var slotObjectsTransformed = [];
    slotIDs.sort();
    slotIDs.forEach(function (slotID) {
        var slotTransformed = {
            ID: slotID,
            configurations: []
        };
        slotObjects.forEach(function (slot) {
            if (slot.slotID === slotID) {
                var configuration = {
                    ID: slot.configurationID,
                    context: slot.context,
                    contextID: slot.contextID,
                    assignedToSite: slot.assignedToSite
                };
                slotTransformed.configurations.push(configuration);
            }
        });
        slotTransformed.configurations.sort(function (a, b) {
            var idA = a.ID.toUpperCase();
            var idB = b.ID.toUpperCase();
            return (idA < idB) ? -1 : (idA > idB) ? 1 : 0;
        });
        slotObjectsTransformed.push(slotTransformed);
    });
    return slotObjectsTransformed;
}

/* Exports of the controller */
/** Shows the main panel
 * @see {@link module:controllers/SlotsExport~show} */
exports.Show = guard.ensure(["https", "get"], show);
/** Exports current site library
 * @see {@link module:controllers/SlotsExport~exportSiteData} */
exports.ExportSiteData = guard.ensure(["https", "post"], exportSiteData);
/** Exports required slots
* @see {@link module:controllers/SlotsExport~customSlotsExport} */
exports.CustomSlotsExport = guard.ensure(["https", "post"], customSlotsExport);
/** Uploads selected file
 * @see {@link module:controllers/SlotsExport~uploadSlots} */
exports.UploadSlots = guard.ensure(["https", "post"], uploadSlots);
/** Exports required content (folders/assets) depending on user preference
 * @see {@link module:controllers/SlotsExport~displaySiteSlots} */
exports.DisplaySiteSlots = guard.ensure(["https", "get"], displaySiteSlots);
/** Exports required content (folders/assets) depending on user preference
 * @see {@link module:controllers/SlotsExport~displaySlotsFound} */
exports.DisplaySlotsFound = guard.ensure(["https", "get"], displaySlotsFound);
