/**
* Description of the Controller and the logic it provides
*
* @module  controllers/MetadataExport
*/

"use strict";

// HINT: do not put all require statements at the top of the file
// unless you really need them for all functions
/* SFCC API */
var File = require("dw/io/File");
var FileWriter = require("dw/io/FileWriter");
var FileReader = require("dw/io/FileReader");
var Logger = require("dw/system/Logger").getLogger("ExportTool", "metadataExport");
var Pipelet = require("dw/system/Pipelet");
var ContentMgr = require("dw/content/ContentMgr");
var Content = require("dw/content/Content");
var XMLStreamReader = require("dw/io/XMLStreamReader");
var XMLStreamConstants = require("dw/io/XMLStreamConstants");
var XMLIndentingStreamWriter = require("dw/io/XMLIndentingStreamWriter");
var ArrayList = require("dw/util/ArrayList");
var CustomObjectMgr = require("dw/object/CustomObjectMgr");
var ISML = require("dw/template/ISML");
/* Script Modules */
var guard = require("~/cartridge/scripts/guard");
var response = require("~/cartridge/scripts/util/Response");
var exportHelper = require("~/cartridge/scripts/exportHelper");
/* Global variables */
var site = dw.system.Site.getCurrent();
var httpsHostName = site.getHttpsHostName();
var siteLink = "https://" + httpsHostName + "/on/demandware.servlet/webdav/Sites";
var WORKING_FOLDER = [File.IMPEX, "src", "export-tool", "metadata-export"].join(File.SEPARATOR);
var workingFolder = new File(WORKING_FOLDER);
if (!workingFolder.exists()) {
    workingFolder.mkdirs();
}
/**
 * Shows the content export main panel.
 */
function show() {
    var metadata = session.privacy.metadata ? session.privacy.metadata : null;
    metadataFilePath = metadata ? siteLink + metadata.file.fullPath : null;
    var metadataObjectsList = metadata ? metadata.content : null;
    ISML.renderTemplate("metadataexport", {
        metadataFilePath: metadataFilePath,
        metadataObjectsList: metadataObjectsList
    });
}
/**
 * Exports current site metadata and puts it in the session (all objects case)
 *
 * @param {String} type - determines whether to export all, system or custom objects
 *
 * @return {Object} response - JSON result of execution
 */
function exportData(type) {
    var params = request.httpParameterMap;
    var fileName;
    var type = params.type.stringValue ? params.type.stringValue : type;
    var exportSystemTypes = true;
    var exportCustomTypes = true;
    var result = {};
    if (type !== "all") {
        fileName = exportHelper.formatName(params.exportFileName.stringValue);
        switch (type) {
            case "system":
                exportCustomTypes = false;
                break;
            case "custom" :
                exportSystemTypes = false;
                break;
        }
    } else {
        fileName = exportHelper.formatName(params.metadataExportFileName.stringValue);
    }
    var file = new File(workingFolder, fileName);
    var exportFileRelPath = ["export-tool", workingFolder.name, fileName].join(File.SEPARATOR);
    if (!empty(fileName)) {
        var ExportContentResult = new dw.system.Pipelet("ExportMetaData").execute({
            ExportFile: exportFileRelPath,
            ExportSystemTypes: exportSystemTypes,
            ExportCustomTypes: exportCustomTypes
        });
        if (ExportContentResult.result === PIPELET_NEXT) {
            var url = siteLink + file.getFullPath();
            if (type == "all") {
                session.privacy.metadata = {
                    file: file,
                    isUploadedMetadata: false
                };
                var metadataContent = getMetadataObjectsList();
                session.privacy.metadata.content = metadataContent;
            }
            result = {
                type: "metadata-export",
                success: true,
                exportMsg: "Export successfully finished.",
                url: url
            };
        }
        if (ExportContentResult.result === PIPELET_ERROR) {
            result = {
                success: false,
                exportMsg: "Failed to export files."
            };
        }
    } else {
        result = {
            success: false,
            exportMsg: "Please set an export file name.",
        };
    }
    return response.renderJSON(result);
}
/**
 * Uploads a metadata file and puts it to the session
 *
 * @return {Object} response - JSON result of execution
 */
function uploadFile() {
    var result = exportHelper.uploadFile("metadata", workingFolder);
    session.privacy.metadata.content = getMetadataObjectsList();
    return response.renderJSON(result);
}
/**
 * Provides metadata system/custom objects export based on user preference
 *
 * @return {Object} response - JSON result of execution
 */
function exportObjects() {
    var params = request.httpParameterMap;
    var exportSettings = buildExportSettingsObject(params);
    var exportFileName = exportSettings.exportFileName;
    var exportType = exportSettings.exportType;
    var metadata = request.session.privacy.metadata;
    if (!metadata.file) {
        return response.renderJSON({
            success: false,
            exportMsg: "Metadata not exported / Session expired"
        });
    }
    if (empty(exportFileName)) {
        return response.renderJSON({
            success: false,
            exportMsg: "Export file name is required."
        });
    }
    if (!exportType) {
        return response.renderJSON({
            success: false,
            exportMsg: "Select an export type."
        });
    }
    var contentRequested = [];
    var contentFound = [];
    var contentNotFound = [];
    var attributesRequested = [];
    var groupDefinitionsRequested = [];
    var fileReader = new FileReader(metadata.file, "UTF-8");
    var xmlStreamReader = new XMLStreamReader(fileReader);
    exportFileName = exportHelper.formatName(exportFileName);
    var file = new File(workingFolder, exportFileName);
    var writer = new FileWriter(file);
    var Iwriter = new XMLIndentingStreamWriter(writer);
    Iwriter.writeStartDocument("UTF-8", "1.0");
    Iwriter.writeStartElement("metadata");
    Iwriter.writeAttribute("xmlns", "http://www.demandware.com/xml/impex/metadata/2006-10-31");
    Iwriter.writeCharacters("");
    var objectProcessing = false;
    var systemAttributesExport = false;
    var objectSpecificAtributesExport = false;
    var firstAttributeDefinition = false;
    var firstAttributeGroupDefinition = false;
    var objectSpecificAtributeGroupsExport = false;
    var objectsWithAttributesFound = false;
    var objectsWithAttributeGroupsFound = false;
    var objectsFound = -1;
    var localName;
    if (exportType == "allSystem") {
        exportData("system");
        return;
    } else if (exportType == "allCustom") {
        exportData("custom");
        return;
    } else {
        while (xmlStreamReader.hasNext()) {
            var next = xmlStreamReader.next();
            if ([XMLStreamConstants.CHARACTERS, XMLStreamConstants.END_DOCUMENT].indexOf(next) == -1) {
                localName = xmlStreamReader.getLocalName();
            }
            if (next == XMLStreamConstants.START_ELEMENT) {
                try {
                    var objectID = xmlStreamReader.getAttributeValue(0);
                    if (exportType == "systemSpecific") {
                        if (exportSettings.contentRequested.map(function (object) { return object.ID; }).indexOf(objectID) !== -1 && localName == "type-extension" && !objectProcessing) {
                            var systemObject = {
                                designation: "",
                                attributesFound: [],
                                attributesNotFound: [],
                                groupDefinitionsFound: [],
                                groupDefinitionsNotFound: []
                            };
                            objectProcessing = true;
                            Iwriter.writeStartElement(localName);
                            Iwriter.writeAttribute("type-id", objectID);
                            Iwriter.writeCharacters("");
                            systemObject.designation = objectID;
                            contentFound.push(systemObject);
                            objectsFound++;
                        } else if (objectProcessing) {
                            if ((exportSettings.systemAttributesCheck || exportSettings.contentRequested[objectsFound].systemAttributes) && localName == "system-attribute-definitions") {
                                var XMLObject = xmlStreamReader.readXMLObject();
                                var XMLString = XMLObject.toXMLString();
                                Iwriter.writeRaw(XMLString);
                            }
                            if (exportSettings.customAttributesCheck && localName == "custom-attribute-definitions" && !objectSpecificAtributesExport) {
                                firstAttributeDefinition = true;
                                objectSpecificAtributesExport = true;
                            }
                            else if (objectSpecificAtributesExport) {
                                if (localName == "attribute-definition" && empty(exportSettings.contentRequested[objectsFound].customAttributes)) {
                                    if (firstAttributeDefinition) {
                                        Iwriter.writeStartElement("custom-attribute-definitions");
                                        Iwriter.writeCharacters("");
                                    }
                                    var XMLObject = xmlStreamReader.readXMLObject();
                                    var XMLString = XMLObject.toXMLString();
                                    Iwriter.writeRaw(XMLString);
                                    firstAttributeDefinition = false;
                                    objectsWithAttributesFound = true;
                                }
                                if (localName == "attribute-definition" && !empty(exportSettings.contentRequested[objectsFound].customAttributes)) {
                                    var XMLObject = xmlStreamReader.readXMLObject();
                                    var XMLString = XMLObject.toXMLString();
                                    var attributeID = XMLObject.attribute("attribute-id").toString();
                                    if (exportSettings.contentRequested[objectsFound].customAttributes.indexOf(attributeID) !== -1) {
                                        if (firstAttributeDefinition) {
                                            Iwriter.writeStartElement("custom-attribute-definitions");
                                            Iwriter.writeCharacters("");
                                        }
                                        Iwriter.writeRaw(XMLString);
                                        contentFound[objectsFound].attributesFound.push(attributeID);
                                        firstAttributeDefinition = false;
                                        objectsWithAttributesFound = true;
                                    }
                                }
                            }
                            if (exportSettings.systemGroupAttributesCheck && localName == "group-definitions" && !objectSpecificAtributeGroupsExport) {
                                firstAttributeGroupDefinition = true;
                                objectSpecificAtributeGroupsExport = true;
                            } else if (objectSpecificAtributeGroupsExport) {
                                if (localName == "attribute-group" && empty(exportSettings.contentRequested[objectsFound].attributeGroups)) {
                                    if (firstAttributeGroupDefinition) {
                                        Iwriter.writeStartElement("group-definitions");
                                        Iwriter.writeCharacters("");
                                    }
                                    var XMLObject = xmlStreamReader.readXMLObject();
                                    var XMLString = XMLObject.toXMLString();
                                    Iwriter.writeRaw(XMLString);
                                    firstAttributeGroupDefinition = false;
                                    objectsWithAttributeGroupsFound = true;
                                }
                                if (localName == "attribute-group" && !empty(exportSettings.contentRequested[objectsFound].attributeGroups)) {
                                    var XMLObject = xmlStreamReader.readXMLObject();
                                    var XMLString = XMLObject.toXMLString();
                                    var attributeGroupID = XMLObject.attribute("group-id").toString();
                                    if (exportSettings.contentRequested[objectsFound].attributeGroups.indexOf(attributeGroupID) !== -1) {
                                        if (firstAttributeGroupDefinition) {
                                            Iwriter.writeStartElement("group-definitions");
                                            Iwriter.writeCharacters("");
                                        }
                                        Iwriter.writeRaw(XMLString);
                                        contentFound[objectsFound].groupDefinitionsFound.push(attributeGroupID);
                                        firstAttributeGroupDefinition = false;
                                        objectsWithAttributeGroupsFound = true;
                                    }
                                }
                            }
                        }
                    } else if (exportType == "customSpecific") {
                        if (exportSettings.contentRequested.map(function (object) { return object.ID; }).indexOf(objectID) !== -1 && localName == "custom-type") {
                            var customObject = {
                                designation: objectID,
                                attributesFound: [],
                                attributesNotFound: [],
                                groupDefinitionsFound: [],
                                groupDefinitionsNotFound: []
                            };
                            var attributesFound = [];
                            var attributesNotFound = [];
                            var groupDefinitionsFound = [];
                            var groupDefinitionsNotFound = [];
                            var XMLElementsToWrite = new XML();
                            var XMLObject = xmlStreamReader.readXMLObject();
                            var objectElements = XMLObject.elements();
                            objectsFound++;
                            for (var i = 0; i < objectElements.length(); i++) {
                                if (objectElements[i].localName() == "attribute-definitions") {
                                    if (!empty(exportSettings.contentRequested[objectsFound].customAttributes)) {
                                        var XMLAttributeDefinitions = new XML();
                                        var XMLAttributesObject = objectElements[i];
                                        var attributeElements = XMLAttributesObject.elements();
                                        for (var a = 0; a < attributeElements.length(); a++) {
                                            var attributeID = attributeElements[a].attribute("attribute-id").toString();
                                            if (exportSettings.contentRequested[objectsFound].customAttributes.indexOf(attributeID) !== -1) {
                                                customObject.attributesFound.push(attributeID);
                                                XMLAttributeDefinitions += attributeElements[a];
                                            }
                                        }
                                        XMLAttributesObject.setChildren(XMLAttributeDefinitions);
                                        if (!empty(XMLAttributesObject.toString())) {
                                            XMLElementsToWrite += XMLAttributesObject;
                                        }
                                    } else {
                                        XMLElementsToWrite += objectElements[i];
                                    }
                                }
                                if (exportSettings.customGroupAttributesCheck && objectElements[i].localName() == "group-definitions") {
                                    if (!empty(exportSettings.contentRequested[objectsFound].attributeGroups)) {
                                        var XMLGroupDefinitions = new XML();
                                        var XMLGroupDefinitionsObject = objectElements[i];
                                        var groupDefinitionElements = XMLGroupDefinitionsObject.elements();
                                        for (var g = 0; g < groupDefinitionElements.length(); g++) {
                                            var attributeGroupID = groupDefinitionElements[g].attribute("group-id").toString();
                                            if (exportSettings.contentRequested[objectsFound].attributeGroups.indexOf(attributeGroupID) !== -1) {
                                                customObject.groupDefinitionsFound.push(attributeGroupID);
                                                XMLGroupDefinitions += groupDefinitionElements[g];
                                            }
                                        }
                                        XMLGroupDefinitionsObject.setChildren(XMLGroupDefinitions);
                                        if (!empty(XMLGroupDefinitionsObject.toString())) {
                                            XMLElementsToWrite += XMLGroupDefinitionsObject;
                                        }
                                    } else {
                                        XMLElementsToWrite += objectElements[i];
                                    }
                                }
                            }
                            contentFound.push(customObject);
                            Iwriter.writeRaw(XMLObject.setChildren(XMLElementsToWrite).toXMLString());
                        }
                    }
                } catch (error) {
                    Logger.error("[Export Tool :: Metadata Export] An error occured: {0} In file: {1}, at line: {2}", error.message, error.fileName, error.lineNumber);
                }
            } else if (next == XMLStreamConstants.END_ELEMENT) {
                if (localName == "custom-attribute-definitions" && objectsWithAttributesFound) {
                    Iwriter.writeEndElement();
                }
                if (localName == "group-definitions" && objectsWithAttributeGroupsFound) {
                    Iwriter.writeEndElement();
                }
                if (localName == "type-extension" && objectProcessing) {
                    Iwriter.writeEndElement();
                    objectProcessing = false;
                    objectSpecificAtributesExport = false;
                    objectSpecificAtributeGroupsExport = false;
                    objectsWithAttributesFound = 0;
                    objectsWithAttributeGroupsFound = 0;
                }
            }
        }
    }
    Iwriter.writeEndDocument();
    Iwriter.flush();
    Iwriter.close();
    if (exportType == "systemSpecific") {
        contentFound.forEach(function (object, i) {
            var requestedObject = exportSettings.contentRequested[i];
            for (var z = 0; z < requestedObject.customAttributes.length; z++) {
                if (object.attributesFound.indexOf(requestedObject.customAttributes[z]) == -1) {
                    object.attributesNotFound.push(requestedObject.customAttributes[z]);
                }
            }
            for (var x = 0; x < requestedObject.attributeGroups.length; x++) {
                if (object.groupDefinitionsFound.indexOf(requestedObject.attributeGroups[x]) == -1) {
                    object.groupDefinitionsNotFound.push(requestedObject.attributeGroups[x]);
                }
            }
        });
    }
    for (var i = 0; i < contentRequested.length; i++) {
        if (contentFound.map(function (object) { return object.designation; }).indexOf(contentRequested[i]) == -1) {
            contentNotFound.push(contentRequested[i]);
        }
    }
    var result = {};
    if (contentFound.length) {
        var url = siteLink + file.getFullPath();
        result = {
            type: "content",
            success: true,
            exportMsg: "Export successfuly completed.",
            url: url,
            contentFound: contentFound,
            contentNotFound: contentNotFound
        };
    } else {
        result = {
            success: false,
            exportMsg: "Nothing found."
        };
    }
    return response.renderJSON(result);
}

function buildExportSettingsObject(params) {
    var exportSettings = {
        exportFileName: "",
        contentRequested: []
    };
    if (params.requestBodyAsString) {
        try {
            var requestBodyAsString = JSON.parse(params.requestBodyAsString);
            var exportType_UI = requestBodyAsString.exportType_UI;
            exportSettings.exportFileName = requestBodyAsString.exportFileName;
            exportSettings.exportType = exportType_UI == "objectsExportUI_SelectSysObj" ? "systemSpecific" : "customSpecific";
            if (exportSettings.exportType == "systemSpecific") {
                exportSettings.contentRequested = requestBodyAsString.systemObjects;
            } else {
                exportSettings.contentRequested = requestBodyAsString.customObjects;
            }
        } catch (error) {
            Logger.error("[Export Tool :: buildExportSettingsObject] An error occured: {0} In file: {1}, at line: {2}", error.message, error.fileName, error.lineNumber);
        }
        exportSettings.systemAttributesCheck = false;
        exportSettings.customAttributesCheck = true;
        exportSettings.systemGroupAttributesCheck = true;
        exportSettings.customGroupAttributesCheck = true;
    } else {
        exportSettings.exportFileName = params.exportFileName.stringValue;
        exportSettings.exportType = params.exportType.stringValue;
        exportSettings.systemAttributesCheck = params.systemAttributesCheck == "on";
        exportSettings.customAttributesCheck = params.customAttributesCheck == "on";
        exportSettings.systemGroupAttributesCheck = params.systemGroupAttributesCheck == "on";
        exportSettings.customGroupAttributesCheck = params.customGroupAttributesCheck == "on";
        if (exportSettings.exportType == "systemSpecific") {
            params.systemObjectIDs.stringValue.split(",").forEach(function (item) {
                var customAttributes = params.systemObjectCustomAttributes.empty ? [] : params.systemObjectCustomAttributes.stringValue.split(",");
                var attributeGroups = params.systemObjectGroupDefinitions.empty ? [] : params.systemObjectGroupDefinitions.stringValue.split(",");
                exportSettings.contentRequested.push({
                    ID: item,
                    systemAttributes: exportSettings.systemAttributesCheck,
                    customAttributes: customAttributes,
                    attributeGroups: attributeGroups
                });
            });
        } else if (exportSettings.exportType == "customSpecific") {
            params.customObjectIDs.stringValue.split(",").forEach(function (item) {
                var customAttributes = params.customObjectAttributes.empty ? [] : params.customObjectAttributes.stringValue.split(",");
                var attributeGroups = params.customObjectGroupDefinitions.empty ? [] : params.customObjectGroupDefinitions.stringValue.split(",");
                exportSettings.contentRequested.push({
                    ID: item,
                    customAttributes: customAttributes,
                    attributeGroups: attributeGroups
                });
            });
        }
    }
    return exportSettings;
}

function getMetadataObjectsList() {
    var metadata = session.privacy.metadata;
    var metadataObjectsList = {
        systemObjects: [],
        customObjects: []
    };
    if (!metadata) {
        return metadataObjectsList;
    }
    var objectProcessing = false;
    var attributesProcessing = false;
    var attributeGroupsProcessing = false;
    var fileReader = new FileReader(session.privacy.metadata.file, "UTF-8");
    var xmlStreamReader = new XMLStreamReader(fileReader);
    var localName;
    var objectID;
    var attributeID;
    var attributeGroupID;
    var abstractObject = {};
    while (xmlStreamReader.hasNext()) {
        var next = xmlStreamReader.next();
        if ([XMLStreamConstants.CHARACTERS, XMLStreamConstants.END_DOCUMENT].indexOf(next) == -1) {
            localName = xmlStreamReader.getLocalName();
        }
        if (next == XMLStreamConstants.START_ELEMENT) {
            try {
                if (localName == "type-extension" && !objectProcessing) {
                    objectID = xmlStreamReader.getAttributeValue(0);
                    abstractObject = {
                        ID: "",
                        attributes: [],
                        attributeGroups: []
                    };
                    objectProcessing = true;
                } else if (objectProcessing) {
                    if (localName == "custom-attribute-definitions" && !attributesProcessing) {
                        attributesProcessing = true;
                    } else if (attributesProcessing) {
                        if (localName == "attribute-definition") {
                            var XMLObject = xmlStreamReader.readXMLObject();
                            attributeID = XMLObject.attribute("attribute-id").toString();
                        }
                    }
                    if (localName == "group-definitions" && !attributeGroupsProcessing) {
                        attributeGroupsProcessing = true;
                    } else if (attributeGroupsProcessing) {
                        if (localName == "attribute-group") {
                            var XMLObject = xmlStreamReader.readXMLObject();
                            attributeGroupID = XMLObject.attribute("group-id").toString();
                        }
                    }
                }
                if (localName == "custom-type" && !objectProcessing) {
                    var objectID = xmlStreamReader.getAttributeValue(0);
                    abstractObject = {
                        ID: "",
                        attributes: [],
                        attributeGroups: []
                    };
                    objectProcessing = true;
                } else if (objectProcessing) {
                    if (localName == "attribute-definitions" && !attributesProcessing) {
                        attributesProcessing = true;
                    } else if (attributesProcessing) {
                        if (localName == "attribute-definition") {
                            var XMLObject = xmlStreamReader.readXMLObject();
                            attributeID = XMLObject.attribute("attribute-id").toString();
                        }
                    }
                    if (localName == "group-definitions" && !attributeGroupsProcessing) {
                        attributeGroupsProcessing = true;
                    } else if (attributeGroupsProcessing) {
                        if (localName == "attribute-group") {
                            var XMLObject = xmlStreamReader.readXMLObject();
                            attributeGroupID = XMLObject.attribute("group-id").toString();
                        }
                    }
                }
            } catch (error) {
                Logger.error("[Export Tool :: getMetadataObjectsList] An error occured: {0} In file: {1}, at line: {2}", error.message, error.fileName, error.lineNumber);
            }
        } else if (next == XMLStreamConstants.END_ELEMENT) {
            if (localName == "type-extension" && objectProcessing) {
                objectProcessing = false;
                attributesProcessing = false;
                attributeGroupsProcessing = false;
                abstractObject.ID = objectID;
                metadataObjectsList.systemObjects.push(abstractObject);
                abstractObject = {};
            }
            if (localName == "custom-type" && objectProcessing) {
                objectProcessing = false;
                attributesProcessing = false;
                attributeGroupsProcessing = false;
                abstractObject.ID = objectID;
                metadataObjectsList.customObjects.push(abstractObject);
                abstractObject = {};
            }
        } else if (next == XMLStreamConstants.CHARACTERS) {
            if (localName == "attribute-definition" && attributesProcessing) {
                abstractObject.attributes.push(attributeID);
            }
            if (localName == "attribute-group" && attributeGroupsProcessing) {
                abstractObject.attributeGroups.push(attributeGroupID);
            }
        }
    }
    return metadataObjectsList;
}
function displaySiteObjects() {
    ISML.renderTemplate("metadataexport/selectobjects", {
        metadataObjectsList: session.privacy.metadata.content
    });
}
/* Exports of the controller */
/** Shows the main panel
 * @see {@link module:controllers/MetadataExport~show} */
exports.Show = guard.ensure(["https", "get"], show);
/** Exports current site metadata
 * @see {@link module:controllers/MetadataExport~exportData} */
exports.ExportData = guard.ensure(["https", "post"], exportData);
/** @see {@link module:controllers/MetadataExport~exportObjects} */
exports.ExportObjects = guard.ensure(["https", "post"], exportObjects);
/** @see {@link module:controllers/MetadataExport~uploadFile} */
exports.UploadFile = guard.ensure(["https", "post"], uploadFile);
/** @see {@link module:controllers/MetadataExport~displaySiteObjects} */
exports.DisplaySiteObjects = guard.ensure(["https", "get"], displaySiteObjects);
