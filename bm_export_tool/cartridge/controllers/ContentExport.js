/**
* Description of the Controller and the logic it provides
*
* @module  controllers/LibraryExport
*/

"use strict";

// HINT: do not put all require statements at the top of the file
// unless you really need them for all functions
/* SFCC API */
var File = require("dw/io/File");
var FileWriter = require("dw/io/FileWriter");
var FileReader = require("dw/io/FileReader");
var Logger = require("dw/system/Logger").getLogger("ExportTool", "contentExport");
var Pipelet = require("dw/system/Pipelet");
var ContentMgr = require("dw/content/ContentMgr");
var Content = require("dw/content/Content");
var XMLStreamReader = require("dw/io/XMLStreamReader");
var XMLStreamConstants = require("dw/io/XMLStreamConstants");
var XMLIndentingStreamWriter = require("dw/io/XMLIndentingStreamWriter");
var ArrayList = require("dw/util/ArrayList");
var ISML = require("dw/template/ISML");
/* Script Modules */
var guard = require("~/cartridge/scripts/guard");
var response = require("~/cartridge/scripts/util/Response");
var XMLParser = require("~/cartridge/scripts/XMLParser");
var exportHelper = require("~/cartridge/scripts/exportHelper");
/* Global variables */
var site = dw.system.Site.getCurrent();
var httpsHostName = site.getHttpsHostName();
var siteLink = "https://" + httpsHostName + "/on/demandware.servlet/webdav/Sites";
var WORKING_FOLDER = [File.IMPEX, "src", "export-tool", "library-content-export"].join(File.SEPARATOR);
var workingFolder = new File(WORKING_FOLDER);
if (!workingFolder.exists()) {
    workingFolder.mkdirs();
}
var TOOL_CONSTANTS = {
    ASSET: "asset",
    FOLDER: "folder",
    BY_ID: "byID",
    BY_NAME: "byName",
    BY_REG_EXP: "byRegExp",
    SELECT_FROM_LIST: "selectFromTheList",
    FOLDERS_ONLY: "folders-only",
    SUBFOLDERS: "folders-subfolders",
    SUBFOLDERS_ASSETS: "folders-subfolders-assets",
    SELECT_ASSESTS_UI: "contentExportUI_SelectAssets",
    SELECT_FOLDERS_UI: "contentExportUI_SelectFolders",
    MANUAL_UI: "contentExportUI_Manual",
    ASSETS_PAGE_SIZE: 15,
    ACTION_ADD: "add",
    ACTION_REMOVE: "remove"
};

function getAssetsPageNavigationData() {
    var assetsPageNavigationData = {
        assetsCurrentPageNumber : 1,
        assetsPagesObject: [],
        currentPageAssets: [],
        numberOfAssetsPages: 1,
        numberOfAssets: 0,
        pageNumbers: [],
        checkedAssets: [],
        expandedAssets: []
    };
    if (session.privacy.assetsPageNavigationData) {
        assetsPageNavigationData.assetsCurrentPageNumber = session.privacy.assetsPageNavigationData.assetsCurrentPageNumber;
        assetsPageNavigationData.currentPageAssets = session.privacy.assetsPageNavigationData.assetsPagesObject[assetsPageNavigationData.assetsCurrentPageNumber];
        assetsPageNavigationData.numberOfAssetsPages = session.privacy.assetsPageNavigationData.numberOfAssetsPages;
        assetsPageNavigationData.numberOfAssets = session.privacy.assetsPageNavigationData.numberOfAssets;
        assetsPageNavigationData.pageNumbers = session.privacy.assetsPageNavigationData.pageNumbers;
        assetsPageNavigationData.checkedAssets = session.privacy.assetsPageNavigationData.checkedAssets;
        assetsPageNavigationData.expandedAssets = session.privacy.assetsPageNavigationData.expandedAssets;
    }
    return assetsPageNavigationData;
}
function createAssetsPageNavigationData(assets) {
    var assetsPagesObject = createAssetsPagesObject(assets);
    var numberOfAssets = assets.length;
    var numberOfAssetsPages = Math.ceil(numberOfAssets / TOOL_CONSTANTS.ASSETS_PAGE_SIZE);
    var pageNumbers = getArrayOfNumbers(1, numberOfAssetsPages);
    session.privacy.assetsPageNavigationData = getAssetsPageNavigationData();
    session.privacy.assetsPageNavigationData.assetsCurrentPageNumber = 1;
    session.privacy.assetsPageNavigationData.assetsPagesObject = assetsPagesObject;
    session.privacy.assetsPageNavigationData.currentPageAssets = assetsPagesObject[0];
    session.privacy.assetsPageNavigationData.numberOfAssetsPages = numberOfAssetsPages;
    session.privacy.assetsPageNavigationData.numberOfAssets = numberOfAssets;
    session.privacy.assetsPageNavigationData.pageNumbers = pageNumbers;
    session.privacy.assetsPageNavigationData.checkedAssets = [];
    session.privacy.assetsPageNavigationData.expandedAssets = [];
}
/**
 * Displays the panel.
 *
 * @param {Object} pdict
 */
function show() {
    var library = session.privacy.library;
    var libraryPath = library ? siteLink + library.file.fullPath : null;
    var libraryContent = library ? session.privacy.library.content : null;
    var assetsPageNavigationData = getAssetsPageNavigationData();
    var folders = [];
    if (libraryContent) {
        folders = libraryContent.folders;
    }
    ISML.renderTemplate("contentexport", {
        libraryPath: libraryPath,
        libraryContent: {
            assets: assetsPageNavigationData.currentPageAssets,
            folders: folders
        },
        assetsPageNavigationData: assetsPageNavigationData
    });
}

function displaySiteLibraryContent() {
    var params = request.httpParameterMap;
    var libraryContent = session.privacy.library.content;
    var assetsPageNavigationData = getAssetsPageNavigationData();
    ISML.renderTemplate("contentexport/selectcontent", {
        libraryContent: {
            assets: assetsPageNavigationData.currentPageAssets,
            folders: libraryContent.folders
        },
        assetsPageNavigationData: assetsPageNavigationData
    });
}

function addRemoveCheckedAssetToSession() {
    var params = request.httpParameterMap;
    var assetID = params.assetID.stringValue;
    var assetAttributes = params.assetAttributes.stringValue.split(",");
    var action = params.action.stringValue;
    var asset = {
        designation: assetID,
        attributes: assetAttributes
    };
    var result = {
        success: true
    };
    try {
        var checkedAssetIndex = session.privacy.assetsPageNavigationData.checkedAssets.map(function (asset) { return asset.designation; }).indexOf(assetID);
        if (action === TOOL_CONSTANTS.ACTION_ADD) {
            if (checkedAssetIndex !== -1) {
                session.privacy.assetsPageNavigationData.checkedAssets.splice(checkedAssetIndex, 1);
            }
            session.privacy.assetsPageNavigationData.checkedAssets.push(asset);
        } else if (action === TOOL_CONSTANTS.ACTION_REMOVE && checkedAssetIndex !== -1) {
            session.privacy.assetsPageNavigationData.checkedAssets.splice(checkedAssetIndex, 1);
        }
    } catch (error) {
        Logger.error("[Export Tool :: addRemoveCheckedAssetToSession] An error occured: {0} In file: {1}, at line: {2}", error.message, error.fileName, error.lineNumber);
        result.success = false;
    }
    return response.renderJSON(result);
}
function addRemoveExpandedAssetToSession() {
    var params = request.httpParameterMap;
    var assetID = params.assetID.stringValue;
    var action = params.action.stringValue;
    var result = {
        success: true
    };
    try {
        var expandedAssetIndex = session.privacy.assetsPageNavigationData.expandedAssets.indexOf(assetID);
        if (action === TOOL_CONSTANTS.ACTION_ADD) {
            session.privacy.assetsPageNavigationData.expandedAssets.push(assetID);
        } else if (action === TOOL_CONSTANTS.ACTION_REMOVE && expandedAssetIndex !== -1) {
            session.privacy.assetsPageNavigationData.expandedAssets.splice(expandedAssetIndex, 1);
        }
    } catch (error) {
        Logger.error("[Export Tool :: addRemoveExpandedAssetToSession] An error occured: {0} In file: {1}, at line: {2}", error.message, error.fileName, error.lineNumber);
        result.success = false;
    }
    return response.renderJSON(result);
}

function getCurrentPageAssets() {
    var assets = assets;
    var params = request.httpParameterMap;
    session.privacy.assetsPageNavigationData.assetsCurrentPageNumber = params.pageNumberAssets.stringValue;
    var assetsPageNavigationData = getAssetsPageNavigationData();
    ISML.renderTemplate("contentexport/selectassets", {
        libraryContent: {
            assets: assetsPageNavigationData.currentPageAssets
        },
        assetsPageNavigationData: assetsPageNavigationData
    });
}

function createAssetsPagesObject(assets) {
    var assetsPages = {};
    var numberOfAssets = assets.length;
    var numberOfPages = Math.ceil(assets.length / TOOL_CONSTANTS.ASSETS_PAGE_SIZE);
    for (var i = 1; i <= numberOfPages; i++) {
        assetsPages[i] = [];
        for (var a = (i-1) * TOOL_CONSTANTS.ASSETS_PAGE_SIZE; a < i * TOOL_CONSTANTS.ASSETS_PAGE_SIZE; a++) {
            if (assets[a]) {
                assetsPages[i].push(assets[a]);
            }
        }
    }
    return assetsPages;
}
/**
 * Exports current site library and puts it in the session
 *
 * @return {Object} response - JSON result of execution
 */
function libraryExport() {
    var params = request.httpParameterMap;
    var Library = ContentMgr.getSiteLibrary();
    var fileName = exportHelper.formatName(params.libraryExportFileName.stringValue);
    var file = new File(workingFolder, fileName);
    var exportFileRelPath = ["export-tool", workingFolder.name, fileName].join(File.SEPARATOR);
    var result = {};

    if (!empty(fileName)) {
        var ExportContentResult = new dw.system.Pipelet("ExportContent").execute({
            Library: Library,
            ExportFile: exportFileRelPath
        });
        if (ExportContentResult.result === PIPELET_NEXT) {
            session.privacy.library = {
                file: file,
                isUploadedLibrary: false
            };
            var libraryContent = getLibraryContent();
            session.privacy.library.content = libraryContent;
            createAssetsPageNavigationData(libraryContent.assets);
            var url = siteLink + file.getFullPath();
            result = {
                type: "library-export",
                success: true,
                exportMsg: "Export successfully finished.",
                url: url
            };
        }
        if (ExportContentResult.result === PIPELET_ERROR) {
            result = {
                success: false,
                exportMsg: "Failed to export files."
            };
        }
    } else {
        result = {
            success: false,
            exportMsg: "Please set an export file name.",
        };
    }

    return response.renderJSON(result);
}
function getArrayOfNumbers(startIndex, quantity) {
    var array = [];
    for (var i = startIndex; i <= quantity; i++) {
        array.push(i);
    }
    return array;
}
/**
 * Uploads a library file and puts it to the session
 *
 * @return {Object} response - JSON result of execution
 */
function libraryUpload() {
    var result = exportHelper.uploadFile("library", workingFolder);
    var libraryContent = getLibraryContent();
    session.privacy.library.content = libraryContent;
    createAssetsPageNavigationData(libraryContent.assets);
    return response.renderJSON(result);
}
function buildExportSettingsObject(params) {
    var exportSettings = {};
    exportSettings.contentRequested = [];
    if (params.requestBodyAsString) {
        try {
            var requestBodyAsString = JSON.parse(params.requestBodyAsString);
            exportSettings.exportType_UI = requestBodyAsString.exportType_UI;
            exportSettings.exportFileName = requestBodyAsString.exportFileName;
            exportSettings.folderExportSettings = requestBodyAsString.folderExportSettings;
            exportSettings.exportStaticContent = requestBodyAsString.exportStaticContent;
            exportSettings.exportType = exportSettings.exportType_UI == TOOL_CONSTANTS.SELECT_ASSESTS_UI ? TOOL_CONSTANTS.ASSET : TOOL_CONSTANTS.FOLDER;
            if (exportSettings.exportType == TOOL_CONSTANTS.ASSET) {
                exportSettings.contentRequested = session.privacy.assetsPageNavigationData.checkedAssets;
            } else if (exportSettings.exportType == TOOL_CONSTANTS.FOLDER) {
                exportSettings.contentRequested = requestBodyAsString.folders;
            }
        } catch (error) {
            Logger.error("[Export Tool :: buildExportSettingsObject] An error occured: {0} In file: {1}, at line: {2}", error.message, error.fileName, error.lineNumber);
        }
        exportSettings.assetSearchMode = TOOL_CONSTANTS.BY_ID;
        exportSettings.folderSearchMode = TOOL_CONSTANTS.BY_ID;

    } else {
        exportSettings.exportFileName = params.exportFileName.stringValue;
        exportSettings.exportType = params.exportType.stringValue;
        exportSettings.assetSearchMode = params.assetSearchMode.stringValue;
        exportSettings.folderSearchMode = params.folderSearchMode.stringValue;
        exportSettings.folderExportSettings = params.folderExportSettings.stringValue;
        exportSettings.exportStaticContent = params.exportStaticContentCheck == "on";
        exportSettings.exportType_UI = TOOL_CONSTANTS.MANUAL_UI;
        switch (exportSettings.exportType) {
            case TOOL_CONSTANTS.ASSET:
                switch (exportSettings.assetSearchMode) {
                    case TOOL_CONSTANTS.BY_ID:
                        params.assetID.stringValue.split(",").forEach(function (item) {
                            var attributes = params.assetAttributes.empty ? [] : params.assetAttributes.stringValue.split(",");
                            exportSettings.contentRequested.push({
                                designation: item,
                                attributes: attributes
                            });
                        });
                        break;
                    case TOOL_CONSTANTS.BY_NAME:
                        params.assetName.stringValue.split(",").forEach(function (item) {
                            var attributes = params.assetAttributes.empty ? [] : params.assetAttributes.stringValue.split(",");
                            exportSettings.contentRequested.push({
                                designation: item,
                                attributes: attributes
                            });
                        });
                        break;
                    case TOOL_CONSTANTS.BY_REG_EXP:
                        params.assetRegExp.stringValue.split(",").forEach(function (item) {
                            var attributes = params.assetAttributes.empty ? [] : params.assetAttributes.stringValue.split(",");
                            exportSettings.contentRequested.push({
                                designation: item,
                                attributes: attributes
                            });
                        });
                        break;
                }
                break;
            case TOOL_CONSTANTS.FOLDER:
                switch (exportSettings.folderSearchMode) {
                    case TOOL_CONSTANTS.BY_ID:
                        params.folderID.stringValue.split(",").forEach(function (item) {
                            var attributes = params.folderAttributes.empty ? [] : params.folderAttributes.stringValue.split(",");
                            exportSettings.contentRequested.push({
                                designation: item,
                                attributes: attributes
                            });
                        });
                        break;
                    case TOOL_CONSTANTS.BY_NAME:
                        params.folderName.stringValue.split(",").forEach(function (item) {
                            var attributes = params.folderAttributes.empty ? [] : params.folderAttributes.stringValue.split(",");
                            exportSettings.contentRequested.push({
                                designation: item,
                                attributes: attributes
                            });
                        });
                        break;
                    case TOOL_CONSTANTS.BY_REG_EXP:
                        params.folderRegExp.stringValue.split(",").forEach(function (item) {
                            var attributes = params.folderAttributes.empty ? [] : params.folderAttributes.stringValue.split(",");
                            exportSettings.contentRequested.push({
                                designation: item,
                                attributes: attributes
                            });
                        });
                        break;
                }
                break;
        }
    }
    return exportSettings;
}
/**
 * Provides assets/folders export based on user preference
 *
 * @return {Object} response - JSON result of execution
 */
function contentExport() {
    var contentRequested = [];
    var contentFound = [];
    var contentNotFound = [];
    var folderIDs = [];
    var params = request.httpParameterMap;
    var exportSettings = buildExportSettingsObject(params);
    var exportFileName = exportSettings.exportFileName;
    var exportType = exportSettings.exportType;
    var library = request.session.privacy.library;
    if (!library.file) {
        return response.renderJSON({
            success: false,
            exportMsg: "Export or upload a library at first."
        });
    }
    if (empty(exportFileName)) {
        return response.renderJSON({
            success: false,
            exportMsg: "Export file name is required."
        });
    }
    if (!exportType) {
        return response.renderJSON({
            success: false,
            exportMsg: "Select an export type."
        });
    }
    var fileReader = new FileReader(library.file, "UTF-8");
    var xmlStreamReader = new XMLStreamReader(fileReader);
    if (exportType == "asset" || (exportType == "folder" && library.isUploadedLibrary)) {
        var file = new File(workingFolder, exportHelper.formatName(exportFileName));
        var writer = new FileWriter(file);
        var Iwriter = new XMLIndentingStreamWriter(writer);
        Iwriter.writeStartDocument("UTF-8", "1.0");
        Iwriter.writeStartElement("library");
        Iwriter.writeAttribute("xmlns", "http://www.demandware.com/xml/impex/library/2006-10-31");
        Iwriter.writeCharacters("");
    }

    while (xmlStreamReader.hasNext()) {
        if (xmlStreamReader.next() == XMLStreamConstants.START_ELEMENT) {
            try {
                if (["folder", "content"].indexOf(xmlStreamReader.getLocalName()) !== -1) {
                    var XMLObject = xmlStreamReader.readXMLObject();
                    var XMLString = XMLObject.toXMLString();
                    var elements = XMLObject.elements();
                    if (exportType == TOOL_CONSTANTS.ASSET) {
                        switch (exportSettings.assetSearchMode) {
                            case TOOL_CONSTANTS.BY_ID:
                                XMLParser.getAssetByID(exportSettings, XMLObject, contentFound, Iwriter);
                                break;
                            case TOOL_CONSTANTS.BY_NAME:
                                XMLParser.getAssetByName(exportSettings, XMLObject, contentFound, Iwriter);
                                break;
                            case TOOL_CONSTANTS.BY_REG_EXP:
                                XMLParser.getAssetByRegExp(exportSettings, XMLObject, contentFound, Iwriter);
                                break;
                        }
                    } else {
                        switch (exportSettings.folderSearchMode) {
                            case TOOL_CONSTANTS.BY_ID:
                                XMLParser.getFolderByID(exportSettings, XMLObject, contentFound, folderIDs, library, Iwriter);
                                break;
                            case TOOL_CONSTANTS.BY_NAME:
                                XMLParser.getFolderByName(exportSettings, XMLObject, contentFound, folderIDs, library, Iwriter);
                                break;
                            case TOOL_CONSTANTS.BY_REG_EXP:
                                XMLParser.getFolderByRegExp(exportSettings, XMLObject, contentFound, folderIDs, library, Iwriter);
                                break;
                        }
                        switch (exportSettings.folderExportSettings) {
                            case TOOL_CONSTANTS.SUBFOLDERS:
                                XMLParser.getFolderSubFolders(exportSettings, XMLObject, contentFound, folderIDs, library, Iwriter);
                                break;
                            case TOOL_CONSTANTS.SUBFOLDERS_ASSETS:
                                XMLParser.getFolderSubFolders(exportSettings, XMLObject, contentFound, folderIDs, library, Iwriter);
                                if (library.isUploadedLibrary) {
                                    folderIDs.forEach(function (item) {
                                        if (item == elements.elements().attribute("folder-id") && XMLObject.localName() == "content") {
                                            Iwriter.writeRaw(XMLString);
                                        }
                                    });
                                }
                                break;
                        }
                    }
                }
            } catch (error) {
                Logger.error("[Export Tool :: Libary Content Export] An error occured: {0} In file: {1}, at line: {2}", error.message, error.fileName, error.lineNumber);
            }
        }
    }
    if (exportType == TOOL_CONSTANTS.ASSET || (exportType == TOOL_CONSTANTS.FOLDER && library.isUploadedLibrary)) {
        Iwriter.writeEndElement();
        Iwriter.writeEndDocument();
        Iwriter.flush();
        Iwriter.close();
        for (var i = 0; i < contentRequested.length; i++) {
            if (contentFound.map(function (object) { return object.designation; }).indexOf(contentRequested[i]) == -1) {
                contentNotFound.push(contentRequested[i]);
            }
        }
        var result = {};
        if (contentFound.length) {
            var url = siteLink + file.getFullPath();
            result = {
                type: "content",
                success: true,
                exportMsg: "Export successfuly completed.",
                url: url,
                contentFound: contentFound,
                contentNotFound: contentNotFound,
                staticContent: exportStaticContent(exportSettings, file)
            };
        } else {
            result = {
                success: false,
                exportMsg: "Nothing found."
            };
        }
        return response.renderJSON(result);
    } else if (exportType == TOOL_CONSTANTS.FOLDER && !library.isUploadedLibrary) {
        result = exportFoldersByID(exportSettings, folderIDs, contentFound);
        return response.renderJSON(result);
    }
}
/**
 * Exports folders by provided array of IDs
 *
 * @param {Object} params - httpParameterMap object
 * @param {Array} folderIDs - array of folder IDs to export
 * @param {Array} contentFound - array of objects with content IDs/Names with attributes that were not found
 * @param {Array} contentRequested - array of requested IDs/Names/RegExps for asset/folder search
 *
 * @return {Object} response - JSON result of execution
 */
function exportFoldersByID(exportSettings, folderIDs, contentFound) {
    var Library = ContentMgr.getSiteLibrary();
    var contentRequested = exportSettings.contentRequested.map(function (object) { return object.designation; });
    var folders = new ArrayList();
    var foldersFound = [];
    var foldersNotFound = [];
    var exportFileName = exportSettings.exportFileName;
    var exportContent;
    var exportFolders;
    var exportSubFolders;
    var exportFileRelPath = ["export-tool", workingFolder.name, exportHelper.formatName(exportFileName)].join(File.SEPARATOR);
    switch (exportSettings.folderExportSettings) {
        case TOOL_CONSTANTS.FOLDERS_ONLY:
            exportContent = false;
            exportFolders = true;
            exportSubFolders = false;
            break;
        case TOOL_CONSTANTS.SUBFOLDERS:
            exportContent = false;
            exportFolders = true;
            exportSubFolders = true;
            break;
        case TOOL_CONSTANTS.SUBFOLDERS_ASSETS:
            exportContent = true;
            exportFolders = true;
            exportSubFolders = true;
            break;
    }
    for (var i = 0; i < folderIDs.length; i++) {
        var folder = ContentMgr.getFolder(Library, folderIDs[i]);
        if (folder) {
            folders.add(folder);
            foldersFound.push(contentFound[i]);
        } else {
            foldersNotFound.push(contentFound[i]);
        }
    }
    for (var i = 0; i < contentRequested.length; i++) {
        if (foldersFound.map(function (object) { return object.designation; }).indexOf(contentRequested[i]) == -1) {
            foldersNotFound.push(contentRequested[i]);
        }
    }
    if (folders.length) {
        var result = {};
        var foldersIterator = folders.iterator();
        var ExportContentResult = new dw.system.Pipelet("ExportContent").execute({
            Library: Library,
            ExportFile: exportFileRelPath,
            Folders: foldersIterator,
            ExportContent: exportContent,
            ExportFolders: exportFolders,
            ExportSubFolders: exportSubFolders
        });
        if (ExportContentResult.result === PIPELET_NEXT) {
            var file = new File(File.IMPEX + "/src/" + exportFileRelPath);
            var url = siteLink + file.getFullPath();
            if (!empty(exportSettings.contentRequested.map(function (object) { return object.attributes; }))) {
                var fileTemporary = file.copyTo(new File(workingFolder, exportHelper.formatName(exportFileName + "-temp")));
                var writer = new FileWriter(file);
                var Iwriter = new XMLIndentingStreamWriter(writer);
                Iwriter.writeStartDocument("UTF-8", "1.0");
                Iwriter.writeStartElement("library");
                Iwriter.writeAttribute("xmlns", "http://www.demandware.com/xml/impex/library/2006-10-31");
                Iwriter.writeCharacters("");
                var fileReader = new FileReader(fileTemporary, "UTF-8");
                var xmlStreamReader = new XMLStreamReader(fileReader);
                while (xmlStreamReader.hasNext()) {
                    if (xmlStreamReader.next() == XMLStreamConstants.START_ELEMENT) {
                        try {
                            if (xmlStreamReader.getLocalName() == "folder") {
                                var XMLObject = xmlStreamReader.readXMLObject();
                                var XMLString = XMLObject.toXMLString();
                                var folderID = XMLObject.attribute("folder-id").toString();
                                var contentFoundIndex = contentFound.map(function (object) { return object.ID; }).indexOf(folderID);
                                var attributesFound = contentFound[contentFoundIndex].attributesFound;
                                XMLString = XMLParser.setCustomAttributes(attributesFound, XMLObject).XMLObject.toXMLString();
                                Iwriter.writeRaw(XMLString);
                            } else if (xmlStreamReader.getLocalName() == "content") {
                                var XMLObject = xmlStreamReader.readXMLObject();
                                var XMLString = XMLObject.toXMLString();
                                Iwriter.writeRaw(XMLString);
                            }
                        } catch (error) {
                            Logger.error("[Export Tool :: Libary Content Export] An error occured: {0} In file: {1}, at line: {2}", error.message, error.fileName, error.lineNumber);
                        }
                    }
                }
                Iwriter.writeEndElement();
                Iwriter.writeEndDocument();
                Iwriter.flush();
                Iwriter.close();
                if (fileTemporary.exists() && !fileTemporary.directory) {
                    fileTemporary.remove();
                }
            }
            result = {
                type: "content",
                success: true,
                exportMsg: "Export successfuly completed.",
                url: url,
                contentFound: foldersFound,
                contentNotFound: foldersNotFound,
                staticContent: exportStaticContent(exportSettings, file)
            };
        }
        if (ExportContentResult.result === PIPELET_ERROR) {
            result = {
                success: false,
                exportMsg: "Failed to export files."
            };
        }
    } else {
        result = {
            success: false,
            exportMsg: "Nothing found."
        };
    }
    return result;
}
function exportStaticContent(exportSettings, file) {
    if (!exportSettings.exportStaticContent) {
        return false;
    }
    var result = {};
    var siteID = site.getID();
    var filePaths = XMLParser.getStaticContentPaths(file);
    if (!empty(filePaths)) {
        var tempDir = new File([WORKING_FOLDER, "static-content"].join(File.SEPARATOR));
        filePaths.forEach(function (filePath) {
            var staticContentFile = new File([File.LIBRARIES, siteID, "default", filePath].join(File.SEPARATOR));
            var parentFolders = filePath.split("/");
            parentFolders.pop();
            var parentFolders = parentFolders.join("/");
            var dir = new File([WORKING_FOLDER, tempDir.name, parentFolders].join(File.SEPARATOR));
            if (!dir.exists()) {
                dir.mkdirs();
            }
            try {
                var staticContentFileCOPY = staticContentFile.copyTo(new File(dir, staticContentFile.name));
            } catch (error) {
                Logger.error("[Export Tool :: exportStaticConent] An error occured: {0} In file: {1}, at line: {2}", error.message, error.fileName, error.lineNumber);
            }
        });
        var zipFile = new File([WORKING_FOLDER, file.name.replace("xml", "zip")].join(File.SEPARATOR));
        tempDir.zip(zipFile);
        if (tempDir.exists() && tempDir.directory) {
            exportHelper.deleteFolder(tempDir);
        }
        result = {
            success: true,
            exportMsg: "Static content exported.",
            url: siteLink + zipFile.getFullPath()
        };
    } else {
        result = {
            success: false,
            exportMsg: "Static content not found.",
        };
    }
    return result;
}
function getLibraryContent() {
    var library = session.privacy.library;
    var libraryContent = {
        assets: [],
        folders: []
    };
    var assetIDs = [];
    var folderIDs = [];
    var fileReader = new FileReader(library.file, "UTF-8");
    var xmlStreamReader = new XMLStreamReader(fileReader);
    while (xmlStreamReader.hasNext()) {
        if (xmlStreamReader.next() == XMLStreamConstants.START_ELEMENT) {
            try {
                var localName = xmlStreamReader.getLocalName();
                if (localName === "content") {
                    var XMLObject = xmlStreamReader.readXMLObject();
                    var XMLString = XMLObject.toXMLString();
                    var elements = XMLObject.elements();
                    var assetID = XMLObject.attribute("content-id").toString();
                    var assetName;
                    var assetAttributes = [];
                    for (var i = 0; i < elements.length(); i++) {
                        var elementLocalName = elements[i].localName();
                        if (elements[i].localName() == "display-name" && elements[i].attributes("xml:lang") == "x-default") {
                            assetName = elements[i].toString();
                        }
                        assetAttributes.push(elementLocalName);
                    }
                    assetAttributes = assetAttributes.filter(function (item, pos, arr) {
                        return arr.indexOf(item) == pos;
                    });
                    var asset = {
                        ID: assetID,
                        name: assetName,
                        attributes: assetAttributes
                    };
                    libraryContent.assets.push(asset);
                }
                if (localName === "folder") {
                    var XMLObject = xmlStreamReader.readXMLObject();
                    var XMLString = XMLObject.toXMLString();
                    var elements = XMLObject.elements();
                    var folderID = XMLObject.attribute("folder-id").toString();
                    var folderName;
                    var folderAttributes = [];
                    for (var i = 0; i < elements.length(); i++) {
                        var elementLocalName = elements[i].localName();
                        if (elementLocalName == "display-name" && elements[i].attributes("xml:lang") == "x-default") {
                            folderName = elements[i].toString();
                        }
                        folderAttributes.push(elementLocalName);
                    }
                    folderAttributes = folderAttributes.filter(function (item, pos, arr) {
                        return arr.indexOf(item) == pos;
                    });
                    var folder = {
                        ID: folderID,
                        name: folderName,
                        attributes: folderAttributes
                    };
                    libraryContent.folders.push(folder);
                }
            } catch (error) {
                Logger.error("[Export Tool :: getLibraryContent] An error occured: {0} In file: {1}, at line: {2}", error.message, error.fileName, error.lineNumber);
            }
        }
    }
    return libraryContent;
}

/* Exports of the controller */
/** Shows the main panel
 * @see {@link module:controllers/ContentExport~show} */
exports.Show = guard.ensure(["https", "get"], show);
/** Exports current site library
 * @see {@link module:controllers/ContentExport~libraryExport} */
exports.LibraryExport = guard.ensure(["https", "post"], libraryExport);
/** Uploads selected file
 * @see {@link module:controllers/ContentExport~libraryUpload} */
exports.LibraryUpload = guard.ensure(["https", "post"], libraryUpload);
/** Exports required content (folders/assets) depending on user preference
 * @see {@link module:controllers/ContentExport~libraryUpload} */
exports.ContentExport = guard.ensure(["https", "post"], contentExport);
exports.DisplaySiteLibraryContent = guard.ensure(["https", "get"], displaySiteLibraryContent);
exports.GetCurrentPageAssets = guard.ensure(["https", "get"], getCurrentPageAssets);
exports.AddRemoveCheckedAssetToSession = guard.ensure(["https", "get"], addRemoveCheckedAssetToSession);
exports.AddRemoveExpandedAssetToSession = guard.ensure(["https", "get"], addRemoveExpandedAssetToSession);
