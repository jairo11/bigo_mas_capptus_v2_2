"use strict";

var billingHelpers = require("./billing");
var base = require("@refapp/js/checkout/checkout");
var formatters = require("@refapp/js/formatters");

Object.keys(billingHelpers).forEach(function (item) {
    if (typeof billingHelpers[item] === "object") {
        base[item] = $.extend({}, base[item], billingHelpers[item]);
    } else {
        base[item] = billingHelpers[item];
    }
});

base.formatters = formatters.init;

module.exports = base;
