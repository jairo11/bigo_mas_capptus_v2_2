'use strict';

var server = require('server');
server.extend(module.superModule);

/**
 * Populate viewData with additional data specific for MercadoPago
 */
server.append('Begin', function (req, res, next) {
    // Guard clause
    var hookMgr = require('dw/system/HookMgr');
    var collections = require('*/cartridge/scripts/util/collections');
    var MercadoPagoHelper = require('*/cartridge/scripts/util/MercadoPagoHelper');
    var isMercadoPagoEnabled = MercadoPagoHelper.isMercadoPagoEnabled();
    if (!isMercadoPagoEnabled) {
        return next();
    }

    var BasketMgr = require('dw/order/BasketMgr');

    var currentBasket = BasketMgr.getCurrentBasket();

    // Remove name attribute for card number and security code fields
    var form = server.forms.getForm('billing').mercadoPagoCreditCard;
    Object.keys(form).forEach(function (key) {
        if (key !== 'cardNumber' && key !== 'securityCode') {
            return;
        }

        Object.defineProperty(form[key], 'attributesWithoutName', {
            get: function () {
                // eslint-disable-next-line no-useless-escape
                return form[key].attributes.replace(/(name=\"){1}(\w)+(\"){1}(\s){1}/, '');
            }
        });
    });

    var viewData = res.getViewData();

    if (req.currentCustomer.raw.authenticated && !viewData.order.orderEmail) {
        viewData.order.orderEmail = req.currentCustomer.profile.email;
    }

    var mpAvailablePaymentMethods = MercadoPagoHelper.getAvailablePaymentMethods();

    if (!mpAvailablePaymentMethods || !mpAvailablePaymentMethods.length > 0) {
        return next();
    }
    var suburbsResponse = {};
    var zipCode = '';
    var suburbsArray = [];
    if (currentBasket.shipments[0].shippingAddress) {
        var zipCode = currentBasket.shipments[0].shippingAddress.postalCode;
        if (hookMgr.hasHook('app.services.cya.brokerDelivery')) {
            suburbsResponse = hookMgr.callHook('app.services.cya.brokerDelivery', 'getValidatePostalCode', zipCode);
        }
        if (suburbsResponse.ResponseCode && suburbsResponse.ResponseCode == 200) {
            suburbsArray = suburbsResponse.postalCode[0].suburbs;
        }
    }

    var mercadoPagoCards = [];
    var paypalCards = [];
    if (req.currentCustomer.raw.authenticated) {
        var cards = viewData.customer.customerPaymentInstruments;
        for (var i = 0; i < cards.length; i++) {
            if (cards[i].paymentMethod === 'MercadoPago') {
                mercadoPagoCards.push(cards[i]);
            } else if (cards[i].paymentMethod === 'CREDIT_CARD') {
                paypalCards.push(cards[i]);
            }
        }
    }

    viewData.mercadoPago = {
        enable: isMercadoPagoEnabled,
        form: form,
        groupedPaymentMethods: MercadoPagoHelper.getGroupedPaymentMethods(),
        availablePaymentMethods: mpAvailablePaymentMethods,
        pseFinancialInstitutions: MercadoPagoHelper.getPseFinancialInstitutions(mpAvailablePaymentMethods),
        customerCards: req.currentCustomer.raw.authenticated && req.currentCustomer.raw.registered ? MercadoPagoHelper.getCustomerCards(req.currentCustomer) : [],
        preferences: MercadoPagoHelper.getPreferences(),
        errorMessages: MercadoPagoHelper.getErrorMessages(),
        resourceMessages: MercadoPagoHelper.getResourceMessages(),
        configuration: MercadoPagoHelper.getConfiguration(),
        orderTotal: currentBasket.totalGrossPrice.value,
        suburbsArray: suburbsArray
    };

    viewData['mercadoPagoCards'] = mercadoPagoCards;

    viewData.paypalCards = paypalCards;

    res.setViewData(viewData);
    return next();
});

module.exports = server.exports();
