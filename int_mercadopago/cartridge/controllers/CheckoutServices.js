"use strict";

/* global session */

var server = require("server");
server.extend(module.superModule);

/**
 * Populate viewData with additional data specific for MercadoPago
 */
server.append("SubmitPayment", function (req, res, next) {
    // Guard clause
    var viewData = res.getViewData();
    if (viewData.error) {
        return next();
    }

    // Guard clause
    var Resource = require("dw/web/Resource");
    if (viewData.paymentMethod.value !== Resource.msg("payment.method.id", "mercadoPagoPreferences", null)) {
        return next();
    }

    var paymentForm = server.forms.getForm("billing");
    var mercadoPagoData = {
        cardType: {
            value: paymentForm.mercadoPagoCreditCard.cardType.value,
            htmlName: paymentForm.mercadoPagoCreditCard.cardType.htmlName
        },
        token: {
            value: paymentForm.mercadoPagoCreditCard.token.value,
            htmlName: paymentForm.mercadoPagoCreditCard.token.htmlName
        },
        identification: {
            docType: { value: paymentForm.mercadoPagoCreditCard.docType.value },
            docNumber: { value: paymentForm.mercadoPagoCreditCard.docNumber.value }
        }
    };

    if (mercadoPagoData.cardType.value === "pse") {
        mercadoPagoData.financialinstitution = paymentForm.mercadoPagoCreditCard.financialinstitution.value;
    }

    mercadoPagoData.paymentTypeId = paymentForm.mercadoPagoCreditCard.paymentTypeId.value;

    viewData.paymentInformation.mercadoPago = mercadoPagoData;

    viewData.mercadoPago = {
        email: { value: paymentForm.mercadoPagoCreditCard.email.value },
        phone: { value: paymentForm.mercadoPagoCreditCard.phone.value }
    };

    res.setViewData(viewData);

    this.on("route:BeforeComplete", function (req, res) { // eslint-disable-line no-shadow
        var BasketMgr = require("dw/order/BasketMgr");
        var Transaction = require("dw/system/Transaction");

        var billingForm = server.forms.getForm("billing");
        var cardId = billingForm.mercadoPagoCreditCard.cardId.value;
        var billingData = res.getViewData();
        var currentBasket = BasketMgr.getCurrentBasket();
        var billingAddress = currentBasket.billingAddress;

        // Clear sensitive data
        billingForm.mercadoPagoCreditCard.cardNumber.htmlValue = "";
        billingForm.mercadoPagoCreditCard.securityCode.htmlValue = "";

        Transaction.wrap(function () {
            var Locale = require("dw/util/Locale");
            var OrderModel = require("*/cartridge/models/order");

            // Set email and phone
            if (req.currentCustomer.raw.authenticated
                && req.currentCustomer.raw.registered
                && cardId
            ) {
                //billingAddress.setPhone(req.currentCustomer.profile.phone);
                currentBasket.setCustomerEmail(req.currentCustomer.profile.email);
            } else {
                //billingAddress.setPhone(billingData.phone.value);
                currentBasket.setCustomerEmail(billingData.email.value);
            }
            billingAddress.setPhone(currentBasket.shipments[0].shippingAddress.phone);



            // Create new Order model with email and phone populated
            var basketModel = new OrderModel(
                currentBasket,
                {
                    usingMultiShipping: req.session.privacyCache.get("usingMultiShipping"),
                    countryCode: Locale.getLocale(req.locale.id).country,
                    containerView: "basket"
                }
            );

            res.json({
                order: basketModel
            });
        });
    });

    return next();
});

server.append("PlaceOrder", server.middleware.https, function (req, res, next) {
    var viewData = res.getViewData();
    // if error status was set in checkoutHelpers, check whether it was a redirect or a real error with additional info and\or reset token directive
    if (viewData.error) {
        // If redirectURL stored in session, send it via JSON to redirect User to MercadoPago payment page
        if (session.privacy.redirectURL) {
            // run fraud detection first
            var OrderMgr = require("dw/order/OrderMgr");
            var Transaction = require("dw/system/Transaction");
            var URLUtils = require("dw/web/URLUtils");
            var Resource = require("dw/web/Resource");
            var hooksHelper = require("*/cartridge/scripts/helpers/hooks");
            var order = OrderMgr.getOrder(session.privacy.orderNumber);
            var fraudDetectionStatus = hooksHelper("app.fraud.detection", "fraudDetection", order, require("*/cartridge/scripts/hooks/fraudDetection").fraudDetection);
            if (fraudDetectionStatus.status === "fail") {
                Transaction.wrap(function () { OrderMgr.failOrder(order, true); });

                // fraud detection failed
                req.session.privacyCache.set("fraudDetectionStatus", true);

                res.json({
                    error: true,
                    cartError: true,
                    redirectUrl: URLUtils.url("Error-ErrorCode", "err", fraudDetectionStatus.errorCode).toString(),
                    errorMessage: Resource.msg("error.technical", "checkout", null)
                });

                return next();
            }
            // If fraud detection passed sucessfully redirect shopper to MercadoPago
            viewData.continueUrl = session.privacy.redirectURL;
            delete viewData.error;
            delete viewData.errorMessage;
            res.setViewData(viewData);
            return next();
        }
        // If there is additional error info from MercadoPago add it to error JSON
        if (session.privacy.resetMpToken) {
            viewData.resetMpToken = session.privacy.resetMpToken;
        }
        // If MercadoPago token must be reset add it to error JSON
        if (session.privacy.detailedError) {
            viewData.detailedError = session.privacy.detailedError;
        }
        res.setViewData(viewData);
    }

    return next();
});

module.exports = server.exports();
