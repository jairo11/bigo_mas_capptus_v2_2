'use strict';

/* global request empty response session */
/* eslint-disable no-param-reassign */

var server = require('server');

var Transaction = require('dw/system/Transaction');
var Logger = require('dw/system/Logger').getLogger('PaymentAPINotification', 'PaymentAPINotification');
var Resource = require('dw/web/Resource');
var OrderMgr = require('dw/order/OrderMgr');
var Order = require('dw/order/Order');

server.post('ConektaPaymentNotification', function (req, res, next) {
    try {
        var notificationData = JSON.parse(request.httpParameterMap.requestBodyAsString);

        Logger.debug(JSON.stringify(notificationData));
    } catch (error) {
        Logger.error('Unable parse payload: ' + error.message);
        res.setStatusCode(500);
        return;
        next();
    }

    switch (notificationData.type) {
        case 'order.paid':
            var order = OrderMgr.searchOrder(
                'custom.OxxoOrderID = {0} AND custom.OxxoReferenceID = {1} AND paymentStatus = {2}',
                notificationData.data.object.charges.data[0].order_id,
                notificationData.data.object.charges.data[0].payment_method.reference,
                Order.PAYMENT_STATUS_PARTPAID
            );

            if (order) {
                Transaction.wrap(function () {
                    order.setPaymentStatus(Order.PAYMENT_STATUS_PAID);
                    order.setConfirmationStatus(Order.CONFIRMATION_STATUS_CONFIRMED);
                    order.setExportStatus(Order.EXPORT_STATUS_READY);
                });
                Logger.info('Order paid: ' + order.getOrderNo());

                callApiOrder(order.getOrderNo());
                sendCashEmail(order, 'APPROVED');

                response.setStatus(200);
            } else {
                Logger.error('unable find order: ' + notificationData.data.object.charges.data[0].order_id + ' - ' + notificationData.data.object.charges.data[0].payment_method.reference);
                response.setStatus(404);
            }
            break;
        case 'order.expired':
            // cancel order
            var order = OrderMgr.searchOrder(
                'custom.OxxoOrderID = {0} AND custom.OxxoReferenceID = {1}',
                notificationData.data.object.charges.data[0].order_id,
                notificationData.data.object.charges.data[0].payment_method.reference
            );

            if (order) {
                Transaction.wrap(function () {
                    order.setPaymentStatus(Order.PAYMENT_STATUS_NOTPAID);
                    OrderMgr.cancelOrder(order);
                    order.setCancelDescription('Order canceled by conekta');
                });
                Logger.info('Order cancel: ' + order.getOrderNo());
                // Implementacion de notificacion de correo
                sendCashEmail(order, 'CANCELLED');
                response.setStatus(200);
            } else {
                Logger.error('unable find order: ' + notificationData.data.object.order_id + ' - ' + notificationData.data.object.charges.data[0].payment_method.reference);
                response.setStatus(404);
            }
            break;
        default:
            response.setStatus(415);

    }
    res.render('checkout/moNotification.isml');
    next();
});

function callApiOrder(orderNo) {
    var HookMgr = require('dw/system/HookMgr');
    if (HookMgr.hasHook('app.services.api.orderXML')) {
            // if(order.paymentStatus.value === 1){
        HookMgr.callHook('app.services.api.orderXML',
                    'sendOrderXML',
                    orderNo
                );
            // }
    }
}

server.post('MercadoPagoPaymentNotification', function (req, res, next) {
    var MercadoPago = require('*/cartridge/scripts/library/libMercadoPago');
    var paymentInfo;
    var orderNo;
    var order;
    var orderStatus;
    var localeID = req.locale.id;

    // In case any problem occur during this steps the catch should return a invalid response for subsequent retries from mercado pago
    // For success cases the script will return a 200 request as it completes
    try {
        paymentInfo = JSON.parse(request.httpParameterMap.requestBodyAsString);

        Logger.debug(JSON.stringify(paymentInfo));
        orderNo = paymentInfo.external_reference;
        order = OrderMgr.getOrder(orderNo);

        Transaction.wrap(function () {
            var MP = new MercadoPago();
            var failOrder = MP.getOtherPaymentMode() === "Link";

            paymentInfo.payer = {};
            paymentInfo.additional_info = {};

            Logger.debug(JSON.stringify(paymentInfo));

            if (!empty(paymentInfo)) {
                if (!empty(order)) {
                    var paymentWasPending = order.paymentStatus.value === 0;
                    order.addNote('MercadoPago Webhook event received', JSON.stringify(paymentInfo));

                    orderStatus = MP.parseOrderStatus(paymentInfo.status);

                    // eslint-disable-next-line no-use-before-define
                    updatePaymentInfo(order, paymentInfo);

                    order.addNote('MercadoPago Webhook new status', JSON.stringify(paymentInfo));
                    // If previous status was pending and now is authorized, place the order and display order confirmation page
                    if (paymentWasPending && orderStatus === 'authorized') {
                        order.setPaymentStatus(Order.PAYMENT_STATUS_PAID);
                        order.setExportStatus(Order.EXPORT_STATUS_READY);
                        order.setConfirmationStatus(Order.CONFIRMATION_STATUS_CONFIRMED);
                        // eslint-disable-next-line no-use-before-define
                        handleAuthorizedResponse(order, localeID);
                        // eslint-disable-next-line no-use-before-define
                    } else if (paymentWasPending && orderStatus === 'declined') {
                        order.setPaymentStatus(Order.PAYMENT_STATUS_NOTPAID);
                        // eslint-disable-next-line no-use-before-define
                        handleDeclinedResponse(order, failOrder);
                        // eslint-disable-next-line no-use-before-define
                        sendCashEmail(order, 'CANCELLED');
                    }
                }
            }
        });
        if (order.paymentStatus.value === 2)callApiOrder(order.getOrderNo());
    } catch (error) {
        Logger.error('MercadoPago.js - MercadoPagoPaymentNotification : An error occurred with this method: ' + error.message);
        response.setStatus(500);
    }

    res.render('checkout/moNotification.isml');

    next();
});

/**
 * @param  {Object} order - Order object
 * @param  {Object} paymentInfo - Object containing payment status
 */
function updatePaymentInfo(order, paymentInfo) {
    Transaction.wrap(function () {
        order.custom.transactionStatus = paymentInfo.status + ' - ' + Resource.msg('status.' + paymentInfo.status, 'mercadoPago', null);
        order.custom.transactionReport = paymentInfo.status_detail ? paymentInfo.status_detail : '';
    });
}

/**
 * @description This function executes when MercadoPago notifies us about approved payments
 * @param  {Object} order - Order object
 * @param  {string} localeID - Id of current locale
 */
function handleAuthorizedResponse(order, localeID) {
    if (order.status.value !== Order.ORDER_STATUS_CREATED) {
        // Mercado pago can fire the same event more than once
        // In the case the change was already done, like order is already placed or failed, don't change anything again
        return;
    }

    try {
        var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
        // possible orderStatus are "authorized", "declined" and "pending"
        // if we have "declined" or "pending", we will not do anything here
        // for "authorized", we will place the order and depending of the result, send a confirmation
        var placeOrderResult = COHelpers.placeOrder(order, { status: true });

        if (placeOrderResult.error) {
            Logger.error('Cash.js - Error in handleAuthorizedResponse() with details: {0}', placeOrderResult);
            // inform the client in some way (like a payment failed e-mail)
        } else {
            sendCashEmail(order, 'APPROVED');
        }
    } catch (e) {
        Logger.error('Cash.js - Error in handleAuthorizedResponse() with details: {0}', e.message);
    }
}

/**
 * @description This function executes when MercadoPago notifies us about declined payments
 * @param  {Object} order - Order object
 * @param  {boolean} failOrder - boolen which shows whether other payment method was accessed via the link
 */
function handleDeclinedResponse(order, failOrder) {
    if (order.status.value === Order.ORDER_STATUS_FAILED) {
        // Mercado pago can fire the same event more than once or not immediately
        // In the case the change was already done, like order is already failed, don't change anything again
        return;
    }

    try {
        if (order.status.value === Order.ORDER_STATUS_CREATED && failOrder) {
            Transaction.wrap(function () {
                OrderMgr.failOrder(order, true);
            });
        } else if (order.status.value === Order.ORDER_STATUS_NEW || order.status.value === Order.ORDER_STATUS_OPEN) {
            Transaction.wrap(function () {
                OrderMgr.cancelOrder(order);
                order.setCancelDescription('Order canceled by MercadoPago');
            });
        }
        // inform the client in some way (like a payment failed e-mail)
    } catch (e) {
        Logger.error('Cash.js - Error in handleAuthorizedResponse() with details: {0}', e.message);
    }
}

// eslint-disable-next-line require-jsdoc
function sendCashEmail(order, status) {
    var Site = require('dw/system/Site');
    var OrderModel = require('*/cartridge/models/order');
    var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');
    var Locale = require('dw/util/Locale');
    var currentLocale = Locale.getLocale('es_MX');
    var templateType = '';
    var orderModel = new OrderModel(order, { countryCode: currentLocale.country, containerView: 'order' });
    var orderObject = { order: orderModel };
    var emailObj = {
        to: order.customerEmail,
        subject: Resource.msg('subject.order.confirmation.email', 'order', null),
        from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@testorganization.com',
        type: emailHelpers.emailTypes.orderConfirmation
    };

    switch (status) {
        case 'APPROVED':
            emailObj.subject = Resource.msg('subject.order.confirmed.title', 'order', null); // change resource message properties
            templateType = 'email/confirmedPaymentOrderEmail';
            break;
        case 'CANCELLED':
            emailObj.subject = Resource.msgf('subject.cancelled.order.title', 'order', null, orderModel.orderNumber); // change resource message properties
            templateType = 'email/canceledCashPaymentOrderEmail';
            break;
        default :
            break;
    }

    emailHelpers.sendEmail(emailObj, templateType, orderObject);
}


module.exports = server.exports();
