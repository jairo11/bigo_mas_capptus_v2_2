"use strict";

var server = require("server");
server.extend(module.superModule);

/**
 * Populate viewData with additional data specific for MercadoPago
 */
server.append("SubmitShipping", function (req, res, next) {
    // Guard clause
    //var hookMgr = require("dw/system/HookMgr");
    var MercadoPagoHelper = require("*/cartridge/scripts/util/MercadoPagoHelper");
    if (!MercadoPagoHelper.isMercadoPagoEnabled()) {
        return next();
    }

    this.on("route:BeforeComplete", function (req, res) { // eslint-disable-line no-shadow
        var viewData = res.getViewData();

        if (req.currentCustomer.raw.authenticated && viewData.order) {
            viewData.order.orderEmail = req.currentCustomer.profile.email;
        }

        // var suburbsResponse = {};
        // var zipCode = '';
        // var suburbsArray = [];
        // if(viewData && viewData.address ){
        //     var zipCode = viewData.address.postalCode;
        //     if (hookMgr.hasHook("app.services.cya.brokerDelivery")) {
        //         suburbsResponse = hookMgr.callHook("app.services.cya.brokerDelivery", "getValidatePostalCode", zipCode);
        //     }
        //     if(suburbsResponse.ResponseCode && suburbsResponse.ResponseCode == 200){
        //         viewData.suburbsArray = suburbsResponse.postalCode[0].suburbs;
        //     }
        // }

        res.setViewData(viewData);

    });

    return next();
});

module.exports = server.exports();