"use strict";

/* global $ */

var base = require("base/checkout/billing");
var cleave = require("base/components/cleave");

function getPaymentMethodName(order, selectedPaymentInstrument) {
    var paymentMethodName = "";
    order.billing.payment.applicablePaymentMethods.forEach(function (paymentMethod) {
        if(paymentMethod.ID === selectedPaymentInstrument.paymentMethod){
            paymentMethodName=paymentMethod.name;
        }
    });
    return paymentMethodName;
}

base.methods.validateAndUpdateBillingPaymentInstrument =function (order) {
    var billing = order.billing;
    if (!billing.payment || !billing.payment.selectedPaymentInstruments
        || billing.payment.selectedPaymentInstruments.length <= 0) return;

    var form = $('form[name=dwfrm_billing]');
    if (!form) return;

    var instrument = billing.payment.selectedPaymentInstruments[0];
    $('select[name$=expirationMonth]', form).val(instrument.expirationMonth);
    $('select[name$=expirationYear]', form).val(instrument.expirationYear);
    // Force security code and card number clear
    $('input[name$=securityCode]', form).val('');
    if($('input[name$=cardNumber]').length > 0){
        $('input[name$=cardNumber]').data('cleave').setRawValue('');
    }
};



/**
 * @function updatePaymentInformation
 * @description Update payment details summary based on payment method
 * @param {Object} order - checkout model to use as basis of new truth
 */
base.methods.updatePaymentInformation = function (order) {
    // update payment details
    var $paymentSummary = $(".payment-details");
    var htmlToAppend = "";

    if (order.billing.payment && order.billing.payment.selectedPaymentInstruments && order.billing.payment.selectedPaymentInstruments.length > 0) {
        order.billing.payment.selectedPaymentInstruments.forEach(function (selectedPaymentInstrument) {
            if (selectedPaymentInstrument.paymentMethod === window.mercado_pago.creditCard || selectedPaymentInstrument.paymentMethod === window.mercado_pago.paypal) {
                var paymentMethodName = getPaymentMethodName(order, selectedPaymentInstrument);
                htmlToAppend += "<span>" + paymentMethodName
                    + "</span><div>"
                    + selectedPaymentInstrument.type
                    + "</div><div><span>"
                    + selectedPaymentInstrument.maskedCreditCardNumber
                    + "</span></div>";
            } else if (selectedPaymentInstrument.paymentMethod === window.mercado_pago.mercadoPagoCard) {
                var paymentMethods = $(".js-mp-available-payment-methods").data("mpAvailablePaymentMethods");
                var paymentInstrumentType = selectedPaymentInstrument.type;
                var paymentMethod = paymentMethods.find(function (method) { return paymentInstrumentType === method.id; });
                var paymentype = paymentMethod ? paymentMethod.name : paymentInstrumentType;
                var paymentMethodName = getPaymentMethodName(order, selectedPaymentInstrument);
                htmlToAppend += "<span>" + paymentMethodName
                    + "</span><div>"
                    + selectedPaymentInstrument.type
                    + "</div><div><span>"
                    + selectedPaymentInstrument.maskedCreditCardNumber
                    + "</span></div>";
            } else if (selectedPaymentInstrument.paymentMethod === window.mercado_pago.giftCertificate) {
                htmlToAppend += "<span>"
                    + order.resources.giftCertificate
                    + "</span><br/>";
            }else if(selectedPaymentInstrument.paymentMethod === window.mercado_pago.cash){
                var paymentMethodName = getPaymentMethodName(order, selectedPaymentInstrument);
                var storeType = "";
                if(selectedPaymentInstrument.storeType == "oxxo"){
                    storeType = window.mercado_pago.oxxo;
                }else{
                    storeType = window.mercado_pago.other;
                }

                htmlToAppend += "<span>" + paymentMethodName + ": " + storeType
                + "</span><div>"
                + window.mercado_pago.amount + " $" + selectedPaymentInstrument.amount.toFixed(2)
                + "</div><div><span>"
                + window.mercado_pago.time
                + "</span></div>";
            }
        });
    }

    $paymentSummary.empty().append(htmlToAppend);
};

/**
 * @function handlePaymentOptionChange
 * @description Handle payment option change
 */
base.methods.handlePaymentOptionChange = function () {
    $(".payment-options .nav-link").removeClass('active');
    var $activeTab = $(this);
    var activeTabId = $activeTab.attr("href");
    var $paymentInformation = $(".payment-information");
    //var isNewPayment = $(".user-payment-instruments").hasClass("checkout-hidden");

    $(".payment-options [role=tab]").each(function (i, tab) {
        let otherTab = $(tab);
        let otherTabId = otherTab.attr("href");

        $(otherTabId).find("input, select").prop("disabled", otherTabId !== activeTabId);
        $(otherTabId).removeClass('active');
        /*$(activeTabId).addClass('active');*/
    });
    $paymentInformation.data("is-new-payment", true);
};

base.selectBillingAddress = function () {
    $(".payment-form .addressSelector").on("change", function () {
        var form = $(this).parents("form")[0];
        var selectedOption = $("option:selected", this);
        var optionID = selectedOption[0].value;

        if (optionID === "new") {
            // Show Address
            $(form).attr("data-address-mode", "new");
        } else {
            // Hide Address
            $(form).attr("data-address-mode", "shipment");
        }

        // Copy fields
        var attrs = selectedOption.data();
        var element;

        Object.keys(attrs).forEach(function (attr) {
            element = attr === "countryCode" ? "country" : attr;
            if (element === "cardNumber") {
                $(".cardNumber").data("cleave").setRawValue(attrs[attr]);
            } else if (element === "mercadoPagoCardNumber") {
                $(".mercadoPagoCardNumber").data("cleave").setRawValue(attrs[attr]);
            } else {
                $("[name$=" + element + "]", form).val(attrs[attr]);
            }
        });
    });
};

base.handleCreditCardNumber = function () {
    if($(".cardNumber").length > 0){
        cleave.handleCreditCardNumber(".cardNumber", "#cardType");
    }
    if ($(".mercadoPagoCardNumber").length > 0) {
        cleave.handleCreditCardNumber(".mercadoPagoCardNumber", "#cardType");
    }
};

/**
 * @function useSameMailPhoneAsAddress
 * @description fill user information for payment data
 */
base.useSameMailPhoneAsAddress = function () {
    var fillSameFields = function () {
        $(".js-mp-phone").val($("#phoneNumber").val());
        $(".js-mp-email").val($("#email").val());
    };

    $("#useSameMailPhoneAsAddress").change(function () {
        $(".js-mail-phone-container").toggleClass("checkout-hidden", this.checked);


        if (this.checked) {
            fillSameFields();
            $("#phoneNumber").on("change.usesame", fillSameFields);
            $("#email").on("change.usesame", fillSameFields);
        } else {
            $(".js-mp-phone").val("");
            $(".js-mp-email").val("");
            $("#phoneNumber").off("change.usesame");
            $("#email").off("change.usesame");
        }
    });
};

/**
 * @function changePaymentOption
 * @description Change payment option
 */
base.changePaymentOption = function () {
    $(".payment-options [role=tab]").on("click", base.methods.handlePaymentOptionChange); // By click
};

/**
 * @function initPaymentOption
 * @description Initiate payment option
 */
base.initPaymentOption = function () {
    // Initial
    $(".payment-options [role=tab].enabled").trigger("click");
    base.methods.handlePaymentOptionChange.call($(".payment-options [role=tab].active"));
};
base.methods.clearCreditCardForm = function () {
    console.log("clearCreditCardForm------------------->");
    $('input[name$="_cardNumber"]').data('cleave').setRawValue('');
    $('select[name$="_expirationMonth"]').val('');
    $('select[name$="_expirationYear"]').val('');
    $('input[name$="_securityCode"]').val('');
    $('input[name$="cardOwner"]').val('');
    $('input[name$="expirationDate"]').val('');
};

// $('.cardSelector').on('change', function (e) {
//     console.log('cardSelector - change');
//     e.preventDefault();
//     if(e.target.value){
//         console.log('seleccion con datos');
//         $('.payment-information').data('is-new-payment', false);
//         base.methods.clearCreditCardForm();
//         $('.credit-card-form').addClass('checkout-hidden');
//     }else{
//         $('.payment-information').data('is-new-payment', true);
//         base.methods.clearCreditCardForm();
//         $('.credit-card-form').removeClass('checkout-hidden');
//     }
// });

base.methods.updateBillingAddress = function (order) {
   var billing = order.billing;
   if (!billing.billingAddress || !billing.billingAddress.address) return;

   var form = $('form[name=dwfrm_billing]');
   if (!form) return;

   $('input[name$=_firstName]', form).val(billing.billingAddress.address.firstName);
   $('input[name$=_lastName]', form).val(billing.billingAddress.address.lastName);
   $('input[name$=_address1]', form).val(billing.billingAddress.address.address1);
   $('input[name$=_city]', form).val(billing.billingAddress.address.city);
   $('input[name$=_postalCode]', form).val(billing.billingAddress.address.postalCode);
   $('select[name$=_stateCode],input[name$=_stateCode]', form)
       .val(billing.billingAddress.address.stateCode);
   $('select[name$=_country]', form).val(billing.billingAddress.address.countryCode.value);
   $('input[name$=_phone]', form).val(billing.billingAddress.address.phone);
   $('input[name$=_suburb]', form).val(billing.billingAddress.address.suburb);
   $('input[name$=_county]', form).val(billing.billingAddress.address.county);
   $('input[name$=_numberExt]', form).val(billing.billingAddress.address.numberExt);
   $('input[name$=_numberInt]', form).val(billing.billingAddress.address.numberInt);
   $('input[name$=_email]', form).val(order.orderEmail);
}

module.exports = base;

