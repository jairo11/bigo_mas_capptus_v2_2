/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./cartridges/int_mercadopago/cartridge/client/default/js/checkout.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./cartridges/app_storefront_base/cartridge/client/default/js/checkout/address.js":
/*!****************************************************************************************!*\
  !*** ./cartridges/app_storefront_base/cartridge/client/default/js/checkout/address.js ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Populate the Billing Address Summary View
 * @param {string} parentSelector - the top level DOM selector for a unique address summary
 * @param {Object} address - the address data
 */
function populateAddressSummary(parentSelector, address) {
    $.each(address, function (attr) {
        var val = address[attr];
        $('.' + attr, parentSelector).text(val || '');
    });
}

/**
 * returns a formed <option /> element
 * @param {Object} shipping - the shipping object (shipment model)
 * @param {boolean} selected - current shipping is selected (for PLI)
 * @param {order} order - the Order model
 * @param {Object} [options] - options
 * @returns {Object} - the jQuery / DOMElement
 */
function optionValueForAddress(shipping, selected, order, options) {
    var safeOptions = options || {};
    var isBilling = safeOptions.type && safeOptions.type === 'billing';
    var className = safeOptions.className || '';
    var isSelected = selected;
    var isNew = !shipping;
    if (typeof shipping === 'string') {
        return $('<option class="' + className + '" disabled>' + shipping + '</option>');
    }
    var safeShipping = shipping || {};
    var shippingAddress = safeShipping.shippingAddress || {};

    if (isBilling && isNew && !order.billing.matchingAddressId) {
        shippingAddress = order.billing.billingAddress.address || {};
        isNew = false;
        isSelected = true;
        safeShipping.UUID = 'manual-entry';
    }

    var uuid = safeShipping.UUID ? safeShipping.UUID : 'new';
    var optionEl = $('<option class="' + className + '" />');
    optionEl.val(uuid);

    var title;

    if (isNew) {
        title = order.resources.addNewAddress;
    } else {
        title = [];
        if (shippingAddress.firstName) {
            title.push(shippingAddress.firstName);
        }
        if (shippingAddress.lastName) {
            title.push(shippingAddress.lastName);
        }
        if (shippingAddress.address1) {
            title.push(shippingAddress.address1);
        }
        if (shippingAddress.address2) {
            title.push(shippingAddress.address2);
        }
        if (shippingAddress.city) {
            if (shippingAddress.state) {
                title.push(shippingAddress.city + ',');
            } else {
                title.push(shippingAddress.city);
            }
        }
        if (shippingAddress.stateCode) {
            title.push(shippingAddress.stateCode);
        }
        if (shippingAddress.postalCode) {
            title.push(shippingAddress.postalCode);
        }
        if (!isBilling && safeShipping.selectedShippingMethod) {
            title.push('-');
            title.push(safeShipping.selectedShippingMethod.displayName);
        }

        if (title.length > 2) {
            title = title.join(' ');
        } else {
            title = order.resources.newAddress;
        }
    }
    optionEl.text(title);

    var keyMap = {
        'data-first-name': 'firstName',
        'data-last-name': 'lastName',
        'data-address1': 'address1',
        'data-address2': 'address2',
        'data-city': 'city',
        'data-state-code': 'stateCode',
        'data-postal-code': 'postalCode',
        'data-country-code': 'countryCode',
        'data-phone': 'phone'
    };
    $.each(keyMap, function (key) {
        var mappedKey = keyMap[key];
        var mappedValue = shippingAddress[mappedKey];
        // In case of country code
        if (mappedValue && typeof mappedValue === 'object') {
            mappedValue = mappedValue.value;
        }

        optionEl.attr(key, mappedValue || '');
    });

    var giftObj = {
        'data-is-gift': 'isGift',
        'data-gift-message': 'giftMessage'
    };

    $.each(giftObj, function (key) {
        var mappedKey = giftObj[key];
        var mappedValue = safeShipping[mappedKey];
        optionEl.attr(key, mappedValue || '');
    });

    if (isSelected) {
        optionEl.attr('selected', true);
    }

    return optionEl;
}

/**
 * returns address properties from a UI form
 * @param {Form} form - the Form element
 * @returns {Object} - a JSON object with all values
 */
function getAddressFieldsFromUI(form) {
    var address = {
        firstName: $('input[name$=_firstName]', form).val(),
        lastName: $('input[name$=_lastName]', form).val(),
        address1: $('input[name$=_address1]', form).val(),
        address2: $('input[name$=_address2]', form).val(),
        city: $('input[name$=_city]', form).val(),
        postalCode: $('input[name$=_postalCode]', form).val(),
        stateCode: $('select[name$=_stateCode],input[name$=_stateCode]', form).val(),
        countryCode: $('select[name$=_country]', form).val(),
        phone: $('input[name$=_phone]', form).val()
    };
    return address;
}

module.exports = {
    methods: {
        populateAddressSummary: populateAddressSummary,
        optionValueForAddress: optionValueForAddress,
        getAddressFieldsFromUI: getAddressFieldsFromUI
    },

    showDetails: function () {
        $('.btn-show-details').on('click', function () {
            var form = $(this).closest('form');
            form.attr('data-address-mode', 'details');
            form.find('.multi-ship-address-actions').removeClass('d-none');
            form.find('.multi-ship-action-buttons .col-12.btn-save-multi-ship').addClass('d-none');
        });
    },

    addNewAddress: function () {
        $('.btn-add-new').on('click', function () {
            var $el = $(this);
            if ($el.parents('#dwfrm_billing').length > 0) {
                // Handle billing address case
                $('body').trigger('checkout:clearBillingForm');
                var $option = $($el.parents('form').find('.addressSelector option')[0]);
                $option.attr('value', 'new');
                var $newTitle = $('#dwfrm_billing input[name=localizedNewAddressTitle]').val();
                $option.text($newTitle);
                $option.prop('selected', 'selected');
                $el.parents('[data-address-mode]').attr('data-address-mode', 'new');
            } else {
                // Handle shipping address case
                var $newEl = $el.parents('form').find('.addressSelector option[value=new]');
                $newEl.prop('selected', 'selected');
                $newEl.parent().trigger('change');
            }
        });
    }
};


/***/ }),

/***/ "./cartridges/app_storefront_base/cartridge/client/default/js/checkout/billing.js":
/*!****************************************************************************************!*\
  !*** ./cartridges/app_storefront_base/cartridge/client/default/js/checkout/billing.js ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var addressHelpers = __webpack_require__(/*! ./address */ "./cartridges/app_storefront_base/cartridge/client/default/js/checkout/address.js");
var cleave = __webpack_require__(/*! ../components/cleave */ "./cartridges/app_storefront_base/cartridge/client/default/js/components/cleave.js");

/**
 * updates the billing address selector within billing forms
 * @param {Object} order - the order model
 * @param {Object} customer - the customer model
 */
function updateBillingAddressSelector(order, customer) {
    var shippings = order.shipping;

    var form = $('form[name$=billing]')[0];
    var $billingAddressSelector = $('.addressSelector', form);
    var hasSelectedAddress = false;

    if ($billingAddressSelector && $billingAddressSelector.length === 1) {
        $billingAddressSelector.empty();
        // Add New Address option
        $billingAddressSelector.append(addressHelpers.methods.optionValueForAddress(
            null,
            false,
            order,
            { type: 'billing' }));

        // Separator -
        $billingAddressSelector.append(addressHelpers.methods.optionValueForAddress(
            order.resources.shippingAddresses, false, order, {
                // className: 'multi-shipping',
                type: 'billing'
            }
        ));

        shippings.forEach(function (aShipping) {
            var isSelected = order.billing.matchingAddressId === aShipping.UUID;
            hasSelectedAddress = hasSelectedAddress || isSelected;
            // Shipping Address option
            $billingAddressSelector.append(
                addressHelpers.methods.optionValueForAddress(aShipping, isSelected, order,
                    {
                        // className: 'multi-shipping',
                        type: 'billing'
                    }
                )
            );
        });

        if (customer.addresses && customer.addresses.length > 0) {
            $billingAddressSelector.append(addressHelpers.methods.optionValueForAddress(
                order.resources.accountAddresses, false, order));
            customer.addresses.forEach(function (address) {
                var isSelected = order.billing.matchingAddressId === address.ID;
                hasSelectedAddress = hasSelectedAddress || isSelected;
                // Customer Address option
                $billingAddressSelector.append(
                    addressHelpers.methods.optionValueForAddress({
                        UUID: 'ab_' + address.ID,
                        shippingAddress: address
                    }, isSelected, order, { type: 'billing' })
                );
            });
        }
    }

    if (hasSelectedAddress
        || (!order.billing.matchingAddressId && order.billing.billingAddress.address)) {
        // show
        $(form).attr('data-address-mode', 'edit');
    } else {
        $(form).attr('data-address-mode', 'new');
    }

    $billingAddressSelector.show();
}

/**
 * Updates the billing address form values within payment forms without any payment instrument validation
 * @param {Object} order - the order model
 */
function updateBillingAddress(order) {
    var billing = order.billing;
    if (!billing.billingAddress || !billing.billingAddress.address) return;

    var form = $('form[name=dwfrm_billing]');
    if (!form) return;

    $('input[name$=_firstName]', form).val(billing.billingAddress.address.firstName);
    $('input[name$=_lastName]', form).val(billing.billingAddress.address.lastName);
    $('input[name$=_address1]', form).val(billing.billingAddress.address.address1);
    $('input[name$=_address2]', form).val(billing.billingAddress.address.address2);
    $('input[name$=_city]', form).val(billing.billingAddress.address.city);
    $('input[name$=_postalCode]', form).val(billing.billingAddress.address.postalCode);
    $('select[name$=_stateCode],input[name$=_stateCode]', form)
        .val(billing.billingAddress.address.stateCode);
    $('select[name$=_country]', form).val(billing.billingAddress.address.countryCode.value);
    $('input[name$=_phone]', form).val(billing.billingAddress.address.phone);
    $('input[name$=_email]', form).val(order.orderEmail);
}

/**
 * Validate and update payment instrument form fields
 * @param {Object} order - the order model
 */
function validateAndUpdateBillingPaymentInstrument(order) {
    var billing = order.billing;
    if (!billing.payment || !billing.payment.selectedPaymentInstruments
        || billing.payment.selectedPaymentInstruments.length <= 0) return;

    var form = $('form[name=dwfrm_billing]');
    if (!form) return;

    var instrument = billing.payment.selectedPaymentInstruments[0];
    $('select[name$=expirationMonth]', form).val(instrument.expirationMonth);
    $('select[name$=expirationYear]', form).val(instrument.expirationYear);
    // Force security code and card number clear
    $('input[name$=securityCode]', form).val('');
    $('input[name$=cardNumber]').data('cleave').setRawValue('');
}

/**
 * Updates the billing address form values within payment forms
 * @param {Object} order - the order model
 */
function updateBillingAddressFormValues(order) {
    module.exports.methods.updateBillingAddress(order);
    module.exports.methods.validateAndUpdateBillingPaymentInstrument(order);
}

/**
 * clears the billing address form values
 */
function clearBillingAddressFormValues() {
    updateBillingAddressFormValues({
        billing: {
            billingAddress: {
                address: {
                    countryCode: {}
                }
            }
        }
    });
}

/**
 * update billing address summary and contact information
 * @param {Object} order - checkout model to use as basis of new truth
 */
function updateBillingAddressSummary(order) {
    // update billing address summary
    addressHelpers.methods.populateAddressSummary('.billing .address-summary',
        order.billing.billingAddress.address);

    // update billing parts of order summary
    $('.order-summary-email').text(order.orderEmail);

    if (order.billing.billingAddress.address) {
        $('.order-summary-phone').text(order.billing.billingAddress.address.phone);
    }
}

/**
 * Updates the billing information in checkout, based on the supplied order model
 * @param {Object} order - checkout model to use as basis of new truth
 * @param {Object} customer - customer model to use as basis of new truth
 * @param {Object} [options] - options
 */
function updateBillingInformation(order, customer) {
    updateBillingAddressSelector(order, customer);

    // update billing address form
    updateBillingAddressFormValues(order);

    // update billing address summary and billing parts of order summary
    updateBillingAddressSummary(order);
}

/**
 * Updates the payment information in checkout, based on the supplied order model
 * @param {Object} order - checkout model to use as basis of new truth
 */
function updatePaymentInformation(order) {
    // update payment details
    var $paymentSummary = $('.payment-details');
    var htmlToAppend = '';

    if (order.billing.payment && order.billing.payment.selectedPaymentInstruments
        && order.billing.payment.selectedPaymentInstruments.length > 0) {
        htmlToAppend += '<span>' + order.resources.cardType + ' '
            + order.billing.payment.selectedPaymentInstruments[0].type
            + '</span><div>'
            + order.billing.payment.selectedPaymentInstruments[0].maskedCreditCardNumber
            + '</div><div><span>'
            + order.resources.cardEnding + ' '
            + order.billing.payment.selectedPaymentInstruments[0].expirationMonth
            + '/' + order.billing.payment.selectedPaymentInstruments[0].expirationYear
            + '</span></div>';
    }

    $paymentSummary.empty().append(htmlToAppend);
}

/**
 * clears the credit card form
 */
function clearCreditCardForm() {
    $('input[name$="_cardNumber"]').data('cleave').setRawValue('');
    $('select[name$="_expirationMonth"]').val('');
    $('select[name$="_expirationYear"]').val('');
    $('input[name$="_securityCode"]').val('');
}

module.exports = {
    methods: {
        updateBillingAddressSelector: updateBillingAddressSelector,
        updateBillingAddressFormValues: updateBillingAddressFormValues,
        clearBillingAddressFormValues: clearBillingAddressFormValues,
        updateBillingInformation: updateBillingInformation,
        updatePaymentInformation: updatePaymentInformation,
        clearCreditCardForm: clearCreditCardForm,
        updateBillingAddress: updateBillingAddress,
        validateAndUpdateBillingPaymentInstrument: validateAndUpdateBillingPaymentInstrument,
        updateBillingAddressSummary: updateBillingAddressSummary
    },

    showBillingDetails: function () {
        $('.btn-show-billing-details').on('click', function () {
            $(this).parents('[data-address-mode]').attr('data-address-mode', 'new');
        });
    },

    hideBillingDetails: function () {
        $('.btn-hide-billing-details').on('click', function () {
            $(this).parents('[data-address-mode]').attr('data-address-mode', 'shipment');
        });
    },

    selectBillingAddress: function () {
        $('.payment-form .addressSelector').on('change', function () {
            var form = $(this).parents('form')[0];
            var selectedOption = $('option:selected', this);
            var optionID = selectedOption[0].value;

            if (optionID === 'new') {
                // Show Address
                $(form).attr('data-address-mode', 'new');
            } else {
                // Hide Address
                $(form).attr('data-address-mode', 'shipment');
            }

            // Copy fields
            var attrs = selectedOption.data();
            var element;

            Object.keys(attrs).forEach(function (attr) {
                element = attr === 'countryCode' ? 'country' : attr;
                if (element === 'cardNumber') {
                    $('.cardNumber').data('cleave').setRawValue(attrs[attr]);
                } else {
                    $('[name$=' + element + ']', form).val(attrs[attr]);
                }
            });
        });
    },

    handleCreditCardNumber: function () {
        cleave.handleCreditCardNumber('.cardNumber', '#cardType');
    },

    santitizeForm: function () {
        $('body').on('checkout:serializeBilling', function (e, data) {
            var serializedForm = cleave.serializeData(data.form);

            data.callback(serializedForm);
        });
    },

    selectSavedPaymentInstrument: function () {
        $(document).on('click', '.saved-payment-instrument', function (e) {
            e.preventDefault();
            $('.saved-payment-security-code').val('');
            $('.saved-payment-instrument').removeClass('selected-payment');
            $(this).addClass('selected-payment');
            $('.saved-payment-instrument .card-image').removeClass('checkout-hidden');
            $('.saved-payment-instrument .security-code-input').addClass('checkout-hidden');
            $('.saved-payment-instrument.selected-payment' +
                ' .card-image').addClass('checkout-hidden');
            $('.saved-payment-instrument.selected-payment ' +
                '.security-code-input').removeClass('checkout-hidden');
        });
    },

    addNewPaymentInstrument: function () {
        $('.btn.add-payment').on('click', function (e) {
            e.preventDefault();
            $('.payment-information').data('is-new-payment', true);
            clearCreditCardForm();
            $('.credit-card-form').removeClass('checkout-hidden');
            $('.user-payment-instruments').addClass('checkout-hidden');
        });
    },

    cancelNewPayment: function () {
        $('.cancel-new-payment').on('click', function (e) {
            e.preventDefault();
            $('.payment-information').data('is-new-payment', false);
            clearCreditCardForm();
            $('.user-payment-instruments').removeClass('checkout-hidden');
            $('.credit-card-form').addClass('checkout-hidden');
        });
    },

    clearBillingForm: function () {
        $('body').on('checkout:clearBillingForm', function () {
            clearBillingAddressFormValues();
        });
    },

    paymentTabs: function () {
        $('.payment-options .nav-item').on('click', function (e) {
            e.preventDefault();
            var methodID = $(this).data('method-id');
            $('.payment-information').data('payment-method-id', methodID);
        });
    }
};


/***/ }),

/***/ "./cartridges/app_storefront_base/cartridge/client/default/js/checkout/formErrors.js":
/*!*******************************************************************************************!*\
  !*** ./cartridges/app_storefront_base/cartridge/client/default/js/checkout/formErrors.js ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var scrollAnimate = __webpack_require__(/*! ../components/scrollAnimate */ "./cartridges/app_storefront_base/cartridge/client/default/js/components/scrollAnimate.js");

/**
 * Display error messages and highlight form fields with errors.
 * @param {string} parentSelector - the form which contains the fields
 * @param {Object} fieldErrors - the fields with errors
 */
function loadFormErrors(parentSelector, fieldErrors) { // eslint-disable-line
    // Display error messages and highlight form fields with errors.
    $.each(fieldErrors, function (attr) {
        $('*[name=' + attr + ']', parentSelector)
            .addClass('is-invalid')
            .siblings('.invalid-feedback')
            .html(fieldErrors[attr]);
    });
    // Animate to top of form that has errors
    scrollAnimate($(parentSelector));
}

/**
 * Clear the form errors.
 * @param {string} parentSelector - the parent form selector.
 */
function clearPreviousErrors(parentSelector) {
    $(parentSelector).find('.form-control.is-invalid').removeClass('is-invalid');
    $('.error-message').hide();
}

module.exports = {
    loadFormErrors: loadFormErrors,
    clearPreviousErrors: clearPreviousErrors
};


/***/ }),

/***/ "./cartridges/app_storefront_base/cartridge/client/default/js/checkout/summary.js":
/*!****************************************************************************************!*\
  !*** ./cartridges/app_storefront_base/cartridge/client/default/js/checkout/summary.js ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * updates the totals summary
 * @param {Array} totals - the totals data
 */
function updateTotals(totals) {
    $('.shipping-total-cost').text(totals.totalShippingCost);
    $('.tax-total').text(totals.totalTax);
    $('.sub-total').text(totals.subTotal);
    $('.grand-total-sum').text(totals.grandTotal);

    if (totals.orderLevelDiscountTotal.value > 0) {
        $('.order-discount').removeClass('hide-order-discount');
        $('.order-discount-total').text('- ' + totals.orderLevelDiscountTotal.formatted);
    } else {
        $('.order-discount').addClass('hide-order-discount');
    }

    if (totals.shippingLevelDiscountTotal.value > 0) {
        $('.shipping-discount').removeClass('hide-shipping-discount');
        $('.shipping-discount-total').text('- ' +
            totals.shippingLevelDiscountTotal.formatted);
    } else {
        $('.shipping-discount').addClass('hide-shipping-discount');
    }
}

/**
 * updates the order product shipping summary for an order model
 * @param {Object} order - the order model
 */
function updateOrderProductSummaryInformation(order) {
    var $productSummary = $('<div />');
    order.shipping.forEach(function (shipping) {
        shipping.productLineItems.items.forEach(function (lineItem) {
            var pli = $('[data-product-line-item=' + lineItem.UUID + ']');
            $productSummary.append(pli);
        });

        var address = shipping.shippingAddress || {};
        var selectedMethod = shipping.selectedShippingMethod;

        var nameLine = address.firstName ? address.firstName + ' ' : '';
        if (address.lastName) nameLine += address.lastName;

        var address1Line = address.address1;
        var address2Line = address.address2;

        var phoneLine = address.phone;

        var shippingCost = selectedMethod ? selectedMethod.shippingCost : '';
        var methodNameLine = selectedMethod ? selectedMethod.displayName : '';
        var methodArrivalTime = selectedMethod && selectedMethod.estimatedArrivalTime
            ? '( ' + selectedMethod.estimatedArrivalTime + ' )'
            : '';

        var tmpl = $('#pli-shipping-summary-template').clone();

        if (shipping.productLineItems.items && shipping.productLineItems.items.length > 1) {
            $('h5 > span').text(' - ' + shipping.productLineItems.items.length + ' '
                + order.resources.items);
        } else {
            $('h5 > span').text('');
        }

        var stateRequiredAttr = $('#shippingState').attr('required');
        var isRequired = stateRequiredAttr !== undefined && stateRequiredAttr !== false;
        var stateExists = (shipping.shippingAddress && shipping.shippingAddress.stateCode)
            ? shipping.shippingAddress.stateCode
            : false;
        var stateBoolean = false;
        if ((isRequired && stateExists) || (!isRequired)) {
            stateBoolean = true;
        }

        var shippingForm = $('.multi-shipping input[name="shipmentUUID"][value="' + shipping.UUID + '"]').parent();

        if (shipping.shippingAddress
            && shipping.shippingAddress.firstName
            && shipping.shippingAddress.address1
            && shipping.shippingAddress.city
            && stateBoolean
            && shipping.shippingAddress.countryCode
            && (shipping.shippingAddress.phone || shipping.productLineItems.items[0].fromStoreId)) {
            $('.ship-to-name', tmpl).text(nameLine);
            $('.ship-to-address1', tmpl).text(address1Line);
            $('.ship-to-address2', tmpl).text(address2Line);
            $('.ship-to-city', tmpl).text(address.city);
            if (address.stateCode) {
                $('.ship-to-st', tmpl).text(address.stateCode);
            }
            $('.ship-to-zip', tmpl).text(address.postalCode);
            $('.ship-to-phone', tmpl).text(phoneLine);

            if (!address2Line) {
                $('.ship-to-address2', tmpl).hide();
            }

            if (!phoneLine) {
                $('.ship-to-phone', tmpl).hide();
            }

            shippingForm.find('.ship-to-message').text('');
        } else {
            shippingForm.find('.ship-to-message').text(order.resources.addressIncomplete);
        }

        if (shipping.isGift) {
            $('.gift-message-summary', tmpl).text(shipping.giftMessage);
        } else {
            $('.gift-summary', tmpl).addClass('d-none');
        }

        // checking h5 title shipping to or pickup
        var $shippingAddressLabel = $('.shipping-header-text', tmpl);
        $('body').trigger('shipping:updateAddressLabelText',
            { selectedShippingMethod: selectedMethod, resources: order.resources, shippingAddressLabel: $shippingAddressLabel });

        if (shipping.selectedShippingMethod) {
            $('.display-name', tmpl).text(methodNameLine);
            $('.arrival-time', tmpl).text(methodArrivalTime);
            $('.price', tmpl).text(shippingCost);
        }

        var $shippingSummary = $('<div class="multi-shipping" data-shipment-summary="'
            + shipping.UUID + '" />');
        $shippingSummary.html(tmpl.html());
        $productSummary.append($shippingSummary);
    });

    $('.product-summary-block').html($productSummary.html());

    // Also update the line item prices, as they might have been altered
    $('.grand-total-price').text(order.totals.subTotal);
    order.items.items.forEach(function (item) {
        if (item.priceTotal && item.priceTotal.renderedPrice) {
            $('.item-total-' + item.UUID).empty().append(item.priceTotal.renderedPrice);
        }
    });
}

module.exports = {
    updateTotals: updateTotals,
    updateOrderProductSummaryInformation: updateOrderProductSummaryInformation
};


/***/ }),

/***/ "./cartridges/app_storefront_base/cartridge/client/default/js/components/cleave.js":
/*!*****************************************************************************************!*\
  !*** ./cartridges/app_storefront_base/cartridge/client/default/js/components/cleave.js ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Cleave = __webpack_require__(/*! cleave.js */ "./node_modules/cleave.js/dist/cleave-esm.js").default;

module.exports = {
    handleCreditCardNumber: function (cardFieldSelector, cardTypeSelector) {
        var cleave = new Cleave(cardFieldSelector, {
            creditCard: true,
            onCreditCardTypeChanged: function (type) {
                var creditCardTypes = {
                    visa: 'Visa',
                    mastercard: 'Master Card',
                    amex: 'Amex',
                    discover: 'Discover',
                    unknown: 'Unknown'
                };

                var cardType = creditCardTypes[Object.keys(creditCardTypes).indexOf(type) > -1
                    ? type
                    : 'unknown'];
                $(cardTypeSelector).val(cardType);
                $('.card-number-wrapper').attr('data-type', type);
                if (type === 'visa' || type === 'mastercard' || type === 'discover') {
                    $('#securityCode').attr('maxlength', 3);
                } else {
                    $('#securityCode').attr('maxlength', 4);
                }
            }
        });

        $(cardFieldSelector).data('cleave', cleave);
    },

    serializeData: function (form) {
        var serializedArray = form.serializeArray();

        serializedArray.forEach(function (item) {
            if (item.name.indexOf('cardNumber') > -1) {
                item.value = $('#cardNumber').data('cleave').getRawValue(); // eslint-disable-line
            }
        });

        return $.param(serializedArray);
    }
};


/***/ }),

/***/ "./cartridges/app_storefront_base/cartridge/client/default/js/components/scrollAnimate.js":
/*!************************************************************************************************!*\
  !*** ./cartridges/app_storefront_base/cartridge/client/default/js/components/scrollAnimate.js ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (element) {
    var position = element && element.length ? element.offset().top : 0;
    $('html, body').animate({
        scrollTop: position
    }, 500);
    if (!element) {
        $('.logo-home').focus();
    }
};


/***/ }),

/***/ "./cartridges/app_storefront_base/cartridge/client/default/js/util.js":
/*!****************************************************************************!*\
  !*** ./cartridges/app_storefront_base/cartridge/client/default/js/util.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (include) {
    if (typeof include === 'function') {
        include();
    } else if (typeof include === 'object') {
        Object.keys(include).forEach(function (key) {
            if (typeof include[key] === 'function') {
                include[key]();
            }
        });
    }
};


/***/ }),

/***/ "./cartridges/int_mercadopago/cartridge/client/default/js/checkout.js":
/*!****************************************************************************!*\
  !*** ./cartridges/int_mercadopago/cartridge/client/default/js/checkout.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/* global $ */

var processInclude = __webpack_require__(/*! base/util */ "./cartridges/app_storefront_base/cartridge/client/default/js/util.js");

$(document).ready(function () {
    processInclude(__webpack_require__(/*! ./checkout/checkout */ "./cartridges/int_mercadopago/cartridge/client/default/js/checkout/checkout.js"));
    processInclude(__webpack_require__(/*! ./checkout/mercadoPago */ "./cartridges/int_mercadopago/cartridge/client/default/js/checkout/mercadoPago.js"));
});


/***/ }),

/***/ "./cartridges/int_mercadopago/cartridge/client/default/js/checkout/address.js":
/*!************************************************************************************!*\
  !*** ./cartridges/int_mercadopago/cartridge/client/default/js/checkout/address.js ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Populate the Billing Address Summary View
 * @param {string} parentSelector - the top level DOM selector for a unique address summary
 * @param {Object} address - the address data
 */
function populateAddressSummary(parentSelector, address) {
    console.log("-------------->populateAddressSummary");
    $.each(address, function (attr) {
        var val = address[attr];
        $('.' + attr, parentSelector).text(val || '');
    });
}

/**
 * returns a formed <option /> element
 * @param {Object} shipping - the shipping object (shipment model)
 * @param {boolean} selected - current shipping is selected (for PLI)
 * @param {order} order - the Order model
 * @param {Object} [options] - options
 * @returns {Object} - the jQuery / DOMElement
 */
function optionValueForAddress(shipping, selected, order, options) {
    console.log("-------------->optionValueForAddress");
    var safeOptions = options || {};
    var isBilling = safeOptions.type && safeOptions.type === 'billing';
    var className = safeOptions.className || '';
    var isSelected = selected;
    var isNew = !shipping;
    if (typeof shipping === 'string') {
        return $('<option class="' + className + '" disabled>' + shipping + '</option>');
    }
    var safeShipping = shipping || {};
    var shippingAddress = safeShipping.shippingAddress || {};

    if (isBilling && isNew && !order.billing.matchingAddressId) {
        shippingAddress = order.billing.billingAddress.address || {};
        isNew = false;
        isSelected = true;
        safeShipping.UUID = 'manual-entry';
    }

    var uuid = safeShipping.UUID ? safeShipping.UUID : 'new';
    var optionEl = $('<option class="' + className + '" />');
    optionEl.val(uuid);

    var title;

    if (isNew) {
        title = order.resources.addNewAddress;
    } else {
        title = [];
        if (shippingAddress.firstName) {
            title.push(shippingAddress.firstName);
        }
        if (shippingAddress.lastName) {
            title.push(shippingAddress.lastName);
        }
        if (shippingAddress.address1) {
            title.push(shippingAddress.address1);
        }
        if (shippingAddress.address2) {
            title.push(shippingAddress.address2);
        }
        if (shippingAddress.city) {
            if (shippingAddress.state) {
                title.push(shippingAddress.city + ',');
            } else {
                title.push(shippingAddress.city);
            }
        }
        if (shippingAddress.stateCode) {
            title.push(shippingAddress.stateCode);
        }
        if (shippingAddress.postalCode) {
            title.push(shippingAddress.postalCode);
        }
        if (!isBilling && safeShipping.selectedShippingMethod) {
            title.push('-');
            title.push(safeShipping.selectedShippingMethod.displayName);
        }

        if (title.length > 2) {
            title = title.join(' ');
        } else {
            title = order.resources.newAddress;
        }
    }
    optionEl.text(title);

    var keyMap = {
        'data-first-name': 'firstName',
        'data-last-name': 'lastName',
        'data-address1': 'address1',
        'data-address2': 'address2',
        'data-city': 'city',
        'data-state-code': 'stateCode',
        'data-postal-code': 'postalCode',
        'data-country-code': 'countryCode',
        'data-number-int': 'numberInt',
        'data-number-ext': 'numberExt',
        'data-suburb': 'suburb',
        'data-county': 'county',
        'data-address-id': 'addressId',
    };
    $.each(keyMap, function (key) {
        var mappedKey = keyMap[key];
        var mappedValue = shippingAddress[mappedKey];
        // In case of country code
        if (mappedValue && typeof mappedValue === 'object') {
            mappedValue = mappedValue.value;
        }

        optionEl.attr(key, mappedValue || '');
    });

    var giftObj = {
        'data-is-gift': 'isGift',
        'data-gift-message': 'giftMessage'
    };

    $.each(giftObj, function (key) {
        var mappedKey = giftObj[key];
        var mappedValue = safeShipping[mappedKey];
        optionEl.attr(key, mappedValue || '');
    });

    if (isSelected) {
        optionEl.attr('selected', true);
    }

    return optionEl;
}

/**
 * returns address properties from a UI form
 * @param {Form} form - the Form element
 * @returns {Object} - a JSON object with all values
 */
function getAddressFieldsFromUI(form) {
    console.log("-------------->getAddressFieldsFromUI");
    var address = {
        firstName: $('input[name$=_firstName]', form).val(),
        lastName: $('input[name$=_lastName]', form).val(),
        address1: $('input[name$=_address1]', form).val(),
        address2: $('input[name$=_address2]', form).val(),
        city: $('input[name$=_city]', form).val(),
        postalCode: $('input[name$=_postalCode]', form).val(),
        stateCode: $('select[name$=_stateCode],input[name$=_stateCode]', form).val(),
        countryCode: $('input[name$=_country]', form).val(),
        phone: $('input[name$=_phone]', form).val(),
        suburb: $('select[name$=_suburb]', form).val(),
        county: $('input[name$=_county]', form).val(),
        numberExt: $('input[name$=_numberExt]', form).val(),
        numberInt: $('input[name$=_numberInt]', form).val(),
        addressId: $('input[name$=_addressId]', form).val()
    };
    if(!address.suburb){
        address.suburb =$('input[name$=_suburb]', form).val()
    }
    return address;
}

module.exports = {
    methods: {
        populateAddressSummary: populateAddressSummary,
        optionValueForAddress: optionValueForAddress,
        getAddressFieldsFromUI: getAddressFieldsFromUI
    },

    showDetails: function () {
        console.log("-------------->showDetails");
        $('.btn-show-details').on('click', function () {
            console.log("-------------->showDetails");
            var form = $(this).closest('form');
            form.attr('data-address-mode', 'details');
            //form.find('.multi-ship-address-actions').removeClass('d-none');
            //form.find('.multi-ship-action-buttons .col-12.btn-save-multi-ship').addClass('d-none');
        });
    },

    addNewAddress: function () {
        console.log("-------------->addNewAddress");
        $('.btn-add-new').on('click', function () {
            console.log("-------------->addNewAddress");
            var $el = $(this);
            if ($el.parents('#dwfrm_billing').length > 0) {
                // Handle billing address case
                $('body').trigger('checkout:clearBillingForm');
                var $option = $($el.parents('form').find('.addressSelector option')[0]);
                $option.attr('value', 'new');
                var $newTitle = $('#dwfrm_billing input[name=localizedNewAddressTitle]').val();
                $option.text($newTitle);
                $option.prop('selected', 'selected');
                $el.parents('[data-address-mode]').attr('data-address-mode', 'new');
            } else {
                // Handle shipping address case
                var $newEl = $el.parents('form').find('.addressSelector option[value=new]');
                $newEl.prop('selected', 'selected');
                $newEl.parent().trigger('change');
            }
        });
    }
};


/***/ }),

/***/ "./cartridges/int_mercadopago/cartridge/client/default/js/checkout/billing.js":
/*!************************************************************************************!*\
  !*** ./cartridges/int_mercadopago/cartridge/client/default/js/checkout/billing.js ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/* global $ */

var base = __webpack_require__(/*! base/checkout/billing */ "./cartridges/app_storefront_base/cartridge/client/default/js/checkout/billing.js");
var cleave = __webpack_require__(/*! base/components/cleave */ "./cartridges/app_storefront_base/cartridge/client/default/js/components/cleave.js");

function getPaymentMethodName(order, selectedPaymentInstrument) {
    var paymentMethodName = "";
    order.billing.payment.applicablePaymentMethods.forEach(function (paymentMethod) {
        if(paymentMethod.ID === selectedPaymentInstrument.paymentMethod){
            paymentMethodName=paymentMethod.name;
        }
    });
    return paymentMethodName;
}

base.methods.validateAndUpdateBillingPaymentInstrument =function (order) {
    var billing = order.billing;
    if (!billing.payment || !billing.payment.selectedPaymentInstruments
        || billing.payment.selectedPaymentInstruments.length <= 0) return;

    var form = $('form[name=dwfrm_billing]');
    if (!form) return;

    var instrument = billing.payment.selectedPaymentInstruments[0];
    $('select[name$=expirationMonth]', form).val(instrument.expirationMonth);
    $('select[name$=expirationYear]', form).val(instrument.expirationYear);
    // Force security code and card number clear
    $('input[name$=securityCode]', form).val('');
    if($('input[name$=cardNumber]').length > 0){
        $('input[name$=cardNumber]').data('cleave').setRawValue('');
    }
};



/**
 * @function updatePaymentInformation
 * @description Update payment details summary based on payment method
 * @param {Object} order - checkout model to use as basis of new truth
 */
base.methods.updatePaymentInformation = function (order) {
    // update payment details
    var $paymentSummary = $(".payment-details");
    var htmlToAppend = "";

    if (order.billing.payment && order.billing.payment.selectedPaymentInstruments && order.billing.payment.selectedPaymentInstruments.length > 0) {
        order.billing.payment.selectedPaymentInstruments.forEach(function (selectedPaymentInstrument) {
            if (selectedPaymentInstrument.paymentMethod === window.mercado_pago.creditCard || selectedPaymentInstrument.paymentMethod === window.mercado_pago.paypal) {
                var paymentMethodName = getPaymentMethodName(order, selectedPaymentInstrument);
                htmlToAppend += "<span>" + paymentMethodName
                    + "</span><div>"
                    + selectedPaymentInstrument.type
                    + "</div><div><span>"
                    + selectedPaymentInstrument.maskedCreditCardNumber
                    + "</span></div>";
            } else if (selectedPaymentInstrument.paymentMethod === window.mercado_pago.mercadoPagoCard) {
                var paymentMethods = $(".js-mp-available-payment-methods").data("mpAvailablePaymentMethods");
                var paymentInstrumentType = selectedPaymentInstrument.type;
                var paymentMethod = paymentMethods.find(function (method) { return paymentInstrumentType === method.id; });
                var paymentype = paymentMethod ? paymentMethod.name : paymentInstrumentType;
                var paymentMethodName = getPaymentMethodName(order, selectedPaymentInstrument);
                htmlToAppend += "<span>" + paymentMethodName
                    + "</span><div>"
                    + selectedPaymentInstrument.type
                    + "</div><div><span>"
                    + selectedPaymentInstrument.maskedCreditCardNumber
                    + "</span></div>";
            } else if (selectedPaymentInstrument.paymentMethod === window.mercado_pago.giftCertificate) {
                htmlToAppend += "<span>"
                    + order.resources.giftCertificate
                    + "</span><br/>";
            }else if(selectedPaymentInstrument.paymentMethod === window.mercado_pago.cash){
                var paymentMethodName = getPaymentMethodName(order, selectedPaymentInstrument);
                var storeType = "";
                if(selectedPaymentInstrument.storeType == "oxxo"){
                    storeType = window.mercado_pago.oxxo;
                }else{
                    storeType = window.mercado_pago.other;
                }

                htmlToAppend += "<span>" + paymentMethodName + ": " + storeType
                + "</span><div>"
                + window.mercado_pago.amount + " $" + selectedPaymentInstrument.amount.toFixed(2)
                + "</div><div><span>"
                + window.mercado_pago.time
                + "</span></div>";
            }
        });
    }

    $paymentSummary.empty().append(htmlToAppend);
};

/**
 * @function handlePaymentOptionChange
 * @description Handle payment option change
 */
base.methods.handlePaymentOptionChange = function () {
    $(".payment-options .nav-link").removeClass('active');
    var $activeTab = $(this);
    var activeTabId = $activeTab.attr("href");
    var $paymentInformation = $(".payment-information");
    //var isNewPayment = $(".user-payment-instruments").hasClass("checkout-hidden");

    $(".payment-options [role=tab]").each(function (i, tab) {
        let otherTab = $(tab);
        let otherTabId = otherTab.attr("href");

        $(otherTabId).find("input, select").prop("disabled", otherTabId !== activeTabId);
        $(otherTabId).removeClass('active');
        /*$(activeTabId).addClass('active');*/
    });
    $paymentInformation.data("is-new-payment", true);
};

base.selectBillingAddress = function () {
    $(".payment-form .addressSelector").on("change", function () {
        var form = $(this).parents("form")[0];
        var selectedOption = $("option:selected", this);
        var optionID = selectedOption[0].value;

        if (optionID === "new") {
            // Show Address
            $(form).attr("data-address-mode", "new");
        } else {
            // Hide Address
            $(form).attr("data-address-mode", "shipment");
        }

        // Copy fields
        var attrs = selectedOption.data();
        var element;

        Object.keys(attrs).forEach(function (attr) {
            element = attr === "countryCode" ? "country" : attr;
            if (element === "cardNumber") {
                $(".cardNumber").data("cleave").setRawValue(attrs[attr]);
            } else if (element === "mercadoPagoCardNumber") {
                $(".mercadoPagoCardNumber").data("cleave").setRawValue(attrs[attr]);
            } else {
                $("[name$=" + element + "]", form).val(attrs[attr]);
            }
        });
    });
};

base.handleCreditCardNumber = function () {
    if($(".cardNumber").length > 0){
        cleave.handleCreditCardNumber(".cardNumber", "#cardType");
    }
    if ($(".mercadoPagoCardNumber").length > 0) {
        cleave.handleCreditCardNumber(".mercadoPagoCardNumber", "#cardType");
    }
};

/**
 * @function useSameMailPhoneAsAddress
 * @description fill user information for payment data
 */
base.useSameMailPhoneAsAddress = function () {
    var fillSameFields = function () {
        $(".js-mp-phone").val($("#phoneNumber").val());
        $(".js-mp-email").val($("#email").val());
    };

    $("#useSameMailPhoneAsAddress").change(function () {
        $(".js-mail-phone-container").toggleClass("checkout-hidden", this.checked);


        if (this.checked) {
            fillSameFields();
            $("#phoneNumber").on("change.usesame", fillSameFields);
            $("#email").on("change.usesame", fillSameFields);
        } else {
            $(".js-mp-phone").val("");
            $(".js-mp-email").val("");
            $("#phoneNumber").off("change.usesame");
            $("#email").off("change.usesame");
        }
    });
};

/**
 * @function changePaymentOption
 * @description Change payment option
 */
base.changePaymentOption = function () {
    $(".payment-options [role=tab]").on("click", base.methods.handlePaymentOptionChange); // By click
};

/**
 * @function initPaymentOption
 * @description Initiate payment option
 */
base.initPaymentOption = function () {
    // Initial
    $(".payment-options [role=tab].enabled").trigger("click");
    base.methods.handlePaymentOptionChange.call($(".payment-options [role=tab].active"));
};
base.methods.clearCreditCardForm = function () {
    console.log("clearCreditCardForm------------------->");
    $('input[name$="_cardNumber"]').data('cleave').setRawValue('');
    $('select[name$="_expirationMonth"]').val('');
    $('select[name$="_expirationYear"]').val('');
    $('input[name$="_securityCode"]').val('');
    $('input[name$="cardOwner"]').val('');
    $('input[name$="expirationDate"]').val('');
};

// $('.cardSelector').on('change', function (e) {
//     console.log('cardSelector - change');
//     e.preventDefault();
//     if(e.target.value){
//         console.log('seleccion con datos');
//         $('.payment-information').data('is-new-payment', false);
//         base.methods.clearCreditCardForm();
//         $('.credit-card-form').addClass('checkout-hidden');
//     }else{
//         $('.payment-information').data('is-new-payment', true);
//         base.methods.clearCreditCardForm();
//         $('.credit-card-form').removeClass('checkout-hidden');
//     }
// });

base.methods.updateBillingAddress = function (order) {
   var billing = order.billing;
   if (!billing.billingAddress || !billing.billingAddress.address) return;

   var form = $('form[name=dwfrm_billing]');
   if (!form) return;

   $('input[name$=_firstName]', form).val(billing.billingAddress.address.firstName);
   $('input[name$=_lastName]', form).val(billing.billingAddress.address.lastName);
   $('input[name$=_address1]', form).val(billing.billingAddress.address.address1);
   $('input[name$=_city]', form).val(billing.billingAddress.address.city);
   $('input[name$=_postalCode]', form).val(billing.billingAddress.address.postalCode);
   $('select[name$=_stateCode],input[name$=_stateCode]', form)
       .val(billing.billingAddress.address.stateCode);
   $('select[name$=_country]', form).val(billing.billingAddress.address.countryCode.value);
   $('input[name$=_phone]', form).val(billing.billingAddress.address.phone);
   $('input[name$=_suburb]', form).val(billing.billingAddress.address.suburb);
   $('input[name$=_county]', form).val(billing.billingAddress.address.county);
   $('input[name$=_numberExt]', form).val(billing.billingAddress.address.numberExt);
   $('input[name$=_numberInt]', form).val(billing.billingAddress.address.numberInt);
   $('input[name$=_email]', form).val(order.orderEmail);
}

module.exports = base;



/***/ }),

/***/ "./cartridges/int_mercadopago/cartridge/client/default/js/checkout/checkout.js":
/*!*************************************************************************************!*\
  !*** ./cartridges/int_mercadopago/cartridge/client/default/js/checkout/checkout.js ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var addressHelpers = __webpack_require__(/*! ./address */ "./cartridges/int_mercadopago/cartridge/client/default/js/checkout/address.js");
var shippingHelpers = __webpack_require__(/*! ./shipping */ "./cartridges/int_mercadopago/cartridge/client/default/js/checkout/shipping.js");
var billingHelpers = __webpack_require__(/*! ./billing */ "./cartridges/int_mercadopago/cartridge/client/default/js/checkout/billing.js");
var summaryHelpers = __webpack_require__(/*! base/checkout/summary */ "./cartridges/app_storefront_base/cartridge/client/default/js/checkout/summary.js");
var formHelpers = __webpack_require__(/*! base/checkout/formErrors */ "./cartridges/app_storefront_base/cartridge/client/default/js/checkout/formErrors.js");
var scrollAnimate = __webpack_require__(/*! base/components/scrollAnimate */ "./cartridges/app_storefront_base/cartridge/client/default/js/components/scrollAnimate.js");


/**
 * Create the jQuery Checkout Plugin.
 *
 * This jQuery plugin will be registered on the dom element in checkout.isml with the
 * id of "checkout-main".
 *
 * The checkout plugin will handle the different state the user interface is in as the user
 * progresses through the varying forms such as shipping and payment.
 *
 * Billing info and payment info are used a bit synonymously in this code.
 *
 */
(function ($) {
    $.fn.checkout = function () { // eslint-disable-line
        var plugin = this;

        //
        // Collect form data from user input
        //
        var formData = {
            // Shipping Address
            shipping: {},

            // Billing Address
            billing: {},

            // Payment
            payment: {},

            // Gift Codes
            giftCode: {}
        };

        //
        // The different states/stages of checkout
        //
        var checkoutStages = [
            'shipping',
            'payment',
            'placeOrder',
            'submitted'
        ];

        /**
         * Updates the URL to determine stage
         * @param {number} currentStage - The current stage the user is currently on in the checkout
         */
        function updateUrl(currentStage) {
            history.pushState(
                checkoutStages[currentStage],
                document.title,
                location.pathname
                + '?stage='
                + checkoutStages[currentStage]
                + '#'
                + checkoutStages[currentStage]
            );
        }

        //
        // Local member methods of the Checkout plugin
        //
        var members = {

            // initialize the currentStage variable for the first time
            currentStage: 0,

            /**
             * Set or update the checkout stage (AKA the shipping, billing, payment, etc... steps)
             * @returns {Object} a promise
             */
            updateStage: function () {
                var stage = checkoutStages[members.currentStage];
                var defer = $.Deferred(); // eslint-disable-line

                if (stage === 'shipping') {
                    console.log("extend 1--------->");
                    //
                    // Clear Previous Errors
                    //
                    formHelpers.clearPreviousErrors('.shipping-form');

                    //
                    // Submit the Shipping Address Form
                    //
                    var isMultiShip = $('#checkout-main').hasClass('multi-ship');
                    var formSelector = isMultiShip ?
                        '.multi-shipping .active form' : '.single-shipping .shipping-form';
                    var form = $(formSelector);

                    if (isMultiShip && form.length === 0) {
                        // disable the next:Payment button here
                        $('body').trigger('checkout:disableButton', '.next-step-button button');
                        // in case the multi ship form is already submitted
                        var url = $('#checkout-main').attr('data-checkout-get-url');
                        $.ajax({
                            url: url,
                            method: 'GET',
                            success: function (data) {
                                // enable the next:Payment button here
                                $('body').trigger('checkout:enableButton', '.next-step-button button');
                                if (!data.error) {
                                    $('body').trigger('checkout:updateCheckoutView',
                                        { order: data.order, customer: data.customer });
                                    defer.resolve();
                                } else if (data.message && $('.shipping-error .alert-danger').length < 1) {
                                    var errorMsg = data.message;
                                    var errorHtml = '<div class="alert alert-danger alert-dismissible valid-cart-error ' +
                                        'fade show" role="alert">' +
                                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                        '<span aria-hidden="true">&times;</span>' +
                                        '</button>' + errorMsg + '</div>';
                                    $('.shipping-error').append(errorHtml);
                                    scrollAnimate($('.shipping-error'));
                                    defer.reject();
                                } else if (data.redirectUrl) {
                                    window.location.href = data.redirectUrl;
                                }
                            },
                            error: function () {
                                // enable the next:Payment button here
                                $('body').trigger('checkout:enableButton', '.next-step-button button');
                                // Server error submitting form
                                defer.reject();
                            }
                        });
                    } else {
                        console.log("extend 2--------->");

                        var shippingFormData = form.serialize();

                        $('body').trigger('checkout:serializeShipping', {
                            form: form,
                            data: shippingFormData,
                            callback: function (data) {
                                shippingFormData = data;
                            }
                        });

                        // disable the next:Payment button here
                        $('body').trigger('checkout:disableButton', '.next-step-button button');
                        $.ajax({
                            url: form.attr('action'),
                            type: 'post',
                            data: shippingFormData,
                            success: function (data) {
                                 // enable the next:Payment button here
                                $('body').trigger('checkout:enableButton', '.next-step-button button');
                                shippingHelpers.methods.shippingFormResponse(defer, data);
                            },
                            error: function (err) {
                                // enable the next:Payment button here
                                $('body').trigger('checkout:enableButton', '.next-step-button button');
                                if (err.responseJSON && err.responseJSON.redirectUrl) {
                                    window.location.href = err.responseJSON.redirectUrl;
                                }
                                // Server error submitting form
                                defer.reject(err.responseJSON);
                            }
                        });
                    }
                    return defer;
                } else if (stage === 'payment') {
                    console.log("extend 3--------->");

                    $("#placeOrderConfirmation").removeClass("d-none");
                    //
                    // Submit the Billing Address Form
                    //
                    formHelpers.clearPreviousErrors('.payment-form');

                    var billingAddressForm = $('#dwfrm_billing .billing-address-block :input').serialize();

                    $('body').trigger('checkout:serializeBilling', {
                        form: $('#dwfrm_billing .billing-address-block'),
                        data: billingAddressForm,
                        callback: function (data) {
                            if (data) {
                                billingAddressForm = data;
                            }
                        }
                    });

                    var contactInfoForm = $('#dwfrm_billing .contact-info-block :input').serialize();

                    $('body').trigger('checkout:serializeBilling', {
                        form: $('#dwfrm_billing .contact-info-block'),
                        data: contactInfoForm,
                        callback: function (data) {
                            if (data) {
                                contactInfoForm = data;
                            }
                        }
                    });

                    var activeTabId = $('.tab-pane.active').attr('id');
                    var paymentInfoSelector = '#dwfrm_billing .' + activeTabId + ' .payment-form-fields :input';
                    var paymentInfoForm = $(paymentInfoSelector).serialize();

                    $('body').trigger('checkout:serializeBilling', {
                        form: $(paymentInfoSelector),
                        data: paymentInfoForm,
                        callback: function (data) {
                            if (data) {
                                paymentInfoForm = data;
                            }
                        }
                    });

                    var paymentForm = billingAddressForm + '&' + contactInfoForm + '&' + paymentInfoForm;

                    if ($('.data-checkout-stage').data('customer-type') === 'registered') {
                        // if payment method is credit card
                        if ($('.payment-information').data('payment-method-id') === 'CREDIT_CARD') {
                            if (!($('.payment-information').data('is-new-payment'))) {
                                 var cvvCode = $('.saved-payment-security-code').val();

                                if (cvvCode === '') {
                                    var cvvElement = $('.saved-payment-security-code');
                                    cvvElement.addClass('is-invalid');
                                    scrollAnimate(cvvElement);
                                    defer.reject();
                                    return defer;
                                }

                                var $savedPaymentInstrument = $('.cardSelectorPaypal').find("option:selected");

                                paymentForm += '&storedPaymentUUID=' +
                                $savedPaymentInstrument.val();

                                paymentForm += '&securityCode=' + cvvCode;
                            }
                        }
                    }
                     // disable the next:Place Order button here
                    $('body').trigger('checkout:disableButton', '.next-step-button button');

                    $.ajax({
                        url: $('#dwfrm_billing').attr('action'),
                        method: 'POST',
                        data: paymentForm,
                        success: function (data) {
                             // enable the next:Place Order button here
                            $('body').trigger('checkout:enableButton', '.next-step-button button');
                            // look for field validation errors
                            if (data.error) {
                                if (data.fieldErrors.length) {
                                    data.fieldErrors.forEach(function (error) {
                                        if (Object.keys(error).length) {
                                            formHelpers.loadFormErrors('.payment-form', error);
                                        }
                                    });
                                }

                                if (data.serverErrors.length) {
                                    data.serverErrors.forEach(function (error) {
                                        $('.error-message').show();
                                        $('.error-message-text').text(error);
                                        scrollAnimate($('.error-message'));
                                    });
                                }

                                if (data.cartError) {
                                    window.location.href = data.redirectUrl;
                                }

                                defer.reject();
                            } else {
                                //
                                // Populate the Address Summary
                                //
                                $('body').trigger('checkout:updateCheckoutView',
                                    { order: data.order, customer: data.customer });

                                if (data.renderedPaymentInstruments) {
                                    $('.stored-payments').empty().html(
                                        data.renderedPaymentInstruments
                                    );
                                }

                                if (data.customer.registeredUser
                                    && data.customer.customerPaymentInstruments.length
                                ) {
                                    $('.cancel-new-payment').removeClass('checkout-hidden');
                                }

                                scrollAnimate();
                                document.querySelector('.step-item.active').classList.remove('active');
                                document.querySelector('.step-3').classList.add('active');
                                defer.resolve(data);
                            }
                        },
                        error: function (err) {
                            // enable the next:Place Order button here
                            $('body').trigger('checkout:enableButton', '.next-step-button button');
                            if (err.responseJSON && err.responseJSON.redirectUrl) {
                                window.location.href = err.responseJSON.redirectUrl;
                            }
                        }
                    });

                    return defer;
                } else if (stage === 'placeOrder') {
                    //* ialvarez@ts4.mx verify  if checkbox is checked
                    var isChecked = $("#checkTermAndConditions").is(":checked");
                    $("#checkTermAndConditions").removeClass("is-invalid");
                    if (!isChecked) {
                        defer.reject({error: true, message: 'Debe aceptar terminos y condiciones'});
                        $("#checkTermAndConditions").addClass("is-invalid");
                    } else {
                        $.spinner().start();
                        // disable the placeOrder button here
                        $('body').trigger('checkout:disableButton', '.next-step-button button');
                        $.ajax({
                            url: $('.place-order').data('action'),
                            method: 'POST',
                            success: function (data) {
                                $.spinner().stop();
                                // enable the placeOrder button here
                                $('body').trigger('checkout:enableButton', '.next-step-button button');
                                if (data.error) {
                                    if (data.cartError) {
                                        window.location.href = data.redirectUrl;
                                        defer.reject();
                                    } else {
                                        // go to appropriate stage and display error message
                                        defer.reject(data);
                                    }
                                } else {
                                    var continueUrl = data.continueUrl;
                                    var urlParams = {
                                        ID: data.orderID,
                                        token: data.orderToken
                                    };

                                    continueUrl += (continueUrl.indexOf('?') !== -1 ? '&' : '?') +
                                        Object.keys(urlParams).map(function (key) {
                                            return key + '=' + encodeURIComponent(urlParams[key]);
                                        }).join('&');

                                    window.location.href = continueUrl;
                                    defer.resolve(data);
                                }
                            },
                            error: function () {
                                // enable the placeOrder button here
                                $('body').trigger('checkout:enableButton', $('.next-step-button button'));
                            }
                        });
                    }

                    return defer;
                }
                var p = $('<div>').promise(); // eslint-disable-line
                setTimeout(function () {
                    p.done(); // eslint-disable-line
                }, 500);
                return p; // eslint-disable-line
            },

            /**
             * Initialize the checkout stage.
             *
             * TODO: update this to allow stage to be set from server?
             */
            initialize: function () {
                // set the initial state of checkout
                members.currentStage = checkoutStages
                    .indexOf($('.data-checkout-stage').data('checkout-stage'));
                $(plugin).attr('data-checkout-stage', checkoutStages[members.currentStage]);

                //
                // Handle Payment option selection
                //
                $('input[name$="paymentMethod"]', plugin).on('change', function () {
                    $('.credit-card-form').toggle($(this).val() === 'CREDIT_CARD');
                });

                //
                // Handle Next State button click
                //
                $(plugin).on('click', '.next-step-button button', function () {
                    members.nextStage();
                });

                //
                // Handle Edit buttons on shipping and payment summary cards
                //
                $('.shipping-summary .edit-button', plugin).on('click', function () {
                    if (!$('#checkout-main').hasClass('multi-ship')) {
                        $('body').trigger('shipping:selectSingleShipping');
                    }

                    members.gotoStage('shipping');
                });

                $('.payment-summary .edit-button', plugin).on('click', function () {
                    members.gotoStage('payment');
                });

                //
                // remember stage (e.g. shipping)
                //
                updateUrl(members.currentStage);

                //
                // Listen for foward/back button press and move to correct checkout-stage
                //
                $(window).on('popstate', function (e) {
                    //
                    // Back button when event state less than current state in ordered
                    // checkoutStages array.
                    //
                    if (e.state === null ||
                        checkoutStages.indexOf(e.state) < members.currentStage) {
                        members.handlePrevStage(false);
                    } else if (checkoutStages.indexOf(e.state) > members.currentStage) {
                        // Forward button  pressed
                        members.handleNextStage(false);
                    }
                });

                //
                // Set the form data
                //
                plugin.data('formData', formData);
            },

            /**
             * The next checkout state step updates the css for showing correct buttons etc...
             */
            nextStage: function () {
                var promise = members.updateStage();

                promise.done(function () {
                    // Update UI with new stage
                    members.handleNextStage(true);
                });

                promise.fail(function (data) {
                    // show errors
                    if (data) {
                        if (data.errorStage) {
                            members.gotoStage(data.errorStage.stage);

                            if (data.errorStage.step === 'billingAddress') {
                                var $billingAddressSameAsShipping = $(
                                    'input[name$="_shippingAddressUseAsBillingAddress"]'
                                );
                                if ($billingAddressSameAsShipping.is(':checked')) {
                                    $billingAddressSameAsShipping.prop('checked', false);
                                }
                            }
                        }

                        if (data.errorMessage) {
                            $('.error-message').show();
                            $('.error-message-text').text(data.errorMessage);
                        }
                    }
                });
            },

            /**
             * The next checkout state step updates the css for showing correct buttons etc...
             *
             * @param {boolean} bPushState - boolean when true pushes state using the history api.
             */
            handleNextStage: function (bPushState) {
                if (members.currentStage < checkoutStages.length - 1) {
                    // move stage forward
                    members.currentStage++;

                    //
                    // show new stage in url (e.g.payment)
                    //
                    if (bPushState) {
                        updateUrl(members.currentStage);
                    }
                }

                // Set the next stage on the DOM
                $(plugin).attr('data-checkout-stage', checkoutStages[members.currentStage]);
            },

            /**
             * Previous State
             */
            handlePrevStage: function () {
                if (members.currentStage > 0) {
                    // move state back
                    members.currentStage--;
                    updateUrl(members.currentStage);
                }

                $(plugin).attr('data-checkout-stage', checkoutStages[members.currentStage]);
            },

            /**
             * Use window history to go to a checkout stage
             * @param {string} stageName - the checkout state to goto
             */
            gotoStage: function (stageName) {
                members.currentStage = checkoutStages.indexOf(stageName);
                updateUrl(members.currentStage);
                $(plugin).attr('data-checkout-stage', checkoutStages[members.currentStage]);
            }
        };

        //
        // Initialize the checkout
        //
        members.initialize();

        return this;
    };
}(jQuery));


var exports = {
    initialize: function () {
        $('#checkout-main').checkout();
    },

    updateCheckoutView: function () {
        $('body').on('checkout:updateCheckoutView', function (e, data) {
            shippingHelpers.methods.updateMultiShipInformation(data.order);
            summaryHelpers.updateTotals(data.order.totals);
            data.order.shipping.forEach(function (shipping) {
                shippingHelpers.methods.updateShippingInformation(
                    shipping,
                    data.order,
                    data.customer,
                    data.options,
                    data.suburbsArray
                );
            });
            billingHelpers.methods.updateBillingInformation(
                data.order,
                data.customer,
                data.options
            );
            billingHelpers.methods.updatePaymentInformation(data.order, data.options);
            summaryHelpers.updateOrderProductSummaryInformation(data.order, data.options);
        });
    },

    disableButton: function () {
        $('body').on('checkout:disableButton', function (e, button) {
            $(button).prop('disabled', true);
        });
    },

    enableButton: function () {
        $('body').on('checkout:enableButton', function (e, button) {
            $(button).prop('disabled', false);
        });
    }
};

[billingHelpers, shippingHelpers, addressHelpers].forEach(function (library) {
    Object.keys(library).forEach(function (item) {
        if (typeof library[item] === 'object') {
            exports[item] = $.extend({}, exports[item], library[item]);
        } else {
            exports[item] = library[item];
        }
    });
});

module.exports = exports;


/***/ }),

/***/ "./cartridges/int_mercadopago/cartridge/client/default/js/checkout/mercadoPago.js":
/*!****************************************************************************************!*\
  !*** ./cartridges/int_mercadopago/cartridge/client/default/js/checkout/mercadoPago.js ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/* global jQuery Mercadopago */

(function ($) {
    /**
     * @constructor
     * @classdesc Integration class
     */
    function MercadoPago() {
        var that = this;

        var $content = $(".js-mercadopago-content");
        var $form = $content.find(".js-mp-form");

        var $elements = {
            paymentOptionTab: $(".js-payment-option-tab"),
            paymentTypeButton: $content.find(".js-toggle-payment-type"),
            customerCardsContainer: $content.find(".js-mp-customer-cards"),
            customerCard: $content.find(".js-mp-customer-card")
        };

        // Regular fields
        var fields = {
            cardType: {
                $el: $form.find(".js-mp-card-type"),
                disable: { other: false, stored: false },
                hide: { other: false, stored: true },
                errors: []
            },
            cardHolder: {
                $el: $form.find(".js-mp-card-holder"),
                disable: { other: true, stored: true },
                hide: { other: true, stored: true },
                errors: ["221", "316"]
            },
            cardNumber: {
                $el: $form.find(".js-mp-card-number"),
                disable: { other: true, stored: true },
                hide: { other: true, stored: true },
                errors: ["205", "E301","ECYA301"]
            },
            expirationDate: {
                 $el: $form.find(".js-mp-expiration-date"),
                 disable: { other: true, stored: true },
                 hide: { other: true, stored: true },
                 errors: ["ECYA225","ECYA327", "ECYA427"]
            },
            cardMonth: {
                $el: $form.find(".js-mp-card-month"),
                disable: { other: true, stored: true },
                hide: { other: true, stored: true },
                errors: ["208", "325"]
            },
            cardYear: {
                $el: $form.find(".js-mp-card-year"),
                disable: { other: true, stored: true },
                hide: { other: true, stored: true },
                errors: ["209", "326"]
            },
            securityCode: {
                $el: $form.find(".js-mp-security-code"),
                disable: { other: true, stored: false },
                hide: { other: true, stored: false },
                errors: ["224", "E302", "E203"]
            },
            email: {
                $el: $form.find(".js-mp-email"),
                disable: { other: false, stored: true },
                hide: { other: false, stored: true },
                errors: ["email"]
            },
            phone: {
                $el: $form.find(".js-mp-phone"),
                disable: { other: false, stored: true },
                hide: { other: false, stored: true },
                errors: ["phone"]
            },
            saveCard: {
                $el: $form.find(".js-mp-save-card"),
                disable: { other: true, stored: true },
                hide: { other: true, stored: true },
                errors: []
            },
            useSameMailPhoneAsAddress: {
                $el: $form.find(".js-mp-use-same"),
                disable: { other: false, stored: true },
                hide: { other: false, stored: true },
                errors: []
            }
        };

        // Extended fields
        fields.issuer = {
            $el: $form.find(".js-mp-issuer"),
            disable: { other: true, stored: true },
            hide: { other: true, stored: false },
            errors: ["issuer"]
        };
        fields.financialinstitution = {
            $el: $form.find(".js-mp-financialinstitution"),
            disable: { other: true, stored: true },
            hide: { other: true, stored: true },
            cardTypeID: "pse",
            errors: ["mercadoPagoFinancialinstitution"]
        };
        fields.installments = {
            $el: $form.find(".js-mp-installments"),
            disable: { other: true, stored: false },
            hide: { other: true, stored: false },
            errors: ["installments"]
        };
        fields.docType = {
            $el: $form.find(".js-mp-doc-type"),
            disable: { other: false, stored: false },
            hide: { other: false, stored: false },
            errors: ["212", "322"]
        };
        fields.docNumber = {
            $el: $form.find(".js-mp-doc-number"),
            $wrapper: $form.find(".js-mp-doc-wrapper"),
            $label: $form.find(".js-mp-doc-label"),
            $tooltip: $form.find(".js-mp-doc-tooltip"),
            disable: { other: false, stored: false },
            hide: { other: false, stored: false },
            errors: ["214", "324", "2067"]
        };

        // Hidden fields
        Object.defineProperty(fields, "cardId", {
            enumerable: false,
            value: {
                $el: $form.find(".js-mp-card-id")
            }
        });
        Object.defineProperty(fields, "token", {
            enumerable: false,
            value: {
                $el: $form.find(".js-mp-token")
            }
        });
        Object.defineProperty(fields, "customerId", {
            enumerable: false,
            value: {
                $el: $form.find(".js-mp-customer-id")
            }
        });

        var methods = {
            paymentOption: {
                /**
                 * @function handleChange
                 * @description Handle change of payment method and set initial state of payment tab
                 */
                handleChange: function () {
                    var $activeTab = $(this);
                    var methodId = $activeTab
                        .closest(".js-method-id")
                        .data("methodId");
                    var initialState = $form.data("mpInitial");

                    if (methodId === that.configuration.paymentMethodId) {
                        /*methods.paymentOption.setInitialState[
                            initialState + "Payment"
                        ]();*/
                    }
                },
                setInitialState: {
                    /**
                     * @function newPayment
                     * @description Set initial state for new payment section
                     */
                    newPayment: function () {
                        var paymentMethodInput = fields.cardType.$el.filter(
                            function () {
                                return (
                                    this.value ===
                                    that.configuration.defaultCardType
                                );
                            }
                        );

                        // Check default card type
                        paymentMethodInput.prop("checked", true);
                        methods.card.handleTypeChange.call(
                            paymentMethodInput[0],
                            { data: { handleOther: true } }
                        );
                    },
                    /**
                     * @function storedPayment
                     * @description Set initial state for stored payment section
                     */
                    storedPayment: function () {
                        var firstCard = $elements.customerCard.filter(":first");

                        // Select first card
                        methods.registeredCustomer.selectCustomerCard.call(firstCard[0]);

                        // Toggle payment type to stored
                        $elements.paymentTypeButton.data("togglePaymentType", "stored");
                        methods.registeredCustomer.togglePaymentType.call($elements.paymentTypeButton[0]);
                    },
                    /**
                     * @function restoreStoredPayment
                     * @description Restore stored payment section
                     */
                    restoreStoredPayment: function () {
                        var firstCard = $elements.customerCard.filter(":first");

                        // Select first card
                        methods.registeredCustomer.selectCustomerCard.call(firstCard[0]);

                        // Show and set disabled to false for stored payment fields
                        /* eslint-disable no-restricted-syntax */
                        for (var field in fields) {
                            if (!fields[field].hide.stored) {
                                fields[field].$el
                                    .closest(".js-mp-container")
                                    .removeClass("checkout-hidden");
                            }
                            if (!fields[field].disable.stored) {
                                fields[field].$el.prop("disabled", false);
                            }
                        }
                        /* eslint-enable no-restricted-syntax */
                    }
                }
            },

            token: {
                validateDocNumber: function ($docNumber) {
                    if (
                        $docNumber.attr("required") &&
                        $docNumber.is(":visible") &&
                        $docNumber.is(":enabled")
                    ) {
                        var fieldValueLength = $docNumber.val().length;
                        if (+fieldValueLength === 0) {
                            return "214";
                        } else if (
                            fieldValueLength > $docNumber.attr("maxlength") ||
                            fieldValueLength < $docNumber.attr("minlength")
                        ) {
                            return "324";
                        }
                    }
                    return null;
                },
                validateSecurityCode: function ($securityCode) {
                    if (
                        $securityCode.attr("required") &&
                        $securityCode.is(":visible") &&
                        $securityCode.is(":enabled")
                    ) {
                        var fieldValueLength = $securityCode.val().length;
                        if( fieldValueLength === 0){
                            return  "224"
                        }else  if (fieldValueLength < 3 || fieldValueLength > 4) {
                            return "E302";
                        }
                    }
                    return null;
                },
                validateCardNumber: function ($cardNumber) {
                    if (
                        $cardNumber.attr("required") &&
                        $cardNumber.is(":visible") &&
                        $cardNumber.is(":enabled")
                    ) {
                        if($cardNumber.val().length === 0){
                            return "205"
                        }
                    }
                    return null;
                },
                validateExpirationDate: function ($expirationDate) {
                    if (
                        $expirationDate.attr("required") &&
                        $expirationDate.is(":visible") &&
                        $expirationDate.is(":enabled")
                    ) {
                        if($expirationDate.val().length === 0){
                            return "ECYA225";
                        }else if(!$expirationDate[0].checkValidity()){
                            return "ECYA427";
                        }
                    }
                    return null;
                },
                validateCardHolderName: function ($cardHolderName) {
                    if (
                        $cardHolderName.attr("required") &&
                        $cardHolderName.is(":visible") &&
                        $cardHolderName.is(":enabled")
                    ) {
                        if($cardHolderName.val().length === 0){
                            return "221"
                        }
                    }
                    return null;
                },
                /**
                 * @function populate
                 * @description Create token and populate field with value during submit
                 * @param {Event} event event
                 * @param {Object} eventData event data
                 */
                populate: function (event, eventData) {
                    var validForm = true;
                    // Continue default flow
                    if (eventData && eventData.continue) {
                        return;
                    }
                    // Stop default flow
                    event.stopImmediatePropagation();

                    var isOtherPaymentMethod = fields.cardType.$el.filter(":checked").data("mpCardType") === that.configuration.otherPaymentMethod;
                    var isMercadoPago = $("input[name$=\"billing_paymentMethod\"]:enabled").val() === that.configuration.paymentMethodId;

                    /* eslint-disable no-else-return */
                    if (isMercadoPago) {
                        var $docNumber = fields.docNumber.$el;
                        var docNumberErrorCode = methods.token.validateDocNumber(
                            $docNumber
                        );
                        if (docNumberErrorCode) {
                            methods.token.errorResponse(docNumberErrorCode);
                            validForm = false;
                        } else {
                            $docNumber.next(".invalid-feedback").hide();
                        }
                        var $securityCode = fields.securityCode.$el;
                        var securityCodeErrorCode = methods.token.validateSecurityCode(
                            $securityCode
                        );
                        if (securityCodeErrorCode) {
                            methods.token.errorResponse(securityCodeErrorCode);
                            validForm = false;
                        } else {
                            $securityCode.next(".invalid-feedback").hide();
                        }
                        var $cardNumber = fields.cardNumber.$el;
                        var cardNumberCodeErrorCode = methods.token.validateCardNumber(
                            $cardNumber
                        );
                        if (cardNumberCodeErrorCode) {
                            methods.token.errorResponse(cardNumberCodeErrorCode);
                            validForm = false;
                        } else {
                            $cardNumber.next(".invalid-feedback").hide();
                        }
                        var $expirationDate = fields.expirationDate.$el;
                        var expirationDateErrorCode = methods.token.validateExpirationDate(
                            $expirationDate
                        );
                        if (expirationDateErrorCode) {
                            methods.token.errorResponse(expirationDateErrorCode);
                            validForm = false;
                        } else {
                            $expirationDate.next(".invalid-feedback").hide();
                            if($expirationDate.hasClass('is-invalid')){
                                $expirationDate.removeClass('is-invalid')
                            }
                        }
                        var $cardHolder = fields.cardHolder.$el;
                        var cardHolderErrorCode = methods.token.validateCardHolderName(
                            $cardHolder
                        );
                        if (cardHolderErrorCode) {
                            methods.token.errorResponse(cardHolderErrorCode);
                            validForm = false;
                        } else {
                            $cardHolder.next(".invalid-feedback").hide();
                        }

                    }

                    if (isMercadoPago) {
                        fields.cardMonth.$el.val('');
                        fields.cardYear.$el.val('');
                        var currentYearFirstTwoDigits = new Date().getFullYear().toString().substr(0,2);
                        var arrayDate = fields.expirationDate.$el.val().split('/');;
                        if(arrayDate.length == 2 && arrayDate[0] !== '' && arrayDate[1] !== '') {
                            fields.cardMonth.$el.val(parseInt(arrayDate[0]));
                            fields.cardYear.$el.val(currentYearFirstTwoDigits + arrayDate[1]);
                        }
                    }

                    /* eslint-enable no-else-return */
                    if (isOtherPaymentMethod || !isMercadoPago) {
                        var submitPayment = $(".next-step-button .submit-payment");
                        fields.token.$el.val("");
                        submitPayment.trigger("click", { continue: true });
                        return;
                    }
                    if(!validForm){return;}
                    Mercadopago.createToken($form, function (status, serviceResponse) {
                        Object.keys(fields).forEach(function (index) {
                            var field = fields[index];
                            if (
                                field.$el.attr("required") &&
                                field.$el.is(":visible") &&
                                field.$el.is(":enabled")
                            ) {
                                /* eslint-disable  eqeqeq */
                                if (field.$el.val().length > 0) {
                                    if (index != "securityCode") {
                                        field.$el.next(".invalid-feedback").hide();
                                    }
                                } else if (field.errors && field.errors.indexOf(index) != -1) {
                                    if (index != "securityCode") {
                                        methods.token.errorResponse(index);
                                    }
                                    validForm = false;
                                }
                                /* eslint-enable  eqeqeq */
                            }
                        });
                        if ((status === 200 || status === 201) && validForm) {
                            methods.token.successResponse(serviceResponse);
                        } else {
                            console.log("not success response");
                            if (
                                fields.cardHolder.$el.attr("required") &&
                                fields.cardHolder.$el.is(":visible") &&
                                fields.cardHolder.$el.is(":enabled") &&
                                fields.cardHolder.$el.val().length === 0
                            ) {
                                methods.token.errorResponse(
                                    fields.cardHolder.errors[0]
                                );
                                validForm = false;
                            }

                            if (
                                fields.securityCode.$el.attr("required") &&
                                fields.securityCode.$el.is(":visible") &&
                                fields.securityCode.$el.is(":enabled") &&
                                fields.securityCode.$el.val().length === 0
                            ) {
                                methods.token.errorResponse(
                                    fields.securityCode.errors[0]
                                );
                                validForm = false;
                            } else if (
                                fields.securityCode.$el.attr("required") &&
                                fields.securityCode.$el.is(":visible") &&
                                fields.securityCode.$el.is(":enabled") &&
                                fields.securityCode.$el.val().length > 0
                            ) {
                                /* eslint-disable no-shadow */
                                var $securityCode = fields.securityCode.$el;
                                var securityCodeErrorCode = methods.token.validateSecurityCode(
                                    $securityCode
                                );
                                /* *eslint-enable no-shadow */
                                if(securityCodeErrorCode){
                                    methods.token.errorResponse(
                                        securityCodeErrorCode
                                    );
                                    validForm = false;
                                }
                            }

                            if (
                                fields.installments.$el.attr("required") &&
                                fields.installments.$el.is(":visible") &&
                                fields.installments.$el.is(":enabled") &&
                                fields.installments.$el.val().length === 0
                            ) {
                                methods.token.errorResponse(
                                    fields.installments.errors[0]
                                );
                                validForm = false;
                            }

                            /* eslint-disable no-shadow, block-scoped-var */
                            var docNumberErrorCode = methods.token.validateDocNumber($docNumber);
                            /* eslint-disable no-shadow, block-scoped-var */
                            if (docNumberErrorCode) {
                                methods.token.errorResponse(docNumberErrorCode);
                            }

                            if (serviceResponse.cause) {
                                serviceResponse.cause.forEach(function (cause) {
                                    methods.token.errorResponse(cause.code);
                                });
                            }
                        }
                    });
                },
                /**
                 * @function successResponse
                 * @description Success callback for token creation
                 * @param {Object} serviceResponse service response
                 */
                successResponse: function (serviceResponse) {
                    var submitPayment = $(".next-step-button .submit-payment");
                    /* eslint-disable  array-callback-return */
                    Object.keys(fields).map(function (fieldKey) {
                        var field = fields[fieldKey];
                        field.$el.next(".invalid-feedback").hide();
                    });
                    /* eslint-enable  array-callback-return */

                    fields.token.$el.val(serviceResponse.id);
                    submitPayment.trigger("click", { continue: true });
                },
                /**
                 * @function errorResponse
                 * @description Error callback for token creation
                 * @param {string} errorCode error code
                 */
                errorResponse: function (errorCode) {
                    if(errorCode === '325' || errorCode === '326'){errorCode = 'ECYA327';}
                    var errorMessages = $form.data("mpErrorMessages");
                    var errorField;

                    // Set error code message if found, otherwise set default error message
                    var errorMessage = errorMessages[errorCode]
                        ? errorMessages[errorCode]
                        : errorMessages.default;
                    /* eslint-disable consistent-return */
                    Object.keys(fields).forEach(function (index) {
                        var field = fields[index];
                        if (
                            field.errors &&
                            field.errors.indexOf(errorCode) !== -1
                        ) {
                            errorField = field;
                            return true;
                        }
                    });
                    /* eslint-enable consistent-return */
                    if (errorField) {
                        errorField.$el
                            .next(".invalid-feedback")
                            .focus()
                            .show()
                            .text(errorMessage);
                    } else {
                        $(".error-message").show();
                        $(".error-message-text").text(errorMessage);
                    }
                }
            },

            card: {
                /**
                 * @function handleTypeChange
                 * @description Handle credit card type change
                 * @param {Event} e event
                 */
                handleTypeChange: function (e) {
                    var $el = $(this);
                    var issuerMandatory = $el.data("mpIssuerRequired");
                    var isOtherType =
                        $el.data("mpCardType") ===
                        that.configuration.otherPaymentMethod;
                    var paymentTypeID = $el.val(); // master, visa, pse, boleto
                    var mpPaymentTypeId = $el.data("mpPaymentTypeId"); // credit_card, bank_transfer, ticket

                    // Handle fields for other payment method
                    if (e.data.handleOther) {
                        methods.card.handleOtherType(
                            isOtherType,
                            paymentTypeID
                        );
                    }

                    if (that.preferences.enableInstallments === true && !isOtherType) {
                        methods.installment.set($el.val());

                        // Set issuer info
                        if (issuerMandatory) {
                            methods.issuer.set($el);
                            fields.issuer.$el.prop("disabled", false);
                            fields.issuer.$el
                                .off("change")
                                .on("change", methods.installment.setByIssuerId);
                        } else {
                            fields.issuer.$el.prop("disabled", true);
                        }
                    }
                    $("#mpPaymentTypeId").val(mpPaymentTypeId);
                },
                /**
                 * @function handleOtherType
                 * @description Toggle other payment method
                 * @param {boolean} isOtherType is other type
                 * @param {string} cardTypeID cart type ID
                 */
                handleOtherType: function (isOtherType, cardTypeID) {
                    /* eslint-disable eqeqeq, no-restricted-syntax, guard-for-in */
                    for (var field in fields) {
                        var fieldsField = fields[field];
                        if (fieldsField.hide.other) {
                            fieldsField.$el
                                .closest(".js-mp-container")
                                .toggleClass("checkout-hidden", isOtherType);
                        }
                        if (fieldsField.disable.other) {
                            fieldsField.$el.prop("disabled", isOtherType);
                        }

                        if (fieldsField.cardTypeID) {
                            fieldsField.$el.prop(
                                "disabled",
                                fieldsField.cardTypeID != cardTypeID
                            );
                            fieldsField.$el
                                .closest(".js-mp-container")
                                .toggleClass(
                                    "checkout-hidden",
                                    fieldsField.cardTypeID != cardTypeID
                                );
                        }
                    }
                    /* eslint-enable eqeqeq, no-restricted-syntax, guard-for-in */
                },

                guessingPaymentMethod: function () {
                    console.log("guessingPaymentMethod--->")
                    const cardnumber = document.getElementById(
                        "mercadoPagoCardNumber"
                    ).value;
                    if(!cardnumber.length > 0){return;}
                    var bin = cardnumber.replace(" ", "").substring(0, 6);

                    fields.cardNumber.$el.next(".invalid-feedback").focus().hide();
                    window.Mercadopago.getPaymentMethod(
                        {
                            bin: bin
                        },
                        methods.card.setPaymentMethodInfo
                    );
                },

                setPaymentMethodInfo: function (status, response) {
                    if (+status === 200 && response.length > 0) {
                        var $paymentMethodElement = $(
                            "#cardType-" + response[0].id
                        );
                        console.log($paymentMethodElement);
                        $paymentMethodElement.prop("checked", true);
                        methods.card.handleTypeChange.call(
                            $paymentMethodElement,
                            { data: { handleOther: false } }
                        );
                    }
                },
                /**
                * @function isBradesCard
                 * @description Check if is bradescard bines
                 */
                isBradesCard: function isBradesCard(bin) {
                  var valid = false;
                  var bradescardBines = JSON.parse(that.preferences.bradescardBines);
                  if (bradescardBines.hasOwnProperty("binesRange")) {
                    var binesRange = bradescardBines.binesRange;
                    for (var i = 0; binesRange.length > i; i++) {
                      var bines = binesRange[i].bines;
                      var binFrom = parseInt(bines.substring(0, bines.indexOf('-')), 10);
                      var binTo = parseInt(bines.substring(bines.indexOf('-') + 1, bines.length), 10);
                      if (bin >= binFrom && bin <= binTo) {
                        valid = true;
                      }
                    }
                  }
                  if (bradescardBines.hasOwnProperty("singleBines")) {
                    var singleBines = bradescardBines.singleBines;
                    for (var i = 0; singleBines.length > i; i++) {
                      var singleBin = parseInt(singleBines[i].bin);
                      if (bin == singleBin) {
                        valid = true;
                      }
                    }
                  }
                  return valid;
                },
                errorResponse: function (errorCode) {
                    var errorMessages = $form.data("mpErrorMessages");
                    var errorField;

                    // Set error code message if found, otherwise set default error message
                    var errorMessage = errorMessages[errorCode]
                        ? errorMessages[errorCode]
                        : errorMessages.default;
                    /* eslint-disable consistent-return */
                    /*Object.keys(fields).forEach(function (index) {
                        var field = fields[index];
                        if (
                            field.errors &&
                            field.errors.indexOf(errorCode) !== -1
                        ) {
                            errorField = field;
                            return true;
                        }
                    });*/
                    errorField = fields.cardNumber;
                    /* eslint-enable consistent-return */
                    if (errorField) {
                        errorField.$el
                            .next(".invalid-feedback")
                            .focus()
                            .show()
                            .text(errorMessage);
                    }
                    /* else {
                        $(".error-message").show();
                        $(".error-message-text").text(errorMessage);
                    }*/
                }
            },

            installment: {
                setapi: function(){
                    var creditCard = $(this).val().replaceAll(" ", "");
                    if(creditCard && creditCard.length >= 15){
                        console.log(creditCard);
                        console.log($("input[name$=\"billing_paymentMethod\"]:enabled").val());
                    }

                },
                /**
                 * @function set
                 * @description Set installments
                 * @param {string} paymentMethodId payment method
                 */
                set: function (paymentMethodId) {
                    // Set installments info
                    console.log(paymentMethodId);
                    console.log($form.data("mpCartTotal"));
                    Mercadopago.getInstallments(
                        {
                            payment_method_id: paymentMethodId,
                            amount: $form.data("mpCartTotal")
                        },
                        methods.installment.handleServiceResponse
                    );
                },
                /**
                 * @function handleServiceResponse
                 * @description Callback for installments
                 * @param {number} status status
                 * @param {Array} response response
                 */
                handleServiceResponse: function (status, response) {
                    fields.installments.$el.find("option").remove();

                    if (+status !== 200 && +status !== 201) {
                        return;
                    }

                    var $defaultOption = $(
                        new Option(
                            that.resourceMessages.defaultInstallments,
                            ""
                        )
                    );
                    fields.installments.$el.append($defaultOption);

                    if (response[0].payer_costs) {
                        $.each(response[0].payer_costs, function (i, item) {
                            fields.installments.$el.append(
                                $("<option>", {
                                    value: item.installments,
                                    text:
                                        item.recommended_message ||
                                        item.installments
                                })
                            );

                            if (
                                fields.installments.$el.val() !== "" &&
                                fields.installments.$el.val() ===
                                    item.installments
                            ) {
                                fields.installments.$el.val(item.installments);
                            }
                        });
                    }
                },
                /**
                 * @function handleServiceResponse
                 * @description Set installments using issuer ID
                 */
                setByIssuerId: function () {
                    var issuerId = $(this).val();

                    if (!issuerId || issuerId === "-1") {
                        return;
                    }

                    Mercadopago.getInstallments(
                        {
                            payment_method_id: fields.cardType.$el
                                .filter(":checked")
                                .val(),
                            amount: $form.data("mpCartTotal"),
                            issuer_id: issuerId
                        },
                        methods.installment.handleServiceResponse
                    );
                }
            },

            issuer: {
                /**
                 * @function set
                 * @description Set issuer
                 * @param {jQuery} $element element
                 */
                set: function ($element) {
                    Mercadopago.getIssuers(
                        $element.val(),
                        methods.issuer.handleServiceResponse
                    );
                },
                /**
                 * @function handleServiceResponse
                 * @description Callback for issuer
                 * @param {number} status status
                 * @param {Array} response response
                 */
                handleServiceResponse: function (status, response) {
                    fields.issuer.$el.find("option").remove();

                    if (+status !== 200 && +status !== 201) {
                        return;
                    }

                    var $defaultOption = $(
                        new Option(that.resourceMessages.defaultIssuer, "")
                    );
                    fields.issuer.$el.append($defaultOption);

                    $.each(response, function (i, item) {
                        fields.issuer.$el.append(
                            $("<option>", {
                                value: item.id,
                                text:
                                    item.name !== "default"
                                        ? item.name
                                        : that.configuration.defaultIssuer
                            })
                        );

                        if (
                            fields.issuer.$el.val() !== "" &&
                            fields.issuer.$el.val() === item.id
                        ) {
                            fields.issuer.$el.val(item.id);
                        }
                    });
                }
            },

            docType: {
                /**
                 * @function init
                 * @description Init identification document type
                 */
                init: function () {
                    Mercadopago.getIdentificationTypes(
                        methods.docType.handleServiceResponse
                    );
                },
                /**
                 * @function handleServiceResponse
                 * @description Callback for identification document type
                 * @param {number} status status
                 * @param {Array} response response
                 */
                handleServiceResponse: function (status, response) {
                    fields.docType.$el.find("option").remove();

                    if (+status !== 200 && +status !== 201) {
                        return;
                    }

                    $.each(response, function (i, item) {
                        fields.docType.$el.append(
                            $("<option>", {
                                value: item.id,
                                text: item.name,
                                "data-min-length": item.min_length,
                                "data-max-length": item.max_length
                            })
                        );
                    });

                    fields.docType.$el.trigger("change");
                }
            },

            docNumber: {
                /**
                 * @function setRange
                 * @description Set range identification document number
                 */
                setRange: function () {
                    var $selectedOption = $(this).find("option:selected");
                    var minLength = $selectedOption.data("minLength");
                    var maxLength = $selectedOption.data("maxLength");

                    // Set label
                    var labelSecondPart =
                        $selectedOption.val() === that.configuration.docTypeDNI
                            ? that.resourceMessages.docNumberLabelDNI
                            : that.resourceMessages.docNumberLabelOther;
                    fields.docNumber.$label.text(
                        that.resourceMessages.docNumberLabel +
                            " " +
                            labelSecondPart
                    );

                    // Set range
                    fields.docNumber.$wrapper.addClass("required");
                    fields.docNumber.$el.attr("maxlength", maxLength);
                    fields.docNumber.$el.attr("minlength", minLength);

                    // Set tooltip
                    fields.docNumber.$tooltip.text(
                        that.resourceMessages.docNumberTooltip
                            .replace("{0}", minLength)
                            .replace("{1}", maxLength)
                    );
                }
            },

            registeredCustomer: {
                /**
                 * @function togglePaymentType
                 * @description Toggle payment type (new or stored)
                 * @param {Event} event event
                 */
                togglePaymentType: function (event) {
                    var $el = $(this);
                    var isNew = $el.data("togglePaymentType") === "new";

                    $elements.customerCardsContainer.toggleClass(
                        "checkout-hidden",
                        isNew
                    );

                    // Disable and remove value to properly create token
                    fields.cardId.$el.prop("disabled", isNew);
                    if (isNew) {
                        fields.cardId.$el.val("");
                    }
                    /* eslint-disable eqeqeq, no-restricted-syntax */
                    for (var field in fields) {
                        if (fields[field].hide.stored) {
                            fields[field].$el
                                .closest(".js-mp-container")
                                .toggleClass("checkout-hidden", !isNew);
                        }
                        if (fields[field].disable.stored) {
                            fields[field].$el.prop("disabled", !isNew);
                        }
                    }
                    /* eslint-enable eqeqeq, no-restricted-syntax */

                    // Set initial states
                    if (isNew) {
                        methods.paymentOption.setInitialState.newPayment();
                    }
                    // Only when triggered from event (to avoid recursion)
                    if (event && !isNew) {
                        methods.paymentOption.setInitialState.restoreStoredPayment();
                    }

                    // Change to opposite
                    $el.data("togglePaymentType", isNew ? "stored" : "new");
                    $el.text(
                        $el.data((isNew ? "stored" : "new") + "PaymentText")
                    );
                },
                /**
                 * @function selectCustomerCard
                 * @description Select store credit card
                 */
                selectCustomerCard: function () {
                    var $el = $(this);
                    $elements.customerCard.removeClass("selected-payment");
                    $el.addClass("selected-payment");
                    fields.cardId.$el.val($el.data("mpCustomerCard"));

                    var paymentMethodInput = fields.cardType.$el.filter(
                        function () {
                            return this.value === $el.data("mpMethodId");
                        }
                    );
                    paymentMethodInput.prop("checked", true);
                    methods.card.handleTypeChange.call(paymentMethodInput[0], {
                        data: { handleOther: false }
                    });
                }
            }
        };

        /**
         * @function initSDK
         * @description Init MercadoPago JS SDK by setting public key
         */
        function initSDK() {
            window.Mercadopago.setPublishableKey(that.preferences.publicKey);
        }

        /**
         * @function initAjaxListener
         * @description This function is necessary to listen to PlaceOrder errors and reset the credit-card token
         */
        function initAjaxListener() {
            $(document).ajaxComplete(function (event, xhr) {
                if (xhr && xhr.responseText.indexOf("resetMpToken") >= 0) {
                    if (Object.hasOwnProperty.call(Mercadopago, "tokenId")) {
                        Mercadopago.tokenId = "";
                    }
                    $("input[name*=\"mercadoPagoCreditCard_token\"]").val("");
                }
                if (xhr && xhr.responseText.indexOf("detailedError") >= 0) {
                    try {
                        var response = JSON.parse(xhr.responseText);
                        if (response.detailedError) {
                            $(".payment-summary .edit-button").trigger("click");
                            methods.token.errorResponse(response.detailedError);
                        }
                    } catch (ex) {
                        // do nothing
                    }
                }
            });
        }

        /**
         * @function events
         * @description Init events
         */
        function events() {
            $elements.paymentOptionTab.on(
                "click",
                methods.paymentOption.handleChange
            ); // By click
            fields.cardType.$el.on(
                "change",
                { handleOther: true },
                methods.card.handleTypeChange
            );
            fields.docType.$el.on("change", methods.docNumber.setRange);
            $elements.paymentTypeButton.on(
                "click",
                methods.registeredCustomer.togglePaymentType
            );
            $elements.customerCard.on(
                "click",
                methods.registeredCustomer.selectCustomerCard
            );
            $(".next-step-button .submit-payment").on(
                "click",
                methods.token.populate
            );
            fields.cardNumber.$el.on(
                "focusout",
                methods.card.guessingPaymentMethod
            );
            $('.cardNumber').on(
                "focusout",
                methods.installment.setapi
            );

            $('.cardSelectorMercadoPago').on('change', function (e) {
                console.log("asdasdasd-->")
                var $el = $(this);
                var isMercadoPago = $("input[name$=\"billing_paymentMethod\"]:enabled").val() === that.configuration.paymentMethodId;

                e.preventDefault();

                if(isMercadoPago){
                    fields.cardId.$el.val(e.target.value);
                    fields.customerId.$el.val($el.find(':selected').data('customerId'));
                    //var $el = $(this);
                    var isNew = !e.target.value;

                    $elements.customerCardsContainer.toggleClass(
                        "checkout-hidden",
                        isNew
                    );

                    // Disable and remove value to properly create token
                    fields.cardId.$el.prop("disabled", isNew);
                    if (isNew) {
                        fields.cardId.$el.val("");
                    }
                    /* eslint-disable eqeqeq, no-restricted-syntax */
                    for (var field in fields) {
                        if (fields[field].hide.stored) {
                            fields[field].$el
                                .closest(".js-mp-container")
                                .toggleClass("checkout-hidden", !isNew);
                        }
                        if (fields[field].disable.stored) {
                            fields[field].$el.prop("disabled", !isNew);
                        }
                    }
                    /* eslint-enable eqeqeq, no-restricted-syntax */

                    // Set initial states
                    if (isNew) {
                        methods.paymentOption.setInitialState.newPayment();
                    }
                    if(e.target.value) {
                        var paymentMethodInput = fields.cardType.$el.filter(
                            function () {
                                return this.value === $el.find(':selected')
                                    .data('cardType');
                            }
                        );
                        paymentMethodInput.prop("checked", true);
                        methods.card.handleTypeChange.call(paymentMethodInput[0], {
                            data: { handleOther: false }
                        });
                        $('#mercadoPagoSaveCreditCard').prop('checked', false);
                        $('select[name$="_expirationMonth"]').val('');
                        $('select[name$="_expirationYear"]').val('');
                        $('input[name$="_expirationDate"]').val('');
                        $('input[name$="_cardOwner"]').val('');
                        $('#mercadoPagoCardNumber').val('');
                        $('#mercadoPagoSecurityCode').val('');
                    }
                    $('#mercadoPagoSecurityCode').val('');
                    // Only when triggered from event (to avoid recursion)
                    /*if (event && !isNew) {
                        methods.paymentOption.setInitialState.restoreStoredPayment();
                    }*/

                    // Change to opposite
                    //$el.data("togglePaymentType", isNew ? "stored" : "new");
                    //$el.text(
                    //$el.data((isNew ? "stored" : "new") + "PaymentText")
                    //);
                }

                // if(!isMercadoPago && e.target.value) {
                //     console.log('seleccion con datos');
                //     $('.payment-information').data('is-new-payment', false);
                //
                //     $('.credit-card-form').addClass('checkout-hidden');
                // }else if(!isMercadoPago && !e.target.value){
                //     $('.payment-information').data('is-new-payment', true);
                //
                //     $('.credit-card-form').removeClass('checkout-hidden');
                // }



            });

            $('.cardSelectorPaypal').on('change', function (e) {
                if(e.target.value) {
                    $('#saveCreditCard').prop('checked', false);
                    console.log('seleccion con datos');
                    $('.payment-information').data('is-new-payment', false);
                    $('.credit-card-form').addClass('checkout-hidden');
                    $('.saved-security-code').removeClass('checkout-hidden');
                    $('input[name$="_cardNumber"]').data('cleave').setRawValue('');
                    $('select[name$="_expirationMonth"]').val('');
                    $('select[name$="_expirationYear"]').val('');
                    $('input[name$="_expirationDate"]').val('');
                    $('input[name$="_securityCode"]').val('');
                    $('input[name$="_cardOwner"]').val('');
                }else {
                    $('.payment-information').data('is-new-payment', true);
                    $('.credit-card-form').removeClass('checkout-hidden');
                    $('.saved-security-code').addClass('checkout-hidden');
                    $('#saved-payment-security-code').val('');

                }
            });

            $('.cardSelectorPaypal').val('');
            $('.cardSelectorMercadoPago').val('');

        }

        this.preferences = $form.data("mpPreferences");
        this.resourceMessages = $form.data("mpResourceMessages");
        this.configuration = $form.data("mpConfiguration");

        /**
         * @function init
         * @description Init integration
         */
        this.init = function () {
            if ($content.length > 0) {
                initSDK();
                if (that.preferences.enableDocTypeNumber) {
                    methods.docType.init();
                }
                events();
                methods.paymentOption.handleChange.call(
                    $elements.paymentOptionTab.filter(".enabled")
                ); // Initial
                initAjaxListener();
            }
        };
    }

    $(document).ready(new MercadoPago().init);
}(jQuery));


/***/ }),

/***/ "./cartridges/int_mercadopago/cartridge/client/default/js/checkout/shipping.js":
/*!*************************************************************************************!*\
  !*** ./cartridges/int_mercadopago/cartridge/client/default/js/checkout/shipping.js ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var addressHelpers = __webpack_require__(/*! ./address */ "./cartridges/int_mercadopago/cartridge/client/default/js/checkout/address.js");
var formHelpers = __webpack_require__(/*! base/checkout/formErrors */ "./cartridges/app_storefront_base/cartridge/client/default/js/checkout/formErrors.js");
var scrollAnimate = __webpack_require__(/*! base/components/scrollAnimate */ "./cartridges/app_storefront_base/cartridge/client/default/js/components/scrollAnimate.js");

function initSaveAddresContainer(){
    var selectedOption = $('option:selected', $('.addressSelector'));
    var shipmentUUID = selectedOption[0].value;
    var saveAddress= $('.saveAddressCheck:checked').val();
    if (shipmentUUID === 'new' || shipmentUUID==='new2' || shipmentUUID.indexOf('ab_') !== 0) {

        $('.saveAddressContainer').removeClass('d-none');
        if(saveAddress){
            $('.addressIdContainer').show();
        }else{
            $('.addressIdContainer').hide();
        }
    }else{
        $('.saveAddressContainer').addClass('d-none');
        $('.addressIdContainer').hide();
    }
}

function zipCodeRemapping(){
    //$('.shippingZipCode').trigger("focusout");
    $('#shippingZipCodedefault').trigger("focusout");
    //$('#')
    //$('select[name$=_suburb],input[name$=_suburb]', form).val(addressObject.suburb);
}

/**
 * updates the shipping address selector within shipping forms
 * @param {Object} productLineItem - the productLineItem model
 * @param {Object} shipping - the shipping (shipment model) model
 * @param {Object} order - the order model
 * @param {Object} customer - the customer model
 */
function updateShippingAddressSelector(productLineItem, shipping, order, customer) {
    console.log("----------------> updateShippingAddressSelector");
    var uuidEl = $('input[value=' + productLineItem.UUID + ']');
    var shippings = order.shipping;

    var form;
    var $shippingAddressSelector;
    var hasSelectedAddress = false;

    if (uuidEl && uuidEl.length > 0) {
        form = uuidEl[0].form;
        $shippingAddressSelector = $('.addressSelector', form);
    }

    if ($shippingAddressSelector && $shippingAddressSelector.length === 1) {
        $shippingAddressSelector.empty();
        // Add New Address option
        $shippingAddressSelector.append(addressHelpers.methods.optionValueForAddress(
            null,
            false,
            order
        ));

        if (customer.addresses && customer.addresses.length > 0) {
            $shippingAddressSelector.append(addressHelpers.methods.optionValueForAddress(
                order.resources.accountAddresses,
                false,
                order
            ));

            customer.addresses.forEach(function (address) {
                var isSelected = shipping.matchingAddressId === address.ID;
                $shippingAddressSelector.append(
                    addressHelpers.methods.optionValueForAddress(
                        { UUID: 'ab_' + address.ID, shippingAddress: address },
                        isSelected,
                        order
                    )
                );
            });
        }
        // Separator -
        $shippingAddressSelector.append(addressHelpers.methods.optionValueForAddress(
            order.resources.shippingAddresses, false, order, { className: 'multi-shipping' }
        ));
        shippings.forEach(function (aShipping) {
            var isSelected = shipping.UUID === aShipping.UUID;
            hasSelectedAddress = hasSelectedAddress || isSelected;
            var addressOption = addressHelpers.methods.optionValueForAddress(
                aShipping,
                isSelected,
                order,
                { className: 'multi-shipping' }
            );

            var newAddress = addressOption.html() === order.resources.addNewAddress;
            var matchingUUID = aShipping.UUID === shipping.UUID;
            if ((newAddress && matchingUUID) || (!newAddress && matchingUUID) || (!newAddress && !matchingUUID)) {
                $shippingAddressSelector.append(addressOption);
            }
            if (newAddress && !matchingUUID) {
                $(addressOption[0]).remove();
            }
        });
    }

    if (!hasSelectedAddress) {
        // show
        $(form).addClass('hide-details');
    } else {
        $(form).removeClass('hide-details');
    }

    $('body').trigger('shipping:updateShippingAddressSelector', {
        productLineItem: productLineItem,
        shipping: shipping,
        order: order,
        customer: customer
    });
}

/**
 * updates the shipping address form values within shipping forms
 * @param {Object} shipping - the shipping (shipment model) model
 */
function updateShippingAddressFormValues(shipping) {
    console.log("updateShippingAddressFormValues------------->");
    var addressObject = $.extend({}, shipping.shippingAddress);
    if (!addressObject) {
        addressObject = {
            firstName: null,
            lastName: null,
            address1: null,
            address2: null,
            city: null,
            postalCode: null,
            stateCode: null,
            countryCode: null,
            phone: null,
            suburb: null,
            county: null,
            numberExt: null,
            numberInt: null,
            addressId: null
        };
    }

    addressObject.isGift = shipping.isGift;
    addressObject.giftMessage = shipping.giftMessage;

    $('input[value=' + shipping.UUID + ']').each(function (formIndex, el) {
        var form = el.form;
        if (!form) return;

        $('input[name$=_firstName]', form).val(addressObject.firstName);
        $('input[name$=_lastName]', form).val(addressObject.lastName);
        $('input[name$=_address1]', form).val(addressObject.address1);
        $('input[name$=_address2]', form).val(addressObject.address2);
        $('input[name$=_city]', form).val(addressObject.city);
        $('input[name$=_postalCode]', form).val(addressObject.postalCode);
        $('select[name$=_stateCode],input[name$=_stateCode]', form)
            .val(addressObject.stateCode);

        $('input[name$=_phone]', form).val(addressObject.phone);
        $('select[name$=_suburb],input[name$=_suburb]', form).val(addressObject.suburb);
        $('input[name$=_county]', form).val(addressObject.county);
        $('input[name$=_numberExt]', form).val(addressObject.numberExt);
        $('input[name$=_numberInt]', form).val(addressObject.numberInt);
        $('input[name$=_addressId]', form).val(addressObject.addressId);
        $('input[name$=_saveAddress]', form).prop('checked', addressObject.saveAddress);
        //$("#shippingAddressIddefault").removeAttr('required');
        //$("#shippingAddressIddefault").removeAttr('aria-required');
        $(form).find('.addressIdContainer').hide();

        $('input[name$=_isGift]', form).prop('checked', addressObject.isGift);
        $('textarea[name$=_giftMessage]', form).val(addressObject.isGift && addressObject.giftMessage ? addressObject.giftMessage : '');
    });

    $('body').trigger('shipping:updateShippingAddressFormValues', { shipping: shipping });
}

/**
 * updates the shipping method radio buttons within shipping forms
 * @param {Object} shipping - the shipping (shipment model) model
 */
function updateShippingMethods(shipping) {
    console.log("updateShippingMethods------------->");
    var uuidEl = $('input[value=' + shipping.UUID + ']');
    if (uuidEl && uuidEl.length > 0) {
        $.each(uuidEl, function (shipmentIndex, el) {
            var form = el.form;
            if (!form) return;

            var $shippingMethodList = $('.shipping-method-list', form);

            if ($shippingMethodList && $shippingMethodList.length > 0) {
                $shippingMethodList.empty();
                var shippingMethods = shipping.applicableShippingMethods;
                var selected = shipping.selectedShippingMethod || {};
                var shippingMethodFormID = form.name + '_shippingAddress_shippingMethodID';
                //
                // Create the new rows for each shipping method
                //
                $.each(shippingMethods, function (methodIndex, shippingMethod) {
                    var tmpl = $('#shipping-method-template').clone();
                    // set input
                    $('input', tmpl)
                        .prop('id', 'shippingMethod-' + shippingMethod.ID + '-' + shipping.UUID)
                        .prop('name', shippingMethodFormID)
                        .prop('value', shippingMethod.ID)
                        .attr('checked', shippingMethod.ID === selected.ID);

                    $('label', tmpl)
                        .prop('for', 'shippingMethod-' + shippingMethod.ID + '-' + shipping.UUID);
                    // set shipping method name
                    $('.display-name', tmpl).text(shippingMethod.displayName);
                    // set or hide arrival time
                    if (shippingMethod.estimatedArrivalTime) {
                        $('.arrival-time', tmpl)
                            .text('(' + shippingMethod.estimatedArrivalTime + ')')
                            .show();
                    }
                    // set shipping cost
                    $('.shipping-cost', tmpl).text(shippingMethod.shippingCost);
                    $shippingMethodList.append(tmpl.html());
                });
            }
        });
    }

    $('body').trigger('shipping:updateShippingMethods', { shipping: shipping });
}

/**
 * Update list of available shipping methods whenever user modifies shipping address details.
 * @param {jQuery} $shippingForm - current shipping form
 */
function updateShippingMethodList($shippingForm) {
    console.log("updateShippingMethodList------------->");
    // delay for autocomplete!
    setTimeout(function () {
        var $shippingMethodList = $shippingForm.find('.shipping-method-list');
        var urlParams = addressHelpers.methods.getAddressFieldsFromUI($shippingForm);
        var shipmentUUID = $shippingForm.find('[name=shipmentUUID]').val();
        var url = $shippingMethodList.data('actionUrl');
        urlParams.shipmentUUID = shipmentUUID;

        $shippingForm.spinner().start();
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: urlParams,
            success: function (data) {
                if (data.error) {
                    window.location.href = data.redirectUrl;
                } else {
                    $('body').trigger('checkout:updateCheckoutView',
                        {
                            order: data.order,
                            customer: data.customer,
                            options: { keepOpen: true },
                            suburbsArray: data.suburbsArray
                        });

                    $shippingForm.spinner().stop();
                }
            }
        });
    }, 300);
}

/**
 * updates the order shipping summary for an order shipment model
 * @param {Object} shipping - the shipping (shipment model) model
 * @param {Object} order - the order model
 */
function updateShippingSummaryInformation(shipping, order) {
    console.log("updateShippingSummaryInformation------------->");
    $('[data-shipment-summary=' + shipping.UUID + ']').each(function (i, el) {
        var $container = $(el);
        var $shippingAddressLabel = $container.find('.shipping-addr-label');
        var $addressContainer = $container.find('.address-summary');
        var $shippingPhone = $container.find('.shipping-phone');
        var $methodTitle = $container.find('.shipping-method-title');
        var $methodArrivalTime = $container.find('.shipping-method-arrival-time');
        var $methodPrice = $container.find('.shipping-method-price');
        var $shippingSummaryLabel = $container.find('.shipping-method-label');
        var $summaryDetails = $container.find('.row.summary-details');
        var giftMessageSummary = $container.find('.gift-summary');

        var address = shipping.shippingAddress;
        var selectedShippingMethod = shipping.selectedShippingMethod;
        var isGift = shipping.isGift;

        addressHelpers.methods.populateAddressSummary($addressContainer, address);

        if (address && address.phone) {
            $shippingPhone.text(address.phone);
        } else {
            $shippingPhone.empty();
        }

        if (selectedShippingMethod) {
            $('body').trigger('shipping:updateAddressLabelText',
                { selectedShippingMethod: selectedShippingMethod, resources: order.resources, shippingAddressLabel: $shippingAddressLabel });
            $shippingSummaryLabel.show();
            $summaryDetails.show();
            $methodTitle.text(selectedShippingMethod.displayName);
            if (selectedShippingMethod.estimatedArrivalTime) {
                $methodArrivalTime.text(
                    '( ' + selectedShippingMethod.estimatedArrivalTime + ' )'
                );
            } else {
                $methodArrivalTime.empty();
            }
            $methodPrice.text(selectedShippingMethod.shippingCost);
        }

        if (isGift) {
            giftMessageSummary.find('.gift-message-summary').text(shipping.giftMessage);
            giftMessageSummary.removeClass('d-none');
        } else {
            giftMessageSummary.addClass('d-none');
        }
    });

    $('body').trigger('shipping:updateShippingSummaryInformation', { shipping: shipping, order: order });
}

/**
 * Update the read-only portion of the shipment display (per PLI)
 * @param {Object} productLineItem - the productLineItem model
 * @param {Object} shipping - the shipping (shipment model) model
 * @param {Object} order - the order model
 * @param {Object} [options] - options for updating PLI summary info
 * @param {Object} [options.keepOpen] - if true, prevent changing PLI view mode to 'view'
 */
function updatePLIShippingSummaryInformation(productLineItem, shipping, order, options) {
    console.log("updatePLIShippingSummaryInformation------------->");
    var $pli = $('input[value=' + productLineItem.UUID + ']');
    var form = $pli && $pli.length > 0 ? $pli[0].form : null;

    if (!form) return;

    var $viewBlock = $('.view-address-block', form);

    var address = shipping.shippingAddress || {};
    var selectedMethod = shipping.selectedShippingMethod;

    var nameLine = address.firstName ? address.firstName + ' ' : '';
    if (address.lastName) nameLine += address.lastName;

    var address1Line = address.address1;
    var address2Line = address.address2;

    var phoneLine = address.phone;

    var shippingCost = selectedMethod ? selectedMethod.shippingCost : '';
    var methodNameLine = selectedMethod ? selectedMethod.displayName : '';
    var methodArrivalTime = selectedMethod && selectedMethod.estimatedArrivalTime
        ? '(' + selectedMethod.estimatedArrivalTime + ')'
        : '';

    var tmpl = $('#pli-shipping-summary-template').clone();

    $('.ship-to-name', tmpl).text(nameLine);
    $('.ship-to-address1', tmpl).text(address1Line);
    $('.ship-to-address2', tmpl).text(address2Line);
    $('.ship-to-city', tmpl).text(address.city);
    if (address.stateCode) {
        $('.ship-to-st', tmpl).text(address.stateCode);
    }
    $('.ship-to-zip', tmpl).text(address.postalCode);
    $('.ship-to-phone', tmpl).text(phoneLine);

    if (!address2Line) {
        $('.ship-to-address2', tmpl).hide();
    }

    if (!phoneLine) {
        $('.ship-to-phone', tmpl).hide();
    }

    if (shipping.selectedShippingMethod) {
        $('.display-name', tmpl).text(methodNameLine);
        $('.arrival-time', tmpl).text(methodArrivalTime);
        $('.price', tmpl).text(shippingCost);
    }

    if (shipping.isGift) {
        $('.gift-message-summary', tmpl).text(shipping.giftMessage);
        var shipment = $('.gift-message-' + shipping.UUID);
        $(shipment).val(shipping.giftMessage);
    } else {
        $('.gift-summary', tmpl).addClass('d-none');
    }
    // checking h5 title shipping to or pickup
    var $shippingAddressLabel = $('.shipping-header-text', tmpl);
    $('body').trigger('shipping:updateAddressLabelText',
        { selectedShippingMethod: selectedMethod, resources: order.resources, shippingAddressLabel: $shippingAddressLabel });

    $viewBlock.html(tmpl.html());

    $('body').trigger('shipping:updatePLIShippingSummaryInformation', {
        productLineItem: productLineItem,
        shipping: shipping,
        order: order,
        options: options
    });
}

/**
 * Update the hidden form values that associate shipping info with product line items
 * @param {Object} productLineItem - the productLineItem model
 * @param {Object} shipping - the shipping (shipment model) model
 */
function updateProductLineItemShipmentUUIDs(productLineItem, shipping) {
    console.log("updateProductLineItemShipmentUUIDs------------->");
    $('input[value=' + productLineItem.UUID + ']').each(function (key, pli) {
        var form = pli.form;
        $('[name=shipmentUUID]', form).val(shipping.UUID);
        $('[name=originalShipmentUUID]', form).val(shipping.UUID);

        $(form).closest('.card').attr('data-shipment-uuid', shipping.UUID);
    });

    $('body').trigger('shipping:updateProductLineItemShipmentUUIDs', {
        productLineItem: productLineItem,
        shipping: shipping
    });
}

/**
 * Update the shipping UI for a single shipping info (shipment model)
 * @param {Object} shipping - the shipping (shipment model) model
 * @param {Object} order - the order/basket model
 * @param {Object} customer - the customer model
 * @param {Object} [options] - options for updating PLI summary info
 * @param {Object} [options.keepOpen] - if true, prevent changing PLI view mode to 'view'
 */
function updateShippingInformation(shipping, order, customer, options,suburbsArray) {
    console.log("updateShippingInformation------------->");
    $("#shippingSuburbdefault").empty();
    $.each(suburbsArray, function (i, item) {
        $("#shippingSuburbdefault").append($("<option> ", {
            value: item,
            text : item
        }, "</option>"));
    });
    // First copy over shipmentUUIDs from response, to each PLI form
    order.shipping.forEach(function (aShipping) {
        aShipping.productLineItems.items.forEach(function (productLineItem) {
            updateProductLineItemShipmentUUIDs(productLineItem, aShipping);
        });
    });

    // Now update shipping information, based on those associations
    updateShippingMethods(shipping);
    updateShippingAddressFormValues(shipping);
    updateShippingSummaryInformation(shipping, order);

    // And update the PLI-based summary information as well
    shipping.productLineItems.items.forEach(function (productLineItem) {
        updateShippingAddressSelector(productLineItem, shipping, order, customer);
        updatePLIShippingSummaryInformation(productLineItem, shipping, order, options);
    });

    $('body').trigger('shipping:updateShippingInformation', {
        order: order,
        shipping: shipping,
        customer: customer,
        options: options
    });
}

/**
 * Update the checkout state (single vs. multi-ship)
 * @param {Object} order - checkout model to use as basis of new truth
 */
function updateMultiShipInformation(order) {
    console.log("updateMultiShipInformation------------->");
    var $checkoutMain = $('#checkout-main');
    var $checkbox = $('[name=usingMultiShipping]');
    var $submitShippingBtn = $('button.submit-shipping');
    $('.shipping-error .alert-danger').remove();

    if (order.usingMultiShipping) {
        $checkoutMain.addClass('multi-ship');
        $checkbox.prop('checked', true);
    } else {
        $checkoutMain.removeClass('multi-ship');
        $checkbox.prop('checked', null);
        $submitShippingBtn.prop('disabled', null);
    }

    $('body').trigger('shipping:updateMultiShipInformation', { order: order });
}

/**
  * Create an alert to display the error message
  * @param {Object} message - Error message to display
  */
function createErrorNotification(message) {
    console.log("createErrorNotification------------->");
    var errorHtml = '<div class="alert alert-danger alert-dismissible valid-cart-error ' +
    'fade show" role="alert">' +
    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
    '<span aria-hidden="true">&times;</span>' +
    '</button>' + message + '</div>';

    $('.shipping-error').append(errorHtml);
    scrollAnimate($('.shipping-error'));
}

/**
 * Handle response from the server for valid or invalid form fields.
 * @param {Object} defer - the deferred object which will resolve on success or reject.
 * @param {Object} data - the response data with the invalid form fields or
 *  valid model data.
 */
function shippingFormResponse(defer, data) {
    console.log("shippingFormResponse------------->");
    var isMultiShip = $('#checkout-main').hasClass('multi-ship');
    var formSelector = isMultiShip
        ? '.multi-shipping .active form'
        : '.single-shipping form';

    // highlight fields with errors
    if (data.error) {
        if (data.fieldErrors.length) {
            data.fieldErrors.forEach(function (error) {
                if (Object.keys(error).length) {
                    formHelpers.loadFormErrors(formSelector, error);
                }
            });
            defer.reject(data);
        }

        if (data.serverErrors && data.serverErrors.length) {
            $.each(data.serverErrors, function (index, element) {
                createErrorNotification(element);
            });

            defer.reject(data);
        }

        if (data.cartError) {
            window.location.href = data.redirectUrl;
            defer.reject();
        }
    } else {
        // Populate the Address Summary

        $('body').trigger('checkout:updateCheckoutView', {
            order: data.order,
            customer: data.customer
        });
        scrollAnimate($('.payment-form'));
        defer.resolve(data);
    }
}
/**
 * Clear out all the shipping form values and select the new address in the drop down
 * @param {Object} order - the order object
 */
function clearShippingForms(order) {
    console.log("clearShippingForms----------------->");
    order.shipping.forEach(function (shipping) {
        $('input[value=' + shipping.UUID + ']').each(function (formIndex, el) {
            var form = el.form;
            if (!form) return;

            $('input[name$=_firstName]', form).val('');
            $('input[name$=_lastName]', form).val('');
            $('input[name$=_address1]', form).val('');
            $('input[name$=_address2]', form).val('');
            $('input[name$=_city]', form).val('');
            $('input[name$=_postalCode]', form).val('');
            $('select[name$=_stateCode],input[name$=_stateCode]', form).val('');

            $('input[name$=_phone]', form).val('');

            $('select[name$=_suburb],input[name$=_suburb]', form).val('');
            $('input[name$=_county]', form).val('');
            $('input[name$=_numberExt]', form).val('');
            $('input[name$=_numberInt]', form).val('');
            $('input[name$=_addressId]', form).val('');
            $('input[name$=_saveAddress]', form).prop('checked', false);
            //$(form).find('.addressIdContainer').addClass('d-none');

            //$("#shippingAddressIddefault").removeAttr('required');
            //$("#shippingAddressIddefault").removeAttr('aria-required');
            $(form).find('.addressIdContainer').hide();
            $('input[name$=_isGift]', form).prop('checked', false);
            $('textarea[name$=_giftMessage]', form).val('');
            $(form).find('.gift-message').addClass('d-none');

            $(form).attr('data-address-mode', 'new');
            var addressSelectorDropDown = $('.addressSelector option[value=new]', form);
            $(addressSelectorDropDown).prop('selected', true);
        });
    });

    $('body').trigger('shipping:clearShippingForms', { order: order });
}

/**
 * Does Ajax call to create a server-side shipment w/ pliUUID & URL
 * @param {string} url - string representation of endpoint URL
 * @param {Object} shipmentData - product line item UUID
 * @returns {Object} - promise value for async call
 */
function createNewShipment(url, shipmentData) {
    console.log("createNewShipment----------------->");
    $.spinner().start();
    return $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: shipmentData
    });
}

/**
 * Does Ajax call to select shipping method
 * @param {string} url - string representation of endpoint URL
 * @param {Object} urlParams - url params
 * @param {Object} el - element that triggered this call
 */
function selectShippingMethodAjax(url, urlParams, el) {
    console.log("selectShippingMethodAjax----------------->");
    $.spinner().start();
    $('body').trigger('checkout:beforeShippingMethodSelected');
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: urlParams
    })
        .done(function (data) {
            if (data.error) {
                window.location.href = data.redirectUrl;
            } else {
                $('body').trigger('checkout:updateCheckoutView',
                    {
                        order: data.order,
                        customer: data.customer,
                        options: { keepOpen: true },
                        urlParams: urlParams
                    }
                );
                $('body').trigger('checkout:postUpdateCheckoutView',
                    {
                        el: el
                    }
                );
            }
            $('body').trigger('checkout:shippingMethodSelected', data);
            $.spinner().stop();
        })
        .fail(function () {
            $.spinner().stop();
        });
}

/**
 * Hide and show to appropriate elements to show the multi ship shipment cards in the enter view
 * @param {jQuery} element - The shipping content
 */
function enterMultishipView(element) {
    console.log("enterMultishipView----------------->");
    element.find('.btn-enter-multi-ship').removeClass('d-none');

    element.find('.view-address-block').addClass('d-none');
    element.find('.shipping-address').addClass('d-none');
    element.find('.btn-save-multi-ship.save-shipment').addClass('d-none');
    element.find('.btn-edit-multi-ship').addClass('d-none');
    element.find('.multi-ship-address-actions').addClass('d-none');
}

/**
 * Hide and show to appropriate elements to show the multi ship shipment cards in the view mode
 * @param {jQuery} element - The shipping content
 */
function viewMultishipAddress(element) {
    console.log("viewMultishipAddress----------------->");
    element.find('.view-address-block').removeClass('d-none');
    element.find('.btn-edit-multi-ship').removeClass('d-none');

    element.find('.shipping-address').addClass('d-none');
    element.find('.btn-save-multi-ship.save-shipment').addClass('d-none');
    element.find('.btn-enter-multi-ship').addClass('d-none');
    element.find('.multi-ship-address-actions').addClass('d-none');
}

/**
 * Hide and show to appropriate elements that allows the user to edit multi ship address information
 * @param {jQuery} element - The shipping content
 */
function editMultiShipAddress(element) {
    console.log("editMultiShipAddress----------------->");
    // Show
    element.find('.shipping-address').removeClass('d-none');
    element.find('.btn-save-multi-ship.save-shipment').removeClass('d-none');

    // Hide
    element.find('.view-address-block').addClass('d-none');
    element.find('.btn-enter-multi-ship').addClass('d-none');
    element.find('.btn-edit-multi-ship').addClass('d-none');
    element.find('.multi-ship-address-actions').addClass('d-none');

    $('body').trigger('shipping:editMultiShipAddress', { element: element, form: element.find('.shipping-form') });
}

/**
 * perform the proper actions once a user has clicked enter address or edit address for a shipment
 * @param {jQuery} element - The shipping content
 * @param {string} mode - the address mode
 */
function editOrEnterMultiShipInfo(element, mode) {
    console.log("editOrEnterMultiShipInfo----------------->");
    var form = $(element).closest('form');
    var root = $(element).closest('.shipping-content');

    $('body').trigger('shipping:updateDataAddressMode', { form: form, mode: mode });

    editMultiShipAddress(root);

    var addressInfo = addressHelpers.methods.getAddressFieldsFromUI(form);

    var savedState = {
        UUID: $('input[name=shipmentUUID]', form).val(),
        shippingAddress: addressInfo
    };

    root.data('saved-state', JSON.stringify(savedState));
}

module.exports = {
    methods: {
        updateShippingAddressSelector: updateShippingAddressSelector,
        updateShippingAddressFormValues: updateShippingAddressFormValues,
        updateShippingMethods: updateShippingMethods,
        updateShippingSummaryInformation: updateShippingSummaryInformation,
        updatePLIShippingSummaryInformation: updatePLIShippingSummaryInformation,
        updateProductLineItemShipmentUUIDs: updateProductLineItemShipmentUUIDs,
        updateShippingInformation: updateShippingInformation,
        updateMultiShipInformation: updateMultiShipInformation,
        shippingFormResponse: shippingFormResponse,
        createNewShipment: createNewShipment,
        selectShippingMethodAjax: selectShippingMethodAjax,
        updateShippingMethodList: updateShippingMethodList,
        clearShippingForms: clearShippingForms,
        editMultiShipAddress: editMultiShipAddress,
        editOrEnterMultiShipInfo: editOrEnterMultiShipInfo,
        createErrorNotification: createErrorNotification,
        viewMultishipAddress: viewMultishipAddress
    },

    selectShippingMethod: function () {
        console.log("selectShippingMethod----------------->");
        var baseObj = this;

        $('.shipping-method-list').change(function () {
            console.log("selectShippingMethod----------------->");
            var $shippingForm = $(this).parents('form');
            var methodID = $(':checked', this).val();
            var shipmentUUID = $shippingForm.find('[name=shipmentUUID]').val();
            var urlParams = addressHelpers.methods.getAddressFieldsFromUI($shippingForm);
            urlParams.shipmentUUID = shipmentUUID;
            urlParams.methodID = methodID;
            urlParams.isGift = $shippingForm.find('.gift').prop('checked');
            urlParams.giftMessage = $shippingForm.find('textarea[name$=_giftMessage]').val();

            var url = $(this).data('select-shipping-method-url');

            if (baseObj.methods && baseObj.methods.selectShippingMethodAjax) {
                baseObj.methods.selectShippingMethodAjax(url, urlParams, $(this));
            } else {
                selectShippingMethodAjax(url, urlParams, $(this));
            }
        });
    },

    toggleMultiship: function () {
        console.log("toggleMultiship----------------->");
        var baseObj = this;

        $('input[name="usingMultiShipping"]').on('change', function () {
            console.log("toggleMultiship----------------->");
            var url = $('.multi-shipping-checkbox-block form').attr('action');
            var usingMultiShip = this.checked;

            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: { usingMultiShip: usingMultiShip },
                success: function (response) {
                    if (response.error) {
                        window.location.href = response.redirectUrl;
                    } else {
                        $('body').trigger('checkout:updateCheckoutView', {
                            order: response.order,
                            customer: response.customer
                        });

                        if ($('#checkout-main').data('customer-type') === 'guest') {
                            if (baseObj.methods && baseObj.methods.clearShippingForms) {
                                baseObj.methods.clearShippingForms(response.order);
                            } else {
                                clearShippingForms(response.order);
                            }
                        } else {
                            response.order.shipping.forEach(function (shipping) {
                                $('input[value=' + shipping.UUID + ']').each(function (formIndex, el) {
                                    var form = el.form;
                                    if (!form) return;

                                    $(form).attr('data-address-mode', 'edit');
                                    var addressSelectorDropDown = $(form).find('.addressSelector option[value="ab_' + shipping.matchingAddressId + '"]');
                                    $(addressSelectorDropDown).prop('selected', true);
                                    $('input[name$=_isGift]', form).prop('checked', false);
                                    $('textarea[name$=_giftMessage]', form).val('');
                                    $(form).find('.gift-message').addClass('d-none');
                                });
                            });
                        }

                        if (usingMultiShip) {
                            $('body').trigger('shipping:selectMultiShipping', { data: response });
                        } else {
                            $('body').trigger('shipping:selectSingleShipping', { data: response });
                        }
                    }

                    $.spinner().stop();
                },
                error: function () {
                    $.spinner().stop();
                }
            });
        });
    },

    selectSingleShipping: function () {
        console.log("selectSingleShipping----------------->");
        $('body').on('shipping:selectSingleShipping', function () {
            console.log("selectSingleShipping----------------->");
            $('.single-shipping .shipping-address').removeClass('d-none');
            initSaveAddresContainer();
            zipCodeRemapping();
        });
    },

    selectMultiShipping: function () {
        var baseObj = this;

        $('body').on('shipping:selectMultiShipping', function (e, data) {
            console.log("selectMultiShipping----------------->");
            $('.multi-shipping .shipping-address').addClass('d-none');

            data.data.order.shipping.forEach(function (shipping) {
                var element = $('.multi-shipping .card[data-shipment-uuid="' + shipping.UUID + '"]');

                if (shipping.shippingAddress) {
                    if (baseObj.methods && baseObj.methods.viewMultishipAddress) {
                        baseObj.methods.viewMultishipAddress($(element));
                    } else {
                        viewMultishipAddress($(element));
                    }
                } else {
                    /* eslint-disable no-lonely-if */
                    if (baseObj.methods && baseObj.methods.enterMultishipView) {
                        baseObj.methods.enterMultishipView($(element));
                    } else {
                        enterMultishipView($(element));
                    }
                    /* eslint-enable no-lonely-if */
                }
            });
        });
    },

    selectSingleShipAddress: function () {
        console.log("------------------> selectSingleShipAddress");
        $('.single-shipping .addressSelector').on('change', function () {
            console.log("------------------> selectSingleShipAddress");

            var form = $(this).parents('form')[0];
            var selectedOption = $('option:selected', this);
            var attrs = selectedOption.data();
            var shipmentUUID = selectedOption[0].value;
            var originalUUID = $('input[name=shipmentUUID]', form).val();
            var element;
            Object.keys(attrs).forEach(function (attr) {
                element = attr;
                console.log("elemento : " + element + " --> " + attrs[attr]);
                $('[name$=' + element + ']', form).val(attrs[attr]);
            });
            //$('[name$=postalCode]', form).trigger('input');
            $('[name$=stateCode]', form).trigger('change');
            if (shipmentUUID === 'new' || shipmentUUID==='new2') {
                //$('.btn-add-new', form).trigger('click');
                var $newEl = $('.addressSelector option[value=new]');
                $newEl.prop('selected', 'selected');
                $(form).attr('data-address-mode', 'new');
                $(form).find('.shipping-address-block').removeClass('d-none');
                $(form).find('.saveAddressContainer').removeClass('d-none');
            } else if (shipmentUUID === originalUUID) {
                $(form).attr('data-address-mode', 'shipment');
                $(form).find('.saveAddressContainer').addClass('d-none');
            } else if (shipmentUUID.indexOf('ab_') === 0) {
                $(form).attr('data-address-mode', 'details');
                $(form).find('.saveAddressContainer').addClass('d-none');
            } else {
                $(form).attr('data-address-mode', 'edit');
                $(form).find('.saveAddressContainer').addClass('d-none');
            }
        });
    },

    selectMultiShipAddress: function () {
        var baseObj = this;

        $('.multi-shipping .addressSelector').on('change', function () {
            console.log("------------------> selectMultiShipAddress");
            var form = $(this).closest('form');
            var selectedOption = $('option:selected', this);
            var attrs = selectedOption.data();
            var shipmentUUID = selectedOption[0].value;
            var originalUUID = $('input[name=shipmentUUID]', form).val();
            var pliUUID = $('input[name=productLineItemUUID]', form).val();
            var createNewShipmentScoped = baseObj.methods && baseObj.methods.createNewShipment ? baseObj.methods.createNewShipment : createNewShipment;

            var element;
            Object.keys(attrs).forEach(function (attr) {
                if (attr === 'isGift') {
                    $('[name$=' + attr + ']', form).prop('checked', attrs[attr]);
                    $('[name$=' + attr + ']', form).trigger('change');
                } else {
                    element = attr === 'countryCode' ? 'country' : attr;
                    $('[name$=' + element + ']', form).val(attrs[attr]);
                }
            });

            if (shipmentUUID === 'new' && pliUUID) {
                var createShipmentUrl = $(this).attr('data-create-shipment-url');
                createNewShipmentScoped(createShipmentUrl, { productLineItemUUID: pliUUID })
                    .done(function (response) {
                        $.spinner().stop();
                        if (response.error) {
                            if (response.redirectUrl) {
                                window.location.href = response.redirectUrl;
                            }
                            return;
                        }

                        $('body').trigger('checkout:updateCheckoutView',
                            {
                                order: response.order,
                                customer: response.customer,
                                options: { keepOpen: true }
                            }
                        );

                        $(form).attr('data-address-mode', 'new');
                    })
                    .fail(function () {
                        $.spinner().stop();
                    });
            } else if (shipmentUUID === originalUUID) {
                $('select[name$=stateCode]', form).trigger('change');
                $(form).attr('data-address-mode', 'shipment');
            } else if (shipmentUUID.indexOf('ab_') === 0) {
                var url = $(form).attr('action');
                var serializedData = $(form).serialize();
                createNewShipmentScoped(url, serializedData)
                    .done(function (response) {
                        $.spinner().stop();
                        if (response.error) {
                            if (response.redirectUrl) {
                                window.location.href = response.redirectUrl;
                            }
                            return;
                        }

                        $('body').trigger('checkout:updateCheckoutView',
                            {
                                order: response.order,
                                customer: response.customer,
                                options: { keepOpen: true }
                            }
                        );

                        $(form).attr('data-address-mode', 'customer');
                        var $rootEl = $(form).closest('.shipping-content');
                        editMultiShipAddress($rootEl);
                    })
                    .fail(function () {
                        $.spinner().stop();
                    });
            } else {
                var updatePLIShipmentUrl = $(form).attr('action');
                var serializedAddress = $(form).serialize();
                createNewShipmentScoped(updatePLIShipmentUrl, serializedAddress)
                    .done(function (response) {
                        $.spinner().stop();
                        if (response.error) {
                            if (response.redirectUrl) {
                                window.location.href = response.redirectUrl;
                            }
                            return;
                        }

                        $('body').trigger('checkout:updateCheckoutView',
                            {
                                order: response.order,
                                customer: response.customer,
                                options: { keepOpen: true }
                            }
                        );

                        $(form).attr('data-address-mode', 'edit');
                    })
                    .fail(function () {
                        $.spinner().stop();
                    });
            }
        });
    },

    updateShippingList: function () {
        console.log("------------------> updateShippingList");
        var baseObj = this;

        $('select[name$="shippingAddress_addressFields_states_stateCode"]')
            .on('change', function (e) {
                console.log("------------------> updateShippingList");
                if (baseObj.methods && baseObj.methods.updateShippingMethodList) {
                    baseObj.methods.updateShippingMethodList($(e.currentTarget.form));
                } else {
                    updateShippingMethodList($(e.currentTarget.form));
                }
            });
    },

    updateDataAddressMode: function () {
        console.log("------------------> updateDataAddressMode");
        $('body').on('shipping:updateDataAddressMode', function (e, data) {
            console.log("------------------> updateDataAddressMode");
            $(data.form).attr('data-address-mode', data.mode);
        });
    },

    enterMultiShipInfo: function () {
        console.log("------------------> enterMultiShipInfo");
        var baseObj = this;

        $('.btn-enter-multi-ship').on('click', function (e) {
            console.log("------------------> enterMultiShipInfo");
            e.preventDefault();

            if (baseObj.methods && baseObj.methods.editOrEnterMultiShipInfo) {
                baseObj.methods.editOrEnterMultiShipInfo($(this), 'new');
            } else {
                editOrEnterMultiShipInfo($(this), 'new');
            }
        });
    },

    editMultiShipInfo: function () {
        console.log("------------------> editMultiShipInfo");
        var baseObj = this;

        $('.btn-edit-multi-ship').on('click', function (e) {
            console.log("------------------> editMultiShipInfo");
            e.preventDefault();

            if (baseObj.methods && baseObj.methods.editOrEnterMultiShipInfo) {
                baseObj.methods.editOrEnterMultiShipInfo($(this), 'edit');
            } else {
                editOrEnterMultiShipInfo($(this), 'edit');
            }
        });
    },

    saveMultiShipInfo: function () {
        console.log("------------------> saveMultiShipInfo");
        var baseObj = this;

        $('.btn-save-multi-ship').on('click', function (e) {
            console.log("------------------> saveMultiShipInfo");
            e.preventDefault();

            // Save address to checkoutAddressBook
            var form = $(this).closest('form');
            var $rootEl = $(this).closest('.shipping-content');
            var data = $(form).serialize();
            var url = $(form).attr('action');

            $rootEl.spinner().start();
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: data
            })
                .done(function (response) {
                    formHelpers.clearPreviousErrors(form);
                    if (response.error) {
                        if (response.fieldErrors && response.fieldErrors.length) {
                            response.fieldErrors.forEach(function (error) {
                                if (Object.keys(error).length) {
                                    formHelpers.loadFormErrors(form, error);
                                }
                            });
                        } else if (response.serverErrors && response.serverErrors.length) {
                            $.each(response.serverErrors, function (index, element) {
                                createErrorNotification(element);
                            });
                        } else if (response.redirectUrl) {
                            window.location.href = response.redirectUrl;
                        }
                    } else {
                        // Update UI from response
                        $('body').trigger('checkout:updateCheckoutView',
                            {
                                order: response.order,
                                customer: response.customer
                            }
                        );

                        if (baseObj.methods && baseObj.methods.viewMultishipAddress) {
                            baseObj.methods.viewMultishipAddress($rootEl);
                        } else {
                            viewMultishipAddress($rootEl);
                        }
                    }

                    if (response.order && response.order.shippable) {
                        $('button.submit-shipping').attr('disabled', null);
                    }

                    $rootEl.spinner().stop();
                })
                .fail(function (err) {
                    if (err.responseJSON.redirectUrl) {
                        window.location.href = err.responseJSON.redirectUrl;
                    }

                    $rootEl.spinner().stop();
                });

            return false;
        });
    },

    cancelMultiShipAddress: function () {
        console.log("------------------> cancelMultiShipAddress");
        var baseObj = this;

        $('.btn-cancel-multi-ship-address').on('click', function (e) {
            console.log("------------------> cancelMultiShipAddress");
            e.preventDefault();

            var form = $(this).closest('form');
            var $rootEl = $(this).closest('.shipping-content');
            var restoreState = $rootEl.data('saved-state');

            // Should clear out changes / restore previous state
            if (restoreState) {
                var restoreStateObj = JSON.parse(restoreState);
                var originalStateCode = restoreStateObj.shippingAddress.stateCode;
                var stateCode = $('[name$=_stateCode]', form).val();

                if (baseObj.methods && baseObj.methods.updateShippingAddressFormValues) {
                    baseObj.methods.updateShippingAddressFormValues(restoreStateObj);
                } else {
                    updateShippingAddressFormValues(restoreStateObj);
                }

                if (stateCode !== originalStateCode) {
                    $('[data-action=save]', form).trigger('click');
                } else {
                    $(form).attr('data-address-mode', 'edit');
                    if (baseObj.methods && baseObj.methods.editMultiShipAddress) {
                        baseObj.methods.editMultiShipAddress($rootEl);
                    } else {
                        editMultiShipAddress($rootEl);
                    }
                }
            }

            return false;
        });
    },

    isGift: function () {
        console.log("------------------> isGift");
        $('.gift').on('change', function (e) {
            console.log("------------------> isGift");
            e.preventDefault();
            var form = $(this).closest('form');

            if (this.checked) {
                $(form).find('.gift-message').removeClass('d-none');
            } else {
                $(form).find('.gift-message').addClass('d-none');
                $(form).find('.gift-message').val('');
            }
        });
    },
    saveAddressCheck: function(){
        $('.saveAddressCheck').on('change', function (e) {
            e.preventDefault();
            var form = $(this).closest('form');
            if(this.checked){
                //$(form).find('.addressIdContainer');
                $(form).find('.addressIdContainer').show();
                //$("#shippingAddressIddefault").attr('required',true);
                //$("#shippingAddressIddefault").attr('aria-required',true);
                console.log("guardar direccion");
            }else{
                $(form).find('.addressIdContainer').hide();
                //$(form).find('.addressIdContainer').addClass('d-none');
                //$("#shippingAddressIddefault").removeAttr('required');
                //$("#shippingAddressIddefault").removeAttr('aria-required');
                console.log("no guardar direccion");
            }
        });
    },
    initForm: function(){
        initSaveAddresContainer();
    }

};


/***/ }),

/***/ "./node_modules/cleave.js/dist/cleave-esm.js":
/*!***************************************************!*\
  !*** ./node_modules/cleave.js/dist/cleave-esm.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {var commonjsGlobal = typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

var NumeralFormatter = function (numeralDecimalMark,
                                 numeralIntegerScale,
                                 numeralDecimalScale,
                                 numeralThousandsGroupStyle,
                                 numeralPositiveOnly,
                                 stripLeadingZeroes,
                                 prefix,
                                 signBeforePrefix,
                                 tailPrefix,
                                 delimiter) {
    var owner = this;

    owner.numeralDecimalMark = numeralDecimalMark || '.';
    owner.numeralIntegerScale = numeralIntegerScale > 0 ? numeralIntegerScale : 0;
    owner.numeralDecimalScale = numeralDecimalScale >= 0 ? numeralDecimalScale : 2;
    owner.numeralThousandsGroupStyle = numeralThousandsGroupStyle || NumeralFormatter.groupStyle.thousand;
    owner.numeralPositiveOnly = !!numeralPositiveOnly;
    owner.stripLeadingZeroes = stripLeadingZeroes !== false;
    owner.prefix = (prefix || prefix === '') ? prefix : '';
    owner.signBeforePrefix = !!signBeforePrefix;
    owner.tailPrefix = !!tailPrefix;
    owner.delimiter = (delimiter || delimiter === '') ? delimiter : ',';
    owner.delimiterRE = delimiter ? new RegExp('\\' + delimiter, 'g') : '';
};

NumeralFormatter.groupStyle = {
    thousand: 'thousand',
    lakh:     'lakh',
    wan:      'wan',
    none:     'none'    
};

NumeralFormatter.prototype = {
    getRawValue: function (value) {
        return value.replace(this.delimiterRE, '').replace(this.numeralDecimalMark, '.');
    },

    format: function (value) {
        var owner = this, parts, partSign, partSignAndPrefix, partInteger, partDecimal = '';

        // strip alphabet letters
        value = value.replace(/[A-Za-z]/g, '')
            // replace the first decimal mark with reserved placeholder
            .replace(owner.numeralDecimalMark, 'M')

            // strip non numeric letters except minus and "M"
            // this is to ensure prefix has been stripped
            .replace(/[^\dM-]/g, '')

            // replace the leading minus with reserved placeholder
            .replace(/^\-/, 'N')

            // strip the other minus sign (if present)
            .replace(/\-/g, '')

            // replace the minus sign (if present)
            .replace('N', owner.numeralPositiveOnly ? '' : '-')

            // replace decimal mark
            .replace('M', owner.numeralDecimalMark);

        // strip any leading zeros
        if (owner.stripLeadingZeroes) {
            value = value.replace(/^(-)?0+(?=\d)/, '$1');
        }

        partSign = value.slice(0, 1) === '-' ? '-' : '';
        if (typeof owner.prefix != 'undefined') {
            if (owner.signBeforePrefix) {
                partSignAndPrefix = partSign + owner.prefix;
            } else {
                partSignAndPrefix = owner.prefix + partSign;
            }
        } else {
            partSignAndPrefix = partSign;
        }
        
        partInteger = value;

        if (value.indexOf(owner.numeralDecimalMark) >= 0) {
            parts = value.split(owner.numeralDecimalMark);
            partInteger = parts[0];
            partDecimal = owner.numeralDecimalMark + parts[1].slice(0, owner.numeralDecimalScale);
        }

        if(partSign === '-') {
            partInteger = partInteger.slice(1);
        }

        if (owner.numeralIntegerScale > 0) {
          partInteger = partInteger.slice(0, owner.numeralIntegerScale);
        }

        switch (owner.numeralThousandsGroupStyle) {
        case NumeralFormatter.groupStyle.lakh:
            partInteger = partInteger.replace(/(\d)(?=(\d\d)+\d$)/g, '$1' + owner.delimiter);

            break;

        case NumeralFormatter.groupStyle.wan:
            partInteger = partInteger.replace(/(\d)(?=(\d{4})+$)/g, '$1' + owner.delimiter);

            break;

        case NumeralFormatter.groupStyle.thousand:
            partInteger = partInteger.replace(/(\d)(?=(\d{3})+$)/g, '$1' + owner.delimiter);

            break;
        }

        if (owner.tailPrefix) {
            return partSign + partInteger.toString() + (owner.numeralDecimalScale > 0 ? partDecimal.toString() : '') + owner.prefix;
        }

        return partSignAndPrefix + partInteger.toString() + (owner.numeralDecimalScale > 0 ? partDecimal.toString() : '');
    }
};

var NumeralFormatter_1 = NumeralFormatter;

var DateFormatter = function (datePattern, dateMin, dateMax) {
    var owner = this;

    owner.date = [];
    owner.blocks = [];
    owner.datePattern = datePattern;
    owner.dateMin = dateMin
      .split('-')
      .reverse()
      .map(function(x) {
        return parseInt(x, 10);
      });
    if (owner.dateMin.length === 2) owner.dateMin.unshift(0);

    owner.dateMax = dateMax
      .split('-')
      .reverse()
      .map(function(x) {
        return parseInt(x, 10);
      });
    if (owner.dateMax.length === 2) owner.dateMax.unshift(0);
    
    owner.initBlocks();
};

DateFormatter.prototype = {
    initBlocks: function () {
        var owner = this;
        owner.datePattern.forEach(function (value) {
            if (value === 'Y') {
                owner.blocks.push(4);
            } else {
                owner.blocks.push(2);
            }
        });
    },

    getISOFormatDate: function () {
        var owner = this,
            date = owner.date;

        return date[2] ? (
            date[2] + '-' + owner.addLeadingZero(date[1]) + '-' + owner.addLeadingZero(date[0])
        ) : '';
    },

    getBlocks: function () {
        return this.blocks;
    },

    getValidatedDate: function (value) {
        var owner = this, result = '';

        value = value.replace(/[^\d]/g, '');

        owner.blocks.forEach(function (length, index) {
            if (value.length > 0) {
                var sub = value.slice(0, length),
                    sub0 = sub.slice(0, 1),
                    rest = value.slice(length);

                switch (owner.datePattern[index]) {
                case 'd':
                    if (sub === '00') {
                        sub = '01';
                    } else if (parseInt(sub0, 10) > 3) {
                        sub = '0' + sub0;
                    } else if (parseInt(sub, 10) > 31) {
                        sub = '31';
                    }

                    break;

                case 'm':
                    if (sub === '00') {
                        sub = '01';
                    } else if (parseInt(sub0, 10) > 1) {
                        sub = '0' + sub0;
                    } else if (parseInt(sub, 10) > 12) {
                        sub = '12';
                    }

                    break;
                }

                result += sub;

                // update remaining string
                value = rest;
            }
        });

        return this.getFixedDateString(result);
    },

    getFixedDateString: function (value) {
        var owner = this, datePattern = owner.datePattern, date = [],
            dayIndex = 0, monthIndex = 0, yearIndex = 0,
            dayStartIndex = 0, monthStartIndex = 0, yearStartIndex = 0,
            day, month, year, fullYearDone = false;

        // mm-dd || dd-mm
        if (value.length === 4 && datePattern[0].toLowerCase() !== 'y' && datePattern[1].toLowerCase() !== 'y') {
            dayStartIndex = datePattern[0] === 'd' ? 0 : 2;
            monthStartIndex = 2 - dayStartIndex;
            day = parseInt(value.slice(dayStartIndex, dayStartIndex + 2), 10);
            month = parseInt(value.slice(monthStartIndex, monthStartIndex + 2), 10);

            date = this.getFixedDate(day, month, 0);
        }

        // yyyy-mm-dd || yyyy-dd-mm || mm-dd-yyyy || dd-mm-yyyy || dd-yyyy-mm || mm-yyyy-dd
        if (value.length === 8) {
            datePattern.forEach(function (type, index) {
                switch (type) {
                case 'd':
                    dayIndex = index;
                    break;
                case 'm':
                    monthIndex = index;
                    break;
                default:
                    yearIndex = index;
                    break;
                }
            });

            yearStartIndex = yearIndex * 2;
            dayStartIndex = (dayIndex <= yearIndex) ? dayIndex * 2 : (dayIndex * 2 + 2);
            monthStartIndex = (monthIndex <= yearIndex) ? monthIndex * 2 : (monthIndex * 2 + 2);

            day = parseInt(value.slice(dayStartIndex, dayStartIndex + 2), 10);
            month = parseInt(value.slice(monthStartIndex, monthStartIndex + 2), 10);
            year = parseInt(value.slice(yearStartIndex, yearStartIndex + 4), 10);

            fullYearDone = value.slice(yearStartIndex, yearStartIndex + 4).length === 4;

            date = this.getFixedDate(day, month, year);
        }

        // mm-yy || yy-mm
        if (value.length === 4 && (datePattern[0] === 'y' || datePattern[1] === 'y')) {
            monthStartIndex = datePattern[0] === 'm' ? 0 : 2;
            yearStartIndex = 2 - monthStartIndex;
            month = parseInt(value.slice(monthStartIndex, monthStartIndex + 2), 10);
            year = parseInt(value.slice(yearStartIndex, yearStartIndex + 2), 10);

            fullYearDone = value.slice(yearStartIndex, yearStartIndex + 2).length === 2;

            date = [0, month, year];
        }

        // mm-yyyy || yyyy-mm
        if (value.length === 6 && (datePattern[0] === 'Y' || datePattern[1] === 'Y')) {
            monthStartIndex = datePattern[0] === 'm' ? 0 : 4;
            yearStartIndex = 2 - 0.5 * monthStartIndex;
            month = parseInt(value.slice(monthStartIndex, monthStartIndex + 2), 10);
            year = parseInt(value.slice(yearStartIndex, yearStartIndex + 4), 10);

            fullYearDone = value.slice(yearStartIndex, yearStartIndex + 4).length === 4;

            date = [0, month, year];
        }

        date = owner.getRangeFixedDate(date);
        owner.date = date;

        var result = date.length === 0 ? value : datePattern.reduce(function (previous, current) {
            switch (current) {
            case 'd':
                return previous + (date[0] === 0 ? '' : owner.addLeadingZero(date[0]));
            case 'm':
                return previous + (date[1] === 0 ? '' : owner.addLeadingZero(date[1]));
            case 'y':
                return previous + (fullYearDone ? owner.addLeadingZeroForYear(date[2], false) : '');
            case 'Y':
                return previous + (fullYearDone ? owner.addLeadingZeroForYear(date[2], true) : '');
            }
        }, '');

        return result;
    },

    getRangeFixedDate: function (date) {
        var owner = this,
            datePattern = owner.datePattern,
            dateMin = owner.dateMin || [],
            dateMax = owner.dateMax || [];

        if (!date.length || (dateMin.length < 3 && dateMax.length < 3)) return date;

        if (
          datePattern.find(function(x) {
            return x.toLowerCase() === 'y';
          }) &&
          date[2] === 0
        ) return date;

        if (dateMax.length && (dateMax[2] < date[2] || (
          dateMax[2] === date[2] && (dateMax[1] < date[1] || (
            dateMax[1] === date[1] && dateMax[0] < date[0]
          ))
        ))) return dateMax;

        if (dateMin.length && (dateMin[2] > date[2] || (
          dateMin[2] === date[2] && (dateMin[1] > date[1] || (
            dateMin[1] === date[1] && dateMin[0] > date[0]
          ))
        ))) return dateMin;

        return date;
    },

    getFixedDate: function (day, month, year) {
        day = Math.min(day, 31);
        month = Math.min(month, 12);
        year = parseInt((year || 0), 10);

        if ((month < 7 && month % 2 === 0) || (month > 8 && month % 2 === 1)) {
            day = Math.min(day, month === 2 ? (this.isLeapYear(year) ? 29 : 28) : 30);
        }

        return [day, month, year];
    },

    isLeapYear: function (year) {
        return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
    },

    addLeadingZero: function (number) {
        return (number < 10 ? '0' : '') + number;
    },

    addLeadingZeroForYear: function (number, fullYearMode) {
        if (fullYearMode) {
            return (number < 10 ? '000' : (number < 100 ? '00' : (number < 1000 ? '0' : ''))) + number;
        }

        return (number < 10 ? '0' : '') + number;
    }
};

var DateFormatter_1 = DateFormatter;

var TimeFormatter = function (timePattern, timeFormat) {
    var owner = this;

    owner.time = [];
    owner.blocks = [];
    owner.timePattern = timePattern;
    owner.timeFormat = timeFormat;
    owner.initBlocks();
};

TimeFormatter.prototype = {
    initBlocks: function () {
        var owner = this;
        owner.timePattern.forEach(function () {
            owner.blocks.push(2);
        });
    },

    getISOFormatTime: function () {
        var owner = this,
            time = owner.time;

        return time[2] ? (
            owner.addLeadingZero(time[0]) + ':' + owner.addLeadingZero(time[1]) + ':' + owner.addLeadingZero(time[2])
        ) : '';
    },

    getBlocks: function () {
        return this.blocks;
    },

    getTimeFormatOptions: function () {
        var owner = this;
        if (String(owner.timeFormat) === '12') {
            return {
                maxHourFirstDigit: 1,
                maxHours: 12,
                maxMinutesFirstDigit: 5,
                maxMinutes: 60
            };
        }

        return {
            maxHourFirstDigit: 2,
            maxHours: 23,
            maxMinutesFirstDigit: 5,
            maxMinutes: 60
        };
    },

    getValidatedTime: function (value) {
        var owner = this, result = '';

        value = value.replace(/[^\d]/g, '');

        var timeFormatOptions = owner.getTimeFormatOptions();

        owner.blocks.forEach(function (length, index) {
            if (value.length > 0) {
                var sub = value.slice(0, length),
                    sub0 = sub.slice(0, 1),
                    rest = value.slice(length);

                switch (owner.timePattern[index]) {

                case 'h':
                    if (parseInt(sub0, 10) > timeFormatOptions.maxHourFirstDigit) {
                        sub = '0' + sub0;
                    } else if (parseInt(sub, 10) > timeFormatOptions.maxHours) {
                        sub = timeFormatOptions.maxHours + '';
                    }

                    break;

                case 'm':
                case 's':
                    if (parseInt(sub0, 10) > timeFormatOptions.maxMinutesFirstDigit) {
                        sub = '0' + sub0;
                    } else if (parseInt(sub, 10) > timeFormatOptions.maxMinutes) {
                        sub = timeFormatOptions.maxMinutes + '';
                    }
                    break;
                }

                result += sub;

                // update remaining string
                value = rest;
            }
        });

        return this.getFixedTimeString(result);
    },

    getFixedTimeString: function (value) {
        var owner = this, timePattern = owner.timePattern, time = [],
            secondIndex = 0, minuteIndex = 0, hourIndex = 0,
            secondStartIndex = 0, minuteStartIndex = 0, hourStartIndex = 0,
            second, minute, hour;

        if (value.length === 6) {
            timePattern.forEach(function (type, index) {
                switch (type) {
                case 's':
                    secondIndex = index * 2;
                    break;
                case 'm':
                    minuteIndex = index * 2;
                    break;
                case 'h':
                    hourIndex = index * 2;
                    break;
                }
            });

            hourStartIndex = hourIndex;
            minuteStartIndex = minuteIndex;
            secondStartIndex = secondIndex;

            second = parseInt(value.slice(secondStartIndex, secondStartIndex + 2), 10);
            minute = parseInt(value.slice(minuteStartIndex, minuteStartIndex + 2), 10);
            hour = parseInt(value.slice(hourStartIndex, hourStartIndex + 2), 10);

            time = this.getFixedTime(hour, minute, second);
        }

        if (value.length === 4 && owner.timePattern.indexOf('s') < 0) {
            timePattern.forEach(function (type, index) {
                switch (type) {
                case 'm':
                    minuteIndex = index * 2;
                    break;
                case 'h':
                    hourIndex = index * 2;
                    break;
                }
            });

            hourStartIndex = hourIndex;
            minuteStartIndex = minuteIndex;

            second = 0;
            minute = parseInt(value.slice(minuteStartIndex, minuteStartIndex + 2), 10);
            hour = parseInt(value.slice(hourStartIndex, hourStartIndex + 2), 10);

            time = this.getFixedTime(hour, minute, second);
        }

        owner.time = time;

        return time.length === 0 ? value : timePattern.reduce(function (previous, current) {
            switch (current) {
            case 's':
                return previous + owner.addLeadingZero(time[2]);
            case 'm':
                return previous + owner.addLeadingZero(time[1]);
            case 'h':
                return previous + owner.addLeadingZero(time[0]);
            }
        }, '');
    },

    getFixedTime: function (hour, minute, second) {
        second = Math.min(parseInt(second || 0, 10), 60);
        minute = Math.min(minute, 60);
        hour = Math.min(hour, 60);

        return [hour, minute, second];
    },

    addLeadingZero: function (number) {
        return (number < 10 ? '0' : '') + number;
    }
};

var TimeFormatter_1 = TimeFormatter;

var PhoneFormatter = function (formatter, delimiter) {
    var owner = this;

    owner.delimiter = (delimiter || delimiter === '') ? delimiter : ' ';
    owner.delimiterRE = delimiter ? new RegExp('\\' + delimiter, 'g') : '';

    owner.formatter = formatter;
};

PhoneFormatter.prototype = {
    setFormatter: function (formatter) {
        this.formatter = formatter;
    },

    format: function (phoneNumber) {
        var owner = this;

        owner.formatter.clear();

        // only keep number and +
        phoneNumber = phoneNumber.replace(/[^\d+]/g, '');

        // strip non-leading +
        phoneNumber = phoneNumber.replace(/^\+/, 'B').replace(/\+/g, '').replace('B', '+');

        // strip delimiter
        phoneNumber = phoneNumber.replace(owner.delimiterRE, '');

        var result = '', current, validated = false;

        for (var i = 0, iMax = phoneNumber.length; i < iMax; i++) {
            current = owner.formatter.inputDigit(phoneNumber.charAt(i));

            // has ()- or space inside
            if (/[\s()-]/g.test(current)) {
                result = current;

                validated = true;
            } else {
                if (!validated) {
                    result = current;
                }
                // else: over length input
                // it turns to invalid number again
            }
        }

        // strip ()
        // e.g. US: 7161234567 returns (716) 123-4567
        result = result.replace(/[()]/g, '');
        // replace library delimiter with user customized delimiter
        result = result.replace(/[\s-]/g, owner.delimiter);

        return result;
    }
};

var PhoneFormatter_1 = PhoneFormatter;

var CreditCardDetector = {
    blocks: {
        uatp:          [4, 5, 6],
        amex:          [4, 6, 5],
        diners:        [4, 6, 4],
        discover:      [4, 4, 4, 4],
        mastercard:    [4, 4, 4, 4],
        dankort:       [4, 4, 4, 4],
        instapayment:  [4, 4, 4, 4],
        jcb15:         [4, 6, 5],
        jcb:           [4, 4, 4, 4],
        maestro:       [4, 4, 4, 4],
        visa:          [4, 4, 4, 4],
        mir:           [4, 4, 4, 4],
        unionPay:      [4, 4, 4, 4],
        general:       [4, 4, 4, 4]
    },

    re: {
        // starts with 1; 15 digits, not starts with 1800 (jcb card)
        uatp: /^(?!1800)1\d{0,14}/,

        // starts with 34/37; 15 digits
        amex: /^3[47]\d{0,13}/,

        // starts with 6011/65/644-649; 16 digits
        discover: /^(?:6011|65\d{0,2}|64[4-9]\d?)\d{0,12}/,

        // starts with 300-305/309 or 36/38/39; 14 digits
        diners: /^3(?:0([0-5]|9)|[689]\d?)\d{0,11}/,

        // starts with 51-55/2221–2720; 16 digits
        mastercard: /^(5[1-5]\d{0,2}|22[2-9]\d{0,1}|2[3-7]\d{0,2})\d{0,12}/,

        // starts with 5019/4175/4571; 16 digits
        dankort: /^(5019|4175|4571)\d{0,12}/,

        // starts with 637-639; 16 digits
        instapayment: /^63[7-9]\d{0,13}/,

        // starts with 2131/1800; 15 digits
        jcb15: /^(?:2131|1800)\d{0,11}/,

        // starts with 2131/1800/35; 16 digits
        jcb: /^(?:35\d{0,2})\d{0,12}/,

        // starts with 50/56-58/6304/67; 16 digits
        maestro: /^(?:5[0678]\d{0,2}|6304|67\d{0,2})\d{0,12}/,

        // starts with 22; 16 digits
        mir: /^220[0-4]\d{0,12}/,

        // starts with 4; 16 digits
        visa: /^4\d{0,15}/,

        // starts with 62/81; 16 digits
        unionPay: /^(62|81)\d{0,14}/
    },

    getStrictBlocks: function (block) {
      var total = block.reduce(function (prev, current) {
        return prev + current;
      }, 0);

      return block.concat(19 - total);
    },

    getInfo: function (value, strictMode) {
        var blocks = CreditCardDetector.blocks,
            re = CreditCardDetector.re;

        // Some credit card can have up to 19 digits number.
        // Set strictMode to true will remove the 16 max-length restrain,
        // however, I never found any website validate card number like
        // this, hence probably you don't want to enable this option.
        strictMode = !!strictMode;

        for (var key in re) {
            if (re[key].test(value)) {
                var matchedBlocks = blocks[key];
                return {
                    type: key,
                    blocks: strictMode ? this.getStrictBlocks(matchedBlocks) : matchedBlocks
                };
            }
        }

        return {
            type: 'unknown',
            blocks: strictMode ? this.getStrictBlocks(blocks.general) : blocks.general
        };
    }
};

var CreditCardDetector_1 = CreditCardDetector;

var Util = {
    noop: function () {
    },

    strip: function (value, re) {
        return value.replace(re, '');
    },

    getPostDelimiter: function (value, delimiter, delimiters) {
        // single delimiter
        if (delimiters.length === 0) {
            return value.slice(-delimiter.length) === delimiter ? delimiter : '';
        }

        // multiple delimiters
        var matchedDelimiter = '';
        delimiters.forEach(function (current) {
            if (value.slice(-current.length) === current) {
                matchedDelimiter = current;
            }
        });

        return matchedDelimiter;
    },

    getDelimiterREByDelimiter: function (delimiter) {
        return new RegExp(delimiter.replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1'), 'g');
    },

    getNextCursorPosition: function (prevPos, oldValue, newValue, delimiter, delimiters) {
      // If cursor was at the end of value, just place it back.
      // Because new value could contain additional chars.
      if (oldValue.length === prevPos) {
          return newValue.length;
      }

      return prevPos + this.getPositionOffset(prevPos, oldValue, newValue, delimiter ,delimiters);
    },

    getPositionOffset: function (prevPos, oldValue, newValue, delimiter, delimiters) {
        var oldRawValue, newRawValue, lengthOffset;

        oldRawValue = this.stripDelimiters(oldValue.slice(0, prevPos), delimiter, delimiters);
        newRawValue = this.stripDelimiters(newValue.slice(0, prevPos), delimiter, delimiters);
        lengthOffset = oldRawValue.length - newRawValue.length;

        return (lengthOffset !== 0) ? (lengthOffset / Math.abs(lengthOffset)) : 0;
    },

    stripDelimiters: function (value, delimiter, delimiters) {
        var owner = this;

        // single delimiter
        if (delimiters.length === 0) {
            var delimiterRE = delimiter ? owner.getDelimiterREByDelimiter(delimiter) : '';

            return value.replace(delimiterRE, '');
        }

        // multiple delimiters
        delimiters.forEach(function (current) {
            current.split('').forEach(function (letter) {
                value = value.replace(owner.getDelimiterREByDelimiter(letter), '');
            });
        });

        return value;
    },

    headStr: function (str, length) {
        return str.slice(0, length);
    },

    getMaxLength: function (blocks) {
        return blocks.reduce(function (previous, current) {
            return previous + current;
        }, 0);
    },

    // strip prefix
    // Before type  |   After type    |     Return value
    // PEFIX-...    |   PEFIX-...     |     ''
    // PREFIX-123   |   PEFIX-123     |     123
    // PREFIX-123   |   PREFIX-23     |     23
    // PREFIX-123   |   PREFIX-1234   |     1234
    getPrefixStrippedValue: function (value, prefix, prefixLength, prevResult, delimiter, delimiters, noImmediatePrefix, tailPrefix, signBeforePrefix) {
        // No prefix
        if (prefixLength === 0) {
          return value;
        }

        // Value is prefix
        if (value === prefix && value !== '') {
          return '';
        }

        if (signBeforePrefix && (value.slice(0, 1) == '-')) {
            var prev = (prevResult.slice(0, 1) == '-') ? prevResult.slice(1) : prevResult;
            return '-' + this.getPrefixStrippedValue(value.slice(1), prefix, prefixLength, prev, delimiter, delimiters, noImmediatePrefix, tailPrefix, signBeforePrefix);
        }

        // Pre result prefix string does not match pre-defined prefix
        if (prevResult.slice(0, prefixLength) !== prefix && !tailPrefix) {
            // Check if the first time user entered something
            if (noImmediatePrefix && !prevResult && value) return value;
            return '';
        } else if (prevResult.slice(-prefixLength) !== prefix && tailPrefix) {
            // Check if the first time user entered something
            if (noImmediatePrefix && !prevResult && value) return value;
            return '';
        }

        var prevValue = this.stripDelimiters(prevResult, delimiter, delimiters);

        // New value has issue, someone typed in between prefix letters
        // Revert to pre value
        if (value.slice(0, prefixLength) !== prefix && !tailPrefix) {
            return prevValue.slice(prefixLength);
        } else if (value.slice(-prefixLength) !== prefix && tailPrefix) {
            return prevValue.slice(0, -prefixLength - 1);
        }

        // No issue, strip prefix for new value
        return tailPrefix ? value.slice(0, -prefixLength) : value.slice(prefixLength);
    },

    getFirstDiffIndex: function (prev, current) {
        var index = 0;

        while (prev.charAt(index) === current.charAt(index)) {
            if (prev.charAt(index++) === '') {
                return -1;
            }
        }

        return index;
    },

    getFormattedValue: function (value, blocks, blocksLength, delimiter, delimiters, delimiterLazyShow) {
        var result = '',
            multipleDelimiters = delimiters.length > 0,
            currentDelimiter = '';

        // no options, normal input
        if (blocksLength === 0) {
            return value;
        }

        blocks.forEach(function (length, index) {
            if (value.length > 0) {
                var sub = value.slice(0, length),
                    rest = value.slice(length);

                if (multipleDelimiters) {
                    currentDelimiter = delimiters[delimiterLazyShow ? (index - 1) : index] || currentDelimiter;
                } else {
                    currentDelimiter = delimiter;
                }

                if (delimiterLazyShow) {
                    if (index > 0) {
                        result += currentDelimiter;
                    }

                    result += sub;
                } else {
                    result += sub;

                    if (sub.length === length && index < blocksLength - 1) {
                        result += currentDelimiter;
                    }
                }

                // update remaining string
                value = rest;
            }
        });

        return result;
    },

    // move cursor to the end
    // the first time user focuses on an input with prefix
    fixPrefixCursor: function (el, prefix, delimiter, delimiters) {
        if (!el) {
            return;
        }

        var val = el.value,
            appendix = delimiter || (delimiters[0] || ' ');

        if (!el.setSelectionRange || !prefix || (prefix.length + appendix.length) <= val.length) {
            return;
        }

        var len = val.length * 2;

        // set timeout to avoid blink
        setTimeout(function () {
            el.setSelectionRange(len, len);
        }, 1);
    },

    // Check if input field is fully selected
    checkFullSelection: function(value) {
      try {
        var selection = window.getSelection() || document.getSelection() || {};
        return selection.toString().length === value.length;
      } catch (ex) {
        // Ignore
      }

      return false;
    },

    setSelection: function (element, position, doc) {
        if (element !== this.getActiveElement(doc)) {
            return;
        }

        // cursor is already in the end
        if (element && element.value.length <= position) {
          return;
        }

        if (element.createTextRange) {
            var range = element.createTextRange();

            range.move('character', position);
            range.select();
        } else {
            try {
                element.setSelectionRange(position, position);
            } catch (e) {
                // eslint-disable-next-line
                console.warn('The input element type does not support selection');
            }
        }
    },

    getActiveElement: function(parent) {
        var activeElement = parent.activeElement;
        if (activeElement && activeElement.shadowRoot) {
            return this.getActiveElement(activeElement.shadowRoot);
        }
        return activeElement;
    },

    isAndroid: function () {
        return navigator && /android/i.test(navigator.userAgent);
    },

    // On Android chrome, the keyup and keydown events
    // always return key code 229 as a composition that
    // buffers the user’s keystrokes
    // see https://github.com/nosir/cleave.js/issues/147
    isAndroidBackspaceKeydown: function (lastInputValue, currentInputValue) {
        if (!this.isAndroid() || !lastInputValue || !currentInputValue) {
            return false;
        }

        return currentInputValue === lastInputValue.slice(0, -1);
    }
};

var Util_1 = Util;

/**
 * Props Assignment
 *
 * Separate this, so react module can share the usage
 */
var DefaultProperties = {
    // Maybe change to object-assign
    // for now just keep it as simple
    assign: function (target, opts) {
        target = target || {};
        opts = opts || {};

        // credit card
        target.creditCard = !!opts.creditCard;
        target.creditCardStrictMode = !!opts.creditCardStrictMode;
        target.creditCardType = '';
        target.onCreditCardTypeChanged = opts.onCreditCardTypeChanged || (function () {});

        // phone
        target.phone = !!opts.phone;
        target.phoneRegionCode = opts.phoneRegionCode || 'AU';
        target.phoneFormatter = {};

        // time
        target.time = !!opts.time;
        target.timePattern = opts.timePattern || ['h', 'm', 's'];
        target.timeFormat = opts.timeFormat || '24';
        target.timeFormatter = {};

        // date
        target.date = !!opts.date;
        target.datePattern = opts.datePattern || ['d', 'm', 'Y'];
        target.dateMin = opts.dateMin || '';
        target.dateMax = opts.dateMax || '';
        target.dateFormatter = {};

        // numeral
        target.numeral = !!opts.numeral;
        target.numeralIntegerScale = opts.numeralIntegerScale > 0 ? opts.numeralIntegerScale : 0;
        target.numeralDecimalScale = opts.numeralDecimalScale >= 0 ? opts.numeralDecimalScale : 2;
        target.numeralDecimalMark = opts.numeralDecimalMark || '.';
        target.numeralThousandsGroupStyle = opts.numeralThousandsGroupStyle || 'thousand';
        target.numeralPositiveOnly = !!opts.numeralPositiveOnly;
        target.stripLeadingZeroes = opts.stripLeadingZeroes !== false;
        target.signBeforePrefix = !!opts.signBeforePrefix;
        target.tailPrefix = !!opts.tailPrefix;

        // others
        target.swapHiddenInput = !!opts.swapHiddenInput;
        
        target.numericOnly = target.creditCard || target.date || !!opts.numericOnly;

        target.uppercase = !!opts.uppercase;
        target.lowercase = !!opts.lowercase;

        target.prefix = (target.creditCard || target.date) ? '' : (opts.prefix || '');
        target.noImmediatePrefix = !!opts.noImmediatePrefix;
        target.prefixLength = target.prefix.length;
        target.rawValueTrimPrefix = !!opts.rawValueTrimPrefix;
        target.copyDelimiter = !!opts.copyDelimiter;

        target.initValue = (opts.initValue !== undefined && opts.initValue !== null) ? opts.initValue.toString() : '';

        target.delimiter =
            (opts.delimiter || opts.delimiter === '') ? opts.delimiter :
                (opts.date ? '/' :
                    (opts.time ? ':' :
                        (opts.numeral ? ',' :
                            (opts.phone ? ' ' :
                                ' '))));
        target.delimiterLength = target.delimiter.length;
        target.delimiterLazyShow = !!opts.delimiterLazyShow;
        target.delimiters = opts.delimiters || [];

        target.blocks = opts.blocks || [];
        target.blocksLength = target.blocks.length;

        target.root = (typeof commonjsGlobal === 'object' && commonjsGlobal) ? commonjsGlobal : window;
        target.document = opts.document || target.root.document;

        target.maxLength = 0;

        target.backspace = false;
        target.result = '';

        target.onValueChanged = opts.onValueChanged || (function () {});

        return target;
    }
};

var DefaultProperties_1 = DefaultProperties;

/**
 * Construct a new Cleave instance by passing the configuration object
 *
 * @param {String | HTMLElement} element
 * @param {Object} opts
 */
var Cleave = function (element, opts) {
    var owner = this;
    var hasMultipleElements = false;

    if (typeof element === 'string') {
        owner.element = document.querySelector(element);
        hasMultipleElements = document.querySelectorAll(element).length > 1;
    } else {
      if (typeof element.length !== 'undefined' && element.length > 0) {
        owner.element = element[0];
        hasMultipleElements = element.length > 1;
      } else {
        owner.element = element;
      }
    }

    if (!owner.element) {
        throw new Error('[cleave.js] Please check the element');
    }

    if (hasMultipleElements) {
      try {
        // eslint-disable-next-line
        console.warn('[cleave.js] Multiple input fields matched, cleave.js will only take the first one.');
      } catch (e) {
        // Old IE
      }
    }

    opts.initValue = owner.element.value;

    owner.properties = Cleave.DefaultProperties.assign({}, opts);

    owner.init();
};

Cleave.prototype = {
    init: function () {
        var owner = this, pps = owner.properties;

        // no need to use this lib
        if (!pps.numeral && !pps.phone && !pps.creditCard && !pps.time && !pps.date && (pps.blocksLength === 0 && !pps.prefix)) {
            owner.onInput(pps.initValue);

            return;
        }

        pps.maxLength = Cleave.Util.getMaxLength(pps.blocks);

        owner.isAndroid = Cleave.Util.isAndroid();
        owner.lastInputValue = '';
        owner.isBackward = '';

        owner.onChangeListener = owner.onChange.bind(owner);
        owner.onKeyDownListener = owner.onKeyDown.bind(owner);
        owner.onFocusListener = owner.onFocus.bind(owner);
        owner.onCutListener = owner.onCut.bind(owner);
        owner.onCopyListener = owner.onCopy.bind(owner);

        owner.initSwapHiddenInput();

        owner.element.addEventListener('input', owner.onChangeListener);
        owner.element.addEventListener('keydown', owner.onKeyDownListener);
        owner.element.addEventListener('focus', owner.onFocusListener);
        owner.element.addEventListener('cut', owner.onCutListener);
        owner.element.addEventListener('copy', owner.onCopyListener);


        owner.initPhoneFormatter();
        owner.initDateFormatter();
        owner.initTimeFormatter();
        owner.initNumeralFormatter();

        // avoid touch input field if value is null
        // otherwise Firefox will add red box-shadow for <input required />
        if (pps.initValue || (pps.prefix && !pps.noImmediatePrefix)) {
            owner.onInput(pps.initValue);
        }
    },

    initSwapHiddenInput: function () {
        var owner = this, pps = owner.properties;
        if (!pps.swapHiddenInput) return;

        var inputFormatter = owner.element.cloneNode(true);
        owner.element.parentNode.insertBefore(inputFormatter, owner.element);

        owner.elementSwapHidden = owner.element;
        owner.elementSwapHidden.type = 'hidden';

        owner.element = inputFormatter;
        owner.element.id = '';
    },

    initNumeralFormatter: function () {
        var owner = this, pps = owner.properties;

        if (!pps.numeral) {
            return;
        }

        pps.numeralFormatter = new Cleave.NumeralFormatter(
            pps.numeralDecimalMark,
            pps.numeralIntegerScale,
            pps.numeralDecimalScale,
            pps.numeralThousandsGroupStyle,
            pps.numeralPositiveOnly,
            pps.stripLeadingZeroes,
            pps.prefix,
            pps.signBeforePrefix,
            pps.tailPrefix,
            pps.delimiter
        );
    },

    initTimeFormatter: function() {
        var owner = this, pps = owner.properties;

        if (!pps.time) {
            return;
        }

        pps.timeFormatter = new Cleave.TimeFormatter(pps.timePattern, pps.timeFormat);
        pps.blocks = pps.timeFormatter.getBlocks();
        pps.blocksLength = pps.blocks.length;
        pps.maxLength = Cleave.Util.getMaxLength(pps.blocks);
    },

    initDateFormatter: function () {
        var owner = this, pps = owner.properties;

        if (!pps.date) {
            return;
        }

        pps.dateFormatter = new Cleave.DateFormatter(pps.datePattern, pps.dateMin, pps.dateMax);
        pps.blocks = pps.dateFormatter.getBlocks();
        pps.blocksLength = pps.blocks.length;
        pps.maxLength = Cleave.Util.getMaxLength(pps.blocks);
    },

    initPhoneFormatter: function () {
        var owner = this, pps = owner.properties;

        if (!pps.phone) {
            return;
        }

        // Cleave.AsYouTypeFormatter should be provided by
        // external google closure lib
        try {
            pps.phoneFormatter = new Cleave.PhoneFormatter(
                new pps.root.Cleave.AsYouTypeFormatter(pps.phoneRegionCode),
                pps.delimiter
            );
        } catch (ex) {
            throw new Error('[cleave.js] Please include phone-type-formatter.{country}.js lib');
        }
    },

    onKeyDown: function (event) {
        var owner = this,
            charCode = event.which || event.keyCode;

        owner.lastInputValue = owner.element.value;
        owner.isBackward = charCode === 8;
    },

    onChange: function (event) {
        var owner = this, pps = owner.properties,
            Util = Cleave.Util;

        owner.isBackward = owner.isBackward || event.inputType === 'deleteContentBackward';

        var postDelimiter = Util.getPostDelimiter(owner.lastInputValue, pps.delimiter, pps.delimiters);

        if (owner.isBackward && postDelimiter) {
            pps.postDelimiterBackspace = postDelimiter;
        } else {
            pps.postDelimiterBackspace = false;
        }

        this.onInput(this.element.value);
    },

    onFocus: function () {
        var owner = this,
            pps = owner.properties;
        owner.lastInputValue = owner.element.value;

        if (pps.prefix && pps.noImmediatePrefix && !owner.element.value) {
            this.onInput(pps.prefix);
        }

        Cleave.Util.fixPrefixCursor(owner.element, pps.prefix, pps.delimiter, pps.delimiters);
    },

    onCut: function (e) {
        if (!Cleave.Util.checkFullSelection(this.element.value)) return;
        this.copyClipboardData(e);
        this.onInput('');
    },

    onCopy: function (e) {
        if (!Cleave.Util.checkFullSelection(this.element.value)) return;
        this.copyClipboardData(e);
    },

    copyClipboardData: function (e) {
        var owner = this,
            pps = owner.properties,
            Util = Cleave.Util,
            inputValue = owner.element.value,
            textToCopy = '';

        if (!pps.copyDelimiter) {
            textToCopy = Util.stripDelimiters(inputValue, pps.delimiter, pps.delimiters);
        } else {
            textToCopy = inputValue;
        }

        try {
            if (e.clipboardData) {
                e.clipboardData.setData('Text', textToCopy);
            } else {
                window.clipboardData.setData('Text', textToCopy);
            }

            e.preventDefault();
        } catch (ex) {
            //  empty
        }
    },

    onInput: function (value) {
        var owner = this, pps = owner.properties,
            Util = Cleave.Util;

        // case 1: delete one more character "4"
        // 1234*| -> hit backspace -> 123|
        // case 2: last character is not delimiter which is:
        // 12|34* -> hit backspace -> 1|34*
        // note: no need to apply this for numeral mode
        var postDelimiterAfter = Util.getPostDelimiter(value, pps.delimiter, pps.delimiters);
        if (!pps.numeral && pps.postDelimiterBackspace && !postDelimiterAfter) {
            value = Util.headStr(value, value.length - pps.postDelimiterBackspace.length);
        }

        // phone formatter
        if (pps.phone) {
            if (pps.prefix && (!pps.noImmediatePrefix || value.length)) {
                pps.result = pps.prefix + pps.phoneFormatter.format(value).slice(pps.prefix.length);
            } else {
                pps.result = pps.phoneFormatter.format(value);
            }
            owner.updateValueState();

            return;
        }

        // numeral formatter
        if (pps.numeral) {
            // Do not show prefix when noImmediatePrefix is specified
            // This mostly because we need to show user the native input placeholder
            if (pps.prefix && pps.noImmediatePrefix && value.length === 0) {
                pps.result = '';
            } else {
                pps.result = pps.numeralFormatter.format(value);
            }
            owner.updateValueState();

            return;
        }

        // date
        if (pps.date) {
            value = pps.dateFormatter.getValidatedDate(value);
        }

        // time
        if (pps.time) {
            value = pps.timeFormatter.getValidatedTime(value);
        }

        // strip delimiters
        value = Util.stripDelimiters(value, pps.delimiter, pps.delimiters);

        // strip prefix
        value = Util.getPrefixStrippedValue(value, pps.prefix, pps.prefixLength, pps.result, pps.delimiter, pps.delimiters, pps.noImmediatePrefix, pps.tailPrefix, pps.signBeforePrefix);

        // strip non-numeric characters
        value = pps.numericOnly ? Util.strip(value, /[^\d]/g) : value;

        // convert case
        value = pps.uppercase ? value.toUpperCase() : value;
        value = pps.lowercase ? value.toLowerCase() : value;

        // prevent from showing prefix when no immediate option enabled with empty input value
        if (pps.prefix) {
            if (pps.tailPrefix) {
                value = value + pps.prefix;
            } else {
                value = pps.prefix + value;
            }


            // no blocks specified, no need to do formatting
            if (pps.blocksLength === 0) {
                pps.result = value;
                owner.updateValueState();

                return;
            }
        }

        // update credit card props
        if (pps.creditCard) {
            owner.updateCreditCardPropsByValue(value);
        }

        // strip over length characters
        value = Util.headStr(value, pps.maxLength);

        // apply blocks
        pps.result = Util.getFormattedValue(
            value,
            pps.blocks, pps.blocksLength,
            pps.delimiter, pps.delimiters, pps.delimiterLazyShow
        );

        owner.updateValueState();
    },

    updateCreditCardPropsByValue: function (value) {
        var owner = this, pps = owner.properties,
            Util = Cleave.Util,
            creditCardInfo;

        // At least one of the first 4 characters has changed
        if (Util.headStr(pps.result, 4) === Util.headStr(value, 4)) {
            return;
        }

        creditCardInfo = Cleave.CreditCardDetector.getInfo(value, pps.creditCardStrictMode);

        pps.blocks = creditCardInfo.blocks;
        pps.blocksLength = pps.blocks.length;
        pps.maxLength = Util.getMaxLength(pps.blocks);

        // credit card type changed
        if (pps.creditCardType !== creditCardInfo.type) {
            pps.creditCardType = creditCardInfo.type;

            pps.onCreditCardTypeChanged.call(owner, pps.creditCardType);
        }
    },

    updateValueState: function () {
        var owner = this,
            Util = Cleave.Util,
            pps = owner.properties;

        if (!owner.element) {
            return;
        }

        var endPos = owner.element.selectionEnd;
        var oldValue = owner.element.value;
        var newValue = pps.result;

        endPos = Util.getNextCursorPosition(endPos, oldValue, newValue, pps.delimiter, pps.delimiters);

        // fix Android browser type="text" input field
        // cursor not jumping issue
        if (owner.isAndroid) {
            window.setTimeout(function () {
                owner.element.value = newValue;
                Util.setSelection(owner.element, endPos, pps.document, false);
                owner.callOnValueChanged();
            }, 1);

            return;
        }

        owner.element.value = newValue;
        if (pps.swapHiddenInput) owner.elementSwapHidden.value = owner.getRawValue();

        Util.setSelection(owner.element, endPos, pps.document, false);
        owner.callOnValueChanged();
    },

    callOnValueChanged: function () {
        var owner = this,
            pps = owner.properties;

        pps.onValueChanged.call(owner, {
            target: {
                name: owner.element.name,
                value: pps.result,
                rawValue: owner.getRawValue()
            }
        });
    },

    setPhoneRegionCode: function (phoneRegionCode) {
        var owner = this, pps = owner.properties;

        pps.phoneRegionCode = phoneRegionCode;
        owner.initPhoneFormatter();
        owner.onChange();
    },

    setRawValue: function (value) {
        var owner = this, pps = owner.properties;

        value = value !== undefined && value !== null ? value.toString() : '';

        if (pps.numeral) {
            value = value.replace('.', pps.numeralDecimalMark);
        }

        pps.postDelimiterBackspace = false;

        owner.element.value = value;
        owner.onInput(value);
    },

    getRawValue: function () {
        var owner = this,
            pps = owner.properties,
            Util = Cleave.Util,
            rawValue = owner.element.value;

        if (pps.rawValueTrimPrefix) {
            rawValue = Util.getPrefixStrippedValue(rawValue, pps.prefix, pps.prefixLength, pps.result, pps.delimiter, pps.delimiters, pps.noImmediatePrefix, pps.tailPrefix, pps.signBeforePrefix);
        }

        if (pps.numeral) {
            rawValue = pps.numeralFormatter.getRawValue(rawValue);
        } else {
            rawValue = Util.stripDelimiters(rawValue, pps.delimiter, pps.delimiters);
        }

        return rawValue;
    },

    getISOFormatDate: function () {
        var owner = this,
            pps = owner.properties;

        return pps.date ? pps.dateFormatter.getISOFormatDate() : '';
    },

    getISOFormatTime: function () {
        var owner = this,
            pps = owner.properties;

        return pps.time ? pps.timeFormatter.getISOFormatTime() : '';
    },

    getFormattedValue: function () {
        return this.element.value;
    },

    destroy: function () {
        var owner = this;

        owner.element.removeEventListener('input', owner.onChangeListener);
        owner.element.removeEventListener('keydown', owner.onKeyDownListener);
        owner.element.removeEventListener('focus', owner.onFocusListener);
        owner.element.removeEventListener('cut', owner.onCutListener);
        owner.element.removeEventListener('copy', owner.onCopyListener);
    },

    toString: function () {
        return '[Cleave Object]';
    }
};

Cleave.NumeralFormatter = NumeralFormatter_1;
Cleave.DateFormatter = DateFormatter_1;
Cleave.TimeFormatter = TimeFormatter_1;
Cleave.PhoneFormatter = PhoneFormatter_1;
Cleave.CreditCardDetector = CreditCardDetector_1;
Cleave.Util = Util_1;
Cleave.DefaultProperties = DefaultProperties_1;

// for angular directive
((typeof commonjsGlobal === 'object' && commonjsGlobal) ? commonjsGlobal : window)['Cleave'] = Cleave;

// CommonJS
var Cleave_1 = Cleave;

/* harmony default export */ __webpack_exports__["default"] = (Cleave_1);

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ })

/******/ });
//# sourceMappingURL=checkout.js.map