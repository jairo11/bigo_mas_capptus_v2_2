/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./cartridges/int_mercadopago/cartridge/client/default/js/checkout/mercadoPago.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./cartridges/int_mercadopago/cartridge/client/default/js/checkout/mercadoPago.js":
/*!****************************************************************************************!*\
  !*** ./cartridges/int_mercadopago/cartridge/client/default/js/checkout/mercadoPago.js ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/* global jQuery Mercadopago */

(function ($) {
    /**
     * @constructor
     * @classdesc Integration class
     */
    function MercadoPago() {
        var that = this;

        var $content = $(".js-mercadopago-content");
        var $form = $content.find(".js-mp-form");

        var $elements = {
            paymentOptionTab: $(".js-payment-option-tab"),
            paymentTypeButton: $content.find(".js-toggle-payment-type"),
            customerCardsContainer: $content.find(".js-mp-customer-cards"),
            customerCard: $content.find(".js-mp-customer-card")
        };

        // Regular fields
        var fields = {
            cardType: {
                $el: $form.find(".js-mp-card-type"),
                disable: { other: false, stored: false },
                hide: { other: false, stored: true },
                errors: []
            },
            cardHolder: {
                $el: $form.find(".js-mp-card-holder"),
                disable: { other: true, stored: true },
                hide: { other: true, stored: true },
                errors: ["221", "316"]
            },
            cardNumber: {
                $el: $form.find(".js-mp-card-number"),
                disable: { other: true, stored: true },
                hide: { other: true, stored: true },
                errors: ["205", "E301","ECYA301"]
            },
            expirationDate: {
                 $el: $form.find(".js-mp-expiration-date"),
                 disable: { other: true, stored: true },
                 hide: { other: true, stored: true },
                 errors: ["ECYA225","ECYA327", "ECYA427"]
            },
            cardMonth: {
                $el: $form.find(".js-mp-card-month"),
                disable: { other: true, stored: true },
                hide: { other: true, stored: true },
                errors: ["208", "325"]
            },
            cardYear: {
                $el: $form.find(".js-mp-card-year"),
                disable: { other: true, stored: true },
                hide: { other: true, stored: true },
                errors: ["209", "326"]
            },
            securityCode: {
                $el: $form.find(".js-mp-security-code"),
                disable: { other: true, stored: false },
                hide: { other: true, stored: false },
                errors: ["224", "E302", "E203"]
            },
            email: {
                $el: $form.find(".js-mp-email"),
                disable: { other: false, stored: true },
                hide: { other: false, stored: true },
                errors: ["email"]
            },
            phone: {
                $el: $form.find(".js-mp-phone"),
                disable: { other: false, stored: true },
                hide: { other: false, stored: true },
                errors: ["phone"]
            },
            saveCard: {
                $el: $form.find(".js-mp-save-card"),
                disable: { other: true, stored: true },
                hide: { other: true, stored: true },
                errors: []
            },
            useSameMailPhoneAsAddress: {
                $el: $form.find(".js-mp-use-same"),
                disable: { other: false, stored: true },
                hide: { other: false, stored: true },
                errors: []
            }
        };

        // Extended fields
        fields.issuer = {
            $el: $form.find(".js-mp-issuer"),
            disable: { other: true, stored: true },
            hide: { other: true, stored: false },
            errors: ["issuer"]
        };
        fields.financialinstitution = {
            $el: $form.find(".js-mp-financialinstitution"),
            disable: { other: true, stored: true },
            hide: { other: true, stored: true },
            cardTypeID: "pse",
            errors: ["mercadoPagoFinancialinstitution"]
        };
        fields.installments = {
            $el: $form.find(".js-mp-installments"),
            disable: { other: true, stored: false },
            hide: { other: true, stored: false },
            errors: ["installments"]
        };
        fields.docType = {
            $el: $form.find(".js-mp-doc-type"),
            disable: { other: false, stored: false },
            hide: { other: false, stored: false },
            errors: ["212", "322"]
        };
        fields.docNumber = {
            $el: $form.find(".js-mp-doc-number"),
            $wrapper: $form.find(".js-mp-doc-wrapper"),
            $label: $form.find(".js-mp-doc-label"),
            $tooltip: $form.find(".js-mp-doc-tooltip"),
            disable: { other: false, stored: false },
            hide: { other: false, stored: false },
            errors: ["214", "324", "2067"]
        };

        // Hidden fields
        Object.defineProperty(fields, "cardId", {
            enumerable: false,
            value: {
                $el: $form.find(".js-mp-card-id")
            }
        });
        Object.defineProperty(fields, "token", {
            enumerable: false,
            value: {
                $el: $form.find(".js-mp-token")
            }
        });
        Object.defineProperty(fields, "customerId", {
            enumerable: false,
            value: {
                $el: $form.find(".js-mp-customer-id")
            }
        });

        var methods = {
            paymentOption: {
                /**
                 * @function handleChange
                 * @description Handle change of payment method and set initial state of payment tab
                 */
                handleChange: function () {
                    var $activeTab = $(this);
                    var methodId = $activeTab
                        .closest(".js-method-id")
                        .data("methodId");
                    var initialState = $form.data("mpInitial");

                    if (methodId === that.configuration.paymentMethodId) {
                        /*methods.paymentOption.setInitialState[
                            initialState + "Payment"
                        ]();*/
                    }
                },
                setInitialState: {
                    /**
                     * @function newPayment
                     * @description Set initial state for new payment section
                     */
                    newPayment: function () {
                        var paymentMethodInput = fields.cardType.$el.filter(
                            function () {
                                return (
                                    this.value ===
                                    that.configuration.defaultCardType
                                );
                            }
                        );

                        // Check default card type
                        paymentMethodInput.prop("checked", true);
                        methods.card.handleTypeChange.call(
                            paymentMethodInput[0],
                            { data: { handleOther: true } }
                        );
                    },
                    /**
                     * @function storedPayment
                     * @description Set initial state for stored payment section
                     */
                    storedPayment: function () {
                        var firstCard = $elements.customerCard.filter(":first");

                        // Select first card
                        methods.registeredCustomer.selectCustomerCard.call(firstCard[0]);

                        // Toggle payment type to stored
                        $elements.paymentTypeButton.data("togglePaymentType", "stored");
                        methods.registeredCustomer.togglePaymentType.call($elements.paymentTypeButton[0]);
                    },
                    /**
                     * @function restoreStoredPayment
                     * @description Restore stored payment section
                     */
                    restoreStoredPayment: function () {
                        var firstCard = $elements.customerCard.filter(":first");

                        // Select first card
                        methods.registeredCustomer.selectCustomerCard.call(firstCard[0]);

                        // Show and set disabled to false for stored payment fields
                        /* eslint-disable no-restricted-syntax */
                        for (var field in fields) {
                            if (!fields[field].hide.stored) {
                                fields[field].$el
                                    .closest(".js-mp-container")
                                    .removeClass("checkout-hidden");
                            }
                            if (!fields[field].disable.stored) {
                                fields[field].$el.prop("disabled", false);
                            }
                        }
                        /* eslint-enable no-restricted-syntax */
                    }
                }
            },

            token: {
                validateDocNumber: function ($docNumber) {
                    if (
                        $docNumber.attr("required") &&
                        $docNumber.is(":visible") &&
                        $docNumber.is(":enabled")
                    ) {
                        var fieldValueLength = $docNumber.val().length;
                        if (+fieldValueLength === 0) {
                            return "214";
                        } else if (
                            fieldValueLength > $docNumber.attr("maxlength") ||
                            fieldValueLength < $docNumber.attr("minlength")
                        ) {
                            return "324";
                        }
                    }
                    return null;
                },
                validateSecurityCode: function ($securityCode) {
                    if (
                        $securityCode.attr("required") &&
                        $securityCode.is(":visible") &&
                        $securityCode.is(":enabled")
                    ) {
                        var fieldValueLength = $securityCode.val().length;
                        if( fieldValueLength === 0){
                            return  "224"
                        }else  if (fieldValueLength < 3 || fieldValueLength > 4) {
                            return "E302";
                        }
                    }
                    return null;
                },
                validateCardNumber: function ($cardNumber) {
                    if (
                        $cardNumber.attr("required") &&
                        $cardNumber.is(":visible") &&
                        $cardNumber.is(":enabled")
                    ) {
                        if($cardNumber.val().length === 0){
                            return "205"
                        }
                    }
                    return null;
                },
                validateExpirationDate: function ($expirationDate) {
                    if (
                        $expirationDate.attr("required") &&
                        $expirationDate.is(":visible") &&
                        $expirationDate.is(":enabled")
                    ) {
                        if($expirationDate.val().length === 0){
                            return "ECYA225";
                        }else if(!$expirationDate[0].checkValidity()){
                            return "ECYA427";
                        }
                    }
                    return null;
                },
                validateCardHolderName: function ($cardHolderName) {
                    if (
                        $cardHolderName.attr("required") &&
                        $cardHolderName.is(":visible") &&
                        $cardHolderName.is(":enabled")
                    ) {
                        if($cardHolderName.val().length === 0){
                            return "221"
                        }
                    }
                    return null;
                },
                /**
                 * @function populate
                 * @description Create token and populate field with value during submit
                 * @param {Event} event event
                 * @param {Object} eventData event data
                 */
                populate: function (event, eventData) {
                    var validForm = true;
                    // Continue default flow
                    if (eventData && eventData.continue) {
                        return;
                    }
                    // Stop default flow
                    event.stopImmediatePropagation();

                    var isOtherPaymentMethod = fields.cardType.$el.filter(":checked").data("mpCardType") === that.configuration.otherPaymentMethod;
                    var isMercadoPago = $("input[name$=\"billing_paymentMethod\"]:enabled").val() === that.configuration.paymentMethodId;

                    /* eslint-disable no-else-return */
                    if (isMercadoPago) {
                        var $docNumber = fields.docNumber.$el;
                        var docNumberErrorCode = methods.token.validateDocNumber(
                            $docNumber
                        );
                        if (docNumberErrorCode) {
                            methods.token.errorResponse(docNumberErrorCode);
                            validForm = false;
                        } else {
                            $docNumber.next(".invalid-feedback").hide();
                        }
                        var $securityCode = fields.securityCode.$el;
                        var securityCodeErrorCode = methods.token.validateSecurityCode(
                            $securityCode
                        );
                        if (securityCodeErrorCode) {
                            methods.token.errorResponse(securityCodeErrorCode);
                            validForm = false;
                        } else {
                            $securityCode.next(".invalid-feedback").hide();
                        }
                        var $cardNumber = fields.cardNumber.$el;
                        var cardNumberCodeErrorCode = methods.token.validateCardNumber(
                            $cardNumber
                        );
                        if (cardNumberCodeErrorCode) {
                            methods.token.errorResponse(cardNumberCodeErrorCode);
                            validForm = false;
                        } else {
                            $cardNumber.next(".invalid-feedback").hide();
                        }
                        var $expirationDate = fields.expirationDate.$el;
                        var expirationDateErrorCode = methods.token.validateExpirationDate(
                            $expirationDate
                        );
                        if (expirationDateErrorCode) {
                            methods.token.errorResponse(expirationDateErrorCode);
                            validForm = false;
                        } else {
                            $expirationDate.next(".invalid-feedback").hide();
                            if($expirationDate.hasClass('is-invalid')){
                                $expirationDate.removeClass('is-invalid')
                            }
                        }
                        var $cardHolder = fields.cardHolder.$el;
                        var cardHolderErrorCode = methods.token.validateCardHolderName(
                            $cardHolder
                        );
                        if (cardHolderErrorCode) {
                            methods.token.errorResponse(cardHolderErrorCode);
                            validForm = false;
                        } else {
                            $cardHolder.next(".invalid-feedback").hide();
                        }

                    }

                    if (isMercadoPago) {
                        fields.cardMonth.$el.val('');
                        fields.cardYear.$el.val('');
                        var currentYearFirstTwoDigits = new Date().getFullYear().toString().substr(0,2);
                        var arrayDate = fields.expirationDate.$el.val().split('/');;
                        if(arrayDate.length == 2 && arrayDate[0] !== '' && arrayDate[1] !== '') {
                            fields.cardMonth.$el.val(parseInt(arrayDate[0]));
                            fields.cardYear.$el.val(currentYearFirstTwoDigits + arrayDate[1]);
                        }
                    }

                    /* eslint-enable no-else-return */
                    if (isOtherPaymentMethod || !isMercadoPago) {
                        var submitPayment = $(".next-step-button .submit-payment");
                        fields.token.$el.val("");
                        submitPayment.trigger("click", { continue: true });
                        return;
                    }
                    if(!validForm){return;}
                    Mercadopago.createToken($form, function (status, serviceResponse) {
                        Object.keys(fields).forEach(function (index) {
                            var field = fields[index];
                            if (
                                field.$el.attr("required") &&
                                field.$el.is(":visible") &&
                                field.$el.is(":enabled")
                            ) {
                                /* eslint-disable  eqeqeq */
                                if (field.$el.val().length > 0) {
                                    if (index != "securityCode") {
                                        field.$el.next(".invalid-feedback").hide();
                                    }
                                } else if (field.errors && field.errors.indexOf(index) != -1) {
                                    if (index != "securityCode") {
                                        methods.token.errorResponse(index);
                                    }
                                    validForm = false;
                                }
                                /* eslint-enable  eqeqeq */
                            }
                        });
                        if ((status === 200 || status === 201) && validForm) {
                            methods.token.successResponse(serviceResponse);
                        } else {
                            console.log("not success response");
                            if (
                                fields.cardHolder.$el.attr("required") &&
                                fields.cardHolder.$el.is(":visible") &&
                                fields.cardHolder.$el.is(":enabled") &&
                                fields.cardHolder.$el.val().length === 0
                            ) {
                                methods.token.errorResponse(
                                    fields.cardHolder.errors[0]
                                );
                                validForm = false;
                            }

                            if (
                                fields.securityCode.$el.attr("required") &&
                                fields.securityCode.$el.is(":visible") &&
                                fields.securityCode.$el.is(":enabled") &&
                                fields.securityCode.$el.val().length === 0
                            ) {
                                methods.token.errorResponse(
                                    fields.securityCode.errors[0]
                                );
                                validForm = false;
                            } else if (
                                fields.securityCode.$el.attr("required") &&
                                fields.securityCode.$el.is(":visible") &&
                                fields.securityCode.$el.is(":enabled") &&
                                fields.securityCode.$el.val().length > 0
                            ) {
                                /* eslint-disable no-shadow */
                                var $securityCode = fields.securityCode.$el;
                                var securityCodeErrorCode = methods.token.validateSecurityCode(
                                    $securityCode
                                );
                                /* *eslint-enable no-shadow */
                                if(securityCodeErrorCode){
                                    methods.token.errorResponse(
                                        securityCodeErrorCode
                                    );
                                    validForm = false;
                                }
                            }

                            if (
                                fields.installments.$el.attr("required") &&
                                fields.installments.$el.is(":visible") &&
                                fields.installments.$el.is(":enabled") &&
                                fields.installments.$el.val().length === 0
                            ) {
                                methods.token.errorResponse(
                                    fields.installments.errors[0]
                                );
                                validForm = false;
                            }

                            /* eslint-disable no-shadow, block-scoped-var */
                            var docNumberErrorCode = methods.token.validateDocNumber($docNumber);
                            /* eslint-disable no-shadow, block-scoped-var */
                            if (docNumberErrorCode) {
                                methods.token.errorResponse(docNumberErrorCode);
                            }

                            if (serviceResponse.cause) {
                                serviceResponse.cause.forEach(function (cause) {
                                    methods.token.errorResponse(cause.code);
                                });
                            }
                        }
                    });
                },
                /**
                 * @function successResponse
                 * @description Success callback for token creation
                 * @param {Object} serviceResponse service response
                 */
                successResponse: function (serviceResponse) {
                    var submitPayment = $(".next-step-button .submit-payment");
                    /* eslint-disable  array-callback-return */
                    Object.keys(fields).map(function (fieldKey) {
                        var field = fields[fieldKey];
                        field.$el.next(".invalid-feedback").hide();
                    });
                    /* eslint-enable  array-callback-return */

                    fields.token.$el.val(serviceResponse.id);
                    submitPayment.trigger("click", { continue: true });
                },
                /**
                 * @function errorResponse
                 * @description Error callback for token creation
                 * @param {string} errorCode error code
                 */
                errorResponse: function (errorCode) {
                    if(errorCode === '325' || errorCode === '326'){errorCode = 'ECYA327';}
                    var errorMessages = $form.data("mpErrorMessages");
                    var errorField;

                    // Set error code message if found, otherwise set default error message
                    var errorMessage = errorMessages[errorCode]
                        ? errorMessages[errorCode]
                        : errorMessages.default;
                    /* eslint-disable consistent-return */
                    Object.keys(fields).forEach(function (index) {
                        var field = fields[index];
                        if (
                            field.errors &&
                            field.errors.indexOf(errorCode) !== -1
                        ) {
                            errorField = field;
                            return true;
                        }
                    });
                    /* eslint-enable consistent-return */
                    if (errorField) {
                        errorField.$el
                            .next(".invalid-feedback")
                            .focus()
                            .show()
                            .text(errorMessage);
                    } else {
                        $(".error-message").show();
                        $(".error-message-text").text(errorMessage);
                    }
                }
            },

            card: {
                /**
                 * @function handleTypeChange
                 * @description Handle credit card type change
                 * @param {Event} e event
                 */
                handleTypeChange: function (e) {
                    var $el = $(this);
                    var issuerMandatory = $el.data("mpIssuerRequired");
                    var isOtherType =
                        $el.data("mpCardType") ===
                        that.configuration.otherPaymentMethod;
                    var paymentTypeID = $el.val(); // master, visa, pse, boleto
                    var mpPaymentTypeId = $el.data("mpPaymentTypeId"); // credit_card, bank_transfer, ticket

                    // Handle fields for other payment method
                    if (e.data.handleOther) {
                        methods.card.handleOtherType(
                            isOtherType,
                            paymentTypeID
                        );
                    }

                    if (that.preferences.enableInstallments === true && !isOtherType) {
                        methods.installment.set($el.val());

                        // Set issuer info
                        if (issuerMandatory) {
                            methods.issuer.set($el);
                            fields.issuer.$el.prop("disabled", false);
                            fields.issuer.$el
                                .off("change")
                                .on("change", methods.installment.setByIssuerId);
                        } else {
                            fields.issuer.$el.prop("disabled", true);
                        }
                    }
                    $("#mpPaymentTypeId").val(mpPaymentTypeId);
                },
                /**
                 * @function handleOtherType
                 * @description Toggle other payment method
                 * @param {boolean} isOtherType is other type
                 * @param {string} cardTypeID cart type ID
                 */
                handleOtherType: function (isOtherType, cardTypeID) {
                    /* eslint-disable eqeqeq, no-restricted-syntax, guard-for-in */
                    for (var field in fields) {
                        var fieldsField = fields[field];
                        if (fieldsField.hide.other) {
                            fieldsField.$el
                                .closest(".js-mp-container")
                                .toggleClass("checkout-hidden", isOtherType);
                        }
                        if (fieldsField.disable.other) {
                            fieldsField.$el.prop("disabled", isOtherType);
                        }

                        if (fieldsField.cardTypeID) {
                            fieldsField.$el.prop(
                                "disabled",
                                fieldsField.cardTypeID != cardTypeID
                            );
                            fieldsField.$el
                                .closest(".js-mp-container")
                                .toggleClass(
                                    "checkout-hidden",
                                    fieldsField.cardTypeID != cardTypeID
                                );
                        }
                    }
                    /* eslint-enable eqeqeq, no-restricted-syntax, guard-for-in */
                },

                guessingPaymentMethod: function () {
                    console.log("guessingPaymentMethod--->")
                    const cardnumber = document.getElementById(
                        "mercadoPagoCardNumber"
                    ).value;
                    if(!cardnumber.length > 0){return;}
                    var bin = cardnumber.replace(" ", "").substring(0, 6);

                    fields.cardNumber.$el.next(".invalid-feedback").focus().hide();
                    window.Mercadopago.getPaymentMethod(
                        {
                            bin: bin
                        },
                        methods.card.setPaymentMethodInfo
                    );
                },

                setPaymentMethodInfo: function (status, response) {
                    if (+status === 200 && response.length > 0) {
                        var $paymentMethodElement = $(
                            "#cardType-" + response[0].id
                        );
                        console.log($paymentMethodElement);
                        $paymentMethodElement.prop("checked", true);
                        methods.card.handleTypeChange.call(
                            $paymentMethodElement,
                            { data: { handleOther: false } }
                        );
                    }
                },
                /**
                * @function isBradesCard
                 * @description Check if is bradescard bines
                 */
                isBradesCard: function isBradesCard(bin) {
                  var valid = false;
                  var bradescardBines = JSON.parse(that.preferences.bradescardBines);
                  if (bradescardBines.hasOwnProperty("binesRange")) {
                    var binesRange = bradescardBines.binesRange;
                    for (var i = 0; binesRange.length > i; i++) {
                      var bines = binesRange[i].bines;
                      var binFrom = parseInt(bines.substring(0, bines.indexOf('-')), 10);
                      var binTo = parseInt(bines.substring(bines.indexOf('-') + 1, bines.length), 10);
                      if (bin >= binFrom && bin <= binTo) {
                        valid = true;
                      }
                    }
                  }
                  if (bradescardBines.hasOwnProperty("singleBines")) {
                    var singleBines = bradescardBines.singleBines;
                    for (var i = 0; singleBines.length > i; i++) {
                      var singleBin = parseInt(singleBines[i].bin);
                      if (bin == singleBin) {
                        valid = true;
                      }
                    }
                  }
                  return valid;
                },
                errorResponse: function (errorCode) {
                    var errorMessages = $form.data("mpErrorMessages");
                    var errorField;

                    // Set error code message if found, otherwise set default error message
                    var errorMessage = errorMessages[errorCode]
                        ? errorMessages[errorCode]
                        : errorMessages.default;
                    /* eslint-disable consistent-return */
                    /*Object.keys(fields).forEach(function (index) {
                        var field = fields[index];
                        if (
                            field.errors &&
                            field.errors.indexOf(errorCode) !== -1
                        ) {
                            errorField = field;
                            return true;
                        }
                    });*/
                    errorField = fields.cardNumber;
                    /* eslint-enable consistent-return */
                    if (errorField) {
                        errorField.$el
                            .next(".invalid-feedback")
                            .focus()
                            .show()
                            .text(errorMessage);
                    }
                    /* else {
                        $(".error-message").show();
                        $(".error-message-text").text(errorMessage);
                    }*/
                }
            },

            installment: {
                setapi: function(){
                    var creditCard = $(this).val().replaceAll(" ", "");
                    if(creditCard && creditCard.length >= 15){
                        console.log(creditCard);
                        console.log($("input[name$=\"billing_paymentMethod\"]:enabled").val());
                    }

                },
                /**
                 * @function set
                 * @description Set installments
                 * @param {string} paymentMethodId payment method
                 */
                set: function (paymentMethodId) {
                    // Set installments info
                    console.log(paymentMethodId);
                    console.log($form.data("mpCartTotal"));
                    Mercadopago.getInstallments(
                        {
                            payment_method_id: paymentMethodId,
                            amount: $form.data("mpCartTotal")
                        },
                        methods.installment.handleServiceResponse
                    );
                },
                /**
                 * @function handleServiceResponse
                 * @description Callback for installments
                 * @param {number} status status
                 * @param {Array} response response
                 */
                handleServiceResponse: function (status, response) {
                    fields.installments.$el.find("option").remove();

                    if (+status !== 200 && +status !== 201) {
                        return;
                    }

                    var $defaultOption = $(
                        new Option(
                            that.resourceMessages.defaultInstallments,
                            ""
                        )
                    );
                    fields.installments.$el.append($defaultOption);

                    if (response[0].payer_costs) {
                        $.each(response[0].payer_costs, function (i, item) {
                            fields.installments.$el.append(
                                $("<option>", {
                                    value: item.installments,
                                    text:
                                        item.recommended_message ||
                                        item.installments
                                })
                            );

                            if (
                                fields.installments.$el.val() !== "" &&
                                fields.installments.$el.val() ===
                                    item.installments
                            ) {
                                fields.installments.$el.val(item.installments);
                            }
                        });
                    }
                },
                /**
                 * @function handleServiceResponse
                 * @description Set installments using issuer ID
                 */
                setByIssuerId: function () {
                    var issuerId = $(this).val();

                    if (!issuerId || issuerId === "-1") {
                        return;
                    }

                    Mercadopago.getInstallments(
                        {
                            payment_method_id: fields.cardType.$el
                                .filter(":checked")
                                .val(),
                            amount: $form.data("mpCartTotal"),
                            issuer_id: issuerId
                        },
                        methods.installment.handleServiceResponse
                    );
                }
            },

            issuer: {
                /**
                 * @function set
                 * @description Set issuer
                 * @param {jQuery} $element element
                 */
                set: function ($element) {
                    Mercadopago.getIssuers(
                        $element.val(),
                        methods.issuer.handleServiceResponse
                    );
                },
                /**
                 * @function handleServiceResponse
                 * @description Callback for issuer
                 * @param {number} status status
                 * @param {Array} response response
                 */
                handleServiceResponse: function (status, response) {
                    fields.issuer.$el.find("option").remove();

                    if (+status !== 200 && +status !== 201) {
                        return;
                    }

                    var $defaultOption = $(
                        new Option(that.resourceMessages.defaultIssuer, "")
                    );
                    fields.issuer.$el.append($defaultOption);

                    $.each(response, function (i, item) {
                        fields.issuer.$el.append(
                            $("<option>", {
                                value: item.id,
                                text:
                                    item.name !== "default"
                                        ? item.name
                                        : that.configuration.defaultIssuer
                            })
                        );

                        if (
                            fields.issuer.$el.val() !== "" &&
                            fields.issuer.$el.val() === item.id
                        ) {
                            fields.issuer.$el.val(item.id);
                        }
                    });
                }
            },

            docType: {
                /**
                 * @function init
                 * @description Init identification document type
                 */
                init: function () {
                    Mercadopago.getIdentificationTypes(
                        methods.docType.handleServiceResponse
                    );
                },
                /**
                 * @function handleServiceResponse
                 * @description Callback for identification document type
                 * @param {number} status status
                 * @param {Array} response response
                 */
                handleServiceResponse: function (status, response) {
                    fields.docType.$el.find("option").remove();

                    if (+status !== 200 && +status !== 201) {
                        return;
                    }

                    $.each(response, function (i, item) {
                        fields.docType.$el.append(
                            $("<option>", {
                                value: item.id,
                                text: item.name,
                                "data-min-length": item.min_length,
                                "data-max-length": item.max_length
                            })
                        );
                    });

                    fields.docType.$el.trigger("change");
                }
            },

            docNumber: {
                /**
                 * @function setRange
                 * @description Set range identification document number
                 */
                setRange: function () {
                    var $selectedOption = $(this).find("option:selected");
                    var minLength = $selectedOption.data("minLength");
                    var maxLength = $selectedOption.data("maxLength");

                    // Set label
                    var labelSecondPart =
                        $selectedOption.val() === that.configuration.docTypeDNI
                            ? that.resourceMessages.docNumberLabelDNI
                            : that.resourceMessages.docNumberLabelOther;
                    fields.docNumber.$label.text(
                        that.resourceMessages.docNumberLabel +
                            " " +
                            labelSecondPart
                    );

                    // Set range
                    fields.docNumber.$wrapper.addClass("required");
                    fields.docNumber.$el.attr("maxlength", maxLength);
                    fields.docNumber.$el.attr("minlength", minLength);

                    // Set tooltip
                    fields.docNumber.$tooltip.text(
                        that.resourceMessages.docNumberTooltip
                            .replace("{0}", minLength)
                            .replace("{1}", maxLength)
                    );
                }
            },

            registeredCustomer: {
                /**
                 * @function togglePaymentType
                 * @description Toggle payment type (new or stored)
                 * @param {Event} event event
                 */
                togglePaymentType: function (event) {
                    var $el = $(this);
                    var isNew = $el.data("togglePaymentType") === "new";

                    $elements.customerCardsContainer.toggleClass(
                        "checkout-hidden",
                        isNew
                    );

                    // Disable and remove value to properly create token
                    fields.cardId.$el.prop("disabled", isNew);
                    if (isNew) {
                        fields.cardId.$el.val("");
                    }
                    /* eslint-disable eqeqeq, no-restricted-syntax */
                    for (var field in fields) {
                        if (fields[field].hide.stored) {
                            fields[field].$el
                                .closest(".js-mp-container")
                                .toggleClass("checkout-hidden", !isNew);
                        }
                        if (fields[field].disable.stored) {
                            fields[field].$el.prop("disabled", !isNew);
                        }
                    }
                    /* eslint-enable eqeqeq, no-restricted-syntax */

                    // Set initial states
                    if (isNew) {
                        methods.paymentOption.setInitialState.newPayment();
                    }
                    // Only when triggered from event (to avoid recursion)
                    if (event && !isNew) {
                        methods.paymentOption.setInitialState.restoreStoredPayment();
                    }

                    // Change to opposite
                    $el.data("togglePaymentType", isNew ? "stored" : "new");
                    $el.text(
                        $el.data((isNew ? "stored" : "new") + "PaymentText")
                    );
                },
                /**
                 * @function selectCustomerCard
                 * @description Select store credit card
                 */
                selectCustomerCard: function () {
                    var $el = $(this);
                    $elements.customerCard.removeClass("selected-payment");
                    $el.addClass("selected-payment");
                    fields.cardId.$el.val($el.data("mpCustomerCard"));

                    var paymentMethodInput = fields.cardType.$el.filter(
                        function () {
                            return this.value === $el.data("mpMethodId");
                        }
                    );
                    paymentMethodInput.prop("checked", true);
                    methods.card.handleTypeChange.call(paymentMethodInput[0], {
                        data: { handleOther: false }
                    });
                }
            }
        };

        /**
         * @function initSDK
         * @description Init MercadoPago JS SDK by setting public key
         */
        function initSDK() {
            window.Mercadopago.setPublishableKey(that.preferences.publicKey);
        }

        /**
         * @function initAjaxListener
         * @description This function is necessary to listen to PlaceOrder errors and reset the credit-card token
         */
        function initAjaxListener() {
            $(document).ajaxComplete(function (event, xhr) {
                if (xhr && xhr.responseText.indexOf("resetMpToken") >= 0) {
                    if (Object.hasOwnProperty.call(Mercadopago, "tokenId")) {
                        Mercadopago.tokenId = "";
                    }
                    $("input[name*=\"mercadoPagoCreditCard_token\"]").val("");
                }
                if (xhr && xhr.responseText.indexOf("detailedError") >= 0) {
                    try {
                        var response = JSON.parse(xhr.responseText);
                        if (response.detailedError) {
                            $(".payment-summary .edit-button").trigger("click");
                            methods.token.errorResponse(response.detailedError);
                        }
                    } catch (ex) {
                        // do nothing
                    }
                }
            });
        }

        /**
         * @function events
         * @description Init events
         */
        function events() {
            $elements.paymentOptionTab.on(
                "click",
                methods.paymentOption.handleChange
            ); // By click
            fields.cardType.$el.on(
                "change",
                { handleOther: true },
                methods.card.handleTypeChange
            );
            fields.docType.$el.on("change", methods.docNumber.setRange);
            $elements.paymentTypeButton.on(
                "click",
                methods.registeredCustomer.togglePaymentType
            );
            $elements.customerCard.on(
                "click",
                methods.registeredCustomer.selectCustomerCard
            );
            $(".next-step-button .submit-payment").on(
                "click",
                methods.token.populate
            );
            fields.cardNumber.$el.on(
                "focusout",
                methods.card.guessingPaymentMethod
            );
            $('.cardNumber').on(
                "focusout",
                methods.installment.setapi
            );

            $('.cardSelectorMercadoPago').on('change', function (e) {
                console.log("asdasdasd-->")
                var $el = $(this);
                var isMercadoPago = $("input[name$=\"billing_paymentMethod\"]:enabled").val() === that.configuration.paymentMethodId;

                e.preventDefault();

                if(isMercadoPago){
                    fields.cardId.$el.val(e.target.value);
                    fields.customerId.$el.val($el.find(':selected').data('customerId'));
                    //var $el = $(this);
                    var isNew = !e.target.value;

                    $elements.customerCardsContainer.toggleClass(
                        "checkout-hidden",
                        isNew
                    );

                    // Disable and remove value to properly create token
                    fields.cardId.$el.prop("disabled", isNew);
                    if (isNew) {
                        fields.cardId.$el.val("");
                    }
                    /* eslint-disable eqeqeq, no-restricted-syntax */
                    for (var field in fields) {
                        if (fields[field].hide.stored) {
                            fields[field].$el
                                .closest(".js-mp-container")
                                .toggleClass("checkout-hidden", !isNew);
                        }
                        if (fields[field].disable.stored) {
                            fields[field].$el.prop("disabled", !isNew);
                        }
                    }
                    /* eslint-enable eqeqeq, no-restricted-syntax */

                    // Set initial states
                    if (isNew) {
                        methods.paymentOption.setInitialState.newPayment();
                    }
                    if(e.target.value) {
                        var paymentMethodInput = fields.cardType.$el.filter(
                            function () {
                                return this.value === $el.find(':selected')
                                    .data('cardType');
                            }
                        );
                        paymentMethodInput.prop("checked", true);
                        methods.card.handleTypeChange.call(paymentMethodInput[0], {
                            data: { handleOther: false }
                        });
                        $('#mercadoPagoSaveCreditCard').prop('checked', false);
                        $('select[name$="_expirationMonth"]').val('');
                        $('select[name$="_expirationYear"]').val('');
                        $('input[name$="_expirationDate"]').val('');
                        $('input[name$="_cardOwner"]').val('');
                        $('#mercadoPagoCardNumber').val('');
                        $('#mercadoPagoSecurityCode').val('');
                    }
                    $('#mercadoPagoSecurityCode').val('');
                    // Only when triggered from event (to avoid recursion)
                    /*if (event && !isNew) {
                        methods.paymentOption.setInitialState.restoreStoredPayment();
                    }*/

                    // Change to opposite
                    //$el.data("togglePaymentType", isNew ? "stored" : "new");
                    //$el.text(
                    //$el.data((isNew ? "stored" : "new") + "PaymentText")
                    //);
                }

                // if(!isMercadoPago && e.target.value) {
                //     console.log('seleccion con datos');
                //     $('.payment-information').data('is-new-payment', false);
                //
                //     $('.credit-card-form').addClass('checkout-hidden');
                // }else if(!isMercadoPago && !e.target.value){
                //     $('.payment-information').data('is-new-payment', true);
                //
                //     $('.credit-card-form').removeClass('checkout-hidden');
                // }



            });

            $('.cardSelectorPaypal').on('change', function (e) {
                if(e.target.value) {
                    $('#saveCreditCard').prop('checked', false);
                    console.log('seleccion con datos');
                    $('.payment-information').data('is-new-payment', false);
                    $('.credit-card-form').addClass('checkout-hidden');
                    $('.saved-security-code').removeClass('checkout-hidden');
                    $('input[name$="_cardNumber"]').data('cleave').setRawValue('');
                    $('select[name$="_expirationMonth"]').val('');
                    $('select[name$="_expirationYear"]').val('');
                    $('input[name$="_expirationDate"]').val('');
                    $('input[name$="_securityCode"]').val('');
                    $('input[name$="_cardOwner"]').val('');
                }else {
                    $('.payment-information').data('is-new-payment', true);
                    $('.credit-card-form').removeClass('checkout-hidden');
                    $('.saved-security-code').addClass('checkout-hidden');
                    $('#saved-payment-security-code').val('');

                }
            });

            $('.cardSelectorPaypal').val('');
            $('.cardSelectorMercadoPago').val('');

        }

        this.preferences = $form.data("mpPreferences");
        this.resourceMessages = $form.data("mpResourceMessages");
        this.configuration = $form.data("mpConfiguration");

        /**
         * @function init
         * @description Init integration
         */
        this.init = function () {
            if ($content.length > 0) {
                initSDK();
                if (that.preferences.enableDocTypeNumber) {
                    methods.docType.init();
                }
                events();
                methods.paymentOption.handleChange.call(
                    $elements.paymentOptionTab.filter(".enabled")
                ); // Initial
                initAjaxListener();
            }
        };
    }

    $(document).ready(new MercadoPago().init);
}(jQuery));


/***/ })

/******/ });
//# sourceMappingURL=mercadoPago.js.map