"use strict";

var LocalServiceRegistry = require("dw/svc/LocalServiceRegistry");

// Global service componentnts
var svc;
var config;
var url;
var user;
var password;
var credential;

/**
 * Initialize the service components
 */
function initservice() {
    // Create a service object
    svc = LocalServiceRegistry.createService("mulesoft.payment.services", {
        createRequest: function (_svc, args) {
            if (args) {
                return JSON.stringify(args);
            }
            return null;
        },
        parseResponse: function (_svc, client) {
            return client.text;
        },
        filterLogMessage: function (msg) {
            return msg;
        }
    });
    // Configure the service related parameters
    config = svc.getConfiguration();
    credential = config.getCredential();
    user = credential.getUser();
    password = credential.getPassword();
    url = !empty(credential.getURL()) ? credential.getURL() : "";
    // let buff = new Buffer(user+":"+password);
    // let credentiales = buff.toString('base64');
    svc.addHeader("Content-Type", "application/json");
    svc.addHeader("Authorization", "Basic " + require('dw/util/StringUtils').encodeBase64(user + ':' + password));
}

function transactionDebCredit(requestPayload, saveCard) {
    initservice();
    var req = {};
    req= requestPayload;
    if (session.custom.paypalFraudnetSessionId) {
        svc.addHeader("id",session.custom.paypalFraudnetSessionId);
    }
    svc.setRequestMethod("POST");
    url = encodeURI(url);
    url += "v1/payments/card?save_card=" + saveCard;
    svc.setURL(url);
    // service call
    var httpResult = svc.call(req);
    return responseFilter(httpResult);
}

function createCard(requestPayload, customerID, email) {
    initservice();
    var req = {};
    req= requestPayload;
    svc.setRequestMethod("POST");
    url = encodeURI(url);
    url += "v1/payments/card/" + customerID + '?email=' + email;
    svc.setURL(url);
    // service call
    var httpResult = svc.call(req);
    return responseFilter(httpResult);
}

function transactionCash(requestPayload) {
    initservice();
    var req = {};
    req= requestPayload;
    svc.setRequestMethod("POST");
    url = encodeURI(url);
    url += "v1/payments/cash";
    svc.setURL(url);
    // service call
    var httpResult = svc.call(req);
    return responseFilter(httpResult);
}

function transactionBradescard(requestPayload, saveCard) {
    initservice();
    var req = {};
    req= requestPayload;
    svc.setRequestMethod("POST");
    url = encodeURI(url);
    url += "/v1/payments/tokenized_card?save_card=" + saveCard;
    svc.setURL(url);
    // service call
    var httpResult = svc.call(req);
    return responseFilter(httpResult);
}

function deleteCard(customerId, cardId, paymentGateWay, email) {
    initservice();
    var req = {};
    svc.setRequestMethod("DELETE");
    url = encodeURI(url);
    url += '/v1/payments/card/' + customerId + '/' + cardId + '?payment_gateway=' + paymentGateWay + '&email=' + email;
    svc.setURL(url);
    // service call
    var httpResult = svc.call(req);
    return responseFilter(httpResult);
}

function getCards(profile) {
    initservice();
    var req = {};
    svc.setRequestMethod("GET");
    url = encodeURI(url);
    url += "v1/payments/card/"+profile.customerNo+"?email="+profile.email;
    svc.setURL(url);
    // service call
    var httpResult = svc.call(req);
    return responseFilter(httpResult);
}

function getFinancing(requestPayload) {
    initservice();
    var req = {};
    req= requestPayload;
    svc.setRequestMethod("POST");
    url = encodeURI(url);
    url += "v1/payments/card/financing";
    svc.setURL(url);
    // service call
    var httpResult = svc.call(req);
    return responseFilter(httpResult);
}

function transactionRefund(idTransaction, amount) {
    var UUIDUtils = require("dw/util/UUIDUtils");
    initservice();
    var req = {};
    req= amount;
    svc.setRequestMethod("POST");
    url = encodeURI(url);
    url += "v1/payments/"+idTransaction+"/refund";
    let uuid=UUIDUtils.createUUID();
    svc.addHeader("id", uuid);
    svc.setURL(url);
    // service call
    var httpResult = svc.call(req);
    return responseFilter(httpResult);
}

function responseFilter(httpResponse) {
    if (httpResponse.status !== "OK") {
        var errorResult = {
            statusCode: httpResponse.status,
            errorMessage: parseMessage(httpResponse),
            url: url
        };
        return errorResult;
    }
    // return a plain javascript object
    return JSON.parse(httpResponse.object);
}

function parseMessage(httpResponse) {
    var msg = "";
    try {
        msg = JSON.parse(httpResponse.getErrorMessage())
    } catch (error) {
        msg = httpResponse.msg;
    }
    return msg;
}


module.exports = {
    transactionDebCredit: transactionDebCredit,
    transactionCash: transactionCash,
    transactionBradescard: transactionBradescard,
    getFinancing: getFinancing,
    getCards: getCards,
    createCard: createCard,
    deleteCard: deleteCard,
    transactionRefund: transactionRefund
};

