"use strict";

function shippingCostResponse(req) {
    var res ={
        priceId : req.idPrice,
        deliveryType : req.DeliveryType,
        shippingPrice : req.PriceList[0].price
    };
    return res;
}

function returnResponse(req) {
    var res ={
        returnId : req.returnId,
        company : req.company,
        guideNo : req.guideNo,
        pdf : req.pdf
    };
    return res;
}

module.exports = {
    shippingCostResponse : shippingCostResponse,
    returnResponse: returnResponse
};
