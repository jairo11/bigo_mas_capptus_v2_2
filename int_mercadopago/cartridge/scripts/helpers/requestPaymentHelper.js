'use strict';

var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
const Money = require('dw/value/Money');
const TaxMgr = require('dw/order/TaxMgr');

function requestCreditoDebito(order, paymentInstrument) {
    var token = paymentInstrument.custom ? paymentInstrument.custom.tempToken || '' : '';
    var parsedExpirationDate = paymentInstrument.custom.creditCardExpirationDate.toISOString().substr(0, 7);
    var customer = order.customerNo ? order.customerNo : ' ';
    var request = {
        intent: 'CAPTURE',
        payer: {
            email: order.customerEmail,
            name: order.billingAddress.firstName,
            surname: order.billingAddress.lastName,
            birth_date: '',
            phone: {
                phone_type: 'MOBILE',
                national_number: (order.billingAddress.phone).replace(/[^0-9]/g, '')
            },
            address: {
                admin_area_1: order.billingAddress.stateCode,
                address_line_1: order.billingAddress.address1 + ' ' + order.billingAddress.custom.numberExt,
                country_codee: order.billingAddress.countryCode.value,
                postal_code: order.billingAddress.postalCode,
                address_line_2: order.billingAddress.custom.suburb,
                admin_area_2: order.billingAddress.custom.county
            },
            account_id: order.customer.isAnonymous() ? order.getCustomerEmail() : order.getCustomerNo(),
            create_date: order.customer.isAnonymous() ? (new Date()).toISOString() : order.customer.profile.creationDate.toISOString(),
            cd_string_one: order.customer.isAnonymous() ? '0' : '1'
        },
        payment_source: {
            installments: {
                term: '1',
                interval_duration: 'P1M'
            }
        }
    };
    if (!token) {
        request.payment_source.card = {
            billing_address: {
                address_line_1: order.billingAddress.address1 + ' ' + order.billingAddress.custom.numberExt,
                address_line_2: order.billingAddress.custom.suburb,
                admin_area_1: order.billingAddress.stateCode,
                postal_code: order.billingAddress.postalCode,
                country_codee: order.billingAddress.countryCode.value
            },
            name: paymentInstrument.creditCardHolder,
            security_code: paymentInstrument.custom.tempCode,
            number: paymentInstrument.custom.tempNo,
            expiry: parsedExpirationDate,
            id: customer
        };
    } else {
        request.payment_source.tokenId = token;
    }

   /* {
        custom_id: customer,
            amount: {
        value: paymentInstrument.paymentTransaction.amount.value.toString(),
            currency_code: paymentInstrument.paymentTransaction.amount.currencyCode
    },
        shipping: {
            surname: order.shipments[0].shippingAddress.firstName,
                name: order.shipments[0].shippingAddress.lastName,
                address: {
                admin_area_1: order.shipments[0].shippingAddress.stateCode,
                    address_line_1: order.shipments[0].shippingAddress.address1 + ' ' + order.shipments[0].shippingAddress.custom.numberExt,
                    country_codee: order.shipments[0].shippingAddress.countryCode.value,
                    postal_code: order.shipments[0].shippingAddress.postalCode,
                    address_line_2: order.shipments[0].shippingAddress.custom.suburb,
                    admin_area_2: order.shipments[0].shippingAddress.custom.county
            },
            email: order.customerEmail
        }
    }*/

    var purchaseUnits = getPurchaseUnit(order, false);

    purchaseUnits.custom_id = order.orderNo;
    purchaseUnits.shipping= {
        surname: order.shipments[0].shippingAddress.firstName,
            name: order.shipments[0].shippingAddress.lastName,
            address: {
            admin_area_1: order.shipments[0].shippingAddress.stateCode,
                address_line_1: order.shipments[0].shippingAddress.address1 + ' ' + order.shipments[0].shippingAddress.custom.numberExt,
                country_codee: order.shipments[0].shippingAddress.countryCode.value,
                postal_code: order.shipments[0].shippingAddress.postalCode,
                address_line_2: order.shipments[0].shippingAddress.custom.suburb,
                admin_area_2: order.shipments[0].shippingAddress.custom.county
        },
        email: order.customerEmail
    }

    request.purchases_unit = [purchaseUnits];

    return request;
}

function saveCreditDebit(basket, billingData) {
    var parsedExpirationDate = billingData.paymentInformation.expirationDate.value.toISOString().substr(0, 7);
    var request = {
        billing_address: {
            address_line_1: billingData.address.address1.value,
            admin_area_1: billingData.address.stateCode.value, // ciudad
            admin_area_2: billingData.address.city.value, // colonia
            postal_code: billingData.address.postalCode.value,
            country_codee: billingData.address.countryCode.value
        },
        name: basket.billingAddress.fullName,
        security_code: billingData.paymentInformation.securityCode.value,
        // security_code: "",
        number: billingData.paymentInformation.cardNumber.value,
        // number: "",
        expiry: parsedExpirationDate
    };
    return request;
}

function requestCashOxxo(order) {
    var date = new Date();
    date.setDate(date.getDate() + 1);
    var request = {
        currency: order.currencyCode,
        items: itemsBuilder(order, 'conekta'),
        shipping_cost: [
            {
                amount: order.getAdjustedShippingTotalPrice().getValue(),
                carrier: order.shipments[0].shippingMethodID
            }
        ],
        payer: {
            first_name: order.billingAddress.firstName,
            last_name: order.billingAddress.lastName,
            email: order.customerEmail,
            phone: {
                area_code: '+52',
                number: order.billingAddress.phone
            },
            address: {
                zip_code: order.shipments[0].shippingAddress.postalCode,
                street_name: order.shipments[0].shippingAddress.address1,
                country: order.shipments[0].shippingAddress.countryCode.value
            }
        },
        charges: [
            {
                payment_method_id: 'oxxo_cash',
                expires_at: date.toISOString().substring(0, 19)
            }
        ],
        payment_gateway: 1,
        callback_url: URLUtils.https('Cash-ConektaPaymentNotification').toString()
    };
    request.binary_mode = true;
    return request;
}

function requestCashOther(order) {
    var request = {
        charges: [
            {
                payment_method_id: 'bancomer'
            }
        ],
        payer: {
            first_name: order.billingAddress.firstName,
            last_name: order.billingAddress.lastName,
            email: order.customerEmail,
            registration_date: (new Date()).toISOString(),
            phone: {
                area_code: '+52',
                number: order.billingAddress.phone
            },
            address: {
                zip_code: order.shipments[0].shippingAddress.postalCode,
                street_name: order.shipments[0].shippingAddress.address1
            }
        },
        items: itemsBuilder(order, 'mpcash'),
        description: order.productLineItems[0].lineItemText,
        payment_gateway: 2,
        reference: order.orderNo,
        callback_url: URLUtils.https('Cash-MercadoPagoPaymentNotification').toString()
    };
    return request;
}

function requestBradescard(order, customer, installments, issuerId, customerId) {
    var collections = require('*/cartridge/scripts/util/collections');
    var payer = {
        first_name: order.billingAddress.firstName,
        last_name: order.billingAddress.lastName,
        email: order.customerEmail,
        phone: {
            area_code: '+52',
            number: order.billingAddress.phone
        },
        address: {
            zip_code: order.billingAddress.postalCode,
            street_name: order.billingAddress.address1 + '-' + order.billingAddress.city + '-' + 'MX',
            street_number: 0
        }
    };

    if (customer.isAuthenticated()) {
        payer.registration_date = customer.profile.getCreationDate();
    }

    if (customerId) {
        payer.id = customerId;
    }

    // Payment method id and token
    var paymentMethodId;
    var token;

    collections.forEach(order.getPaymentInstruments(), function (payInstrument) {
        if (payInstrument.paymentMethod !== Resource.msg('payment.method.id', 'mercadoPagoPreferences', null)) {
            return;
        }

        paymentMethodId = payInstrument.creditCardType;
        token = payInstrument.creditCardToken;
    });

    var transactionAmount = getTotalAmount(order);

    var payDataObj = {
        token: token, // Required only for credit card payments
        payment_method_id: paymentMethodId, // Required
        transaction_amount: transactionAmount.value,
        installments: 1,
        additional_info: {
            items: itemsBuilder(order, 'mp'),
            payer: payer
        },
    };
    // notification_url: URLUtils.https("MercadoPago-MercadoPagoPaymentNotification").toString()
    // Set issuer id
    if (issuerId !== 0) {
        payDataObj.issuer_id = issuerId;
    }

    // Set installments if they are greater than 1
    if (installments !== 1) {
        payDataObj.installments = installments;
    }

    payDataObj.external_reference = order.orderNo;
    return payDataObj;
}

/**
 * Gets total amount for Line Item
 * @param {Object} lineItemCtnr - Line item container for which total amount will be retrieved
 * @returns {number} - total amount of the item
 */
function getTotalAmount(lineItemCtnr) {
    var totalAmount = lineItemCtnr.getTotalGrossPrice();

    lineItemCtnr.getGiftCertificatePaymentInstruments().toArray().forEach(function (item) {
        if (item.paymentTransaction && item.paymentTransaction.amount) {
            totalAmount = totalAmount.subtract(item.paymentTransaction.amount);
        }
    });

    return totalAmount;
}

function requestFinancing(data) {
    var request = {
        financing_country_code: 'MX',
        transaction_amount: {
            value: data.amount,
            currency_code: data.currency
        },
        funding_instrument_token: {
            type: data.option,
            payment: data.value
        }
    };
    return request;
}

function itemsBuilder(order, paymentMethod) {
    var collections = require('*/cartridge/scripts/util/collections');
    var items = collections.map(order.allProductLineItems, function (prodLineItem) {
        var item = {};

        item.id = prodLineItem.productID;
        if (paymentMethod === 'conekta' || paymentMethod === 'mpcash') {
            item.name = prodLineItem.product.name;
        } else {
            item.title = prodLineItem.product.name;
        }

        if (prodLineItem.product.longDescription && prodLineItem.product.longDescription !== null) {
            item.description = prodLineItem.product.longDescription.markup.slice(0, 250);
        }

        item.category_id = 'others';

        item.quantity = prodLineItem.quantityValue;
        item.unit_price = prodLineItem.getProratedPrice().divide(prodLineItem.getQuantity().getValue()).getValue();

        return item;
    });

    if (paymentMethod === 'mp' || paymentMethod === 'mpcash') {
    // add shipment costs as item
        if (order.getAdjustedShippingTotalPrice() > 0) {
            var shippingItem = {
                id: order.defaultShipment.shippingMethod.ID,
                quantity: 1,
                unit_price: order.getAdjustedShippingTotalPrice().getValue(),
                category_id: 'others'
            };
            if (paymentMethod === 'mpcash') {
                shippingItem.name = order.defaultShipment.shippingMethod.displayName;
            } else {
                shippingItem.title = order.defaultShipment.shippingMethod.displayName;
            }


            items.push(shippingItem);
        }
    }
    return items;
}

/**
 * Creates puchase unit data
 * PayPal amount should equal: item_total + tax_total + shipping + handling + insurance - shipping_discount - discount
 *
 * @param {dw.order.Basket} currentBasket - user's basket
 * @param {boolean} isCartFlow - whether from cart or no
 * @returns {Object} with purchase unit data
 */
function getPurchaseUnit(currentBasket, isCartFlow) {
    var {
        currencyCode,
        defaultShipment,
        productLineItems,
        shippingTotalPrice,
        adjustedShippingTotalPrice,
        merchandizeTotalPrice,
        adjustedMerchandizeTotalPrice,
        giftCertificateLineItems,
        giftCertificateTotalPrice,
        totalTax
    } = currentBasket;
    var orderNo;
    var handling;
    var insurance;


    var nonShippingDiscount = Array.reduce(
        currentBasket.giftCertificatePaymentInstruments,
        getAppliedGiftCertificateTotal,
        merchandizeTotalPrice.subtract(adjustedMerchandizeTotalPrice)
    );
    var description = getItemsDescription(productLineItems) + ' ' + getGiftCertificateDescription(giftCertificateLineItems);

    var purchaseUnit = {
        description: description.trim(),
        amount: {
            currency_code: currencyCode,
            value: calculateNonGiftCertificateAmount(currentBasket).value.toString(),
            breakdown: {
                item_total:  merchandizeTotalPrice.add(giftCertificateTotalPrice).value.toString(),
                shipping: shippingTotalPrice.value.toString(),
                tax_total: TaxMgr.getTaxationPolicy() === TaxMgr.TAX_POLICY_GROSS ? '0' : totalTax.value.toString(),
                handling:  !empty(handling) ? handling : '0',
                insurance:  !empty(insurance) ? insurance : '0',
                shipping_discount: shippingTotalPrice.subtract(adjustedShippingTotalPrice).value.toString(),
                discount: nonShippingDiscount.value.toString()
            }
        },
        invoice_id: currentBasket.orderNo
    };


    return purchaseUnit;
}

/**
 * @param  {dw.value.Money} acc current basket order + product discount
 * @param  {dw.order.OrderPaymentInstrument} giftCertificate GC from the basket
 * @returns {dw.value.Money} Gift certificate cotal
 */
function getAppliedGiftCertificateTotal(acc, giftCertificate) {
    return acc.add(giftCertificate.paymentTransaction.amount);
}

/**
 * Create purchase unit description based on items in the basket
 * @param  {dw.order.ProductLineItem} productLineItems Items in the basket
 * @returns {string} item description
 */
function getItemsDescription(productLineItems) {
    if (productLineItems.length === 0) {
        return '';
    }
    return Array.map(productLineItems, function (productLineItem) {
        return productLineItem.productName;
    }).join(',').substring(0, 127);
}

/**
 * Create purchase unit description based on gifts in the basket
 * @param  {dw.order.LineItemCtnr} giftCertificateLineItems Items in the basket
 * @returns {string} gift description
 */
function getGiftCertificateDescription(giftCertificateLineItems) {
    if (giftCertificateLineItems.length === 0) {
        return '';
    }
    return Array.map(giftCertificateLineItems, function (giftCertLineItem) {
        return giftCertLineItem.lineItemText + ' for ' + giftCertLineItem.recipientEmail;
    }).join(',').substring(0, 127);
}

/**
 * Calculates the amount to be payed by a non-gift certificate payment instrument based
 * on the given basket. The method subtracts the amount of all redeemed gift certificates
 * from the order total and returns this value.
 *
 * @param {Object} lineItemCtnr - LineIteam Container (Basket or Order)
 * @returns {dw.value.Money} non gift certificate amount
 */
function calculateNonGiftCertificateAmount(lineItemCtnr) {
    var giftCertTotal = new Money(0.0, lineItemCtnr.currencyCode);
    var gcPaymentInstrs = lineItemCtnr.getGiftCertificatePaymentInstruments();
    var iter = gcPaymentInstrs.iterator();
    var orderPI = null;

    while (iter.hasNext()) {
        orderPI = iter.next();
        giftCertTotal = giftCertTotal.add(orderPI.getPaymentTransaction().getAmount());
    }

    var orderTotal = lineItemCtnr.totalGrossPrice;
    var amountOpen = orderTotal.subtract(giftCertTotal);
    return amountOpen;
}


module.exports = {
    requestCreditoDebito: requestCreditoDebito,
    requestCashOxxo: requestCashOxxo,
    requestCashOther: requestCashOther,
    requestBradescard: requestBradescard,
    requestFinancing: requestFinancing,
    saveCreditDebit: saveCreditDebit
};
