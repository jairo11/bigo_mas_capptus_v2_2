"use strict";

/* global session */

var base = module.superModule;

var HookMgr = require("dw/system/HookMgr");
var OrderMgr = require("dw/order/OrderMgr");
var PaymentMgr = require("dw/order/PaymentMgr");
var Transaction = require("dw/system/Transaction");
var PaymentInstrument = require('dw/order/PaymentInstrument');

/**
 * handles the payment authorization for each payment instrument
 * @param {dw.order.Order} order - the order object
 * @param {string} orderNumber - The order number for the order
 * @returns {Object} an error object
 */
function handlePayments(order, orderNumber) {
    var result = {};

    if (order.totalNetPrice !== 0.00) {
        var paymentInstruments = order.paymentInstruments;

        if (paymentInstruments.length === 0) {
            Transaction.wrap(function () { OrderMgr.failOrder(order, true); });
            result.error = true;
        }

        if (!result.error) {
            for (var i = 0; i < paymentInstruments.length; i++) {
                var paymentInstrument = paymentInstruments[i];
                var paymentProcessor = PaymentMgr
                    .getPaymentMethod(paymentInstrument.paymentMethod)
                    .paymentProcessor;
                var authorizationResult;
                if (paymentProcessor === null) {
                    Transaction.begin();
                    paymentInstrument.paymentTransaction.setTransactionID(orderNumber);
                    Transaction.commit();
                } else {
                    if (HookMgr.hasHook("app.payment.processor." +
                            paymentProcessor.ID.toLowerCase())) {
                        authorizationResult = HookMgr.callHook(
                            "app.payment.processor." + paymentProcessor.ID.toLowerCase(),
                            "Authorize",
                            orderNumber,
                            paymentInstrument,
                            paymentProcessor
                        );
                    } else {
                        authorizationResult = HookMgr.callHook(
                            "app.payment.processor.default",
                            "Authorize"
                        );
                    }

                    // Clean up previous session
                    delete session.privacy.redirectURL;
                    delete session.privacy.resetMpToken;
                    delete session.privacy.detailedError;
                    delete session.privacy.orderNumber;

                    if (authorizationResult.redirectURL) {
                        session.privacy.redirectURL = authorizationResult.redirectURL;
                        session.privacy.orderNumber = orderNumber;
                        result.error = true;
                        break;
                    }

                    if (authorizationResult.resetMpToken) {
                        session.privacy.resetMpToken = authorizationResult.resetMpToken;
                    }

                    if (authorizationResult.detailedError) {
                        session.privacy.detailedError = authorizationResult.detailedError;
                    }

                    if (authorizationResult.error) {
                        Transaction.wrap(function () { OrderMgr.failOrder(order, true); });
                        result.error = true;
                        break;
                    }
                }
            }
        }
    }

    return result;
}

function savePaymentInstrumentToWallet(form, paymentInstrument, customer, generatedToken, customerId) {
    var wallet = customer.getProfile().getWallet();

    return Transaction.wrap(function () {
        var storedPaymentInstrument = wallet.createPaymentInstrument(paymentInstrument.paymentMethod);

        storedPaymentInstrument.setCreditCardHolder(
            form.mercadoPagoCreditCard.cardOwner.value
        );
        storedPaymentInstrument.setCreditCardNumber(
            paymentInstrument.maskedCreditCardNumber
        );
        storedPaymentInstrument.setCreditCardType(
            paymentInstrument.creditCardType
        );
        storedPaymentInstrument.setCreditCardExpirationMonth(
            paymentInstrument.creditCardExpirationMonth
        );
        storedPaymentInstrument.setCreditCardExpirationYear(
            paymentInstrument.creditCardExpirationYear
        );

        storedPaymentInstrument.setCreditCardToken(generatedToken);

        if (customerId) {
            storedPaymentInstrument.custom.mpCustomerID = customerId;
        }

        return storedPaymentInstrument;
    });
}

/**
 * Sets the payment transaction amount
 * @returns {Object} an error object
 */
function calculatePaymentTransaction() {
    // overring logic because it doesn't support gift-certificates
    var result = { error: false };
    return result;
}

module.exports = {
    handlePayments: handlePayments,
    calculatePaymentTransaction: calculatePaymentTransaction,
    savePaymentInstrumentToWallet: savePaymentInstrumentToWallet
};

Object.keys(base).forEach(function (prop) {
    // eslint-disable-next-line no-prototype-builtins
    if (!module.exports.hasOwnProperty(prop)) {
        module.exports[prop] = base[prop];
    }
});
