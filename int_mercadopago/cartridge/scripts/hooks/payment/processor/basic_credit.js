'use strict';

var collections = require('*/cartridge/scripts/util/collections');

var PaymentInstrument = require('dw/order/PaymentInstrument');
var PaymentMgr = require('dw/order/PaymentMgr');
var PaymentStatusCodes = require('dw/order/PaymentStatusCodes');
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');
var OrderMgr = require("dw/order/OrderMgr");
var HookMgr = require("dw/system/HookMgr");
var Order = require("dw/order/Order");

/**
 * Creates a token. This should be replaced by utilizing a tokenization provider
 * @returns {string} a token
 */
function createToken() {
    return Math.random().toString(36).substr(2);
}

/**
 * Verifies that entered credit card information is a valid card. If the information is valid a
 * credit card payment instrument is created
 * @param {dw.order.Basket} basket Current users's basket
 * @param {Object} paymentInformation - the payment information
 * @param {string} paymentMethodID - paymentmethodID
 * @param {Object} req the request object
 * @return {Object} returns an error object
 */
function Handle(basket, paymentInformation, paymentMethodID, req) {
    //git-flow-test-commit
    //git-flow-test-commit-two
    var currentBasket = basket;
    var cardErrors = {};
    var cardNumber = paymentInformation.cardNumber.value;
    var cardSecurityCode = paymentInformation.securityCode.value;
    var expirationMonth = paymentInformation.expirationMonth.value;
    var expirationYear = paymentInformation.expirationYear.value;
    var serverErrors = [];
    var creditCardStatus;


    var cardType = paymentInformation.cardType.value;
    var paymentCard = PaymentMgr.getPaymentCard(cardType);


    // Validate payment instrument
    if (paymentMethodID === PaymentInstrument.METHOD_CREDIT_CARD) {
        var creditCardPaymentMethod = PaymentMgr.getPaymentMethod(PaymentInstrument.METHOD_CREDIT_CARD);
        var paymentCardValue = PaymentMgr.getPaymentCard(paymentInformation.cardType.value);

        var applicablePaymentCards = creditCardPaymentMethod.getApplicablePaymentCards(
            req.currentCustomer.raw,
            req.geolocation.countryCode,
            null
        );

        if (!applicablePaymentCards.contains(paymentCardValue)) {
            // Invalid Payment Instrument
            var invalidPaymentMethod = Resource.msg('error.payment.not.valid', 'checkout', null);
            return { fieldErrors: [], serverErrors: [invalidPaymentMethod], error: true };
        }
    }

    if (!paymentInformation.creditCardToken) {
        if (paymentCard) {
            creditCardStatus = paymentCard.verify(
                expirationMonth,
                expirationYear,
                cardNumber,
                cardSecurityCode
            );
        } else {
            cardErrors[paymentInformation.cardNumber.htmlName] =
                Resource.msg('error.invalid.card.number', 'creditCard', null);

            return { fieldErrors: [cardErrors], serverErrors: serverErrors, error: true };
        }

        if (creditCardStatus.error) {
            collections.forEach(creditCardStatus.items, function (item) {
                switch (item.code) {
                    case PaymentStatusCodes.CREDITCARD_INVALID_CARD_NUMBER:
                        cardErrors[paymentInformation.cardNumber.htmlName] =
                            Resource.msg('error.invalid.card.number', 'creditCard', null);
                        break;

                    case PaymentStatusCodes.CREDITCARD_INVALID_EXPIRATION_DATE:
                        cardErrors[paymentInformation.expirationMonth.htmlName] =
                            Resource.msg('error.expired.credit.card', 'creditCard', null);
                        cardErrors[paymentInformation.expirationYear.htmlName] =
                            Resource.msg('error.expired.credit.card', 'creditCard', null);
                        cardErrors[paymentInformation.expirationDate.htmlName] =
                            Resource.msg('error.expired.credit.card', 'creditCard', null);
                        break;

                    case PaymentStatusCodes.CREDITCARD_INVALID_SECURITY_CODE:
                        cardErrors[paymentInformation.securityCode.htmlName] =
                            Resource.msg('error.invalid.security.code', 'creditCard', null);
                        break;
                    default:
                        serverErrors.push(
                            Resource.msg('error.card.information.error', 'creditCard', null)
                        );
                }
            });

            return { fieldErrors: [cardErrors], serverErrors: serverErrors, error: true };
        }
    }

    Transaction.wrap(function () {
        collections.forEach(currentBasket.getPaymentInstruments(), function (item) {
            if (!item.giftCertificateCode) {
                currentBasket.removePaymentInstrument(item);
            }
        });

        var paymentInstrument = currentBasket.createPaymentInstrument(
            PaymentInstrument.METHOD_CREDIT_CARD, currentBasket.totalGrossPrice
        );

        paymentInstrument.setCreditCardHolder(currentBasket.billingAddress.fullName);
        paymentInstrument.setCreditCardNumber(cardNumber);
        paymentInstrument.setCreditCardType(cardType);
        paymentInstrument.setCreditCardExpirationMonth(expirationMonth);
        paymentInstrument.setCreditCardExpirationYear(expirationYear);
        paymentInstrument.custom.tempCode = cardSecurityCode;
        paymentInstrument.custom.tempNo = cardNumber;
        paymentInstrument.custom.creditCardExpirationDate = paymentInformation.expirationDate.value;
        if(paymentInformation.creditCardToken)
            paymentInstrument.custom.tempToken = paymentInformation.creditCardToken;
    });

    return { fieldErrors: cardErrors, serverErrors: serverErrors, error: false };
}

/**
 * Authorizes a payment using a credit card. Customizations may use other processors and custom
 *      logic to authorize credit card payment.
 * @param {number} orderNumber - The current order's number
 * @param {dw.order.PaymentInstrument} paymentInstrument -  The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor -  The payment processor of the current
 *      payment method
 * @return {Object} returns an error object
 */
function Authorize(orderNumber, paymentInstrument, paymentProcessor) {
    var experienceAPI = require('*/cartridge/scripts/services/paymentMulesoft');
    var requestHelpers = require('*/cartridge/scripts/helpers/requestPaymentHelper');
    var serverErrors = [];
    var fieldErrors = {};
    var error = false;
    var order = OrderMgr.getOrder(orderNumber);
    var creditCardForm = session.forms.billing;
    var saveCard = creditCardForm.creditCardFields.saveCard.value;
    try {
        var requestServiceCredDeb = requestHelpers.requestCreditoDebito(order,paymentInstrument);
        var paymentResponse = experienceAPI.transactionDebCredit(requestServiceCredDeb, creditCardForm.creditCardFields.saveCard.value);
        //SERVICE_UNAVAILABLE
        if (paymentResponse && paymentResponse.status) {
            if(saveCard && paymentResponse.details.tokenId &&
                customer.authenticated && customer.registered){
                HookMgr.callHook('app.payment.form.processor.' + paymentProcessor.ID.toLowerCase(),
                    'savePaymentInformation', creditCardForm, paymentInstrument, customer, paymentResponse.details.tokenId
                );
            }
            Transaction.wrap(function () {
                order.setPaymentStatus(Order.PAYMENT_STATUS_PAID);
                paymentInstrument.paymentTransaction.setTransactionID(paymentResponse.captures_id);
                paymentInstrument.paymentTransaction.setPaymentProcessor(paymentProcessor);
                if (paymentResponse.fee_breakdown && paymentResponse.fee_breakdown.length === 1) {
                    paymentInstrument.paymentTransaction.custom.application_fee = paymentResponse.fee_breakdown[0].amount;
                }
                paymentInstrument.paymentTransaction.custom.cardType=paymentResponse.details.provider;
                paymentInstrument.paymentTransaction.custom.date_created=paymentResponse.payment_date;
                paymentInstrument.paymentTransaction.custom.date_last_updated=paymentResponse.payment_date;
                paymentInstrument.paymentTransaction.custom.es_bradesco=false;
                paymentInstrument.paymentTransaction.custom.expiration_month=paymentInstrument.creditCardExpirationMonth;
                paymentInstrument.paymentTransaction.custom.expiration_year=paymentInstrument.creditCardExpirationYear;
                paymentInstrument.paymentTransaction.custom.last_four_digits=paymentInstrument.creditCardNumberLastDigits;
                paymentInstrument.paymentTransaction.custom.info_msi=paymentResponse.details.provider + ' 1';
                paymentInstrument.paymentTransaction.custom.name=paymentInstrument.creditCardHolder;
                paymentInstrument.paymentTransaction.custom.payment_type_id='PAYPAL';
                paymentInstrument.paymentTransaction.custom.status=paymentResponse.status;

                // Set order payment status to paid if order is authorized
                if (paymentResponse.captures_status === "COMPLETED") {
                    order.setPaymentStatus(Order.PAYMENT_STATUS_PAID);
                } else {
                    order.setPaymentStatus(Order.PAYMENT_STATUS_NOTPAID);
                    error = true;
                    serverErrors.push(
                        Resource.msg("error.technical", "checkout", null)
                    );
                }
            });
            delete session.custom.paypalFraudnetSessionId;

        } else {
            error = true;
            serverErrors.push(
                Resource.msg("error.technical", "checkout", null)
            );
        }
    } catch (e) {
        error = true;
        serverErrors.push(
            Resource.msg('error.technical', 'checkout', null)
        );
    }

    return { fieldErrors: fieldErrors, serverErrors: serverErrors, error: error };
}

/**
 * @description Create refund data and make call to API
 * @param {number} order - The current order's number
 * @param {number} amountReturn - The current amount to refund
 * @param {number} rma - The rma to refund
 */
 function Refund(order, amountReturn, rma) {
    var Logger = require("dw/system/Logger");
    var error=false;
    var paymentServices = require('*/cartridge/scripts/services/paymentMulesoft');
    try {
        // Create payment data
        let idTransaction=order.paymentInstrument.paymentTransaction.transactionID;
        let amountRefund= {
            amount: Number(amountReturn),
            payment_gateway: "PAYPAL",
            currency: "MXN"
        };
        var paymentResponse = paymentServices.transactionRefund(idTransaction, amountRefund);
        if(paymentResponse.statusCode === "ERROR"){
            Logger.error("Error - refund. IDTransaction to refund = "+idTransaction);
            error=true;
        }
    } catch (e) {
        Logger.error(JSON.stringify(e));
        error = true;
        serverErrors.push(
            Resource.msg("error.technical", "checkout", null)
        );
    }

    return error;
}

exports.Handle = Handle;
exports.Authorize = Authorize;
exports.createToken = createToken;
exports.Refund = Refund;
