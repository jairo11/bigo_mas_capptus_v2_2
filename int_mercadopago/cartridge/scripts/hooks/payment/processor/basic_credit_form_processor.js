'use strict';

var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
var experienceAPI = require('*/cartridge/scripts/services/paymentMulesoft');
var requestHelpers = require('*/cartridge/scripts/helpers/requestHelper');

function parseExpirationMonth(expirationDate){
    var month = expirationDate.substr(0, 2);
    return parseInt(month, 10)-1;
}

function parseExpirationYear(expirationDate){
    var currentYearFirstTwoDigits = new Date().getFullYear().toString().substr(0,2);
    var cardYearLastTwoDigits = expirationDate.substr(3, 4);
    return parseInt(currentYearFirstTwoDigits+cardYearLastTwoDigits, 10);
}

function pad(number) {
    if (number < 10) {
      return '0' + number;
    }
    return number;
  }

/**
 * Verifies the required information for billing form is provided.
 * @param {Object} req - The request object
 * @param {Object} paymentForm - the payment form
 * @param {Object} viewFormData - object contains billing form data
 * @returns {Object} an object that has error information or payment information
 */
function processForm(req, paymentForm, viewFormData) {
    var array = require('*/cartridge/scripts/util/array');

    var viewData = viewFormData;
    var creditCardErrors = {};

    if (!req.form.storedPaymentUUID) {
        // verify credit card form data
        creditCardErrors = COHelpers.validateCreditCard(paymentForm);
    }

    if (Object.keys(creditCardErrors).length) {
        return {
            fieldErrors: creditCardErrors,
            error: true
        };
    }

    var expirationDate = new Date(Date.UTC(
        parseExpirationYear(paymentForm.creditCardFields.expirationDate.htmlValue),
        parseExpirationMonth(paymentForm.creditCardFields.expirationDate.htmlValue)));
    viewData.paymentMethod = {
        value: paymentForm.paymentMethod.value,
        htmlName: paymentForm.paymentMethod.value
    };

    viewData.paymentInformation = {
        cardType: {
            value: paymentForm.creditCardFields.cardType.value,
            htmlName: paymentForm.creditCardFields.cardType.htmlName
        },
        cardNumber: {
            value: paymentForm.creditCardFields.cardNumber.value,
            htmlName: paymentForm.creditCardFields.cardNumber.htmlName
        },
        securityCode: {
            value: paymentForm.creditCardFields.securityCode.value,
            htmlName: paymentForm.creditCardFields.securityCode.htmlName
        },
        expirationMonth: {
            value: expirationDate.getUTCMonth()+1,
            htmlName: paymentForm.creditCardFields.expirationMonth.htmlName
        },
        expirationYear: {
            value: expirationDate.getUTCFullYear(),
            htmlName: paymentForm.creditCardFields.expirationYear.htmlName
        },
        expirationDate: {
            value: expirationDate,
            htmlName: paymentForm.creditCardFields.expirationDate.htmlName
        }
    };

    if (req.form.storedPaymentUUID) {
        viewData.storedPaymentUUID = req.form.storedPaymentUUID;
    }

    viewData.saveCard = paymentForm.creditCardFields.saveCard.checked;

    // process payment information
    if (viewData.storedPaymentUUID
        && req.currentCustomer.raw.authenticated
        && req.currentCustomer.raw.registered
    ) {
        var paymentInstruments = req.currentCustomer.wallet.paymentInstruments;
        var paymentInstrument = array.find(paymentInstruments, function (item) {
            return viewData.storedPaymentUUID === item.UUID;
        });

        viewData.paymentInformation.cardNumber.value = paymentInstrument.creditCardNumber;
        viewData.paymentInformation.cardType.value = paymentInstrument.creditCardType;
        viewData.paymentInformation.securityCode.value = req.form.securityCode;
        viewData.paymentInformation.expirationMonth.value = paymentInstrument.creditCardExpirationMonth;
        viewData.paymentInformation.expirationYear.value = paymentInstrument.creditCardExpirationYear;
        viewData.paymentInformation.creditCardToken = paymentInstrument.raw.creditCardToken;
    }

    return {
        error: false,
        viewData: viewData
    };
}

function savePaymentInformation(creditCardForm, paymentInstrument, sessionCustomer, cardToken) {
    var CustomerMgr = require('dw/customer/CustomerMgr');

    var customer = CustomerMgr.getCustomerByCustomerNumber(
        sessionCustomer.profile.customerNo
    );
    COHelpers.savePaymentInstrumentToWallet(
        creditCardForm,
        paymentInstrument,
        customer,
        cardToken
    );
}

exports.processForm = processForm;
exports.savePaymentInformation = savePaymentInformation;
