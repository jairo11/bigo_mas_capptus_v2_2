"use strict";

var collections = require("*/cartridge/scripts/util/collections");

var Transaction = require("dw/system/Transaction");
var Resource = require("dw/web/Resource");
var OrderMgr = require("dw/order/OrderMgr");

var CASH = 'Cash';

/**
 * @description Create payment instrument
 * @param {dw.order.Basket} basket Current basket
 * @param {Object} paymentInformation - the payment information
 * @returns {Object}
 */
function Handle(basket, paymentInformation) {
    var PaymentMgr = require('dw/order/PaymentMgr');
    var currentBasket   = basket;

    Transaction.wrap(function () {
        collections.forEach(currentBasket.getPaymentInstruments(), function (item) {
            if (!item.giftCertificateCode) {
                currentBasket.removePaymentInstrument(item);
            }
        });

        var paymentInstrument = currentBasket.createPaymentInstrument(
            CASH, currentBasket.totalGrossPrice
        );
        paymentInstrument.custom.storeType = paymentInformation.storeType.value;
        var paymentProcessor = PaymentMgr.getPaymentMethod(paymentInstrument.getPaymentMethod()).getPaymentProcessor();
        paymentInstrument.getPaymentTransaction().setPaymentProcessor(paymentProcessor);

    });

    return { fieldErrors: {}, serverErrors: [], error: false };
}

/**
 * @description Create payment data and make call to API
 * @param {Number} orderNumber - The current order's number
 * @param {dw.order.PaymentInstrument} paymentInstrument -  The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor -  The payment processor of the current
 *      payment method
 * @returns {Object}
 */
function Authorize(orderNumber, paymentInstrument, paymentProcessor) {
    var Order = require('dw/order/Order');
    var experienceAPI = require('*/cartridge/scripts/services/paymentMulesoft');
    var requestHelpers = require('*/cartridge/scripts/helpers/requestPaymentHelper');
    var serverErrors = [];
    var fieldErrors = {};
    var error = false;
    var requestService;
    var paymentResponse;
    var order = OrderMgr.getOrder(orderNumber);

    try {
        if (paymentInstrument.custom.storeType === "oxxo") {
            requestService = requestHelpers.requestCashOxxo(order);
        } else if (paymentInstrument.custom.storeType === "other") {
            requestService = requestHelpers.requestCashOther(order);
        }

        paymentResponse = experienceAPI.transactionCash(requestService);
        if (paymentResponse && !paymentResponse.statusCode) {
                Transaction.wrap(function () {
                    if (paymentInstrument.custom.storeType === "oxxo") {
                        order.custom.OxxoOrderID = paymentResponse.id;
                        order.custom.OxxoReferenceID = paymentResponse.details.reference;
                    }
                    order.setPaymentStatus(Order.PAYMENT_STATUS_PARTPAID);
                    order.setConfirmationStatus(Order.CONFIRMATION_STATUS_NOTCONFIRMED);
                    paymentInstrument.paymentTransaction.setTransactionID(paymentResponse.id);
                    paymentInstrument.paymentTransaction.custom.bankReference = paymentResponse.details.reference;

                    if (paymentResponse.details.branch) {
                        paymentInstrument.paymentTransaction.custom.bankAgreement = paymentResponse.details.branch;
                    }
                    if (paymentResponse.fee_breakdown && paymentResponse.fee_breakdown.length === 1) {
                        paymentInstrument.paymentTransaction.custom.application_fee = paymentResponse.fee_breakdown[0].amount;
                    }
                    paymentInstrument.paymentTransaction.custom.date_created=paymentResponse.payment_date;
                    paymentInstrument.paymentTransaction.custom.date_last_updated=paymentResponse.payment_date;
                    paymentInstrument.paymentTransaction.custom.es_bradesco=false;
                    paymentInstrument.paymentTransaction.custom.payment_type_id=paymentInstrument.custom.storeType === 'oxxo' ? 'CONEKTA' : 'MERCADOPAGO';
                    paymentInstrument.paymentTransaction.custom.status=paymentResponse.status;

                });

        } else {
            error = true;
            serverErrors.push(
                Resource.msg("error.technical", "checkout", null)
            );
        }
    } catch (e) {
        error = true;
        serverErrors.push(
            Resource.msg("error.technical", "checkout", null)
        );
    }

    return { fieldErrors: fieldErrors, serverErrors: serverErrors, error: error };
}

exports.Handle = Handle;
exports.Authorize = Authorize;


