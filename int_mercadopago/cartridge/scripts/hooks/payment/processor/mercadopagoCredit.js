"use strict";

/* global customer empty session */
/* eslint-disable no-param-reassign */


var collections = require("*/cartridge/scripts/util/collections");
var array = require("*/cartridge/scripts/util/array");
var MercadoPago = require("*/cartridge/scripts/library/libMercadoPago");

var Transaction = require("dw/system/Transaction");
var Resource = require("dw/web/Resource");
var Order = require("dw/order/Order");
var OrderMgr = require("dw/order/OrderMgr");
var Logger = require("dw/system/Logger").getLogger("OSFMercadoPago", "OSFMercadoPago");
var HookMgr = require("dw/system/HookMgr");


/**
 * @description Create payment instrument
 * @param {dw.order.Basket} basket Current basket
 * @param {Object} paymentInformation - the payment information
 * @returns {Object} - Object containing error description
 */
function Handle(basket, paymentInformation) {
    var currentBasket = basket;
    var paymentTypeId = paymentInformation.mercadoPago.paymentTypeId;
    var cardType = paymentInformation.mercadoPago.cardType.value;
    var creditCardToken = paymentInformation.mercadoPago.token.value;
    var docType = paymentInformation.mercadoPago.identification.docType.value;
    var docNumber = paymentInformation.mercadoPago.identification.docNumber.value;
    var MP = new MercadoPago();

    Transaction.wrap(function () {
        collections.forEach(currentBasket.getPaymentInstruments(), function (item) {
            if (!item.giftCertificateCode) {
                currentBasket.removePaymentInstrument(item);
            }
        });

        var amount = MP.getTotalAmount(currentBasket);
        var paymentInstrument = currentBasket.createPaymentInstrument(
            Resource.msg("payment.method.id", "mercadoPagoPreferences", null), amount
        );

        /**
         * Credit card data isn't saved
         * It's saved on MercadoPago side
         * Instead token is obtained
         */
        paymentInstrument.setCreditCardType(cardType); // Required always
        if (creditCardToken && paymentInstrument.setCreditCardToken) {
            paymentInstrument.setCreditCardToken(creditCardToken); // Required only for credit card payments
        }

        if (docType && docNumber) {
            paymentInstrument.custom.customerDocType = docType;
            paymentInstrument.custom.customerDocNumber = docNumber;
        }
        if (cardType === "pse") {
            paymentInstrument.custom.mercadoPagoFinancialInstitution = paymentInformation.mercadoPago.financialinstitution;
        }
        paymentInstrument.custom.mercadoPagoPaymentTypeId = paymentTypeId;
    });

    return { fieldErrors: {}, serverErrors: [], error: false };
}

/**
 * @description Create payment data and make call to API
 * @param {number} orderNumber - The current order's number
 * @param {dw.order.PaymentInstrument} paymentInstrument -  The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor -  The payment processor of the current payment method
 * @returns {Object} - Object containing errors or redirect token and URL
 */
function Authorize(orderNumber, paymentInstrument, paymentProcessor) {
    var paymentServices = require('*/cartridge/scripts/services/paymentMulesoft');
    var requestHelpers = require('*/cartridge/scripts/helpers/requestPaymentHelper');
    var order = OrderMgr.getOrder(orderNumber);
    var MP = new MercadoPago();
    var creditCardForm = session.forms.billing;
    var serverErrors = [];
    var error = false;
    var resetToken = false;

    // Default values
    var installments = 1;
    var issuerId = 0;
    var redirectURL = null;
    var redirectPaymentMethod = false;
    var detailedError = null;

    var customerSearch;
    var customerResult;
    var newCustomer;
    var customerID = "";

    var cardData = "";
    var parsedResponse;
    var saveCard = creditCardForm.mercadoPagoCreditCard.saveCard.value;
    var customerId = creditCardForm.mercadoPagoCreditCard.customerId.value;

    // Set installment option
    if (creditCardForm.mercadoPagoCreditCard.installments.value) {
        installments = parseInt(creditCardForm.mercadoPagoCreditCard.installments.value, 10);
    }

    // Set issuer option
    if (creditCardForm.mercadoPagoCreditCard.issuer.value) {
        issuerId = parseInt(creditCardForm.mercadoPagoCreditCard.issuer.value, 10);
    }


    try {
        // Create payment data
        var paymentData = requestHelpers.requestBradescard(order, customer, installments, issuerId, customerId);

        // Do payment request
        var paymentResponse = paymentServices.transactionBradescard(paymentData, saveCard);

        Transaction.wrap(function () {
            paymentInstrument.paymentTransaction.setPaymentProcessor(paymentProcessor);

            if (paymentInstrument.creditCardType) {
                cardData = paymentInstrument.creditCardType;
            }

            if (cardData) {
                paymentInstrument.paymentTransaction.custom.cardType = cardData;
            }

            if (paymentResponse && paymentResponse.status) {
                parsedResponse = MP.parseOrderStatus(paymentResponse.status);
                redirectPaymentMethod = (paymentResponse.payment_type_id === "ticket" || paymentResponse.payment_type_id === "bank_transfer");

                var isValidPaymentStatus = parsedResponse === "authorized" || parsedResponse === "pending";

                error = empty(parsedResponse) || !isValidPaymentStatus;

                // If response is successful, create customer card based on the token
                if (customer.authenticated && parsedResponse === "authorized" && saveCard) {
                    //MP.createCustomerCard(customerID, { token: paymentInstrument.creditCardToken });
                    HookMgr.callHook('app.payment.form.processor.' + paymentProcessor.ID.toLowerCase(),
                        'savePaymentInformation', creditCardForm, paymentInstrument, customer, paymentResponse.details.tokenId, paymentResponse.details.customerId
                    );
                }

                // Updates transaction statuses to be seen in BM
                order.custom.transactionStatus = paymentResponse.status + " - " + Resource.msg("status." + paymentResponse.status, "mercadoPago", null);
                order.custom.transactionReport = paymentResponse.status_detail;

                // Set transaction id
                if (paymentResponse.id) {
                    paymentInstrument.paymentTransaction.transactionID = paymentResponse.id;

                    // if (paymentResponse.transaction_details.external_resource_url) {
                    //     order.custom.transactionNote = paymentResponse.transaction_details.external_resource_url;
                    // }
                }
                //set values for xml order api
                if (paymentResponse.fee_breakdown && paymentResponse.fee_breakdown.length === 1) {
                    paymentInstrument.paymentTransaction.custom.application_fee = paymentResponse.fee_breakdown[0].amount;
                }
                paymentInstrument.paymentTransaction.custom.cardType=paymentResponse.details.provider;
                paymentInstrument.paymentTransaction.custom.date_created=paymentResponse.payment_date;
                paymentInstrument.paymentTransaction.custom.date_last_updated=paymentResponse.payment_date;
                paymentInstrument.paymentTransaction.custom.es_bradesco=true;
                paymentInstrument.paymentTransaction.custom.expiration_month=paymentResponse.card.expiration_month;
                paymentInstrument.paymentTransaction.custom.expiration_year=paymentResponse.card.expiration_year;
                paymentInstrument.paymentTransaction.custom.last_four_digits=paymentResponse.card.last_digits;
                paymentInstrument.paymentTransaction.custom.first_six_digits=paymentResponse.card.first_digits;
                paymentInstrument.paymentTransaction.custom.info_msi=paymentResponse.details.provider + ' 1';
                paymentInstrument.paymentTransaction.custom.name=paymentResponse.card.name;
                paymentInstrument.paymentTransaction.custom.payment_type_id='MERCADOPAGO';
                paymentInstrument.paymentTransaction.custom.status=paymentResponse.status;

                // Set order payment status to paid if order is authorized
                if (parsedResponse === "authorized") {
                    order.setPaymentStatus(Order.PAYMENT_STATUS_PAID);
                } else if (parsedResponse === "declined") {
                    order.setPaymentStatus(Order.PAYMENT_STATUS_NOTPAID);
                    resetToken = true;
                    error = true;
                    serverErrors.push(
                        Resource.msg("error.technical", "checkout", null)
                    );
                } else {
                    // can be pending-payment or transfer depending on payment option chosen
                    order.setPaymentStatus(Order.PAYMENT_STATUS_NOTPAID);
                }
                if (parsedResponse && paymentResponse.status_detail) {
                    order.addNote("MercadoPago response", parsedResponse + " - " + paymentResponse.status_detail);
                }
            } else {
                try {
                    var mpError = JSON.parse(session.privacy.mercadoPagoErrorMessage);
                    if (mpError && mpError.cause && mpError.cause[0] && mpError.cause[0].code) {
                        detailedError = mpError.cause[0].code.toString();
                    }
                } catch (ex) {
                    // do nothing
                } finally {
                    delete session.privacy.mercadoPagoErrorMessage;
                }
                error = true;
                resetToken = true;
                serverErrors.push(
                    Resource.msg("error.technical", "checkout", null)
                );
            }
        });
    } catch (e) {
        Logger.error(JSON.stringify(e));
        error = true;
        serverErrors.push(
            Resource.msg("error.technical", "checkout", null)
        );
    }

    if (redirectPaymentMethod && MP.getOtherPaymentMode() === "Redirect") {
        redirectURL = order.custom.transactionNote;
    }

    return {
        serverErrors: serverErrors,
        error: error,
        redirectURL: redirectURL,
        resetMpToken: resetToken,
        detailedError: detailedError
    };
}

/**
 * @description Create refund data and make call to API
 * @param {number} order - The current order's number
 * @param {number} amountReturn - The current amount to refund
 * @param {number} rma - The rma to refund
 */
 function Refund(order, amountReturn, rma) {
    var Logger = require("dw/system/Logger");
    var paymentServices = require('*/cartridge/scripts/services/paymentMulesoft');
    try {
        // Create payment data
        let error=false;
        let idTransaction=order.paymentInstrument.paymentTransaction.transactionID;
        let amountRefund= {
            amount:amountReturn,
            payment_gateway: "MP",
            currency: "MXN"
        };
        var paymentResponse = paymentServices.transactionRefund(idTransaction, amountRefund);
        if(paymentResponse.statusCode === "ERROR"){
            Logger.error("Error - refund. IDTransaction to refund = "+idTransaction);
            error=true;
        }
    } catch (e) {
        Logger.error(JSON.stringify(e));
        error = true;
        serverErrors.push(
            Resource.msg("error.technical", "checkout", null)
        );
    }

    return error;
}

exports.Refund = Refund;
exports.Handle = Handle;
exports.Authorize = Authorize;
