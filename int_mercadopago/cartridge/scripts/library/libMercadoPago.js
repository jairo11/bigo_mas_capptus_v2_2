/**
 * libMercadoPago.js
 *
 * A library file for Mercado Pago communication.
 */

 /* eslint-disable no-restricted-syntax */
 /* global request empty dw session */

var collections = require("*/cartridge/scripts/util/collections");
var array = require("*/cartridge/scripts/util/array");
var MercadoPagoHelper = require("*/cartridge/scripts/util/MercadoPagoHelper");

var Site = require("dw/system/Site");
var Resource = require("dw/web/Resource");
var Logger = require("dw/system/Logger").getLogger("OSFMercadoPago", "OSFMercadoPago");
var URLUtils = require("dw/web/URLUtils");
var LocalServiceRegistry = require("dw/svc/LocalServiceRegistry");
var System = require("dw/system/System");
/**
* Mercadopago service and base functions definions
*/
function MercadoPago() {
    // Initialize HTTP services for a cartridge
    var serviceID = Resource.msg("service.name", "mercadoPagoPreferences", null);
    var sandboxEnabled = true;

    this.service = LocalServiceRegistry.createService(serviceID, {
        createRequest: function (svc, args) {
            var svcRef = svc;
            svcRef.addHeader("Content-Type", args.contentType);
            svcRef.setRequestMethod(args.req);
            svcRef.URL = svc.getConfiguration().credential.URL + args.urlPart;

            if (!empty(args.data)) {
                return JSON.stringify(args.data);
            }
            return null;
        },
        parseResponse: function (svc, client) {
            if (client.statusCode === 200 || client.statusCode === 201) {
                return MercadoPagoHelper.parseJson(client.getText(), null);
            }

            Logger.error("[MercadoPago][{0}] : {1}", svc.getRequestData(), client.getErrorText());

            return {};
        },
        mockCall: function (svc) {
            return {
                statusCode: 200,
                statusMessage: "Success",
                text: "MOCK RESPONSE (" + svc.URL + ")"
            };
        },
        filterLogMessage: function (msg) {
            return msg;
        }
    });

    if (System.getInstanceType() === System.PRODUCTION_SYSTEM) {
        sandboxEnabled = false;
    }

    // overwrite sandboxEnabled if jsonData has enableSandbox set to true
    this.credentialsConfig = MercadoPagoHelper.parseJson(this.service.getConfiguration().credential.custom.jsonData, null);

    if (!empty(this.credentialsConfig) && "enableSandbox" in this.credentialsConfig && this.credentialsConfig.enableSandbox === true) {
        sandboxEnabled = true;
    }

    this.sandboxEnabled = sandboxEnabled;

    this.exec = function (args) {
        var result = this.service.call(args);

        if (!result.object && result.errorMessage) {
            session.privacy.mercadoPagoErrorMessage = result.errorMessage;
        }
        return result.object;
    };

    this.get = function (urlPart, contentType) {
        return this.exec({
            urlPart: urlPart,
            req: "GET",
            contentType: contentType
        });
    };

    this.post = function (urlPart, data, contentType) {
        return this.exec({
            urlPart: urlPart,
            req: "POST",
            data: data,
            contentType: contentType
        });
    };

    this.put = function (urlPart, data, contentType) {
        return this.exec({
            urlPart: urlPart,
            req: "PUT",
            data: data,
            contentType: contentType
        });
    };

    this.del = function (urlPart, contentType) {
        return this.exec({
            urlPart: urlPart,
            req: "DELETE",
            contentType: contentType
        });
    };
}

/**
 * Builds query for requested data to use in request URL
 * @param  {Object} data - object from which to build the query
 * @return {string} - query
 */
MercadoPago.prototype.buildquery = function (data) {
    var elements = [];

    for (var key in data) {
        if (Object.prototype.hasOwnProperty.call(data, key)) {
            elements.push(key + "=" + encodeURI(data[key]));
        }
    }

    return elements.join("&");
};

/**
 * Gets stored access token which is kept in service credentials password field.
 * @return {string} access token
 */
MercadoPago.prototype.getAccessToken = function () {
    // always return token
    var credentialId = MercadoPagoHelper.getServiceCredentialID();
    if (credentialId) {
        try {
            this.service.setCredentialID(credentialId);
        } catch (error) {
            return null;
        }
    }
    return this.service.getConfiguration().credential.password;
};

/**
 * @description Get available payment methods
 * @return {Array} available payment methods
 */
MercadoPago.prototype.getPaymentMethods = function () {
    var accessToken = this.getAccessToken();

    if (!accessToken) {
        return [];
    }

    var responseCollection = this.get("/v1/payment_methods?access_token=" + encodeURI(accessToken), "application/json");
    if (!responseCollection || !responseCollection.length > 0) {
        Logger.error("libMercadoPago.js - MercadoPago returned no payment methods");
        return null;
    }

    var responseArray = collections.map(responseCollection, function (responseItem) {
        return responseItem;
    });
    var paymentArray = [];

    if (this.credentialsConfig && this.credentialsConfig.includedPaymentIds) {
        try {
            var includedPaymentIds = this.credentialsConfig.includedPaymentIds;
            responseArray.forEach(function (responseItem) {
                for (var includedPaymentId in includedPaymentIds) {
                    if (includedPaymentId === responseItem.id) {
                        paymentArray.push(responseItem);
                    }
                }
            });
        } catch (e) {
            Logger.error("libMercadoPago.js - Error when filtering mercadopago payment methods : " + e.message);
        }
    }
    return paymentArray.length ? paymentArray : responseArray;
};

/**
 * Checks that basic checkout enabled from Json data in service credentials
 * @return {boolean} boolean value of basic checkout enabled or not
 */
MercadoPago.prototype.isBasicCheckoutEnabled = function () {
    var credential = this.service.getConfiguration().getCredential();
    var isBasicCheckoutEnabled = MercadoPagoHelper.parseJson(credential.custom.jsonData, {}).basicCheckoutEnabled;

    return !!isBasicCheckoutEnabled;
};

/**
 * @description Get group payment methods by payment type
 * @param {Array} paymentMethods - array of MercadoPago payment methods
 * @return {Object} grouped payment types as two different arrays in an object
 */
MercadoPago.prototype.groupPaymentMethods = function (paymentMethods) {
    var cards = [];
    var other = [];

    if (paymentMethods && paymentMethods.length > 0) {
        paymentMethods.forEach(function (paymentMethod) {
            if (paymentMethod.payment_type_id.indexOf("card") >= 0) {
                cards.push(paymentMethod);
            } else {
                other.push(paymentMethod);
            }
        });

        if (this.isBasicCheckoutEnabled()) {
            other.push({
                id: "basiccheckout",
                payment_type_id: "basiccheckout",
                name: "Basic Checkout",
                thumbnail: null,
                additional_info_needed: "no"
            });
        }
    }

    return {
        cards: cards,
        other: other
    };
};

/**
 * Creates payment on billing page to be a Mercadopago Payment
 * @param {Object} paymentData - Object containing information on customer, order, norification URL, etc.
 * @return {Object} payment creation response from Mercadopago
 */
MercadoPago.prototype.createPayment = function (paymentData) {
    var accessToken = this.getAccessToken();

    if (!empty(accessToken)) {
        return this.post("/v1/payments?access_token=" + encodeURI(accessToken), paymentData, "application/json");
    }

    return null;
};

/**
 * Returns payment information for webhooks from Mercadopago
 * @param {string} id - id of payment
 * @return {Object} payment information from Mercadopago
 */
MercadoPago.prototype.getPaymentInfoWebhook = function (id) {
    var accessToken = this.getAccessToken();

    if (!empty(accessToken)) {
        return this.get("/v1/payments/" + id + "?access_token=" + encodeURI(accessToken), "application/json");
    }

    return null;
};

/**
 * Creates an authenticated customer on Mercadopago side.
 * @param {Object} customerData - Object containing customer's email
 * @return {Object} create customer event result from Mercadopago
 */
MercadoPago.prototype.createCustomer = function (customerData) {
    var accessToken = this.getAccessToken();

    if (!empty(accessToken)) {
        return this.post("/v1/customers?access_token=" + encodeURI(accessToken), customerData, "application/json");
    }

    return null;
};

/**
 * Creates an authenticated customer on Mercadopago side.
 * @param {Object} filter - Object containing customer's email
 * @return {Object} customer on Mercadopago with matched email
 */
MercadoPago.prototype.searchCustomer = function (filter) {
    var accessToken = this.getAccessToken();
    var filters = this.buildquery(filter);

    if (!empty(accessToken)) {
        return this.get("/v1/customers/search?" + filters + "&access_token=" + encodeURI(accessToken), "application/json");
    }

    return null;
};

/**
 * Creates an authenticated customer on Mercadopago side.
 * @param {Object} customerID - Object containing customer's email
 * @param {Object} cardToken - Object containing credit card token
 * @return {Object} customer on Mercadopago with matched email
 */
MercadoPago.prototype.createCustomerCard = function (customerID, cardToken) {
    var accessToken = this.getAccessToken();

    if (!empty(accessToken)) {
        return this.post("/v1/customers/" + customerID + "/cards?access_token=" + encodeURI(accessToken), cardToken, "application/json");
    }

    return null;
};

/**
 * Creates test user for given site id.
 * @param {string} siteID - ID of the site
 * @return {Object} result from
 */
MercadoPago.prototype.createTestCustomer = function (siteID) {
    var accessToken = this.getAccessToken();

    if (!empty(accessToken)) {
        return this.post("/users/test_user?access_token=" + encodeURI(accessToken), siteID, "application/json");
    }

    return null;
};

/**
 * Gets notifications for payment from Mercadopago
 * @param {string} id - id of payment
 * @return {Object} notification for requested payment.
 */
MercadoPago.prototype.getPaymentInfo = function (id) {
    var accessToken = this.getAccessToken();

    if (!empty(accessToken)) {
        return this.get("/v1/payments/" + id + "?access_token=" + encodeURI(accessToken), "application/json");
    }

    return null;
};

/**
 * Gets paymentInstrument from order
 * @param {Order} order - order of payment
 * @return {PaymentInstrument} the payment instrument
 */
MercadoPago.prototype.getPaymentInstrument = function (order) {
    return order.getPaymentInstruments().toArray().filter(function (payInstrument) {
        return payInstrument.paymentMethod === Resource.msg("payment.method.id", "mercadoPagoPreferences", null);
    })[0];
};

/**
 * Gets order information from Mercadopago with given ID
 * @param {string} id - id of Order
 * @return {Object} order list from Mercadopago.
 */
MercadoPago.prototype.getMerchantOrderInfo = function (id) {
    var accessToken = this.getAccessToken();

    if (!empty(accessToken)) {
        return this.get("/merchant_orders/" + id + "?access_token=" + encodeURI(accessToken), "application/json");
    }

    return null;
};

/**
 * Gets authorized payment information
 * @param {string} id - id of Payment
 * @return {Object} payments list from Mercadopago.
 */
MercadoPago.prototype.getAuthorizedPayment = function (id) {
    var accessToken = this.getAccessToken();

    if (!empty(accessToken)) {
        return this.get("/authorized_payments/" + id + "?access_token=" + encodeURI(accessToken), "application/json");
    }

    return null;
};

/**
 * Refunds payment
 * @param {string} id - id of Payment
 * @return {Object} refund payment response
 */
MercadoPago.prototype.refundPayment = function (id) {
    var accessToken = this.getAccessToken();
    var refundStatus = {
        status: "refunded"
    };

    if (!empty(accessToken)) {
        return this.put("/v1/payments/" + id + "?access_token=" + encodeURI(accessToken), refundStatus, "application/json");
    }

    return null;
};

/**
 * Cancels payment
 * @param {string} id - id of Payment
 * @return {Object} Cancel payment response
 */
MercadoPago.prototype.cancelPayment = function (id) {
    var accessToken = this.getAccessToken();
    var cancelStatus = {
        status: "cancelled"
    };

    if (!empty(accessToken)) {
        return this.put("/v1/payments/" + id + "?access_token=" + encodeURI(accessToken), cancelStatus, "application/json");
    }

    return null;
};

/**
 * Cancels payment
 * @param {string} id - id of Payment
 * @return {Object} Cancel pre-aproval response
 */
MercadoPago.prototype.cancelPreaprovalPayment = function (id) {
    var accessToken = this.getAccessToken();
    var cancelStatus = {
        status: "cancelled"
    };

    if (!empty(accessToken)) {
        return this.put("/preapproval/" + id + "?access_token=" + encodeURI(accessToken), cancelStatus, "application/json");
    }

    return null;
};

/**
 * Creates preference
 * @param {Object} preference - Object containing shipping and order info, return urls
 * @return {Object} create preference response
 */
MercadoPago.prototype.createPreference = function (preference) {
    var accessToken = this.getAccessToken();

    if (!empty(accessToken)) {
        return this.post("/checkout/preferences?access_token=" + encodeURI(accessToken), preference, "application/json");
    }

    return null;
};

/**
 * Generates access token based on clientId and clientSecret
 * @return {Object} generated access token from Mercadopago
 */
MercadoPago.prototype.getPreferenceAccessToken = function () {
    var credential = this.service.getConfiguration().getCredential();
    var clientId = credential.custom.jsonData.clientId || "";
    var clientSecret = credential.custom.jsonData.clientSecret || "";

    return this.post("/oauth/token", {
        client_id: clientId,
        client_secret: clientSecret,
        grant_type: "client_credentials"
    }, "application/json");
};

/**
 * Create preference based on token generated using clientId and clientSecret
 * @param {Object} preference - Object containing shipping and order info, return urls
 * @return {Object} preference object created by token generated.
 */
MercadoPago.prototype.createPreferenceObject = function (preference) {
    var accessToken = (this.getPreferenceAccessToken() || {}).access_token;

    if (!empty(accessToken)) {
        return this.post("/checkout/preferences?access_token=" + encodeURI(accessToken), preference, "application/json");
    }

    return null;
};

/**
 * Create preference based on token generated using clientId and clientSecret
 * @param {Object} order - Payment Order
 * @return {Object} preference object created by token generated.
 */
MercadoPago.prototype.createPreferenceUrl = function (order) {
    if (empty(order)) {
        return null;
    }

    var backUrl = URLUtils.httpsContinue().toString();
    var items = [];
    var payer = {};

    order.allProductLineItems.toArray().forEach(function (productLineItem) {
        var item = {
            id: productLineItem.productID,
            title: productLineItem.product.name,
            quantity: productLineItem.quantityValue,
            currency_id: session.getCurrency().getCurrencyCode(),
            unit_price: productLineItem.getProratedPrice().divide(productLineItem.getQuantity().getValue()).getValue()
        };

        if (!empty(productLineItem.product.longDescription)) {
            item.description = productLineItem.product.longDescription;
        }

        if (!empty(productLineItem.product.primaryCategory)) {
            item.category_id = productLineItem.product.primaryCategory.displayName;
        }

        items.push(item);
    });

    // add shipment costs as item
    if (order.getAdjustedShippingTotalPrice() > 0) {
        var shippingItem = {
            id: order.defaultShipment.shippingMethod.ID,
            title: order.defaultShipment.shippingMethod.displayName,
            quantity: 1,
            currency_id: session.getCurrency().getCurrencyCode(),
            unit_price: order.getAdjustedShippingTotalPrice().getValue()
        };

        items.push(shippingItem);
    }

    payer.name = order.billingAddress.firstName;
    payer.surname = order.billingAddress.lastName;
    payer.email = order.customerEmail;
    payer.phone = payer.phone || {};
    payer.phone.area_code = "-";
    payer.phone.number = order.billingAddress.phone;
    payer.address = payer.address || {};
    payer.address.street_name = order.billingAddress.address1 + "-" + order.billingAddress.city + "-" + order.billingAddress.countryCode;
    payer.address.street_number = "0";
    payer.address.zip_code = order.billingAddress.postalCode;

    var createPreferenceResponse = this.createPreferenceObject({
        external_reference: order.orderNo,
        items: items,
        payer: payer,
        shipments: {
            receiver_address: {
                apartment: "-",
                floor: "-",
                street_name: order.defaultShipment.shippingAddress.address1 + "-" + order.defaultShipment.shippingAddress.city + "-" + order.defaultShipment.shippingAddress.countryCode,
                street_number: "0",
                zip_code: order.defaultShipment.shippingAddress.postalCode
            },
            mode: "not_specified",
            free_shipping: false
        },
        back_urls: {
            success: backUrl,
            pending: backUrl,
            failure: backUrl
        },
        auto_return: "all",
        installments: 1,
        transaction_amount: order.getTotalGrossPrice().getValue()
    });

    return this.sandboxEnabled ? createPreferenceResponse.sandbox_init_point : createPreferenceResponse.init_point;
};

/**
 * Creates DW Transaction for given Order to save.
 * @param {Objet} order - Order for which transaction will be created
 */
MercadoPago.prototype.createTransaction = function (order) {
    if (empty(order)) {
        return;
    }

    var transactionID = request.httpParameterMap.collection_id.getStringValue();
    var paymentInstrument;
    var paymentProcessor;

    // Get payment instrument
    order.getPaymentInstruments("MercadoPago").toArray().forEach(function (payInstrument) {
        paymentInstrument = payInstrument;
        paymentProcessor = require("dw/order/PaymentMgr").getPaymentMethod(payInstrument.paymentMethod).paymentProcessor;
    });

    // Set payment proccessor
    if (!empty(paymentInstrument)) {
        paymentInstrument.paymentTransaction.paymentProcessor = paymentProcessor;
    }

    // Set transaction id
    paymentInstrument.paymentTransaction.transactionID = transactionID;

    // Set order payment status to paid if order is authorized
    order.setPaymentStatus(require("dw/order/Order").PAYMENT_STATUS_PAID);
};

/**
 * Updates preference.
 * @param {string} id - ID of the payment
 * @param {Object} preference - Object containing shipping and order info, return urls
 * @return {Object} create preference response
 */
MercadoPago.prototype.updatePreference = function (id, preference) {
    var accessToken = this.getAccessToken();

    if (!empty(accessToken)) {
        return this.put("/checkout/preferences/" + id + "?access_token=" + encodeURI(accessToken), preference, "application/json");
    }

    return null;
};

/**
 * Searches payment with constructed filter.
 * @param {Objet} filters - object containing filter parameters, which will be used to create the query
 * @param {string} offset - offset number
 * @param {string} limit - limit number
 * @return {Object} search result with provided filter parameters in URL
 */
MercadoPago.prototype.searchPayment = function (filters, offset, limit) {
    var accessToken = this.getAccessToken();
    var uriPrefix = this.sandboxEnabled ? "/sandbox" : "";

    var filtersObj = {};
    filtersObj.offset = !empty(offset) ? offset : "0";
    filtersObj.limit = !empty(offset) ? limit : "0";
    var filtersString = this.buildquery(filtersObj);

    if (!empty(accessToken)) {
        return this.get(uriPrefix + "/collections/search?" + filtersString + "&access_token=" + encodeURI(accessToken), "application/json");
    }

    return null;
};

/**
 * Gets preference
 * @param {string} id - Id of the preference
 * @returns {Object} - Result of the get preference call
 */
MercadoPago.prototype.getPreference = function (id) {
    var accessToken = this.getAccessToken();

    if (!empty(accessToken)) {
        return this.get("/checkout/preferences/" + id + "?access_token=" + encodeURI(accessToken), "application/json");
    }

    return null;
};

/**
 * Creates pre-approval payment
 * @param {Object} preaprovalpayment - desc
 * @returns {Object} - Result of the get createPreaprovalPayment call
 */
MercadoPago.prototype.createPreaprovalPayment = function (preaprovalpayment) {
    var accessToken = this.getAccessToken();

    if (!empty(accessToken)) {
        return this.post("/preapproval?access_token=" + encodeURI(accessToken), preaprovalpayment, "application/json");
    }

    return null;
};

/**
 * Gets pre-approval payment
 * @param {Object} id - ID of the preapproval
 * @returns {Object} - Result of the get createPreaprovalPayment call
 */
MercadoPago.prototype.getPreaprovalPayment = function (id) {
    var accessToken = this.getAccessToken();

    if (!empty(accessToken)) {
        return this.get("/preapproval/" + id + "?access_token=" + encodeURI(accessToken), "application/json");
    }

    return null;
};

/**
 * Updates pre-aproval payment
 * @param {Object} id - ID of the preapproval
 * @param {Object} preaprovalpayment - desc
 * @returns {Object} - Result of the get createPreaprovalPayment call
 */
MercadoPago.prototype.updatePreaprovalPayment = function (id, preaprovalpayment) {
    var accessToken = this.getAccessToken();

    if (!empty(accessToken)) {
        return this.put("/preapproval/" + id + "?access_token=" + encodeURI(accessToken), preaprovalpayment, "application/json");
    }

    return null;
};

/**
 * Gets total amount for Line Item
 * @param {Object} lineItemCtnr - Line item container for which total amount will be retrieved
 * @returns {number} - total amount of the item
 */
function getTotalAmount(lineItemCtnr) {
    var totalAmount = lineItemCtnr.getTotalGrossPrice();

    lineItemCtnr.getGiftCertificatePaymentInstruments().toArray().forEach(function (item) {
        if (item.paymentTransaction && item.paymentTransaction.amount) {
            totalAmount = totalAmount.subtract(item.paymentTransaction.amount);
        }
    });

    return totalAmount;
}

MercadoPago.prototype.getTotalAmount = getTotalAmount;

/**
 * Constructs payment data object to verify the payment on Mercadopago
 * @param {Order} order - Order Object
 * @param {Object} customer - Customer Object
 * @param {Object} installments - Number of installments
 * @param {string} issuerId - ID of the issuer
 * @param {string} customerID - ID of the customer
 * @return {Object} generated payment data to use in payment call
 */
MercadoPago.prototype.createPaymentData = function (order, customer, installments, issuerId, customerID) {
    // Construct product line items
    var items = collections.map(order.allProductLineItems, function (prodLineItem) {
        var item = {};

        item.id = prodLineItem.productID;
        item.title = prodLineItem.product.name;

        if (prodLineItem.product.longDescription) {
            item.description = prodLineItem.product.longDescription.markup;
        }

        if (prodLineItem.product.primaryCategory) {
            item.category_id = prodLineItem.product.primaryCategory.displayName;
        }

        item.quantity = prodLineItem.quantityValue;
        item.unit_price = prodLineItem.adjustedGrossPrice.value;

        return item;
    });

    // Payer billing information for payer
    var payer = {
        address: {
            street_name: order.billingAddress.address1 + "-" + order.billingAddress.city + "-" + order.billingAddress.countryCode,
            street_number: "0",
            zip_code: order.billingAddress.postalCode
        },
        first_name: order.billingAddress.firstName,
        last_name: order.billingAddress.lastName,
        phone: {
            area_code: "-",
            number: order.billingAddress.phone
        }
    };

    // Customer information
    var paymentPayer = {};
    if (customerID) {
        paymentPayer.id = customerID; // Required for registered customer
        paymentPayer.type = "customer";
    }

    paymentPayer.email = order.customerEmail;

    if (customer.isAuthenticated()) {
        payer.registration_date = customer.profile.getCreationDate();
    }

    // Payment method id and token
    var paymentMethodId;
    var token;
    var docType;
    var docNumber;
    var paymentTypeId;
    var financialInstitution;

    collections.forEach(order.getPaymentInstruments(), function (payInstrument) {
        if (payInstrument.paymentMethod !== Resource.msg("payment.method.id", "mercadoPagoPreferences", null)) {
            return;
        }

        paymentMethodId = payInstrument.creditCardType;
        token = payInstrument.creditCardToken;
        docType = payInstrument.custom.customerDocType;
        docNumber = payInstrument.custom.customerDocNumber;
        financialInstitution = payInstrument.custom.mercadoPagoFinancialInstitution;
        paymentTypeId = payInstrument.custom.mercadoPagoPaymentTypeId;
    });

    if (!token) {
        paymentPayer.first_name = order.billingAddress.firstName;
        paymentPayer.last_name = order.billingAddress.lastName;

        if (docType && docNumber) {
            paymentPayer.identification = {
                type: docType,
                number: docNumber
            };
        }
    }

    var transactionAmount = getTotalAmount(order);

    var payDataObj = {
        payer: paymentPayer,
        external_reference: order.orderNo,
        additional_info: {
            items: items,
            payer: payer,
            shipments: {
                receiver_address: {
                    apartment: "-",
                    floor: "-",
                    street_name: order.defaultShipment.shippingAddress.address1 + "-" + order.defaultShipment.shippingAddress.city + "-" + order.defaultShipment.shippingAddress.countryCode,
                    street_number: "0",
                    zip_code: order.defaultShipment.shippingAddress.postalCode
                }
            }
        },
        installments: 1,
        payment_method_id: paymentMethodId, // Required
        token: token, // Required only for credit card payments
        transaction_amount: transactionAmount.value,
        notification_url: URLUtils.https("MercadoPago-MercadoPagoPaymentNotification").toString()
    };

    // Set issuer id
    if (issuerId !== 0) {
        payDataObj.issuer_id = issuerId;
    }

    // Set installments if they are greater than 1
    if (installments !== 1) {
        payDataObj.installments = installments;
    }

    if (paymentTypeId === "bank_transfer" || paymentTypeId === "ticket") {
        payDataObj.callback_url = URLUtils.https("MercadoPago-MercadoPagoReturnURL", "ID", order.orderNo, "token", order.orderToken).toString();
        payDataObj.payer.entity_type = "individual";
        payDataObj.additional_info.ip_address = request.httpHeaders["x-is-remote_addr"];
    }

    if (paymentMethodId === "pse") {
        payDataObj.transaction_details = {
            financial_institution: financialInstitution
        };
    }

    if (paymentMethodId === "webpay") {
        payDataObj.transaction_details = {
            financial_institution: 1234 // harcoded by documentation
        };
    }
    var localeID = order.customerLocaleID;
    var siteDefaultLocaleID = Site.getCurrent().getDefaultLocale();
    if (MercadoPagoHelper.getPreferences().enableSendTaxes && (localeID === "es_CO" || (localeID === "default" && siteDefaultLocaleID === "es_CO"))) {
        payDataObj.net_amount = order.getTotalNetPrice().getValue();
        payDataObj.taxes = [{
            value: order.getTotalTax().getValue(),
            type: "IVA"
        }];
    }

    return payDataObj;
};

/**
 * @description Parse and returns the status of Order
 * @param {string} status - Response status from Mercado Pago
 * @returns {string} returnStatus classified status as string result
 */
MercadoPago.prototype.parseOrderStatus = function (status) {
    var pendingStatuses = {
        pending: Resource.msg("status.pending", "mercadoPago", null),
        in_process: Resource.msg("status.in_process", "mercadoPago", null),
        in_mediation: Resource.msg("status.in_mediation", "mercadoPago", null)
    };
    var approvedStatuses = {
        approved: Resource.msg("status.approved", "mercadoPago", null)
    };
    var rejectedStatuses = {
        rejected: Resource.msg("status.rejected", "mercadoPago", null),
        cancelled: Resource.msg("status.cancelled", "mercadoPago", null)
    };
    var returnStatus = pendingStatuses[status] ? "pending" : "";

    if (!returnStatus) {
        returnStatus = approvedStatuses[status] ? "authorized" : "";
    }

    if (!returnStatus) {
        returnStatus = rejectedStatuses[status] ? "declined" : "";
    }

    return returnStatus;
};

/**
 * @description Get stored cards on Mercadopago side with current authenticated customer
 * @param {Object} currentCustomer - current authenticated customer
 * @returns {Array} array of objects which holds customer cards data
 */
MercadoPago.prototype.getCustomerCards = function (currentCustomer) {
    if (!currentCustomer.raw.authenticated || !currentCustomer.raw.registered) {
        return null;
    }

    var customerSearch = this.searchCustomer({ email: currentCustomer.profile.email });

    if (!customerSearch) {
        return null;
    }

    var foundCustomer = array.find(customerSearch.results, function (customer) {
        return customer.email === currentCustomer.profile.email;
    });

    if (!foundCustomer || !foundCustomer.cards.length) {
        return null;
    }

    return foundCustomer.cards;
};

MercadoPago.prototype.getOtherPaymentMode = function () {
    return MercadoPagoHelper.getPreferences().otherPaymentMode;
};

module.exports = MercadoPago;
