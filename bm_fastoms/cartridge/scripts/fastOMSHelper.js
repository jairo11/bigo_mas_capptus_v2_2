/**
 *  Property of OSF Global Services, Inc., (with its brand OSF Commerce). OSF remains the sole owner of all right, title and interest in the software.
 *  Do not copy, sell, reverse engineer or otherwise attempt to derive or obtain information about the functioning, manufacture or operation therein.
 */

/* eslint-disable valid-jsdoc, require-jsdoc, eqeqeq, guard-for-in, new-cap, no-use-before-define, no-mixed-operators, consistent-return, */

"use strict";

var Order = require("dw/order/Order");
var BasketMgr = require("dw/order/BasketMgr");
var OrderMgr = require("dw/order/OrderMgr");
var Template = require("dw/util/Template");
var PaymentMgr = require("dw/order/PaymentMgr");
var ProductMgr = require("dw/catalog/ProductMgr");
var HookMgr = require("dw/system/HookMgr");
var Site = require("dw/system/Site");
var Status = require("dw/system/Status");
var Transaction = require("dw/system/Transaction");
var Resource = require("dw/web/Resource");
var URLUtils = require("dw/web/URLUtils");
var FOMSLogger = require("dw/system/Logger").getLogger("FastOMS", "FastOMS");
var Decimal = require("dw/util/Decimal");
var Currency = require("dw/util/Currency");
var Cipher = require("dw/crypto/Cipher");
var CustomerMgr = require("dw/customer/CustomerMgr");
var Money = require("dw/value/Money");
var TaxMgr = require("dw/order/TaxMgr");
var GiftCertificateMgr = require("dw/order/GiftCertificateMgr");
var GiftCertificate = require("dw/order/GiftCertificate");
var GiftCertificateStatusCodes = require("dw/order/GiftCertificateStatusCodes");
var PaymentInstrument = require("dw/order/PaymentInstrument");
var ArrayList = require("dw/util/ArrayList");
var CustomObjectMgr = require("dw/object/CustomObjectMgr");
var CouponMgr = require("dw/campaign/CouponMgr");
var CouponStatusCodes = require("dw/campaign/CouponStatusCodes");
var couponManual = require("*/cartridge/scripts/couponManual");
var couponAutomation = require("*/cartridge/scripts/couponAutomation");
var siteName = Site.getCurrent().getName();

/**
 * Return the Payment Integration Manager
 */
function getPaymentManager(order) {
    var paymentIntegration = require("*/cartridge/controllers/PaymentIntegration/PaymentIntegration.js");
    var paymentProcessorID = null;
    /* eslint-disable no-restricted-syntax */
    for (var key in order.paymentInstruments) {
        var paymentInstrument = order.paymentInstruments[key];
        if (paymentInstrument.getPaymentMethod() != dw.order.PaymentInstrument.METHOD_GIFT_CERTIFICATE) {
            paymentProcessorID = dw.order.PaymentMgr.getPaymentMethod(paymentInstrument.paymentMethod).getPaymentProcessor().getID();
        }
    }
    /* eslint-enable no-restricted-syntax */

    if (paymentIntegration[paymentProcessorID]) {
        return paymentIntegration[paymentProcessorID].GetManager();
    }
    return null;
}

/**
 * return Card Payment Instrument
 */
function getCardPaymentInstrument(order) {
    var cardPaymentInstrument = null;
    /* eslint-disable no-restricted-syntax */
    for (var key in order.paymentInstruments) {
        var paymentInstrument = order.paymentInstruments[key];
        if (paymentInstrument.getPaymentMethod() == dw.order.PaymentInstrument.METHOD_GIFT_CERTIFICATE) {
            continue;
        }
        if (!paymentInstrument.getCreditCardType() && !paymentInstrument.getCreditCardToken()) {
            continue;
        }
        cardPaymentInstrument = paymentInstrument;
        break;
    }
    /* eslint-enable no-restricted-syntax */
    return cardPaymentInstrument;
}

/**
 * Return the Payment Manager Transaction
 */
function getPaymentManagerTransaction(order) {
    var paymentTransaction;
    /* eslint-disable no-restricted-syntax */
    for (var key in order.paymentInstruments) {
        var paymentInstrument = order.paymentInstruments[key];
        if (paymentInstrument.getPaymentMethod() != dw.order.PaymentInstrument.METHOD_GIFT_CERTIFICATE) {
            paymentTransaction = paymentInstrument.paymentTransaction;
        }
    }
    /* eslint-enable no-restricted-syntax */

    return paymentTransaction;
}

/**
 * Gets return information for the given product or null if N/A
 */
function getReturnInfo(order, pid) {
    var returnInfo = null;
    var returnProductsInfo = JSON.parse(order.custom.LOMSReturnItems);
    var pendingReturnProducts = returnProductsInfo && returnProductsInfo.items;
    var refundedProducts = JSON.parse(order.custom.LOMSRefundedItems);

    if (pendingReturnProducts) {
        //Find pending return products total quantity related pid
        var pendingReturnProductQuantity = 0;
        var confirmedReturnProductQuantity = 0;
        pendingReturnProducts.forEach(function (pendingReturnProduct) {
            if (pendingReturnProduct.pid === pid) {
                returnInfo = pendingReturnProduct;
                returnInfo.fromReturn = true;
                returnInfo.fromRefunded = true;
                if (pendingReturnProduct.ret) {
                    confirmedReturnProductQuantity += 1;
                } else {
                    pendingReturnProductQuantity += 1;
                }
            }
        });
        returnInfo.pendingReturnProductQuantity = pendingReturnProductQuantity;
        returnInfo.confirmedReturnProductQuantity = confirmedReturnProductQuantity;
    }

    if (!returnInfo && refundedProducts) {
        for (var keyRefunded in refundedProducts) {
            var item = refundedProducts[keyRefunded];
            if (item.pid == pid) {
                returnInfo = item;
                returnInfo.fromRefunded = true;
                break;
            }
        }
    }

    return returnInfo;
}

/**
 * Returns a list of unique values for that Variation Attribute. Will favour available for shopping variants.
 */
function filterVariants(variants, VA, fixedAttributes) {
    var processedIDs = [];
    var filteredVariants = [];
    var allowReplace = [];
    /* eslint-disable no-restricted-syntax */
    for (var keyVariant in variants) {
        var variant = variants[keyVariant];
        var currentValue = variant.custom[VA.attributeID];
        var allowedVariant = true;

        if (fixedAttributes != null) {
            for (var keyAttributes in fixedAttributes) {
                var fa = fixedAttributes[keyAttributes];
                var variantValue = variant.custom[fa.ID];
                if (typeof variantValue === "undefined") {
                    allowedVariant = false;
                    break;
                } else if (variantValue != fa.value) { // NOSONAR
                    allowedVariant = false;
                    break;
                }
            }
        }
        /* eslint-disable no-continue */
        if (!allowedVariant) {
            continue;
        }
        /* eslint-enable no-continue */

        if (processedIDs.indexOf(currentValue) == -1) {
            processedIDs.push(currentValue);
            filteredVariants.push(variant);

            if (!variant.availabilityModel.inStock) {
                allowReplace.push({
                    variant: variant,
                    VAID: currentValue
                });
            }
        } else {
            /* eslint-disable no-loop-func */
            var existingVariantOutOfStock = allowReplace.filter(function (el) {
                return el.VAID == currentValue;
            });
            /* eslint-enable no-restricted-syntax */
            if (existingVariantOutOfStock.length > 0) {
                var index;
                if (variant.availabilityModel.inStock) {
                    var variantToRemove = existingVariantOutOfStock[0];
                    index = filteredVariants.indexOf(variantToRemove);

                    filteredVariants.splice(index, 1);
                    filteredVariants.push(variant);
                }

                index = allowReplace.indexOf(existingVariantOutOfStock);
                allowReplace.splice(index, 1);
            }
        }
    }
    /* eslint-enable no-restricted-syntax */

    return filteredVariants;
}


/**
 * refresh Stock
 */
function refreshStock(product, fixedAttributes, variationAttribute) {
    return filterVariants(product.masterProduct.getVariants(), variationAttribute, fixedAttributes);
}


/**
 * return Amount O fBonus Products
 */
function getAmountOfBonusProductsByPid(lineItemCntr, pid) {
    var amoutnOfProducts = 0;
    for (var i = 0; i < lineItemCntr.bonusLineItems.length; i++) {
        if (lineItemCntr.bonusLineItems[i].productID == pid) {
            amoutnOfProducts++;
        }
    }
    return amoutnOfProducts;
}

/**
 * get Added Bonus Items
 */
function getAddedBonusItems(lineItemCntr, newLineItemCntr) {
    var addedBonusLineItems = [];
    for (var i = 0; i < newLineItemCntr.bonusLineItems.length; i++) {
        var foundNewItem = true;
        if (lineItemCntr.bonusLineItems.length == 0) {
            addedBonusLineItems.push(newLineItemCntr.bonusLineItems[i].productName);
        } else {
            for (var j = 0; j < lineItemCntr.bonusLineItems.length; j++) {
                if (newLineItemCntr.bonusLineItems[i].productID == lineItemCntr.bonusLineItems[j].productID) {
                    foundNewItem = false;
                }
                if (foundNewItem == false) {
                    var amoutnOfNewProducts = getAmountOfBonusProductsByPid(newLineItemCntr, newLineItemCntr.bonusLineItems[i].productID);
                    var amountOfExistingProducts = getAmountOfBonusProductsByPid(lineItemCntr, lineItemCntr.bonusLineItems[j].productID);
                    if (amoutnOfNewProducts > amountOfExistingProducts) {
                        addedBonusLineItems.push(newLineItemCntr.bonusLineItems[i].productName);
                    }
                } else {
                    addedBonusLineItems.push(newLineItemCntr.bonusLineItems[i].productName);
                }
            }
        }
    }
    return addedBonusLineItems;
}

/**
 * return Lost(deleted) Bonus Items
 */
function getLostBonusItems(lineItemCntr, newLineItemCntr) {
    var lostBonusLineItems = [];
    for (var i = 0; i < lineItemCntr.bonusLineItems.length; i++) {
        var foundLostItem = true;
        if (newLineItemCntr.bonusLineItems.length == 0) {
            lostBonusLineItems.push(lineItemCntr.bonusLineItems[i].productName);
        } else {
            for (var j = 0; j < newLineItemCntr.bonusLineItems.length; j++) {
                if (newLineItemCntr.bonusLineItems[j].productID == lineItemCntr.bonusLineItems[i].productID) {
                    foundLostItem = false;
                }
                if (foundLostItem == false) {
                    var amoutnOfNewProducts = getAmountOfBonusProductsByPid(newLineItemCntr, newLineItemCntr.bonusLineItems[j].productID);
                    var amountOfExistingProducts = getAmountOfBonusProductsByPid(lineItemCntr, lineItemCntr.bonusLineItems[i].productID);
                    if (amoutnOfNewProducts < amountOfExistingProducts) {
                        lostBonusLineItems.push(lineItemCntr.bonusLineItems[i].productName);
                    }
                } else {
                    lostBonusLineItems.push(lineItemCntr.bonusLineItems[i].productName);
                }
            }
        }
    }
    return lostBonusLineItems;
}

/**
 * detect if Promotion Based On Coupons
 */
function isPromotionBasedOnCoupons(promotion) {
    return (promotion.basedOnCoupon == false && promotion.basedOnCoupons == false && promotion.basedOnSourceCodes == false);
}

/**
 * return Amount Of Bonus Discounts
 */
function getAmountOfBonusDiscountsByPromotionID(lineItemCntr, promotionID) {
    var amountOfBonusDiscounts = 0;
    for (var i = 0; i < lineItemCntr.bonusDiscountLineItems.length; i++) {
        if (lineItemCntr.bonusDiscountLineItems[i].promotionID == promotionID) {
            amountOfBonusDiscounts++;
        }
    }
    return amountOfBonusDiscounts;
}

/**
 * remove Duplicates from array
 */
function removeDuplicates(firstArray, secondArray) {
    for (var i = 0; i < firstArray.length; i++) {
        for (var j = 0; j < secondArray.length; j++) {
            if (firstArray[i] == secondArray[j]) {
                firstArray.splice(i, 1);
                i--;
                secondArray.splice(j, 1);
                j--;
            }
        }
    }
}

/**
 * return Added Discount Line Items
 */
function getAddedDiscountLineItems(lineItemCntr, newLineItemCntr) {
    var addedDiscountLineItems = [];
    for (var i = 0; i < newLineItemCntr.bonusDiscountLineItems.length; i++) {
        if (!isPromotionBasedOnCoupons(newLineItemCntr.bonusDiscountLineItems[i].promotion)) {
            var foundNewDiscount = true;
            for (var j = 0; j < lineItemCntr.bonusDiscountLineItems.length; j++) {
                if (newLineItemCntr.bonusDiscountLineItems[i].promotionID == lineItemCntr.bonusDiscountLineItems[j].promotionID) {
                    foundNewDiscount = false;
                }
                if (foundNewDiscount == false) {
                    var amoutnOfNewBonusDiscounts = getAmountOfBonusDiscountsByPromotionID(newLineItemCntr, newLineItemCntr.bonusDiscountLineItems[i].promotionID);
                    var amountOfExistingBonusDiscounts = getAmountOfBonusDiscountsByPromotionID(lineItemCntr, lineItemCntr.bonusDiscountLineItems[j].promotionID);
                    if (amoutnOfNewBonusDiscounts > amountOfExistingBonusDiscounts) {
                        addedDiscountLineItems.push(newLineItemCntr.bonusDiscountLineItems[i].promotion.description);
                    }
                } else {
                    addedDiscountLineItems.push(newLineItemCntr.bonusDiscountLineItems[i].promotion.description);
                }
            }
        }
    }
    return addedDiscountLineItems;
}

/**
 * return Lost(deleted) Discount Line Items
 */
function getLostDiscountLineItems(lineItemCntr, newLineItemCntr) {
    var lostDiscountLineItems = [];
    for (var i = 0; i < lineItemCntr.bonusDiscountLineItems.length; i++) {
        if (!isPromotionBasedOnCoupons(lineItemCntr.bonusDiscountLineItems[i].promotion)) {
            var foundLostDiscount = true;
            for (var j = 0; j < lineItemCntr.bonusDiscountLineItems.length; j++) {
                if (newLineItemCntr.bonusDiscountLineItems[j].productID == lineItemCntr.bonusDiscountLineItems[i].productID) {
                    foundLostDiscount = false;
                }
                if (foundLostDiscount == false) {
                    var amoutnOfNewBonusDiscounts = getAmountOfBonusDiscountsByPromotionID(newLineItemCntr, newLineItemCntr.bonusDiscountLineItems[j].promotionID);
                    var amountOfExistingBonusDiscounts = getAmountOfBonusDiscountsByPromotionID(lineItemCntr, lineItemCntr.bonusDiscountLineItems[i].promotionID);
                    if (amoutnOfNewBonusDiscounts < amountOfExistingBonusDiscounts) {
                        lostDiscountLineItems.push(lineItemCntr.bonusDiscountLineItems[i].promotion.description);
                    }
                } else {
                    lostDiscountLineItems.push(lineItemCntr.bonusDiscountLineItems[i].promotion.description);
                }
            }
        }
    }
    return lostDiscountLineItems;
}

/**
 * Returns a product variation that is not already in basket
 */
function getProductVariantion(productToAdd, existingProducts) {
    var product = ProductMgr.getProduct(productToAdd);
    var isInStock = false;
    var isProductInBasket = false;

    // If product is standard product
    if (empty(product.variants)) {
        isProductInBasket = existingProducts.indexOf(product.ID) != -1;
        isInStock = product.availabilityModel.inStock || product.availabilityModel.availabilityStatus === "PREORDER";
        if (!isProductInBasket && isInStock) {
            return { product: product };
        }
    } else { // If product is variant product
        for (var keyPossibleVariants in product.variants) {
            var possibleVariant = product.variants[keyPossibleVariants];
            isProductInBasket = existingProducts.indexOf(possibleVariant.ID) != -1;
            isInStock = possibleVariant.availabilityModel.inStock || possibleVariant.availabilityModel.availabilityStatus === "PREORDER";
            if (!isProductInBasket && isInStock) {
                return { product: possibleVariant };
            }
        }
    }
    /* eslint-enable no-restricted-syntax */

    return {
        isProductInBasket: isProductInBasket,
        isInStock: isInStock
    };
}

/**
 * Retrieves the list of price adjustments
 */
function getValues(lineItemCtnr) {
    var taxPolicy = TaxMgr.getTaxationPolicy();

    var totals = {
        products: 0.00,
        gifts: 0.00,
        shipping: 0.00,
        shippingTax: 0.00,
        oldShipping: 0.00,
        discount: 0.00,
        lineItemsPriceInfo: []
    };
    /* eslint-disable no-restricted-syntax */
    lineItemCtnr.allLineItems.toArray().forEach(function (lineItem) {
        switch (true) {
            case lineItem instanceof dw.order.ProductLineItem:
                totals.products += lineItem.priceValue;
                break;

            case lineItem instanceof dw.order.GiftCertificateLineItem:
                totals.gifts += lineItem.priceValue;
                break;

            case lineItem instanceof dw.order.ShippingLineItem:
                if (lineItem.priceValue == 0) {
                    totals.oldShipping += lineItem.adjustedGrossPrice.value;
                }
                totals.shipping += lineItem.priceValue;
                totals.shippingTax += lineItem.adjustedTax.value;
                break;

            case lineItem instanceof dw.order.PriceAdjustment:
                totals.discount += Math.abs(lineItem.priceValue);
                break;

            default:
                break;
        }
    });

    lineItemCtnr.productLineItems.toArray().forEach(function (productLineItem) {
        var productTotalGrossPrice = productLineItem.grossPrice.value;
        var productTotalNetPrice = productLineItem.netPrice.value;
        var unitProductPrice = taxPolicy === TaxMgr.TAX_POLICY_GROSS ? productTotalGrossPrice / productLineItem.quantityValue : productTotalNetPrice / productLineItem.quantityValue;
        var productLineItemID = productLineItem.productID;
        var shippingProductLineItemsPriceTotals = 0;
        var shippingProductLineItemsPriceNet = 0;
        var shippingProductLineItemsPriceTax = 0;
        var shippingLineItemText = "";
        var unitProductLineItemTax = productLineItem.tax.value / productLineItem.quantityValue;
        if (!empty(productLineItem.shippingLineItem)) {
            shippingLineItemText = productLineItem.shippingLineItem.lineItemText;
            shippingProductLineItemsPriceTotals = productLineItem.shippingLineItem.adjustedGrossPrice.value;
            shippingProductLineItemsPriceNet = productLineItem.shippingLineItem.priceValue;
            shippingProductLineItemsPriceTax = productLineItem.shippingLineItem.tax.value;
        }

        totals.lineItemsPriceInfo.push({
            productID: productLineItemID,
            productQuantity: productLineItem.quantityValue,
            productTotalNetPrice: productTotalNetPrice,
            productTotalGrossPrice: productTotalGrossPrice,
            unitProductPrice: unitProductPrice,
            unitProductLineItemTax: unitProductLineItemTax,
            shippingProductLineItemsPriceTotals: shippingProductLineItemsPriceTotals,
            shippingProductLineItemsPriceNet: shippingProductLineItemsPriceNet,
            shippingProductLineItemsPriceTax: shippingProductLineItemsPriceTax,
            shippingLineItemText: shippingLineItemText
        });
    });

    return totals;
}

/**
 * remove Shipment Value
 */
function removeShipmentValue(lineItemCtnr) {
    /* eslint-disable no-restricted-syntax */
    for (var key in lineItemCtnr.allLineItems) {
        var li = lineItemCtnr.allLineItems[key];
        if (li instanceof dw.order.ShippingLineItem) {
            li.createShippingPriceAdjustment("shippingFastOMSRefund", new dw.campaign.FixedPriceShippingDiscount(0));
        }
    }
    /* eslint-enable no-restricted-syntax */
}

/**
 * Do the Order Cancelation
 */
function orderCancelation(order, params) {
    var coupons = [];
    var giftCert = null;
    var status = null;

    Transaction.wrap(function () {
        status = OrderMgr.cancelOrder(order);

        if (status.status == Status.OK) {
            FOMSLogger.info("Order {0} CANCELLED", order.orderNo);
            order.setCancelCode(Order.ORDER_STATUS_CANCELLED);
            order.setCancelDescription(params.description);

            if (!order.getCouponLineItems().empty) {
                /* eslint-disable no-restricted-syntax */
                for (var key in order.couponLineItems) {
                    var couponLineItem = order.couponLineItems[key];
                    coupons.push(couponLineItem.couponCode);
                }
                /* eslint-enable no-restricted-syntax */
            }
            unlockOrder(order);
            request.setLocale(order.customerLocaleID);
            sendEmail("mail/orderCancellationEmail", order.customerEmail, Resource.msgf("mail.cancel.subject", "fastoms", null, siteName), { order: order, giftCert: giftCert, coupons: coupons.join() });
        }
    });
    return status;
}

/**
 * Send Email
 */
function sendEmail(emailTemplate, emailRecipient, emailSubject, emailContext) {
    if (!empty(emailRecipient)) {
        var Mail = require("dw/net/Mail");
        var HashMap = require("dw/util/HashMap");
        var context = new HashMap();
        var mail = new Mail();
        var template = new Template(emailTemplate);
        mail.addTo(emailRecipient);
        mail.setSubject(emailSubject);
        mail.setFrom(Site.getCurrent().getCustomPreferenceValue("customerServiceEmail") || "noreply@dsalesforce.com");
        /* eslint-disable no-restricted-syntax */
        for (var key in emailContext) {
            if (emailContext.hasOwnProperty(key)) { //eslint-disable-line
                context.put(key, emailContext[key]);
            }
        }
        /* eslint-enable no-restricted-syntax */

        context.CurrentForms = session.forms;
        context.CurrentHttpParameterMap = request.httpParameterMap;
        context.CurrentCustomer = customer;
        var content = template.render(context).text;
        mail.setContent(content, "text/html", "UTF-8");
        mail.send();
    }
}

/**
 * Gets non-gift paymentInstrument
 */
function getNonGiftPaymentInstrument(order) {
    var allPaymentInstruments = order.getPaymentInstruments();
    var nonGiftPI = null;
    /* eslint-disable no-restricted-syntax */
    for (var key in allPaymentInstruments) {
        var pi = allPaymentInstruments[key];
        if (pi.getPaymentMethod() != dw.order.PaymentInstrument.METHOD_GIFT_CERTIFICATE) {
            nonGiftPI = pi;
            break;
        }
    }
    /* eslint-enable no-restricted-syntax */

    return nonGiftPI;
}

/**
 * Create a new Order and Cancel the Old one
 */
function orderCreateAndCancel(order, orderObj) {
    OrderMgr.cancelOrder(order);
    var ret = createNewOrder(order, orderObj);
    var newOrder = null;

    if (ret.success) {
        newOrder = ret.newOrder;
        newOrder.custom.LOMSShippingRefunded = order.custom.LOMSShippingRefunded;
        newOrder.custom.LOMSOriginalOrder = (order.custom.LOMSOriginalOrder == null) ? order.orderNo : order.custom.LOMSOriginalOrder;
        newOrder.custom.LOMSReplacedOrder = order.orderNo;

        //copy payment instrument custom attributes from one order to another
        for (var i in order.paymentInstruments) {
            var paymentInstrument = order.paymentInstruments[i];
            for (var key in paymentInstrument.custom) {
                newOrder.paymentInstruments[i].custom[key] = paymentInstrument.custom[key];
            }

            if (paymentInstrument.paymentMethod === "MercadoPago") {
                newOrder.paymentInstruments[i].creditCardType = paymentInstrument.creditCardType;
            }
        }

        order.setCancelCode(Order.ORDER_STATUS_CANCELLED);
        order.setCancelDescription(Resource.msgf("fastoms.replacedescription", "fastoms", null, order.orderNo, newOrder.orderNo));
        request.setLocale(order.customerLocaleID);
        sendEmail("mail/orderCreationEmail", newOrder.customerEmail, Resource.msgf("fastoms.replacedescription", "fastoms", null, order.orderNo, newOrder.orderNo), { order: newOrder, oldOrder: order });
    } else {
        FOMSLogger.error("[orderCreateAndCancel] Cann't create new order", ret.responseMsg);
        return {
            newOrder: null,
            success: false,
            responseMsg: ret.responseMsg ? ret.responseMsg : false
        };
    }

    return {
        newOrder: newOrder,
        success: true
    };
}

function getArrayOfResources(resourceName, values) {
    var array = [];
    for (var i = 0; i < values.length; i++) {
        array.push(Resource.msgf(resourceName, "fastoms", null, values[i]));
    }
    return array;
}

/**
 * Creates a Basket from a given Order
 * @param order     Order   : Original order
 */
function copyToBasket(order, obj) {
    var basket;
    var baskets = BasketMgr.baskets;
    if (baskets.length > 3) {
        /* eslint-disable no-restricted-syntax */
        for (var keyBaskets in baskets) {
            var oldBasket = baskets[keyBaskets];
            BasketMgr.deleteBasket(oldBasket);
        }
        /* eslint-enable no-restricted-syntax */
    }

    basket = BasketMgr.createAgentBasket();
    var newCurrency = Currency.getCurrency(order.getCurrencyCode());
    session.setCurrency(newCurrency);
    basket.updateCurrency();
    basket.createBillingAddress();
    var allShipments = {};
    /* eslint-disable no-restricted-syntax */
    for (var keyShipment in order.getShipments()) {
        var originalShipment = order.getShipments()[keyShipment];
        var shipmentId = originalShipment.ID;
        var shipment = basket.getDefaultShipment() || basket.createShipment(shipmentId);
        shipment.setShippingMethod(originalShipment.getShippingMethod());
        shipment.setGift(originalShipment.isGift());
        shipment.createShippingAddress();
        if (shipment.isGift()) {
            shipment.setGiftMessage(originalShipment.getGiftMessage());
        }
        allShipments[shipmentId] = shipment;
    }

    if (!empty(order.couponLineItems)) {
        var couponFlow = Site.getCurrent().getCustomPreferenceValue("FastOMSCouponFlow");

        switch (couponFlow) {
            case "manualFlow":
                //Compatible With Coupon Manual
                couponManual.configureFastOMSCouponsEnabledStatus(order, true);
                couponManual.applyFastOMSCouponToNewOrder(order, basket);
                break;
            case "automatedFlow":
                //Compatible With Coupon Automation
                couponAutomation.createFastOMSCouponAndAssignItToCampaign(order);
                couponAutomation.applyFastOMSCouponToNewOrder(order, basket);
                break;
            default:
                break;
        }
    }

    var allPaymentInstruments = order.getPaymentInstruments();
    for (var keyPaymentInstrumets in allPaymentInstruments) {
        var pi = allPaymentInstruments[keyPaymentInstrumets];
        var orderPaymentInstrument = basket.createPaymentInstrument(pi.getPaymentMethod(), pi.getPaymentTransaction().amount);
        if (pi.getPaymentMethod() == dw.order.PaymentInstrument.METHOD_CREDIT_CARD) {
            orderPaymentInstrument.setCreditCardExpirationMonth(pi.getCreditCardExpirationMonth());
            orderPaymentInstrument.setCreditCardExpirationYear(pi.getCreditCardExpirationYear());
            orderPaymentInstrument.setCreditCardNumber(obj.card);
            orderPaymentInstrument.setCreditCardType(pi.getCreditCardType());
            orderPaymentInstrument.setCreditCardHolder(pi.getCreditCardHolder());
            orderPaymentInstrument.setCreditCardHolder(pi.getCreditCardHolder());
            orderPaymentInstrument.paymentTransaction.transactionID = pi.paymentTransaction.transactionID;
            var transactionType = pi.paymentTransaction.getType();
            if (transactionType != "") {
                orderPaymentInstrument.paymentTransaction.setType(transactionType);
            }
            var cardToken = pi.getCreditCardToken();
            if (cardToken != null) {
                orderPaymentInstrument.setCreditCardToken(cardToken);
            }
        }
        if (pi.getPaymentMethod() == dw.order.PaymentInstrument.METHOD_GIFT_CERTIFICATE) {
            orderPaymentInstrument.setGiftCertificateCode(pi.getGiftCertificateCode());
        }
    }
    /* eslint-enable no-restricted-syntax */

    HookMgr.callHook("dw.order.calculate", "calculate", basket);

    return basket;
}

/**
 * Copy stati to new order from JSON
 */
function copyStati(ord, obj) {
    var order = ord;
    order.status = parseInt(obj.status, 10);
    order.paymentStatus = parseInt(obj.paymentStatus, 10);
    order.confirmationStatus = parseInt(obj.confirmationStatus, 10);
    order.exportStatus = parseInt(obj.exportStatus, 10);
    setShipmentsStatus(order, parseInt(obj.shippingStatus, 10));
}

/**
 * Update order shipments status
 */
function setShipmentsStatus(ord, newStatus) {
    ord.setShippingStatus(newStatus);
    /* eslint-disable no-restricted-syntax */
    for (var shipmentIdx in ord.shipments) {
        var shipment = ord.shipments[shipmentIdx];
        shipment.setShippingStatus(newStatus);
    }
    /* eslint-enable no-restricted-syntax */
}

function controlBasketProductsATSBeforeCreatingOrder(basket) {
    //To catching product ATS limit exceeds.
    //This situation might be occur the situation, if a all stocks of product is bought, and that product will be added as a bonus product.

    var basketProductLineItems = basket.productLineItems;
    var controlResult = {
        success: true,
        message: ""
    };

    //Find more than one productLineItems with same product ID in basket.
    for (var i = 0; i < basketProductLineItems.length; i++) {
        var product = basketProductLineItems[i].product;
        var examinedProductLineItems = basket.getProductLineItems(product.ID);
        var productQuantity = 0;
        var productAtsValue = product.availabilityModel.inventoryRecord.getATS().value;
        var isBonusProduct = false;

        if (examinedProductLineItems && examinedProductLineItems.length > 1) {
            for (var j = 0; j < examinedProductLineItems.length; j++) {
                var examinedProductLineItem = examinedProductLineItems[j];

                if (examinedProductLineItem.bonusProductLineItem) {
                    isBonusProduct = true;
                }

                productQuantity += examinedProductLineItem.quantityValue;
                if (productQuantity > productAtsValue) {
                    controlResult.success = false;
                    controlResult.message = isBonusProduct ? Resource.msgf("basket.specificError", "fastoms", null, examinedProductLineItem.lineItemText) : "Quantity exceeds inventory";
                }
            }
        }
    }

    return controlResult;
}

/**
 * Create New Order
 */
function createNewOrder(currentOrder, obj) {
    session.setCurrency(Currency.getCurrency(currentOrder.getCurrencyCode()));

    // Temporarily change the customer of the current order to  be able to use
    // createBasketFromOrder and OrderMgr.createOrder() functions
    var basket = copyToBasket(currentOrder, obj);

    var result = updateBasket(basket, obj, null, null);

    if (result.success) {
        FOMSLogger.info("Basket {0} CREATED", currentOrder.orderNo);
    } else {
        FOMSLogger.error("updateBasket failed", result.errorMessage);

        return {
            success: false,
            responseMsg: result.errorMessage
        };
    }

    if (obj.refundShipping || currentOrder.custom.LOMSShippingRefunded) {
        removeShipmentValue(basket);
    }

    try {
        var order = OrderMgr.createOrder(basket);
    } catch (e) {
        FOMSLogger.error("order creation failed", e);
        return {
            success: false,
            responseMsg: e.toString()
        };
    }

    FOMSLogger.info("Order {0} CREATED", order.orderNo);

    var orderCustomer = currentOrder.getCustomer();
    var orderCustomerEmail = obj.customerEmail;
    var orderCustomerName = obj.customerName;
    var paymentInstrument = getNonGiftPaymentInstrument(currentOrder);

    var paymentProcessor;
    if (paymentInstrument) {
        paymentProcessor = PaymentMgr.getPaymentMethod(paymentInstrument.paymentMethod).getPaymentProcessor();
    }

    order.setCustomer(orderCustomer);
    order.setCustomerEmail(orderCustomerEmail);
    order.setCustomerName(orderCustomerName);

    var newPaymentInstrument = getNonGiftPaymentInstrument(order);
    if (newPaymentInstrument) {
        newPaymentInstrument.paymentTransaction.paymentProcessor = paymentProcessor;
    }

    order.custom.LOMSTransactionNotes = currentOrder.custom.LOMSTransactionNotes;
    order.custom.LOMSNotes = currentOrder.custom.LOMSNotes;

    var status = OrderMgr.placeOrder(order);
    var isStatusOk = (status.status == Status.OK);

    if (isStatusOk) {
        // only after order is placed we can set status from the obj
        FOMSLogger.info("Order {0} PLACED", order.orderNo);
        copyStati(order, obj);
    }

    HookMgr.callHook("dw.order.calculate", "calculate", order);

    return {
        success: isStatusOk,
        newOrder: order
    };
}

/**
 * return Applicable Shipping Methods
 */
function getApplicableShippingMethods(shipment) {
    var address = {
        countryCode: shipment.shippingAddress.countryCode,
        stateCode: shipment.shippingAddress.stateCode,
        postalCode: shipment.shippingAddress.postalCode,
        city: shipment.shippingAddress.city,
        address1: shipment.shippingAddress.address1,
        address2: shipment.shippingAddress.address2
    };

    return dw.order.ShippingMgr.getShipmentShippingModel(shipment).getApplicableShippingMethods(address);
}

/**
 *  Update Shipping Method
 */
function updateShippingMethod(shipment, methodID) {
    var shipList = getApplicableShippingMethods(shipment);
    /* eslint-disable no-restricted-syntax */
    for (var key in shipList) {
        var ship = shipList[key];
        if (ship.ID == methodID) {
            shipment.setShippingMethod(ship);
            break;
        }
    }
    /* eslint-enable no-restricted-syntax */
}

// Update Basket(bsk) with products from obj
function updateBasket(bsk, obj, productToRemove, productToAdd) {
    var changedProduct; var refundProductItemObj; var pliRemove; var newQtd; var initLength; var tempI; var removeId; var
        tempId;

    updateAddress(bsk.billingAddress, obj.billing);
    updateAddress(bsk.defaultShipment.shippingAddress, obj.shipping);
    updateShippingMethod(bsk.defaultShipment, obj.shippingMethod);

    // Existing product addition
    var objProducts = obj.products;
    objProducts.forEach(function (product) {
        getProductAndAdd(product.pid, bsk, product.qtd);
    });

    // New Product Addition
    var newProductLineItem = null;
    if (productToAdd) {
        newProductLineItem = bsk.createProductLineItem(productToAdd, bsk.defaultShipment);
        newProductLineItem.setQuantityValue(1);
    }

    // Product remove
    if (productToRemove) {
        var toRemovedProductLineItem = null;
        var toRemovedProduct = bsk.getProductLineItems(productToRemove);
        if (!empty(toRemovedProduct)) {
            toRemovedProductLineItem = toRemovedProduct[0];
            bsk.removeProductLineItem(toRemovedProductLineItem);
        }
    }

    if (obj.changes && obj.changes.refunds) {
        for (var i = 0; i < obj.changes.refunds.length; i++) {
            refundProductItemObj = obj.changes.refunds[i];
            changedProduct = ProductMgr.getProduct(refundProductItemObj.pid);
            removeId = changedProduct.ID;
            initLength = bsk.productLineItems.length;
            /* eslint-disable no-restricted-syntax */
            for (var basketRemoveKey in bsk.productLineItems) {
                tempI = initLength - basketRemoveKey - 1;
                pliRemove = bsk.productLineItems[tempI];
                tempId = pliRemove.product.ID;
                if (removeId == tempId) {
                    if (pliRemove.quantityValue == refundProductItemObj.qtd) {
                        bsk.removeProductLineItem(pliRemove);
                    } else {
                        newQtd = pliRemove.quantityValue - refundProductItemObj.qtd;
                        pliRemove.setQuantityValue(newQtd);
                    }
                    break;
                }
            }
            /* eslint-enable no-restricted-syntax */
        }
    }

    var discount;
    var value;

    if (obj.discountValue.toString().indexOf("%") != -1) {
        value = parseFloat(obj.discountValue.replace("%", ""));
        if (!isNaN(value) && value > 0) {
            discount = new dw.campaign.PercentageDiscount(value);
        }
    } else {
        value = parseFloat(obj.discountValue);
        if (!isNaN(value) && value > 0) {
            discount = new dw.campaign.AmountDiscount(value);
        }
    }

    if (discount) {
        bsk.createPriceAdjustment(Resource.msg("fastoms.title", "fastoms", null), discount);
    }

    if (obj.changes.couponCode && obj.changes.couponCode.length > 0) {
        var couponApplied = true;
        var responseMsg = "";
        try {
            var couponLineItem = bsk.createCouponLineItem(obj.changes.couponCode, true);
            HookMgr.callHook("dw.order.calculate", "calculate", bsk);
            if (!couponLineItem.applied) {
                couponApplied = false;
                var statusCode = couponLineItem.statusCode;
                if (statusCode === CouponStatusCodes.NO_APPLICABLE_PROMOTION) {
                    responseMsg = Resource.msg("fastoms.coupon.notApplicable", "fastoms", null);
                }
            }
        } catch (e) {
            couponApplied = false;
            var err = e.errorCode;
            FOMSLogger.error("Coupon error: {0}. Coupon code: {1}", err, obj.changes.couponCode);
            if (err === "NO_ACTIVE_PROMOTION") {
                responseMsg = Resource.msgf("fastoms.coupon.error", "fastoms", null, obj.changes.couponCode);
            }
        }
        if (!couponApplied) {
            return {
                success: false,
                responseMsg: responseMsg ? responseMsg : "Coupon error: " + err
            };
        }
    }

    if (!empty(obj.changes.giftCardCode)) {
        var giftCertificate = dw.order.GiftCertificateMgr.getGiftCertificateByCode(obj.changes.giftCardCode);
        if (giftCertificate.balance.currencyCode === bsk.currencyCode) {
            // create gift card payment instruments
            redeemGiftCertificate(giftCertificate.getGiftCertificateCode());
        }
    }

    HookMgr.callHook("dw.order.calculate", "calculate", bsk);

    var checkBasket = controlBasketProductsATSBeforeCreatingOrder(bsk);
    if (!checkBasket.success) {
        return {
            success: false,
            responseMsg: checkBasket.message
        };
    }

    return {
        success: true,
        pli: newProductLineItem
    };
}

/**
 * add products that were added in preview and removed before save
 */
function getProductAndAdd(productId, lineItemContainer, quantity) {
    var product = ProductMgr.getProduct(productId);
    var newProductLineItem = null;
    Transaction.wrap(function () {
        newProductLineItem = lineItemContainer.createProductLineItem(product.ID, lineItemContainer.defaultShipment);
        newProductLineItem.setQuantityValue(Number(quantity));
    });

    return newProductLineItem;
}

/**
 * This flow of order changing will refund overcharged values after changes.
 */
function refundPartialFlow(paymentManager, order, orderObj) {
    var currentValue = order.getTotalGrossPrice().getDecimalValue();
    var newValue = parseFloat(orderObj.orderTotal);
    var result = { success: false };

    if (order.getPaymentStatus() != Order.PAYMENT_STATUS_PAID) {
        var paymentTransaction = getPaymentManagerTransaction(order);
        var newAmount = paymentTransaction.getAmount().newMoney(new Decimal(newValue));

        if (paymentManager.canReAuthorize(order)) {
            result = paymentManager.ReAuthorize(order, newValue);
            if (result.success == true) {
                Transaction.wrap(function () {
                    /* eslint-disable no-param-reassign */
                    paymentTransaction.transactionID = result.transactionID;
                    paymentTransaction.setAmount(newAmount);
                    /* eslint-disable no-param-reassign */
                });
            }
        } else {
            Transaction.wrap(function () {
                paymentTransaction.setAmount(newAmount);
            });
            result.success = true;
        }
    } else {
        result = paymentManager.Refund(order, Number(currentValue - newValue).toFixed(2));
    }

    if (result.success == true) {
        Transaction.wrap(function () {
            updateBasket(order, orderObj, null, null);
        });

        return true;
    }

    return false;
}

/**
 * Create Gift Certificate
 */
function createGiftCertificate(order, giftValue, orderObj) {
    var newCurrency = Currency.getCurrency(order.getCurrencyCode());
    var giftCert,
        giftMoney;
    Transaction.wrap(function () {
        session.setCurrency(newCurrency);
        giftCert = dw.order.GiftCertificateMgr.createGiftCertificate(giftValue);
        giftMoney = new Money(giftValue, Currency.getCurrency(order.getCurrencyCode()));
        giftCert.setOrderNo(order.getOrderNo());
        giftCert.setSenderName(siteName);
        giftCert.setRecipientName(order.getBillingAddress().getFullName());
        giftCert.setRecipientEmail(order.getCustomerEmail());
        request.setLocale(order.customerLocaleID);
        sendEmail("mail/giftCertificateRefundEmail", order.customerEmail, Resource.msgf("mail.refund.gift.subject", "fastoms", null, siteName), { order: order, giftCert: giftCert });
    });
    if (orderObj) {
        orderObj.orderNotes.notes.push({
            text: Resource.msgf("order.notes.giftRefund", "fastoms", null, giftMoney, giftCert.getGiftCertificateCode())
        });
    }
}

/**
 * return Original Order
 */
function getOriginalOrder(order) {
    var originalOrder = null;
    if (order.custom.LOMSOriginalOrder) {
        var firstOrder = OrderMgr.getOrder(order.custom.LOMSOriginalOrder);
        if (firstOrder) {
            originalOrder = firstOrder;
        }
    }

    return originalOrder;
}

/**
* login fake customer to seesion to be able to create basket (not possible without session customer)
*/
function logCustomer() {
    Transaction.begin();
    var FastOMSOrderUserUsername = Site.current.getCustomPreferenceValue("FastOMSOrderUserUsername");
    var FastOMSOrderUserPassword = Site.current.getCustomPreferenceValue("FastOMSOrderUserPassword");
    if (!CustomerMgr.loginExternallyAuthenticatedCustomer(FastOMSOrderUserUsername, FastOMSOrderUserPassword, false)) {
        CustomerMgr.createExternallyAuthenticatedCustomer(FastOMSOrderUserUsername, FastOMSOrderUserPassword);
        CustomerMgr.loginExternallyAuthenticatedCustomer(FastOMSOrderUserUsername, FastOMSOrderUserPassword, false);
    }
    Transaction.commit();
}

/**
 * return obj from OCAPI response
 */
function parseOCAPIBody(requestBody) {
    var orderObj = {};
    var req = JSON.parse(requestBody);
    var clientDetails = req.clientDetails;
    var paramDetails = req.paramDetails;
    var cipher = new Cipher();
    var base64EncryptKey = Site.getCurrent().getPreferences().getCustom().LOMSEncryptionKey;

    for (var keyClient in clientDetails) {
        orderObj[keyClient] = clientDetails[keyClient];
    }

    for (var keyParam in paramDetails) {
        orderObj[keyParam] = paramDetails[keyParam];
    }
    orderObj.products = req.products;
    orderObj.refundProducts = req.refundProducts;
    orderObj.changes = req.changes;
    orderObj.description = req.description;
    if (orderObj.card) {
        orderObj.card = cipher.decrypt(orderObj.card, base64EncryptKey, "AES", null, 1);
    } else {
        orderObj.card = null;
    }

    return orderObj;
}

/**
 * return Gift Certificates Amount
 */
function getGiftCertificatesAmount(newOrder, newGC) {
    // The total redemption amount of all gift certificate payment instruments in the basket.
    var giftCertTotal = new Money(0.0, newOrder.getCurrencyCode());

    // Gets the list of all gift certificate payment instruments
    var gcPaymentInstrs = newOrder.getGiftCertificatePaymentInstruments();
    var iter = gcPaymentInstrs.iterator();
    var orderPI = null;

    // Sums the total redemption amount.
    while (iter.hasNext()) {
        orderPI = iter.next();
        giftCertTotal = giftCertTotal.add(orderPI.getPaymentTransaction().getAmount());
        if (orderPI.getGiftCertificateCode() === newGC) {
            var giftCert = GiftCertificateMgr.getGiftCertificateByCode(newGC);
            GiftCertificateMgr.redeemGiftCertificate(orderPI);
            giftCert.setOrderNo(newOrder.orderNo);
            if (giftCert.getBalance() > 0) {
                giftCert.setOrderNo(newOrder.orderNo);
                giftCert.setStatus(giftCert.STATUS_PARTIALLY_REDEEMED);
            }
        }
    }

    // Returns the open amount to be paid.
    return giftCertTotal;
}

/**
 * This flow of order changing will cancel a payment and create a new authorization.
 */
function cancelAndNewFlow(paymentManager, order, orderObj) {
    // if the original order had a gift certificate, we have to recover it for the new authorization
    var originalOrder = getOriginalOrder(order) || order;
    var giftCosts = originalOrder.giftCertificateTotalGrossPrice.value;
    var success = false;
    var newOrderNo = null;
    var result = paymentManager.Cancel(order);
    var newGC = orderObj.changes.giftCardCode || null;

    if (!result.success) {
        return {
            success: false,
            responseMsg: result.errorMsg
        };
    }

    if (result.success) {
        var nonGiftPayment = 0.0;
        var oldOrderTotal = order.totalGrossPrice.value;
        var newOrderTotal = orderObj.orderTotal;
        var totalRefunded = oldOrderTotal - newOrderTotal;
        var auth = {
            error: true
        };
        /* eslint-disable no-restricted-syntax */
        for (var keyPaymentInstrument in order.paymentInstruments) {
            var paymentInstrument = order.paymentInstruments[keyPaymentInstrument];
            if (paymentInstrument.getPaymentMethod() != dw.order.PaymentInstrument.METHOD_GIFT_CERTIFICATE) {
                nonGiftPayment += paymentInstrument.paymentTransaction.amount.value;
            }
        }
        /* eslint-enable no-restricted-syntax */

        var giftPayment = oldOrderTotal - nonGiftPayment;

        var giftRefund = totalRefunded - nonGiftPayment;
        var needsNewAuth = true;
        if (giftRefund > 0) {
            createGiftCertificate(order, giftRefund);
            needsNewAuth = false;
        }

        Transaction.begin();
        var response = orderCreateAndCancel(order, orderObj);
        var newOrder = response.newOrder;

        if (response.success) {
            // assures that the new authorization includes the original gift costs, even if they are not gonna be recreated.
            if (giftCosts > 0.0 && orderObj.products.length > 0) {
                /* eslint-disable no-restricted-syntax */
                for (var keyGC in originalOrder.getGiftCertificateLineItems()) {
                    var gcli = originalOrder.getGiftCertificateLineItems()[keyGC];
                    newOrder.createGiftCertificateLineItem(gcli.getGrossPrice().value, gcli.recipientEmail);
                }
                /* eslint-enable no-restricted-syntax */
            }
            if (newGC) {
                giftPayment = getGiftCertificatesAmount(newOrder, newGC);
            }
            // ensure new costs are updated
            HookMgr.callHook("dw.order.calculate", "calculate", newOrder);

            if (needsNewAuth) {
                var pi = getNonGiftPaymentInstrument(newOrder);
                pi.paymentTransaction.setAmount(pi.paymentTransaction.getAmount().newMoney(new Decimal(newOrderTotal - giftPayment)));

                auth = paymentManager.Authorize(newOrder, orderObj);
                if (auth.success) {
                    pi.paymentTransaction.transactionID = auth.requestID;
                } else {
                    OrderMgr.failOrder(newOrder);
                    Transaction.commit();
                    return {
                        success: false,
                        responseMsg: auth.errorMsg
                    };
                }
            } else {
                /* eslint-disable no-restricted-syntax */
                // if no new auth is needed, means the order is currently paid with a partial usage of the Gift Certificate
                for (var keyNewPaymentInstrument in newOrder.paymentInstruments) {
                    var newPaymentInstrument = newOrder.paymentInstruments[keyNewPaymentInstrument];
                    if (newPaymentInstrument.getPaymentMethod() == dw.order.PaymentInstrument.METHOD_GIFT_CERTIFICATE) {
                        var pt = newPaymentInstrument.paymentTransaction;
                        pt.setAmount(pt.getAmount().newMoney(new Decimal(newOrder.totalGrossPrice.value)));
                    } else {
                        newOrder.removePaymentInstrument(newPaymentInstrument);
                    }
                }
                /* eslint-enable no-restricted-syntax */
                HookMgr.callHook("dw.order.calculate", "calculate", newOrder);
            }
            if (needsNewAuth && auth.success) {
                newOrderNo = newOrder.orderNo;
                success = true;
                sendEmail("mail/orderCreationEmail", newOrder.customerEmail, Resource.msgf("fastoms.replacedescription", "fastoms", null, orderObj.orderNo, newOrderNo), { order: newOrder, oldOrder: order });
                Transaction.commit();
            } else {
                Transaction.rollback();
                return {
                    success: false,
                    responseMsg: response.responseMsg ? response.responseMsg : false
                };
            }
        } else {
            Transaction.rollback();
            return {
                success: success,
                newOrderNo: newOrderNo,
                responseMsg: response.responseMsg ? response.responseMsg : false
            };
        }
    }

    return {
        success: success,
        newOrderNo: newOrderNo
    };
}


function createPaymentTransaction(order, agent, type, transactionID, amount) {
    var trasactionAmount;

    if (amount) {
        trasactionAmount = new Money(amount, Currency.getCurrency(order.getCurrencyCode()));
    } else {
        var paymentTransaction = getPaymentManagerTransaction(order);
        trasactionAmount = new Money(paymentTransaction.amount.value, Currency.getCurrency(order.getCurrencyCode()));
    }

    var newPaymentTransaction = {
        time: Date(),
        note: Resource.msgf("order.notes.transactionnote", "fastoms", null, type, transactionID, trasactionAmount.toFormattedString()),
        agent: agent
    };

    var transactionNotes = order.custom.LOMSTransactionNotes ? JSON.parse(order.custom.LOMSTransactionNotes) : { notes: [] };
    transactionNotes.notes.push(newPaymentTransaction);

    Transaction.begin();
    order.custom.LOMSTransactionNotes = JSON.stringify(transactionNotes);
    Transaction.commit();
}


/**
 * create order notes
 */
function addEditionOrRefundNote(order, type, options) {
    if (options) {
        var orderNotes = options.orderNotes;
        var oldOrderNumber = options.orderNo;
        var newNote = {
            time: Date(),
            notes: orderNotes.notes,
            agent: orderNotes.agent
        };

        var transactionNotes = order.custom.LOMSTransactionNotes ? JSON.parse(order.custom.LOMSTransactionNotes) : { notes: [] };

        if (oldOrderNumber !== order.getCurrentOrderNo()) {
            var replacedNote = {
                time: Date(),
                note: "order replaced with " + order.getCurrentOrderNo()
            };

            var oldOrder = OrderMgr.getOrder(oldOrderNumber);
            var oldTransactionNotes = oldOrder.custom.LOMSTransactionNotes ? JSON.parse(oldOrder.custom.LOMSTransactionNotes) : { notes: [] };
            oldTransactionNotes.notes.push(newNote);
            oldTransactionNotes.notes.push(replacedNote);
            oldOrder.custom.LOMSTransactionNotes = JSON.stringify(oldTransactionNotes);
            replacedNote = {
                time: Date(),
                note: order.getCurrentOrderNo() + " order created based on " + oldOrderNumber
            };
            transactionNotes.notes.push(replacedNote);
        }

        transactionNotes.notes.push(newNote);
        order.custom.LOMSTransactionNotes = JSON.stringify(transactionNotes);
    }
}

/**
 * Update Order
 */
function updateOrder(order, orderObj) {
    order.customerName = orderObj.customerName;
    order.customerEmail = orderObj.customerEmail;
    copyStati(order, orderObj);

    var shippingDest = order.defaultShipment.shippingAddress;
    if (shippingDest == null) {
        shippingDest = order.defaultShipment.createShippingAddress();
    }

    updateAddress(order.billingAddress, orderObj.billing);
    updateAddress(shippingDest, orderObj.shipping);

    if (order.defaultShipment) {
        updateShippingMethod(order.defaultShipment, orderObj.shippingMethod);
    }
}

// Update Address
function updateAddress(address, obj) {
    address.firstName = obj.firstName;
    address.lastName = obj.lastName;
    address.address1 = obj.address1;
    address.address2 = (obj.address2) ? obj.address2 : "";
    address.city = obj.city;
    address.postalCode = obj.postalCode;
    address.countryCode = obj.countryCode;
    address.stateCode = obj.stateCode;
    address.phone = obj.phone;
}

/**
 * Calculates the amount to be paid by a non-gift certificate payment instrument based on the given basket.
 * The function subtracts the amount of all redeemed gift certificates from the order total and returns this
 * value.
 * @returns {dw.value.Money} The amount to be paid by a non-gift certificate payment instrument.
 */
function getNonGiftCertificateAmount(currentBasket) {
    // The total redemption amount of all gift certificate payment instruments in the basket.
    var giftCertTotal = new Money(0.0, currentBasket.getCurrencyCode());

    // Gets the list of all gift certificate payment instruments
    var gcPaymentInstrs = currentBasket.getGiftCertificatePaymentInstruments();
    var iter = gcPaymentInstrs.iterator();
    var orderPI = null;

    // Sums the total redemption amount.
    while (iter.hasNext()) {
        orderPI = iter.next();
        giftCertTotal = giftCertTotal.add(orderPI.getPaymentTransaction().getAmount());
    }

    // Gets the order total.
    var orderTotal = currentBasket.getTotalGrossPrice();

    // Calculates the amount to charge for the payment instrument.
    // This is the remaining open order total that must be paid.
    var amountOpen = orderTotal.subtract(giftCertTotal);

    // Returns the open amount to be paid.
    return amountOpen;
}

/**
 * calculate Payment Transaction for Basket
 */
function calculatePaymentTransaction(currentBasket) {
    var result = { error: false };

    try {
        Transaction.wrap(function () {
            // Gets all payment instruments for the basket.
            var iter = currentBasket.getPaymentInstruments().iterator();
            var paymentInstrument = null;
            var nonGCPaymentInstrument = null;
            var giftCertTotal = new Money(0.0, currentBasket.getCurrencyCode());

            // Locates a non-gift certificate payment instrument if one exists.
            while (iter.hasNext()) {
                paymentInstrument = iter.next();
                /* eslint-disable no-continue */
                if (PaymentInstrument.METHOD_GIFT_CERTIFICATE.equals(paymentInstrument.getPaymentMethod())) {
                    giftCertTotal = giftCertTotal.add(paymentInstrument.getPaymentTransaction().getAmount());
                    continue;
                }
                /* eslint-disable no-continue */

                // Captures the non-gift certificate payment instrument.
                nonGCPaymentInstrument = paymentInstrument;
                break;
            }

            // Gets the order total.
            var orderTotal = currentBasket.getTotalGrossPrice();

            // If a gift certificate payment and non-gift certificate payment
            // instrument are found, the function returns true.
            if (!nonGCPaymentInstrument) {
                // If there are no other payment types and the gift certificate
                // does not cover the open amount, then return false.
                if (giftCertTotal < orderTotal) {
                    result.error = true;
                } else {
                    result.error = false;
                }
            } else {
                // Calculates the amount to be charged for the
                // non-gift certificate payment instrument.
                var amount = getNonGiftCertificateAmount(currentBasket);

                // now set the non-gift certificate payment instrument total.
                if (amount.value <= 0.0) {
                    var zero = new Money(0, amount.getCurrencyCode());
                    nonGCPaymentInstrument.getPaymentTransaction().setAmount(zero);
                } else {
                    nonGCPaymentInstrument.getPaymentTransaction().setAmount(amount);
                }
            }
        });
    } catch (e) {
        result.error = e;
        FOMSLogger.error("calculatePaymentTransaction error", e);
    }

    return result;
}

/**
 * Creates a gift certificate payment instrument from the given gift certificate ID for the basket. The
 * method attempts to redeem the current balance of the gift certificate. If the current balance exceeds the
 * order total, this amount is redeemed and the balance is lowered.
 * @param {dw.order.GiftCertificate} giftCertificate - The gift certificate.
 * @returns {dw.order.PaymentInstrument} The created PaymentInstrument.
 */
function createGiftCertificatePaymentInstrument(giftCertificate) {
    var currentBasket = BasketMgr.getCurrentOrNewBasket();

    // Removes any duplicates.
    // Iterates over the list of payment instruments to check.
    var giftCertificateCode = giftCertificate.getGiftCertificateCode();
    var allGcPaymentInstrs = currentBasket.getGiftCertificatePaymentInstruments();
    var gcPaymentInstrs = new ArrayList();
    /* eslint-disable no-restricted-syntax */
    for (var gcPaymentInstrsIdx in allGcPaymentInstrs) {
        var gcPI = allGcPaymentInstrs[gcPaymentInstrsIdx];
        if (gcPI.getGiftCertificateCode() === giftCertificateCode) {
            gcPaymentInstrs.push(gcPI);
        }
    }
    /* eslint-enable no-restricted-syntax */
    var iter = gcPaymentInstrs.iterator();
    var existingPI = null;

    // Removes found gift certificates, to prevent duplicates.
    while (iter.hasNext()) {
        existingPI = iter.next();
        currentBasket.removePaymentInstrument(existingPI);
    }

    // Fetches the balance and the order total.
    var balance = giftCertificate.getBalance();
    var orderTotal = currentBasket.getTotalGrossPrice();

    // Sets the amount to redeem equal to the remaining balance.
    var amountToRedeem = balance;

    // Since there may be multiple gift certificates, adjusts the amount applied to the current
    // gift certificate based on the order total minus the aggregate amount of the current gift certificates.

    var giftCertTotal = new Money(0.0, currentBasket.getCurrencyCode());

    // Iterates over the list of gift certificate payment instruments
    // and updates the total redemption amount.
    gcPaymentInstrs = currentBasket.getGiftCertificatePaymentInstruments().iterator();
    var orderPI = null;

    while (gcPaymentInstrs.hasNext()) {
        orderPI = gcPaymentInstrs.next();
        giftCertTotal = giftCertTotal.add(orderPI.getPaymentTransaction().getAmount());
    }

    // Calculates the remaining order balance.
    // This is the remaining open order total that must be paid.
    var orderBalance = orderTotal.subtract(giftCertTotal);

    // The redemption amount exceeds the order balance.
    // use the order balance as maximum redemption amount.
    if (orderBalance < amountToRedeem) {
        // Sets the amount to redeem equal to the order balance.
        amountToRedeem = orderBalance;
    }

    // Creates a payment instrument from this gift certificate.
    return currentBasket.createGiftCertificatePaymentInstrument(giftCertificate.getGiftCertificateCode(), amountToRedeem);
}

/**
 * Removes a gift certificate payment instrument with the given gift certificate ID
 * from the basket.
 *
 * @transactional
 * @param {String} giftCertificateID - The ID of the gift certificate to remove the payment instrument for.
 */
function removeGiftCertificatePaymentInstrument(giftCertificateID) {
    var currentBasket = BasketMgr.getCurrentOrNewBasket();

    // Iterates over the list of payment instruments.
    var allGcPaymentInstrs = currentBasket.getGiftCertificatePaymentInstruments();
    var gcPaymentInstrs = new ArrayList();
    /* eslint-disable no-restricted-syntax */
    for (var gcPaymentInstrsIdx in allGcPaymentInstrs) {
        var gcPI = allGcPaymentInstrs[gcPaymentInstrsIdx];
        if (gcPI.getGiftCertificateCode() === giftCertificateID) {
            gcPaymentInstrs.push(gcPI);
        }
    }
    /* eslint-enable no-restricted-syntax */
    var iter = gcPaymentInstrs.iterator();
    var existingPI = null;

    // Remove (one or more) gift certificate payment
    // instruments for this gift certificate ID.
    while (iter.hasNext()) {
        existingPI = iter.next();
        currentBasket.removePaymentInstrument(existingPI);
    }
}

/**
 * Creates a list of gift certificate ids from gift certificate payment instruments.
 *
 * @alias module:models/CartModel~CartModel/getGiftCertIdList
 * @returns {dw.util.ArrayList} The list of gift certificate IDs.
 */
function getGiftCertIdList() {
    var currentBasket = BasketMgr.getCurrentOrNewBasket();
    var gcIdList = new ArrayList();
    var gcPIIter = currentBasket.getGiftCertificatePaymentInstruments().iterator();

    while (gcPIIter.hasNext()) {
        gcIdList.add((gcPIIter.next()).getGiftCertificateCode());
    }

    return gcIdList;
}

/**
 * adjust Gift Certificates
 *
 */
function adjustGiftCertificates() {
    var i;
    var j;
    var gcIdList;
    var gcID;
    var gc;
    var currentBasket = BasketMgr.getCurrentOrNewBasket();

    if (currentBasket) {
        gcIdList = getGiftCertIdList();

        Transaction.wrap(function () {
            for (i = 0; i < gcIdList.length; i += 1) {
                removeGiftCertificatePaymentInstrument(gcIdList[i]);
            }

            gcID = null;

            for (j = 0; j < gcIdList.length; j += 1) {
                gcID = gcIdList[j];

                gc = GiftCertificateMgr.getGiftCertificateByCode(gcID);

                if ((gc) // make sure exists
                    && (currentBasket.getGiftCertificateLineItems().length === 0) // make sure there is no GC in cart
                    && (gc.isEnabled()) // make sure it is enabled
                    && (gc.getStatus() !== GiftCertificate.STATUS_PENDING) // make sure it is available for use
                    && (gc.getStatus() !== GiftCertificate.STATUS_REDEEMED) // make sure it has not been fully redeemed
                    && (gc.balance.currencyCode === currentBasket.getCurrencyCode())) { // make sure the GC is in the right currency
                    createGiftCertificatePaymentInstrument(gc);
                }
            }
        });
    }
}

/**
 * Redeems a gift certificate. If the gift certificate was not successfully
 * redeemed, the form field is invalidated with the appropriate error message.
 * If the gift certificate was redeemed, the form gets cleared. This function
 * is called by an Ajax request and generates a JSON response.
 * @param {String} giftCertCode - Gift certificate code entered into the giftCertCode field in the billing form.
 * @returns {object} JSON object containing the status of the gift certificate.
 */
function redeemGiftCertificate(giftCertCode) {
    var gc;
    var newGCPaymentInstrument;
    var gcPaymentInstrument;
    var status;
    var result;
    var basket;
    var giftCertificateStatusCodeInCart = "giftcertredeem.giftincart";
    basket = BasketMgr.getCurrentOrNewBasket();

    if (basket.getGiftCertificateLineItems().length > 0) {
        result = new Status(Status.ERROR, giftCertificateStatusCodeInCart, giftCertificateStatusCodeInCart, null);
    } else if (basket) {
        // fetch the gift certificate
        gc = GiftCertificateMgr.getGiftCertificateByCode(giftCertCode);

        if (!gc) { // make sure exists
            result = new Status(Status.ERROR, GiftCertificateStatusCodes.GIFTCERTIFICATE_NOT_FOUND, GiftCertificateStatusCodes.GIFTCERTIFICATE_NOT_FOUND, giftCertCode);
        } else if (!gc.isEnabled()) { // make sure it is enabled
            result = new Status(Status.ERROR, GiftCertificateStatusCodes.GIFTCERTIFICATE_DISABLED, GiftCertificateStatusCodes.GIFTCERTIFICATE_DISABLED, null);
        } else if (gc.getStatus() === GiftCertificate.STATUS_PENDING) { // make sure it is available for use
            result = new Status(Status.ERROR, GiftCertificateStatusCodes.GIFTCERTIFICATE_PENDING, GiftCertificateStatusCodes.GIFTCERTIFICATE_PENDING, null);
        } else if (gc.getStatus() === GiftCertificate.STATUS_REDEEMED) { // make sure it has not been fully redeemed
            result = new Status(Status.ERROR, GiftCertificateStatusCodes.GIFTCERTIFICATE_INSUFFICIENT_BALANCE, GiftCertificateStatusCodes.GIFTCERTIFICATE_INSUFFICIENT_BALANCE, null);
        } else if (gc.balance.currencyCode !== basket.getCurrencyCode()) { // make sure the GC is in the right currency
            result = new Status(Status.ERROR, GiftCertificateStatusCodes.GIFTCERTIFICATE_CURRENCY_MISMATCH, GiftCertificateStatusCodes.GIFTCERTIFICATE_CURRENCY_MISMATCH, null);
        } else {
            newGCPaymentInstrument = Transaction.wrap(function () {
                gcPaymentInstrument = createGiftCertificatePaymentInstrument(gc);
                Transaction.wrap(function () {
                    HookMgr.callHook("dw.order.calculate", "calculate", basket);
                    adjustGiftCertificates();
                });
                return gcPaymentInstrument;
            });

            status = new Status(Status.OK);
            status.addDetail("NewGCPaymentInstrument", newGCPaymentInstrument);
            result = status;
        }
    } else {
        result = new Status(Status.ERROR, "BASKET_NOT_FOUND");
    }
    return result;
}

/**
 * Assign or unaaign fake FastOMS customer to same customer groups
 * as real order customer
 *
 * @param {dw.order.Order} order
 * @param mode //assign OR unassign
 */
function assignFastOMSUSerToOrderCustomerGroups(order, mode) {
    var orderCustomer = order.getCustomer();
    var customerGroups = orderCustomer.getCustomerGroups();
    var customerGroupsIterator = customerGroups.iterator();
    while (customerGroupsIterator.hasNext()) {
        var tempCustomerGroup = customerGroupsIterator.next();
        if (tempCustomerGroup.getID() == "Everyone" || tempCustomerGroup.getID() == "Registered") {
            continue;
        }
        try {
            if (mode == "assign") {
                tempCustomerGroup.assignCustomer(customer);
            } else {
                tempCustomerGroup.unassignCustomer(customer);
            }
        } catch (e) {
            continue;
        }
    }
}

/**
 * Get used promotions information on order
 *
 * @param {dw.order.LineItemCtnr} lineItemCtnr
 * @returns {object} promotions
 */
function getUsedPromotions(lineItemCtnr) {
    var promotions = {
        productPromotions: [],
        shippingPromotions: [],
        orderPromotions: [],
        productPromotionsTotal: 0,
        shippingPromotionsTotal: 0
    };

    var usedPromotionsArray = function (usedPromotions, priceAdjustments) {
        priceAdjustments.toArray().forEach(function (priceAdjusment) {
            var promotion = priceAdjusment.promotion;
            // if promotion is null, it means promotion was not assigned any campaign for now.
            usedPromotions.push({
                promotionID: priceAdjusment.promotionID,
                promotionClass: promotion ? promotion.promotionClass : "",
                promotionCalloutMsg: promotion && promotion.calloutMsg ? promotion.calloutMsg.markup : "",
                netDiscount: priceAdjusment.basePrice.value,
                grossDiscount: priceAdjusment.grossPrice.value,
                promotionName: promotion ? promotion.name : priceAdjusment.promotionID
            });
        });
    };

    // ProductLineItems promotions && ProductLineItems-specific shipping promotions
    lineItemCtnr.productLineItems.toArray().forEach(function (productLineItem) {
        var usedProductPromotions = [];

        if (!empty(productLineItem.priceAdjustments)) {
            usedPromotionsArray(usedProductPromotions, productLineItem.priceAdjustments);
        }

        if (productLineItem.shippingLineItem && !empty(productLineItem.shippingLineItem.priceAdjustments)) {
            usedPromotionsArray(usedProductPromotions, productLineItem.shippingLineItem.priceAdjustments);
        }


        promotions.productPromotions.push({
            productID: productLineItem.productID,
            totalDiscount: productDiscountTotal,
            usedPromotions: usedProductPromotions
        });
    });

    var productDiscountTotal = 0;
    promotions.productPromotions.forEach(function (promotion) {
        promotion.usedPromotions.forEach(function (usedPromotion) {
            productDiscountTotal += usedPromotion.netDiscount;
        });
    });
    promotions.productPromotionsTotal = productDiscountTotal;

    // Shipping promotions
    if (lineItemCtnr.defaultShipment && !empty(lineItemCtnr.defaultShipment.shippingPriceAdjustments)) {
        var usedShippingPromotions = [];
        usedPromotionsArray(usedShippingPromotions, lineItemCtnr.defaultShipment.shippingPriceAdjustments);
        promotions.shippingPromotions.push({
            usedPromotions: usedShippingPromotions
        });
    }

    var shippingDiscountTotal = 0;
    promotions.shippingPromotions.forEach(function (promotion) {
        promotion.usedPromotions.forEach(function (usedPromotion) {
            shippingDiscountTotal += usedPromotion.discount;
        });
    });
    promotions.shippingPromotionsTotal = shippingDiscountTotal;

    // Order promotions
    if (lineItemCtnr.priceAdjustments && !empty(lineItemCtnr.priceAdjustments)) {
        var usedOrderPromotions = [];
        usedPromotionsArray(usedOrderPromotions, lineItemCtnr.priceAdjustments);
        promotions.orderPromotions.push({
            usedPromotions: usedOrderPromotions
        });
    }

    return promotions;
}

function removeOldRequests(objectTypeID) {
    var currentDate = new Date();
    var compareDate = new Date(currentDate - 60000);

    var oldRequests = CustomObjectMgr.queryCustomObjects(objectTypeID, "creationDate <={0}", null, compareDate);

    while (oldRequests.hasNext()) {
        var tempRequest = oldRequests.next();

        try {
            Transaction.begin();
            CustomObjectMgr.remove(tempRequest);
            Transaction.commit();
            FOMSLogger.info(tempRequest.custom.orderNo + " request deleted");
        } catch (error) {
            Transaction.rollback();
        }

    }

    oldRequests.close();
}

function reApplyOriginalCoupon(order, newOrder) {
    var couponFlow = Site.getCurrent().getCustomPreferenceValue("FastOMSCouponFlow");

    if (couponFlow === "manualFlow") {
        //Coupon Manual
        couponManual.reApplyToOriginalCodeToCreatedNewOrder(order, newOrder);
    } else if (couponFlow === "automatedFlow") {
        //Coupon Automation
        couponAutomation.reApplyToOriginalCodeToCreatedNewOrder(order, newOrder);
    }
}

function disableFastOMSCoupons(lineItemCtnr) {
    var couponFlow = Site.getCurrent().getCustomPreferenceValue("FastOMSCouponFlow");

    if (couponFlow === "manualFlow") {
        //Coupon Manual
        couponManual.configureFastOMSCouponsEnabledStatus(lineItemCtnr, false);
    } else if (couponFlow === "automatedFlow") {
        //Coupon Automation
        couponAutomation.disableFastOMSCoupons(lineItemCtnr);
    }
}

function checkUsedCouponsInBM(order) {
    var couponLineItems = order.getCouponLineItems().toArray();
    var removedCoupons = [];
    var unsAssignedCoupons = [];

    for (var i = 0; i < couponLineItems.length; i++) {
        var couponLineItem = couponLineItems[i];
        var originalCouponCode = couponLineItem.getCouponCode();
        var originalCoupon = CouponMgr.getCouponByCode(originalCouponCode);

        if (!originalCoupon) {
            removedCoupons.push(originalCouponCode);
        } else if (empty(originalCoupon.getPromotions())) {
            unsAssignedCoupons.push(originalCouponCode);
        }
    }

    if (!empty(removedCoupons)) {
        return {
            success: false,
            msg: Resource.msgf("fastoms.coupon.removed", "fastoms", null, removedCoupons.join())
        };
    } else if (!empty(unsAssignedCoupons)) {
        return {
            success: false,
            msg: Resource.msgf("fastoms.coupon.unassigned", "fastoms", null, unsAssignedCoupons.join())
        };
    }

    return {
        success: true,
        msg: null
    };
}

function getResources() {
    // application resources
    var resources = {
        TYPEAHEAD_EMPTY: Resource.msg("typeahead.empty", "fastoms", null),
        EDIT_ORDER_BUTTON: Resource.msg("btn.edit", "fastoms", null),
        CANCEL_EDITION_BUTTON: Resource.msg("btn.canceledit", "fastoms", null),
        UNEXPECTED_ERROR: Resource.msg("fastoms.unexpectederror", "fastoms", null),
        MODAL_TITLE_INFO: Resource.msg("fastoms.modaltitle.info", "fastoms", null),
        ORDER_CANCEL_ERROR: Resource.msg("order.cancel.error", "fastoms", null),
        OUTSTANDING_AMOUNT: Resource.msg("order.totals.outstanding", "fastoms", null),
        REFUND_AMOUNT: Resource.msg("order.totals.refund", "fastoms", null),
        REFUND_QUANTITY_ERROR: Resource.msg("order.quantity.refund.error", "fastoms", null),
        REMOVE: Resource.msg("product.remove", "fastoms", null),
        ORDER_SAVE_EDIT: Resource.msg("order.save.edit", "fastoms", null),
        ORDER_SAVE_CONFIRM: Resource.msg("order.save.text", "fastoms", null),
        STATUS_CANCELLED: Order.ORDER_STATUS_CANCELLED,
        CONDITION_GIFTCERT_OUTSTANDING: Resource.msg("save.disable.giftcert", "fastoms", null),
        CONDITION_NO_NEW_AUTHORIZATIONS: Resource.msg("save.disable.authrestricted", "fastoms", null),
        CONDITION_REFUND_UNCAPTURED: Resource.msg("save.disable.uncaptured", "fastoms", null),
        ORDER_SAVE_HEADERITEM: Resource.msg("order.save.headeritem", "fastoms", null),
        ORDER_SAVE_HEADERDETAIL: Resource.msg("order.save.headerdetail", "fastoms", null),
        ORDER_SAVE_ADDED: Resource.msg("order.save.added", "fastoms", null),
        ORDER_SAVE_REMOVED: Resource.msg("order.save.removed", "fastoms", null),
        SHIPPING_LABEL: Resource.msg("order.totals.shipping", "fastoms", null),
        NOTES_PRODUCTS_WITH: Resource.msg("order.notes.productsWith", "fastoms", null),
        NOTES_IDREFUNDED: Resource.msg("order.notes.idRefunded", "fastoms", null),
        NOTES_IDPENDING: Resource.msg("order.notes.idPending", "fastoms", null),
        NOTES_REFUNDSHIPMENT: Resource.msg("order.notes.refundshipment", "fastoms", null),
        NOTES_REFUNDSHIPMENTPENDING: Resource.msg("order.notes.refundshipmentpending", "fastoms", null),
        NOTES_CONFIRM_RETURN: Resource.msg("order.notes.idConfirmReturn", "fastoms", null),
        NOTES_CANCEL_RETURN: Resource.msg("order.notes.idCancelReturn", "fastoms", null),
        NOTES_ORDERS: Resource.msg("order.notes.orders", "fastoms", null),
        NOTES_CHANGEDTO: Resource.msg("order.notes.changed", "fastoms", null),
        NOTES_ADDED: Resource.msg("order.notes.added", "fastoms", null),
        NOTES_REMOVED: Resource.msg("order.notes.removed", "fastoms", null),
        NOTES_QUANTITYCHANGE: Resource.msg("order.notes.quantityChange", "fastoms", null),
        NOTES_REPLACED: Resource.msg("order.notes.replaced", "fastoms", null),
        RETURN_OUTOF: Resource.msg("order.return.message.outOf", "fastoms", null),
        RETURN_MARKED: Resource.msg("order.return.message.itemsMarkedReturned", "fastoms", null),
        RETURN_MARKED_CANCELED: Resource.msg("order.return.message.itemsMarkedCanceled", "fastoms", null),
        PAYMENT_PROCESSOR_PAYPAL: Resource.msg("order.payment.PAYPAL", "fastoms", null),
        PAY_PAL_OUTSTANDING_ERROR: Resource.msg("order.edit.outstanding.paypal", "fastoms", null),
        CHECK_PRODUCTS_QUANTITY: Resource.msg("fastoms.limitinventory.error", "fastoms", null)
    };
    return resources;
}

function getUrls() {
    // application urls
    var urls = {
        orderSearch: URLUtils.url("FastOMSController-OrderSearch").toString(),
        typeahead: URLUtils.url("FastOMSController-TypeaheadOrders").toString(),
        orderDetails: URLUtils.url("FastOMSController-ShowOrderDetails").toString(),
        calculate: URLUtils.url("FastOMSController-CalculateChanges").toString(),
        calcRefund: URLUtils.url("FastOMSController-CalculateRefund").toString(),
        variant: URLUtils.url("FastOMSController-UpdateVariant").toString(),
        addNote: URLUtils.url("FastOMSController-AddNote").toString(),
        refund: URLUtils.url("FastOMSController-RefundOrder").toString(),
        manageReturn: URLUtils.url("FastOMSController-ManageReturn").toString(),
        queryProducts: URLUtils.url("FastOMSController-QueryProducts").toString(),
        addProduct: URLUtils.url("FastOMSController-AddProduct").toString(),
        updateVariantStock: URLUtils.url("FastOMSController-UpdateVariantStock").toString(),
        validateCouponCode: URLUtils.url("FastOMSController-ValidateCouponCode").toString(),
        checkDuplicateProduct: URLUtils.url("FastOMSController-CheckDuplicateProduct").toString(),
        validateGiftCertificate: URLUtils.url("FastOMSController-ValidateGiftCertificate").toString()
    };
    return urls;
}

function getOptions() {
    var options = {

        months: [
            { name: Resource.msg("month.january", "fastoms", null), value: "01" },
            { name: Resource.msg("month.february", "fastoms", null), value: "02" },
            { name: Resource.msg("month.march", "fastoms", null), value: "03" },
            { name: Resource.msg("month.april", "fastoms", null), value: "04" },
            { name: Resource.msg("month.may", "fastoms", null), value: "05" },
            { name: Resource.msg("month.june", "fastoms", null), value: "06" },
            { name: Resource.msg("month.july", "fastoms", null), value: "07" },
            { name: Resource.msg("month.august", "fastoms", null), value: "08" },
            { name: Resource.msg("month.september", "fastoms", null), value: "09" },
            { name: Resource.msg("month.october", "fastoms", null), value: "10" },
            { name: Resource.msg("month.november", "fastoms", null), value: "11" },
            { name: Resource.msg("month.december", "fastoms", null), value: "12" }
        ],

        years: [
            { name: "2018", value: "2018" },
            { name: "2019", value: "2019" },
            { name: "2020", value: "2020" },
            { name: "2021", value: "2021" },
            { name: "2022", value: "2022" },
            { name: "2023", value: "2023" },
            { name: "2024", value: "2024" }
        ],

        countries: [
            { name: Resource.msg("global.select", "fastoms", null), value: "" },
            { name: Resource.msg("country.unitedstates", "fastoms", null), value: "us" }
        ],

        states: [
            { name: Resource.msg("global.select", "fastoms", null), value: "" },
            { name: Resource.msg("state.us.alabama", "fastoms", null), value: "AL" },
            { name: Resource.msg("state.us.alaska", "fastoms", null), value: "AK" },
            { name: Resource.msg("state.us.americansamoa", "fastoms", null), value: "AS" },
            { name: Resource.msg("state.us.arizona", "fastoms", null), value: "AZ" },
            { name: Resource.msg("state.us.arkansas", "fastoms", null), value: "AR" },
            { name: Resource.msg("state.us.california", "fastoms", null), value: "CA" },
            { name: Resource.msg("state.us.colorado", "fastoms", null), value: "CO" },
            { name: Resource.msg("state.us.connecticut", "fastoms", null), value: "CT" },
            { name: Resource.msg("state.us.delaware", "fastoms", null), value: "DE" },
            { name: Resource.msg("state.us.dc", "fastoms", null), value: "DC" },
            { name: Resource.msg("state.us.florida", "fastoms", null), value: "FL" },
            { name: Resource.msg("state.us.georgia", "fastoms", null), value: "GA" },
            { name: Resource.msg("state.us.guam", "fastoms", null), value: "GU" },
            { name: Resource.msg("state.us.hawaii", "fastoms", null), value: "HI" },
            { name: Resource.msg("state.us.idaho", "fastoms", null), value: "ID" },
            { name: Resource.msg("state.us.illinois", "fastoms", null), value: "IL" },
            { name: Resource.msg("state.us.indiana", "fastoms", null), value: "IN" },
            { name: Resource.msg("state.us.iowa", "fastoms", null), value: "IA" },
            { name: Resource.msg("state.us.kansas", "fastoms", null), value: "KS" },
            { name: Resource.msg("state.us.kentucky", "fastoms", null), value: "KY" },
            { name: Resource.msg("state.us.louisiana", "fastoms", null), value: "LA" },
            { name: Resource.msg("state.us.maine", "fastoms", null), value: "ME" },
            { name: Resource.msg("state.us.maryland", "fastoms", null), value: "MD" },
            { name: Resource.msg("state.us.massachusetts", "fastoms", null), value: "MA" },
            { name: Resource.msg("state.us.michigan", "fastoms", null), value: "MI" },
            { name: Resource.msg("state.us.minnesota", "fastoms", null), value: "MN" },
            { name: Resource.msg("state.us.mississippi", "fastoms", null), value: "MS" },
            { name: Resource.msg("state.us.missouri", "fastoms", null), value: "MO" },
            { name: Resource.msg("state.us.montana", "fastoms", null), value: "MT" },
            { name: Resource.msg("state.us.nebraska", "fastoms", null), value: "NE" },
            { name: Resource.msg("state.us.nevada", "fastoms", null), value: "NV" },
            { name: Resource.msg("state.us.newhampshire", "fastoms", null), value: "NH" },
            { name: Resource.msg("state.us.newjersey", "fastoms", null), value: "NJ" },
            { name: Resource.msg("state.us.newmexico", "fastoms", null), value: "NM" },
            { name: Resource.msg("state.us.newyork", "fastoms", null), value: "NY" },
            { name: Resource.msg("state.us.northcarolina", "fastoms", null), value: "NC" },
            { name: Resource.msg("state.us.northdakota", "fastoms", null), value: "ND" },
            { name: Resource.msg("state.us.ohio", "fastoms", null), value: "OH" },
            { name: Resource.msg("state.us.oklahoma", "fastoms", null), value: "OK" },
            { name: Resource.msg("state.us.oregon", "fastoms", null), value: "OR" },
            { name: Resource.msg("state.us.pennsylvania", "fastoms", null), value: "PA" },
            { name: Resource.msg("state.us.puertorico", "fastoms", null), value: "PR" },
            { name: Resource.msg("state.us.rhodeisland", "fastoms", null), value: "RI" },
            { name: Resource.msg("state.us.southcarolina", "fastoms", null), value: "SC" },
            { name: Resource.msg("state.us.southdakota", "fastoms", null), value: "SD" },
            { name: Resource.msg("state.us.tennessee", "fastoms", null), value: "TN" },
            { name: Resource.msg("state.us.texas", "fastoms", null), value: "TX" },
            { name: Resource.msg("state.us.utah", "fastoms", null), value: "UT" },
            { name: Resource.msg("state.us.vermont", "fastoms", null), value: "VT" },
            { name: Resource.msg("state.us.virginislands", "fastoms", null), value: "VI" },
            { name: Resource.msg("state.us.virginia", "fastoms", null), value: "VA" },
            { name: Resource.msg("state.us.washington", "fastoms", null), value: "WA" },
            { name: Resource.msg("state.us.westvirginia", "fastoms", null), value: "WV" },
            { name: Resource.msg("state.us.wisconsin", "fastoms", null), value: "WI" },
            { name: Resource.msg("state.us.wyoming", "fastoms", null), value: "WY" },
            { name: Resource.msg("state.us.armedforcesafrica", "fastoms", null), value: "AE" },
            { name: Resource.msg("state.us.armedforcesamerica", "fastoms", null), value: "AA" },
            { name: Resource.msg("state.us.armedforcescanada", "fastoms", null), value: "AE" },
            { name: Resource.msg("state.us.armedforceseurope", "fastoms", null), value: "AE" },
            { name: Resource.msg("state.us.armedforcesmiddleeast", "fastoms", null), value: "AE" },
            { name: Resource.msg("state.us.armedforcespacific", "fastoms", null), value: "AP" }
        ]
    };
    return options;
}

function lockOrder(order, source) {
    var orderLockJson = {
        date: new Date(),
        source: source || "FastOMS"
    };
    order.custom.orderLock = true;
    order.custom.orderLockJson = JSON.stringify(orderLockJson);
}

function unlockOrder(order) {
    order.custom.orderLock = false;
    order.custom.orderLockJson = "";
}

function createObjLog(orderObj, mode) {
    FOMSLogger.info("[" + mode + "] Order obj: {0}", JSON.stringify(orderObj));
}

exports.getPaymentManager = getPaymentManager;
exports.getPaymentManagerTransaction = getPaymentManagerTransaction;
exports.getReturnInfo = getReturnInfo;
exports.filterVariants = filterVariants;
exports.refreshStock = refreshStock;
exports.getValues = getValues;
exports.removeShipmentValue = removeShipmentValue;
exports.orderCancelation = orderCancelation;
exports.sendEmail = sendEmail;
exports.getNonGiftPaymentInstrument = getNonGiftPaymentInstrument;
exports.orderCreateAndCancel = orderCreateAndCancel;
exports.copyToBasket = copyToBasket;
exports.copyStati = copyStati;
exports.createNewOrder = createNewOrder;
exports.getApplicableShippingMethods = getApplicableShippingMethods;
exports.updateShippingMethod = updateShippingMethod;
exports.updateBasket = updateBasket;
exports.refundPartialFlow = refundPartialFlow;
exports.createGiftCertificate = createGiftCertificate;
exports.cancelAndNewFlow = cancelAndNewFlow;
exports.addEditionOrRefundNote = addEditionOrRefundNote;
exports.updateOrder = updateOrder;
exports.updateAddress = updateAddress;
exports.parseOCAPIBody = parseOCAPIBody;
exports.getCardPaymentInstrument = getCardPaymentInstrument;
exports.logCustomer = logCustomer;
exports.getAddedBonusItems = getAddedBonusItems;
exports.getLostBonusItems = getLostBonusItems;
exports.getAmountOfBonusProductsByPid = getAmountOfBonusProductsByPid;
exports.isPromotionBasedOnCoupons = isPromotionBasedOnCoupons;
exports.getAmountOfBonusDiscountsByPromotionID = getAmountOfBonusDiscountsByPromotionID;
exports.createPaymentTransaction = createPaymentTransaction;
exports.getAddedDiscountLineItems = getAddedDiscountLineItems;
exports.getLostDiscountLineItems = getLostDiscountLineItems;
exports.getArrayOfResources = getArrayOfResources;
exports.removeDuplicates = removeDuplicates;
exports.getProductVariantion = getProductVariantion;
exports.getGiftCertificatesAmount = getGiftCertificatesAmount;
exports.redeemGiftCertificate = redeemGiftCertificate;
exports.calculatePaymentTransaction = calculatePaymentTransaction;
exports.getResources = getResources;
exports.getUrls = getUrls;
exports.getOptions = getOptions;
exports.lockOrder = lockOrder;
exports.unlockOrder = unlockOrder;
exports.assignFastOMSUSerToOrderCustomerGroups = assignFastOMSUSerToOrderCustomerGroups;
exports.getUsedPromotions = getUsedPromotions;
exports.removeOldRequests = removeOldRequests;
exports.createObjLog = createObjLog;
exports.reApplyOriginalCoupon = reApplyOriginalCoupon;
exports.disableFastOMSCoupons = disableFastOMSCoupons;
exports.checkUsedCouponsInBM = checkUsedCouponsInBM;

