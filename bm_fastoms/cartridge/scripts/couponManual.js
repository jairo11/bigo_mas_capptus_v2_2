/**
 *  Property of OSF Global Services, Inc., (with its brand OSF Commerce). OSF remains the sole owner of all right, title and interest in the software.
 *  Do not copy, sell, reverse engineer or otherwise attempt to derive or obtain information about the functioning, manufacture or operation therein.
 */

"use strict";
var CouponMgr = require("dw/campaign/CouponMgr");
var couponAutomation = require("./couponAutomation");
var FOMSLogger = require("dw/system/Logger").getLogger("FastOMS", "FastOMS");
var HookMgr = require("dw/system/HookMgr");
var Transaction = require("dw/system/Transaction");
var Resource = require("dw/web/Resource");
var OrderMgr = require("dw/order/OrderMgr");
var Site = require("dw/system/Site");

var fastOMSCouponSuffix = "-FOMS";

function checkFastOMSCouponsPresence(lineItemCtnr) {
    var couponFlow = Site.getCurrent().getCustomPreferenceValue("FastOMSCouponFlow");
    var couponLineItems = lineItemCtnr.getCouponLineItems().toArray();

    if (empty(couponLineItems) || couponFlow !== "manualFlow") {
        return {
            success: true,
            errorMsg: null
        };
    }

    var shouldAssignedPromotionIDs = [];

    for (var i = 0; i < couponLineItems.length; i++) {
        var couponLineItem = couponLineItems[i];
        var originalCouponCode = couponLineItem.getCouponCode();
        var originalCoupon = CouponMgr.getCouponByCode(originalCouponCode);

        if (originalCoupon && !empty(originalCoupon.getPromotions())) {
            var originalCouponID = originalCoupon.getID();
            var fastOMSCouponID = originalCouponID + fastOMSCouponSuffix;
            var fastOMSCoupon = CouponMgr.getCoupon(fastOMSCouponID);

            var couponLineItemPriceAdjustments = couponLineItem.getPriceAdjustments().toArray();
            var appliedPromotionsIDsBasedOriginalCoupon = couponLineItemPriceAdjustments.map(function (couponLineItemPriceAdjustment) {
                return couponLineItemPriceAdjustment.getPromotionID();
            });

            if (empty(fastOMSCoupon)) {
                return {
                    success: false,
                    errorMsg: Resource.msgf("fastoms.coupon.error.notexist", "fastoms", null, fastOMSCouponID, appliedPromotionsIDsBasedOriginalCoupon.join())
                };
            }


            var fastOMSCouponPromotionAssignments = fastOMSCoupon.getPromotions().toArray().map(function (promotion) {
                return promotion.getID();
            });

            for (var j = 0; j < appliedPromotionsIDsBasedOriginalCoupon.length; j++) {
                var appliedPromotionID = appliedPromotionsIDsBasedOriginalCoupon[j];
                if (fastOMSCouponPromotionAssignments.indexOf(appliedPromotionID) === -1) {
                    shouldAssignedPromotionIDs.push(appliedPromotionID);
                }
            }

            if (!empty(shouldAssignedPromotionIDs)) {
                return {
                    success: false,
                    errorMsg: Resource.msgf("fastoms.coupon.error.isnotassignedpromotion", "fastoms", null, fastOMSCouponID, shouldAssignedPromotionIDs.join())
                };
            }
        }
    }

    return {
        success: true,
        errorMsg: null
    };

}

function applyFastOMSCouponToNewOrder(oldLineItemCtnr, newLineItemCtnr) {
    var oldOrderCouponLineItems = oldLineItemCtnr.getCouponLineItems().toArray();
    if (empty(oldOrderCouponLineItems)) {
        return;
    }

    oldOrderCouponLineItems.forEach(function (oldOrderCouponLineItem) {
        var originalCouponCode = oldOrderCouponLineItem.getCouponCode();
        var originalCoupon = CouponMgr.getCouponByCode(originalCouponCode);

        if (originalCoupon && !empty(originalCoupon.getPromotions())) {
            var originalCouponID = originalCoupon.getID();
            FOMSLogger.info("Orginal Coupon ID:" + originalCouponID);
            var fastOMSCouponCode = originalCouponID + fastOMSCouponSuffix;
            newLineItemCtnr.createCouponLineItem(fastOMSCouponCode, true);
        }
    });

    HookMgr.callHook("dw.order.calculate", "calculate", newLineItemCtnr);
}

function reApplyToOriginalCodeToCreatedNewOrder(oldLineItemCtnr, newLineItemCntr) {
    var oldOrderCouponLineItems = oldLineItemCtnr.getCouponLineItems().toArray();
    var newOrderCouponLineItems = newLineItemCntr.getCouponLineItems().toArray();

    //if old order has no coupon line items dont do anything
    if (empty(oldOrderCouponLineItems)) {
        return;
    }

    // if a new coupons added old order and old order has already another coupon
    // new coupon will be in new order as original code
    // old coupon will be in new order as fastoms code
    // just convert fastoms coupon to original coupon
    Transaction.wrap(function () {
        newOrderCouponLineItems.forEach(function (newOrderCouponLineItem) {
            var tempCouponCode = newOrderCouponLineItem.getCouponCode();
            var tempCoupon = CouponMgr.getCouponByCode(tempCouponCode);
            var tempCouponID = tempCoupon.getID();

            oldOrderCouponLineItems.forEach(function (oldOrderCouponLineItem) {
                var originalCouponCode = oldOrderCouponLineItem.getCouponCode();
                var originalCoupon = CouponMgr.getCouponByCode(originalCouponCode);
                var originalCouponID = originalCoupon.getID();
                var fastOMSCouponID = originalCouponID + fastOMSCouponSuffix;

                if (tempCouponID == fastOMSCouponID) {
                    newLineItemCntr.removeCouponLineItem(newOrderCouponLineItem);
                    newLineItemCntr.createCouponLineItem(originalCouponCode, true);
                }
            });
        });

        HookMgr.callHook("dw.order.calculate", "calculate", newLineItemCntr);

        OrderMgr.cancelOrder(newLineItemCntr);
        OrderMgr.undoCancelOrder(newLineItemCntr);
        OrderMgr.placeOrder(newLineItemCntr);
    });
}

function configureFastOMSCouponsEnabledStatus(lineItemCtnr, enabledStatus) {
    var lineItemCtnrCouponLineItems = lineItemCtnr.getCouponLineItems().toArray();
    if (empty(lineItemCtnrCouponLineItems)) {
        return;
    }

    var fastOMSCouponID = "";
    lineItemCtnrCouponLineItems.forEach(function (lineItemCtnrCouponLineItem) {
        var originalCouponCode = lineItemCtnrCouponLineItem.getCouponCode();
        var originalCoupon = CouponMgr.getCouponByCode(originalCouponCode);

        if (originalCoupon && !empty(originalCoupon.getPromotions())) {
            var originalCouponID = originalCoupon.getID();
            if (lineItemCtnr instanceof dw.order.Basket) {
                if (originalCouponID.indexOf(fastOMSCouponSuffix) >= 0) {
                    fastOMSCouponID = originalCouponID;
                }
            } else {
                fastOMSCouponID = originalCouponID + fastOMSCouponSuffix;
            }
            couponAutomation.changeFastOMSCouponAvailabilityStatusInBusinessManager(fastOMSCouponID, enabledStatus);
            FOMSLogger.info(fastOMSCouponID + " enable status was changed to " + (enabledStatus ? "YES" : "NO") + " by FastOMS");
        }
    });

}


module.exports = {
    checkFastOMSCouponsPresence: checkFastOMSCouponsPresence,
    applyFastOMSCouponToNewOrder: applyFastOMSCouponToNewOrder,
    reApplyToOriginalCodeToCreatedNewOrder: reApplyToOriginalCodeToCreatedNewOrder,
    configureFastOMSCouponsEnabledStatus: configureFastOMSCouponsEnabledStatus
};
