/**
 *  Property of OSF Global Services, Inc., (with its brand OSF Commerce). OSF remains the sole owner of all right, title and interest in the software.
 *  Do not copy, sell, reverse engineer or otherwise attempt to derive or obtain information about the functioning, manufacture or operation therein.
 */

"use strict";

var LocalServiceRegistry = require("dw/svc/LocalServiceRegistry");
var System = require("dw/system/System");
var Site = require("dw/system/Site");
var OrderMgr = require("dw/order/OrderMgr");
var CouponMgr = require("dw/campaign/CouponMgr");
var HookMgr = require("dw/system/HookMgr");
var Transaction = require("dw/system/Transaction");
var FOMSLogger = require("dw/system/Logger").getLogger("FastOMS", "FastOMS");

var fastOMSSuffix = "-FOMS";

/**
 *
 * @param {string} couponID the original coupon id that used in order
 * @returns {string} FastOMS coupon code related to coupon
 */
function getFastOMSCouponCode(couponID) {
    return couponID + fastOMSSuffix;
}

/**
 * @description Created FastOMSCoupon and assigns it to related Campaigns
 * @param {dw.order.LineItemCtnr} lineItemCtnr order or basket that are applied coupon
 * @returns {void}
 */
function createFastOMSCouponAndAssignItToCampaign(lineItemCtnr) {
    var couponLineItems = lineItemCtnr.getCouponLineItems().toArray();
    if (empty(couponLineItems)) {
        return;
    }

    couponLineItems.forEach(function (couponLineItem) {
        //Check coupon line item is based on Commerce Cloud Digital campaign
        var isCouponLineItemBasedOnCampaign = couponLineItem.isBasedOnCampaign();
        if (!isCouponLineItemBasedOnCampaign) {
            return;
        }

        var originalCouponCode = couponLineItem.getCouponCode();
        var originalCoupon = CouponMgr.getCouponByCode(originalCouponCode);

        if (originalCoupon && !empty(originalCoupon.getPromotions())) {
            var originalCouponID = originalCoupon.getID();

            //Check if is original coupon enable
            var isOriginalCouponIsEnabled = originalCoupon.isEnabled();
            if (!isOriginalCouponIsEnabled) {
                return;
            }

            //Search FastOMSCoupon if its exist just make it enabled, else create it
            createFastOMSCouponInBusinessManager(originalCouponID);

            //Assign FastOMSCoupons to the original coupons campaigns
            var fastOMSCouponID = originalCouponID + fastOMSSuffix;
            var couponAssignedCampaigns = getCampaignsFromCouponID(originalCouponID);
            if (!empty(couponAssignedCampaigns)) {
                couponAssignedCampaigns.forEach(function (couponAssignedCampaign) {
                    editFastOMSCouponCampaignAssignments(couponAssignedCampaign, fastOMSCouponID, true);
                });
            }
        }
    });
}

/**
 * @description Applies new line item container to applied coupon code,
 *  if old line item has no coupon line item, return.
 * @param {dw.order.LineItemCtnr} oldLineItemCtnr old line item container
 * @param {dw.order.LineItemCtnr} newLineItemCtnr the new line item container to applied coupo
 * @returns {void}
 */
function applyFastOMSCouponToNewOrder(oldLineItemCtnr, newLineItemCtnr) {
    var oldOrderCouponLineItems = oldLineItemCtnr.getCouponLineItems().toArray();
    if (empty(oldOrderCouponLineItems)) {
        return;
    }

    oldOrderCouponLineItems.forEach(function (oldOrderCouponLineItem) {
        var originalCouponCode = oldOrderCouponLineItem.getCouponCode();
        var originalCoupon = CouponMgr.getCouponByCode(originalCouponCode);

        if (originalCoupon && !empty(originalCoupon.getPromotions())) {
            var originalCouponCodeID = originalCoupon.getID();
            var fastOMSCouponCode = getFastOMSCouponCode(originalCouponCodeID);
            newLineItemCtnr.createCouponLineItem(fastOMSCouponCode, true);
        }
    });

    HookMgr.callHook("dw.order.calculate", "calculate", newLineItemCtnr);
}

/**
 * @description Takes original coupon codes from old line item container
 * and reapplies them to new line item container
 * @param {*} oldLineItemCtnr old line item container
 * @param {*} newLineItemCntr new line item container
 * @returns {void}
 */
function reApplyToOriginalCodeToCreatedNewOrder(oldLineItemCtnr, newLineItemCntr) {
    var oldOrderCouponLineItems = oldLineItemCtnr.getCouponLineItems().toArray();
    var newOrderCouponLineItems = newLineItemCntr.getCouponLineItems().toArray();

    //if old order has no coupon line items dont do anything
    if (empty(oldOrderCouponLineItems)) {
        return;
    }

    // if a new coupons added old order and old order has already another coupon
    // new coupon will be in new order as original code
    // old coupon will be in new order as fastoms code
    // just convert fastoms coupon to original coupon
    Transaction.wrap(function () {
        newOrderCouponLineItems.forEach(function (newOrderCouponLineItem) {
            var tempCouponCode = newOrderCouponLineItem.getCouponCode();
            var tempCoupon = CouponMgr.getCouponByCode(tempCouponCode);
            var tempCouponID = tempCoupon.getID();

            oldOrderCouponLineItems.forEach(function (oldOrderCouponLineItem) {
                var originalCouponCode = oldOrderCouponLineItem.getCouponCode();
                var originalCoupon = CouponMgr.getCouponByCode(originalCouponCode);
                var originalCouponID = originalCoupon.getID();
                var fastOMSCouponID = originalCouponID + fastOMSSuffix;

                if (tempCouponID == fastOMSCouponID) {
                    newLineItemCntr.removeCouponLineItem(newOrderCouponLineItem);
                    newLineItemCntr.createCouponLineItem(originalCouponCode, true);
                }
            });
        });

        HookMgr.callHook("dw.order.calculate", "calculate", newLineItemCntr);

        OrderMgr.cancelOrder(newLineItemCntr);
        OrderMgr.undoCancelOrder(newLineItemCntr);
        OrderMgr.placeOrder(newLineItemCntr);
    });
}

/**
 * @description Makes FastOMS coupons enable status NO after processes is done
 * @param {dw.order.LineItemCtnr} lineItemCtnr
 * @returns {void}
 */
function disableFastOMSCoupons(lineItemCtnr) {
    if (empty(lineItemCtnr)) {
        return;
    }

    var couponLineItems = lineItemCtnr.getCouponLineItems();

    if (empty(couponLineItems)) {
        return;
    }

    var usedFastOMSCoupons = [];
    couponLineItems = couponLineItems.toArray();
    couponLineItems.forEach(function (couponLineItem) {
        var couponCode = couponLineItem.getCouponCode();
        var coupon = CouponMgr.getCouponByCode(couponCode);

        if (coupon && !empty(coupon.getPromotions())) {
            var couponID = coupon.getID();

            if (lineItemCtnr instanceof dw.order.Basket) {
                if (couponID.indexOf(fastOMSSuffix) >= 0) {
                    usedFastOMSCoupons.push(couponID);
                }
            } else {
                var fastOMSCouponID = couponID + fastOMSSuffix;
                var fastOMSCoupon = CouponMgr.getCoupon(fastOMSCouponID);
                if (fastOMSCoupon) {
                    usedFastOMSCoupons.push(fastOMSCouponID);
                }
            }
        }
    });

    //Disable FastOMSCoupons
    if (!empty(usedFastOMSCoupons)) {
        usedFastOMSCoupons.forEach(function (usedCoupon) {
            FOMSLogger.info(usedCoupon + " enable status was changed to 'NO' by FastOMS");
            changeFastOMSCouponAvailabilityStatusInBusinessManager(usedCoupon, false);
        });
    }
}

/**
 * @description Creates FastOMS coupon with given properties in business manager, Request Method => PUT
 * @param {string} couponID Original coupon id.
 * @param {object} couponObject Should be given as in ocapi document
 * @returns {object} Created coupon in Business Manager, New coupon should created;
 * orginalCouponID + -FOMS
 */
function createFastOMSCouponInBusinessManager(couponID) {
    var fastOMSCoupon = {
        coupon_id: couponID + fastOMSSuffix,
        description: "This Coupon Code created by FastOMS automatically for calculating coupon applied order amounts. Please do not change anything",
        enabled: true,
        type: "single_code",
        single_code: getFastOMSCouponCode(couponID)
    };

    //Get access token
    var accessToken = getOCAPIAccessToken();

    //Service Registry
    var fastOMSCouponService = LocalServiceRegistry.createService("FastOMSCoupon", {
        createRequest: function (service, params) {
            updateServicesURLs(service, { couponID: fastOMSCoupon.coupon_id });
            service.addHeader("Content-Type", "application/json");
            service.addHeader("Authorization", "Bearer " + accessToken);
            service.setRequestMethod("PUT");
            return JSON.stringify(params);
        },
        parseResponse: function (service, client) {
            return JSON.parse(client.text);
        }
    });

    var response = fastOMSCouponService.call(fastOMSCoupon);
    if (response.ok) {
        FOMSLogger.info(fastOMSCoupon.coupon_id + " coupon was enabled by FastOMS");
    }
    return response;
}

/**
 * @description Makes FastOMS coupon enabled property "No", Request Method => PATCH
 * @param {string} couponID FastOMS Coupon ID
 * @param {boolean} availability If coupon will be enable give true, else false
 * @returns {void}
 */
function changeFastOMSCouponAvailabilityStatusInBusinessManager(couponID, availability) {
    var enable = {
        enabled: availability
    };

    //Get access token
    var accessToken = getOCAPIAccessToken();

    //Service Registry
    var fastOMSCouponService = LocalServiceRegistry.createService("FastOMSCoupon", {
        createRequest: function (service, params) {
            updateServicesURLs(service, { couponID: couponID });
            service.addHeader("Content-Type", "application/json");
            service.addHeader("Authorization", "Bearer " + accessToken);
            service.setRequestMethod("PATCH");
            return JSON.stringify(params);
        },
        parseResponse: function (service, client) {
            return JSON.parse(client.text);
        }
    });

    //Call
    fastOMSCouponService.call(enable);
}

/**
 * @description This function creates campaign search query via given coupon id, Request Method => POST
 * @param {string} couponID
 * @returns {Array} campaign ids
 */
function getCampaignsFromCouponID(couponID) {
    // Campaign search query
    var query = {
        query: {
            text_query: {
                "fields": ["coupon_id"],
                "search_phrase": couponID
            }
        },
        select: "(hits.(campaign_id))"
    };

    //Get access token
    var accessToken = getOCAPIAccessToken();

    //Service Registy
    var fastOMSCampignSearchService = LocalServiceRegistry.createService("FastOMSCampaignSearch", {
        createRequest: function (service, params) {
            updateServicesURLs(service);
            service.addHeader("Content-Type", "application/json");
            service.addHeader("Authorization", "Bearer " + accessToken);
            service.setRequestMethod("POST");
            return JSON.stringify(params);
        },
        parseResponse: function (service, client) {
            var campaignHits = JSON.parse(client.text).hits;
            var campaignIDs = [];
            if (campaignHits) {
                campaignIDs = campaignHits.map(function (campaignHit) {
                    return campaignHit.campaign_id;
                });
            }
            return campaignIDs;
        }
    });

    //Call
    var response = processServiceResponse(fastOMSCampignSearchService.call(query));
    return response.toArray();
}

/**
 * @description searches used coupons via order customer email, Request Method => POST
 * @param {string} customerEmail order customer email
 * @returns {object} searchResults
 */
function searchCouponRedemptions(customerEmail) {
    // Coupon redemption search query
    var query = {
        query: {
            text_query: {
                "fields": ["customer_email", "code"],
                "search_phrase": customerEmail
            }
        },
        select: "(**)"
    };

    //Get access token
    var accessToken = getOCAPIAccessToken();

    //Service Registry
    var fastOMSCouponRedemptionService = LocalServiceRegistry.createService("FastOMSCouponRedemptionSearch", {
        createRequest: function (service, params) {
            updateServicesURLs(service);
            service.addHeader("Content-Type", "application/json");
            service.addHeader("Authorization", "Bearer " + accessToken);
            service.setRequestMethod("POST");
            return JSON.stringify(params);
        },
        parseResponse: function (service, client) {
            return JSON.parse(client.text);
        }
    });

    var searchResult = fastOMSCouponRedemptionService.call(query);
    return searchResult;
}

/**
 * @description It configures coupon assignments.
 * if assignStatus set as true, coupon assigned to campaign, Request Method => PUT.
 * if assignStatus set as false, coupon unassigned from campaign, Request Method => DELETE
 * @param {string} fastOMSCouponID FastOMS coupon id
 * @param {string} campaigID related campaign id
 * @param {boolean} assignStatus
 * @returns {void}
 */
function editFastOMSCouponCampaignAssignments(campaigID, fastOMSCouponID, assignStatus) {
    //Set request method
    var requestMethod = assignStatus ? "PUT" : "DELETE";

    //Get access token
    var accessToken = getOCAPIAccessToken();

    //Service Registry
    var fastOMSCampaignService = LocalServiceRegistry.createService("FastOMSCampaign", {
        createRequest: function (service) {
            updateServicesURLs(service, { couponID: fastOMSCouponID, campaignID: campaigID });
            service.addHeader("Content-Type", "application/json");
            service.addHeader("Authorization", "Bearer " + accessToken);
            service.setRequestMethod(requestMethod);
        },
        parseResponse: function (service, client) {
            return client;
        }
    });

    //Call
    fastOMSCampaignService.call();
}

/**
 * @description It grants ocapi access token, Request Method => POST
 * @returns {string} It returns ocapi access token
 */
function getOCAPIAccessToken() {
    //Service Registry
    var fastOMSAuthService = LocalServiceRegistry.createService("FastOMSAuth", {
        createRequest: function (service) {
            updateServicesURLs(service);
            var base64Credentials = System.getPreferences().getCustom().FastOMSOCAPIAuth;
            service.addHeader("Content-Type", "application/x-www-form-urlencoded");
            service.addHeader("Authorization", "Basic " + base64Credentials);
            service.setRequestMethod("POST");
            return "grant_type=urn:demandware:params:oauth:grant-type:client-id:dwsid:dwsecuretoken";
        },
        parseResponse: function (service, client) {
            return JSON.parse(client.text).access_token;
        }
    });

    //Call
    var response = processServiceResponse(fastOMSAuthService.call());
    return response;
}

/**
 * @description Process service response
 * @param {object} response
 * @returns {object} It returns processed response object
 */
function processServiceResponse(response) {
    if (response && !response.ok && response.errorMessage) {
        FOMSLogger.error("FastOMSCouponAutomation Error: " + response);
        response = JSON.parse(response.errorMessage);
    } else {
        response = response.object;
    }

    return response;
}

/**
 * @description It updates service's url with related parameters
 * @param {dw.svc.service} service
 * @param {object} params optional parameters
 * @return {void}
 */
function updateServicesURLs(service, params) {
    var serviceURL = service.getURL();
    var hostName = Site.current.httpsHostName;
    var version = "v" + Site.getCurrent().getPreferences().getCustom().FOMSOCAPIVersion.replace(".", "_");
    var clientId = Site.getCurrent().getPreferences().getCustom().FOMSOCAPIClientID;
    var siteID = Site.getCurrent().getID();
    serviceURL = serviceURL.replace("{hostName}", hostName);
    serviceURL = serviceURL.replace("{siteID}", siteID);
    serviceURL = serviceURL.replace("{version}", version);
    serviceURL = serviceURL.replace("{clientId}", clientId);
    serviceURL = serviceURL.replace("{site_id}", siteID);

    if (params && params.couponID) {
        serviceURL = serviceURL.replace("{coupon_id}", encodeURIComponent(params.couponID));
    }

    if (params && params.campaignID) {
        serviceURL = serviceURL.replace("{campaign_id}", encodeURIComponent(params.campaignID));
    }

    service.setURL(serviceURL);
}

module.exports = {
    searchCouponRedemptions: searchCouponRedemptions,
    createFastOMSCouponAndAssignItToCampaign: createFastOMSCouponAndAssignItToCampaign,
    applyFastOMSCouponToNewOrder: applyFastOMSCouponToNewOrder,
    reApplyToOriginalCodeToCreatedNewOrder: reApplyToOriginalCodeToCreatedNewOrder,
    changeFastOMSCouponAvailabilityStatusInBusinessManager: changeFastOMSCouponAvailabilityStatusInBusinessManager,
    disableFastOMSCoupons: disableFastOMSCoupons
};
