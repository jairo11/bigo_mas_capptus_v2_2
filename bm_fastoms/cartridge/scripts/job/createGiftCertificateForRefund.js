/**
 *  Property of OSF Global Services, Inc., (with its brand OSF Commerce). OSF remains the sole owner of all right, title and interest in the software.
 *  Do not copy, sell, reverse engineer or otherwise attempt to derive or obtain information about the functioning, manufacture or operation therein.
 */

/* eslint-disable guard-for-in, eqeqeq */

"use strict";

var GiftCertificateMgr = require("dw/order/GiftCertificateMgr");
var OrderMgr = require("dw/order/OrderMgr");
var Transaction = require("dw/system/Transaction");
var Currency = require("dw/util/Currency");
var Site = require("dw/system/Site");
var Resource = require("dw/web/Resource");
var PaymentInstrument = require("dw/order/PaymentInstrument");
var sendEmail = require("*/cartridge/scripts/fastOMSHelper").sendEmail;
var FOMSLogger = require("dw/system/Logger").getLogger("FastOMS", "FastOMS");

/**
 * @description refund order
 * job needed as a workarond to avoid platform limitation: ordes creating in BM only with USD
 */
function createGiftCertificateForRefund(req) {
    var orderNo = req.orderNo;
    var order = OrderMgr.getOrder(orderNo);
    var giftCertificatePaymentInstruments = order.getPaymentInstruments(PaymentInstrument.METHOD_GIFT_CERTIFICATE).toArray();
    var siteName = Site.getCurrent().getName();
    var orderCurrency = Currency.getCurrency(order.getCurrencyCode());
    var totalUsedGiftCertificateAmountValues = 0;

    if (req.giftVal) {
        totalUsedGiftCertificateAmountValues = req.giftVal;
    } else {
        giftCertificatePaymentInstruments.forEach(function (giftCertificatePaymentInstrument) {
            var usedGiftCertificateAmountValue = giftCertificatePaymentInstrument.getPaymentTransaction().getAmount().getValue();
            totalUsedGiftCertificateAmountValues += usedGiftCertificateAmountValue;
        });
    }

    Transaction.begin();
    try {
        session.setCurrency(orderCurrency);
        var giftCertificate = GiftCertificateMgr.createGiftCertificate(totalUsedGiftCertificateAmountValues);
        giftCertificate.setOrderNo(orderNo);
        giftCertificate.setSenderName(siteName);
        giftCertificate.setRecipientName(order.getBillingAddress().getFullName());
        giftCertificate.setRecipientEmail(order.getCustomerEmail());


        // Create Notes
        var transactionNotes = order.custom.LOMSTransactionNotes ? JSON.parse(order.custom.LOMSTransactionNotes) : { notes: [] };
        var note = {
            time: Date(),
            note: Resource.msgf("order.notes.giftRefund", "fastoms", null, giftCertificate.getAmount(), giftCertificate.getGiftCertificateCode()),
            agent: PaymentInstrument.METHOD_GIFT_CERTIFICATE
        };
        transactionNotes.notes.push(note);
        order.custom.LOMSTransactionNotes = JSON.stringify(transactionNotes);

    } catch (_e) {
        Transaction.rollback();
        FOMSLogger.error("Error in [Create gift Certificate]\n" + _e.fileName + "\n(" + _e.lineNumber + "): " + _e.message);
    }
    Transaction.commit();
    sendEmail("mail/giftCertificateRefundEmail", order.customerEmail, Resource.msgf("mail.refund.gift.subject", "fastoms", null, siteName), { order: order, giftCert: giftCertificate });

}

module.exports = {
    createGiftCertificateForRefund: createGiftCertificateForRefund
};
