/**
 *  Property of OSF Global Services, Inc., (with its brand OSF Commerce). OSF remains the sole owner of all right, title and interest in the software.
 *  Do not copy, sell, reverse engineer or otherwise attempt to derive or obtain information about the functioning, manufacture or operation therein.
 */

"use strict";

var OrderMgr = require("dw/order/OrderMgr");
var Transaction = require("dw/system/Transaction");
var Template = require("dw/util/Template");
var HashMap = require("dw/util/HashMap");
var Helper = require("*/cartridge/scripts/fastOMSHelper");
var CustomObjectMgr = require("dw/object/CustomObjectMgr");
var FOMSLogger = require("dw/system/Logger").getLogger("FastOMS", "FastOMS");
var getValues = Helper.getValues;
var parseOCAPIBody = Helper.parseOCAPIBody;
var logCustomer = Helper.logCustomer;
var copyToBasket = Helper.copyToBasket;
var updateBasket = Helper.updateBasket;
var getLostBonusItems = Helper.getLostBonusItems;
var getAddedBonusItems = Helper.getAddedBonusItems;
var getAddedDiscountLineItems = Helper.getAddedDiscountLineItems;
var getLostDiscountLineItems = Helper.getLostDiscountLineItems;
var getArrayOfResources = Helper.getArrayOfResources;
var removeDuplicates = Helper.removeDuplicates;
var assignCustomerGroups = Helper.assignFastOMSUSerToOrderCustomerGroups;
var getUsedPromotions = Helper.getUsedPromotions;
var removeOldRequests = Helper.removeOldRequests;
var disableFastOMSCoupons = Helper.disableFastOMSCoupons;

/**
 * @description add new product to existing order
 * job needed as a workarond to avoid platform limitation: ordes creating in BM only with USD
 */
function addProducts() {
    // Removing old requests from queue which are broken, timedout etc.
    removeOldRequests("FastOMSAddProductsQueue");

    var addProductObject = CustomObjectMgr.queryCustomObjects("FastOMSAddProductsQueue", "", "creationDate asc").first();
    if (!addProductObject) {
        return false;
    }

    try {
        var orderObj = parseOCAPIBody(addProductObject.custom.requestBody);
        var order = OrderMgr.getOrder(addProductObject.custom.orderNo);
        logCustomer();

        var basket;
        Transaction.wrap(function () {
            assignCustomerGroups(order, "assign");
            // will copy gift certificates only to maintain the value correct on the comparison
            basket = copyToBasket(order, orderObj);
            var productToAdd = orderObj.addProductsPidAdd;
            var newProductLine = updateBasket(basket, orderObj, null, productToAdd);

            if (newProductLine.success) {
                newProductLine = newProductLine.pli;
                var priceObj = getValues(basket);
                var lostProducts = getLostBonusItems(order, basket);
                var addedProducts = getAddedBonusItems(order, basket);
                var lostBonusDiscounts = getLostDiscountLineItems(order, basket);
                var addedBonusDiscounts = getAddedDiscountLineItems(order, basket);
                var templateParams = new HashMap();
                templateParams.put("productLineItem", newProductLine);
                templateParams.put("Order", order);
                templateParams.put("fastOMSHelper", Helper);
                templateParams.put("Prices", priceObj);

                var newTR = new Template("tabs/components/productLine").render(templateParams).getText();

                removeDuplicates(lostProducts, addedProducts);
                removeDuplicates(lostBonusDiscounts, addedBonusDiscounts);

                var promotionsMsg = [];
                promotionsMsg.push.apply(promotionsMsg, getArrayOfResources("order.totals.promotions.lostproduct", lostProducts));
                promotionsMsg.push.apply(promotionsMsg, getArrayOfResources("order.totals.promotions.addedproduct", addedProducts));
                promotionsMsg.push.apply(promotionsMsg, getArrayOfResources("order.totals.promotions.lostpromotion", lostBonusDiscounts));
                promotionsMsg.push.apply(promotionsMsg, getArrayOfResources("order.totals.promotions.addedpromotion", addedBonusDiscounts));
                var promotions = getUsedPromotions(basket);

                order.custom.LOMSAddPorductsObject = JSON.stringify({
                    success: true,
                    newTR: newTR,
                    totals: {
                        lineItemsPriceInfo: priceObj.lineItemsPriceInfo,
                        products: priceObj.products,
                        shipping: priceObj.shipping,
                        discount: priceObj.discount,
                        tax: basket.getTotalTax().getValue(),
                        total: basket.getTotalGrossPrice().getValue(),
                        promotionMsg: promotionsMsg,
                        promotions: promotions
                    }
                });
            } else {
                var responseMsg = newProductLine.responseMsg;
                FOMSLogger.error("Error " + responseMsg + " during [CALCULATE ORDER]");

                order.custom.LOMSAddPorductsObject = JSON.stringify({
                    success: false,
                    msg: responseMsg ? responseMsg.toString() : false
                });
            }

            order.custom.LOMSPendingAddProducts = 2;
            if (CustomObjectMgr.getCustomObject("FastOMSAddProductsQueue", addProductObject.custom.orderNo)) {
                CustomObjectMgr.remove(addProductObject);
            }
            assignCustomerGroups(order, "unassign");
            disableFastOMSCoupons(basket);
        });
    } catch (_e) {
        FOMSLogger.error("Error in [ADD_PRODUCT] " + _e.fileName + "(" + _e.lineNumber + "): " + _e.message);
        disableFastOMSCoupons(basket);
        Transaction.wrap(function () {
            if (CustomObjectMgr.getCustomObject("FastOMSAddProductsQueue", addProductObject.custom.orderNo)) {
                CustomObjectMgr.remove(addProductObject);
            }
            assignCustomerGroups(order, "unassign");
            order.custom.LOMSPendingAddProducts = 0;
        });
    }
}

module.exports = {
    addProducts: addProducts
};
