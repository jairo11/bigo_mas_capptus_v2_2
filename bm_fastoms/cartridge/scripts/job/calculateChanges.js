/**
 *  Property of OSF Global Services, Inc., (with its brand OSF Commerce). OSF remains the sole owner of all right, title and interest in the software.
 *  Do not copy, sell, reverse engineer or otherwise attempt to derive or obtain information about the functioning, manufacture or operation therein.
 */

"use strict";

var OrderMgr = require("dw/order/OrderMgr");
var Transaction = require("dw/system/Transaction");
var Helper = require("*/cartridge/scripts/fastOMSHelper");
var FOMSLogger = require("dw/system/Logger").getLogger("FastOMS", "FastOMS");
var CustomObjectMgr = require("dw/object/CustomObjectMgr");
var getValues = Helper.getValues;
var parseOCAPIBody = Helper.parseOCAPIBody;
var logCustomer = Helper.logCustomer;
var copyToBasket = Helper.copyToBasket;
var updateBasket = Helper.updateBasket;
var getLostBonusItems = Helper.getLostBonusItems;
var getAddedBonusItems = Helper.getAddedBonusItems;
var getAddedDiscountLineItems = Helper.getAddedDiscountLineItems;
var getLostDiscountLineItems = Helper.getLostDiscountLineItems;
var getArrayOfResources = Helper.getArrayOfResources;
var removeDuplicates = Helper.removeDuplicates;
var assignCustomerGroups = Helper.assignFastOMSUSerToOrderCustomerGroups;
var getUsedPromotions = Helper.getUsedPromotions;
var removeOldRequests = Helper.removeOldRequests;
var disableFastOMSCoupons = Helper.disableFastOMSCoupons;

/**
 * @description calculate order totals after changes
 * job needed as a workarond to avoid platform limitation: ordes managing in BM only with USD
 */
function calculateChanges() {
    // Removing old requests from queue which are broken, timedout etc.
    removeOldRequests("FastOMSCalcChangQueue");

    var calculateChangesObject = CustomObjectMgr.queryCustomObjects("FastOMSCalcChangQueue", "", "creationDate asc").first();
    if (!calculateChangesObject) {
        return false;
    }

    try {
        var orderObj = parseOCAPIBody(calculateChangesObject.custom.requestBody);
        var order = OrderMgr.getOrder(calculateChangesObject.custom.orderNo);

        logCustomer();
        var basket;
        Transaction.wrap(function () {
            assignCustomerGroups(order, "assign");
            // will copy gift certificates only to maintain the value correct on the comparison
            basket = copyToBasket(order, true, (!orderObj.pidRemove), orderObj);
            var productToRemove = orderObj.pidRemove;
            var responseObj = updateBasket(basket, orderObj, productToRemove, null);
            var responseMsg = responseObj.responseMsg;

            if (!responseObj.success) {
                var success = false;
                FOMSLogger.error("Error " + responseMsg + " during [CALCULATE ORDER]");

                order.custom.LOMSResponseMessage = JSON.stringify({
                    success: success,
                    msg: responseMsg ? responseMsg.toString() : false
                });
            } else {
                var priceObj = getValues(basket);
                var lostProducts = getLostBonusItems(order, basket);
                var addedProducts = getAddedBonusItems(order, basket);
                var lostBonusDiscounts = getLostDiscountLineItems(order, basket);
                var addedBonusDiscounts = getAddedDiscountLineItems(order, basket);

                removeDuplicates(lostProducts, addedProducts);
                removeDuplicates(lostBonusDiscounts, addedBonusDiscounts);

                var promotionsMsg = [];
                promotionsMsg.push.apply(promotionsMsg, getArrayOfResources("order.totals.promotions.lostproduct", lostProducts));
                promotionsMsg.push.apply(promotionsMsg, getArrayOfResources("order.totals.promotions.addedproduct", addedProducts));
                promotionsMsg.push.apply(promotionsMsg, getArrayOfResources("order.totals.promotions.lostpromotion", lostBonusDiscounts));
                promotionsMsg.push.apply(promotionsMsg, getArrayOfResources("order.totals.promotions.addedpromotion", addedBonusDiscounts));

                order.custom.LOMSResponseMessage = JSON.stringify({
                    success: true
                });
                var promotions = getUsedPromotions(basket);
                order.custom.LOMSCalcChangObject = JSON.stringify({
                    lineItemsPriceInfo: priceObj.lineItemsPriceInfo,
                    products: priceObj.products,
                    shipping: priceObj.shipping,
                    all: priceObj,
                    shippingMethod: basket.getDefaultShipment().shippingMethod.displayName,
                    discount: priceObj.discount,
                    tax: basket.getTotalTax().getValue(),
                    total: basket.getTotalGrossPrice().getValue(),
                    promotionMsg: promotionsMsg,
                    promotions: promotions
                });
            }

            assignCustomerGroups(order, "unassign");
            if (CustomObjectMgr.getCustomObject("FastOMSCalcChangQueue", calculateChangesObject.custom.orderNo)) {
                CustomObjectMgr.remove(calculateChangesObject);
            }
            order.custom.LOMSPendingCalcChang = 2;
            disableFastOMSCoupons(basket);
        });
    } catch (_e) {
        FOMSLogger.error("Error in [CALCULATE] " + _e.fileName + "(" + _e.lineNumber + "): " + _e.message + "object = " + JSON.stringify(calculateChangesObject));
        disableFastOMSCoupons(basket);
        Transaction.wrap(function () {
            assignCustomerGroups(order, "unassign");
            if (CustomObjectMgr.getCustomObject("FastOMSCalcChangQueue", calculateChangesObject.custom.orderNo)) {
                CustomObjectMgr.remove(calculateChangesObject);
            }
            order.custom.LOMSPendingCalcChang = 0;
        });
    }
}

module.exports = {
    calculateChanges: calculateChanges
};
