/**
 *  Property of OSF Global Services, Inc., (with its brand OSF Commerce). OSF remains the sole owner of all right, title and interest in the software.
 *  Do not copy, sell, reverse engineer or otherwise attempt to derive or obtain information about the functioning, manufacture or operation therein.
 */
/* eslint-disable eqeqeq */

"use strict";

var OrderMgr = require("dw/order/OrderMgr");
var Transaction = require("dw/system/Transaction");
var Helper = require("*/cartridge/scripts/fastOMSHelper");
var CustomObjectMgr = require("dw/object/CustomObjectMgr");
var FOMSLogger = require("dw/system/Logger").getLogger("FastOMS", "FastOMS");
var Resource = require("dw/web/Resource");
var getPaymentManager = Helper.getPaymentManager;
var orderCreateAndCancel = Helper.orderCreateAndCancel;
var addEditionOrRefundNote = Helper.addEditionOrRefundNote;
var sendEmail = Helper.sendEmail;
var cancelAndNewFlow = Helper.cancelAndNewFlow;
var refundPartialFlow = Helper.refundPartialFlow;
var getNonGiftPaymentInstrument = Helper.getNonGiftPaymentInstrument;
var parseOCAPIBody = Helper.parseOCAPIBody;
var logCustomer = Helper.logCustomer;
var unlockOrder = Helper.unlockOrder;
var assignCustomerGroups = Helper.assignFastOMSUSerToOrderCustomerGroups;
var removeOldRequests = Helper.removeOldRequests;
var reApplyOriginalCoupon = Helper.reApplyOriginalCoupon;
var disableFastOMSCoupons = Helper.disableFastOMSCoupons;

/**
 * @description save order changes job needed as a workarond to avoid platform limitation: ordes creating in BM only with USD
 */
function saveOrder() {
    // Removing old requests from queue which are broken, timedout etc.
    removeOldRequests("FastOMSSaveOrderQueue");

    var saveOrderObject = CustomObjectMgr.queryCustomObjects("FastOMSSaveOrderQueue", "", "creationDate asc").first();
    if (!saveOrderObject) {
        return false;
    }

    try {
        var orderObj = parseOCAPIBody(saveOrderObject.custom.requestBody);
        var order = OrderMgr.getOrder(saveOrderObject.custom.orderNo);
        logCustomer();

        var paymentManager = getPaymentManager(order);
        var newOrderNo = null;
        var success = false;
        var currentValue = order.getTotalGrossPrice().getDecimalValue();
        var newValue = parseFloat(orderObj.orderTotal);
        var responseMsg = "";

        orderObj.billing.countryCode = order.billingAddress.countryCode;
        orderObj.shipping.countryCode = order.shipments[0].shippingAddress.countryCode;

        if (paymentManager) {
            Transaction.begin();
            assignCustomerGroups(order, "assign");
            // verifies if this payment manager accepts new authorizations
            if (paymentManager.canAuthorize(order)) {
                var responseObj = cancelAndNewFlow(paymentManager, order, orderObj);
                success = responseObj.success;
                responseMsg = responseObj.responseMsg;
                if (success == true) {
                    newOrderNo = responseObj.newOrderNo;
                    Transaction.commit();
                } else {
                    success = false;
                    FOMSLogger.error("[SAVE ORDER] Error: " + responseMsg);
                    Transaction.rollback();
                }
            } else if (currentValue > newValue) {
                success = refundPartialFlow(paymentManager, order, orderObj);
            } else {
                FOMSLogger.error("This Payment Manager does not accept the automatic creation of orders, so the total value cannot be higher than or equal the original value.");
            }
        } else {
            Transaction.begin();
            var response = orderCreateAndCancel(order, orderObj);
            var newOrder = response.newOrder;

            if (newOrder) {
                newOrderNo = newOrder.orderNo;
                unlockOrder(order);
                var newPI = getNonGiftPaymentInstrument(newOrder);
                newPI.paymentTransaction.transactionID = newOrderNo;
                sendEmail("mail/orderCreationEmail", newOrder.customerEmail, Resource.msgf("fastoms.replacedescription", "fastoms", null, orderObj.orderNo, newOrderNo), { order: newOrder, oldOrder: order });
                Transaction.commit();
            } else {
                Transaction.rollback();
            }
        }

        Transaction.wrap(function () {
            if (newOrderNo) {
                if (!newOrder) {
                    newOrder = OrderMgr.getOrder(newOrderNo);
                }

                order.custom.LOMSNewOrderJob = newOrderNo;
                reApplyOriginalCoupon(order, newOrder);
                addEditionOrRefundNote(newOrder, "EDIT", orderObj);
            } else {
                order.custom.LOMSNewOrderJob = orderObj.oldOrderNo;
            }

            disableFastOMSCoupons(order);
            assignCustomerGroups(order, "unassign");
            order.custom.LOMSPendingSaveOrder = false;
            order.custom.LOMSResponseMessage = JSON.stringify({
                success: success,
                msg: responseMsg ? responseMsg.toString() : false
            });
            if (CustomObjectMgr.getCustomObject("FastOMSSaveOrderQueue", saveOrderObject.custom.orderNo)) {
                CustomObjectMgr.remove(saveOrderObject);
            }
        });
    } catch (_e) {
        FOMSLogger.error("Error in [SAVE] " + _e.fileName + "(" + _e.lineNumber + "): " + _e.message);
        disableFastOMSCoupons(order);
        Transaction.wrap(function () {
            assignCustomerGroups(order, "unassign");
            if (CustomObjectMgr.getCustomObject("FastOMSSaveOrderQueue", saveOrderObject.custom.orderNo)) {
                CustomObjectMgr.remove(saveOrderObject);
            }
            order.custom.LOMSPendingSaveOrder = false;
        });
    }
}

module.exports = {
    saveOrder: saveOrder
};
