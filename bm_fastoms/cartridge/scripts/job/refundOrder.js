/**
 *  Property of OSF Global Services, Inc., (with its brand OSF Commerce). OSF remains the sole owner of all right, title and interest in the software.
 *  Do not copy, sell, reverse engineer or otherwise attempt to derive or obtain information about the functioning, manufacture or operation therein.
 */

/* eslint-disable guard-for-in, eqeqeq */

"use strict";

var OrderMgr = require("dw/order/OrderMgr");
var Transaction = require("dw/system/Transaction");
var Helper = require("*/cartridge/scripts/fastOMSHelper");
var CustomObjectMgr = require("dw/object/CustomObjectMgr");
var FOMSLogger = require("dw/system/Logger").getLogger("FastOMS", "FastOMS");
var Resource = require("dw/web/Resource");
var Site = require("dw/system/Site");
var removeShipmentValue = Helper.removeShipmentValue;
var orderCreateAndCancel = Helper.orderCreateAndCancel;
var addEditionOrRefundNote = Helper.addEditionOrRefundNote;
var parseOCAPIBody = Helper.parseOCAPIBody;
var logCustomer = Helper.logCustomer;
var unlockOrder = Helper.unlockOrder;
var assignCustomerGroups = Helper.assignFastOMSUSerToOrderCustomerGroups;
var removeOldRequests = Helper.removeOldRequests;
var createGiftCertificate = Helper.createGiftCertificate;
var reApplyOriginalCoupon = Helper.reApplyOriginalCoupon;
var disableFastOMSCoupons = Helper.disableFastOMSCoupons;
var sendEmail = Helper.sendEmail;

/**
 * @description refund order
 * job needed as a workarond to avoid platform limitation: ordes creating in BM only with USD
 */
function refundOrder() {
    // Removing old requests from queue which are broken, timedout etc.
    removeOldRequests("FastOMSRefundQueue");

    var refundOrderObject = CustomObjectMgr.queryCustomObjects("FastOMSRefundQueue", "", "creationDate asc").first();
    if (!refundOrderObject) {
        return false;
    }

    try {
        var orderObj = parseOCAPIBody(refundOrderObject.custom.requestBody);
        var order = OrderMgr.getOrder(refundOrderObject.custom.orderNo);
        logCustomer();


        var success = true;
        var refundShipping = orderObj.refundShipping;
        var products = [];
        var tempArray = [];
        var responseMsg = "";

        // Gift
        if (orderObj.gift > 0) {
            createGiftCertificate(order, orderObj.gift, orderObj);
        }

        // products without return
        if (orderObj.refundProducts) {
            products = orderObj.refundProducts;
        }

        // products with return
        var returnProducts = JSON.parse(order.custom.LOMSReturnItems);

        // search products with confirmed return and without refund, to be refunded
        if (returnProducts && returnProducts.items) {
            tempArray = returnProducts.items.reduce(function (acc, el) {
                if (el.ret && !el.refunded) {
                    acc[el.pid] = (acc[el.pid] || 0) + 1;
                }
                return acc;
            }, {});

            Object.keys(tempArray).forEach(function (productID) {
                products.push({
                    pid: productID,
                    qtd: tempArray[productID],
                    ret: true
                });
            });
        }

        if (!orderObj.billing.countryCode || orderObj.billing.countryCode.length === 0) {
            orderObj.billing.countryCode = order.billingAddress.countryCode;
        }

        if (!orderObj.shipping.countryCode || orderObj.shipping.countryCode.length === 0) {
            orderObj.shipping.countryCode = order.shipments[0].shippingAddress.countryCode;
        }

        Transaction.begin();
        assignCustomerGroups(order, "assign");

        if (refundShipping) {
            order.custom.LOMSShippingRefunded = true;
            removeShipmentValue(order);
        }

        order.custom.LOMSRefundedItems = JSON.stringify(products);

        // prepare new order to be created
        var newOrderObj = orderObj;
        newOrderObj.changes.refunds = products;

        var responseObj = orderCreateAndCancel(order, newOrderObj);
        var newOrder = responseObj.newOrder;
        responseMsg = responseObj.responseMsg;

        if (newOrder) {
            Transaction.commit();
        } else {
            success = false;
            Transaction.rollback();
        }
        if (success == true) {
            Transaction.wrap(function () {
                addEditionOrRefundNote(newOrder, "REFUND", orderObj);
            });
        }

        Transaction.wrap(function () {
            if (newOrder) {
                var newPaymentTransaction = Helper.getPaymentManagerTransaction(newOrder);
                var oldPaymentTransaction = Helper.getPaymentManagerTransaction(order);
                order.custom.LOMSNewOrderJob = newOrder.orderNo;

                if (newPaymentTransaction) {
                    newPaymentTransaction.setTransactionID(oldPaymentTransaction.getTransactionID());
                }

                newOrder.custom.LOMSRefundedAmount = order.custom.LOMSRefundedAmount;
                reApplyOriginalCoupon(order, newOrder);

                var siteName = Site.getCurrent().getName();
                sendEmail("mail/orderRefundEmail", order.customerEmail, Resource.msgf("mail.refund.subject", "fastoms", null, siteName), { order: order });

            }
            unlockOrder(order);
            order.custom.LOMSPendingRefund = false;
            order.custom.LOMSResponseMessage = JSON.stringify({
                success: success,
                msg: responseMsg ? responseMsg.toString() : false
            });

            disableFastOMSCoupons(order);
            assignCustomerGroups(order, "unassign");
            if (CustomObjectMgr.getCustomObject("FastOMSRefundQueue", refundOrderObject.custom.orderNo)) {
                CustomObjectMgr.remove(refundOrderObject);
            }
        });
    } catch (_e) {
        FOMSLogger.error("Error in [REFUND] " + _e.fileName + "(" + _e.lineNumber + "): " + _e.message);
        disableFastOMSCoupons(order);
        Transaction.wrap(function () {
            if (CustomObjectMgr.getCustomObject("FastOMSRefundQueue", refundOrderObject.custom.orderNo)) {
                CustomObjectMgr.remove(refundOrderObject);
            }
            assignCustomerGroups(order, "unassign");
            order.custom.LOMSPendingRefund = false;
        });
    }
}

module.exports = {
    refundOrder: refundOrder
};
