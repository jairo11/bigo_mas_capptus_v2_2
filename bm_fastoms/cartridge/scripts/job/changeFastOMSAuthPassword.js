/**
 *  Property of OSF Global Services, Inc., (with its brand OSF Commerce). OSF remains the sole owner of all right, title and interest in the software.
 *  Do not copy, sell, reverse engineer or otherwise attempt to derive or obtain information about the functioning, manufacture or operation therein.
 */

"use strict";

/* eslint-disable no-use-before-define, new-cap */
var FOMSLogger = require("dw/system/Logger").getLogger("FOMSLogger", "FOMSLogger");
var Transaction = require("dw/system/Transaction");
var Site = require("dw/system/Site");
var StringUtils = require("dw/util/StringUtils");
var System = require("dw/system/System");
var LocalServiceRegistry = require("dw/svc/LocalServiceRegistry");

/**
 * @description change FastOMS BM user password to avoid 30 days password expiration limitation
 * @param {Object} parameters job parameters
 */
exports.ChangeFastOMSAuthPassword = function (parameters) {
    try {
        var userPasswordExpirationDateOCAPICallResponse = UserPasswordExpirationDateOCAPICall();
        var pwExpirationDateObject = safeParseJSON(userPasswordExpirationDateOCAPICallResponse.object, "Error changing OCPI user password");
        var pwExpirationDateString = pwExpirationDateObject ? pwExpirationDateObject.password_expiration_date : null;
        var diffDays = pwExpirationDateString ? getDateDifferenceAsDays(pwExpirationDateString) : null;
        diffDays = parameters.DateOverride ? parameters.DateOverride : diffDays;

        if (diffDays && diffDays < 10) {
            ChangeUserPasswordOCAPICall();
        } else if (!diffDays) {
            FOMSLogger.error("FastOMS: Error while calculating the diffDays");
        }
    } catch (error) {
        FOMSLogger.error("FastOMS: Generic ChangeFastOMSAuthPassword error: " + error);
    }
};

/**
 * @description return FastOMS BM user password expiration date
 * @returns {Object} Service status
 */
function UserPasswordExpirationDateOCAPICall() {
    var authToken = getOCAPIAuthToken();
    var service = LocalServiceRegistry.createService("FastOMSUserPasswordExpirationDate", {
        createRequest: function (_service) { // NOSONAR
            updateServiceURL(_service);
            _service.addHeader("Content-Type", "application/json");
            _service.addHeader("Authorization", "Bearer " + authToken);
            _service.setRequestMethod("GET");
        },
        parseResponse: function (_service, args) {
            return args.text;
        },
        filterLogMessage: function (msg) {
            return msg;
        }
    });

    if (authToken && !authToken.error) {
        return service.call();
    }
    return {
        error: true,
        message: authToken.message
    };
}

/**
 * @description change FastOMS BM user password call
 * @returns {Object} service status
 */
function ChangeUserPasswordOCAPICall() {
    var authToken = getOCAPIAuthToken();
    var currentUserPassword = getOCAPIUserPassword();
    var generatedPassword = generateOCAPIUserPassword();
    var FastOMSChangeUserPasswordService = LocalServiceRegistry.createService("FastOMSChangeUserPassword", {
        // eslint-disable-next-line
        createRequest: function (service, params) {//NOSONAR
            updateServiceURL(service);
            service.addHeader("Content-Type", "application/json");
            service.addHeader("Authorization", "Bearer " + authToken);
            service.setRequestMethod("PATCH");
            return JSON.stringify({
                current_password: currentUserPassword,
                password: generatedPassword
            });
        },
        // eslint-disable-next-line consistent-return
        parseResponse: function (_service, args) {
            // eslint-disable-next-line eqeqeq
            if (args.statusCode == 200 && args.text) {
                FOMSLogger.debug("FastOMS: Generated pw after user pw change: " + generatedPassword);
                AuthPasswordUpdate(generatedPassword);
                return args.text;
            }
        },
        filterLogMessage: function (msg) {
            return msg;
        }
    });

    if (authToken && !authToken.error) {
        var response = FastOMSChangeUserPasswordService.call();
        return response;
    }
    return {
        error: true,
        message: authToken.message
    };
}

/**
 * @description update BM preferences with new BM password
 * @param {string} password authentification password
 */
function AuthPasswordUpdate(password) {
    var orgPrefs = System.getPreferences();
    var currentAuth = orgPrefs.getCustom().FastOMSOCAPIAuth;
    var newAuth = "";
    if (!empty(currentAuth)) {
        newAuth = StringUtils.decodeBase64(currentAuth);
        var authObject = newAuth.split(":");
        newAuth = authObject[0] + ":" + password + ":" + authObject[2];
        newAuth = StringUtils.encodeBase64(newAuth);
        Transaction.wrap(function () {
            orgPrefs.getCustom().FastOMSOCAPIAuth = newAuth;
        });
    }
}

/**
 * @description return OCAPI Auth Token
 * @param {string} customPasswordOverride customer password
 * @returns {Object} service status
 */
function getOCAPIAuthToken(customPasswordOverride) {
    var orgPrefs = System.getPreferences();
    var service = LocalServiceRegistry.createService("FastOMSAuth", {
        createRequest: function (_service) { // NOSONAR
            var base64Credentials = orgPrefs.getCustom().FastOMSOCAPIAuth;
            var currentAuth = StringUtils.decodeBase64(base64Credentials);
            var authObject = currentAuth.split(":");
            var newBase64Credentials = "";
            if (!empty(authObject) && !empty(authObject[2])) {
                _service.addParam("client_id", authObject[2]);
            }

            if (!empty(authObject) && !empty(customPasswordOverride)) {
                newBase64Credentials = StringUtils.encodeBase64(authObject[0] + ":" + customPasswordOverride + ":" + authObject[2]);
                _service.addHeader("Authorization", "Basic " + newBase64Credentials);
            } else {
                _service.addHeader("Authorization", "Basic " + base64Credentials);
            }

            updateServiceURL(_service);
            _service.addHeader("Content-Type", "application/x-www-form-urlencoded");
            _service.setRequestMethod("POST");
            return "grant_type=urn:demandware:params:oauth:grant-type:client-id:dwsid:dwsecuretoken";
        },
        // eslint-disable-next-line
        parseResponse: function (service, params) { // NOSONAR
            return params.text;
        },
        filterLogMessage: function (msg) {
            return msg;
        }
    });

    var response = service.call();
    var responseObj = safeParseJSON(response.object);

    if (responseObj) {
        return responseObj.access_token;
    }
    var errorMessage = safeParseJSON(response.errorMessage);
    errorMessage = errorMessage ? errorMessage.error_description : response.errorMessage;
    return {
        error: true,
        message: errorMessage
    };
}

/**
 * @description parse response JSON
 * @param {string} jsonObject json to parse
 * @param {string} errorDescription error message
 * @returns {Object} parsed json object
 */
function safeParseJSON(jsonObject, errorDescription) {
    var result = null;
    try {
        result = JSON.parse(jsonObject);
    } catch (error) {
        var errorMessage = errorDescription || "FastOMS: An error has occured while parsing JSON";
        FOMSLogger.error(errorMessage + ": " + error);
    }
    return result;
}

/**
 * @description update Service URL with current site data and OCAPI preferences
 * @param {dw.svc.Service} service service object
 */
function updateServiceURL(service) {
    var url = service.getURL();
    var hostName = Site.current.httpsHostName;
    var siteID = Site.current.ID;
    var version = "v" + Site.getCurrent().getPreferences().getCustom().FOMSOCAPIVersion.replace(".", "_");
    var clientId = Site.getCurrent().getPreferences().getCustom().FOMSOCAPIClientID;
    url = url.replace("{hostName}", hostName);
    url = url.replace("{siteID}", siteID);
    url = url.replace("{version}", version);
    url = url.replace("{clientId}", clientId);
    service.setURL(url);
}

/**
 * @description generate new password
 * @returns {string} new password
 */
function generateOCAPIUserPassword() {
    var constantPasswordPrefix = "q%9R"; // add this to prefs
    var UUIDUtils = require("dw/util/UUIDUtils");
    var uuid = UUIDUtils.createUUID();
    return constantPasswordPrefix + uuid;
}

/**
 * @description get current OCAPI User Password from preferences
 * @returns {string} curent password
 */
function getOCAPIUserPassword() {
    var orgPrefs = System.getPreferences();
    var currentAuth = orgPrefs.getCustom().FastOMSOCAPIAuth;
    if (!empty(currentAuth)) {
        currentAuth = StringUtils.decodeBase64(currentAuth);
        var authObject = currentAuth.split(":");
        return authObject[1];
    }
    return null;
}

/**
 * @description return gifference in days between two dates
 * @param {Date} dateParam date to validate
 * @returns {number} date diff
 */
function getDateDifferenceAsDays(dateParam) {
    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    var today = new Date();
    var diffDays = dateParam ? Math.round(Math.abs((new Date(dateParam).getTime() - today.getTime()) / (oneDay))) : null;
    return diffDays;
}
