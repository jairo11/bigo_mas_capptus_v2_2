"use strict";

var OrderMgr = require("dw/order/OrderMgr");
var Logger = require("dw/system/Logger");
var Helper = require("*/cartridge/scripts/fastOMSHelper");
var HOUR_IN_MILLS = 3600000;


module.exports.execute = function (params) {
    var orderIterator = OrderMgr.searchOrders(
        "custom.orderLock = true",
        "creationDate asc",
        null
    );

    var minutesInMilss = params.minutes ? parseInt(params.minutes, 10) * 60000 : null;
    var dateToCompare = minutesInMilss || HOUR_IN_MILLS;

    while (orderIterator.hasNext()) {
        var order = orderIterator.next();
        var orderLockJsonString = order.custom.orderLockJson;
        var orderLockJson = orderLockJsonString ? JSON.parse(orderLockJsonString) : {};
        var lockCalendarDate = orderLockJson.date ? new Date(orderLockJson.date) : null;
        var now = new Date();
        if (lockCalendarDate == null || (now.getTime() - lockCalendarDate.getTime()) > dateToCompare) {
            Helper.unlockOrder(order);
            Logger.info("Unlocked Order Number " + order.orderNo);
        }
    }
};
