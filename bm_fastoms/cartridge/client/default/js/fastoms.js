/**
 *  Property of OSF Global Services, Inc., (with its brand OSF Commerce). OSF remains the sole owner of all right, title and interest in the software.
 *  Do not copy, sell, reverse engineer or otherwise attempt to derive or obtain information about the functioning, manufacture or operation therein.
 */

/* global Resources, Urls */
/* eslint-disable valid-jsdoc, require-jsdoc, no-use-before-define, eqeqeq, no-eval,  no-underscore-dangle*/

"use strict";

/**
 * @module fastoms
 */
// Global Variable to follow the changes in the current order
var orderChanges = {};

//This object created for stored historical datas for order
var orderNotes = [];

var currentQuery = null;
var lastQuery = null;
var runningQuery = null;
var listTotal = -1;
var listCurrent = -1;
var delay = 30;
var lastQueryDate = new Date();
var $resultsContainer;
var jQuery = window.jQuery;

function setError(message, modalTitle) {
    jQuery("#errorModal").modal("show");
    jQuery("#errorModalBody").html(message);
    if (modalTitle) {
        jQuery(".modal-title").text(modalTitle);
    }
}

/**
 * @function
 * @description Handles keyboard's arrow keys
 * @param keyCode Code of an arrow key to be handled
 */
function handleArrowKeys(keyCode) {
    switch (keyCode) {
        case 38:
            // keyUp
            listCurrent = (listCurrent <= 0) ? (listTotal - 1) : (listCurrent - 1);
            break;
        case 40:
            // keyDown
            listCurrent = (listCurrent >= listTotal - 1) ? 0 : listCurrent + 1;
            break;
        default:
            // reset
            listCurrent = -1;
            return false;
    }

    $resultsContainer.children().removeClass("selected").eq(listCurrent).addClass("selected");
    jQuery("input[name=\"q\"]").val($resultsContainer.find(".selected .suggestionterm").first().text());
    return true;
}

function addProductAjax(that, e, productToAdd) {
    // build the request url
    var productId = that.find(".suggestion-sku").text();
    jQuery("#totals .loms-loader").removeClass("hidden");
    jQuery(".saveBtn").attr("disabled", true);

    jQuery.ajax({
        type: "POST",
        url: Urls.addProduct,
        data: {
            pidAdd: productToAdd,
            order: prepareOrderObj(jQuery("#orderForm").serializeArray())
        }
    }).done(function (response) {
        if (response && response.success && !response.jobWaiting) {
            if (!jQuery(".giftCertificateLineItem .column-remove").length) {
                jQuery(".giftCertificateLineItem").append("<td class=\"column-remove\">&nbsp;</td>");
            }

            if (!orderChanges.added) {
                orderChanges.added = [];
            }
            orderChanges.added.push(that.find(".suggestion-name").text());

            if (orderChanges.removed) {
                orderChanges.removed = orderChanges.removed.filter(function (product) {
                    if (JSON.parse(product).pid != productToAdd) {
                        return product;
                    }
                });
            }

            if (orderChanges.removed && orderChanges.removed.length == 0) {
                delete orderChanges.removed;
            }

            createTransactionOrderNotes(productToAdd, "ADDED");

            redoTotals(response.totals);
            if (response.promotionMsg && response.promotionMsg.length > 0) {
                var html = "";
                for (var p = 0; p < response.promotionMsg.length; p++) {
                    html += "<tr><td>" + response.promotionMsg[p] + "</td></tr>";
                }
                jQuery("#promotion-changes").empty().html(html);
                jQuery("#promotionModal").modal("show");
            }

            var $productTableLines = jQuery("#products-table").find("tr[not-bonus-product]");
            var firstLine = $productTableLines[0];
            var $productIdsInOrder = jQuery(".product-id");
            var addNewProduct = true;

            for (var k = 0; k < $productIdsInOrder.length; k++) {
                var pid = $productIdsInOrder[k].innerText;
                if (pid.split("-")[0] == productId) {
                    addNewProduct = false;
                    var $qtd = jQuery("input[name=Qtd_" + pid + "]");
                    var qty = parseInt($qtd.val(), 10);
                    $qtd.val(qty + 1);
                }
            }

            if (addNewProduct) {
                if (jQuery("#products-header-row .column-remove").length <= 0) {
                    var newTH = "<th class='column-remove'>" + Resources.REMOVE + "</th>";
                    jQuery("#products-header-row").append(newTH);
                }
                if (response.newTR) {
                    jQuery("#products-table").append(response.newTR);
                    if ($productTableLines.length == 1 && !jQuery(firstLine).hasClass("giftCertificateLineItem")) {
                        //now that the table will have more than 1 element, we can allow the former only product to be removed,
                        // unless that product is a gift certificate.
                        var removeCell = document.createElement("td");
                        var removeButton = document.createElement("div");
                        var $removeButton = jQuery(removeButton);

                        removeCell.classList.add("column-remove");
                        $removeButton.addClass("removeCTA");
                        $removeButton.addClass("removeBtn");

                        removeButton.id = "Del_" + jQuery(firstLine).find(".product-id")[0].innerText;
                        removeCell.appendChild(removeButton);
                        firstLine.append(removeCell);
                    }
                }
            }

            jQuery(".saveBtn").removeAttr("disabled");
            jQuery(".giftCertificateLineItem .column-remove").removeClass("hidden");
        } else if (response.jobWaiting && response.success) {
            setTimeout(addProductAjax, 1000, that, e, productToAdd);
        } else if (!response.success && response.responseMsg) {
            setError(response.responseMsg);
            if (Object.keys(orderChanges).length > 0) {
                jQuery(".saveBtn").removeAttr("disabled");
            }
            jQuery("#totals .loms-loader").addClass("hidden");
        } else {
            setError(Resources.UNEXPECTED_ERROR);
        }

        if (response.totals) {
            handlePromotions(response.totals);
        }

    }).fail(function () {
        setError(Resources.UNEXPECTED_ERROR);
    });
}

var searchsuggest = {
    /**
     * @function
     * @description Configures parameters and required object instances
     */
    init: function (container, defaultValue) {
        var searchField = "input[name=\"q\"]";

        // on focus listener (clear default value)
        jQuery(document).on("focus", searchField, function () {
            if (!$resultsContainer) {
                // create results container if needed
                $resultsContainer = jQuery("<div/>").attr({ id: "search-suggestions", class: "col-10" }).appendTo(jQuery(container));
            }
            if (jQuery(searchField).val() === defaultValue) {
                jQuery(searchField).val("");
            }
        });
        jQuery(document).on("click", ".product-suggestion", function (e) {
            var that = jQuery(this);
            var productId = that.find(".suggestion-sku").text();
            jQuery.ajax({
                type: "POST",
                url: Urls.checkDuplicateProduct,
                data: {
                    pidAdd: productId,
                    order: prepareOrderObj(jQuery("#orderForm").serializeArray())
                }
            }).done(function (response) {
                if (response.success) {
                    addProductAjax(that, e, response.pid);
                } else {
                    setError(response.message);
                }
            }).fail(function () {
                setError(Resources.UNEXPECTED_ERROR);
            });
        });
        jQuery(document).on("click", container, function () {
            setTimeout(this.clearResults, 200);
        }.bind(this));
        // on key up listener
        jQuery(document).on("keyup", searchField, function (e) {
            // get keyCode (window.event is for IE)
            var keyCode = e.keyCode || window.event.keyCode;

            // check and treat up and down arrows
            if (handleArrowKeys(keyCode)) {
                return;
            }
            // check for an ENTER or ESC
            if (keyCode === 13 || keyCode === 27) {
                /* eslint-disable no-unused-expressions */
                this.clearResults();
                return;
                /* eslint-enable no-unused-expressions */
            }

            currentQuery = jQuery(searchField).val().trim();

            var currentQueryDate = new Date();
            lastQueryDate.setSeconds(lastQueryDate.getSeconds() + 1);
            if (lastQueryDate < currentQueryDate) {
                runningQuery = null;
                lastQueryDate = new Date();
            }

            // no query currently running, init an update
            if (!runningQuery && currentQuery.length >= 3) {
                runningQuery = currentQuery;
                setTimeout(searchsuggest.suggest, delay);
            }
        });
    },

    /**
     * @function
     * @description trigger suggest action
     */
    suggest: function () {
        // check whether query to execute (runningQuery) is still up to date and had not changed in the meanwhile
        // (we had a little delay)
        if (runningQuery !== currentQuery) {
            // update running query to the most recent search phrase
            runningQuery = currentQuery;
        }

        // if it's empty clear the results box and return
        if (runningQuery.length === 0) {
            searchsuggest.clearResults();
            runningQuery = null;
            return;
        }

        // if the current search phrase is the same as for the last suggestion call, just return
        if (lastQuery === runningQuery) {
            runningQuery = null;
            return;
        }

        // build the request url
        var reqUrl = searchsuggest.appendParamToURL(Urls.queryProducts, "q", runningQuery);

        // execute server call
        jQuery.getJSON(reqUrl, function (data) {
            if (data.available == false) {
                searchsuggest.clearResults();
            } else {
                // update the results div
                var suggestionHTML = "";

                for (var i = 0; i < data.products.length; i++) {
                    var orderableClass = (data.products[i].availability.orderable ? "orderable" : "not-orderable");
                    suggestionHTML += "<div class=\"product-suggestion " + orderableClass + "\">" +
                        "<span class=\"suggestion-name\">" + data.products[i].display + "</span>" +
                        "ID: <span class=\"suggestion-sku\">" + data.products[i].sku + "</span>" +
                        "Availability: <span class=\"suggestion-quantity\">" + data.products[i].availability.quantity + "</span>" +
                        "<span class =\"suggestion-variant-count\">" + "&nbsp" + data.products[i].variantCount + "</span></div>";
                }

                // re-init suggestions box if destroyed
                if (jQuery("#search-suggestions").length == 0) {
                    $resultsContainer = jQuery("<div/>").attr("id", "search-suggestions").appendTo(jQuery("#addProduct"));
                }
                $resultsContainer.html(suggestionHTML).fadeIn(200);
            }

            // record the query that has been executed
            lastQuery = runningQuery;
            // reset currently running query
            runningQuery = null;

            // check for another required update (if current search phrase is different from just executed call)
            if (currentQuery !== lastQuery) {
                // ... and execute immediately if search has changed while this server call was in transit
                runningQuery = currentQuery;
                setTimeout(searchsuggest.suggest, delay);
            }
        });
    },
    /**
     * @function
     * @description
     */
    clearResults: function () {
        if (!$resultsContainer) { return; }
        $resultsContainer.fadeOut(200, function () { $resultsContainer.empty(); });
    },
    /**
     * @function
     * @description appends the parameter with the given name and value to the given url and returns the changed url
     * @param {String} url the url to which the parameter will be added
     * @param {String} name the name of the parameter
     * @param {String} value the value of the parameter
     */
    appendParamToURL: function (url, name, value) {
        // quit if the param already exists
        if (url.indexOf(name + "=") !== -1) {
            return url;
        }
        var separator = url.indexOf("?") !== -1 ? "&" : "?";
        return url + separator + name + "=" + encodeURIComponent(value);
    }
};

/**
 * Order Search - Begin
 */
function initializeSearchOrder() {
    var $errorTracking = jQuery(".errorTrackingNo");

    jQuery(".searchBtn").on("click", function (e) {
        e.preventDefault();

        if (!jQuery("#simple-search-tab").hasClass("active") &&
            !(jQuery("input[name=\"orderNo\"]").val().length ||
            jQuery("input[name=\"customerName\"]").val().length ||
            jQuery("input[name=\"customerEmail\"]").val().length ||
            jQuery("select[name=\"status\"]").val().length ||
            jQuery("input[name=\"returnTracking\"]").val().length ||
            jQuery("input[name=\"trackingNumber\"]").val().length ||
            jQuery("input[name=\"startDate\"]").val().length
            )
        ) {
            $errorTracking.show();
        } else {
            $errorTracking.hide();
            searchOrders();
        }
    });

    jQuery(document).change("#searchForm :input", function () {
        $errorTracking.hide();
    });

    initilizePagination();
}
/**
 * Order Search - End
 */

/**
 * Clear All - Begin
 */
function initializeClearAll() {
    jQuery(".clearBtn").on("click", function (e) {
        e.preventDefault();
        jQuery("#pills-tabContent .tab-pane.active .searchForm").trigger("reset");
    });
}
/**
 * Clear All - End
 */

/**
 * Typeahead Configuration - Begin
 */
function initializeTypeahead() {
    jQuery(".typeahead").typeahead({
        hint: true,
        highlight: true,
        minLength: 3,
        classNames: {
            input: "typeahed-input",
            menu: "typeahead-menu",
            suggestion: "typeahead-selected"
        }
    }, {
        display: "data.orderNo",
        templates: {
            empty: [
                "<div class=\"empty-message\">",
                Resources.TYPEAHEAD_EMPTY,
                "</div>"
            ].join("\n"),
            suggestion: function (data) {
                return "<div class=\"card text-center\">" +
                    "<div class=\"card-body\">" +
                    "<h5 class=\"card-title\" style=\"font-size: 1.6rem\"> #" + data.orderNo + " - " + new Date(data.creationDate).toLocaleDateString() + " - $" + data.total + "</h5>" +
                    "<p class=\"card-text\">" + data.customerName + " (" + data.customerEmail + ")</p>" +
                    "<p class=\"card-text\"><small class=\"text-muted\">" + data.status + "</small></p></div></div>";
            }
        },
        source: function (query, sync, async) {
            var url = Urls.typeahead;
            return jQuery.post(url, { query: query }, function (data) {
                return async(data.orders);
            });
        }
    });

    jQuery(".typeahead").bind("typeahead:select", function (e, suggestion) {
        e.preventDefault();
        viewOrderDetails(suggestion.orderNo);
    });
}
/**
 * Typeahead Configuration - End
 */

/**
 * Pending Return List - Begin
 */
function initializePendingReturnList() {
    jQuery("#pending-return-tab").on("show.bs.tab", function () {
        jQuery(".orderslist .results").addClass("hidden");
        jQuery(".orderslist .loms-loader").removeClass("hidden");
        jQuery(".orderview .result").empty();
        jQuery.ajax({
            type: "GET",
            url: Urls.orderSearch
        }).done(function (response) {
            if (!response) {
                setError(Resources.UNEXPECTED_ERROR);
            } else {
                jQuery(".orderslist .results").empty().html(response);
                jQuery(".orderslist .results").removeClass("hidden");
                jQuery(".orderslist .loms-loader").addClass("hidden");
                initializeViewOrder();
            }
        });
    });
}
/**
 * Pending Return List - End
 */
function goToReplacedOrOriginalOrder() {
    if (jQuery(".js-move-to-order")) {
        jQuery(".js-move-to-order").each((n, val) => {
            jQuery(val).one("click", function (e) {
                e.preventDefault();
                var orderUrl = jQuery(this).attr("data-link");

                //
                if (Object.keys(orderChanges).length > 0) {
                    jQuery("#saveOrderModal").on("show.bs.modal", function () {
                        var text = Resources.ORDER_SAVE_CONFIRM.replace("_", jQuery("#orderNumber").text());
                        jQuery("#saveMsg").text(text);
                        jQuery("#changes").empty();
                        jQuery(".dontSaveOrder").removeClass("hidden");
                        jQuery(".dontSaveOrder").off("click").on("click", function () {
                            jQuery("#saveOrderModal").modal("hide");
                            jQuery.ajax({
                                type: "GET",
                                url: orderUrl,
                                beforeSend: function () {
                                    jQuery(".orderview .loms-loader").removeClass("hidden");
                                }
                            }).done(function (response) {
                                if (!response) {
                                    setError(Resources.UNEXPECTED_ERROR);
                                } else {
                                    prepareOrderEdition(response);
                                }
                            });

                        });
                    });
                    jQuery("#saveOrderModal").modal("show");
                } else {
                    if (jQuery(".cancelEditBtn").css("display") !== "none") {
                        var url = jQuery(".cancelEditBtn").attr("data-link");
                        e.preventDefault();
                        jQuery.ajax({
                            type: "GET",
                            url: url
                        }).done(function (response) {
                            if (!response) {
                                setError(Resources.UNEXPECTED_ERROR);
                            } else {
                                if (!response.orderLocked) {
                                    jQuery.ajax({
                                        type: "GET",
                                        url: orderUrl,
                                        beforeSend: function () {
                                            jQuery(".orderview .loms-loader").removeClass("hidden");
                                        }
                                    }).done(function (res) {
                                        if (!res) {
                                            setError(Resources.UNEXPECTED_ERROR);
                                        } else {
                                            prepareOrderEdition(res);
                                        }
                                    });
                                } else {
                                    setError(Resources.ORDER_LOCKED);
                                }
                            }
                        });
                    } else {
                        var changedOrderStatus = jQuery("#general select[name=\"orderStatus\"]").find("option:selected").text().trim();
                        var changedPaymentStatus = jQuery("#paymentStatus").text().trim();
                        var changedName = jQuery("#general input[name=\"customerName\"").val();
                        var changedEmail = jQuery("#general input[name=\"customerEmail\"").val();

                        jQuery("#orders-list .order-list-orderNo").each((m, item) => {
                            if (jQuery(item).html() == jQuery("#general #orderNumber").html()) {
                                jQuery(item).closest(".displayOrder").find(".order-list-status").html(changedOrderStatus);
                                jQuery(item).closest(".displayOrder").find(".order-list-payment-status").html(changedPaymentStatus);
                                jQuery(item).closest(".displayOrder").find(".order-list-name").html(changedName);
                                jQuery(item).closest(".displayOrder").find(".order-list-email").html(changedEmail);
                            }
                        });

                        jQuery.ajax({
                            type: "GET",
                            url: orderUrl,
                            beforeSend: function () {
                                jQuery(".orderview .loms-loader").removeClass("hidden");
                            }
                        }).done(function (response) {
                            if (!response) {
                                setError(Resources.UNEXPECTED_ERROR);
                            } else {
                                prepareOrderEdition(response);
                            }
                        });
                    }
                }
            });
        });
    }
}

/**
 * Order View - Begin
 */
function initializeViewOrder() {
    jQuery(".displayOrder").on("click", function (e) {
        e.preventDefault();
        var url = jQuery(this).attr("data-link");

        jQuery(".orderview .result").addClass("hidden");
        jQuery(".orderslist .loms-loader").removeClass("hidden");
        jQuery.ajax({
            type: "GET",
            url: url
        }).done(function (response) {
            if (!response) {
                setError(Resources.UNEXPECTED_ERROR);
            } else {
                prepareOrderEdition(response);
            }
        });
    });

    // Pagination
    jQuery("#orders-list").jplist({
        panelPath: ".list-panel",
        itemsBox: ".list",
        itemPath: ".list-item"
    });
}
/**
 * Order View - End
 */

/**
 * Order Edit - Begin
 */
function initializeEditOrder() {

    var $editBtn = jQuery(".editBtn");
    var $cancelEditBtn = jQuery(".cancelEditBtn");
    var $discount = jQuery("#discountValue");
    var $couponAdd = jQuery(".addCouponBtn");
    var $couponRemove = jQuery(".removeCouponBtn");
    var $couponCode = jQuery("#couponCode");
    var $giftCardAdd = jQuery("#addGiftCard");
    var $giftCardRemove = jQuery("#removeGiftCard");
    var $giftCardCode = jQuery("#giftCardCode");
    var $hasGiftCardLi = jQuery("input[name=\"hasGiftCertificate\"]").val();
    // orders that cannot have total value increased cannot add products
    if (jQuery("#paymentMethodID").data("restricted") == true) {
        jQuery("#searchProduct").attr("disabled", "disabled");
    }

    $couponCode.on("keydown input", function () {
        var tempVal = $couponCode.val().replace(/\s/g, "");
        $couponCode.val(tempVal);

        if ($couponCode.val() !== "") {
            $couponAdd.prop("disabled", false);
        } else {
            $couponAdd.prop("disabled", true);
        }
    });

    if (typeof $hasGiftCardLi === "undefined") {
        $giftCardCode.on("keydown", function () {
            if ($giftCardCode.val() !== "") {
                $giftCardAdd.prop("disabled", false);
            } else {
                $giftCardAdd.prop("disabled", true);
            }
        });
    }

    $giftCardAdd.on("click", function () {
        var giftCardCode = $giftCardCode.val();
        var errorContainer = jQuery(".giftcard-invalid");
        var $paidWithGift = jQuery("#totalPaidWithGift");
        var orderTotal = parseFloat(jQuery("#orderTotal").text());
        jQuery.ajax({
            type: "GET",
            url: Urls.validateGiftCertificate,
            data: { giftCardCode: giftCardCode }
        }).done(function (response) {
            if (response.success) {
                var newBalance = orderTotal > parseFloat(response.balance) ? 0 : parseFloat(response.balance) - orderTotal;
                var contents = "<h3 class=\"center\">Amount: " + response.amount + "</h3>" +
                    "<h3 class=\"center\">Balance: " + response.balance + "</h3>" +
                    "<h3 class=\"center\">New balance:" + newBalance + "</h3>";
                jQuery("#giftCertApplyModal").modal("show");
                jQuery("#giftCertApplyModal").find(".modal-body").html(contents);
                jQuery("#applyGiftCardBtn").on("click", function () {
                    jQuery("#giftCertApplyModal").modal("hide");
                    $paidWithGift.text(parseFloat($paidWithGift.text()) + parseFloat(response.balance));
                    calculateBasket();
                    $giftCardCode.prop("disabled", true);
                    $giftCardAdd.addClass("hidden");
                    errorContainer.addClass("hidden");
                    $giftCardRemove.removeClass("hidden");
                });
            } else {
                errorContainer.text(response.message);
                errorContainer.removeClass("hidden");
            }
        });
    });

    $giftCardRemove.on("click", function () {
        var $paidWithGift = jQuery("#totalPaidWithGift");
        var $initialGiftCertTotalPaid = jQuery("input[name=\"initialGiftCertTotalPaid\"]");
        $paidWithGift.text(parseFloat($initialGiftCertTotalPaid.val()));
        $giftCardCode.val("");
        delete orderChanges.giftCardCode;
        calculateBasket();
        $giftCardAdd.removeClass("hidden");
        $giftCardCode.prop("disabled", false);
        $giftCardRemove.addClass("hidden");
        $giftCardAdd.prop("disabled", true);
    });

    $couponAdd.one("click", function () {
        var couponCode = $couponCode.val();
        var errorContainer = jQuery(".coupon-invalid");
        jQuery.ajax({
            type: "GET",
            url: Urls.validateCouponCode,
            data: {
                couponCode: couponCode,
                orderNo: jQuery("#orderNumber").text()
            }
        }).done(function (response) {
            if (response.success) {
                calculateBasket();
                $couponCode.prop("disabled", true);
                $couponAdd.addClass("hidden");
                errorContainer.addClass("hidden");
                $couponRemove.removeClass("hidden");
            } else {
                delete orderChanges.couponCode;
                errorContainer.text(response.message);
                errorContainer.removeClass("hidden");
            }
        });
    });

    $couponRemove.on("click", function () {
        $couponCode.val("");
        delete orderChanges.couponCode;
        calculateBasket();
        $couponAdd.removeClass("hidden");
        $couponCode.prop("disabled", false);
        $couponRemove.addClass("hidden");
        $couponAdd.prop("disabled", true);
    });

    $editBtn.off("click").on("click", function (e) {
        if (jQuery("#couponInquiryResult").length) {
            setError(jQuery("#couponInquiryResult").text());
            return;
        }

        $editBtn.attr("disabled", "disabled");
        var url = jQuery(this).attr("data-link");
        var _this = this;
        e.preventDefault();
        jQuery.ajax({
            type: "GET",
            url: url,
            data: {
                agent: jQuery(".headermenu").text().trim()
            }
        }).done(function (response) {
            if (!response) {
                setError(Resources.UNEXPECTED_ERROR);
            } else {
                if (!response.orderLocked) {
                    enableEditOrder(_this);
                } else {
                    setError(response.responseMsg);
                }
            }
        });
    });

    $cancelEditBtn.off("click").on("click", function (e) {
        orderNotes = {};
        $cancelEditBtn.attr("disabled", "disabled");
        var url = jQuery(this).attr("data-link");
        e.preventDefault();
        jQuery.ajax({
            type: "GET",
            url: url
        }).done(function (response) {
            if (!response) {
                setError(Resources.UNEXPECTED_ERROR);
            } else {
                if (!response.orderLocked) {
                    viewOrderDetails();
                } else {
                    setError(Resources.ORDER_LOCKED);
                }
            }
        });
    });

    function enableEditOrder(editBtn) {
        if (jQuery(".table input.hidden").length > 2 && !jQuery(editBtn).hasClass("cancelEdit") && !jQuery(editBtn).hasClass("refundEdit")) {
            var currentShipping = jQuery("#hidden-current-shipping-method").val();
            jQuery(".table input, .table select, .table p").removeClass("hidden");
            jQuery(".saveBtn").removeClass("hidden");
            jQuery(".cancelBtn").removeClass("hidden");
            jQuery(".cancelEditBtn").removeClass("hidden");
            jQuery(".totalPaid").removeClass("hidden");
            jQuery(".newTotal").removeClass("hidden");
            jQuery(".column-remove").removeClass("hidden");
            jQuery(".table .variant-value").addClass("hidden");
            jQuery("#removeCoupon").addClass("hidden");
            jQuery("#editBtn").addClass("hidden");
            jQuery(".editBtn").addClass("hidden");
            jQuery("#removeGiftCard").addClass("hidden");
            jQuery("#addProduct").removeClass("hidden");
            jQuery("#shippingMethod").val(currentShipping);
            $couponAdd.prop("disabled", true);
            $giftCardAdd.prop("disabled", true);

            if (jQuery("input[name=\"paidWithGift\"]").val() == "false" && jQuery("input[name=\"isSupportedPayment\"]").val() == "true") {
                jQuery(".captureBtn").removeClass("hidden");
            }

            if (jQuery(".tdAddress").length) {
                jQuery(".tdAddress").removeClass("hidden");
            }

            $editBtn.text(Resources.CANCEL_EDITION_BUTTON);
            if ($discount.length) {
                $discount.removeAttr("readonly");
                $discount.on("change", function () {
                    orderChanges[$discount.attr("id")] = $discount.val();
                    calculateBasket();
                });
            }
        } else if (!jQuery(editBtn).hasClass("cancelEdit") && jQuery(editBtn).hasClass("refundEdit")) {
            jQuery(editBtn).addClass("cancelEdit").prop("disabled", false);
            var makeEditable = [
                "shippingStatus",
                "shippingFirstName",
                "shippingLastName",
                "shippingPhone"
            ];
            for (var i = 0; i < makeEditable.length; i++) {
                var makeEditableSelector = jQuery(".table input[name=\"" + makeEditable[i] + "\"], .table select[name=\"" + makeEditable[i] + "\"]");
                makeEditableSelector.removeClass("hidden");
                makeEditableSelector.prev().addClass("hidden");
            }

            jQuery(".saveBtn").removeClass("hidden");
            $cancelEditBtn.removeClass("hidden");
            $editBtn.addClass("hidden");

        } else {
            viewOrderDetails();
        }
    }
}
/**
 * Order Edit - End
 */

/**
 * Order Save - Begin
 */
function initializeSaveOrder() {
    jQuery(".saveOrder").one("click", function () {
        // Verify if it's a cancelation
        if (jQuery("select[name=\"orderStatus\"]").val() != Resources.STATUS_CANCELLED) {
            var $orderForm = jQuery("#orderForm");
            jQuery.ajax({
                type: "POST",
                url: $orderForm.attr("action"),
                data: {
                    order: prepareOrderObj($orderForm.serializeArray())
                }
            }).done(function (response) {
                if (response && response.success) {
                    if (response.jobExecution) {
                        viewOrderDetails(response.oldOrderNo, null, null, response.jobExecution);
                    } else {
                        viewOrderDetails(response.orderNo);
                    }
                } else if (response && !response.success && response.paidWithGift) {
                    jQuery("#saveOrderModal").modal("hide");
                    jQuery("#giftCertModal").modal("show");
                } else if (response && response.message) {
                    setError(response.message);
                } else {
                    setError(Resources.UNEXPECTED_ERROR);
                }
            }).fail(function () {
                setError(Resources.UNEXPECTED_ERROR);
            });
        } else {
            jQuery("#cancelOrderModal").modal("show");
        }
    });

    // Disable Save handler start
    var conditionsMap = [];

    conditionsMap.push({
        condition: "+jQuery('#outstanding.exceed').text() > 0 && jQuery('#paymentMethodID').data('restricted') == true",
        message: Resources.CONDITION_NO_NEW_AUTHORIZATIONS.replace("{0}", jQuery("#paymentMethodID").text().trim())
    });

    jQuery("#saveBtn").on("click", function () {
        var disableButton = false;
        var outstanding = jQuery("#outstanding").text();
        var isOutstanding = jQuery(".js-isOutstanding").text();
        var paymentProcessor = jQuery("#outstanding").data("paymentProcessor");

        if (isOutstanding == Resources.OUTSTANDING_AMOUNT && outstanding > 0 && paymentProcessor == Resources.PAYMENT_PROCESSOR_PAYPAL) {
            setError(Resources.PAY_PAL_OUTSTANDING_ERROR);
            return false;
        }

        var intentoryError = jQuery(".limit-inventory:visible").length;

        if (intentoryError) {
            jQuery("#btnSaveOrder").attr("disabled", "disabled");
            setError(Resources.CHECK_PRODUCTS_QUANTITY);
            return false;
        }

        /* eslint-disable consistent-return */
        conditionsMap.each(function (verification) {
            var valid = eval(verification.condition);
            if (valid) {
                jQuery("#cannot-save").text(verification.message);
                disableButton = true;
                return false; // break on first match
            }
        });
        /* eslint-enable consistent-return */

        if (disableButton) {
            jQuery("#btnSaveOrder").attr("disabled", "disabled");
            jQuery("#cannot-save").show();
            disableEditProducts();
        } else {
            jQuery("#btnSaveOrder").removeAttr("disabled");
            jQuery("#cannot-save").hide();
            enableEditProducts();
        }

        jQuery("#saveOrderModal").on("show.bs.modal", function () {
            jQuery(".dontSaveOrder").addClass("hidden");
            var giftPaymentSpan = jQuery("#amountFor_GIFT_CERTIFICATE");
            if (giftPaymentSpan.length > 0) {
                var otherPayment = jQuery("#paymentMethodID").text().trim();
                var otherValue = +jQuery("#amountFor_" + otherPayment).text().trim();
                var refundValue = +jQuery("#outstanding.refund").text();

                if (refundValue > otherValue) {
                    var newText = jQuery("#giftcert-refund").text().replace("{1}", otherValue.toFixed(2)).replace("{2}", (Math.abs(otherValue - refundValue)).toFixed(2));
                    jQuery("#giftcert-refund").text(newText);
                    jQuery("#giftcert-refund").show();
                } else {
                    jQuery("#giftcert-refund").hide();
                }
            } else {
                jQuery("#giftcert-refund").hide();
            }

            if (Object.keys(orderChanges).length > 0) {
                jQuery("#saveMsg").text(Resources.ORDER_SAVE_EDIT);
                prepareChangesList();
            } else {
                var text = Resources.ORDER_SAVE_CONFIRM.replace("_", jQuery("#orderNumber").text());
                jQuery("#saveMsg").text(text);
            }

            jQuery(".dontSaveOrder").off("click").on("click", function () {
                viewOrderDetails();
            });
        });
    });

    jQuery("#saveOrderModal").on("hide.bs.modal", function () {
        jQuery("body").removeClass("modal-open");
        jQuery(".modal-backdrop").remove();
    });
}
/**
 * Order Save - End
 */

function initializeGiftCertificateRestrictions() {
    if (jQuery(".giftCertificateLineItem").length > 0) {
        jQuery("#gift-unaltered").removeClass("hidden");
    }
}
/**
 * Order Cancelation - Begin
 */
function initializeCancelOrder() {
    var $withoutRefundOption = jQuery("input[name=\"withoutRefund\"]");
    var $cancelBtn = jQuery(".cancelOrder");
    var $description = jQuery("#cancelDescription");
    var $cancelModal = jQuery("#cancelOrderModal");

    $cancelBtn.one("click", function (e) {
        e.preventDefault();
        cancelModalValidation();

        var url = jQuery(this).attr("data-link");

        jQuery.ajax({
            type: "GET",
            url: url,
            data: {
                cancelWithRefund: !$withoutRefundOption.prop("checked"),
                orderCancelDescription: $description.val()
            }
        }).done(function (response) {
            if (!response) {
                setError(Resources.ORDER_CANCEL_ERROR);
            } else if (response.success && !response.errorMsg) {
                $cancelModal.modal("hide");
                viewOrderDetails();
            } else if (!response.succes && response.errorMsg) {
                setError(response.errorMsg);
            } else {
                setError(Resources.ORDER_CANCEL_ERROR);
            }
        });
    });

    $description.on("change keyup", function () {
        var $errorDesc = jQuery(".errorDesc");

        $errorDesc.hide();
        cancelModalValidation();
    });

    $withoutRefundOption.on("change", function () {
        if (jQuery(this).prop("checked")) {
            jQuery(".do-refund").hide();
            if ($description.val().length > 0) {
                $cancelBtn.removeAttr("disabled");
            }
        } else {
            jQuery(".do-refund").show();
        }
    });

    jQuery("#cancelOrderModal").on("hide.bs.modal", function () {
        $withoutRefundOption.prop("checked", false);
        jQuery("#cancelDescription").val("");
        $cancelBtn.attr("disabled", "disabled");
        jQuery("body").removeClass("modal-open");
        jQuery(".modal-backdrop").remove();
        jQuery(".do-refund").show();
    });
}
/**
 * Order Cancelation - End
 */

/**
 * Order Capture - Begin
 */
function initializeCaptureOrder() {
    jQuery(".captureBtn").on("click", function () {
        jQuery(".captureOrder").removeAttr("disabled");
    });

    jQuery(".captureOrder").one("click", function () {
        var url = jQuery(this).attr("data-link");

        jQuery.ajax({
            type: "POST",
            url: url
        }).done(function (response) {
            if (response && response.success) {
                jQuery("#captureOrderModal").modal("hide");
                viewOrderDetails(response.orderNo);
            } else if (response && !response.success && response.responseMsg) {
                jQuery("#captureOrderModal").modal("hide");
                viewOrderDetails(response.orderNo, null, null, null, response.responseMsg);
            } else {
                setError(Resources.UNEXPECTED_ERROR);
            }
            jQuery(".modal-backdrop").remove();
        }).fail(function () {
            setError(Resources.UNEXPECTED_ERROR);
            jQuery(".modal-backdrop").remove();
        });
    });
}
/**
 * Order Capture - End
 */

/**
 * Order Refund - Begin
 */
function initializeRefundOrder() {
    function initRefundOptions() {
        var paymentMethods = jQuery("[data-paymethod]");
        var hasGift = false;
        var hasCredit = false;
        var hasPaypal = false;
        paymentMethods.each(function () {
            var methodId = jQuery(this).attr("data-paymethod");
            if (methodId == "CREDIT_CARD" || methodId == "MercadoPago" || methodId == "AdyenComponent") {
                hasCredit = true;
            } else if (methodId == "PayPal") {
                hasPaypal = true;
            } else if (methodId == "GIFT_CERTIFICATE") {
                hasGift = true;
            }
        });
        if (hasCredit) {
            jQuery("#refundOption").find("option[value=\"paypal\"]").hide();
            jQuery("#refundOption").find("option[value=\"mixed-paypal\"]").hide();
            jQuery("#refundOption").val("credit");
        } else if (hasPaypal) {
            jQuery("#refundOption").find("option[value=\"credit\"]").hide();
            jQuery("#refundOption").find("option[value=\"mixed-credit\"]").hide();
            jQuery("#refundOption").val("paypal");
        } else if (hasGift) {
            jQuery("#refundOption").find("option[value=\"credit\"]").hide();
            jQuery("#refundOption").find("option[value=\"mixed-credit\"]").hide();
            jQuery("#refundOption").find("option[value=\"paypal\"]").hide();
            jQuery("#refundOption").find("option[value=\"mixed-paypal\"]").hide();
            jQuery("#refundOption").val("gift");
        }
    }
    var $refundForm = jQuery("#refundForm");
    jQuery(".refund-item__check").on("click", false);
    // hide invalid choices
    initRefundOptions();

    function openRefundModal() {
        initRefundOptions();
        jQuery("#refundOrderModal").one("shown.bs.modal", function () {
            jQuery("#refundOption").trigger("change");
        });

        var url = jQuery(".js-refund-btn").attr("data-link-edit");

        jQuery("#refundModalClose").one("click", function () {
            var cancelEditUrl = jQuery(".js-refund-btn").attr("data-link-stopEdit");
            jQuery.ajax({
                type: "GET",
                url: cancelEditUrl,
                data: {
                    agent: jQuery(".headermenu").text().trim()
                }
            }).done(function (response) {
                if (!response) {
                    setError(Resources.UNEXPECTED_ERROR);
                }
            });
        });

        if (jQuery("#couponInquiryResult").length) {
            setError(jQuery("#couponInquiryResult").text());
            return;
        }

        jQuery.ajax({
            type: "GET",
            url: url,
            data: {
                agent: jQuery(".headermenu").text().trim()
            }
        }).done(function (response) {
            if (!response) {
                setError(Resources.UNEXPECTED_ERROR);
            } else if (response.orderLocked) {
                setError(response.responseMsg);
            } else {
                jQuery("#refundOrderModal").modal("show");
            }
            jQuery(".js-refund-btn").one("click", openRefundModal);
        });
    }

    jQuery(".js-refund-btn").one("click", openRefundModal);


    // clear all values if close refund modal
    jQuery("#refundOrderModal").on("hidden.bs.modal", function () {
        var modalBody = jQuery(this).find(".modal-content .modal-body");
        modalBody.find(".refund-item").each((s, item) => {
            jQuery(item).find(".input-refund-quantity").val(0);
            jQuery(item).find("#refundTotal_" + jQuery(item).find(".product-id").text()).val(0.00);

        });
        modalBody.find("#refundProducts, #productTax, #shippingTax, #refundValue").each((t, item) => {
            jQuery(item).text("0.00");
        });

        jQuery(this).find(".modal-content .modal-footer .modal-footer_text").css("visibility", "hidden");
        jQuery(this).find(".modal-content .modal-footer .js-refundConfirm").prop("disabled", true);
    });

    jQuery("#refundOrderModal").on("click", function (e) {
        var isSaveButtonDisable = jQuery(".refundConfirm").prop("disabled");
        if (jQuery(e.target).hasClass("refund-item__check") || jQuery(e.target).hasClass("input-refund-quantity")) {
            var refundItem = jQuery(e.target).closest(".refund-item");
            var isCheckboxChecked = refundItem.find(".refund-item__check").prop("checked");
            var isQuantity = refundItem.find(".input-refund-quantity").val() > 0;

            if (isCheckboxChecked && isQuantity && !isSaveButtonDisable) {
                jQuery(".modal-footer_text").css("visibility", "visible");
            } else {
                jQuery(".modal-footer_text").css("visibility", "hidden");
            }
        }
        if (jQuery(e.target).is("#refundReason")) {
            jQuery(".refund-item").each((key, val) => {
                isCheckboxChecked = jQuery(val).find(".refund-item__check").prop("checked");
                isQuantity = jQuery(val).find(".input-refund-quantity").val() > 0;
                if (isCheckboxChecked && isQuantity && !isSaveButtonDisable) {
                    jQuery(".modal-footer_text").css("visibility", "visible");
                    return false;
                } else {
                    jQuery(".modal-footer_text").css("visibility", "hidden");
                }
            });
        }
    });

    // Calculate the refund
    $refundForm.find("input:not([type=\"checkbox\"])").each(function () {
        jQuery(this).on("change click input", function () {
            var maxValue = Math.floor(parseInt(jQuery(this).attr("max"), 10));
            var curVal = Math.floor(parseInt(jQuery(this).val(), 10));
            jQuery(this).val(curVal);

            jQuery(this).closest("tr").find(".refund-item__check").prop("checked", jQuery("#selectAll").prop("checked"));

            if (jQuery("#selectAll").prop("checked")) {
                jQuery(this).closest("tr").find("#return-status").removeClass("hidden");
            }

            if (!curVal) {
                //If quantity of line item product is 0, make checked false
                jQuery(this).closest("tr").find(".refund-item__check").prop("checked", false);
                jQuery(this).closest("tr").find("#return-status").addClass("hidden");
            }

            if (curVal > maxValue) {
                jQuery(this).val(maxValue);
                jQuery(this).closest("td").find(".fade-out-error").remove();
                jQuery(this).closest("td").append(`<span class="fade-out-error" style="color:red">${Resources.REFUND_QUANTITY_ERROR} ${maxValue}</span>`);
                setTimeout(() => {
                    jQuery(".fade-out-error").fadeOut("slow", function () {
                        jQuery(this).remove();
                    });
                }, 2000);
            }

            $refundForm.validate();

            if ($refundForm.valid()) {
                calculateRefund();
            }
        });
    });

    $refundForm.find(".refund-item input[type=\"checkbox\"]").each(function () {
        jQuery(this).on("change", function () {
            var qtyInput = jQuery("input[name=\"refundQtd_" + jQuery(this).attr("name").split("_")[1] + "\"]");
            var auxMax = qtyInput.attr("max");
            qtyInput.attr("max", qtyInput.attr("max-return"));
            qtyInput.attr("max-return", auxMax);

            $refundForm.validate();

            if ($refundForm.valid()) {
                calculateRefund();
            }
        });
    });

    jQuery(document).off("change", "#refundCreditValue, #refundGiftValue, #refundPayPalValue").on("change", "#refundCreditValue, #refundGiftValue, #refundPayPalValue", function () {
        var $this = jQuery(this);
        var selectedRefundOption = jQuery("#refundOption").val();
        var maxAmount = Number(jQuery("#originalTotal").text());
        var creditAmount = Number(jQuery("#refundCreditValue").val());
        var paypalAmount = Number(jQuery("#refundPayPalValue").val());
        var moneyAmount = creditAmount || paypalAmount;
        var giftAmount = Number(jQuery("#refundGiftValue").val());
        var totalAmount = moneyAmount + giftAmount;

        jQuery("#refundValue").html(totalAmount);

        if (totalAmount > maxAmount) {
            jQuery("#refundValue").html(maxAmount);

            if (selectedRefundOption === "credit" || selectedRefundOption === "gift") {
                $this.val(maxAmount);
            } else if (selectedRefundOption === "mixed-credit") {
                if (giftAmount > maxAmount) {
                    $this.val((maxAmount - moneyAmount).toFixed(2));
                } else if (moneyAmount > maxAmount) {
                    $this.val((maxAmount - giftAmount).toFixed(2));
                }
            }
        }

        refundModalValidation();
    });

    // Select All if quantity more than 1
    jQuery("#selectAll").on("change", function () {
        var chk = this.checked;
        jQuery("input[name^=\"return_\"]").each(function () {
            var toRefundedLineItemQuantity = jQuery(this).closest("tr").find(".input-refund-quantity").val();
            if (Number(toRefundedLineItemQuantity)) {
                this.checked = chk;
                if (chk) {
                    jQuery(this).closest("tr").find("#return-status").removeClass("hidden");
                } else {
                    jQuery(this).closest("tr").find("#return-status").addClass("hidden");
                }

            }
        });
    });

    // Show/Hide Gift Certificate Option
    jQuery("#refundOption").on("change", function () {
        var optionSelected = jQuery(this).find("option:selected").val();
        var $giftDiv = jQuery(".refundGiftDiv");
        var $creditDiv = jQuery(".refundCreditDiv");
        var $paypalDiv = jQuery(".refundPayPalDiv");
        var refund = 0;
        var half = 0;

        if (optionSelected == "gift") {
            $giftDiv.removeClass("hidden");
            $creditDiv.addClass("hidden");
            $paypalDiv.addClass("hidden");
            $giftDiv.find("input").val(jQuery("#refundValue").html());
            $creditDiv.find("input").val(0.00);
            $paypalDiv.find("input").val(0.00);
            $creditDiv.find("input").prop("checked", false);
            $giftDiv.find("input").prop("checked", true);
        } else if (optionSelected == "credit") {
            $giftDiv.addClass("hidden");
            $paypalDiv.addClass("hidden");
            $creditDiv.removeClass("hidden");
            $creditDiv.find("input").val(jQuery("#refundValue").html());
            $giftDiv.find("input").prop("checked", false);
            $creditDiv.find("input").prop("checked", true);
            $giftDiv.find("input").val(0.00);
            $paypalDiv.find("input").val(0.00);
        } else if (optionSelected == "paypal") {
            $giftDiv.addClass("hidden");
            $creditDiv.addClass("hidden");
            $paypalDiv.removeClass("hidden");
            $paypalDiv.find("input").val(jQuery("#refundValue").html());
            $giftDiv.find("input").prop("checked", false);
            $creditDiv.find("input").prop("checked", false);
            $giftDiv.find("input").val(0.00);
            $creditDiv.find("input").val(0.00);
        } else if (optionSelected == "mixed-paypal") {
            $giftDiv.removeClass("hidden");
            $paypalDiv.removeClass("hidden");
            $creditDiv.addClass("hidden");
            $giftDiv.find("input").prop("checked", true);
            $paypalDiv.find("input").prop("checked", true);
            refund = parseFloat(jQuery("#refundValue").html());
            half = (refund / 2).toFixed(2);
            $paypalDiv.find("input").val(half);
            $giftDiv.find("input").val((refund - half).toFixed(2));
        } else {
            $giftDiv.removeClass("hidden");
            $creditDiv.removeClass("hidden");
            $paypalDiv.addClass("hidden");
            $giftDiv.find("input").prop("checked", true);
            $creditDiv.find("input").prop("checked", true);
            refund = parseFloat(jQuery("#refundValue").html());
            half = (refund / 2).toFixed(2);
            $creditDiv.find("input").val(half);
            $giftDiv.find("input").val((refund - half).toFixed(2));
        }
        refundModalValidation();
    });

    // Enable/Disable Save Button
    jQuery("#refundCreditValue, #refundGiftValue, #refundPayPalValue, #refundReason").on("change", function () {
        refundModalValidation();
    });

    // Remove product from refund list
    jQuery(".refund-remove").on("click", function () {
        jQuery(this).parents(".refund-item").remove();
        calculateRefund();
    });

    jQuery("#refundShipping").on("change", function () {
        calculateRefund();
    });
    jQuery(".refundConfirm").on("click", function () {
        var orderNo = jQuery("#orderNumber").text();
        var credit = !jQuery(".refundCreditDiv").hasClass("hidden");
        var gift = !jQuery(".refundGiftDiv").hasClass("hidden");
        var paypal = !jQuery(".refundPayPalDiv").hasClass("hidden");
        var totalRefund = jQuery("#refundValue").html();
        var username = jQuery("#orderForm").find("input[name=\"customerName\"]").val();
        var refundOption = "";
        if (credit || paypal) {
            refundOption += "Payment";
        }
        if (gift) {
            if (credit || paypal) {
                refundOption += " and";
            }
            refundOption += " Store credit";
        }
        jQuery("#refundorderid").html(orderNo);
        jQuery("#refundoptions").html(refundOption);
        jQuery("#refundamountrefund").html(totalRefund);
        jQuery("#refundcustomername").html(username);
        jQuery("#confirmRefund").modal("show");

        // add message in confirm refund if all products are selected to refund
        var isFullRefund = false;
        var amoutToRefund = parseInt(jQuery("#refundValue").text(), 10);
        var totalAvailableRefund = parseInt(jQuery("#originalTotal").text(), 10);

        jQuery("#refundForm .refund-item").each((c, prod) => {
            var prodId = jQuery(prod).find(".product-id").text();
            var qty = parseInt(jQuery(prod).find("#baseQty_" + prodId).text(), 10);
            var selectedQuantity = parseInt(jQuery(prod).find(".input-refund-quantity").val(), 10);

            if (qty == selectedQuantity) {
                isFullRefund = true;
            } else {
                isFullRefund = false;
                return false;
            }
        });

        if (isFullRefund && (amoutToRefund != totalAvailableRefund)) {
            jQuery("#confirmRefund .totalRefundMsg").removeClass("hidden");
        } else {
            jQuery("#confirmRefund .totalRefundMsg").addClass("hidden");
        }
        return false;
    });

    // Save Refund
    jQuery(".refundSave").one("click", function () {
        jQuery("#confirmRefund").modal("hide");
        // block re-click
        var refundButton = jQuery(this);
        refundButton.attr("disabled", "disabled");

        var oldShip = parseFloat(jQuery("#originalShipping").val());
        var ship = jQuery("#refundShipping").prop("checked");
        var arr = jQuery("#refundForm").serializeArray();
        var prods = [];
        var $orderForm = jQuery("#orderForm");

        arr.each(function (e) {
            if (e.name.indexOf("refundQtd_") != -1) {
                var pid = e.name.substring(e.name.indexOf("_") + 1);
                var qtd = parseInt(e.value, 10);

                if (qtd > 0) {
                    prods.push({
                        pid: pid,
                        qtd: qtd,
                        ret: jQuery("input[name=\"return_" + pid + "\"]").prop("checked")
                    });
                }


                if (!jQuery("#selectAll").prop("checked")) {
                    if (qtd != 0) {
                        orderNotes.push({
                            text: qtd + " " + Resources.NOTES_PRODUCTS_WITH + " " + pid + " " + Resources.NOTES_IDREFUNDED
                        });
                    }
                } else {
                    orderNotes.push({
                        text: qtd + " " + Resources.NOTES_PRODUCTS_WITH + " " + pid + " " + Resources.NOTES_IDPENDING
                    });
                }
            }
        });

        if (!jQuery("#selectAll").prop("checked") && jQuery("#refundShipping").prop("checked")) {
            orderNotes.push({
                text: Resources.NOTES_REFUNDSHIPMENT
            });
        } else if (jQuery("#selectAll").prop("checked") && jQuery("#refundShipping").prop("checked")) {
            orderNotes.push({
                text: Resources.NOTES_REFUNDSHIPMENTPENDING
            });
        }

        jQuery.ajax({
            type: "POST",
            url: Urls.refund,
            data: {
                orderNo: jQuery("#orderNumber").text(),
                credit: (!jQuery(".refundCreditDiv").hasClass("hidden")) ? jQuery("#refundCreditValue").val() : 0,
                gift: (!jQuery(".refundGiftDiv").hasClass("hidden")) ? jQuery("#refundGiftValue").val() : 0,
                paypal: (!jQuery(".refundPayPalDiv").hasClass("hidden")) ? jQuery("#refundPayPalValue").val() : 0,
                products: JSON.stringify(prods),
                subtotal: jQuery("#refundProducts").text(),
                prodTax: jQuery("#productTax").text(),
                shipping: ship,
                originalShipping: oldShip,
                creditCardAvailable: jQuery("#refundTotalCredit").val(),
                shipTax: jQuery("#shippingTax").text(),
                reason: jQuery("#refundReason").find("option:selected").val(),
                order: prepareOrderObj($orderForm.serializeArray())
            }
        }).done(function (response) {
            if (response && response.success && response.jobSuccess) {
                jQuery("#refundOrderModal").modal("hide");
                if (response.jobExecution) {
                    viewOrderDetails(response.oldOrderNo, null, null, response.jobExecution);
                } else {
                    viewOrderDetails(response.orderNo);
                }
            } else if (response.message) {
                setError(response.message);
            } else {
                setError(Resources.UNEXPECTED_ERROR);
            }
        }).fail(function () {
            setError(Resources.UNEXPECTED_ERROR);
        }).always(function () {
            refundButton.removeAttr("disabled");
        });
    });

    jQuery("#refundOrderModal").on("hide.bs.modal", function () {
        jQuery("#refundForm").find("input").val(0);
        jQuery("#refundForm").find("select").val("credit");
        jQuery("#refundForm").find("input[type=\"checkbox\"]").prop("checked", false);
        jQuery("#refundOption").trigger("change");
        jQuery("[id^=\"refundValue\"]").text("0.00");
        jQuery("[id^=\"refundTotal_\"]").text("0.00");
        jQuery("#additionalFields").find("select").val("");
        jQuery("#additionalFields").find("input").val(0);
        jQuery(".refundOptions, .refundFields").addClass("hidden");
        jQuery("body").removeClass("modal-open");
        jQuery(".modal-backdrop").remove();
    });
}
/**
 * Order Refund - End
 */


/**
 * Manage Returns - Begin
 */
function initializeManageReturns() {
    var $allConfirm = jQuery("#selectAllConfirm");
    var $allCancel = jQuery("#selectAllCancel");

    function openReturnModal() {
        var url = jQuery(".js-return-btn").attr("data-link-edit");

        if (jQuery("#couponInquiryResult").length) {
            setError(jQuery("#couponInquiryResult").text());
            return;
        }

        jQuery.ajax({
            type: "GET",
            url: url,
            data: {
                agent: jQuery(".headermenu").text().trim()
            }
        }).done(function (response) {
            if (!response) {
                setError(Resources.UNEXPECTED_ERROR);
            } else if (response.orderLocked) {
                setError(response.responseMsg);
            } else {
                jQuery("#manageReturnModal").modal("show");
            }
            jQuery(".js-return-btn").one("click", openReturnModal);
        });
    }

    jQuery(".js-return-btn").one("click", openReturnModal);

    // Select All Confirm
    $allConfirm.on("change", function () {
        var chk = this.checked;
        if (chk) {
            jQuery(".modal-footer_text").css("visibility", "visible");
            $allCancel.attr("disabled", "disabled");
        } else {
            jQuery(".modal-footer_text").css("visibility", "hidden");
            $allCancel.removeAttr("disabled");
        }
        jQuery("input[name^=\"confirm_\"]").each(function () {
            this.checked = chk;
            jQuery(this).trigger("change");
        });
    });

    jQuery("input[name^=\"confirm_\"]").each(function () {
        var $this = jQuery(this);
        var tmp = $this.attr("name");
        var idx = tmp.slice(tmp.indexOf("_") + 1);

        $this.on("change", function () {
            var $cancel = jQuery("input[name=\"cancel_" + idx + "\"]");
            if ($this.prop("checked")) {
                $cancel.attr("disabled", "disabled");
            } else {
                $cancel.removeAttr("disabled");
            }
        });
    });

    // Select All Cancel
    $allCancel.on("change", function () {
        var chk = this.checked;
        if (chk) {
            jQuery(".modal-footer_text").css("visibility", "visible");
            $allConfirm.attr("disabled", "disabled");
        } else {
            jQuery(".modal-footer_text").css("visibility", "hidden");
            $allConfirm.removeAttr("disabled");
        }
        jQuery("input[name^=\"cancel_\"]").each(function () {
            this.checked = chk;
            jQuery(this).trigger("change");
        });

        // remove reason to cancel error text
        jQuery(".reason_controlwrapper").each(function () {
            jQuery(this).addClass("hidden");
        });
    });

    // Show/Hide Cancel Return Reason
    jQuery("input[name^=\"cancel_\"]").each(function () {
        var $this = jQuery(this);
        var tmp = $this.attr("name");
        var idx = tmp.slice(tmp.indexOf("_") + 1);

        $this.on("change", function () {
            var $reason = jQuery("select[name=\"reason_" + idx + "\"]");
            var $confirm = jQuery("input[name=\"confirm_" + idx + "\"]");
            if ($this.prop("checked")) {
                $reason.removeClass("hidden");
                $confirm.attr("disabled", "disabled");
            } else {
                $reason.addClass("hidden");
                $confirm.removeAttr("disabled");
            }
        });
    });

    jQuery(".returnConfirm").on("click", function () {
        var formElements = jQuery("#returnForm").serializeArray();
        var reasonControlError = false;
        var response = [];
        var confirmed = "";
        var cancelled = "";
        formElements.each(function (formElement) {
            var idx = parseInt(formElement.name.substring(formElement.name.indexOf("_") + 1), 10);
            var item = formElement.name.slice(0, formElement.name.indexOf("_"));
            if (!response[idx]) {
                response[idx] = {};
            }
            if (item == "confirm") {
                confirmed += "<li>" + response[idx].productname + "</li>";
            }
            if (item == "cancel") {
                cancelled += "<li>" + response[idx].productname + "</li>";
            }

            var cancelOrderChecked = jQuery("input[name=\"cancel_" + idx + "\"]").prop("checked");

            if (item == "reason" && !formElement.value && cancelOrderChecked) {
                jQuery(".reason_control_error_" + idx).removeClass("hidden");
                reasonControlError = true;
            } else {
                jQuery(".reason_control_error_" + idx).addClass("hidden");
            }
            response[idx][item] = formElement.value;
        });

        var productsArray = response;
        var totalProductsInModalNumber = response.length;
        var confirmedProductsNumber = 0;
        var cancelledProductsNumber = 0;
        productsArray.forEach(function (product) {
            if (product.confirm === "on") {
                confirmedProductsNumber += 1;
            }
            if (product.cancel === "on") {
                cancelledProductsNumber += 1;
            }
        });

        var message = "<strong>" +
            confirmedProductsNumber.toString() + " " +
            Resources.RETURN_OUTOF + " " +
            totalProductsInModalNumber + " " +
            Resources.RETURN_MARKED +
            "<br/>" +
            cancelledProductsNumber.toString() + " " +
            Resources.RETURN_OUTOF + " " +
            totalProductsInModalNumber + " " +
            Resources.RETURN_MARKED_CANCELED +
            "</strong>";

        jQuery("#returnDetails").html(message);
        jQuery("#confirmedItems").html(confirmed);
        jQuery("#cancelledItems").html(cancelled);
        jQuery("#confirmedItemsLabel").toggleClass("hidden", confirmed.length < 1);
        jQuery("#cancelledItemsLabel").toggleClass("hidden", cancelled.length < 1);

        if (!reasonControlError) {
            jQuery("#confirmReturn").modal("show");
        }

        return false;
    });

    // Save Changes
    jQuery(".returnBtn").one("click", function () {
        jQuery("#confirmReturn").modal("hide");
        var arr = jQuery("#returnForm").serializeArray();
        var $orderForm = jQuery("#orderForm");
        var orderFormInputs = jQuery("#returnForm").find("input[product-id]");
        var temp = {};
        orderFormInputs.toArray().forEach(function (inputElement) {
            var confirmedQuantity = 0;
            var canceledQuantity = 0;
            var productID = jQuery(inputElement).attr("product-id");
            jQuery("input[product-id=" + productID + "]").toArray().forEach(function (input) {
                if (input.checked) {
                    confirmedQuantity += 1;
                }
                if (input.disabled) {
                    canceledQuantity += 1;
                }
            });
            temp[productID] = {
                confirmedQuantity :confirmedQuantity,
                canceledQuantity :canceledQuantity
            };
        });
        Object.getOwnPropertyNames(temp).forEach(function (productID) {
            if (temp[productID].confirmedQuantity > 0) {
                orderNotes.push({
                    text: temp[productID].confirmedQuantity + " " + Resources.NOTES_PRODUCTS_WITH + " " + productID + " " + Resources.NOTES_CONFIRM_RETURN
                });
            }
            if (temp[productID].canceledQuantity > 0) {
                orderNotes.push({
                    text: temp[productID].canceledQuantity + " " + Resources.NOTES_PRODUCTS_WITH + " " + productID + " " + Resources.NOTES_CANCEL_RETURN
                });
            }
        });

        var rsp = [];

        arr.each(function (e) {
            var idx = parseInt(e.name.substring(e.name.indexOf("_") + 1), 10);
            var item = e.name.slice(0, e.name.indexOf("_"));

            if (!rsp[idx]) {
                rsp[idx] = {};
            }
            rsp[idx][item] = e.value;
        });

        jQuery.ajax({
            type: "POST",
            url: Urls.manageReturn,
            data: {
                orderNo: jQuery("#orderNumber").text(),
                order: prepareOrderObj($orderForm.serializeArray()),
                resp: JSON.stringify(rsp)
            }
        }).done(function (response) {
            if (response && response.success && response.jobSuccess) {
                jQuery("#manageReturnModal").modal("hide");
                if (response.jobExecution) {
                    viewOrderDetails(response.oldOrderNo, null, null, response.jobExecution);
                } else {
                    viewOrderDetails(response.orderNo);
                }
            } else if (response && response.success && response.message) {
                jQuery("#manageReturnModal").hide();
                jQuery(".modal-backdrop").remove();
                viewOrderDetails(jQuery("#orderNumber").text(), null, null, response.jobExecution, response.message);
            } else {
                setError(Resources.UNEXPECTED_ERROR);
            }
        }).fail(function () {
            setError(Resources.UNEXPECTED_ERROR);
        });
    });

    jQuery("#manageReturnModal").on("hide.bs.modal", function () {
        var url = jQuery(".js-return-btn").attr("data-link-stopEdit");
        jQuery.ajax({
            type: "GET",
            url: url,
            data: {
                agent: jQuery(".headermenu").text().trim()
            }
        }).done(function (response) {
            if (!response) {
                setError(Resources.UNEXPECTED_ERROR);
            }
        });
        jQuery("body").removeClass("modal-open");
        jQuery(".modal-backdrop").remove();
    });

    // block wrong input of action
    jQuery(document).off("click", "input.manage-return-action").on("click", "input.manage-return-action", function (el) {
        var reasonControlError = ".reason_control_error_" + el.target.name.split("_")[1];
        if (reasonControlError.length) {
            jQuery(reasonControlError).addClass("hidden");
        }

        if (el.target.name.split("_")[0] == "cancel") {
            if (el.target.checked) {
                jQuery("#selectAllConfirm").attr("disabled", "disabled");
            } else {
                jQuery("#selectAllConfirm").removeAttr("disabled");
            }
        } else if (el.target.checked) {
            jQuery("#selectAllCancel").attr("disabled", "disabled");
        } else {
            jQuery("#selectAllCancel").removeAttr("disabled");
        }
    });

    jQuery(document).off("click", "input[name^='confirm_'].manage-return-action").on("click", "input[name^='confirm_'].manage-return-action", function () {
        var totalRefund = 0;
        var showTotal = false;
        jQuery("#returnForm tbody tr:not(.select-actions)").each((i, el) => {
            if (jQuery(el).find("input[name^='confirm_'].manage-return-action").is(":checked")) {
                totalRefund += parseInt(jQuery(el).find(".currencyCode + span").text(), 10);
                showTotal = true;
            }
        });
        showTotal
            ? jQuery(".modal-footer_text").css("visibility", "visible")
            : jQuery(".modal-footer_text").css("visibility", "hidden");

        jQuery(".modal-footer_text span").html(totalRefund);
    });
}
/**
 * Manage Returns - End
 */


/**
 * Form Validation - Begin
 */
function initializeFormValidation() {
    var $orderForm = jQuery("#orderForm");
    var $saveBtn = jQuery(".saveBtn");

    jQuery(".close-warn").on("click", function () {
        jQuery(".errorEdit").hide();
    });

    /* eslint-disable consistent-return */
    $orderForm.on("change", "input, select", function () {
        jQuery(".errorEdit").hide();
        var $this = jQuery(this);
        if ($this.hasClass("quantity-product")) {
            var maxValue = $this.attr("max");
            if (maxValue && parseInt($this.val(), 10) > parseInt(maxValue, 10)) {
                $this.closest("tr").find(".limit-inventory").show();
                $this.val(maxValue);
                $this.closest("tr").find("[total-availability]").text(0);
                return false;
            } else {
                var totalAvailability = Number($this.closest("tr").find("[total-availability]").attr("total-availability"));
                var currentQuantity = Number($this.val());
                $this.closest("tr").find("[total-availability]").text(totalAvailability - currentQuantity);
                $this.closest("tr").find(".limit-inventory").hide();
            }
        }
        if ($this.hasClass("calc") && (jQuery("input[name=\"paidWithGift\"]").val() == "true" || jQuery("input[name=\"isSupportedPayment\"]").val() == "false")) {
            $saveBtn.attr("disabled", "disabled");
            jQuery(".errorEdit").show();
            if ($this.is("select")) {
                $this.find("option").filter(function () {
                    return (jQuery(this).text() == $this.prev().text());
                }).prop("selected", true);
            } else {
                $this.val($this.prev().text());
            }
        } else {
            // Change all the selects with the same name
            if ($this.is("select")) {
                var selectVal = $this.val();
                var selectName = $this.attr("name");

                jQuery("select[name=\"" + selectName + "\"]").each(function () {
                    jQuery(this).val(selectVal);
                });
            }

            // Store Changes
            if ($this.attr("name").indexOf("_") != -1) {
                var product = $this.closest("tr").find(".product-name").text();
                var type;
                var value;

                if (!orderChanges[product]) {
                    orderChanges[product] = {};
                }

                if ($this.attr("name").indexOf("VA_") != -1) {
                    var temp = $this.attr("name").substring(3);
                    var index = temp.indexOf("_");

                    type = temp.slice(0, index);
                    value = " " + $this.find("option:selected").html();
                } else {
                    type = $this.attr("name").slice(0, 3);
                    type = type == "Qtd" ? "Quantity" : type;
                    value = " " + $this.val();
                    if (type === "Quantity") {
                        var productID = jQuery(this).closest("tr").find(".product-id").text();
                        createTransactionOrderNotes(productID, "QUANTITY", $this.val());
                    }
                }

                orderChanges[product][type] = value;


            } else if ($this.attr("name") != "q") {
                var val = ($this.is("input")) ? $this.val() : $this.find("option:selected").html().trim();
                if ($this.attr("name") === "billingPhone") {
                    jQuery("input[name=billingPhone]").val(val);
                }
                orderChanges[$this.attr("name")] = val;
                orderNotes.push({
                    text: Resources.NOTES_ORDERS + " " + [$this.attr("name")] + " " + Resources.NOTES_CHANGEDTO + " " + val
                });
            }

            // Check if the form is valid
            var invalidProduct = false;
            $orderForm.validate();

            jQuery(".invalid").each(function () {
                if (!jQuery(this).hasClass("hidden")) {
                    invalidProduct = true;
                    return false;
                }
            });

            if ($orderForm.valid() && !invalidProduct) {
                $saveBtn.removeAttr("disabled");

                if ($this.hasClass("calc")) {
                    calculateBasket();
                }
            } else {
                $saveBtn.attr("disabled", "disabled");
            }
        }
    });
    /* eslint-enable consistent-return */
}
/**
 * Form Validation - End
 */

function initializeProductVariantDone(targetSelector, response) {
    var options = jQuery(targetSelector).find("option").toArray();
    for (var j = 0; j < options.length; j++) {
        var option = options[j];
        var available = response[option.value];

        if (available) {
            jQuery(option).removeAttr("disabled");
        } else {
            jQuery(option).attr("disabled", "disabled");
        }
    }
}

/**
 * Product Variant - Begin
 */
function initializeProductVariant() {
    jQuery(document).off("click", ".variant").on("click", ".variant", function () {
        var previousSelectedVariantIndex = this.selectedIndex;
        changeVariation(previousSelectedVariantIndex);
    });
}

function changeVariation(previousVariantIndex) {
    jQuery(document).off("change", ".variant").on("change", ".variant", function () {
        var tmp = jQuery(this).attr("name").substring(3);
        var idx = tmp.indexOf("_");
        var pid = tmp.slice(idx + 1);
        var arr = [];
        var _this = this;

        jQuery("select[name$=\"" + pid + "\"]").each(function () {
            var $product = jQuery(this);
            var temp = $product.attr("name").substring(3);
            var index = temp.indexOf("_");
            var type = temp.slice(0, index);
            var val = $product.val() || $product.find("option:selected").val();

            arr.push({
                type: type,
                value: val
            });
        });
        // refresh price and pid
        var $btn = jQuery(".saveBtn").attr("disabled", "disabled");
        jQuery.ajax({
            type: "POST",
            url: Urls.variant,
            data: {
                pid: pid,
                variants: JSON.stringify(arr),
                order: prepareOrderObj(jQuery("#orderForm").serializeArray())
            }
        }).done(function (response) {
            if (response && response.success) {
                var $err = jQuery("#Err_" + pid);
                if (!response.valid) {
                    $err.removeClass("hidden");
                } else if (!response.pid && response.message) {
                    setError(response.message, Resources.MODAL_TITLE_INFO);
                    _this.selectedIndex = previousVariantIndex;
                    $btn.removeAttr("disabled");
                } else {
                    jQuery(_this).closest("tr").find("span.product-id").text(response.pid);
                    jQuery(_this).closest("tr").find(".product-image").find("img").attr("src", response.image);
                    updateProduct(pid, response.pid, response.price.toFixed(2), response.ats);
                    $err.addClass("hidden");
                    $btn.removeAttr("disabled");
                    createTransactionOrderNotes(pid, "REPLACED", response.pid);
                    calculateBasket();
                    orderChanges["variant"] = true;
                }
            } else {
                setError(Resources.UNEXPECTED_ERROR);
            }
        }).fail(function () {
            setError(Resources.UNEXPECTED_ERROR);
        });

        // refresh stock of other variations
        var allVariantSelectors = jQuery(_this).closest("td").find(".variant").toArray();

        for (var i = 0; i < allVariantSelectors.length; i++) {
            var variantSelector = allVariantSelectors[i];
            if (variantSelector != _this) {
                var targetSelector = variantSelector;
                var VAName = _this.name.split("_")[1];
                jQuery.ajax({
                    type: "POST",
                    url: Urls.updateVariantStock,
                    data: {
                        fixedname: VAName,
                        fixedvalue: jQuery(_this).val(),
                        variantid: targetSelector.name.split("_")[1],
                        pid: pid
                    }
                }).done(initializeProductVariantDone.bind(null, targetSelector));
            }
        }
    });
}
/**
 * Product Variant - End
 */

function initializeProductRemoveAjax(that, e) {
    var pidRemove = that.attr("id").substring(4);
    jQuery.ajax({
        type: "POST",
        url: Urls.calculate,
        data: {
            pidRemove: pidRemove,
            order: prepareOrderObj(jQuery("#orderForm").serializeArray())
        }
    }).done(function (response) {
        if (response && response.success && !response.jobWaiting) {
            if (!orderChanges.removed) {
                orderChanges.removed = [];
            }

            var productRow = that.closest("tr");
            var productName = productRow.find(".product-name").text();
            var removedProductObject = {
                pid: pidRemove,
                qtd: productRow.find(".quantity-product").val(),
                name: productName
            };

            var stringedObj = JSON.stringify(removedProductObject);
            var hasBeenListed = orderChanges.removed.indexOf(stringedObj) != -1;
            if (!hasBeenListed) {
                orderChanges.removed.push(stringedObj);
            }

            // if a product is removed, there is no need to change previous changes
            if (typeof orderChanges[productName] !== "undefined") {
                delete orderChanges[productName];
            }

            var productQuantity = productRow.find(".quantity-product").val();
            createTransactionOrderNotes(pidRemove, "REMOVED", productQuantity);
            redoTotals(response.totals);
            jQuery(that.closest("tr")).remove();
            jQuery(".saveBtn").removeAttr("disabled");
            var $productTableLines = jQuery("#products-table").find("tr[not-bonus-product]");
            if ($productTableLines.length == 1) {
                // if we have only product on the order, ensure it is impossible to remove it
                var firstLine = $productTableLines[0];
                jQuery(firstLine).find("td.column-remove")[0].remove();
            }
        } else if (response.jobWaiting && response.success) {
            setTimeout(initializeProductRemoveAjax, 1000, that, e);
        } else {
            setError(Resources.UNEXPECTED_ERROR);
        }
    }).fail(function () {
        setError(Resources.UNEXPECTED_ERROR);
    });
}

/**
 * Product Removal - Begin
 */
function initializeProductRemove() {
    jQuery(document).off("click", ".removeBtn").on("click", ".removeBtn", function (e) {
        jQuery("#totals .loms-loader").removeClass("hidden");
        var that = jQuery(this);
        initializeProductRemoveAjax(that, e);
    });
}
/**
 * Product Removal - End
 */

/**
 * Back to Orders List - Begin
 */
function initializeBackToList() {
    jQuery("#mainMenu").hide("slide");
    jQuery(".orderview").show("slide", { direction: "right" });

    jQuery("#backToList").one("click", function (e) {
        if (Object.keys(orderChanges).length > 0) {
            jQuery("#saveOrderModal").on("show.bs.modal", function () {
                var text = Resources.ORDER_SAVE_CONFIRM.replace("_", jQuery("#orderNumber").text());
                jQuery("#saveMsg").text(text);
                jQuery("#changes").empty();
                jQuery(".dontSaveOrder").removeClass("hidden");
                jQuery(".dontSaveOrder").off("click").on("click", function () {
                    jQuery("#mainMenu").show("slide");
                    jQuery(".orderview").hide("slide", { direction: "right" });
                    jQuery("#saveOrderModal").modal("hide");
                });
            });
            jQuery("#saveOrderModal").modal("show");
        } else {
            if (jQuery(".cancelEditBtn").css("display") !== "none") {
                var url = jQuery(".cancelEditBtn").attr("data-link");
                e.preventDefault();
                jQuery.ajax({
                    type: "GET",
                    url: url
                }).done(function (response) {
                    if (!response) {
                        setError(Resources.UNEXPECTED_ERROR);
                    } else {
                        if (!response.orderLocked) {
                            jQuery("#mainMenu").show("slide");
                            jQuery(".orderview").hide("slide", { direction: "right" });
                        } else {
                            setError(Resources.ORDER_LOCKED);
                        }
                    }
                });
            } else {
                var changedOrderStatus = jQuery("#general select[name=\"orderStatus\"]").find("option:selected").text().trim();
                var changedPaymentStatus = jQuery("#paymentStatus").text().trim();
                var changedName = jQuery("#general input[name=\"customerName\"").val();
                var changedEmail = jQuery("#general input[name=\"customerEmail\"").val();
                jQuery("#orders-list .order-list-orderNo").each((n, item) => {
                    if (jQuery(item).html() == jQuery("#general #orderNumber").html()) {
                        jQuery(item).closest(".displayOrder").find(".order-list-status").html(changedOrderStatus);
                        jQuery(item).closest(".displayOrder").find(".order-list-payment-status").html(changedPaymentStatus);
                        jQuery(item).closest(".displayOrder").find(".order-list-name").html(changedName);
                        jQuery(item).closest(".displayOrder").find(".order-list-email").html(changedEmail);
                    }
                });

                jQuery("#mainMenu").show("slide");
                jQuery(".orderview").hide("slide", { direction: "right" });
            }
        }
    });
}
/**
 * Back to Orders List - End
 */

/**
 * Pagination - Begin
 */
function initilizePagination() {
    jQuery(document).on("click", ".fastOMS-module__pagination a", function (e) {
        e.preventDefault();
        var url = jQuery(this).attr("href");
        searchOrders(url);
    });

    jQuery(document).on("change", ".fastOMS-module__pagination .pageSize", function () {
        var url = jQuery(this).val();
        searchOrders(url);
    });
}
/**
 * Pagination - End
 */

/**
 * Save Notes - Begin
 */
function initializeNotes() {
    var $showActions = jQuery("#showNoteActions");
    var $actions = jQuery("#noteActions");
    var $noteBtn = jQuery(".saveNote");
    var $cancelBtn = jQuery(".cancelNote");
    var $original = jQuery("#originalOrderNotes");
    var $noteTxt = jQuery("#orderNotes");

    $showActions.on("click", function () {
        $actions.removeClass("hidden");
        $noteTxt.removeAttr("disabled");
        $showActions.hide();
    });

    $cancelBtn.on("click", function () {
        $actions.addClass("hidden");
        $showActions.show();
        $noteTxt.attr("disabled", "disabled");
        $noteTxt.val($original.val());
        jQuery("#notesCharCount")[0].innerText = $original.val().length;
    });

    $noteBtn.on("click", function () {
        jQuery.ajax({
            type: "POST",
            url: Urls.addNote,
            data: {
                orderNo: jQuery("#orderNumber").text(),
                notes: jQuery("#orderNotes").val()
            }
        }).done(function (response) {
            if (response && response.success) {
                $actions.addClass("hidden");
                $showActions.show();
                $noteTxt.attr("disabled", "disabled");
                $original.val(jQuery("#orderNotes").val());
            } else {
                setError(Resources.UNEXPECTED_ERROR);
            }
        }).fail(function () {
            setError(Resources.UNEXPECTED_ERROR);
        });
    });

    $noteTxt.on("keyup", function () {
        jQuery("#notesCharCount")[0].innerText = jQuery("#orderNotes").val().length;
    });
}
/**
 * Save Notes - End
 */


/**
 * Addresses List - Begin
 */
function initializeAddressesList() {
    if (jQuery("#addressList").length) {
        jQuery("#addressList").off("change").on("change", function () {
            var selected = jQuery(this).children(":selected").first();
            var address = jQuery(selected).data("address");

            if (!address) {
                return;
            }
            /* eslint-disable no-restricted-syntax, no-continue */
            for (var field in address) {
                if (field === "ID") {
                    continue;
                }
                jQuery("#orderForm").find("[name$=\"" + field + "\"]").val(address[field]);
            }
            /* eslint-enable no-restricted-syntax, no-continue */
        });
    }
}
/**
 * Addresses List - End
 */

/*
 * Helper functions
 */
function disableEditProducts() {
    jQuery("#product").find("input").attr("disabled", "disabled");
    jQuery("td").find("div.removeCTA").removeClass("removeBtn");
}

function enableEditProducts() {
    jQuery("#product").find("input").removeAttr("disabled");
    jQuery("td").find("div.removeCTA").addClass("removeBtn");
}
// prepare Order Object
function prepareOrderObj(arr) {
    var obj = {
        orderNo: jQuery("#orderNumber").text(),
        orderTotal: jQuery("#orderTotal").text(),
        discountValue: (jQuery("#discountValue").length) ? jQuery("#discountValue").val() : 0,
        customerName: getParameter(arr, "customerName"),
        customerEmail: getParameter(arr, "customerEmail"),
        status: getParameter(arr, "orderStatus"),
        shippingStatus: getParameter(arr, "shippingStatus"),
        paymentStatus: jQuery("#paymentStatus").find("span").attr("status-code"),
        confirmationStatus: getParameter(arr, "confirmationStatus"),
        exportStatus: getParameter(arr, "exportStatus"),
        products: getProducts(),
        shippingMethod: getParameter(arr, "shippingMethod"),
        shipping: {
            firstName: getParameter(arr, "shippingFirstName"),
            lastName: getParameter(arr, "shippingLastName"),
            address1: getParameter(arr, "shippingAddress1"),
            address2: getParameter(arr, "shippingAddress2"),
            city: getParameter(arr, "shippingCity"),
            postalCode: getParameter(arr, "shippingPostalCode"),
            countryCode: jQuery("input[name=shippingCountryCode]").val(),
            stateCode: getParameter(arr, "shippingStateCode"),
            phone: getParameter(arr, "shippingPhone")
        },
        billing: {
            firstName: getParameter(arr, "billingFirstName"),
            lastName: getParameter(arr, "billingLastName"),
            address1: getParameter(arr, "billingAddress1"),
            address2: getParameter(arr, "billingAddress2"),
            city: getParameter(arr, "billingCity"),
            postalCode: getParameter(arr, "billingPostalCode"),
            countryCode: getParameter(arr, "billingCountryCode"),
            stateCode: getParameter(arr, "billingStateCode"),
            phone: getParameter(arr, "billingPhone")
        },
        changes: orderChanges,
        orderNotes: {
            notes: orderNotes,
            agent: jQuery(".headermenu").text().trim()
        }
    };

    return JSON.stringify(obj);
}

// Calculate Basket
function calculateBasket() {
    jQuery("#totals .loms-loader").removeClass("hidden");
    jQuery(".orderview .loms-loader").removeClass("hidden");
    jQuery.ajax({
        type: "POST",
        url: Urls.calculate,
        data: {
            order: prepareOrderObj(jQuery("#orderForm").serializeArray())
        }
    }).done(function (response) {
        if (response && response.success && !response.jobWaiting) {
            redoTotals(response.totals);
            jQuery(".saveBtn").removeAttr("disabled");
        } else if (response.jobWaiting && response.success) {
            setTimeout(calculateBasket, 1000);
        } else if (!response.success && response.responseMsg) {
            setError(response.responseMsg);
            jQuery("#saveBtn").attr("disabled", true);
            jQuery("#totals .loms-loader").addClass("hidden");
            jQuery(".orderview .loms-loader").addClass("hidden");
        } else {
            setError(Resources.UNEXPECTED_ERROR);
        }
    }).fail(function () {
        setError(Resources.UNEXPECTED_ERROR);
    });
}

//Clear orders-list changing search options
function initializeClearingOrdersList() {
    jQuery(document).on("mousedown", "#simple-search-tab, #advanced-search-tab", function (e) {
        if (!jQuery(e.target).hasClass("active") && e.which == 1) {
            jQuery("#orders-list").empty();
        }
    });
}

// Prepare Order for Edition
function prepareOrderEdition(resp, tabQuery, backToEdit) {
    jQuery(".orderview .result").empty().html(resp);
    jQuery(".orderview .result").removeClass("hidden");
    jQuery(".orderview .loms-loader").addClass("hidden");
    jQuery(".orderslist .loms-loader").addClass("hidden");
    initializeEditOrder();
    initializeBackToList();
    initializeFormValidation();
    initializeProductVariant();
    initializeProductRemove();
    initializeSaveOrder();
    initializeCancelOrder();
    initializeCaptureOrder();
    initializeRefundOrder();
    initializeManageReturns();
    initializeNotes();
    initializeGiftCertificateRestrictions();
    initializeAddressesList();
    goToReplacedOrOriginalOrder();
    orderChanges = {};
    orderNotes= [];
    var agent = jQuery(".headermenu").text().trim();
    orderNotes.push({agent: agent});

    if (tabQuery) {
        jQuery(tabQuery).trigger("click");
    }

    if (backToEdit) {
        jQuery(".editBtn").trigger("click");
    }

    var couponInitialError = jQuery("#couponInBM");
    if (couponInitialError.length) {
        var msgText = couponInitialError.text();
        setError(msgText);
    }
}

// Prepare Order's Changes List
function prepareChangesList() {
    // Prepare the table header
    var html = "<table class=\"table table__changeslist\">"
        + "<thead>"
        + "<tr class=\"table-active\">"
        + "<th class=\"first-column\">" + Resources.ORDER_SAVE_HEADERITEM + "</th>"
        + "<th>" + Resources.ORDER_SAVE_HEADERDETAIL + "</th>"
        + "</tr>"
        + "</thead>";

    jQuery.each(orderChanges, function (key, value) {
        if (key == "variant") {
            return;
        }
        var objectParse = false;
        var k = key;
        var val = value;
        // Replace some keys for a more user friendly value
        switch (k) {
            case "added":
                k = Resources.ORDER_SAVE_ADDED;
                break;
            case "removed":
                objectParse = true;
                k = Resources.ORDER_SAVE_REMOVED;
                break;
            default:
                break;
        }

        if (objectParse) {
            var allRemovedNames = [];
            for (var i = 0; i < val.length; i++) {
                var removedProductObject = val[i];
                allRemovedNames.push(JSON.parse(removedProductObject).name);
            }
            val = allRemovedNames.join(", ");
        } else {
            val = JSON.stringify(val);
            val = val.replace(/{|}|"|\[|\]/gi, "").split(",").join(", ");
        }

        var className = (k == "customerEmail") ? "" : "text-capitalized";
        html += "<tr>";
        html += "<td>" + k + "</td>";
        html += "<td class=\"" + className + "\">" + val + "</td>";
        html += "</tr>";
    });
    html += "</table>";

    jQuery("#changes").empty().html(html);
}

// Get Parameter
function getParameter(arr, name) {
    var idx = arr.map(function (e) {
        return e.name;
    }).indexOf(name);

    if (idx != -1) {
        return arr[idx].value;
    }

    return "";
}

// Simple and Advanced Search Order
function searchOrders(url) {
    var $searchForm = jQuery("#pills-tabContent .tab-pane.active .searchForm");
    jQuery(".orderslist .results").addClass("hidden");
    jQuery(".orderslist .loms-loader").removeClass("hidden");
    jQuery(".orderview .result").empty();
    jQuery.ajax({
        type: "GET",
        url: url ? url : Urls.orderSearch,
        data: $searchForm.serializeArray()
    }).done(function (response) {
        if (!response) {
            setError(Resources.UNEXPECTED_ERROR);
        } else {
            jQuery(".orderslist .results").empty().html(response);
            jQuery(".orderslist .results").removeClass("hidden");
            jQuery(".orderslist .loms-loader").addClass("hidden");
            initializeViewOrder();
        }
    });
}

// Order Details
function viewOrderDetails(orderNo, tabQuery, backToEdit, jobExecution, initialMessage) {
    jQuery("#saveOrderModal").modal("hide");
    jQuery(".orderview .loms-loader").removeClass("hidden");

    jQuery.ajax({
        type: "GET",
        url: Urls.orderDetails,
        data: {
            orderNo: (orderNo || jQuery("#orderNumber").text()),
            jobExecution: jobExecution
        }
    }).done(function (response) {
        if (!response) {
            setError(Resources.UNEXPECTED_ERROR);
        } else if (response.jobExecution) {
            setTimeout(viewOrderDetails, 1000, orderNo, null, null, response.jobExecution);
        } else if (response.message) {
            setError(response.message);
        } else {
            prepareOrderEdition(response, tabQuery, backToEdit);
            if (jQuery("#errorMsg").length > 0) {
                setError(jQuery("#errorMsg").html());
            }
            if (initialMessage) {
                setError(initialMessage);
            }
        }
    });
}


function handlePromotions(totals) {
    var promotions = {
        productPromotions: [],
        shippingPromotions: [],
        orderPromotions: [],
        productPromotionsTotal: 0.00
    };
    totals.promotions.productPromotions.forEach(prod => {
        promotions.productPromotions.push(prod);
    });

    totals.promotions.shippingPromotions.forEach(val => {
        if (val.usedPromotions.length > 0) {
            promotions.shippingPromotions = val.usedPromotions;
        }

    });
    totals.promotions.orderPromotions.forEach(val => {
        if (val.usedPromotions.length > 0) {
            promotions.orderPromotions = val.usedPromotions;
        }
    });

    promotions.productPromotionsTotal = totals.promotions.productPromotionsTotal || 0;



    jQuery("#orderAndShippingPromotions, .productPromotions-total, .productPromotions-promotionId").each((k, val) => {
        jQuery(val).html("").hide();
    });

    promotions.productPromotions.forEach((prom, index) => {
        jQuery("#products-table tr").each((j, row) => {
            if (prom.productID == jQuery(row).find(".product-id").html() && j === index) {
                prom.usedPromotions.forEach(userProm => {
                    jQuery(row)
                        .find(".productPromotions-IDs")
                        .append("<div class=\"productPromotions-promotionId\">" + userProm.promotionName + "</div>").show();
                    jQuery(row)
                        .find("#productPromotions-net")
                        .append("<div><span class=\"productPromotions-number\">" + userProm.netDiscount.toFixed(2) + "</span> <span> " + jQuery(row).data("currency") + "</span></div>").show();
                    jQuery(row)
                        .find("#productPromotions-gross")
                        .append("<div><span class=\"productPromotions-number\">" + userProm.grossDiscount.toFixed(2) + "</span> <span> " + jQuery(row).data("currency") + "</span></div>").show();
                });
            }
        });
    });

    if (promotions.orderPromotions.length > 0 || promotions.shippingPromotions || Math.abs(promotions.productPromotionsTotal) > 0) {
        jQuery("#totals #orderAndShippingPromotions").show();

        if (Math.abs(promotions.productPromotionsTotal) > 0) {
            jQuery("#orderAndShippingPromotions").append("<div class=\"d-flex justify-content-between\"> <div>" + "Product Level Discount" + "</div> <div> <span class=\"totals-currency\">" + jQuery("#totals").data("currency") + "</span> <span class=\"totals-number\">" + promotions.productPromotionsTotal.toFixed(2) + "</div> </div>");
        }

        if (promotions.orderPromotions.length > 0) {
            promotions.orderPromotions.forEach(ordProm => {
                jQuery("#orderAndShippingPromotions").append("<div class=\"d-flex justify-content-between js-order-promotion\"> <div>" + ordProm.promotionName + "</div> <div> <span class=\"totals-currency\">" + jQuery("#totals").data("currency") + "</span> <span class=\"totals-number\">" + ordProm.netDiscount.toFixed(2) + "</div> </div>");
            });
        }
        if (promotions.shippingPromotions.length > 0) {
            promotions.shippingPromotions.forEach(shipProm => {
                jQuery("#orderAndShippingPromotions").append("<div class=\"d-flex justify-content-between js-shipping-promotion\"> <div>" + shipProm.promotionName + "</div> <div> <span class=\"totals-currency\">" + jQuery("#totals").data("currency") + "</span> <span class=\"totals-number\">" + shipProm.netDiscount.toFixed(2) + "</div> </div>");
            });
        }

    } else {
        jQuery("#totals #orderAndShippingPromotions").hide();
    }

}

// Products Prices and Totals
function redoTotals(totals) {
    handlePromotions(totals);


    jQuery("#orderProducts").text(totals.products.toFixed(2));
    jQuery(".orderShipping").text(totals.shipping.toFixed(2));
    if (totals.discount > 0) {
        jQuery("#orderDiscount").show().text(totals.discount.toFixed(2));
    } else {
        jQuery("#orderDiscount").hide();
    }
    jQuery("#orderTax").text(totals.tax.toFixed(2));
    jQuery("#orderTotal").text(totals.total.toFixed(2));

    if (totals.shippingMethod) {
        var newShippingMethod = totals.shippingMethod;
        jQuery("#shippingName").text(Resources.SHIPPING_LABEL.replace("{0}", newShippingMethod));
    }

    var paid = parseFloat(jQuery("#orderPaid").text());
    var out = parseFloat(totals.total) - paid;

    if (out < 0) {
        jQuery(".newTotal .bold").text(Resources.REFUND_AMOUNT);
        out = Math.abs(out);
        jQuery("#outstanding").addClass("refund");
        jQuery("#outstanding").removeClass("exceed");
    } else {
        jQuery(".newTotal .bold").text(Resources.OUTSTANDING_AMOUNT);
        jQuery("#outstanding").removeClass("refund");
        jQuery("#outstanding").addClass("exceed");
    }

    jQuery("#outstanding").text(out.toFixed(2));

    jQuery("td[id^=\"Base_\"").each(function (index) {
        var productID = jQuery(this).attr("id").substring(5);
        var unitPrice = jQuery(this);
        var totalNetPrice = jQuery(this).closest("tr").find("#Total_" + productID);
        var totalGrossPrice = jQuery(this).closest("tr").find("#TotalGross_" + productID);
        var shippingProductLineItemPriceNet = jQuery(this).closest("tr").find("#ShippingProductLineItemsPriceNet_"+ productID);
        var shippingProductLineItemsPriceTax = jQuery(this).closest("tr").find("#ShippingProductLineItemsPriceTax"+ productID);


        totals.lineItemsPriceInfo.forEach(function (lineItemPriceInfo, lineItemPriceInfoIndex) {
            if (lineItemPriceInfo.productID == productID && lineItemPriceInfoIndex === index) {
                unitPrice.text(lineItemPriceInfo.unitProductPrice.toFixed(2));
                totalNetPrice.text(lineItemPriceInfo.productTotalNetPrice.toFixed(2));
                totalGrossPrice.text(lineItemPriceInfo.productTotalGrossPrice.toFixed(2));
                shippingProductLineItemPriceNet.text(lineItemPriceInfo.shippingProductLineItemsPriceNet.toFixed(2));
                shippingProductLineItemsPriceTax.text(lineItemPriceInfo.shippingProductLineItemsPriceTax.toFixed(2));
            }
        });
    });

    if (totals.promotionMsg && totals.promotionMsg.length > 0) {
        var html = "";
        for (var i = 0; i < totals.promotionMsg.length; i++) {
            html += "<tr><td>" + totals.promotionMsg[i] + "</td></tr>";
        }
        jQuery("#promotion-changes").empty().html(html);
        jQuery("#promotionModal").modal("show");
    }

    jQuery("#totals .loms-loader").addClass("hidden");
    jQuery(".orderview .loms-loader").addClass("hidden");
}

// Get List of Products and Quantities
function getProducts() {
    var basketProducts = [];
    var productsLines = jQuery("#products-table").find("tr[not-bonus-product]");
    productsLines.toArray().forEach(function (productLine) {
        var productVariants = [];
        var productID = jQuery(productLine).find(".product-id").text();
        var productQuantity = jQuery(productLine).find(".quantity-product").val();
        //Product Variants
        var productLineVariants = jQuery(productLine).find(".variant");
        productLineVariants.toArray().forEach(function (variant) {
            var type = variant.name.split("_")[1];
            var value = variant.value;
            productVariants.push({
                type: type,
                value: value
            });
        });
        basketProducts.push({
            pid: productID,
            qtd: productQuantity,
            variants: productVariants,
        });
    });
    return basketProducts;
}

// Calculate Refund
function calculateRefund() {
    jQuery("#refundTotals .loms-loader").removeClass("hidden");
    var newShipTax = parseFloat(jQuery("#originalShippingTax").val());
    var productsTotalAmount = 0.00;
    var produstsTaxesTotalAmount = 0.00;
    var maxRefundableAmount = parseFloat(jQuery("#originalTotal").text());
    var selectedRefundOption = jQuery("#refundOption").val();

    jQuery("td[id^=\"refundBase_\"").each(function () {
        var unitPrice = parseFloat(jQuery(this).find(".baseValue").text());
        var productID = jQuery(this).attr("id").substring(11);
        var qtd = jQuery("input[name=\"refundQtd_" + productID + "\"]").val();
        var unitTax = +jQuery("#refundBaseTax_" + productID)[0].innerText.trim();
        var productTotal = unitPrice * qtd;
        var productTaxTotal = unitTax * qtd;

        jQuery("#refundTotal_" + productID).text((+productTotal + +productTaxTotal).toFixed(2));
        productsTotalAmount += productTotal;
        produstsTaxesTotalAmount += productTaxTotal;
    });

    var refundAmount = productsTotalAmount + produstsTaxesTotalAmount;
    if (jQuery("#refundShipping").prop("checked")) {
        refundAmount = refundAmount + newShipTax + parseFloat(jQuery("#originalShipping").val());
    }

    if (refundAmount > maxRefundableAmount) {
        refundAmount = maxRefundableAmount;
        jQuery(".totalRefundDiff").removeClass("hidden");
    } else {
        jQuery(".totalRefundDiff").addClass("hidden");
    }

    var giftValue = 0;
    var cartValue = 0;

    jQuery(".refundOptions, .refundFields").toggleClass("hidden", refundAmount == 0);
    if (selectedRefundOption === "credit") {
        jQuery("#refundCreditValue").val(refundAmount.toFixed(2));
        jQuery("#refundPayPalValue").val(0);
        jQuery("#refundGiftValue").val(0);
    } else if (selectedRefundOption === "paypal") {
        jQuery("#refundCreditValue").val(0);
        jQuery("#refundPayPalValue").val(refundAmount.toFixed(2));
        jQuery("#refundGiftValue").val(0);
    } else if (selectedRefundOption === "gift") {
        jQuery("#refundCreditValue").val(0);
        jQuery("#refundPayPalValue").val(0);
        jQuery("#refundGiftValue").val(refundAmount.toFixed(2));
    } else if (selectedRefundOption === "mixed-credit") {
        jQuery("#refundPayPalValue").val(0);
        giftValue = (refundAmount / 2).toFixed(2);
        cartValue = (refundAmount - giftValue).toFixed(2);
        jQuery("#refundCreditValue").val(cartValue);
        jQuery("#refundGiftValue").val(giftValue);
    } else {
        jQuery("#refundCreditValue").val(0);
        giftValue = (refundAmount / 2).toFixed(2);
        cartValue = (refundAmount - giftValue).toFixed(2);
        jQuery("#refundPayPalValue").val(cartValue);
        jQuery("#refundGiftValue").val(giftValue);
    }

    jQuery("#refundValue").text(refundAmount.toFixed(2));

    if (jQuery("#orderAndShippingPromotions .js-order-promotion").length > 0 || jQuery("#orderAndShippingPromotions .js-shipping-promotion").length > 0) {
        jQuery(".refundFields .errorPromo").removeClass("hidden");
    } else {
        jQuery(".refundFields .errorPromo").addClass("hidden");
    }
    jQuery("#refundProducts").text(productsTotalAmount.toFixed(2));
    jQuery("#productTax").text(produstsTaxesTotalAmount.toFixed(2));
    jQuery("#shippingTax").text(newShipTax.toFixed(2));
    jQuery("#refundTotals .loms-loader").addClass("hidden");
    refundModalValidation();
}

// Cancel Modal Validation
function cancelModalValidation() {
    var $errorDesc = jQuery(".errorDesc");
    var $errorAct = jQuery(".errorAct");
    var $cancelBtn = jQuery(".cancelOrder");
    var $cancelRefundOption = jQuery("#cancelRefundOption").find("option:selected");
    var $withoutRefund = jQuery("input[name=\"withoutRefund\"]");
    var $description = jQuery("#cancelDescription");
    var refundCreditValue = parseFloat(jQuery("#cancelRefundCreditValue").val());
    var refundGiftValue = parseFloat(jQuery("#cancelRefundGiftValue").val());
    var creditAmount = parseFloat(jQuery("input[name=\"creditAmount\"]").val());
    var totalAmount = parseFloat(jQuery("input[name=\"totalAmount\"]").val());
    var giftAmount = parseFloat(jQuery("input[name=\"giftAmount\"]").val());

    $cancelBtn.removeAttr("disabled");

    if ($description.val().length > 0) {
        if (!$withoutRefund.prop("checked") && $cancelRefundOption.val() == "default") {
            $errorAct.show();
            $cancelBtn.attr("disabled", "disabled");
        } else if (!$withoutRefund.prop("checked") && $cancelRefundOption.val() == "gift" && (refundGiftValue > totalAmount || refundGiftValue < giftAmount || refundCreditValue > creditAmount)) {
            $cancelBtn.attr("disabled", "disabled");
        }
    } else {
        $cancelBtn.attr("disabled", "disabled");
        $errorDesc.show();
    }
}

// Refund Modal Validation
function refundModalValidation() {
    var creditChk = !jQuery(".refundCreditDiv").hasClass("hidden");
    var creditVal = parseFloat(jQuery("#refundCreditValue").val());
    var totalValue = parseFloat(jQuery("#originalTotal").html());
    var paypalChk = !jQuery(".refundPayPalDiv").hasClass("hidden");
    var paypalVal = parseFloat(jQuery("#refundPayPalValue").val());
    var giftChk = !jQuery(".refundGiftDiv").hasClass("hidden");
    var giftVal = parseFloat(jQuery("#refundGiftValue").val());
    var refundVal = parseFloat(jQuery("#refundValue").text());
    var totalCredit = (creditVal + giftVal).toFixed(2);
    var totalPaypal = (paypalVal + giftVal).toFixed(2);
    var $refBtn = jQuery(".refundConfirm");

    if (((creditChk && !giftChk && !paypalChk && creditVal > 0 && (creditVal == refundVal || creditVal == totalValue)) ||
        (!creditChk && giftChk && !paypalChk && giftVal > 0 && (giftVal == refundVal || giftVal == totalValue)) ||
        (!creditChk && !giftChk && paypalChk && paypalVal > 0 && (paypalVal == refundVal || paypalVal == totalValue)) ||
        (creditChk && giftChk && !paypalChk && creditVal > 0 && giftVal > 0 && (totalCredit == refundVal || totalCredit == totalValue)) ||
        (!creditChk && giftChk && paypalChk && paypalVal > 0 && giftVal > 0 && (totalPaypal == refundVal || totalPaypal == totalValue))) &&
        jQuery("#refundReason").val() != "") {
        $refBtn.removeAttr("disabled");
    } else {
        $refBtn.attr("disabled", "disabled");
    }

    // Validation messages
    if (refundVal > 0) {
        var $errorType = jQuery(".errorType");
        var $errorVal = jQuery(".errorVal");
        var $errorDiff = jQuery(".errorDiff");

        if (!creditChk && !giftChk && !paypalChk) {
            $errorType.removeClass("hidden");
        } else {
            $errorType.addClass("hidden");
        }

        if ((creditChk && creditVal == 0) || (giftChk && giftVal == 0) || (paypalChk && paypalVal == 0)) {
            $errorVal.removeClass("hidden");
        } else {
            $errorVal.addClass("hidden");
        }

        if ((creditChk && !giftChk && !paypalChk && creditVal > 0 && (creditVal != refundVal && creditVal != totalValue)) ||
            (!creditChk && giftChk && !paypalChk && giftVal > 0 && (giftVal != refundVal && giftVal != totalValue)) ||
            (!creditChk && !giftChk && paypalChk && paypalVal > 0 && (paypalVal != refundVal && paypalVal != totalValue)) ||
            (creditChk && giftChk && !paypalChk && creditVal > 0 && giftVal > 0 && (totalCredit != refundVal && totalCredit != totalValue)) ||
            (!creditChk && giftChk && paypalChk && paypalVal > 0 && giftVal > 0 && (totalPaypal != refundVal && totalPaypal != totalValue))) {
            $errorDiff.removeClass("hidden");
        } else {
            $errorDiff.addClass("hidden");
        }
    }
}

function updateProduct(oldProductId, newProductId, price, ats) {
    var selectElements = jQuery("select[name$=" + oldProductId + "]");
    selectElements.each(function (index) {
        var element = selectElements[index];
        var selectElementArr = element.name.split("_");
        selectElementArr[2] = newProductId;
        element.name = selectElementArr.join("_");

    });

    var errorArr = jQuery("#Err_" + oldProductId).attr("id").split("_");
    if (errorArr) {
        errorArr[1] = newProductId;
        jQuery("#Err_" + oldProductId).attr("id", errorArr.join("_"));
    }

    var baseArr = jQuery("#Base_" + oldProductId).attr("id").split("_");
    if (baseArr) {
        baseArr[1] = newProductId;
        jQuery("#Base_" + oldProductId).attr("id", baseArr.join("_"));
        jQuery("#Base_" + oldProductId).text(price);
    }

    var quantity = Number(jQuery("input[name=Qtd_" + oldProductId + "]").val());
    var availabilityArr = jQuery("#Availability_" + oldProductId).attr("id").split("_");
    if (availabilityArr) {
        availabilityArr[1] = newProductId;
        jQuery("#Availability_" + oldProductId).attr({"id": availabilityArr.join("_"), "total-availability": ats}).text(ats - quantity);

    }

    var quantityArr = jQuery("input[name=Qtd_" + oldProductId + "]").attr("name").split("_");
    if (quantityArr) {
        quantityArr[1] = newProductId;
        jQuery("input[name=Qtd_" + oldProductId + "]").attr({"name": quantityArr.join("_"), "max": ats});
    }

    var totalArr = jQuery("#Total_" + oldProductId).attr("id").split("_");
    if (totalArr) {
        totalArr[1] = newProductId;
        jQuery("#Total_" + oldProductId).attr("id", totalArr.join("_"));
    }

    var totalGrossArr = jQuery("#TotalGross_" + oldProductId).attr("id").split("_");
    if (totalGrossArr) {
        totalGrossArr[1] = newProductId;
        jQuery("#TotalGross_" + oldProductId).attr("id", totalGrossArr.join("_"));
    }

    var totalProductQuantity = jQuery("#products-table").find("tr").length;
    var delArr = jQuery("#Del_" + oldProductId).attr("id");
    if (delArr && totalProductQuantity != 1) {
        delArr = delArr.split("_");
        delArr[1] = newProductId;
        jQuery("#Del_" + oldProductId).attr("id", delArr.join("_"));
    }

    var returnArr = jQuery("input[name=return_" + oldProductId + "]").attr("name");
    if (returnArr) {
        returnArr = returnArr.split("_");
        returnArr[1] = newProductId;
        jQuery("input[name=return_" + oldProductId + "]").attr("name", returnArr.join("_"));
    }

    var smallArr = jQuery("small");
    smallArr.each(function (index) {
        var a = smallArr[index].innerText;
        if (a == oldProductId) {
            smallArr[index].innerText = newProductId;
        }
    });

    var refundTaxArr = jQuery("#refundBaseTax_" + oldProductId).attr("id");
    if (refundTaxArr) {
        refundTaxArr = refundTaxArr.split("_");
        refundTaxArr[1] = newProductId;
        jQuery("#refundBaseTax_" + oldProductId).attr("id", refundTaxArr.join("_"));
    }

    var refundBaseArr = jQuery("#refundBase_" + oldProductId).attr("id");
    if (refundBaseArr) {
        refundBaseArr = refundBaseArr.split("_");
        refundBaseArr[1] = newProductId;
        jQuery("#refundBase_" + oldProductId).attr("id", refundBaseArr.join("_"));
    }

    var baseQtyArr = jQuery("#baseQty_" + oldProductId).attr("id");
    if (baseQtyArr) {
        baseQtyArr = baseQtyArr.split("_");
        baseQtyArr[1] = newProductId;
        jQuery("#baseQty_" + oldProductId).attr("id", baseQtyArr.join("_"));
    }

    var refundQtdArr = jQuery("input[name=refundQtd_" + oldProductId + "]").attr("name");
    if (refundQtdArr) {
        refundQtdArr = refundQtdArr.split("_");
        refundQtdArr[1] = newProductId;
        jQuery("input[name=refundQtd_" + oldProductId + "]").attr("name", refundQtdArr.join("_"));
    }

    var refundTotalArr = jQuery("#refundTotal_" + oldProductId).attr("id");
    if (refundTotalArr) {
        refundTotalArr = refundTotalArr.split("_");
        refundTotalArr[1] = newProductId;
        jQuery("#refundTotal_" + oldProductId).attr("id", refundTotalArr.join("_"));
    }
}

function createTransactionOrderNotes(currentProductID, actionType, options) {
    var orderNoteObject = {
        text: ""
    };

    switch (actionType) {
        case "QUANTITY":
            orderNoteObject.text = currentProductID + " " + Resources.NOTES_QUANTITYCHANGE + " " + options;
            break;
        case "REPLACED":
            orderNoteObject.text = currentProductID + " " + Resources.NOTES_REPLACED + " " + options;
            break;
        case "ADDED":
            orderNoteObject.text = currentProductID + " " + Resources.NOTES_ADDED;
            break;
        case "REMOVED":
            orderNoteObject.text = currentProductID + " " + Resources.NOTES_REMOVED;
            break;
        default:
            break;
    }

    orderNotes.push(orderNoteObject);
}

// Initialize
jQuery(document).ready(function () {
    initializeSearchOrder();
    initializeClearAll();
    initializeTypeahead();
    initializePendingReturnList();
    initializeClearingOrdersList();
    jQuery(".orderview").hide("slide", { direction: "right" });
    searchsuggest.init("#addProduct");
    jQuery("[name=startDate]").datepicker({
        dateFormat: "yy-mm-dd",
        maxDate: "-1d",
    });
});
