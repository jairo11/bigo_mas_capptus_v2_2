/**
 *  Property of OSF Global Services, Inc., (with its brand OSF Commerce). OSF remains the sole owner of all right, title and interest in the software.
 *  Do not copy, sell, reverse engineer or otherwise attempt to derive or obtain information about the functioning, manufacture or operation therein.
 */

"use strict";

(function ($) {
    $(document).ready(function () {
        $(".form-row").on("click", ".input-value-add", function (e) {
            if (e.offsetX > e.target.offsetWidth) {
                var inputElement = $(e.target).children("input");
                var inputValue = inputElement.val().trim();
                var inputName = inputElement.attr("id");
                var elementWrapper = $(e.target).siblings(".multi-input-wrapper");

                if (inputValue.length && !elementWrapper.find("input").filter("[value=\"" + inputValue + "\"]").length) {
                    inputElement.removeClass("red");
                    var newElement = $("<div class=\"input-value-remove\"><input name=\"" + inputName + "[]\" value=\"" + inputValue + "\" /><div>");
                    elementWrapper.append(newElement);
                    inputElement.val("");
                } else {
                    inputElement.addClass("red");
                }
            }
        });

        $(".form-row").on("click", ".input-value-remove", function (e) {
            if (e.offsetX > e.target.offsetWidth) {
                $(e.target).remove();
            }
        });
        $(".pw").each(function () {
            var style = window.getComputedStyle(this);

            if (!style.webkitTextSecurity) {
                this.setAttribute("type", "password");
            }
        });
    });
}(window.jQuery));
