/* eslint-disable valid-jsdoc, require-jsdoc, eqeqeq */

"use strict";

var Logger = require("dw/system/Logger").getLogger("FastOMS", "FastOMS");
var Resource = require("dw/web/Resource");
var CybersourceHelper = require("*/cartridge/controllers/PaymentIntegration/CyberSource/CyberSourceHelper.js");
var DataStructure = require("*/cartridge/controllers/PaymentIntegration/CyberSource/CyberSourceDataStructure.js");
var Helper = require("*/cartridge/scripts/fastOMSHelper");
var csReference = webreferences.CyberSourceTransaction;

/**
 * Prepare and Call the CyberSource API to Create a Credit Request (Refund)
 */
function Credit(order, amount) {
    var service = CybersourceHelper.getServiceRequest(order.orderNo);
    var paymentInstrument = Helper.getCardPaymentInstrument(order);

    service.billTo = CybersourceHelper.getBillTo(order);
    service.purchaseTotals = CybersourceHelper.getPurchaseTotals(order, amount);
    service.ccCreditService = new csReference.CCCreditService();
    service.ccCreditService.captureRequestID = paymentInstrument.paymentTransaction.transactionID;
    service.ccCreditService.run = true;

    var serviceResponse = CybersourceHelper.callService(service);

    if (empty(serviceResponse) || serviceResponse.status != "OK") {
        Logger.error(Resource.msgf("fastoms.nullresponse", "fastoms", null, "CyberSourceManager.js", "Credit"));
        return {
            success: false,
            error: true,
            errorMsg: Resource.msgf("fastoms.emptyerror", "fastoms", null, "Credit", serviceResponse)
        };
    }

    Logger.info("[Credit] Cyber Source reasonCode: {0}", serviceResponse.object.reasonCode);
    if (serviceResponse.object.reasonCode != 100) {
        return {
            success: false,
            error: true,
            errorMsg: "[Credit] Cyber Source response code: " + serviceResponse.object.reasonCode
        };
    }

    var responseObject = DataStructure.mountCreditResponse(serviceResponse.object);
    return {
        success: (responseObject.reasonCode == 100),
        serviceResponse: responseObject,
        requestID: serviceResponse.object.requestID
    };
}

/**
 * CyberSource Authorize call
 */
function Authorize(order, orderObj) {
    var service = CybersourceHelper.getServiceRequest(order.orderNo);
    var paymentInstrument = Helper.getCardPaymentInstrument(order);
    var cardTransaction = paymentInstrument.paymentTransaction;

    service.purchaseTotals = CybersourceHelper.getPurchaseTotals(order, cardTransaction.amount.value);
    service.recurringSubscriptionInfo = new csReference.RecurringSubscriptionInfo();
    service.recurringSubscriptionInfo.subscriptionID = paymentInstrument.creditCardToken;
    service.billTo = CybersourceHelper.getBillTo(order);
    service.item = CybersourceHelper.getItems(order);
    service.card = CybersourceHelper.getCard(order, orderObj);
    service.ccAuthService = new csReference.CCAuthService();
    service.ccAuthService.run = true;

    var serviceResponse = CybersourceHelper.callService(service);

    if (empty(serviceResponse) || serviceResponse.status != "OK") {
        Logger.error(Resource.msgf("fastoms.nullresponse", "fastoms", null, "CyberSourceManager.js", "Credit"));
        return {
            success: false,
            error: true,
            errorMsg: Resource.msgf("fastoms.emptyerror", "fastoms", null, "Credit", serviceResponse)
        };
    }

    Logger.info("[Authorize] Cyber Source reasonCode: {0}", serviceResponse.object.reasonCode);
    if (serviceResponse.object.reasonCode != 100) {
        return {
            success: false,
            error: true,
            errorMsg: "[Authorize] Cyber Source response code: " + serviceResponse.object.reasonCode
        };
    }

    var responseObject = DataStructure.mountCreditResponse(serviceResponse.object);
    return {
        success: (responseObject.reasonCode == 100),
        serviceResponse: responseObject,
        requestID: serviceResponse.object.requestID
    };
}

/**
 * Prepare and Call the CyberSource API to Create a Reversal of the Authorized Amount
 */
function Reversal(order, description) {
    var service = CybersourceHelper.getServiceRequest(order.orderNo);
    var cardTransaction = Helper.getCardPaymentInstrument(order).paymentTransaction;

    service.billTo = CybersourceHelper.getBillTo(order);
    service.purchaseTotals = CybersourceHelper.getPurchaseTotals(order, cardTransaction.amount.value);
    service.ccAuthReversalService = new csReference.CCAuthReversalService();
    service.ccAuthReversalService.authRequestID = cardTransaction.transactionID;
    service.ccAuthReversalService.reversalReason = description;
    service.ccAuthReversalService.run = true;

    var serviceResponse = CybersourceHelper.callService(service);

    if (empty(serviceResponse) || serviceResponse.status != "OK") {
        return {
            success: false,
            error: true,
            errorMsg: Resource.msgf("[Reversal] Cyber Source error")
        };
    }

    Logger.info("[Reversal] Cyber Source reasonCode: {0}", serviceResponse.object.reasonCode);
    if (serviceResponse.object.reasonCode == 243) {
        return {
            success: false,
            error: true,
            errorMsg: "[Reversal] Cyber Source response code (243) :" + Resource.msg("cybersource.reversal.error", "fastoms", null)
        };
    }
    if (serviceResponse.object.reasonCode != 100) {
        return {
            success: false,
            error: true,
            errorMsg: "[Reversal] Cyber Source response code: " + serviceResponse.object.reasonCode
        };
    }

    var responseObject = DataStructure.mountCreditResponse(serviceResponse.object);
    return {
        success: (responseObject.reasonCode == 100),
        serviceResponse: responseObject,
        requestID: serviceResponse.object.requestID
    };
}

/**
 * Prepare and Call the CyberSource API to Capture the Credit Card Amount
 */
function Capture(order) {
    var service = CybersourceHelper.getServiceRequest(order.orderNo);
    var paymentTransaction = Helper.getPaymentManagerTransaction(order);

    service.billTo = CybersourceHelper.getBillTo(order);
    service.purchaseTotals = CybersourceHelper.getPurchaseTotals(order, paymentTransaction.amount.value);
    service.ccCaptureService = new csReference.CCCaptureService();
    service.ccCaptureService.authRequestID = paymentTransaction.transactionID;
    service.ccCaptureService.run = true;

    var serviceResponse = CybersourceHelper.callService(service);

    Logger.info("[Capture] Cyber Source reasonCode: {0}", serviceResponse.object.reasonCode);
    if (empty(serviceResponse) || serviceResponse.status != "OK" || serviceResponse.object.reasonCode != 100) {
        if (serviceResponse.object.reasonCode == 243) {
            return {
                success: false,
                error: true,
                errorMsg: "[Capture] Cyber Source response code (243) :" + Resource.msg("cybersource.reversal.error", "fastoms", null)
            };
        }
        Logger.error(Resource.msgf("fastoms.nullresponse", "fastoms", null, "CyberSourceManager.js", "Reversal"));
        return {
            success: false,
            error: true,
            errorMsg: Resource.msgf("fastoms.emptyerror", "fastoms", null, "Reversal", serviceResponse)
        };
    }
    var responseObject = DataStructure.mountCreditResponse(serviceResponse.object);
    return {
        success: (responseObject.reasonCode == 100),
        serviceResponse: responseObject,
        requestID: serviceResponse.object.requestID
    };
}

/**
 * True if this processor will allow a new Authorization made by BM.
 * @returns
 */
function canReAuthorize() {
    return true;
}

/**
 * True if this processor will allow a new Authorization made by BM.
 * @returns
 */
function canAuthorize() {
    return true;
}

exports.Credit = Credit;
exports.Authorize = Authorize;
exports.Reversal = Reversal;
exports.canAuthorize = canAuthorize;
exports.canReAuthorize = canReAuthorize;
exports.Capture = Capture;
