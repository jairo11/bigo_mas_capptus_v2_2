/* eslint-disable guard-for-in */

"use strict";

var Site = require("dw/system/Site");
var StringUtils = require("dw/util/StringUtils");
var Helper = require("*/cartridge/scripts/fastOMSHelper");
var csReference = webreferences.CyberSourceTransaction;
var CyberSourceHelper = {

    /*
     * return CyberSource service request
     */
    getServiceRequest: function (orderNo) {
        var serviceRequest = new csReference.RequestMessage();

        serviceRequest.merchantID = Site.getCurrent().getCustomPreferenceValue("CsMerchantId");
        serviceRequest.merchantReferenceCode = orderNo;

        return serviceRequest;
    },

    /*
     * return Purchase Totals
     */
    getPurchaseTotals: function (order, amount) {
        var purchaseTotals = new csReference.PurchaseTotals();
        var amountValue = (amount) || order.totalGrossPrice.value;

        purchaseTotals.currency = order.totalGrossPrice.currencyCode;
        purchaseTotals.grandTotalAmount = StringUtils.formatNumber(amountValue, "000000.00", "en_US");

        return purchaseTotals;
    },

    /*
     * prepare "Bill To" object from order fields
     */
    getBillTo: function (order) {
        var billTo = new csReference.BillTo();
        var names = order.customerName.split(" ");

        billTo.firstName = names[0];
        billTo.lastName = (names.length > 1) ? names[1] : "";
        billTo.email = order.customerEmail;
        billTo.phoneNumber = order.billingAddress.phone;
        billTo.street1 = order.billingAddress.address1;
        billTo.city = order.billingAddress.city;
        billTo.state = order.billingAddress.stateCode;
        billTo.country = order.billingAddress.countryCode;
        billTo.postalCode = order.billingAddress.postalCode;

        return billTo;
    },

    /*
     * get Credit Card information
     */
    getCard: function (order, orderObj) {
        var card = new csReference.Card();

        var cardPaymentInstrument = Helper.getCardPaymentInstrument(order);

        card.expirationMonth = StringUtils.formatNumber(cardPaymentInstrument.creditCardExpirationMonth, "00");
        card.expirationYear = cardPaymentInstrument.creditCardExpirationYear;
        card.fullName = cardPaymentInstrument.creditCardHolder;

        if (empty(cardPaymentInstrument.getCreditCardToken())) {
            card.accountNumber = (orderObj && orderObj.card) ? orderObj.card : cardPaymentInstrument.creditCardNumber;
        }

        switch (cardPaymentInstrument.creditCardType.toLowerCase()) {
            case "visa":
                card.cardType = "001";
                break;
            case "master card":
                card.cardType = "002";
                break;
            case "mastercard":
                card.cardType = "002";
                break;
            case "amex":
                card.cardType = "003";
                break;
            case "discover":
                card.cardType = "004";
                break;
            case "maestro":
                card.cardType = "042";
                break;
            default:
                card.cardType = "001";
                break;
        }

        return card;
    },

    /*
     * return order items in CyberSource friendly format
     */
    getItems: function (order) {
        var items = [];
        var count = 1;
        /* eslint-disable no-restricted-syntax */
        for (var key in order.allLineItems) {
            var li = order.allLineItems[key];
            var itemObject = new csReference.Item();

            switch (true) {
                case li instanceof dw.order.ProductLineItem:
                    itemObject.id = count;
                    itemObject.productCode = "default";
                    itemObject.productSKU = li.productID;
                    itemObject.productName = li.productName;
                    itemObject.quantity = li.quantityValue;
                    itemObject.unitPrice = StringUtils.formatNumber(li.proratedPrice.value, "000000.00", "en_US");
                    itemObject.taxAmount = StringUtils.formatNumber(li.adjustedTax.value, "000000.00", "en_US");
                    break;

                case li instanceof dw.order.ShippingLineItem:
                    itemObject.id = count;
                    itemObject.productCode = li.ID;
                    itemObject.productSKU = li.ID;
                    itemObject.productName = li.ID;
                    itemObject.quantity = 1;
                    itemObject.unitPrice = StringUtils.formatNumber(li.adjustedPrice.value, "000000.00", "en_US");
                    itemObject.taxAmount = StringUtils.formatNumber(li.adjustedTax.value, "000000.00", "en_US");
                    break;

                case li instanceof dw.order.ProductShippingLineItem:
                    itemObject.id = count;
                    itemObject.productCode = "SHIPPING_SURCHARGE";
                    itemObject.productSKU = "SHIPPING_SURCHARGE";
                    itemObject.productName = "SHIPPING_SURCHARGE";
                    itemObject.quantity = 1;
                    itemObject.unitPrice = StringUtils.formatNumber(li.adjustedPrice.value, "000000.00", "en_US");
                    itemObject.taxAmount = StringUtils.formatNumber(li.adjustedTax.value, "000000.00", "en_US");
                    break;

                case li instanceof dw.order.GiftCertificateLineItem:
                    itemObject.id = count;
                    itemObject.productCode = "GIFT_CERTIFICATE";
                    itemObject.productSKU = "GIFT_CERTIFICATE";
                    itemObject.productName = "GIFT_CERTIFICATE";
                    itemObject.quantity = 1;
                    itemObject.unitPrice = StringUtils.formatNumber(li.grossPrice.value, "000000.00", "en_US");
                    itemObject.taxAmount = StringUtils.formatNumber(0, "000000.00", "en_US");
                    break;

                default:
                    break;
            }

            if (!(li instanceof dw.order.PriceAdjustment)) {
                items.push(itemObject);
                count++;
            }
        }
        /* eslint-enable no-restricted-syntax */

        return items;
    },

    /*
     * call CyberSource service
     */
    callService: function (request) {
        var Logger = require("dw/system/Logger");
        var SOAPUtil = require("dw/rpc/SOAPUtil");
        var HashMap = require("dw/util/HashMap");

        try {
            var service = dw.svc.LocalServiceRegistry.createService("cybersource.soap.transactionprocessor.generic", {
                initServiceClient: function (svc) {
                    var svce = svc;
                    svce.serviceClient = csReference.getDefaultService();
                    return svce.serviceClient;
                },
                execute: function (svc, parameter) {
                    var userName = svc.getConfiguration().getCredential().getUser();
                    var password = Site.getCurrent().getCustomPreferenceValue("CsSecurityKey");
                    var secretsMap = new HashMap();
                    var requestCfg = new HashMap();
                    var responseCfg = new HashMap();

                    secretsMap.put(userName, password);

                    requestCfg.put(SOAPUtil.WS_ACTION, SOAPUtil.WS_USERNAME_TOKEN);
                    requestCfg.put(SOAPUtil.WS_USER, userName);
                    requestCfg.put(SOAPUtil.WS_PASSWORD_TYPE, SOAPUtil.WS_PW_TEXT);
                    requestCfg.put(SOAPUtil.WS_SECRETS_MAP, secretsMap);

                    responseCfg.put(SOAPUtil.WS_ACTION, SOAPUtil.WS_TIMESTAMP);

                    SOAPUtil.setWSSecurityConfig(svc.serviceClient, requestCfg, responseCfg);

                    return svc.serviceClient.runTransaction(parameter);
                },
                parseResponse: function (_service, response) {
                    return response;
                }
            });

            return service.call(request);
        } catch (e) {
            Logger.error(dw.web.Resource.msgf("fastoms.errorin", "fastoms", null, "CyberSourceHelper.js", "callService", e.message));
            return {
                error: true,
                errorMsg: e.message
            };
        }
    }
};

module.exports = CyberSourceHelper;
