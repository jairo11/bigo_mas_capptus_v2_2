"use strict";

var CyberSourceDataStructures = {

    /*
     * parce CyberSource main service response
     */
    mountMainResponse: function (object) {
        return {
            requestID: object.requestID,
            requestToken: object.requestToken,
            decision: object.decision,
            reasonCode: object.reasonCode.get(),
            invalidField: object.invalidField,
            missingField: object.missingField
        };
    },

    /*
     * parce CyberSource credit service response
     */
    mountCreditResponse: function (object) {
        var respObj = this.mountMainResponse(object);

        if (object.ccCreditReply) {
            respObj.amount = object.ccCreditReply.amount;
            respObj.reconciliationID = object.ccCreditReply.reconciliationID;
            respObj.requestDateTime = object.ccCreditReply.requestDateTime;
        }

        return respObj;
    },

    /*
     * parce CyberSource authorize response
     */
    mountAuthorizeResponse: function (object) {
        var respObj = this.mountMainResponse(object);

        if (object.ccAuthReply) {
            respObj.amount = object.ccAuthReply.amount;
            respObj.reconciliationID = object.ccAuthReply.reconciliationID;
        }

        return respObj;
    },

    /*
     * parce CyberSource reversal response
     */
    mountReversalResponse: function (object) {
        var respObj = this.mountMainResponse(object);

        if (object.ccAuthReversalReply) {
            respObj.amount = object.ccAuthReversalReply.amount;
            respObj.requestDateTime = object.ccAuthReversalReply.requestDateTime;
        }

        return respObj;
    }

};

module.exports = CyberSourceDataStructures;
