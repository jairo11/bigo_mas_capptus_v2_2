/**
 *  Property of OSF Global Services, Inc., (with its brand OSF Commerce). OSF remains the sole owner of all right, title and interest in the software.
 *  Do not copy, sell, reverse engineer or otherwise attempt to derive or obtain information about the functioning, manufacture or operation therein.
 */

"use strict";

/* eslint-disable  no-unused-vars, new-cap */
exports.CYBERSOURCE_CREDIT = {
    GetManager: function () {
        var csManager = require("*/cartridge/controllers/PaymentIntegration/CyberSource/CyberSourceManager.js");
        var Helper = require("*/cartridge/scripts/fastOMSHelper");
        var Resource = require("dw/web/Resource");

        return {
            Authorize: function (order, orderObj) {
                var result = csManager.Authorize(order, orderObj);
                Helper.createPaymentTransaction(order, "CYBERSOURCE_CREDIT", "AUTH", result.requestID);
                return result;
            },

            Refund: function (order, amount) {
                var result = csManager.Credit(order, amount);
                if (result.success) {
                    Helper.createPaymentTransaction(order, "CYBERSOURCE_CREDIT", "REFUND", result.requestID, amount);
                }
                return result;
            },

            canAuthorize: function (order) {
                return csManager.canAuthorize(order);
            },

            canReAuthorize: function (order) {
                return csManager.canReAuthorize(order);
            },

            Cancel: function (order, description) {
                try {
                    var result = csManager.Reversal(order, description);
                    Helper.createPaymentTransaction(order, "CYBERSOURCE_CREDIT", "REVERSAL", result.requestID);
                    return result;
                } catch (error) {
                    return {
                        success: false,
                        errorMsg: Resource.msg("cybersource.connection.error", "fastoms", null)
                    };
                }
            },

            Capture: function (order, _description) {
                try {
                    var result = csManager.Capture(order);
                    Helper.createPaymentTransaction(order, "CYBERSOURCE_CREDIT", "CAPTURE", result.requestID);
                    return result;
                } catch (error) {
                    return {
                        success: false,
                        errorMsg: Resource.msg("cybersource.connection.error", "fastoms", null)
                    };
                }
            },

            canPartialReversal: function () {
                return false;
            }
        };
    }
};

exports.PAYPAL = {
    GetManager: function () {
        var ppManager = require("*/cartridge/controllers/PaymentIntegration/PayPal/PayPalManager.js");
        var Helper = require("*/cartridge/scripts/fastOMSHelper");
        var Resource = require("dw/web/Resource");

        return {

            Refund: function (order, amount) {
                var result = ppManager.Refund(order, amount);
                if (result.success) {
                    Helper.createPaymentTransaction(order, "PAYPAL_CREDIT", "REFUND", result.serviceResponse.REFUNDTRANSACTIONID, amount);
                }
                return result;
            },

            ReAuthorize: function (order, amount) {
                var result = ppManager.ReAuthorize(order, amount);
                return result;
            },

            canAuthorize: function (order) {
                return ppManager.canAuthorize(order);
            },

            canReAuthorize: function (order) {
                return ppManager.canReAuthorize(order);
            },

            Cancel: function (order, description) {
                try {
                    var result =  ppManager.Cancel(order, description);
                    Helper.createPaymentTransaction(order, "PAYPAL", "VOID", result.serviceResponse.AUTHORIZATIONID);
                    return result;
                } catch (error) {
                    return {
                        success: false,
                        errorMsg: result.errorMsg || Resource.msg("cybersource.connection.error", "fastoms", null)
                    };
                }
            },

            Capture: function (order, _description) {
                try {
                    var result = ppManager.Capture(order);
                    Helper.createPaymentTransaction(order, "PAYPAL_CREDIT", "CAPTURE", result.serviceResponse.TRANSACTIONID);
                    return result;
                } catch (error) {
                    return {
                        success: false,
                        errorMsg: result.errorMsg || Resource.msg("cybersource.connection.error", "fastoms", null)
                    };
                }
            },

            canPartialReversal: function () {
                return false;
            }
        };
    }
};

exports.BASIC_CREDIT = {
    GetManager: function () {
        return {
            Authorize: function (order, orderObj) {
                return {
                    success: true,
                    requestID: "BASIC_TRANSACTION_ID"
                };
            },

            Refund: function (order, amount) {
                return {
                    success: true,
                    serviceResponse: {}
                };
            },

            canAuthorize: function (order) {
                return true;
            },

            canReAuthorize: function (order) {
                return true;
            },

            Cancel: function (order, description) {
                return {
                    success: true
                };
            },

            Capture: function (order, _description) {
                return {
                    success: true,
                    serviceResponse: {}
                };
            },

            canPartialReversal: function () {
                return false;
            }
        };
    }
};

exports.MERCADOPAGO_CREDIT = {
    GetManager: function () {
        var mercadoPagoHelper = require("*/cartridge/controllers/PaymentIntegration/MercadoPago/MercadoPagoHelper.js");
        var Helper = require("*/cartridge/scripts/fastOMSHelper");

        return {
            Authorize: function (order, orderObj) {
                var result = mercadoPagoHelper.authorize(order, orderObj);
                if (result.success) {
                    Helper.createPaymentTransaction(order, "MERCADOPAGO_CREDIT", "AUTH", result.requestID);
                }
                return result;
            },

            Refund: function (order, amount) {
                var result = mercadoPagoHelper.refund(order, Number(amount));
                if (result.success) {
                    Helper.createPaymentTransaction(order, "MERCADOPAGO_CREDIT", "REFUND", result.requestID, amount);
                }
                return result;
            },

            canAuthorize: function () {
                return false;
            },

            canReAuthorize: function () {
                return false;
            },

            Cancel: function (order, description) {
                var result = mercadoPagoHelper.cancel(order, description);
                if (result.success) {
                    Helper.createPaymentTransaction(order, "MERCADOPAGO_CREDIT", "REVERSAL", result.requestID);
                }
                return result;
            },

            Capture: function (order, _description) {
                var result = mercadoPagoHelper.capture(order);
                if (result.success) {
                    Helper.createPaymentTransaction(order, "MERCADOPAGO_CREDIT", "CAPTURE", result.requestID);
                }
                return result;
            },

            canPartialReversal: function () {
                return false;
            }
        };
    }
};

exports.Adyen_Component = {
    GetManager: function () {
        var adyenManager = require("*/cartridge/controllers/PaymentIntegration/Adyen/AdyenManager.js");

        return {
            ReAuthorize: function (order, amount) {
                return adyenManager.ReAuthorize(order, amount);
            },

            Capture: function (order) {
                return adyenManager.Capture(order);
            },

            Refund: function (order, amount) {
                return adyenManager.Refund(order, amount);
            },

            Cancel: function (order) {
                return adyenManager.Cancel(order);
            },

            canPartialReversal: function () {
                return false;
            },

            canAuthorize: function () {
                return false;
            },

            canReAuthorize: function () {
                return true;
            }
        };
    }
};
