"use strict";

var Site = require("dw/system/Site");
var FOMSLogger = require("dw/system/Logger").getLogger("FastOMS", "FastOMS");

var PayPalHelper = {
    /*
     * get PayPal service
     */
    getService: function (serviceCallback) {
        var service = null;
        var serviceID = "int_paypal.http.nvp.payment.PayPal." + Site.getCurrent().getID();

        if (serviceCallback) {
            try {
                service = dw.svc.LocalServiceRegistry.createService(serviceID, serviceCallback);
            } catch (error) {
                FOMSLogger.info("\n Paypal service ID :" + serviceID + " was not found \n int_paypal.http.nvp.payment.PayPal was tried as service");
                serviceID = "int_paypal.http.nvp.payment.PayPal";
                service = dw.svc.LocalServiceRegistry.createService(serviceID, serviceCallback);
            }
        }

        return service;
    },

    /*
     *  prepare PayPal service call
     */
    getServiceCallback: function (pairsArr) {
        var serviceCallback = {
            createRequest: function (service) {
                service.addHeader("Content-Type", "application/x-www-form-urlencoded");

                var credentials = service.getConfiguration().getCredential();

                pairsArr.unshift("VERSION=121.0");
                pairsArr.unshift("SIGNATURE=" + credentials.custom.PP_API_Signature);
                pairsArr.unshift("PWD=" + credentials.getPassword());
                pairsArr.unshift("USER=" + credentials.getUser());

                return pairsArr.join("&");
            },
            parseResponse: function (_service, listOutput) {
                return listOutput.text;
            }
        };

        return serviceCallback;
    },

    /*
     *  parse PayPal service response
     */
    parseResponse: function (respStr) {
        var result = {};

        if (!empty(respStr)) {
            var decodedStr = dw.crypto.Encoding.fromURI(respStr);
            var pairsArr = decodedStr.split("&");

            for (var i = 0; i < pairsArr.length; i++) {
                var pair = pairsArr[i].split("=");
                result[pair[0]] = pair[1];
            }
        }

        return result;
    }
};

module.exports = PayPalHelper;
