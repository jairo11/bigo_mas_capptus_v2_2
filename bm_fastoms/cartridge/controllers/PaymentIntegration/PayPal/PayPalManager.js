/* eslint-disable valid-jsdoc, require-jsdoc, no-use-before-define, eqeqeq */

"use strict";

var Logger = require("dw/system/Logger").getLogger("FastOMS", "FastOMS");
var Resource = require("dw/web/Resource");
var Helper = require("*/cartridge/controllers/PaymentIntegration/PayPal/PayPalHelper.js");
var FastOMSHelper = require("*/cartridge/scripts/fastOMSHelper");
var Transaction = require("dw/system/Transaction");

/**
 * Prepare and Call the PayPal API to Refund an Order
 */
function Refund(order, amount) {
    var paymentTransaction = FastOMSHelper.getPaymentManagerTransaction(order);
    var pairsArr = [
        "METHOD=RefundTransaction",
        "TRANSACTIONID=" + paymentTransaction.transactionID,
        "PAYERID=" + paymentTransaction.paymentInstrument.custom.paypalPayerID,
        "REFUNDTYPE=PARTIAL",
        "CURRENCYCODE=" + order.currencyCode,
        "AMT=" + parseFloat(amount).toFixed(2)
    ];

    var service = Helper.getService(Helper.getServiceCallback(pairsArr));
    var svcResponse = service.call(order);

    return getResultObj(svcResponse);
}

/**
 * Prepares and Call the PayPal API to Capture an Order
 */
function Capture(order) {
    var paymentTransaction = FastOMSHelper.getPaymentManagerTransaction(order);
    //In initial order AUTHORIZATIONID === TRANSACTIONID
    var pairsArr = [
        "METHOD=DoCapture",
        "AUTHORIZATIONID=" + paymentTransaction.transactionID,
        "AMT=" + paymentTransaction.amount,
        "COMPLETETYPE=NotComplete",
        "CURRENCYCODE=" + order.currencyCode
    ];

    var service = Helper.getService(Helper.getServiceCallback(pairsArr));
    var svcResponse = service.call(order);
    var resultObject = getResultObj(svcResponse);

    //Change initial authorization id(transaction id) with transaction id
    if (resultObject.success) {
        var orderNo = order.getOrderNo();
        var paymentTransactionID = paymentTransaction.getTransactionID();
        var responseTransactionID = resultObject.serviceResponse.TRANSACTIONID;
        Logger.info("OrderNo: {0} was captured\nTransaction ID was changed from {1} to {2}", orderNo, paymentTransactionID, responseTransactionID);
        Transaction.wrap(function () {
            paymentTransaction.transactionID = responseTransactionID;
        });
    }
    return resultObject;
}

/**
 * Prepare and Call the PayPal API to Re-Authorize an Order with new amount
 * (Works only three days after the Original Authorization - honor period)
 */
function ReAuthorize(order, amount) {
    var paymentTransaction = FastOMSHelper.getPaymentManagerTransaction(order);
    var pairsArr = [
        "METHOD=DoReauthorization",
        "AUTHORIZATIONID=" + paymentTransaction.transactionID,
        "CURRENCYCODE=" + order.currencyCode,
        "AMT=" + amount.toFixed(2)
    ];

    var service = Helper.getService(Helper.getServiceCallback(pairsArr));
    var svcResponse = service.call(order);

    return getResultObj(svcResponse);
}

/**
 * Defines whether this order can or not be re-authorized
 * @returns boolean according to honor period defined on API:
 * https://developer.paypal.com/docs/classic/paypal-payments-standard/integration-guide/authcapture/#honor-period-and-authorization-period
 */
function canReAuthorize(order) {
    var now = new Date().getTime();
    var creationDate = order.getCreationDate().getTime();

    var daysSinceAuth = (now - creationDate) / (24 * 60 * 60 * 1000);

    if (daysSinceAuth > 3) {
        return true;
    }

    return false;
}
/**
 * Prepares a void request for the PayPal API, calls service and handles a response
 */
function Cancel(order, description) {
    var paymentTransaction = FastOMSHelper.getPaymentManagerTransaction(order);
    var pairsArr = [
        "METHOD=DoVoid",
        "AUTHORIZATIONID=" + paymentTransaction.transactionID,
        "NOTE=" + (description || "")
    ];

    var service = Helper.getService(Helper.getServiceCallback(pairsArr));
    var svcResponse = service.call(order);

    return getResultObj(svcResponse);
}

// helper function for validation and constructing the PayPalManager's work result object
function getResultObj(serviceResponse) {
    if (empty(serviceResponse) || serviceResponse.status != "OK") {
        Logger.error(Resource.msgf("fastoms.nullresponse", "fastoms", null, "PayPalManager.js", "Cancel"));

        return {
            error: true,
            errorMsg: Resource.msgf("fastoms.emptyerror", "fastoms", null, "Cancel", serviceResponse.errorMessage)
        };
    }

    var parsedResp = Helper.parseResponse(serviceResponse.object);

    if (empty(parsedResp) || parsedResp.ACK != "Success") {
        var errorMsg = parsedResp.L_ERRORCODE0 ? Resource.msgf("paypal.dovoid.error", "fastoms", null, parsedResp.L_ERRORCODE0, parsedResp.L_LONGMESSAGE0) : parsedResp.L_LONGMESSAGE0;
        return {
            error: true,
            errorMsg: errorMsg,
        };
    }

    return {
        success: true,
        serviceResponse: parsedResp
    };
}

/**
 * True if this processor will allow a new Authorization made by BM.
 * @returns
 */
function canAuthorize() {
    return false;
}

exports.Cancel = Cancel;
exports.Refund = Refund;
exports.ReAuthorize = ReAuthorize;
exports.canReAuthorize = canReAuthorize;
exports.canAuthorize = canAuthorize;
exports.Capture = Capture;
