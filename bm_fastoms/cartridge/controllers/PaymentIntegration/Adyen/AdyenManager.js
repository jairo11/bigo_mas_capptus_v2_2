"use strict";

/**
 * @description Refund order
 * @param {dw.order.Order} order
 * @param {Number} amount
 * @returns {Object}
 */
function Refund(order, amount) {
    var Transaction = require("dw/system/Transaction");
    var adyenRefund = require("int_adyen_overlay/cartridge/scripts/adyenRefund");

    var result = Transaction.wrap(function () {
        return adyenRefund.refund(order, amount);
    });

    return {success: result === "SUCCESS"};
}

/**
 * @description Cancel order
 * @param {dw.order.Order} order
 * @returns {Object}
 */
function Cancel(order) {
    var Transaction = require("dw/system/Transaction");
    var adyenCancel = require("int_adyen_overlay/cartridge/scripts/adyenCancel");

    var result = Transaction.wrap(function () {
        return adyenCancel.cancel(order);
    });

    return {success: result === "SUCCESS"};
}

/**
 * @description Capture order
 * @param {dw.order.Order} order
 * @returns {Object}
 */
function Capture(order) {
    var Transaction = require("dw/system/Transaction");
    var adyenCapture = require("int_adyen_overlay/cartridge/scripts/adyenCapture");

    var result = Transaction.wrap(function () {
        return adyenCapture.capture(order);
    });

    return {success: result === "SUCCESS"};
}

/**
 * @description ReAuthorize order
 * @param {dw.order.Order} order
 * @param {Number} amount
 * @returns {Object}
 */
function ReAuthorize(order, amount) {
    var Transaction = require("dw/system/Transaction");
    var adyenReauthorize = require("int_adyen_overlay/cartridge/scripts/adyenReauthorize");

    var result = Transaction.wrap(function () {
        return adyenReauthorize.reauthorize(order, amount);
    });

    return {success: result === "SUCCESS"};
}

module.exports = {
    Capture: Capture,
    Cancel: Cancel,
    Refund: Refund,
    ReAuthorize: ReAuthorize
};
