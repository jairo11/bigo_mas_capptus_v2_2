"use strict";

var PaymentMgr = require("dw/order/PaymentMgr");
var Resource = require("dw/web/Resource");
var MercadoPago = require("*/cartridge/scripts/library/libMercadoPago");

/**
 * @description Make call to API to refund payment
 * @param {String} transactionId - The transaction ID
 * @param {Number} amount - The amount to be refunded
 * @returns {Object}
 */
function Refund(transactionId, amount) {
    var MP = new MercadoPago();
    var serverErrors = [];
    var error = false;

    try {
        // Do refund request
        var refundResponse = MP.refundPayment(transactionId, amount);
        if (!(refundResponse && refundResponse.status)) {
            error = true;
            serverErrors.push(
                Resource.msg("error.technical", "checkout", null)
            );
        }
    } catch (e) {
        error = true;
        serverErrors.push(
            Resource.msg("error.technical", "checkout", null)
        );
    }

    return { serverErrors: serverErrors, error: error };
}

/**
 * @description Make call to API to cancel payment
 * @param {String} transactionId - The transaction ID
 * @returns {Object}
 */
function Cancel(transactionId) {
    var MP = new MercadoPago();
    var serverErrors = [];
    var error = false;

    try {
        // Do cancel request
        var cancelResponse = MP.cancelPayment(transactionId);
        if (!(cancelResponse && cancelResponse.status)) {
            error = true;
            serverErrors.push(
                Resource.msg("error.technical", "checkout", null)
            );
        }
    } catch (e) {
        error = true;
        serverErrors.push(
            Resource.msg("error.technical", "checkout", null)
        );
    }

    return { serverErrors: serverErrors, error: error };
}

/**
 * @description Make call to API to capture payment
 * @param {String} transactionId - The transaction ID
 * @returns {Object}
 */
function Capture(transactionId) {
    var MP = new MercadoPago();
    var serverErrors = [];
    var error = false;

    try {
        // Do capture request
        var captureResponse = MP.capturePayment(transactionId);
        if (!(captureResponse && captureResponse.status)) {
            error = true;
            serverErrors.push(
                Resource.msg("error.technical", "checkout", null)
            );
        }
    } catch (e) {
        error = true;
        serverErrors.push(
            Resource.msg("error.technical", "checkout", null)
        );
    }

    return { serverErrors: serverErrors, error: error };
}

/**
 * Get Mercado Pago payment instrument from array of payment instruments
 * @param {dw.order.LineItemCtnr} lineItemContainer Order object
 * @return {dw.order.OrderPaymentInstrument} Mercado Pago Payment Instrument
 */
function getMercadoPagoPaymentInstrument(lineItemContainer) {
    var paymentInstruments = lineItemContainer.getPaymentInstruments();
    var mercadoPagoPaymentInstrument = null;

    var iterator = paymentInstruments.iterator();
    var paymentInstrument = null;
    while (iterator.hasNext()) {
        paymentInstrument = iterator.next();
        var paymentProcessorId = PaymentMgr.getPaymentMethod(paymentInstrument.getPaymentMethod()).getPaymentProcessor().getID();

        if (paymentProcessorId === "MERCADOPAGO_CREDIT") {
            mercadoPagoPaymentInstrument = paymentInstrument;
            break;
        }
    }

    return mercadoPagoPaymentInstrument;
}

exports.authorize = function () {
    return {
        success: false,
        requestID: ""
    };
};

exports.refund = function (order, amount) {
    var paymentInstrument = getMercadoPagoPaymentInstrument(order);
    var transactionId = paymentInstrument.getPaymentTransaction().getTransactionID();
    var refundResult = Refund(transactionId, amount);

    return {
        success: refundResult && !refundResult.error,
        requestID: transactionId
    };
};

exports.cancel = function (order) {
    var paymentInstrument = getMercadoPagoPaymentInstrument(order);
    var transactionId = paymentInstrument.getPaymentTransaction().getTransactionID();
    var cancelResult = Cancel(transactionId);

    return {
        success: cancelResult && !cancelResult.error,
        requestID: transactionId
    };
};

exports.capture = function (order) {
    var paymentInstrument = getMercadoPagoPaymentInstrument(order);
    var transactionId = paymentInstrument.getPaymentTransaction().getTransactionID();
    var captureResult = Capture(transactionId);

    return {
        success: captureResult && !captureResult.error,
        requestID: transactionId
    };
};
