/**
 *  Property of OSF Global Services, Inc., (with its brand OSF Commerce). OSF remains the sole owner of all right, title and interest in the software.
 *  Do not copy, sell, reverse engineer or otherwise attempt to derive or obtain information about the functioning, manufacture or operation therein.
 */

/* eslint-disable  eqeqeq, valid-jsdoc, no-use-before-define, guard-for-in, consistent-return, new-cap */

"use strict";

/**
 * Controller for managing the FastOMS module
 * @module controllers/FastOMSController
 */

var OrderMgr = require("dw/order/OrderMgr");
var Order = require("dw/order/Order");
var ProductMgr = require("dw/catalog/ProductMgr");
var CouponMgr = require("dw/campaign/CouponMgr");
var GiftCertificateMgr = require("dw/order/GiftCertificateMgr");
var PaymentMgr = require("dw/order/PaymentMgr");
var Site = require("dw/system/Site");
var Status = require("dw/system/Status");
var System = require("dw/system/System");
var Transaction = require("dw/system/Transaction");
var Resource = require("dw/web/Resource");
var SuggestModel = require("dw/suggest/SuggestModel");
var FOMSLogger = require("dw/system/Logger").getLogger("FastOMS", "FastOMS");
var StringUtils = require("dw/util/StringUtils");
var Calendar = require("dw/util/Calendar");
var Cipher = require("dw/crypto/Cipher");
var URLAction = require("dw/web/URLAction");
var URLUtils = require("dw/web/URLUtils");
var URLParameter = require("dw/web/URLParameter");
var Currency = require("dw/util/Currency");
var ISML = require("dw/template/ISML");
var Helper = require("*/cartridge/scripts/fastOMSHelper");
var PagingModel = require("dw/web/PagingModel");
var CustomObjectMgr = require("dw/object/CustomObjectMgr");
var PaymentInstrument = require("dw/order/PaymentInstrument");
var DIS = require("*/cartridge/scripts/util/DIS");
var couponAutomation = require("*/cartridge/scripts/couponAutomation");
var couponManual = require("*/cartridge/scripts/couponManual");
var siteID = Site.getCurrent().getID();
var siteName = Site.getCurrent().getName();
var params = request.httpParameterMap;
var getPaymentManager = Helper.getPaymentManager;
var refreshStock = Helper.refreshStock;
var getValues = Helper.getValues;
var orderCancelation = Helper.orderCancelation;
var sendEmail = Helper.sendEmail;
var getApplicableShippingMethods = Helper.getApplicableShippingMethods;
var createGiftCertificate = Helper.createGiftCertificate;
var addEditionOrRefundNote = Helper.addEditionOrRefundNote;
var removeShipmentValue = Helper.removeShipmentValue;
var updateOrder = Helper.updateOrder;
var getProductVariantion = Helper.getProductVariantion;
var lockOrder = Helper.lockOrder;
var unlockOrder = Helper.unlockOrder;
var getUsedPromotions = Helper.getUsedPromotions;
var removeOldRequests = Helper.removeOldRequests;
var checkUsedCouponsInBM = Helper.checkUsedCouponsInBM;

//It controls whetever the user is a bm or storefront user
function controlUser() {
    var sessionUserName = session.getUserName();
    if (sessionUserName === "storefront") {
        throw Error("Unauthorized Access");
    }
}
/**
  Checks if the license key is valid or not
 */
function isValidLicense() {
    var OSFLicenseManager = require("*/cartridge/scripts/OSFLicenseManager");
    try {
        var license = OSFLicenseManager.getLicenseStatus("LOMS");
        if (!license.isValid) {
            FOMSLogger.error("LOMS : Invalid license");
            return false;
        }
    } catch (e) {
        FOMSLogger.error(e.toString());
        return false;
    }
    return true;
}
/**
 * generate a json response
 */
function renderJSON(object) {
    response.setContentType("application/json");

    var json = JSON.stringify(object);
    response.writer.print(json);
}
/**
 * Render Main Dashboard
 */

exports.Manage = function () {
    ISML.renderTemplate("fastOMS", {
        ValidLicense: isValidLicense(),
        FDResources: Helper.getResources(),
        FDUrls: Helper.getUrls()
    });
};
exports.Manage.public = true;

/**
 * Generates paging model for orders/subscription search results
 * @param {Object[]} elements a colection of Objects representing the scheduled order/subscriptions stored in the customer profile
 */
function constructPagingModel(elements, hasCount) {
    var defaultPaginationSize = 10;
    var parameterMap = request.httpParameterMap;
    var pageSize = parameterMap.sz.intValue || defaultPaginationSize;
    var start = parameterMap.start.intValue || 0;
    var elementsPagingModel;
    if (hasCount) {
        elementsPagingModel = new PagingModel(elements, elements.count);
    } else {
        elementsPagingModel = new PagingModel(elements);
    }

    elementsPagingModel.setPageSize(pageSize);
    elementsPagingModel.setStart(start);

    return elementsPagingModel;
}

function preparePagingURL() {
    var pagingURL = URLUtils.https("FastOMSController-OrderSearch");
    if (!params.startDate.empty) {
        pagingURL = pagingURL.append("startDate", params.startDate.stringValue);
    }
    if (!params.simpleSearch.empty) {
        pagingURL = pagingURL.append("simpleSearch", params.simpleSearch.stringValue);
    }
    if (!params.orderNo.empty) {
        pagingURL = pagingURL.append("orderNo", params.orderNo.stringValue);
    }
    if (!params.customerName.empty) {
        pagingURL = pagingURL.append("customerName", params.customerName.stringValue);
    }
    if (!params.customerEmail.empty) {
        pagingURL = pagingURL.append("orderNo", params.customerEmail.stringValue);
    }
    if (!params.status.empty) {
        pagingURL = pagingURL.append("status", params.status.stringValue);
    }
    if (!params.returnTracking.empty) {
        pagingURL = pagingURL.append("returnTracking", params.returnTracking.stringValue);
    }
    return pagingURL;
}

/**
 * Order Search
 */
exports.OrderSearch = function () {
    controlUser();

    var queryStr = "";
    var currentDate = new Calendar(new Date());
    var dateString;
    if (params.startDate.empty) {
        currentDate.add(Calendar.MONTH, -1);
        dateString = currentDate.time.toISOString().split("T")[0] + "T00:00:00";
    } else {
        dateString = params.startDate.stringValue + "T00:00:00";
    }
    if (params.parameterCount == 1) {
        queryStr = "custom.LOMSPendingReturn = true";
    } else if (!params.simpleSearch.empty) {
        if (params.simpleSearch.stringValue != "") {
            var search = "'*" + params.simpleSearch.stringValue.replace(/'/g, "") + "*'";
            queryStr = "orderNo ILIKE " + search + " OR customerName ILIKE " + search + " OR customerEmail ILIKE " + search;
        }
    } else {
        var queryObj = {
            orderNo: params.orderNo.stringValue,
            customerName: params.customerName.stringValue,
            customerEmail: params.customerEmail.stringValue,
            status: params.status.intValue,
            "custom.LOMSReturnTracking": params.returnTracking.stringValue
        };

        queryStr = prepareQueryString(queryObj);
    }

    queryStr = queryStr + (!empty(queryStr) ? " AND " : "") + "creationDate > " + dateString;
    var orders = OrderMgr.queryOrders(queryStr, "creationDate desc", true);
    var orderPagingModel = constructPagingModel(orders, true);

    if (orders.count > 0 && !empty(params.trackingNumber.stringValue)) {
        var ordersList = new dw.util.ArrayList();
        while (orders.hasNext()) {
            var order = orders.next();
            /* eslint-disable no-restricted-syntax */
            for (var key in order.shipments) {
                var shipment = order.shipments[key];
                if (!empty(shipment.trackingNumber) && shipment.trackingNumber.indexOf(params.trackingNumber.stringValue) != -1) {
                    ordersList.push(order);
                }
            }
            /* eslint-enable no-restricted-syntax */
        }
        orderPagingModel = constructPagingModel(ordersList, false);
    }

    ISML.renderTemplate("ordersList", {
        fastOMSHelper: Helper,
        OrderPagingModel: orderPagingModel,
        pagingURL: preparePagingURL()
    });
};
exports.OrderSearch.public = true;

function processSaveOrRefundQueue(order) {
    var orderNo = order.getOrderNo();
    var toProcessedObject = null;
    var ocapiCallAddress = "";
    var message = "";

    var pendingSaveOrderObject = CustomObjectMgr.queryCustomObjects("FastOMSSaveOrderQueue", "", "creationDate asc").first();
    var pendingRefundOrderObject = CustomObjectMgr.queryCustomObjects("FastOMSRefundQueue", "", "creationDate asc").first();

    if (pendingRefundOrderObject && pendingRefundOrderObject.custom.orderNo == orderNo) {
        ocapiCallAddress = "FastOMSRefund";
        toProcessedObject = pendingRefundOrderObject;
    } else if (pendingSaveOrderObject && pendingSaveOrderObject.custom.orderNo == orderNo) {
        ocapiCallAddress = "FastOMSSaveOrder";
        toProcessedObject = pendingSaveOrderObject;
    }

    removeOldRequests("FastOMSSaveOrderQueue");
    removeOldRequests("FastOMSRefundQueue");

    if (toProcessedObject && toProcessedObject.custom.processing === false) {
        Transaction.wrap(function () {
            try {
                toProcessedObject.custom.processing = true;
            } catch (error) {
                FOMSLogger.info("Can't update toProcessedObject processing", error);
            }
        });
        var ocapiResponse = ordersOCAPICall(ocapiCallAddress);
        if (ocapiResponse.error) {
            Transaction.wrap(function () {
                if (ocapiCallAddress === "FastOMSSaveOrder") {
                    order.custom.LOMSPendingSaveOrder = false;
                } else {
                    order.custom.LOMSPendingRefund = false;
                }
            });
            message = ocapiResponse.message ? ocapiResponse.message : Resource.msgf("order.refund.job.ocapi.error.user", "fastoms", null);
            return {
                success: false,
                message: message
            };
        }
    }
    return {
        success: true
    };
}

/**
 * Render the details of order.
 */
exports.ShowOrderDetails = function () {
    if (!params.orderNo.empty) {
        controlUser();

        var orderNo = params.orderNo.stringValue;
        var order = OrderMgr.getOrder(orderNo);
        var continueExecution = true;
        var response = {};

        var result = processSaveOrRefundQueue(order);
        if (!result.success) {
            return renderJSON({
                message: result.message,
                jobExecution: false
            });
        }

        if (!params.jobExecution.empty && params.jobExecution.booleanValue == true) {
            if (order.custom.LOMSPendingSaveOrder || order.custom.LOMSPendingRefund) {
                continueExecution = false;
                renderJSON({
                    jobExecution: true
                });
            } else {
                response = JSON.parse(order.custom.LOMSResponseMessage);
                orderNo = order.custom.LOMSNewOrderJob;
                order = OrderMgr.getOrder(orderNo);
            }
        }

        if (continueExecution) {
            try {
                Transaction.wrap(function () {
                    order.custom.LOMSPendingCalcChang = 0;
                    order.custom.LOMSPendingAddProducts = 0;
                });
            } catch (e) {
                FOMSLogger.error("Can't update LOMSPendingCalcChang", e);
            }
            var urlShippingListAction = new URLAction("FastOMSController-ApplicableShippingMethods", siteID);
            var urlShippingListParameter = new URLParameter("orderNo", params.orderNo.stringValue);
            var urlShippingList = URLUtils.abs(urlShippingListAction, urlShippingListParameter);

            var shipList = getApplicableShippingMethods(order.defaultShipment);
            var priceObj = getValues(order);
            var paymentManager = getPaymentManager(order);
            var addressesList = getAddressesList(order);
            var restrictEdit = (paymentManager != null) ? (paymentManager.Authorize == null) : true;
            var notRefundProducts = [];
            var returningProducts = order.custom.LOMSReturnItems ? JSON.parse(order.custom.LOMSReturnItems).items : null;
            var refundedProducts = order.custom.LOMSRefundedItems ? JSON.parse(order.custom.LOMSRefundedItems) : null;
            var allItemsAlreadyRefunded = false;
            var idsReturn = [];
            var idsRefunded = [];
            var appliedCoupons = [];
            var promotions = getUsedPromotions(order);
            var isGiftCertificateActive = PaymentMgr.getPaymentMethod(dw.order.PaymentInstrument.METHOD_GIFT_CERTIFICATE).isActive();
            var avaliableRefundAmount = (order.paymentInstruments[0].paymentTransaction.getAmount().value - order.custom.LOMSRefundedAmount - priceObj.gifts).toFixed(2);
            var couponInquiryResult = couponManual.checkFastOMSCouponsPresence(order);
            var checkCouponsInBM = checkUsedCouponsInBM(order);
            var transactionNotes;

            if (returningProducts) {
                idsReturn = returningProducts.map(function (e) {
                    return e.pid;
                });
            }
            if (refundedProducts) {
                idsRefunded = refundedProducts.map(function (e) {
                    return e.pid;
                });
            }
            /* eslint-disable no-restricted-syntax */
            for (var key in order.productLineItems) {
                var pli = order.productLineItems[key];
                if (idsReturn.length > 0) {
                    var idReturn = idsReturn.indexOf(pli.productID);
                    if (idReturn != -1 && returningProducts && returningProducts[idReturn].qtd == pli.quantityValue) {
                        notRefundProducts.push(returningProducts[idReturn]);
                    }
                }
                if (idsRefunded.length > 0) {
                    var idRefunded = idsRefunded.indexOf(pli.productID);
                    if (idRefunded != -1 && refundedProducts) {
                        notRefundProducts.push(refundedProducts[idRefunded]);
                    }
                }
            }

            if (order.productLineItems.length == notRefundProducts.length) {
                allItemsAlreadyRefunded = true;
            }
            if (order.couponLineItems.length > 0) {
                for (var couponIdx in order.couponLineItems) {
                    var coupon = order.couponLineItems[couponIdx];
                    if (coupon.valid && coupon.applied) {
                        appliedCoupons.push(coupon.couponCode);
                    }
                }
            }
            /* eslint-enable no-restricted-syntax */
            var isSupportedPayment = !empty(getPaymentManager(order));
            var canPartialReversal = (isSupportedPayment) ? getPaymentManager(order).canPartialReversal() : false;

            // ProductLineItems quantity without bonusProductLineItems
            var productLineItemsQuantity = 0;
            order.productLineItems.toArray().forEach(function (productLineItem) {
                if (!productLineItem.bonusProductLineItem) {
                    productLineItemsQuantity += 1;
                }
            });

            //Payment Instruments Details
            var orderPaymentInstruments = order.getPaymentInstruments().toArray();
            var orderPaymentInstrumentsDetails = orderPaymentInstruments.map(function (orderPaymentInstrument) {
                var amount = orderPaymentInstrument.getPaymentTransaction().getAmount();
                var paymentMethodID = orderPaymentInstrument.getPaymentMethod();
                var paymentMethod = PaymentMgr.getPaymentMethod(paymentMethodID);
                var paymentMethodName = paymentMethod.getName();
                var creditCardType = orderPaymentInstrument.getCreditCardType();
                var maskedCreditCardNumber = orderPaymentInstrument.getMaskedCreditCardNumber();
                return ({
                    amount: amount,
                    paymentMethodName: paymentMethodName,
                    creditCardType: creditCardType,
                    maskedCreditCardNumber: maskedCreditCardNumber
                });
            });

            var paidByMixed = orderPaymentInstruments.length > 1;

            transactionNotes = order.custom.LOMSTransactionNotes;

            ISML.renderTemplate("orderDetails", {
                URLShippingList: urlShippingList,
                siteID: siteID,
                Order: order,
                productLineItemsQuantity: productLineItemsQuantity,
                ShipmentsList: shipList,
                Prices: priceObj,
                avaliableRefundAmount: avaliableRefundAmount,
                AddressesList: addressesList,
                restrictEdit: restrictEdit,
                NotRefundProducts: notRefundProducts,
                AllItemsAlreadyRefunded: allItemsAlreadyRefunded,
                appliedCoupons: appliedCoupons,
                currencyCode: order.getTotalGrossPrice().getCurrencyCode(),
                Options: Helper.getOptions(),
                isSupportedPayment: isSupportedPayment,
                canPartialReversal: canPartialReversal,
                message: response.msg ? response.msg : false,
                promotions: promotions,
                transactionNotes: transactionNotes ? JSON.parse(transactionNotes).notes : {},
                fastOMSHelper: Helper,
                isGiftCertificateActive: isGiftCertificateActive,
                orderPaymentInstrumentsDetails: orderPaymentInstrumentsDetails,
                couponInquiryResult: couponInquiryResult,
                checkCouponsInBM: checkCouponsInBM,
                paidByMixed: paidByMixed
            });
        }
    }
};
exports.ShowOrderDetails.public = true;

exports.ApplicableShippingMethods = function () {
    if (!params.orderNo.empty) {
        var orderNo = params.orderNo.stringValue;
        var order = OrderMgr.getOrder(orderNo);
        var newCurrency = Currency.getCurrency(order.getCurrencyCode());

        session.setCurrency(newCurrency);

        var shipList = getApplicableShippingMethods(order.defaultShipment);
        ISML.renderTemplate("tabs/components/shippingList", {
            Order: order,
            ShipmentsList: shipList
        });
    }
};
exports.ApplicableShippingMethods.public = true;

/**
* Capture orders flow.
*/
exports.CaptureOrder = function () {
    if (!params.orderNo.empty) {
        var order = OrderMgr.getOrder(params.orderNo.value);
        var success = false;
        var paymentManager = getPaymentManager(order);

        if (paymentManager) {
            var result = paymentManager.Capture(order); //eslint-disable-line

            if (result.success) {
                Transaction.wrap(function () {
                    order.setPaymentStatus(dw.order.Order.PAYMENT_STATUS_PAID);
                    addEditionOrRefundNote(order, "CAPTURE");
                    unlockOrder(order);
                });
                success = true;
            } else {
                FOMSLogger.error("CaptureOrder Error", result.errorMsg);
                renderJSON({
                    orderNo: order.orderNo,
                    success: false,
                    responseMsg: result.errorMsg
                });
                return;
            }
        }

        renderJSON({
            success: success,
            orderNo: order.orderNo
        });
    }
};
exports.CaptureOrder.public = true;

/**
 * return OCAPI Auth Token
 */
function getOCAPIAuthToken() {
    var createRequest = function (ocapiService) {
        var base64Credentials = System.getPreferences().getCustom().FastOMSOCAPIAuth;
        updateServiceURL(ocapiService);

        ocapiService.addHeader("Content-Type", "application/x-www-form-urlencoded");
        ocapiService.addHeader("Authorization", "Basic " + base64Credentials);
        ocapiService.setRequestMethod("POST");

        return "grant_type=urn:demandware:params:oauth:grant-type:client-id:dwsid:dwsecuretoken";
    };

    var parseResponse = function (_service, args) {
        return args.text;
    };

    var serviceCallback = {
        createRequest: createRequest,
        parseResponse: parseResponse
    };

    var service = dw.svc.LocalServiceRegistry.createService("FastOMSAuth", serviceCallback);

    var response = service.call();

    var responseObj = JSON.parse(response.object);

    if (responseObj) {
        return responseObj.access_token;
    }
    return {
        error: true,
        message: JSON.parse(response.errorMessage).error_description
    };
}

/**
 * return OCAPI Request Body
 */
function getOCAPIRequestBody() {
    var orderObj = JSON.parse(params.order.value);
    var order = OrderMgr.getOrder(orderObj.orderNo);
    var card = Helper.getCardPaymentInstrument(order);
    var base64EncryptKey = Site.getCurrent().getPreferences().getCustom().LOMSEncryptionKey;
    var cipher = new Cipher();
    var cardEncrypted = null;

    if (card && card.creditCardNumber) {
        cardEncrypted = cipher.encrypt(card.creditCardNumber, base64EncryptKey, "AES", null, 1);
    }

    var requestObj = {};
    requestObj.paramDetails = {};
    requestObj.clientDetails = {};
    requestObj.description = params.description.stringValue;
    requestObj.refundProducts = JSON.parse(params.products.stringValue);
    requestObj.products = orderObj.products;
    requestObj.changes = orderObj.changes;

    if (cardEncrypted) {
        requestObj.paramDetails.card = cardEncrypted;
    }

    requestObj.paramDetails.credit = params.credit.doubleValue;
    requestObj.paramDetails.gift = params.gift.doubleValue;
    requestObj.paramDetails.paypal = params.paypal.doubleValue;
    requestObj.paramDetails.originalShipping = params.originalShipping.doubleValue;
    requestObj.paramDetails.shipTax = params.shipTax.doubleValue;
    requestObj.paramDetails.reason = (!params.reason.empty) ? params.reason.stringValue : Resource.msgf("fastoms.refunddescription", "fastoms", null, orderObj.orderNo);
    requestObj.paramDetails.creditCardAvailable = params.creditCardAvailable.doubleValue;
    requestObj.paramDetails.refundGift = params.refundGift.value;
    requestObj.paramDetails.refundValue = params.credit.doubleValue + params.gift.doubleValue + params.paypal.doubleValue;
    requestObj.paramDetails.nonGiftRefund = params.credit.doubleValue == 0.0 ? params.paypal.doubleValue : params.credit.doubleValue;
    requestObj.paramDetails.refundShipping = (params.shipping && params.shipping.booleanValue) ? params.shipping.booleanValue : false;
    requestObj.paramDetails.oldOrderNo = params.orderNo.stringValue;
    requestObj.paramDetails.addProductsPidAdd = (!params.pidAdd.empty) ? params.pidAdd.stringValue : null;
    requestObj.paramDetails.addProductsPidRemove = (!params.pidRemove.empty);
    requestObj.paramDetails.pidRemove = (!params.pidRemove.empty) ? params.pidRemove.stringValue : null;

    for (var key in orderObj) {
        if (key != "products" && key != "changes") {
            requestObj.clientDetails[key] = orderObj[key];
        }
    }

    return {
        body: JSON.stringify(requestObj),
        orderNo: orderObj.orderNo
    };
}

function addOcapiRequestToQueue(objectType) {
    var request = getOCAPIRequestBody();
    FOMSLogger.info("Job queue parameters", request);
    var queueObject = CustomObjectMgr.getCustomObject(objectType, request.orderNo);

    if (queueObject) {
        Transaction.wrap(function () {
            queueObject.custom.requestBody = request.body;
        });
    } else {
        Transaction.wrap(function () {
            queueObject = CustomObjectMgr.createCustomObject(objectType, request.orderNo);
            queueObject.custom.requestBody = request.body;
            queueObject.custom.processing = false;
            FOMSLogger.info("Created queue object custom orderNo: " + queueObject.custom.orderNo + " Object type: " + objectType);
        });
    }

    return queueObject;
}


/**
 * OCAPI call
 */
function ordersOCAPICall(serviceName, requestBody) {
    var authToken = getOCAPIAuthToken();
    var createRequest = function (ocapiService) {
        ocapiService.addHeader("Content-Type", "application/json");
        ocapiService.addHeader("Authorization", "Bearer " + authToken);
        ocapiService.setRequestMethod("POST");
        if (requestBody) {
            return requestBody;
        }
    };

    var parseResponse = function (_service, args) {
        return args.text;
    };

    var serviceCallback = {
        createRequest: createRequest,
        parseResponse: parseResponse
    };

    if (!authToken || authToken.error) {
        return {
            error: true,
            message: authToken.message
        };
    }

    var service = dw.svc.LocalServiceRegistry.createService(serviceName, serviceCallback);
    updateServiceURL(service);

    var resp = service.call();

    try {
        if (resp.errorMessage && JSON.parse(resp.errorMessage).fault.type === "JobAlreadyRunningException") {
            FOMSLogger.info("Multiple run of job");
            return {
                error: false
            };
        }
    } catch (error) {
        return {
            error: true
        };
    }

    return resp;
}

/**
 * Save the Current Order or Create a New One
 */
exports.SaveOrder = function () {
    if (!params.order.empty) {
        var orderObj = JSON.parse(params.order.value);
        var order = OrderMgr.getOrder(orderObj.orderNo);
        var success = false;
        var newOrderNo = order.orderNo;
        var paidWithGift = false;
        var jobExecution = false;
        var currentValue = order.getTotalGrossPrice().getDecimalValue();
        var message = null;
        var newValue = parseFloat(orderObj.orderTotal);

        if (!orderObj.billing.countryCode || orderObj.billing.countryCode.length === 0) {
            orderObj.billing.countryCode = order.billingAddress.countryCode;
        }

        if (!orderObj.shipping.countryCode || orderObj.shipping.countryCode.length === 0) {
            orderObj.shipping.countryCode = order.shipments[0].shippingAddress.countryCode;
        }

        var needNewOrder = false;

        if (orderObj.changes) {
            if (orderObj.changes.added || orderObj.changes.removed || orderObj.changes.variant || orderObj.changes.couponCode) {
                needNewOrder = true;
            }
        }

        if (currentValue == newValue && !needNewOrder) {
            Transaction.wrap(function () {
                updateOrder(order, orderObj);
                addEditionOrRefundNote(order, "EDIT", orderObj);
                unlockOrder(order);
                success = true;
            });
        } else if (!empty(order.giftCertificatePaymentInstruments) && order.paymentInstruments.length == 1) {
            paidWithGift = true;
        } else {
            Transaction.wrap(function () {
                order.custom.LOMSPendingSaveOrder = true;
                order.custom.LOMSResponseMessage = "";
            });

            addOcapiRequestToQueue("FastOMSSaveOrderQueue");

            newOrderNo = 0;
            jobExecution = true;
            success = true;
        }

        renderJSON({
            success: success,
            paidWithGift: paidWithGift,
            orderNo: newOrderNo,
            jobExecution: jobExecution,
            oldOrderNo: orderObj.orderNo,
            message: message
        });
    }
};
exports.SaveOrder.public = true;

/**
 * Refund Order
 */
exports.RefundOrder = function () {
    if (!params.orderNo.empty) {
        var order = OrderMgr.getOrder(params.orderNo.stringValue);
        var orderStatus = order.getStatus().getValue();
        if (orderStatus === dw.order.Order.ORDER_STATUS_CANCELLED) {
            renderJSON({
                success: false,
                message: Resource.msgf("order.status.alreadyCanceled", "fastoms", null, order.getOrderNo()),
            });
            return;
        }

        var refundValue = params.credit.doubleValue + params.gift.doubleValue + params.paypal.doubleValue;
        var nonGiftRefund = params.credit.doubleValue == 0.0 ? params.paypal.doubleValue : params.credit.doubleValue;
        var products = JSON.parse(params.products.stringValue);
        var orderObject = JSON.parse(params.order);
        var paymentManager = getPaymentManager(order);
        var returnItems = [];
        var refundedItems = [];
        var calendarNow = new Calendar();
        var nowValue = 0;
        var futureValue = 0;
        var success = false;
        var prices = getValues(order);
        var jobSuccess = true;
        var jobExecution = false;
        var newOrderNo = null;
        var message = null;
        var refundingNow = {};
        var refundShipping = (params.shipping && params.shipping.booleanValue) ? params.shipping.booleanValue : false;
        if (refundShipping) {
            nowValue = nowValue + params.originalShipping.doubleValue + params.shipTax.doubleValue;
        }
        // Check if will be returning items and calculate how much will be paid immediately
        // and how much will be paid after the return confirmation
        if (products.length > 0) {
            /* eslint-disable no-restricted-syntax */
            for (var keyPli in order.productLineItems) {
                var pli = order.productLineItems[keyPli];
                var idx = products.map(function (e) {
                    return e.pid;
                }).indexOf(pli.productID);

                if (idx != -1) {
                    var prod = products[idx];
                    var base = (pli.adjustedGrossPrice.value / pli.quantityValue);
                    var qtd = prod.qtd;
                    if (prod.ret) {
                        futureValue += (base * prod.qtd);
                        prod.val = base;
                        prod.ret = false;
                        prod.initiation_date = StringUtils.formatCalendar(calendarNow, "MM/dd/YYYY");
                        var returnedQtd = qtd;
                        while (returnedQtd > 0) {
                            returnItems.push(prod);
                            returnedQtd--;
                        }
                    } else {
                        nowValue += base * prod.qtd;
                    }
                    refundingNow[pli.productID] = qtd;
                    prod.qtd = 1;
                    while (qtd > 0) {
                        refundedItems.push(prod);
                        qtd--;
                    }
                }
            }
            /* eslint-enable no-restricted-syntax */
        }

        nowValue = parseFloat(nowValue.toFixed(2));
        futureValue = parseFloat(futureValue.toFixed(2));

        // Refund Without Return
        if (!returnItems.length) {
            // non-gift refund (Paypal or Credit)
            if (nonGiftRefund > 0 && paymentManager) {
                var responseRefund = paymentManager.Refund(order, Number(nonGiftRefund).toFixed(2));
                if (responseRefund.success) {
                    Transaction.wrap(function () {
                        var oldRefundedAmount = !empty(order.custom.LOMSRefundedAmount) ? Number(order.custom.LOMSRefundedAmount) : 0;
                        if (nonGiftRefund > 0) {
                            order.custom.LOMSRefundedAmount = (oldRefundedAmount + Number(nonGiftRefund)).toFixed(2);
                        }
                    });
                } else {
                    FOMSLogger.error("\nOrder No: " + order.getOrderNo() + "\nPayPal Refund Error: " + responseRefund.errorMsg);
                    renderJSON({
                        success: false,
                        orderNo: order.orderNo,
                        jobSuccess: true,
                        message: responseRefund.errorMsg,
                        oldOrderNo: order.orderNo,
                        jobExecution: true
                    });
                    return {
                        error: true,
                        errorMsg: responseRefund.errorMsg
                    };
                }
            }

            // check if all products are already refunded
            var refundedProducts = order.custom.LOMSRefundedItems ? JSON.parse(order.custom.LOMSRefundedItems) : null;
            var currentProducts = refundedItems.length;
            var idsRefunded = [];
            var orderItemQtd = 0;
            var notRefundItemQtd = 0;
            if (refundedProducts) {
                idsRefunded = refundedProducts.map(function (e) {
                    return e.pid;
                });
            }
            /* eslint-disable no-restricted-syntax */
            for (var keyPliReturn in order.productLineItems) {
                var pliReturn = order.productLineItems[keyPliReturn];
                orderItemQtd += pliReturn.quantityValue;
                if (idsRefunded.length > 0) {
                    var idRefunded = idsRefunded.indexOf(pliReturn.productID);
                    if (idRefunded != -1 && refundedProducts) {
                        notRefundItemQtd += pliReturn.quantityValue;
                    } else {
                        notRefundItemQtd += (pliReturn.quantityValue - refundingNow[pliReturn.productID]);
                    }
                }
            }

            var bonusProductsQuantity = getTotalBonusProductQuantity(order);

            /* eslint-enable no-restricted-syntax */
            // Total Refund
            if (refundValue >= (prices.products + prices.shipping).toFixed(2) || orderItemQtd == (notRefundItemQtd + currentProducts + bonusProductsQuantity)) {
                success = true;
                newOrderNo = order.orderNo;
                orderCancelation(order, params);

                if (params.gift.doubleValue) {
                    createGiftCertificateWithJob(newOrderNo, params.gift.doubleValue);
                }

                Transaction.wrap(function () {
                    if (refundShipping) {
                        order.custom.LOMSShippingRefunded = true;
                    }
                    unlockOrder(order);
                });
                // Partial Refund
            } else if (refundShipping && (nowValue == params.originalShipping.doubleValue
                || (nowValue - params.shipTax.doubleValue) == params.originalShipping.doubleValue)) {
                // in this case just the shipping value is refunded
                Transaction.begin();
                order.custom.LOMSShippingRefunded = true;
                removeShipmentValue(order);
                newOrderNo = order.orderNo;
                success = true;
                Transaction.commit();
            } else {
                Transaction.wrap(function () {
                    order.custom.LOMSPendingRefund = true;
                });
                addOcapiRequestToQueue("FastOMSRefundQueue");
                newOrderNo = 0;
                success = true;
                jobExecution = true;
            }
            // Refund With Return
        } else {
            // Prepare the Return Items
            if (returnItems.length > 0) {
                var returnObj = {
                    orderNo: order.orderNo,
                    reason: params.reason.stringValue,
                    total: refundValue,
                    shipping: params.originalShipping.doubleValue + params.shipTax.doubleValue,
                    paid: nowValue,
                    refund: futureValue,
                    credit: nonGiftRefund,
                    totalAvailableCredit: params.creditCardAvailable.doubleValue,
                    gift: params.gift.doubleValue,
                    items: returnItems,
                    shippingPending: refundShipping
                };

                Transaction.wrap(function () {
                    order.custom.LOMSPendingReturn = true;
                    order.custom.LOMSReturnItems = JSON.stringify(returnObj);
                    addEditionOrRefundNote(order, "PENDINGRETURN", orderObject);
                });
                sendEmail("mail/returnItemStepsEmail", order.customerEmail, Resource.msgf("mail.refund.return.steps", "fastoms", null, siteName), { order: order });
            }
            success = true;
            newOrderNo = order.orderNo;
        }

        /* eslint-disable block-scoped-var */
        renderJSON({
            success: success,
            orderNo: newOrderNo,
            jobSuccess: jobSuccess,
            message: message,
            oldOrderNo: params.orderNo.stringValue,
            jobExecution: jobExecution
        });
        /* eslint-enable block-scoped-var */
    }
};
exports.RefundOrder.public = true;

/**
 * Manage Returning Items
 */
exports.ManageReturn = function () {
    if (!params.orderNo.empty) {
        var order = OrderMgr.getOrder(params.orderNo.stringValue);
        var returnItems = JSON.parse(order.custom.LOMSReturnItems);
        var orderObj = JSON.parse(params.order);
        var calendarNow = new Calendar();
        var respArray = JSON.parse(params.resp.stringValue);
        var paymentManager = getPaymentManager(order);
        var count = 0;
        var jobExecution; var success; var message; var
            newOrderNo;
        var jobSuccess = true;
        var confirmationEmailNotes = "";
        var cancelEmailNotes = "";
        var returnTracking = [];

        respArray.map(function (element, index) {
            if (element.confirm) {
                return returnItems.items[index].ret = true;
            } else if (element.cancel) {
                return returnItems.items[index].ret = false;
            }
        });

        Transaction.wrap(function () {
            unlockOrder(order);
        });

        for (var idx = respArray.length - 1; idx >= 0; idx--) {
            returnItems.items[idx].tracking = respArray[idx].tracking;
            returnItems.items[idx].carrier = respArray[idx].carrier;
            returnItems.items[idx].refunded = false;
            if (!empty(respArray[idx].tracking)) {
                returnTracking.push(respArray[idx].tracking);
            }
            if (!returnItems.items[idx].initiation_date) {
                returnItems.items[idx].initiation_date = StringUtils.formatCalendar(calendarNow, "MM/dd/YYYY");
            }
            if (returnItems.items[idx].ret) {
                returnItems.items[idx].ret = true;
                count++;
                confirmationEmailNotes += "\n" + Resource.msgf("order.return.confirmitem", "fastoms", null, ProductMgr.getProduct(returnItems.items[idx].pid).name);
            } else if (respArray[idx].cancel) {
                cancelEmailNotes += "\n" + Resource.msgf("order.return.cancelitem", "fastoms", null, ProductMgr.getProduct(returnItems.items[idx].pid).name);
                returnItems.items.splice(idx, 1);
            }
        }

        if (confirmationEmailNotes != "" || cancelEmailNotes != "") {
            sendEmail("mail/returnConfirmationEmail", order.customerEmail, Resource.msgf("mail.return.confirm.subject", "fastoms", null, siteName), { order: order, confirmationEmailNotes: confirmationEmailNotes, cancelEmailNotes: cancelEmailNotes });
        }

        if (returnTracking.length > 1) {
            returnTracking = returnTracking.join(",");
        }

        Transaction.wrap(function () {
            order.custom.LOMSReturnItems = JSON.stringify(returnItems);
            order.custom.LOMSPendingReturn = (returnItems.items.length != count);
            order.custom.LOMSReturnTracking = returnTracking;
            order.custom.LOMSShippingRefunded = returnItems.shippingPending;
        });

        //Still some products need to be process according to cancel or confirm.
        if (order.custom.LOMSPendingReturn) {
            Transaction.wrap(function () {
                addEditionOrRefundNote(order, "CONFIRMRETURN", orderObj);
            });
            renderJSON({
                success: true,
                message: Resource.msg("order.return.info", "fastoms", null),
                orderLocked: order.custom.orderLock
            });
            return true;
        }

        //If all products were canceled return no need to do anything
        if (empty(returnItems.items)) {
            Transaction.wrap(function () {
                addEditionOrRefundNote(order, "CONFIRMRETURN", orderObj);
            });
            renderJSON({
                success: true,
                jobExecution: false,
                jobSuccess: true,
                orderNo: order.getOrderNo()
            });
            return;
        }

        //Determine Requested Amounts (Fixed by 2)
        var toRefundedMoneyAmount = +Number(returnItems.credit).toFixed(2);
        var toRefundedGiftAmount = +Number(returnItems.gift).toFixed(2);

        //Create gift refund if it's exist
        if (toRefundedGiftAmount > 0) {
            createGiftCertificate(order, toRefundedGiftAmount, orderObj);
            Transaction.wrap(function () {
                addEditionOrRefundNote(order, "CONFIRMRETURN", orderObj);
            });
        }

        //Create credit refund if it's exist
        if (paymentManager && toRefundedMoneyAmount > 0) {
            var responseRefund = paymentManager.Refund(order, toRefundedMoneyAmount);
            if (!responseRefund.success) {
                FOMSLogger.error(responseRefund.errorMsg);
                renderJSON({
                    success: false,
                    orderNo: order.orderNo,
                    jobSuccess: jobSuccess,
                    message: responseRefund.errorMsg,
                    oldOrderNo: order.orderNo,
                    jobExecution: true,
                    orderLocked: order.custom.orderLock
                });
                return {
                    error: true,
                    errorMsg: responseRefund.errorMsg
                };
            } else {
                Transaction.wrap(function () {
                    var oldRefundedAmount = !empty(order.custom.LOMSRefundedAmount) ? Number(order.custom.LOMSRefundedAmount) : 0;
                    if (toRefundedMoneyAmount > 0) {
                        order.custom.LOMSRefundedAmount = (oldRefundedAmount + Number(toRefundedMoneyAmount)).toFixed(2);
                    }
                });
                sendEmail("mail/orderRefundEmail", order.customerEmail, Resource.msgf("mail.refund.subject", "fastoms", null, siteName), { order: order });
            }
        }

        Transaction.wrap(function () {
            order.custom.LOMSPendingRefund = true;
        });

        //Cancel Order if all products in order has already confirmed
        var bonusProductsQuantity = getTotalBonusProductQuantity(order);
        if (order.productQuantityTotal === count + bonusProductsQuantity) {
            orderCancelation(order, params);
            Transaction.wrap(function () {
                order.custom.LOMSPendingRefund = false;
            });
            renderJSON({
                success: true,
                message: Resource.msg("order.refund.info", "fastoms", null),
                orderLocked: order.custom.orderLock
            });
            return;
        }

        addOcapiRequestToQueue("FastOMSRefundQueue");
        jobExecution = true;
        newOrderNo = 0;
        success = true;

        renderJSON({
            success: success,
            jobExecution: jobExecution,
            orderNo: newOrderNo,
            jobSuccess: jobSuccess,
            oldOrderNo: order.orderNo,
            responseMsg: message,
            orderLocked: order.custom.orderLock
        });
    }
};
exports.ManageReturn.public = true;

/**
 * Cancel Order
 */
exports.CancelOrder = function () {
    if (params.orderNo.empty || params.orderCancelDescription.empty) {
        return;
    }
    var orderNo = params.orderNo.stringValue;
    var cancelWithRefund = params.cancelWithRefund.booleanValue;
    var orderCancelDescription = params.orderCancelDescription.stringValue;
    var order = OrderMgr.getOrder(orderNo);
    var usedCouponCodes = order.getCouponLineItems().toArray().map(function (couponLineItem) {
        return couponLineItem.getCouponCode();
    });

    Transaction.begin();

    var orderCancelationResult = OrderMgr.cancelOrder(order);
    if (orderCancelationResult.status === Status.OK) {
        order.setCancelCode(Order.ORDER_STATUS_CANCELLED);
        order.setCancelDescription(orderCancelDescription);
    } else {
        Transaction.rollback();
        return renderJSON({
            success: false,
            errorMsg: orderCancelationResult.message
        });
    }

    var paymentManager = getPaymentManager(order);
    if (cancelWithRefund && paymentManager) {
        var paymentManagerResponse = paymentManager.Cancel(order);
        if (!paymentManagerResponse.success) {
            Transaction.rollback();
            return renderJSON({
                success: false,
                errorMsg: paymentManagerResponse.errorMsg
            });
        }
    }

    Transaction.commit();

    var giftCertificatePayments = order.getPaymentInstruments(PaymentInstrument.METHOD_GIFT_CERTIFICATE);
    var isGiftCertificateUsed = !empty(giftCertificatePayments);

    if (isGiftCertificateUsed && cancelWithRefund) {
        createGiftCertificateWithJob(orderNo);
    }

    sendEmail("mail/orderCancellationEmail", order.customerEmail, Resource.msgf("mail.cancel.subject", "fastoms", null, siteName), { order: order, coupons: usedCouponCodes.join() });

    return renderJSON({
        success: true,
        errorMsg: null
    });
};
exports.CancelOrder.public = true;

/**
 * Typeahead Return Function
 */
exports.TypeaheadOrders = function () {
    var ret = [];
    if (!params.query.empty) {
        var search = params.query.stringValue;
        var queryStr = "orderNo LIKE {0} OR customerName ILIKE {0} OR customerEmail ILIKE {0}";
        var orders = OrderMgr.queryOrders(queryStr, "creationDate desc", "*" + search + "*");

        var ordersList = orders.asList(0, 7);
        /* eslint-disable no-restricted-syntax */
        for (var key in ordersList) {
            var order = ordersList[key];
            ret.push({
                customerName: order.customerName,
                customerEmail: order.customerEmail,
                orderNo: order.orderNo,
                creationDate: order.creationDate,
                total: order.totalGrossPrice.value,
                status: order.status.displayValue
            });
        }
        /* eslint-enable no-restricted-syntax */
    }

    renderJSON({
        empty: ret.length === 0,
        orders: ret
    });
};
exports.TypeaheadOrders.public = true;

/**
 * Calculate the Order after changes
 */
exports.CalculateChanges = function () {
    if (!params.order.empty) {
        var orderObj = JSON.parse(params.order.value);
        var order = OrderMgr.getOrder(orderObj.orderNo);
        var response = {};

        if (order) {
            if (empty(order.custom.LOMSPendingCalcChang) || order.custom.LOMSPendingCalcChang == 0) {
                Transaction.wrap(function () {
                    order.custom.LOMSPendingCalcChang = 1;
                });

                addOcapiRequestToQueue("FastOMSCalcChangQueue");
                ordersOCAPICall("FastOMSCalcChang");
            }

            if (order.custom.LOMSPendingCalcChang == 1) {
                renderJSON({
                    success: true,
                    jobWaiting: true
                });
            }

            if (order.custom.LOMSPendingCalcChang == 2) {
                Transaction.wrap(function () {
                    order.custom.LOMSPendingCalcChang = 0;
                });

                var calculateChangesObject = CustomObjectMgr.queryCustomObjects("FastOMSCalcChangQueue", "", "creationDate asc").first();
                if (!empty(calculateChangesObject)) {
                    ordersOCAPICall("FastOMSCalcChang");
                }

                response = JSON.parse(order.custom.LOMSResponseMessage);

                renderJSON({
                    success: !empty(response.success) ? response.success : true,
                    jobWaiting: false,
                    totals: JSON.parse(order.custom.LOMSCalcChangObject),
                    responseMsg: response.msg ? response.msg.toString() : false
                });
            }
        }
    }
};
exports.CalculateChanges.public = true;

/**
 * Check if the coupon code is valid
 */
exports.ValidateCouponCode = function () {
    //Get coupon
    var submittedCouponCode = params.couponCode.stringValue;
    var coupon = CouponMgr.getCouponByCode(submittedCouponCode);

    //If order has already coupon, prevent adding more coupon
    var orderNo = params.orderNo.stringValue;
    var order = OrderMgr.getOrder(orderNo);
    if (!empty(order.couponLineItems)) {
        renderJSON({
            success: false,
            message: Resource.msg("fastoms.coupon.limitation.error", "fastoms", null)
        });
        return;
    }

    //If coupon is not exist
    if (!coupon) {
        renderJSON({
            success: false,
            message: Resource.msg("fastoms.invalidcoupon", "fastoms", null)
        });
        return;
    }

    if (!coupon.enabled) {
        renderJSON({
            success: false,
            message: Resource.msg("fastoms.disabledcoupon", "fastoms", null)
        });
        return;
    }

    //Check is there any active promotion for coupon code
    var promotions = coupon.getPromotions();
    var isAnyPromotionAssignedToCoupon = false;
    if (!empty(promotions)) {
        promotions = promotions.toArray();
        promotions.forEach(function (promotion) {
            if (promotion.isActive()) {
                isAnyPromotionAssignedToCoupon = true;
            }
        });
    }

    if (!isAnyPromotionAssignedToCoupon) {
        renderJSON({
            success: false,
            message: Resource.msgf("fastoms.coupon.error", "fastoms", null, submittedCouponCode)
        });
        return;
    }

    var couponID = coupon.getID();
    var couponRedemptionLimitPerCode = coupon.getRedemptionLimitPerCode();
    var couponRedemptionLimitPerCustomer = coupon.getRedemptionLimitPerCustomer();

    //Search redemtion limit per code
    var couponRedemptions = CouponMgr.getRedemptions(couponID, submittedCouponCode);
    var couponUsageCountsViaCode = couponRedemptions.length;

    if (couponRedemptionLimitPerCode && couponRedemptionLimitPerCode === couponUsageCountsViaCode) {
        renderJSON({
            success: false,
            message: Resource.msg("fastoms.redemption.code.error", "fastoms", null)
        });
        return;
    }

    //Search redemption limit per customer
    var customer = order.getCustomer();
    var orderCustomerEmail = customer.isRegistered() ? customer.getProfile().getEmail() : order.getCustomerEmail();
    var customerRedemptionsSearchResults = couponAutomation.searchCouponRedemptions(orderCustomerEmail);
    var couponUsageCountsViaCustomer = 0;

    if (customerRedemptionsSearchResults.ok) {
        var searchHits = customerRedemptionsSearchResults.object.hits;
        if (!empty(searchHits)) {
            searchHits.forEach(function (searchHit) {
                if (searchHit.coupon_id === couponID) {
                    couponUsageCountsViaCustomer += 1;
                }
            });
        }
    } else {
        renderJSON({
            succes: false,
            message: Resource.msg("fastoms.redemption.searchservice.error", "fastoms", null)
        });
        return;
    }

    if (couponRedemptionLimitPerCustomer && couponRedemptionLimitPerCustomer === couponUsageCountsViaCustomer) {
        renderJSON({
            success: false,
            message: Resource.msg("fastoms.redemption.customer.error", "fastoms", null)
        });
        return;
    }

    renderJSON({
        success: true,
        message: null
    });
};
exports.ValidateCouponCode.public = true;

exports.ValidateGiftCertificate = function () {
    var sumittedGiftCertCode = params.giftCardCode.stringValue;
    var resp = {
        success: false,
        message: null
    };

    var giftCertificate = GiftCertificateMgr.getGiftCertificateByCode(sumittedGiftCertCode);
    if (!empty(giftCertificate) && giftCertificate.enabled && (giftCertificate.getStatus() == giftCertificate.STATUS_ISSUED || giftCertificate.getStatus() == giftCertificate.STATUS_PARTIALLY_REDEEMED)) {
        resp.success = true;
        resp.amount = giftCertificate.amount.value;
        resp.balance = giftCertificate.balance.value + giftCertificate.balance.currencyCode;
        resp.currency = giftCertificate.balance.currencyCode;
    } else if (!empty(giftCertificate) && giftCertificate.getStatus() == giftCertificate.STATUS_REDEEMED) {
        resp.message = Resource.msg("fastoms.redeemedgiftcertificate", "fastoms", null);
    } else {
        resp.message = Resource.msg("fastoms.invalidgiftcertificate", "fastoms", null);
    }

    renderJSON(resp);
};
exports.ValidateGiftCertificate.public = true;

// Check to added product, is it duplicate of existed product
exports.CheckDuplicateProduct = function () {
    var productid = params.pidAdd.stringValue;
    var orderObject = JSON.parse(params.order.stringValue);

    var orderProducts = orderObject.products.map(function (product) {
        return product.pid;
    });

    var result = getProductVariantion(productid, orderProducts);
    var resp = {
        success: true,
        message: null,
        pid: result.product ? result.product.ID : null
    };

    if (!result.product) {
        if (result.isProductInBasket) {
            resp.message = Resource.msg("fastoms.duplicateproduct", "fastoms", null);
        } else if (!result.isInStock) {
            resp.message = Resource.msg("product.notavailable", "fastoms", null);
        }
        resp.success = false;
    }

    renderJSON(resp);
};
exports.CheckDuplicateProduct.public = true;

/**
 * Calculate the Order after changes
 */
exports.AddProduct = function () {
    if (!params.order.empty) {
        var orderObj = JSON.parse(params.order.value);
        var order = OrderMgr.getOrder(orderObj.orderNo);

        if (!order.custom.LOMSPendingAddProducts) {
            Transaction.wrap(function () {
                order.custom.LOMSPendingAddProducts = 0;
            });
        }

        if (order.custom.LOMSPendingAddProducts == 0) {
            Transaction.wrap(function () {
                order.custom.LOMSPendingAddProducts = 1;
            });

            addOcapiRequestToQueue("FastOMSAddProductsQueue");
            ordersOCAPICall("FastOMSAddProducts");

            renderJSON({
                success: true,
                jobWaiting: true
            });
        } else if (order.custom.LOMSPendingAddProducts == 1) {
            renderJSON({
                success: true,
                jobWaiting: true
            });
        } else {
            Transaction.wrap(function () {
                order.custom.LOMSPendingAddProducts = 0;
            });

            var addProductObject = CustomObjectMgr.queryCustomObjects("FastOMSAddProductsQueue", "", "creationDate asc").first();
            if (!empty(addProductObject)) {
                ordersOCAPICall("FastOMSAddProducts");
            }

            var addPrdouctsResponseObj = JSON.parse(order.custom.LOMSAddPorductsObject);

            renderJSON({
                success: addPrdouctsResponseObj.success,
                jobWaiting: false,
                newTR: addPrdouctsResponseObj.success ? addPrdouctsResponseObj.newTR : null,
                responseMsg: !addPrdouctsResponseObj.success ? addPrdouctsResponseObj.msg : null,
                totals: addPrdouctsResponseObj.totals
            });
        }
    }
};
exports.AddProduct.public = true;

/**
 * Update a Product Variant
 */
exports.UpdateVariant = function () {
    if (!params.pid.empty && !params.variants.empty) {
        var orderObject = JSON.parse(params.order);
        var orderObjectProducts = orderObject.products;
        var product = ProductMgr.getProduct(params.pid.stringValue);
        var variants = JSON.parse(params.variants.stringValue);
        var variationModel = product.masterProduct.variationModel;
        var valid = false;
        var basePrice = null;
        var message = null;
        var pid = null;
        var productQtd = null;

        orderObjectProducts.forEach(function (productObject) {
            if (productObject.pid == params.pid.stringValue) {
                return productQtd = Number(productObject.qtd);
            }
        });

        /* eslint-disable no-restricted-syntax */
        for (var key in variants) {
            var vrnt = variants[key];
            var allPVA = variationModel.getProductVariationAttributes();
            var pvaAttributeID = null;
            for (var keyPVA in allPVA) {
                var tempPVA = allPVA[keyPVA];
                if (tempPVA.attributeID == vrnt.type) {
                    pvaAttributeID = tempPVA.ID;
                    break;
                }
            }
            variationModel.setSelectedAttributeValue(pvaAttributeID, vrnt.value);
        }
        /* eslint-disable no-restricted-syntax */

        if (variationModel.selectedVariant) {
            valid = true;
            basePrice = variationModel.selectedVariant.priceModel.price.value;
            pid = variationModel.selectedVariant.ID;
            var image = DIS.getImages(pid, "small");
            var ats = variationModel.selectedVariant.availabilityModel.inventoryRecord.getATS().value;

            if (productQtd > ats) {
                pid = null;
                message = Resource.msgf("fastoms.limitinventory.value", "fastoms", null, ats);
            }

            for (var productIndex in orderObjectProducts) {
                var orderObjectProductID = orderObjectProducts[productIndex].pid;
                if (orderObjectProductID === variationModel.selectedVariant.ID) {
                    pid = null;
                    message = Resource.msg("fastoms.duplicatevariationproduct", "fastoms", null);
                    break;
                }
            }
        }

        renderJSON({
            success: true,
            valid: valid,
            price: basePrice,
            pid: pid,
            message: message,
            image: image,
            ats: ats
        });
    }
};
exports.UpdateVariant.public = true;

/**
 * Save Notes to the Current Order
 */
exports.AddNote = function () {
    if (!params.orderNo.empty) {
        var order = OrderMgr.getOrder(params.orderNo.stringValue);

        Transaction.wrap(function () {
            order.custom.LOMSNotes = params.notes.stringValue;
        });

        renderJSON({
            success: true
        });
    }
};
exports.AddNote.public = true;

/**
 * Retrives the list of a customer's addresses
 */
function getAddressesList(order) {
    var customerAddresses = new dw.util.ArrayList();

    if (!order.customer.anonymous) {
        /* eslint-disable no-restricted-syntax */
        for (var key in order.customer.profile.addressBook.addresses) {
            var address = order.customer.profile.addressBook.addresses[key];
            customerAddresses.add(
                {
                    ID: address.ID,
                    shippingFirstName: address.firstName,
                    shippingLastName: address.lastName,
                    shippingAddress1: address.address1,
                    shippingAddress2: address.address2,
                    shippingCity: address.city,
                    shippingStateCode: address.stateCode,
                    shippingPostalCode: address.postalCode,
                    shippingCountryCode: address.countryCode.value,
                    shippingPhone: address.phone
                }
            );
        }
        /* eslint-enable no-restricted-syntax */
    }

    return customerAddresses;
}

/**
 * Prepare Query String for Search
 */
function prepareQueryString(obj) {
    var query = "";
    /* eslint-disable no-restricted-syntax */
    for (var key in obj) {
        var value = obj[key];
        if (!empty(value)) {
            if (query != "") {
                query += " AND ";
            }
            var item = (key != "status") ? "'*" + value.replace(/'/g, "") + "*'" : value;
            query += (key != "status") ? key + " ILIKE " + item : key + " = " + item;
        }
    }
    /* eslint-enable no-restricted-syntax */
    return query;
}

/**
 * Query Products
 * @param q Query value for product name or ID
 * @returns JSON response with up to seven suggestions according to inputted query
 */
exports.QueryProducts = function () {
    var suggestModel = new SuggestModel();
    var searchPhrase = request.httpParameterMap.q.stringValue;
    /* eslint-disable indent */
    var responseObj = {
        available: false
    };
    /* eslint-enable indent */

    suggestModel.setSearchPhrase(searchPhrase);
    suggestModel.setMaxSuggestions(10);

    var suggestions = suggestModel.getProductSuggestions();
    if (!suggestions) {
        return responseObj;
    }

    var responseList = suggestions.getSuggestedProducts().asList();
    var products = [];

    for (var i = 0; i < responseList.length; i++) {
        var suggestion = responseList[i];
        var suggestedProduct = suggestion.productSearchHit.product;
        var availabilityModel = suggestedProduct.availabilityModel;
        var availableQuantity = 0;
        if (availabilityModel.availability != 1) {
            if (suggestedProduct.master) {
                for (var j = 0; j < suggestedProduct.variants.length; j++) {
                    availableQuantity += suggestedProduct.variants[j].availabilityModel.inventoryRecord ? suggestedProduct.variants[j].availabilityModel.inventoryRecord.ATS.value : 0;
                }
            } else {
                availableQuantity = availabilityModel.inventoryRecord ? availabilityModel.inventoryRecord.ATS.value : 0;
            }
        } else {
            availableQuantity = "unlimited";
        }
        products.push({
            display: suggestion.productSearchHit.product.name,
            sku: suggestion.productSearchHit.productID,
            variantCount: suggestedProduct.variants.length ? Resource.msgf("product.variant.count", "fastoms", null, suggestedProduct.variants.length) : "",
            availability: {
                orderable: availabilityModel.isOrderable(),
                quantity: availableQuantity
            }
        });
    }

    responseObj.available = suggestions.hasSuggestions();
    responseObj.products = products;

    return renderJSON(responseObj);
};
exports.QueryProducts.public = true;

exports.UpdateVariantStock = function () {
    var variantid = params.variantid.stringValue;
    var fixedValue = params.fixedvalue.stringValue;
    var fixedName = params.fixedname.stringValue;
    var product = ProductMgr.getProduct(params.pid.stringValue);
    var fixedAttributes = [];
    var fixedAttribute = {
        ID: fixedName,
        value: fixedValue
    };
    var variationAttribute = {
        attributeID: variantid
    };
    var filteredVariants = null;
    var stockCondition = {};

    // FIXME: allows multiple restrictions though only one is assigned now.
    fixedAttributes.push(fixedAttribute);
    filteredVariants = refreshStock(product, fixedAttributes, variationAttribute);

    for (var key in filteredVariants) {
        var variant = filteredVariants[key];
        if (variant.availabilityModel.inStock || variant.availabilityModel.availabilityStatus === "PREORDER") {
            stockCondition[variant.custom[variantid]] = true;
        }
    }

    return renderJSON(stockCondition);
};
exports.UpdateVariantStock.public = true;

exports.EditOrder = function () {
    var orderNo = params.orderNo.stringValue;
    var order = OrderMgr.getOrder(orderNo);
    var orderLocked = false;
    var responseMsg = "";
    var orderLockJson = order.custom.orderLockJson && JSON.parse(order.custom.orderLockJson);
    var agent = params.agent.stringValue.replace(/([()])/g, "");

    if (order.custom.orderLock == true && orderLockJson && orderLockJson.source !== agent) {
        orderLocked = true;
        var lockObj = JSON.parse(order.custom.orderLockJson);
        responseMsg = Resource.msgf("order.lock.lockedby", "fastoms", null, lockObj.source);
    } else {
        Transaction.wrap(function () {
            lockOrder(order, agent);
        });
    }

    renderJSON({
        orderLocked: orderLocked,
        responseMsg: responseMsg
    });
};
exports.EditOrder.public = true;

exports.CancelEditOrder = function () {
    var orderNo = params.orderNo.stringValue;
    var order = OrderMgr.getOrder(orderNo);

    Transaction.wrap(function () {
        unlockOrder(order);
    });

    renderJSON({
        orderLocked: order.custom.orderLock
    });
};
exports.CancelEditOrder.public = true;

/**
 * update Service URL with current site URL
 */
function updateServiceURL(service) {
    var url = service.getURL();
    var hostName = Site.current.httpsHostName;
    var version = "v" + Site.getCurrent().getPreferences().getCustom().FOMSOCAPIVersion.replace(".", "_");
    var clientId = Site.getCurrent().getPreferences().getCustom().FOMSOCAPIClientID;
    url = url.replace("{hostName}", hostName);
    url = url.replace("{siteID}", siteID);
    url = url.replace("{version}", version);
    url = url.replace("{clientId}", clientId);
    service.setURL(url);
}

function createGiftCertificateWithJob(orderNo, giftValue) {
    var ocapiRequestBody = {
        "parameters": [
            {
                name: "orderNo",
                value: orderNo
            }
        ]
    };

    if (giftValue) {
        giftValue = giftValue.toString();
        ocapiRequestBody.parameters.push({
            name: "giftVal",
            value: giftValue
        });
    }

    ordersOCAPICall("FastOMSCreateGiftCertificateForRefund", JSON.stringify(ocapiRequestBody));
}

function getTotalBonusProductQuantity(order) {
    var totalBonusProductQuantity = 0;
    var bonusProductLineItems = order.getBonusLineItems();

    if (bonusProductLineItems.length) {
        bonusProductLineItems.toArray().forEach(function (bonusProductLineItem) {
            totalBonusProductQuantity += bonusProductLineItem.getQuantityValue();
        });
    }

    return totalBonusProductQuantity;
}
