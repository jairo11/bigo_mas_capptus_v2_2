/**
 *  Property of OSF Global Services, Inc., (with its brand OSF Commerce). OSF remains the sole owner of all right, title and interest in the software.
 *  Do not copy, sell, reverse engineer or otherwise attempt to derive or obtain information about the functioning, manufacture or operation therein.
 */

"use strict";

/* eslint-disable no-use-before-define, valid-jsdoc */

/**
 * Controller for managing the OSF Smart Order Refill Business Manager module
 * This Business Manager module is used to display the reports about and manage customer subscriptions and subscription orders
 * @module controllers/FastOMSConfiguration
 */

var Transaction = require("dw/system/Transaction");
var Site = require("dw/system/Site");
var ISML = require("dw/template/ISML");
var StringUtils = require("dw/util/StringUtils");
var URLUtils = require("dw/web/URLUtils");

var OSFLicenseManager = require("*/cartridge/scripts/OSFLicenseManager");

/**
 * show main Dashboard
 */
exports.Dashboard = function () {
    ISML.renderTemplate("foms/mainDashboard");
};
exports.Dashboard.public = true;

/**
 * configutaion tools
 */
exports.Config = function () {
    var System = require("dw/system/System");
    var form = session.forms.config;
    var currentSite = Site.current;
    var FastOMSOCAPIAuth;
    var cancelreturnreason;
    var refundreason;
    var orgPrefs = System.getPreferences();
    if (!empty(form.submittedAction) && form.valid) {
        Transaction.wrap(function () {
            FastOMSOCAPIAuth = form.username.htmlValue + ":" + form.password.htmlValue + ":" + form.ocapipassword.htmlValue;
            FastOMSOCAPIAuth = StringUtils.encodeBase64(FastOMSOCAPIAuth);
            cancelreturnreason = request.httpParameterMap["cancelreturnreason[]"];
            if (!cancelreturnreason.empty) {
                currentSite.setCustomPreferenceValue("LOMSCancelReturnReasons", cancelreturnreason.values);
            }
            refundreason = request.httpParameterMap["refundreason[]"];
            if (!refundreason.empty) {
                currentSite.setCustomPreferenceValue("LOMSRefundReasons", refundreason.values);
            }
            orgPrefs.getCustom().FastOMSOCAPIAuth = FastOMSOCAPIAuth;
            currentSite.setCustomPreferenceValue("FOMSOCAPIVersion", form.ocapiversion.value);
            currentSite.setCustomPreferenceValue("FOMSOCAPIClientID", form.clientid.value);
            currentSite.setCustomPreferenceValue("FOMSOCAPIPassword", form.ocapipassword.value);
            currentSite.setCustomPreferenceValue("LOMSEncryptionKey", form.encriptionkey.value);
            currentSite.setCustomPreferenceValue("FastOMSCouponFlow", form.couponFlow.value);
        });
        response.redirect(URLUtils.url("FastOMSConfiguration-Config"));
    } else if (empty(form.submittedAction)) {
        form.clearFormElement();
        FastOMSOCAPIAuth = orgPrefs.getCustom().FastOMSOCAPIAuth;
        if (!empty(FastOMSOCAPIAuth)) {
            FastOMSOCAPIAuth = StringUtils.decodeBase64(FastOMSOCAPIAuth);
            var authObject = FastOMSOCAPIAuth.split(":");
            form.username.value = authObject[0];
            form.password.value = authObject[1];
            form.ocapipassword.value = authObject[2];
        }

        form.ocapiversion.value = currentSite.getCustomPreferenceValue("FOMSOCAPIVersion");
        form.encriptionkey.value = currentSite.getCustomPreferenceValue("LOMSEncryptionKey");
        form.clientid.value = currentSite.getCustomPreferenceValue("FOMSOCAPIClientID");
        form.couponFlow.value = currentSite.getCustomPreferenceValue("FastOMSCouponFlow");
        cancelreturnreason = currentSite.getCustomPreferenceValue("LOMSCancelReturnReasons");
        refundreason = currentSite.getCustomPreferenceValue("LOMSRefundReasons");

        ISML.renderTemplate("foms/config", {
            navigation: "config",
            refundreason: refundreason,
            cancelreturnreason: cancelreturnreason,
            Authorized: hasValidLicense()
        });
    }
};
exports.Config.public = true;

/**
 * license configuraion
 */
exports.License = function () {
    var CustomObjectMgr = require("dw/object/CustomObjectMgr");
    var OSFLicenseConstants = require("*/cartridge/scripts/utils/OSFLicenseConstants");
    var ProductList = require("*/cartridge/products_mapping.json");
    var form = session.forms.license;
    var productCode = "LOMS";
    var isValid;
    var expiryDate;
    var productId;
    var productName;
    var isEntered;
    var foundLicense;
    for (var i = 0; i < ProductList.PRODUCTS.length; i++) {
        if (ProductList.PRODUCTS[i].Code === productCode) {
            productId = ProductList.PRODUCTS[i].ID;
            productName = ProductList.PRODUCTS[i].Name;
            break;
        }
    }
    if (!empty(form.submittedAction) && form.valid) {
        try {
            if (!empty(form.licenseUniqueID.htmlValue)) {
                var oldLicense = CustomObjectMgr.getCustomObject(
                    OSFLicenseConstants.CUSTOM_OBJECT_TYPE,
                    form.licenseUniqueID.htmlValue
                );
                if (!empty(oldLicense)) {
                    Transaction.wrap(function () {
                        CustomObjectMgr.remove(oldLicense);
                    });
                }
            }
            OSFLicenseManager.getLicenseStatus({
                activationKey: form.licensekey.htmlValue,
                email: form.email.htmlValue,
                pcID: form.email.htmlValue,
                productID: productId,
                productName: productName,
                productCode: productCode
            });
        // eslint-disable-next-line
        } catch (error) {}//NOSONAR

        response.redirect(URLUtils.url("FastOMSConfiguration-License"));
    } else if (empty(form.submittedAction)) {
        form.clearFormElement();
        var installedLicenses = CustomObjectMgr.queryCustomObjects(
            OSFLicenseConstants.CUSTOM_OBJECT_TYPE,
            "custom.siteID = {0}",
            "custom.productCode ASC",
            Site.current.ID
        );
        while (installedLicenses.hasNext()) {
            var license = installedLicenses.next();

            if (!empty(license.custom.activationKey) && license.custom.productCode === "LOMS") {
                form.email.value = license.custom.email;
                form.licensekey.value = license.custom.activationKey;
                form.licenseUniqueID.value = license.custom.licenseUniqueID;
                isValid = license.custom.isValid;
                expiryDate = license.custom.expiryDate;
                foundLicense = license;
                break;
            }
        }
        isEntered = !empty(foundLicense);
    }

    ISML.renderTemplate("foms/license", {
        navigation: "license",
        isValid: isValid,
        isEntered: isEntered,
        expiryDate: expiryDate
    });
};

exports.License.public = true;

/**
 * Determins if the FastOMS license is valid
 * @returns {Boolean}
 */
function hasValidLicense() {
    try {
        return OSFLicenseManager.getLicenseStatus("LOMS").isValid;
    } catch (error) {
        return false;
    }
}
