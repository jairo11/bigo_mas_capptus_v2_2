<isdecorate template="application/MenuFrame">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" >
    <link rel="stylesheet" href="${URLUtils.staticURL('/css/fastoms.css')}" />
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

    <isif condition="${pdict.ValidLicense}" >
        <div id="main-container">
            <div class="container">
                <div id="mainMenu" class="container-panel">
                    <div class="card">
                        <div class="card-body">
                            <div class="row nav-search">
                                <div class="col-lg-12">
                                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link mainTab active" id="simple-search-tab" data-toggle="pill" href="#simple-search" role="tab" aria-controls="simple-search" aria-selected="true">${Resource.msg('fastoms.simplesearch', 'fastoms', null)}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link mainTab" id="advanced-search-tab" data-toggle="pill" href="#advanced-search" role="tab" aria-controls="advanced-search" aria-selected="false">${Resource.msg('fastoms.advancedsearch', 'fastoms', null)}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link mainTab" id="pending-return-tab" data-toggle="pill" href="#pending-return" role="tab" aria-controls="pending-return" aria-selected="false">${Resource.msg('fastoms.pendingreturn', 'fastoms', null)}</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="tab-content" id="pills-tabContent">
                                        <div class="tab-pane fade show active" id="simple-search" role="tabpanel" aria-labelledby="simple-search-tab">
                                            <form method="post" class="navbar-form searchForm" role="search">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <input class="typeahead form-control" type="input" name="simpleSearch" placeholder="${Resource.msg('fastoms.search', 'fastoms', null)}...">
                                                        <div class="input-group-append">
                                                            <button class="btn btn-outline-secondary searchBtn" type="submit">${Resource.msg('fastoms.search', 'fastoms', null)}</button>
                                                        </div>
                                                    </div>
                                                    <small class="form-text text-muted">${Resource.msg('fastoms.searchhelp', 'fastoms', null)}</small>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="advanced-search" role="tabpanel" aria-labelledby="advanced-search-tab">
                                            <form method="post" class="navbar-form searchForm" role="search">
                                                <fieldset>
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="form-group">
                                                                <label>${Resource.msg('fastoms.orderno', 'fastoms', null)}</label>
                                                                <input class="form-control" type="input" name="orderNo">
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-group">
                                                                <label>${Resource.msg('fastoms.customername', 'fastoms', null)}</label>
                                                                <input class="form-control" type="input" name="customerName">
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-group">
                                                                <label>${Resource.msg('fastoms.customeremail', 'fastoms', null)}</label>
                                                                <input class="form-control" type="email" name="customerEmail">
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-group">
                                                                <label>${Resource.msg('fastoms.status', 'fastoms', null)}</label>
                                                                <select class="form-control dropdown" name="status">
                                                                    <option value="">${Resource.msg('global.select','fastoms',null)}</option>
                                                                    <option value="${dw.order.Order.ORDER_STATUS_CREATED}">${Resource.msg('order.object.status.created', 'fastoms', null)}</option>
                                                                    <option value="${dw.order.Order.ORDER_STATUS_NEW}">${Resource.msg('order.object.status.new', 'fastoms', null)}</option>
                                                                    <option value="${dw.order.Order.ORDER_STATUS_OPEN}">${Resource.msg('order.object.status.open', 'fastoms', null)}</option>
                                                                    <option value="${dw.order.Order.ORDER_STATUS_COMPLETED}">${Resource.msg('order.object.status.completed', 'fastoms', null)}</option>
                                                                    <option value="${dw.order.Order.ORDER_STATUS_FAILED}">${Resource.msg('order.object.status.failed', 'fastoms', null)}</option>
                                                                    <option value="${dw.order.Order.ORDER_STATUS_CANCELLED}">${Resource.msg('order.object.status.cancelled', 'fastoms', null)}</option>
                                                                    <option value="${dw.order.Order.ORDER_STATUS_REPLACED}">${Resource.msg('order.object.status.replaced', 'fastoms', null)}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-group">
                                                                <label>${Resource.msg('fastoms.tracking', 'fastoms', null)}</label>
                                                                <input class="form-control" type="input" name="trackingNumber">
                                                            </div>
                                                            <div class="alert alert-warning errorTrackingNo hidden">
                                                                ${Resource.msg('fastoms.advancedsearch.trackingerror', 'fastoms', null)}
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-group">
                                                                <label>${Resource.msg('fastoms.returntracking', 'fastoms', null)}</label>
                                                                <input class="form-control" type="input" name="returnTracking">
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-group">
                                                                <label>${Resource.msg('fastoms.startDate', 'fastoms', null)}</label>
                                                                <input class="form-control" type="input" name="startDate"  autocomplete="off" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn btn-info searchBtn">
                                                        <span>${Resource.msg('fastoms.search', 'fastoms', null)}</span>
                                                    </button>
                                                    <button class="btn btn-default clearBtn">
                                                        <span>${Resource.msg('fastoms.clearall', 'fastoms', null)}</span>
                                                    </button>
                                                </fieldset>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade show" id="pending-return" role="tabpanel" aria-labelledby="pending-return-tab"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="orderslist position-relative">
                        <div class="loms-loader hidden"></div>
                        <div class="results"></div>
                    </div>
                </div>

                <div class="orderview container-panel position-relative">
                    <h3 id="backToList" class="header-back-link">« ${Resource.msg('fastoms.backorders', 'fastoms', null)}</h3> <br>
                    <div class="loms-loader hidden"></div>
                    <div class="result"></div>
                </div>
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="${URLUtils.staticURL('/libs/js/typeahead.min.js')}"></script>
        <script src="${URLUtils.staticURL('/libs/js/jplist.core.min.js')}"></script>
        <script src="${URLUtils.staticURL('/libs/js/jplist.bootstrap-pagination-bundle.min.js')}"></script>
        
        <script>
        (function(){
            window.Resources = <isprint value="${JSON.stringify(pdict.FDResources)}" encoding="htmlsinglequote"/>;
            window.Urls = <isprint value="${JSON.stringify(pdict.FDUrls)}" encoding="htmlsinglequote"/>;
        }());
        </script>
        <script src="${URLUtils.staticURL('/js/fastoms.js')}"></script>
    <iselse>
        <h2>${Resource.msg('fastoms.invalidlicense', 'fastoms', null)}</h2>
    </isif>
</isdecorate>
