"use strict";

var CustomObjectMgr = require("dw/object/CustomObjectMgr");
var Logger = require("dw/system/Logger");
var OrderMgr = require("dw/order/OrderMgr");

const ReturnStatus = {
    CREATED: 1,
    ON_TRIP: 2, //ON_TRIP
    IN_CEDIS: 3, //IN_CEDIS
    CORRECT_VALIDATION: 4,  //CORRECT_VALIDATION
    REFUND: 5,  //REFUND
    COMPLETED: 6,
    DONT_PROCEED: 7,  //DONT_PROCEED
    REFUND_PACKAGE: 8,  //REFUND_PACKAGE
    EXPIRED_SHIPPING: 9,  //EXPIRED_SHIPPING
    FAILED: 10,
    CANCELLED: 11
};

function getReturnOrderByRMA(RMA) {
    var returnOrder = CustomObjectMgr.getCustomObject("ReturnOrder", RMA);
    return returnOrder;
}

function getReturnOrderItemByID(ID) {
    var returnOrderItem = CustomObjectMgr.getCustomObject("ReturnOrderItem", ID);
    return returnOrderItem;
}

function appplyRefund(order, amountToRefund, rma) {
    var returnCore = require("*/cartridge/scripts/core/returnCore");

    var result=null;
    var HookMgr = require("dw/system/HookMgr");
    var PaymentMgr = require("dw/order/PaymentMgr");
    var paymentMethodID = order.paymentInstrument.paymentMethod;
    var processor = PaymentMgr.getPaymentMethod(paymentMethodID).getPaymentProcessor();
    if (HookMgr.hasHook("app.payment.processor." + processor.ID.toLowerCase())) {
        result = HookMgr.callHook("app.payment.processor." + processor.ID.toLowerCase(),
            "Refund", order, amountToRefund, rma
        );
    }
    //set oms status
    if (result != true) {
        let request = [{
            rma: rma, 
            statusId: 5,
            refundSuccess: true
        }];
        returnCore.setStatusReturnOrder(request);
        result = true;
        return result;
    }
    
}

function sendChangeStatusEmail(returnOrder, order, status) {
    var Resource = require('dw/web/Resource');
    var Site = require('dw/system/Site');
    var OrderModel = require('*/cartridge/models/order');
    var ReturnModel = require('*/cartridge/models/product/returnProducts');
    var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');
    var Locale = require('dw/util/Locale');
    var currentLocale = Locale.getLocale('es_MX');
    var templateType = '';
    var orderModel = new OrderModel(order, { countryCode: currentLocale.country, containerView: 'order' });
    var orderObject = { order: orderModel };

    // var returnModel = new ReturnModel(getReturnItems(returnOrder.custom.RMA));

    var emailObj = {
        to: order.customerEmail,
        subject: Resource.msg('subject.refund.confirmation.email', 'order', null),
        from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@testorganization.com',
        type: emailHelpers.emailTypes.orderConfirmation
    };
        // creada 1, devolucion acepatda 4, reembolso 5,  rechazada7, 8 faltate, 9 faltante
    if (status == ReturnStatus.CORRECT_VALIDATION) {
        emailObj.subject = Resource.msg('subjec.order.accepted.returned.email', 'order', null); // change resource message properties
        templateType = 'email/acceptedReturnEmail';
    } else if (status == ReturnStatus.DONT_PROCEED) {
        emailObj.subject = Resource.msg('subjec.order.rejected.returned.email', 'order', null); // change resource message properties
        templateType = 'email/rejectedReturnEmail';
    } else if (status == ReturnStatus.REFUND) {
        emailObj.subject = Resource.msg('subjec.order.completed.returned.email', 'order', null); // change resource message properties
        templateType = 'email/completedReturnEmail';
    } else if(status == ReturnStatus.REFUND_PACKAGE){
        emailObj.subject = Resource.msg('subjec.order.returned.email', 'order', null); // change resource message properties
        templateType = 'email/returnPackageEmail';
    }

    emailHelpers.sendEmail(emailObj, templateType, getReturnItems(returnOrder.custom.RMA));
}

function parseReturnOrder(returnOrder, order) {
    var parsedReturnOrder={};
    parsedReturnOrder.customerEmail = order.customerEmail;
    parsedReturnOrder.firstName = order.customer.profile.firstName;
    parsedReturnOrder.lastName = order.customer.profile.lastName;
    parsedReturnOrder.RMA = returnOrder.custom.RMA;
    return parsedReturnOrder;
}

function parseXMLReturnOrder(xmlStreamReader, xmlReturnOrder, create){
    switch (xmlStreamReader.getLocalName()) {
        case "returnOrder":
            xmlReturnOrder.RMA = xmlStreamReader.getAttributeValue(null, "rma");
            xmlReturnOrder.create = create;
            break;
        case "appliedRefund":
            xmlReturnOrder.appliedRefund = xmlStreamReader.getElementText();
            break;
        case "dateCompleted":
            xmlReturnOrder.dateCompleted = xmlStreamReader.getElementText();
            break;
        case "itemsTotalCost":
            xmlReturnOrder.itemsTotalCost = xmlStreamReader.getElementText();
            break;
        case "orderNO":
            xmlReturnOrder.orderNO = xmlStreamReader.getElementText();
            break;
        case "pdf":
            xmlReturnOrder.pdf = xmlStreamReader.getElementText();
            break;
        case "refundAmount":
            xmlReturnOrder.refundAmount = xmlStreamReader.getElementText();
            break;
        case "shippingPrice":
            xmlReturnOrder.shippingPrice = xmlStreamReader.getElementText();
            break;
        case "status":
            xmlReturnOrder.status = xmlStreamReader.getElementText();
            break;
        case "trackingNumber":
            xmlReturnOrder.trackingNumber = xmlStreamReader.getElementText();
            break;
        default:
            break;
    }
}

function parseXMLReturnOrderItem(xmlStreamReader, xmlReturnOrderItem, create){
    switch (xmlStreamReader.getLocalName()) {
        case "returnOrderItem":
            xmlReturnOrderItem.ID = xmlStreamReader.getAttributeValue(null, "id");
            xmlReturnOrderItem.create = create;
            xmlReturnOrderItem.create = create;
            break;
        case "RMA":
            xmlReturnOrderItem.RMA = xmlStreamReader.getElementText();
            break;
        case "UUID":
            xmlReturnOrderItem.UUID = xmlStreamReader.getElementText();
            break;
        case "cedisComments":
            xmlReturnOrderItem.cedisComments = xmlStreamReader.getElementText();
            break;
        case "comments":
            xmlReturnOrderItem.comments = xmlStreamReader.getElementText();
            break;
        case "creationDate":
            xmlReturnOrderItem.creationDate = xmlStreamReader.getElementText();
            break;
        case "dateCompleted":
            xmlReturnOrderItem.dateCompleted = xmlStreamReader.getElementText();
            break;
        case "itemRefundAmount":
            xmlReturnOrderItem.itemRefundAmount = xmlStreamReader.getElementText();
            break;
        case "lastModified":
            xmlReturnOrderItem.lastModified = xmlStreamReader.getElementText();
            break;
        case "productID":
            xmlReturnOrderItem.productID = xmlStreamReader.getElementText();
            break;
        case "productName":
            xmlReturnOrderItem.productName = xmlStreamReader.getElementText();
            break;
        case "quantity":
            xmlReturnOrderItem.quantity = xmlStreamReader.getElementText();
            break;
        case "reasonReturn":
            xmlReturnOrderItem.reasonReturn = xmlStreamReader.getElementText();
            break;
        case "status":
            xmlReturnOrderItem.status = xmlStreamReader.getElementText();
            break;
        default:
            break;
    }

}

function createReturnOrder(returnOrderToCreate){
    var ro = CustomObjectMgr.createCustomObject("ReturnOrder", returnOrderToCreate.RMA);
    ro.custom.trackingNumber = returnOrderToCreate.trackingNumber;
    ro.custom.orderNO = returnOrderToCreate.orderNO;
    ro.custom.pdf = returnOrderToCreate.pdf;
    ro.custom.status = parseInt(returnOrderToCreate.status, 10);
    ro.custom.shippingPrice = new Number(returnOrderToCreate.shippingPrice);
    ro.custom.refundAmount = new Number(returnOrderToCreate.refundAmount);
    ro.custom.appliedRefund = new Boolean (returnOrderToCreate.appliedRefund === 'true');
    ro.custom.itemsTotalCost = new Number(returnOrderToCreate.itemsTotalCost);
    Logger.info(" Se crea returnOrder : " + JSON.stringify(returnOrderToCreate));
    if(parseInt(returnOrderToCreate.status,10) == ReturnStatus.REFUND_PACKAGE){
        var storedReturnOrder =  getReturnOrderByRMA(returnOrderToCreate.RMA);
        processRefund(storedReturnOrder,returnOrderToCreate)
    }
}

function createReturnOrderItem(returnOrderItemToCreate){
    var roi = CustomObjectMgr.createCustomObject("ReturnOrderItem", returnOrderItemToCreate.ID);
    roi.custom.RMA = returnOrderItemToCreate.RMA;
    roi.custom.itemRefundAmount = new Number(returnOrderItemToCreate.itemRefundAmount);
    roi.custom.productID = returnOrderItemToCreate.productID;
    roi.custom.productName = returnOrderItemToCreate.productName;
    roi.custom.quantity = parseInt(returnOrderItemToCreate.quantity, 10);
    roi.custom.status = parseInt(returnOrderItemToCreate.status, 10);
    roi.custom.comments = returnOrderItemToCreate.comments != '' ? returnOrderItemToCreate.comments : '' ;
    roi.custom.reasonReturn = parseInt(returnOrderItemToCreate.reasonReturn, 10);
    roi.custom.cedisComments = returnOrderItemToCreate.cedisComments != '' ? returnOrderItemToCreate.cedisComments : '';
    Logger.info(" Se crea returnOrderItem : " + JSON.stringify(returnOrderItemToCreate));
}

function updateReturnOrder(xmlReturnOrder){
    var storedReturnOrder =  getReturnOrderByRMA(xmlReturnOrder.RMA);
    Logger.info("updateReturnOrder RMA : " + xmlReturnOrder.RMA + " estatus : " + xmlReturnOrder.status);
    switch (parseInt(xmlReturnOrder.status,10)) {
        case ReturnStatus.CORRECT_VALIDATION:
            processRefund(storedReturnOrder, xmlReturnOrder);
            break;
        case ReturnStatus.DONT_PROCEED:
            if(storedReturnOrder.custom.status == xmlReturnOrder.status) return;
            order = OrderMgr.getOrder(storedReturnOrder.custom.orderNO);
            storedReturnOrder.custom.status = parseInt(ReturnStatus.DONT_PROCEED, 10);
            if(xmlReturnOrder.cedisComments != ''){
                storedReturnOrder.custom.cedisComments = xmlReturnOrder.cedisComments;
            }
            sendChangeStatusEmail(storedReturnOrder, order, ReturnStatus.DONT_PROCEED);
            break;
        case ReturnStatus.COMPLETED:
            if(storedReturnOrder.custom.status == xmlReturnOrder.status) return;
            storedReturnOrder.custom.status = parseInt(ReturnStatus.COMPLETED, 10);
            storedReturnOrder.custom.dateCompleted = new Date();
            break;
        case ReturnStatus.REFUND_PACKAGE:
            processRefund(storedReturnOrder, xmlReturnOrder);
            cancelOrder(storedReturnOrder);
            break;
        default:
            if(storedReturnOrder.custom.status == xmlReturnOrder.status) return;
            storedReturnOrder.custom.status = parseInt(xmlReturnOrder.status, 10);
            break;
    }

}

function cancelOrder(storedReturnOrder) {
    var Transaction = require("dw/system/Transaction");
    var Order = require('dw/order/Order');
    order = OrderMgr.getOrder(storedReturnOrder.custom.orderNO);

    Transaction.wrap(function () {
        order.setStatus(Order.ORDER_STATUS_CANCELLED);
    });
}

function updateReturnOrderItem(xmlReturnOrderItem){
    Logger.info("updateReturnOrderItem ID : " + xmlReturnOrderItem.ID + " status : " + xmlReturnOrderItem.status);
    var storedReturnOrderItem = getReturnOrderItemByID(xmlReturnOrderItem.ID);
    if(xmlReturnOrderItem.status != storedReturnOrderItem.custom.status)
        storedReturnOrderItem.custom.status = parseInt(xmlReturnOrderItem.status, 10);
    if(xmlReturnOrderItem.cedisComments != '' && xmlReturnOrderItem.cedisComments != storedReturnOrderItem.custom.cedisComments){
        storedReturnOrderItem.custom.cedisComments = xmlReturnOrderItem.cedisComments;
    }
    if(xmlReturnOrderItem.status == ReturnStatus.COMPLETED){
        storedReturnOrderItem.custom.dateCompleted = new Date();
    }
}

function processRefund(storedReturnOrder, xmlReturnOrder){
    if(storedReturnOrder.custom.status == xmlReturnOrder.status && storedReturnOrder.custom.appliedRefund) return;
    if(!storedReturnOrder.custom.appliedRefund){
        order = OrderMgr.getOrder(storedReturnOrder.custom.orderNO);
        var amountToRefund=  xmlReturnOrder.refundAmount;
        if (appplyRefund(order, amountToRefund, storedReturnOrder.custom.RMA)) {
            sendChangeStatusEmail(storedReturnOrder, order, xmlReturnOrder.status);
            let status= xmlReturnOrder.status === "4" ? "5" : xmlReturnOrder.status;
            storedReturnOrder.custom.status = parseInt(status, 10);
            storedReturnOrder.custom.appliedRefund = true;
            storedReturnOrder.custom.refundAmount = new Number(amountToRefund);
            if(xmlReturnOrder.cedisComments != '' &&  xmlReturnOrder.cedisComments != storedReturnOrder.custom.cedisComments){
                storedReturnOrder.custom.cedisComments = xmlReturnOrder.cedisComments;
            }
            Logger.info("Refund {0} to returnOrder with RMA : {1}", amountToRefund, storedReturnOrder.custom.RMA);
        } else{
            Logger.info("Can´t refund {0} to returnOrder with RMA : {1}", amountToRefund, storedReturnOrder.custom.RMA);
        }
    }
}

function processReturnOrder(xmlReturnOrder){
    if(xmlReturnOrder.create){
        createReturnOrder(xmlReturnOrder);
    }else{
        updateReturnOrder(xmlReturnOrder);
    }

    for (var i = 0; i < xmlReturnOrder.xmlReturnOrderItems.length; i++) {
        if(xmlReturnOrder.xmlReturnOrderItems[i].create){
            createReturnOrderItem(xmlReturnOrder.xmlReturnOrderItems[i]);
            sendConfirmationEmail(xmlReturnOrder.xmlReturnOrderItems[i]);
        }else{
            updateReturnOrderItem(xmlReturnOrder.xmlReturnOrderItems[i]);
        }
    }

}

function sendConfirmationEmail(xmlReturnOrder){
    try {
        var returnOH = require("*/cartridge/scripts/helpers/returnOrderHelper");
        var site = require("dw/system/Site");
        var currentSite = site.getCurrent();var URLUtils = require("dw/web/URLUtils");
        var returnOrder = returnOH.getReturnOrderByRMA(xmlReturnOrder.RMA);
        if(returnOrder){
            var order = OrderMgr.getOrder(returnOrder.orderNO);
            returnOrder.customerEmail = order.customerEmail;
            returnOrder.firstName = order.customer.profile.firstName;
            returnOrder.lastName = order.customer.profile.lastName;
            var orderDetailsURL = URLUtils.url("Order-Details", "orderID", returnOrder.orderNO).relative().toString();
            var url = currentSite.httpsHostName + orderDetailsURL;
            returnOrder.url = url;
            returnOH.sendReturnConfirmationEmail(order, getReturnItems(xmlReturnOrder.RMA));
        }else{
            Logger.info("No se encontro la devolucion {} para enviar correo de confirmacion de la orden {1} ", xmlReturnOrder.RMA, xmlReturnOrder.orderNO);
        }
    } catch (error) {
        Logger.error("Error al enviar correo de confirmacion de la devolucion {0} de la orden {1} ", xmlReturnOrder.RMA, xmlReturnOrder.orderNO);
    }
}

function getReturnItems(rma) {
    var ProductMgr = require('dw/catalog/ProductMgr');
    var OrderMgr = require('dw/order/OrderMgr');
        var orderHelpers = require('*/cartridge/scripts/order/orderHelpers');
        var queryReturnOrder= "custom.RMA = '"+rma+"'";

        var returnOrder = CustomObjectMgr.queryCustomObjects("ReturnOrder", queryReturnOrder, null);
        var order=[];
        if (returnOrder.count>0) {
            var returnOrderArray = returnOrder.asList().toArray();
            let c=0;
            returnOrderArray.forEach(function (returnOrder) {
                order[c]=returnOrder;
                c++;
            });
        };

        let detailsOrder = OrderMgr.getOrder(order[0].custom.orderNO);
        let arrayDetails = [];

        
        for (let index = 0; index < (detailsOrder.productLineItems).length; index++) {
            let product = detailsOrder.productLineItems[index].product;
            let detail ={
                color: product.custom.refinementColor.displayValue,
                size: product.custom.size,
                id: product.ID
            };
            arrayDetails[index]=detail;
        }

        var returnItems = CustomObjectMgr.queryCustomObjects("ReturnOrderItem", queryReturnOrder, null);
        var items=[];

        if (returnItems.count>0) {
            var returnOrderArray = returnItems.asList().toArray();
            let c=0;
            returnOrderArray.forEach(function (returnItems) {
                let item={};
                //loop for all items of order, and compare id for get size and color
                arrayDetails.forEach(function (element) {
                    if (returnItems.custom.productID === element.id){
                        item = {color: element.color,
                                size: element.size
                        }
                    }
                });

                item.creationDate= returnItems.creationDate;
                item.custom= returnItems.custom;
                item.image= (ProductMgr.getProduct(returnItems.custom.productID)).getImage('small',1);
                let detailItem=ProductMgr.getProduct(returnItems.custom.productID);
                items[c]=item;
                c++;
            });
        };

    let ordeReturn={
        name: detailsOrder.customerName,
        order: order,
        items: items
    };

   return  ordeReturn ;
}


module.exports = {
    getReturnOrderByRMA: getReturnOrderByRMA,
    getReturnOrderItemByID: getReturnOrderItemByID,
    parseXMLReturnOrder: parseXMLReturnOrder,
    parseXMLReturnOrderItem: parseXMLReturnOrderItem,
    processReturnOrder: processReturnOrder,
    sendChangeStatusEmail: sendChangeStatusEmail
};
