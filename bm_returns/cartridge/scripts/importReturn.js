"use strict";

var FileReader = require("dw/io/FileReader");
var XMLStreamReader = require("dw/io/XMLStreamReader");
var Status = require("dw/system/Status");
var File = require("dw/io/File");
var Logger = require("dw/system/Logger");
var OrderMgr = require("dw/order/OrderMgr");
var Transaction = require("dw/system/Transaction");
var WebDAVUtils = require("~/cartridge/scripts/util/WebDAVUtils");
var returnOrderUtil = require("~/cartridge/scripts/util/returnOrderUtil");
var XMLStreamConstants = require("dw/io/XMLStreamConstants");



/**
 * Update using a XML file stored on webdav in Sites/Impex/src/ folder.
 */
module.exports.execute = function (params) {
    var folder = File.IMPEX + File.SEPARATOR + "src" + File.SEPARATOR + params.folder;
    var filePattern = params.filePattern;
    var afterImportOption = params.afterImportOption;

    Logger.info("Order Import - Getting local file");
    var files = WebDAVUtils.getFilesInLocalFolder(folder, filePattern);
    if (empty(files)) {
        return new Status(Status.OK, "NO_FILES", "No files to import in folder " + folder);
    }

    var errorsCount = 0;

    files.toArray().forEach(function (file) {
        try {
            Logger.info("Return Order Import - Working with file " + file.name);
            var returnOrder = null;
            var returnOrderItem = null;
            var fileReader = new FileReader(file, "UTF-8");
            var xmlStreamReader = new XMLStreamReader(fileReader);
            var inReturnOrderItem = false;
            var inReturnOrder = false;
            var xmlReturnOrder = {};
            xmlReturnOrder.xmlReturnOrderItems =[];
            var xmlReturnOrderItem = {};
            while (xmlStreamReader.hasNext()) {
                Transaction.begin();
                xmlStreamReader.next();
                if (xmlStreamReader.getEventType() === XMLStreamConstants.START_ELEMENT) {
                    if (xmlStreamReader.getLocalName() === "returnOrder") {
                        inReturnOrder= true;
                        inReturnOrderItem= false;
                        var rma = xmlStreamReader.getAttributeValue(null, "rma");
                        Logger.info("Se busca returnOrder con id : " + rma);
                        if (!empty(rma)){
                            returnOrder = returnOrderUtil.getReturnOrderByRMA(rma);
                        }

                        if(empty(returnOrder)){
                            Logger.info("No se encuentra returnOrder con id : " + rma);
                        }else{
                            Logger.info("Se encuentra returnOrder con id : " + rma);
                        }
                    } 

                    if (xmlStreamReader.getLocalName() === "returnOrderItem") {
                        inReturnOrder = false;
                        inReturnOrderItem= true;
                        var returnorderitemid = xmlStreamReader.getAttributeValue(null, "id");
                        Logger.info("Se busca returnOrderItem con id : " + returnorderitemid);
                        if (!empty(returnorderitemid)) {
                            returnOrderItem = returnOrderUtil.getReturnOrderItemByID(returnorderitemid);
                        }

                        if(empty(returnOrderItem)){
                            Logger.info("No se encuentra returnOrderItem con id : " + returnorderitemid);
                        }else{
                            Logger.info("Se encuentra returnOrderItem con id : " + returnorderitemid);
                        }
                    } 

                    if(inReturnOrder){
                        returnOrderUtil.parseXMLReturnOrder(xmlStreamReader, xmlReturnOrder, empty(returnOrder));
                    }
                    if(inReturnOrderItem){
                        returnOrderUtil.parseXMLReturnOrderItem(xmlStreamReader, xmlReturnOrderItem, empty(returnOrderItem));
                    }
                }

                if (xmlStreamReader.getEventType() === XMLStreamConstants.END_ELEMENT) {
                    if (xmlStreamReader.getLocalName() === "returnOrder") {
                        returnOrderUtil.processReturnOrder(xmlReturnOrder);
                        xmlReturnOrder = {};
                        xmlReturnOrder.xmlReturnOrderItems =[];
                    }
                    if (xmlStreamReader.getLocalName() === "returnOrderItem") {
                        xmlReturnOrder.xmlReturnOrderItems.push(xmlReturnOrderItem);
                        xmlReturnOrderItem ={};
                    }
                }
                
                Transaction.commit();
            }
        } catch (e) {
            Logger.error("Return Order Import - " + e.toString());
            Transaction.rollback();
            errorsCount++;
        } finally {
            xmlStreamReader.close();
            fileReader.close();
        }

        WebDAVUtils.processFile(afterImportOption, file, folder);
    });

    if (errorsCount > 0) {
        return new Status(Status.ERROR, "ERROR", "Errors during file(s) processing");
    }

    return new Status(Status.OK, "OK", "Returns Order import has been successfully finished");
};