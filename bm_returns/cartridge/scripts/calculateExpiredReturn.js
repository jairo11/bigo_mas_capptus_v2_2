"use strict";

var FileReader = require("dw/io/FileReader");
var XMLStreamReader = require("dw/io/XMLStreamReader");
var Status = require("dw/system/Status");
var File = require("dw/io/File");
var Logger = require("dw/system/Logger");
var OrderMgr = require("dw/order/OrderMgr");
var Transaction = require("dw/system/Transaction");
var WebDAVUtils = require("~/cartridge/scripts/util/WebDAVUtils");
var returnOrderUtil = require("~/cartridge/scripts/util/returnOrderUtil");
var XMLStreamConstants = require("dw/io/XMLStreamConstants");


var CustomObjectMgr = require("dw/object/CustomObjectMgr");
var hookMgr = require("dw/system/HookMgr");
var Site = require("dw/system/Site");


/**
 * Update using a XML file stored on webdav in Sites/Impex/src/ folder.
 */
module.exports.execute = function (params) {
    var daysToExpire = JSON.parse(Site.getCurrent().getCustomPreferenceValue("daysToExpire"));
    var queryReturnOrder = "custom.status = " + 1;
    var returnOrder = CustomObjectMgr.queryCustomObjects("ReturnOrder", queryReturnOrder, null);
    var returnOrderArray = returnOrder.asList().toArray();
    returnOrderArray.forEach(function (ro) {
        Logger.info("Procesando order :  " + ro.custom.RMA);
        var elapsedTIme = (new Date().getTime() - ro.creationDate.getTime() ) 
        if(elapsedTIme> (daysToExpire * 24 *60 * 60 * 1000 )){
            try {
                Transaction.begin();
                ro.custom.status = 9;
                var returnOrderItems = CustomObjectMgr.queryCustomObjects("ReturnOrderItem", "custom.RMA = " + "'" + ro.custom.RMA + "'" , null);
                var returnOrderItemArray = returnOrderItems.asList().toArray();
                returnOrderItemArray.forEach(function (roi) {
                    roi.custom.status = 9;
                });
                var order = OrderMgr.getOrder(ro.custom.orderNO);
                returnOrderUtil.sendChangeStatusEmail(ro, order, 9);
				
				//integracion servicio  notificacion OMS
                var requestPayload = {};
                if (hookMgr.hasHook("app.services.cya.orderReturn")) {
                    //response = hookMgr.callHook("app.services.cya.orderReturn", "updateReturnOrderStatus", requestPayload);
                }

                Transaction.commit();
                Logger.info("Se cancela returnOrden :  " + ro.custom.RMA);
            } catch (e) {
                Transaction.rollback();
                Logger.error("Error al actualizar returnOrden :  " + ro.custom.RMA + " Error : " + e.toString()) ;
            }
        }
    });
};