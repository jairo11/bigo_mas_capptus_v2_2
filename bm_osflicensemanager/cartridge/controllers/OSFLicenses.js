"use strict";

//API includes
var CustomObjectMgr = require("dw/object/CustomObjectMgr");
var ISML            = require("dw/template/ISML");
var Site            = require("dw/system/Site");
var Transaction     = require("dw/system/Transaction");

//OSF module includes
var OSFLicenseConstants = require("*/cartridge/scripts/utils/OSFLicenseConstants");
var productList         = require("../products_mapping.json");
var OSFLicenseManager   = require("*/cartridge/scripts/OSFLicenseManager");

//Controller modules
var guard = require("~/cartridge/scripts/guard");

function printJSON(object) {
    response.setContentType("application/json");
    var json = JSON.stringify(object);
    response.writer.print(json);
}

function getLicenses() {
    var legacyLicenses    = [];
    var osfLicenses       = [];
    var installedProducts = [];

    var installedLicenses = CustomObjectMgr.queryCustomObjects(
        OSFLicenseConstants.CUSTOM_OBJECT_TYPE,
        "custom.siteID = {0}",
        "custom.productCode ASC",
        Site.current.ID
    );
    while (installedLicenses.hasNext()) {
        var license = installedLicenses.next();

        if (empty(license.custom.organizationID)) {
            osfLicenses.push({
                licenseUniqueID   : license.custom.licenseUniqueID,
                isValid           : license.custom.isValid,
                productName       : license.custom.productName,
                productID         : license.custom.productID,
                activationKey     : license.custom.activationKey,
                email             : license.custom.email,
                validationDateKey : license.custom.validationDateKey,
                expiryDate        : license.custom.expiryDate
            });
        } else {
            legacyLicenses.push({
                licenseUniqueID   : license.custom.licenseUniqueID,
                isValid           : license.custom.isValid,
                organizationID    : license.custom.organizationID,
                productName       : license.custom.productName,
                productCode       : license.custom.productCode,
                securityToken     : license.custom.securityToken,
                validationDateKey : license.custom.validationDateKey,
                expiryDate        : license.custom.expiryDate
            });
        }
        installedProducts.push(license.custom.productName);
    }

    ISML.renderTemplate("licenses", {
        legacyLicenses    : legacyLicenses,
        osfLicenses       : osfLicenses,
        productList       : productList.PRODUCTS,
        installedProducts : installedProducts
    });
}

function saveLicense() {
    var license = JSON.parse(request.httpParameterMap.requestBodyAsString);

    if ((empty(license.productID) || empty(license.productCode)) && !empty(license.activationKey)) {
        var licenseCredential = searchProductCredential(license.productName);
        license.productCode = licenseCredential.productCode;
        license.productID = licenseCredential.productID;
    }

    OSFLicenseManager.getLicenseStatus(license);
}

function removeLicense() {
    var license = JSON.parse(request.httpParameterMap.requestBodyAsString);
    var result;
    var licenseToRemove = CustomObjectMgr.getCustomObject(
        OSFLicenseConstants.CUSTOM_OBJECT_TYPE,
        license.licenseUniqueID
    );

    var equalProduct        = licenseToRemove.custom.productCode === license.productCode;
    var equalToken          = licenseToRemove.custom.securityToken === license.securityToken;
    var equalOrganizationID = licenseToRemove.custom.organizationID === license.organizationID;
    var isValid             = licenseToRemove.custom.isValid;
    var isInstalled         = licenseToRemove.custom.isInstalled;

    if (license.action && (equalProduct && equalToken && equalOrganizationID) && (isValid || isInstalled)) {
        return;
    }

    try {
        Transaction.wrap(function () {
            CustomObjectMgr.remove(licenseToRemove);
        });
        result = {
            success  : true,
            errorMsg : null
        };
    } catch (e) {
        result = {
            success  : false,
            errorMsg : e.message
        };
    }

    printJSON(result);
}

function updateLicense() {
    removeLicense();
    saveLicense();
}

function searchProductCredential(productName) {
    var products = productList.PRODUCTS;
    for (var i = 0; i < products.length; i++) {
        if (products[i].Name === productName) {
            return {
                productID   : products[i].ID,
                productCode : products[i].Code
            };
        }
    }
}

exports.GetLicenses = guard.ensure(["get"], getLicenses);
exports.RemoveLicense = guard.ensure(["post"], removeLicense);
exports.SaveLicense = guard.ensure(["post"], saveLicense);
exports.UpdateLicense = guard.ensure(["post"], updateLicense);
