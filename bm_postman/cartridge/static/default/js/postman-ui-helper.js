// @ts-check

/**
 * Data model for a data request
 * @class
 * @param {{ method: any; name?: string; url: any; body?: any; headers: any; params: any; auth?: string; encoding: any; id?: any; }} data
 */
function RequestData(data) {
    /** @type {string} */
    this.id = data.id || getRandomId(10);

    /** @type {string} */
    this.name = data.name || 'Untitled request';

    /** @type {string}  */
    this.method = data.method || 'GET';

    /** @type {string}  */
    this.url = data.url;

    /** @type {string}  */
    this.encoding = data.encoding || 'UTF-8';

    /** @type {{ [k: string]: any }} */
    this.params = data.params || {};

    /** @type {{ [k: string]: any }} */
    this.headers = data.headers || {};

    /** @type {string} */
    this.body = data.body;

    /** @type {string} */
    this.auth = data.auth || 'NONE';
}

RequestData.prototype.toControllerRequest = function() {
    return {
        method: this.method, 
        url: this.url,
        params: JSON.stringify(this.params),
        headers: JSON.stringify(this.headers),
        body: this.body,
        encoding: this.encoding,
        auth: this.auth,
    }
};

RequestData.prototype.toJSON = function () {
    return {
        id: this.id,
        name: this.name,
        method: this.method, 
        url: this.url,
        params: this.params,
        headers: this.headers,
        body: this.body,
        encoding: this.encoding,
        auth: this.auth,
    }
}

RequestData.fromJSON = function(json) {
    return new RequestData(json);
}

/**
 * Data model for the request collection
 * @param {{ name?: any, id?: number, requests?: RequestData[]|any[] }} data
 */
function RequestCollection(data) {
    this.name = data.name || RequestCollection.defaultName;
    this.id = data.id || getRandomId(10);

    /** @type {RequestData[]} */
    this.requests = [];

    // Process the requests json
    if(data.requests && data.requests.length) {
        for (var i = 0; i <data.requests.length; i++) {
            if (data.requests[i] instanceof RequestData) {
                this.requests.push(data.requests[i]);
            } else {
                this.requests.push(new RequestData(data.requests[i]));
            }
        }
    }
}

/**
 * Default name that is used when the collection has no name
 */
RequestCollection.defaultName = document.getElementById('request-collection-default-name').innerText;

/**
 * Adds a new request to the collection
 * @param {RequestData} request
 */
RequestCollection.prototype.addRequest = function (request) {
    this.requests.push(request);
    
    /** @type {{[k: string]: RequestData[]}} */
    var methods = {};

    /** @type {RequestData[]}  */
    var newRequests = [];

    // Group by method
    for (var i = 0; i < this.requests.length; i++) {
        if (!methods[this.requests[i].method]) {
            methods[this.requests[i].method] = [this.requests[i]];
        } else {
            methods[this.requests[i].method].push(this.requests[i]);
        }
    }    
    
    // Fills the new array with all the requests sorted by group and name
    Object.keys(methods).forEach(function(method) {
        newRequests = [].concat(
            newRequests,
            sortByStringAttribute(methods[method], 'name')
        );
    });

    this.requests = newRequests;
}

/**
 * Removes a request from the collection
 * @param {RequestData|string} request
*/
RequestCollection.prototype.removeRequest = function(request) {
    /** @type {string} */
    var id = (request instanceof RequestData) ? request.id : request.toString();
    
    this.requests = this.requests.filter(function (req) {
        return req.id !== id;
    });
}

RequestCollection.prototype.toJSON = function() {
    return {
        name: this.name,
        id: this.id,
        requests: this.requests.map(function(r) {
            return r.toJSON()
        }),
    };
}

RequestCollection.fromJSON = function(json) {
    var collection = new RequestCollection(json);
    return collection;
}

/**
 * @param {number} length
 * @returns {string}
 */
function getRandomId(length) {
    length = length || 10;
    return Math.random().toString(36).substr(2, length - 2);
}

/**
 * Sorts an array by an attribute
 * @param {{[k: string]: any}[]} array 
 * @param {string} key 
 */
function sortByStringAttribute (array, key) {
    return array.sort(function (a, b) {
        return a[key].toLowerCase() < b[key].toLowerCase() ? -1 : a[key].toLowerCase() > b[key].toLowerCase() ? 1 : 0;
    });
}

/**
 * @param {string[]} names
 * @param {string} newName
 * @returns {string}
 */
function getNewNamePreventingDuplicates(names, newName) {
    var TRAILING_NUMBER_REGEX = /\([0-9]+\)/;
    newName = newName.trim();

    while (names.indexOf(newName) !== -1) {
        var index = names.indexOf(newName);
        var match = names[index].match(TRAILING_NUMBER_REGEX);
        if (match && match[0]) {
            // If the title already existed and contained a (<number>), increase it
            var number = Number.parseInt(match[0].replace('(','').replace(')',''));
            number ++;
            newName = newName.replace(TRAILING_NUMBER_REGEX, '(' + number + ')');
        } else {
            // If duplicated for first time, set (1)
            newName += ' (1)';
        }
    }

    return newName.trim();
}