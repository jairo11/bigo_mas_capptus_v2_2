// @ts-check

$(document).ready(function () {
    var collections = [];
    var collectionsStorage = new PostmanStorage(new CookieStorage());
    var activeRequestId;
    var activeCollectionId;
    var AUTH_TYPES = {
        BASIC: 'BASIC',
        NONE: 'NONE'
    };

    collectionsStorage.getData().then(function (data) {
        collections = data;
        // Load the request collections
        initUserCollections();
    });

    var $cache = {
        tabLinks: $('.tablinks'),
        requester: {
            form: $('.js_requester-form'),
            url: $('.js_url-input'),
            paramsForm: $('.requester-params_table'),
            headersForm: $('.requester-headers_table'),
            updateButton: $('.requester-button_update')
        },
        tabs: {
            collections: $('[data-tab-id="collections"]')
        }
    };

    createRequesterAuthOptions();

    // Tabs
    $cache.tabLinks.on('click', function () {
        var currentTabContainer = $(this).closest('.requester-pane');

        $(currentTabContainer).find('.tabcontent').hide();
        $(currentTabContainer).find('.tablinks').removeClass('active');

        tabid = $(this).data('href');
        var tabContent = $('#' + tabid);
        tabContent.show();
        $(this).addClass('active');
    });

    $('.js_table-row_delete').on('click', function () {
        var currentTableRow = $(this).closest('.table-row');
        var currentTable = currentTableRow.closest('.requester-table');

        if ($(currentTable).find('.table-row:not(.table-header)').length >= 2) {
            if (currentTableRow.hasClass('active')) {
                currentTableRow.prev().addClass('active');
            }
            currentTableRow.remove();
        } else {
            currentTableRow.find('input').val('');
        }
    });

    // Table input
    $('.table-row .requester-params_key').on('keyup', function (e) {
        // verification for only characters to activate function
        if ($.inArray(e.keyCode, [13, 16, 17, 18, 19, 20, 27, 35, 36, 37, 38, 39, 40, 91, 93, 224]) !== -1) return;
        if ($(this).closest('.table-row').hasClass('active') && $(this).val()) {
            createNewRequestParamsTableRow();
        }
    });

    /**
     * @description Show dialog with custom options.
     * @param {string} textBody The message shown in the dialog
     * @param {Boolean} [btnCancel] Show buttton cancel
     * @param {Boolean} [newCollection] Whether collection show input
     * @returns {Promise<Boolean>}
     */
    function showDialog(textBody, btnCancel = true, newCollection = false) {
        var response = false;
        var $dialog = $('#dialog');
        var dialogOpts = {
            resizable: false,
            draggable: false,
            modal: true
        };

        return new Promise(function(resolve) {
            var buttons;
            if (btnCancel) {
                buttons = [
                    {
                        text: 'Cancel',
                        click: function() {
                            $dialog.dialog('close');
                            resolve(response);
                        }
                    },
                    {
                        text: 'Ok',
                        click: function() {
                            response = true;
                            $dialog.dialog('close');
                            resolve(response);
                        }
                    }
                ]
            } else {
                buttons = [
                    {
                        text: 'Ok',
                        click: function() {
                            response = true;
                            $dialog.dialog('close');
                            resolve(response);
                        }
                    }
                ]
            }

            $dialog.dialog(dialogOpts, {
                buttons,
                open: function() {
                    $dialog.find('p').html(textBody);
                    if (newCollection) {
                        $dialog.find('input').removeClass('hide');
                    }
                },
                close: function() {
                    if (newCollection) {
                        $dialog.find('input').addClass('hide');
                    }
                }
            });
        });
    }

    /**
     * Creates a new row in the request parameters table
     * @returns {void}
     */
    function createNewRequestParamsTableRow() {
        $cache.requester.paramsForm.find('.table-row').removeClass('active');

        var $row = $cache.requester.paramsForm.find('.table-row:last').clone(true);
        $row.find('.js_table_param-key').val('');
        $row.find('.js_table_param-value').val('');
        $row.find('.js_table_param-description').val('');
        $row.addClass('active');

        $cache.requester.paramsForm.append($row);
    }

    /**
     * Clears the request parameters table
     * @returns {void}
     */
    function clearRequestParamsTable() {
        $('.requester-params_table').find('.table-row:not(.active, .table-header)').remove();
    }

    $('.table-row .requester-headers_key').on('keyup', function(e) {
        // verification for only characters to activate function
        if ($.inArray(e.keyCode, [13, 16, 17, 18, 19, 20, 27, 35, 36, 37, 38, 39, 40, 91, 93, 224]) !== -1) return;
        if ($(this).closest('.table-row').hasClass('active') && $(this).val()) {
            console.log('creating new headers param')
            createNewRequestHeadersTableRow();
        }
    });

    /**
     * Creates a new row in the request parameters table
     * @returns {void}
     */
    function createNewRequestHeadersTableRow() {
        $cache.requester.headersForm.find('.table-row').removeClass('active');

        var $row = $cache.requester.headersForm.find('.table-row:last').clone(true);
        $row.find('.js_table_header-key').val('');
        $row.find('.js_table_header-value').val('');
        $row.find('.js_table_header-description').val('');
        $row.addClass('active');

        $cache.requester.headersForm.append($row);
    }

    /**
     * Clears the request parameters table
     * @returns {void}
     */
    function clearRequestHeadersTable() {
        $('.requester-headers_table').find('.table-row:not(.active, .table-header)').remove();
    }

    // Form submition
    $cache.requester.form.on('submit', function (e) {
        e.preventDefault();
        cleanResponsePanels();

        var requestData = getRequestData();

        // Send the data to the Postman controller
        $.ajax({
            data: requestData.toControllerRequest(),
            url: $(this).data('url'),
            type: 'POST',
            success: onSuccess,
        });
    });

    /**
     * Function executed with the response of 
     * the Postman controller
     * @returns {void}
     */
    function onSuccess(response) {
        if (response.body != null) {
            $('.js_response-body').text(response.body.replace(/"/g, ''));
        }

        $('.js_response-meta').removeClass('hide');
        $('.js_response-time').empty().append(document.createTextNode(' ' + response.time + ' ms'));
        $('.js_response-status').empty().append(document.createTextNode(' ' + response.status));

        if (response.headers && response.headers.length > 0) {
            response.headers.forEach(function (item) {
                var header = JSON.parse(item);
                for (var key in header) {
                    if (header.hasOwnProperty(key)) {
                        $('.js_response-headers').append($('<div class="response-header_row"> <span class="response-header_key">' + key + '</span> → ' + header[key].value + '</div>'));

                        if (header[key].isCookie) {
                            displayResponseCookie(header[key]);
                        }
                    }
                }
            });
        }

        var cookiesCount = $('.js_cookie_data').length;
    }

    /**
     * Cleans the response panels to be able to set new data
     */
    function cleanResponsePanels() {
        $('.js_response-headers').empty();
        $('.js_response-time').empty();
        $('.js_response-body').empty();
        $('.js_cookie_data').remove();
        $('.js_response-status').empty();
    }

    /**
     * Cleans the request information
     */
    function cleanRequestPanels() {
        // clean the request url
        $('.requester-url_input').val('');

        // clean the request 
        createNewRequestParamsTableRow();
        $('.requester-params_table').find('.table-row:not(.table-header, .active)').remove();

        // clean the auth data
        $('.select-authorization').val('NONE').trigger('change');
        $('.js_auth_basic_username').val('');
        $('.js_auth_basic_password').val('');

        // clean the headers
        createNewRequestParamsTableRow();
        $('.requester-headers_table').find('.table-row:not(.table-header, .active)').remove();

        // clean the body
        $('.js_requester-body').val('');

        // save options
        $('.request-save-name').val('');
    }

    function displayResponseCookie(cookie) {
        var cookieData = {
            name: '',
            value: '',
            isHttpOnly: false,
            isSecure: false,
            path: '',
            expirationDate: ''
        };
        cookieData.isHttpOnly = cookie.value.includes('HttpOnly');
        cookieData.isSecure = cookie.value.includes('Secure');
        var values = cookie.value.split(';');
        for (var index = 0; index < values.length; index++) {
            var cookie = values[index].split('=');
            if (index == 0) {
                cookieData.name = cookie[0];
                cookieData.value = cookie[1].replace(/"/g, '');
            } else {
                switch (cookie[0].trim()) {
                    case 'Path':
                        cookieData.path = cookie[1];
                        break;
                    case 'Expires':
                        cookieData.expirationDate = cookie[1];
                        break;
                }
            }
        }
        var newRow = document.createElement('div');
        $(newRow).addClass('table-row js_cookie_data').appendTo($('.js_response-cookies'));
        $(newRow).append($('<div class="text table-cell">' + cookieData.name + '</div>'));
        $(newRow).append($('<div class="text table-cell">' + cookieData.value + '</div>'));
        $(newRow).append($('<div class="text table-cell">' + cookieData.path + '</div>'));
        $(newRow).append($('<div class="text table-cell">' + cookieData.expirationDate + '</div>'));
        $(newRow).append($('<div class="text table-cell">' + cookieData.isHttpOnly + '</div>'));
        $(newRow).append($('<div class="text table-cell">' + cookieData.isSecure + '</div>'));
    }

    /**
     * Gets the request parameters
     * @returns {RequestData}
     */
    function getRequestData() {
        var contentType = getRequestContentType();

        var data = {
            method: $('.js_method :selected').val(),
            url: $cache.requester.url.val(),
            body: getRequestBody(contentType),
            headers: getRequestHeaders(),
            params: getRequestParams(),
            auth: getRequestAuth(),
            encoding: getRequestEncoding(contentType),
        };

        return new RequestData(data);
    }

    /**
     * Gets the request body
     * @param {string} contentType 
     */
    function getRequestBody(contentType) {
        var body = $('.js_requester-body').val();

        switch (contentType) {
            case 'JSON':
                try {
                    JSON.parse(body);
                } catch (error) {
                    //Show error in the field
                    console.log('Error in JSON body')
                    return;
                }
                break;
            case 'XML':
                try {
                    $.parseXML(body);
                } catch (error) {
                    //Show error in the field
                    console.log('Error in XML body')
                    return;
                }
                break;
            default:
        }

        return body;
    }

    /**
     * Gets the request content type
     * @returns {string}
     */
    function getRequestContentType() {
        return $('.js_requester-content-type :selected').val().split(';')[0];
    }

    /**
     * Gets the request encoding
     * @returns {string}
     */
    function getRequestEncoding(contentType) {
        var encoding = 'UTF-8';

        if (contentType.includes('charset') || contentType.includes('CHARSET') || contentType.includes('Charset')) {
            try {
                encoding = contentType.split('=')[1];
            } catch (error) {
                //Set error in the field
                console.log('Error in charset definition');
            }
        }

        return encoding;
    }

    /**
     * Gets the request headers
     * @returns {{ [k:string]: string }|{}}
     */
    function getRequestHeaders() {
        var headers = {};

        // Set all the headers that have at least a key defined
        $('.js_table_header-key').each(function () {
            if (!$(this).val()) return;

            var $this = $(this);
            var key = $this.val();
            var value = $this.closest('.table-row').find('.js_table_header-value').val();
            var description = $this.closest('.table-row').find('.js_table_header-description').val();
            
            headers[key] = {
                value: value,
                description: description
            };
        });

        // Set the headers authorization
        var auth = getRequestAuth();
        if (auth.type == AUTH_TYPES.BASIC) {
            var credentials = auth.params.username + ':' + auth.params.password;
            credentials = btoa(credentials);
            headers['Authorization'] = 'Basic ' + credentials;
        }

        return headers;
    }

    /**
     * Gets all the user set request parameters
     * @returns {{ [k:string]: string }|{}}
     */
    function getRequestParams() {
        var params = {};

        $('.js_table_param-key').each(function () {
            if (!$(this).val()) return;

            var $this = $(this);
            var key = $this.val();
            var value = $this.closest('.table-row').find('.js_table_param-value').val();
            var description = $this.closest('.table-row').find('.js_table_param-description').val();
            
            params[key] = {
                value: value,
                description: description
            };
        });

        return params;
    }

    /**
     * Returns the selected request auth method
     * @returns {{ type: string, params: { [k: string]: any } }}
     */
    function getRequestAuth() {
        var type = $('.js_authorization :selected').val();
        var params = {};

        if (type === AUTH_TYPES.BASIC) {
            params = {
                username: $('.js_auth_basic_username').val(),
                password: $('.js_auth_basic_password').val()
            };
        }

        return {
            type: type,
            params: params
        }
    }

    
    /**
     * Created the requester authentication options
     */
    function createRequesterAuthOptions() {
        if (!$('#authorization').find('.js_authorization').length) {
            var $authorization = $('#authorization');
            var options = $authorization.data('options');
            $authorization.prepend('<div class="select-content_selector"><select class="select-authorization js_authorization"></select></div>');
            options.authTypes.each(function (val) {
                $('.js_authorization').append('<option value="' + val + '"> ' + val + '</option>')
            });
        }

        $('.js_authorization').off('change').on('change', function () {
            var currentVal = $(this).val();
            $('.select-content').removeClass('active');
            $('.select-content_' + currentVal.toLowerCase()).addClass('active');
        });
    }

    /**
     * Returns all the collections for the user
     * @returns {void}
     */
    function initUserCollections() {
        displayUserCollections();
        setupUserCollectionsEvents();
        updateRequestSaveSelect();

        // display the update button if it is necessary
        if (activeCollectionId && activeRequestId) {
            $cache.requester.updateButton.show();
        } else {
            $cache.requester.updateButton.hide();
        }
    }

    /**
     * Renders all the user collections in the 
     * side interface
     * @returns {void}
     */
    function displayUserCollections() {
        var $tabContent = $cache.tabs.collections.find('.tab-content');

        $tabContent.empty();

        // Display all the user collections
        collections.forEach(function (collection) {
            var $collectionItem = $('<div class="collection-item" data-id=' + collection.id + '><div class="collection-item-info">' + collection.name + ' <span class="collection-requests-count">' + collection.requests.length + '</span>' + ' <div class="collection-item-options" style="display: none"><span class="delete-button">X</span></div></div></div>');

            // Loop through each collection request and add the item
            var $collectionRequests = $('<div class="collection-item-requests" style="display: none"></div>');
            collection.requests.forEach(function (request) {
                var $request = $('<div class="collection-item-request" data-json=\'' + JSON.stringify(request.toJSON()) + '\'><span class="request-method">' + request.method + '</span>' + request.name + ' <div class="collection-item-options" style="display: none"><span class="delete-button">X</span></div></div>');
                $collectionRequests.append($request);
            });

            if (!collection.requests.length) {
                $collectionRequests.append('<p class="collection-empty">This collection is empty.</p>');
            }

            $collectionItem.append($collectionRequests);

            // Append the collection and the requests to the tab content
            $tabContent.append($collectionItem);
        });
    }

    /**
     * Sets up all the user collection's events
     * @returns {void}
     */
    function setupUserCollectionsEvents() {
        // Setup collection item clicks
        $('.collection-item-info').off('click').on('click', onSelectCollection);

        // Setup request item clicks
        $('.collection-item-request').off('click').on('click', onSelectRequest);

        // Setup button to create new collections
        $('.button-add-collection').off('click').on('click', onCreateNewCollection);

        // Setup button to save request on selected collection
        $('.request-save-button').off('click').on('click', onSaveRequestOnCollection);

        $('.collection-item-info, .collection-item-request').off('hover').hover(function (e) {
            $(this).find('.collection-item-options').toggle();
        });

        $('.collection-item-request .delete-button').off('click').on('click', onDeleteRequest);

        $('.collection-item-info .delete-button').off('click').on('click', onDeleteCollection);

        $('.requester-button_new').off('click').on('click', onNewRequestButtonClick);

        $('.requester-button_update').off('click').on('click', onUpdateRequest);
    }

    /**
     * Gets triggered when the user creates a new request
     * @param {Event} e 
     */
    function onNewRequestButtonClick(e) {
        e.preventDefault();

        // Unset the active request id;
        activeRequestId = undefined;
        activeCollectionId = undefined;

        cleanResponsePanels();
        cleanRequestPanels();
        initUserCollections();
    }

    /**
     * On select a collection, toggle it's requests display
     * @param {Event} e 
     */
    function onSelectCollection(e) {
        e.preventDefault();

        // Toggle the collection's requests display
        $(e.target)
            .parent('.collection-item')
            .toggleClass('open')
            .find('.collection-item-requests')
            .toggle();
    }

    /**
     * On select request, sets the data in the
     * user interface
     * @param {Event} e 
     */
    function onSelectRequest(e) {
        e.preventDefault();

        var $element = $(e.target);
        var json = $element.data('json');
        if (!json) return;

        // Set the active request id
        activeRequestId = json.id;
        activeCollectionId = $element.closest('.collection-item').data('id');

        // Display the update button
        $cache.requester.updateButton.show();

        // Clear the previous response items
        cleanResponsePanels();

        // Render the url
        $cache.requester.url.val(json.url);

        // Render the method
        $('select.js_method').val(json.method);
        $('.requester-form .select2-selection__rendered').data('title', json.method);
        $('.requester-form .select2-selection__rendered').html(json.method);

        // Render the params
        clearRequestParamsTable();
        Object.keys(json.params).forEach(function (key) {
            if (!json.params[key]) return;

            var $row = $cache.requester.paramsForm.find('.table-row:last');
            $row.find('.js_table_param-key').val(key);
            $row.find('.js_table_param-value').val(json.params[key].value);
            $row.find('.js_table_param-description').val(json.params[key].description);
            $row.removeClass('active');

            $cache.requester.paramsForm.append($row);

            createNewRequestParamsTableRow();
        });

        // Render the authorization
        var auth = json.auth || {};
        $('.js_authorization').val(auth.type || AUTH_TYPES.NONE).trigger('change');

        if (auth.type === AUTH_TYPES.BASIC) {
            var authParams = auth.params || {};
            $('.js_auth_basic_username').val(authParams.username);
            $('.js_auth_basic_password').val(authParams.password);
        }

        // render the headers
        clearRequestHeadersTable();
        Object.keys(json.headers).forEach(function (key) {
            if (key === 'Authorization' || !json.headers[key]) return;

            var $row = $cache.requester.headersForm.find('.table-row:last');
            $row.find('.js_table_header-key').val(key);
            $row.find('.js_table_header-value').val(json.headers[key].value);
            $row.find('.js_table_header-description').val(json.headers[key].description);
            $row.removeClass('active');

            $cache.requester.headersForm.append($row);

            createNewRequestHeadersTableRow();
        });

        // Render the body
        $('.js_requester-body').val(json.body);

        // Render the request name
        $('.request-save-name').val(json.name);
    }

    /**
     * Function that gets triggered when the user
     * creates a new collection
     * @param {Event} e 
     */
    function onCreateNewCollection(e) {
        e.preventDefault();

        showDialog('Please set a new name for the collection:', true, true)
        .then((responseDialog) => {
            if (responseDialog) {
                /** @type {string} */
                var name = $('.dialog input').val().trim() || RequestCollection.defaultName;
                $('.dialog input').val('');

                var names = collections.map(function (collection) {
                    return collection.name;
                });

                // Add the new request collection
                var collection = new RequestCollection({ name: getNewNamePreventingDuplicates(names, name) });
                collections.push(collection);
                collections = sortByStringAttribute(collections, 'name');
                collectionsStorage.saveData(collections);
                initUserCollections();
            }
        });
    }

    /**
     * Function that gets triggered when the user
     * saves a request on a collection
     * @param {Event} e 
     */
    function onSaveRequestOnCollection(e) {
        e.preventDefault();

        // Get the data from the current UI configuration
        var data = getRequestData();
        if (!data.url) {
            showDialog('Please add an url.', false);
            return;
        }

        var requestName = $('.request-save-name').val();
        if (!requestName) {
            showDialog('Please add a request name.', false);
            return;
        }

        // Get the current collection id selected in the select
        var collectionId = $('.request-save-select-collection').val();
        data.name = requestName;

        // Get the collection with the same id, add the request
        // and save the data
        for (var i = 0; i < collections.length; i++) {
            if (collections[i].id === collectionId) {
                /** @type {string[]} */
                var requestNames = collections[i].requests.map(function (/** @type {RequestData} */ request) {
                    return request.name;
                });

                // Prevent duplicates
                data.name = getNewNamePreventingDuplicates(requestNames, data.name);

                collections[i].addRequest(data);
                collectionsStorage.saveData(collections);
                initUserCollections();
                break;
            }
        }
    }

    function onUpdateRequest(e) {
        e.preventDefault();

        if (!activeRequestId || !activeCollectionId) {
            showDialog('Please select first a request from the left panel.', false);
            return;
        }

        // Get the data from the current UI configuration
        var data = getRequestData();
        if (!data.url) {
            showDialog('Please add an url.', false);
            return;
        }

        var requestName = $('.request-save-name').val();
        if (!requestName) {
            showDialog('Please add a request name.', false);
            return;
        }
        
        data.name = requestName;

        // Get the active collection and request and update
        // the data
        for (var i = 0; i < collections.length; i++) {
            if (collections[i].id === activeCollectionId) {
                for (var j = 0; j < collections[i].requests.length; j++) {
                    if (collections[i].requests[j].id === activeRequestId) {
                        collections[i].requests[j] = data;
                        collectionsStorage.saveData(collections);
                        initUserCollections();
                    }
                }
                break;
            }
        }
    }

    /**
     * Updates the request save select
     */
    function updateRequestSaveSelect() {
        var $requestSaveSelect = $('.request-save-select-collection');
        var $requestSaveButton = $('.request-save-button');
        var $requestErrorCreateCollection = $('.request-error-create-collection');

        $requestSaveButton.removeAttr('disabled');
        $requestErrorCreateCollection.hide();
        $requestSaveSelect.empty();

        collections.forEach(function (c) {
            var $option = $('<option value="' + c.id + '">' + c.name + '</option>');
            $requestSaveSelect.append($option);
        });

        if (!collections.length) {
            $requestSaveButton.attr('disabled', 'disabled');
            $requestErrorCreateCollection.show();
        }
    }

    /**
     * Function that gets triggered when the user
     * delets a request
     * @param {Event} e 
     */
    async function onDeleteRequest(e) {
        e.preventDefault();
        
        var responseDialog = await showDialog('Are you sure you want to delete this request?');
        
        if (responseDialog) {
            var $request = $(e.target).closest('.collection-item-request');
            var json = $request.data('json');
            var collectionIndex;
            for (var i = 0; i < collections.length; i++) {
                for (var j = 0; j < collections[i].requests.length; j++) {
                    if (collections[i].requests[j].id === json.id) {
                        collectionIndex = i;
                        break;
                    }
                }
    
                if (typeof collectionIndex !== 'undefined') {
                    break;
                }
            }
    
            // Remove the request from the collection index
            collections[collectionIndex].removeRequest(json.id);
    
            // Persist the changes and update
            collectionsStorage.saveData(collections);
            initUserCollections();
        }
    }

    /**
     * Function that gets triggered when the user
     * deletes a collection
     * @param {Event} e 
     */
    async function onDeleteCollection(e) {
        e.preventDefault();
        let responseDialog = await showDialog('Are you sure you want to delete this collection?');

        if (responseDialog) {
            var $request = $(e.target).closest('.collection-item');
            var id = $request.data('id');
            var collectionIndex;
            for (var i = 0; i < collections.length; i++) {
                if (collections[i].id === id) {
                    collectionIndex = i;
                    break;
                }
            }
    
            collections.splice(collectionIndex, 1);
    
            // Persist the changes and update
            collectionsStorage.saveData(collections);
            initUserCollections();
        }
    }
});
