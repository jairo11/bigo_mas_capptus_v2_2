// @ts-check

/**
 * Class to save the data on the cookie storage
 * @class
 */
function CookieStorage() {
    this.COOKIE_ATTRIBUTE_NAME = "postmanCollections";
    this.COOKIE_EXPIRATION_DAYS = 365 * 10; // 10 years
}

/**
 * @returns {Promise<RequestCollection[]>}
 */
CookieStorage.prototype.getData = function () {
    var that = this;
    return new Promise(function (resolve) {
        var value = Cookies.get(that.COOKIE_ATTRIBUTE_NAME);
        
        /** @type {any[]} */
        var json = value ? JSON.parse(value) : [];
    
        // Prevent cookie value failures
        if (!Array.isArray(json)) {
            json = [];
        }
    
        var collections = json.map(function (collection) {
            return RequestCollection.fromJSON(collection);
        });
    
        resolve(collections);
    });
}

/**
 * @param {RequestCollection[]} collections
 * @returns {Promise<void>}
 */
CookieStorage.prototype.saveData = function(collections) {
    var that = this;
    return new Promise(function (resolve) {
        var json = collections.map(function (json) {
            return json.toJSON();
        });
    
        var collectionsString = JSON.stringify(json);
        Cookies.set(that.COOKIE_ATTRIBUTE_NAME, collectionsString, { expires: that.COOKIE_EXPIRATION_DAYS });
        resolve();
    });
}


/**
 * Storage class incharged of retrieving
 * and saving the storage data with capacities
 * for multiple data storages that may act like plugins,
 * being able to ultimately save the data to any database
 * required in the future
 * @class
 * @param {*} dataStore 
 */
function PostmanStorage (dataStore) {
    this.dataStore = dataStore;
}

/**
 * Gets the Postman collections data
 * @returns {Promise<RequestCollection[]>}
 */
PostmanStorage.prototype.getData = function () {
    return this.dataStore.getData();
}

/**
 * Saves the data
 * @param {RequestCollection[]} collections
 * @returns {Promise<void>}
 */
PostmanStorage.prototype.saveData = function(collections) {
    return this.dataStore.saveData(collections);
}
