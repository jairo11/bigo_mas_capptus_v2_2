'use strict';
/**
*
*
*/

function PostmanService() {
    var serviceID = require('dw/web/Resource').msg('postman.servicename', 'postman', null);
    var ServiceRegistry = require('dw/svc/LocalServiceRegistry');

    this.service = ServiceRegistry.createService(serviceID, {
        /**
         * Used to create the request
         * @param svc {HTTPService}
         * @return {Object} 
         */
        createRequest: function(svc, args) {
            svc.setRequestMethod(args.method);
            svc.URL = args.url;
            svc.setAuthentication(!empty(args.auth) ? args.auth : 'NONE');
            svc.setEncoding(!empty(args.encoding) ? args.encoding : 'UTF-8');

            if (!args.headers.empty){
                var headers = JSON.parse(args.headers);
                for (var property in headers) {
                    if (headers.hasOwnProperty(property)) {
                        svc.addHeader(property, headers[property]);
                    }
                }
            }
            if (!args.params.empty){
                var params = JSON.parse(args.params);
                for (var property in params) {
                    if (params.hasOwnProperty(property)) {
                        svc.addParam(property, params[property]);
                    }
                }
            }

            if (!empty(args.data)) {
               return args.data;
            }
        },
        /**
         * Used to parse the response when sucessfull
         * @param svc {HTTPService}
         * @param client {HTTPClient}
         */
        parseResponse: function(svc, client) {
            var arrayHeaders = [];
            if (!empty(client.responseHeaders)) {
                for (var key in client.responseHeaders) {
                    if (!empty(client.responseHeaders[key])){
                        for (var index in client.responseHeaders[key]) {
                            var tempHeader = {};
                            tempHeader[key] = {
                                value: client.responseHeaders[key][index],
                                isCookie: key.toString().toLowerCase() == 'set-cookie'
                            };
                            arrayHeaders.push(JSON.stringify(tempHeader));                            
                        }
                    }
                }
            }
            return {
                body: client.getText(),
                status: client.statusCode, 
                headers: arrayHeaders 
            };
        },
        /**
         * Used when the service is in mock mode
         * @param svc {HTTPService}
         * @param client {HTTPClient}
        */
        mockCall: function(svc, client) {
            return {
                statusCode: 200,
                statusMessage: 'Success',
                text: 'MOCK RESPONSE (' + svc.URL + ')'
            };
        }
    });

    /*================================================================
    =            Helper methods to be used in the library            =
    ================================================================*/

    this.__exec = function(args) {
       var result = this.service.call({
           'url': args.url,
           'method': args.method,
           'data': args.body,
           'encoding': args.encoding,
           'params': args.params,
           'headers': args.headers,
           'auth': args.auth
       });
       if (result.status == 'OK') {
           result = {
               response: result.object,
               statusMessage: result.msg
           };
       } else {
           result = {
               response: {
                   status: result.error,
                   body: result.errorMessage
               },
               statusMessage: result.msg
           };
       }
       return result;
    };
    /*-----  End of Helper methods to be used in the library  ------*/
}

PostmanService.prototype.execute = function(args){
    return this.__exec(args);
}

module.exports = PostmanService;
