"use strict";

/**
 * Controller that provides functions for debugging REST APIs.
 * @module controllers/Postman
 */

/* Script Modules */
var app = require("~/cartridge/scripts/app");
var guard = require("~/cartridge/scripts/guard");


/**
 * Renders the general Postman page.
 */
function start() {
    app.getView({}).render("postmanui");
}

/**
 * Create the communication with the information sent and renders the response
 */
function makeCall() {
    var PostmanService = require("~/cartridge/scripts/PostmanService.ds");
    var PS = new PostmanService();
    var data = request.httpParameterMap;
    var duration = new Date();
    var result = PS.execute({
        "method": data.method,
        "url": data.url,
        "params": data.params,
        "headers": data.headers,
        "body": data.body,
        "encoding": data.encoding,
        "auth": data.auth
    });
    duration = new Date().getTime() - duration;
    app.getView({
        obj: {
            body: result.response.body,
            status: result.response.status + " " + result.statusMessage.toUpperCase(),
            time: duration,
            headers: result.response.headers,
        }
    }).render("postmanresponsearea");
}

/*
* Web exposed methods
*/

/** Renders the general Postman page
 * @see {@link module:controllers/Postman~start} */
exports.Start = guard.ensure(["get", "https"], start);

/** Renders the response for REST call
 * @see {@link module:controllers/Postman~makeCall} */
exports.MakeCall = guard.ensure(["post", "https"], makeCall);
