'use strict';

$(document).ready(function () {
    $(document).on('focusout', '#shippingZipCodedefault',  function () {
        //$('.shippingZipCode')on("focusout",  function () {
        console.log("validatepostal------->");
        $("#shippingSuburbdefault").empty();
        $("#defaultZipCode").empty();
        $("#shippingZipCodedefault").removeClass('is-invalid');
        var tam = document.getElementById('shippingZipCodedefault').value;
        if (tam.length == 5) {
            var returnSubmitUrl = $('#shippingZipCodedefault').attr('data-url');
            var form = {
                zip: $('#shippingZipCodedefault').val(),
                csrf_token: $(this).closest('form').find('input[name="csrf_token"]').val()
            };
            var $rootEl = $(this).closest('.shipping-address-block');
            $rootEl.spinner().start();
            $.ajax({
                url: returnSubmitUrl,
                method: 'POST',
                data: form,
                success: function (data) {
                    if (data.info) {
                        if ( data.hasCoverage ) {
                            var options = data.info.suburbs;
                            $("#shippingAddressCitydefault").val(data.info.city);
                            $.each(options, function (i, item) {
                                $("#shippingSuburbdefault").append($("<option> ", {
                                    value: item,
                                    text : item
                                }, "</option>"));
                            });
                            var state = data.info.state;
                            var mapState = {};
                            $("#shippingStatedefault > option").each(function() {
                                mapState[this.text] =  this.value;
                            });
                            if (!mapState.hasOwnProperty(state)) {
                                Object.keys(mapState).forEach(function(key) {
                                    if (mapState[key].indexOf(state) > -1 ) {
                                        state = key;
                                    }
                                });
                            }
                            console.log("ialvarez: ", {mapState, state, value: mapState[state], data});
                            $("#shippingStatedefault").val(mapState[state]);
                        } else {
                            $("#shippingZipCodedefault").addClass('is-invalid');
                            $("#defaultZipCode").html(data.message);

                        }
                    }else{
                        console.log("Not delivery in ZipPostal")
                    }
                    $rootEl.spinner().stop();
                },
                error: function () {
                    $rootEl.spinner().stop();
                }
            });
        }else{
            $("#shippingZipCodedefault").addClass('is-invalid');
            $("#defaultZipCode").html("Codigo Postal Invalido");
        }
    });

    $(document).on('input', '#billingZipCodedefault', function () {
        $("#billingSuburbdefault").empty();
        var tam = document.getElementById('billingZipCodedefault').value;
        if (tam.length == 5) {
            var returnSubmitUrl = $('#billingZipCodedefault').attr('data-url');
            var form = {
                zip: $('#billingZipCodedefault').val(),
                csrf_token: $(this).closest('form').find('input[name="csrf_token"]').val()
            };
            $.ajax({
                url: returnSubmitUrl,
                method: 'POST',
                data: form,
                success: function (data) {
                    if (data.info){
                        var options = data.info.suburbs;
                        $("#billingAddressCity").val(data.info.city);
                        $.each(options, function (i, item) {
                            $("#billingSuburbdefault").append($("<option> ", {
                                value: item,
                                text : item
                            }, "</option>"));
                        });
                    }else{
                        console.log("Not delivery in ZipPostal")
                    }
                },
                error: function () {
                }
            });
        }
    });
});

