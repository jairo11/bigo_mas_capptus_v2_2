'use strict';
var base = require('base/search/search');

/**
 * Update DOM elements with Ajax results
 *
 * @param {Object} $results - jQuery DOM element
 * @param {string} selector - DOM element to look up in the $results
 * @return {undefined}
 */
function updateDom($results, selector) {
    var $updates = $results.find(selector);
    $(selector).empty().html($updates.html());
}

/**
 * Keep refinement panes expanded/collapsed after Ajax refresh
 *
 * @param {Object} $results - jQuery DOM element
 * @return {undefined}
 */
function handleRefinements($results) {
    $('.refinement.active').each(function () {
        $(this).removeClass('active');
        var activeDiv = $results.find('.' + $(this)[0].className.replace(/ /g, '.'));
        activeDiv.addClass('active');
        activeDiv.find('button.title').attr('aria-expanded', 'true');
    });

    updateDom($results, '.refinements');
}

/**
 * Parse Ajax results and updated select DOM elements
 *
 * @param {string} response - Ajax response HTML code
 * @return {undefined}
 */
function parseResults(response) {
    var $results = $(response);
    var specialHandlers = {
        '.refinements': handleRefinements
    };

    // Update DOM elements that do not require special handling
    [
        '.grid-header',
        '.header-bar',
        '.header.page-title',
        ".breadcrumbs",
        '.product-grid',
        '.show-more',
        '.filter-bar'
    ].forEach(function (selector) {
        updateDom($results, selector);
    });

    Object.keys(specialHandlers).forEach(function (selector) {
        specialHandlers[selector]($results);
    });
}

function applyFilter () {
    // Handle refinement value selection and reset click
    $('.container').on(
        'click',
        '.refinements li button, a.reset, .filter-value button, .swatch-filter button',
        function (e) {
            e.preventDefault();
            e.stopPropagation();

            $.spinner().start();
            $(this).trigger('search:filter', e);
            $.ajax({
                url: $(this).data('href'),
                data: {
                    page: $('.grid-footer').data('page-number'),
                    selectedUrl: $(this).data('href')
                },
                method: 'GET',
                success: function (response) {
                    parseResults(response);
                    $.spinner().stop();
                },
                error: function () {
                    $.spinner().stop();
                }
            });
        });
}

function changeGrid () {
    $('.grid-select .grid').on('click', function(){
        $('.grid-select .grid').removeClass('selected');
        $(this).addClass('selected');
        var gridType = $(this).attr('data-grid');
        $('.product-grid').attr('data-grid', gridType);
    });

    $(window).on("load resize", function(){
        var width = $(this).width();

        if(width <= 992){
            $('.grid[data-grid="grid-2"]').addClass('selected').siblings().removeClass('selected');
            $('.product-grid').attr('data-grid', 'grid-2');
        }
        else{
            $('.grid[data-grid="grid-4"]').addClass('selected').siblings().removeClass('selected');
            $('.product-grid').attr('data-grid', 'grid-4');
        }
    });
}

base.applyFilter = applyFilter;
base.changeGrid = changeGrid;

module.exports = base;