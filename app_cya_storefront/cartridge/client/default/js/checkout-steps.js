
let buttonEdit = document.querySelectorAll('span.js-change-step'),
    buttonSubmit = document.querySelectorAll('.btn.js-change-step'),
    progress = document.querySelector('.checkout-progress-indicator-in-in').getAttribute('data-step');
var step = (step)=>{
    if(document.querySelector('.step-item.active')){
        document.querySelector('.step-item.active').classList.remove('active');
    }
    document.querySelector('.'+step).classList.add('active');
}
step(progress);
for (const [a,b] of buttonEdit.entries()) {
    buttonEdit[a].addEventListener('click', function() {
        let value = this.getAttribute('data-step');
        step(value);
    });
}
for (const [a,b] of buttonSubmit.entries()) {
    buttonSubmit[a].addEventListener('click', function() {
        let value1 = window.location.search.split('=');
        value1 = value1[value1.length-1];
        setTimeout(function(){
            let value2 = window.location.search.split('=');
            value2 = value2[value2.length-1];
            if (value1 != value2){
                step(value2);
            }
        },800)
    });
}
$('.checkout-page__purchase--title').on('click', function(){
    $('.checkout-page__purchase').toggleClass('hide-content');
});