'use strict';
var base = require('base/product/detail');
var data = require('./base');

base.updateAttribute = function () {
    $('body').on('product:afterAttributeSelect', function (e, response) {
        if ($('.product-detail>.bundle-items').length) {
            response.container.data('pid', response.data.product.id);
            response.container.find('.product-id').text(response.data.product.id);
        } else if ($('.product-set-detail').eq(0)) {
            response.container.data('pid', response.data.product.id);
            response.container.find('.product-id').text(response.data.product.id);
            if (response.data.product.productName) {
                response.container.find('.product-name').text(response.data.product.productName);
            }
            if (response.data.product.shortDescription) {
                response.container.find('.short-description > .row > .col-12 > .content').text(response.data.product.shortDescription);
            }
            response.container.find('.product-id').text(response.data.product && response.data.product.productModel ? response.data.product.productModel : response.data.product.id);
        } else {
            $('.product-id').text(response.data.product.id);
            $('.product-detail:not(".bundle-item")').data('pid', response.data.product.id);
        }
    });
},

base.updateAddToCart = function () {
    $('body').on('product:updateAddToCart', function (e, response) {
        // update local add to cart (for sets)
        $('button.add-to-cart', response.$productContainer).attr('disabled',
            (!response.product.readyToOrder || !response.product.available));

        // update global add to cart (single products, bundles)
        var dialog = $(response.$productContainer)
            .closest('.quick-view-dialog');

        $('.add-to-cart-global', dialog).attr('disabled',
            !$('.global-availability', dialog).data('ready-to-order')
            || !$('.global-availability', dialog).data('available')
        );
    });
},

$('.arrow-size-left, .arrow-size-right').remove();

module.exports = base;
