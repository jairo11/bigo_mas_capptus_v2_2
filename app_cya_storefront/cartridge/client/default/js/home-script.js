let searchIcon= document.querySelector('.search-mobile .only-button'),
    headerElements = document.querySelector('.header-elements'),
    searchDesktop = document.querySelector('.search-bar .search-field'),
    closeSearchDesk = document.querySelector('.header-elements .icon-close'),
    closeSearch= document.querySelector('.search-mobile .reset-button'),
    elements= document.querySelector('.mobile-elements'),
    title= document.querySelectorAll('.custom-text-title'),
    paragraph= document.querySelectorAll('.custom-text-paragraph');

if(searchIcon){
    searchIcon.onclick = ()=>{
        elements.classList.add('hide-mobile');
        document.getElementById('logo-mobile').style.display = "none";
        document.getElementById("brand-logo").style.zIndex = "3";
    }
    closeSearch.onclick = ()=>{
        elements.classList.remove('hide-mobile');
        document.getElementById('logo-mobile').style.display = "initial"; 
        document.getElementById("brand-logo").style.zIndex = "4";
    }
    for (const [a,b] of title.entries()) {
        title[a].onclick = ()=>{
            if(title[a].classList.contains('show-mobile')){
                title[a].classList.remove('show-mobile');
                paragraph[a].classList.remove('hide-mobile');
            }else{
                title[a].classList.add('show-mobile');
                paragraph[a].classList.add('hide-mobile');
            }
        }

    }
}
if(searchDesktop){
    searchDesktop.onclick = ()=>{
        headerElements.classList.add('open-search');
        document.querySelector('.navbar-header.icons').classList.add('col-lg-11');
        document.querySelector('.search-bar').classList.add('col-lg-10');
        document.querySelector('.login-header').classList.remove('col-lg-5');
        document.querySelector('.login-header').classList.add('col-lg-1');
        $('#maincontent').addClass('blur');
    }
    closeSearchDesk.onclick = ()=>{
        headerElements.classList.remove('open-search');
        document.querySelector('.navbar-header.icons').classList.remove('col-lg-11');
        document.querySelector('.search-bar').classList.remove('col-lg-10');
        document.querySelector('.login-header').classList.add('col-lg-5');
        document.querySelector('.login-header').classList.remove('col-lg-1');
        $('#maincontent').removeClass('blur');
    }
}

// set email at homePage to modal Newsletter
$(document).on('click', '#btnNewsletter', function () {
    var valor=document.getElementById("hpEmailSignUp").value;
    $("#newsletter-email").val(valor);
});

$(document).on('click', '#btnNewsletterFooter', function () {
    var valor=document.getElementById("emailNewslettter").value;
    $("#newsletter-email").val(valor);
});

$(document).on('click', '#btnSaveNewsLetter', function () {
    campoVacio();
});

function campoVacio(obj){
    var correcto = true;
    var email=document.getElementById("newsletter-email").value;
    var date=document.getElementById("newsletter-birthdate").value;
    var interest=document.getElementById("newsletter-preferences").value
    if(email && date && interest){
        $('#gratefulnessModal').modal('show');
        $('#newsletterModal').modal('hide');
    }
};