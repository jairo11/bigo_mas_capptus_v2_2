'use strict';
var base = require('base/product/base');
function arrowSize(a,b,c){
    var listWidth = 0,
        quantity = 0,
        margin = parseInt(a.css('margin-left'));
    if(c==='root'){
        setTimeout(function(){
            for(var i=1; i<=a.children('li').length;i++){
                listWidth = listWidth + a.children('li:nth-of-type('+i+')').width()+12;
            }
            if(listWidth>a.parent().width()){
                a.siblings('.arrow-attr-right').removeClass('disabled');
            }
        },1000);
        return false;
    }
    else{
        for(var i=1; i<=a.children('li').length;i++){
            listWidth = listWidth + a.children('li:nth-of-type('+i+')').width()+12;
        }
    }
    if(c==='right'){
        quantity = margin-(a.children('li:nth-of-type('+b+')').width()+12);
        a.siblings('.arrow-attr-left').removeClass('disabled');
    }
    else if(c==='left'){
        quantity = margin+(a.children('li:nth-of-type('+b+')').width()+12);
        a.siblings('.arrow-attr-right').removeClass('disabled');
    }
    a.css('margin-left', quantity+'px');

    if(a.parent().width() - listWidth === parseFloat(a.css('margin-left'))){
        a.siblings('.arrow-attr-right').addClass('disabled');
    }
}
function getQuantitySelector($el) {
    var quantitySelected;
    if ($el && $('.set-items').length) {
        quantitySelected = $($el).closest('.product-detail').find('.quantity-select');
    } else if ($el && $('.product-bundle').length) {
        var quantitySelectedModal = $($el).closest('.modal-footer').find('.quantity-select');
        var quantitySelectedPDP = $($el).closest('.bundle-footer').find('.quantity-select');
        if (quantitySelectedModal.val() === undefined) {
            quantitySelected = quantitySelectedPDP;
        } else {
            quantitySelected = quantitySelectedModal;
        }
    } else {
        quantitySelected = $('.quantity-select');
    }
    return quantitySelected;
}
function updateOptions(optionsHtml, $productContainer) {
    $productContainer.find('.product-options').empty().html(optionsHtml);
}

function updateQuantities(quantities, $productContainer) {
    if ($productContainer.parent('.bonus-product-item').length <= 0) {
        var optionsHtml = quantities.map(function (quantity) {
            if (quantity.value<=5){
                var selected = quantity.selected ? ' selected ' : '';
                return '<option value="' + quantity.value + '"  data-url="' + quantity.url + '"' +
                    selected + '>' + quantity.value + '</option>';
            }
        }).join('');
        getQuantitySelector($productContainer).empty().html(optionsHtml);
    }
}
function processSwatchValues(attr, $productContainer, msgs) {
    attr.values.forEach(function (attrValue) {
        var $attrValue = $productContainer.find('[data-attr="' + attr.id + '"] [data-attr-value="' +
            attrValue.value + '"]');
        var $swatchButton = $attrValue.parent();

        if (attrValue.selected) {
            $attrValue.addClass('selected');
            $attrValue.siblings('.selected-assistive-text').text(msgs.assistiveSelectedText);
        } else {
            $attrValue.removeClass('selected');
            $attrValue.siblings('.selected-assistive-text').empty();
        }

        if (attrValue.url) {
            $swatchButton.attr('data-url', attrValue.url);
        } else {
            $swatchButton.removeAttr('data-url');
        }

        // Disable if not selectable
        $attrValue.removeClass('selectable unselectable');

        $attrValue.addClass(attrValue.selectable ? 'selectable' : 'unselectable');
    });
}

function processNonSwatchValues(attr, $productContainer) {
    var $attr = '[data-attr="' + attr.id + '"]';
    var $defaultOption = $productContainer.find($attr + ' .select-' + attr.id + ' option:first');
    $defaultOption.attr('value', attr.resetUrl);

    attr.values.forEach(function (attrValue) {
        var $attrValue = $productContainer
            .find($attr + ' [data-attr-value="' + attrValue.value + '"]');
        $attrValue.attr('value', attrValue.url)
            .removeAttr('disabled');

        if (!attrValue.selectable) {
            $attrValue.attr('disabled', true);
        }
    });
}

function updateAttrs(attrs, $productContainer, msgs) {
    // Currently, the only attribute type that has image swatches is Color.
    var attrsWithSwatches = ['color'];

    attrs.forEach(function (attr) {
        if (attrsWithSwatches.indexOf(attr.id) > -1) {
            processSwatchValues(attr, $productContainer, msgs);
        } else {
            processNonSwatchValues(attr, $productContainer);
        }
    });
}

function updateAvailability(response, $productContainer) {
    var availabilityValue = '';
    var availabilityMessages = response.product.availability.messages;
    if (!response.product.readyToOrder) {
        availabilityValue = '<li><div>' + response.resources.info_selectforstock + '</div></li>';
    } else {
        availabilityMessages.forEach(function (message) {
            availabilityValue += '<li><div>' + message + '</div></li>';
        });
    }

    $($productContainer).trigger('product:updateAvailability', {
        product: response.product,
        $productContainer: $productContainer,
        message: availabilityValue,
        resources: response.resources
    });
}

function getAttributesHtml(attributes) {
    if (!attributes) {
        return '';
    }

    var html = '';

    attributes.forEach(function (attributeGroup) {
        if (attributeGroup.ID === 'mainAttributes') {
            attributeGroup.attributes.forEach(function (attribute) {
                html += '<div class="attribute-values">' + attribute.label + ': '
                    + attribute.value + '</div>';
            });
        }
    });

    return html;
}
window.onload = function() {
    var el = document.querySelectorAll(".list-category");
    for (var i = 0; i < el.length; i++) {
        el[i].addEventListener("click", loadYotpoImage, false);
    }
    loadYotpoStars();
    loadTitleProduct();
  };
function loadTitleProduct(){
    var name = $('.product-detail .product-name').text();
    $('.write-review .yotpo-header-title').after('<div class="yotpo-product-title">'+name+'</div>');
    $('.write-review .submit-button input').attr('value', 'ENVIAR RESEÑA');
}
function loadYotpoImage (){
    var image = $('.row-container-pdp .container-img:nth-of-type(1) img').attr('src');
    if($('.yotpo-reviews')){
        setTimeout(function(){
            $('.yotpo-review .yotpo-header-element:nth-of-type(1)').append('<div class="yotpo-product-image"><img src="'+image+'"></div>');
            $('.write-review .yotpo-header').before('<div class="yotpo-product-image"><img src="'+image+'"></div>');
        },1000)
    }
}
function loadYotpoStars (){
    var rating = $('.yotpo-bottomline-2-boxes .sr-only').text().slice(0,3);
    if(rating !='0.0'){
        $('.yotpo-bottomline-2-boxes .yotpo-stars-and-sum-reviews').append('<div class="yotpo-general-rating">'+rating+'</div>');
    }
}
loadYotpoImage();
base.createCarousel= function (imgs, $productContainer) {
    var carousel = $productContainer.find('.carousel');
    $(carousel).carousel('dispose');
    var carouselId = $(carousel).attr('id');
    $(carousel).empty().append('<ol class="carousel-indicators"></ol><div class="carousel-inner" role="listbox"></div><a class="carousel-control-prev" href="#' + carouselId + '" role="button" data-slide="prev"><span class="fa icon-prev" aria-hidden="true"></span><span class="sr-only">' + $(carousel).data('prev') + '</span></a><a class="carousel-control-next" href="#' + carouselId + '" role="button" data-slide="next"><span class="fa icon-next" aria-hidden="true"></span><span class="sr-only">' + $(carousel).data('next') + '</span></a>');
    for (var i = 0; i < imgs.length; i++) {
        $('<div class="carousel-item"><img src="' + imgs[i].url + '" class="d-block img-fluid" alt="' + imgs[i].alt + ' image number ' + parseInt(imgs[i].index, 10) + '" title="' + imgs[i].title + '" itemprop="image" /></div>').appendTo($(carousel).find('.carousel-inner'));
        $('<li data-target="#' + carouselId + '" data-slide-to="' + i + '" class=""></li>').appendTo($(carousel).find('.carousel-indicators'));
    }
    $($(carousel).find('.carousel-item')).first().addClass('active');
    $($(carousel).find('.carousel-indicators > li')).first().addClass('active');
    if (imgs.length === 1) {
        $($(carousel).find('.carousel-indicators, a[class^="carousel-control-"]')).detach();
    }
    $(carousel).carousel();
    $($(carousel).find('.carousel-indicators')).attr('aria-hidden', true);
}

base.createGrid= function (imgs, $productContainer) {
    var grid = $productContainer.find('.primary-images-grid');
    var gridId = $(grid).attr('id');
    grid.html('').append(' <div class="row-container-pdp"></div>');
    for (var i = 0; i < imgs.length; i++) {
        $('<div class="container-img"><img src="' + imgs[i].url + '" alt="' + imgs[i].alt + ' image number ' + parseInt(imgs[i].index, 10) + '" title="' + imgs[i].title + '" itemprop="image" /></div>').appendTo($(grid).find('.row-container-pdp'));
    }
}


base.handleVariantResponse= function (response, $productContainer) {
    var isChoiceOfBonusProducts =
        $productContainer.parents('.choose-bonus-product-dialog').length > 0;
    var isVaraint;
    if (response.product.variationAttributes) {
        updateAttrs(response.product.variationAttributes, $productContainer, response.resources);
        isVaraint = response.product.productType === 'variant';
        if (isChoiceOfBonusProducts && isVaraint) {
            $productContainer.parent('.bonus-product-item')
                .data('pid', response.product.id);

            $productContainer.parent('.bonus-product-item')
                .data('ready-to-order', response.product.readyToOrder);
        }
    }

    // Update primary images
    var primaryImageUrls = response.product.images.large;
    base.createCarousel(primaryImageUrls, $productContainer);
    base.createGrid(primaryImageUrls, $productContainer);
    // Update pricing
    if (!isChoiceOfBonusProducts) {
        var $priceSelector = $('.prices .price', $productContainer).length
            ? $('.prices .price', $productContainer)
            : $('.prices .price');
        $priceSelector.replaceWith(response.product.price.html);
    }

    // Update promotions
    $productContainer.find('.promotions').empty().html(response.product.promotionsHtml);

    updateAvailability(response, $productContainer);

    if (isChoiceOfBonusProducts) {
        var $selectButton = $productContainer.find('.select-bonus-product');
        $selectButton.trigger('bonusproduct:updateSelectButton', {
            product: response.product, $productContainer: $productContainer
        });
    } else {
        // Enable "Add to Cart" button if all required attributes have been selected
        $('button.add-to-cart, button.add-to-cart-global, button.update-cart-product-global').trigger('product:updateAddToCart', {
            product: response.product, $productContainer: $productContainer
        }).trigger('product:statusUpdate', response.product);
    }

    // Update attributes
    $productContainer.find('.main-attributes').empty()
        .html(getAttributesHtml(response.product.attributes));
}

base.colorAttribute =function () {
    $(document).on('click', '[data-attr="color"] button', function (e) {
        e.preventDefault();

        if ($(this).attr('disabled')) {
            return;
        }
        var $productContainer = $(this).closest('.set-item');
        if (!$productContainer.length) {
            $productContainer = $(this).closest('.product-detail');
        }

        base.attributeSelect($(this).attr('data-url'), $productContainer);
    });
}
base.selectAttribute= function () {
    $(document).on('click', '.attr-select', function (e) {
        e.preventDefault();
        var attr = $(this).attr('data-attr');
        var $productContainer = $(this).closest('.set-item');
        if (!$productContainer.length) {
            $productContainer = $(this).closest('.product-detail');
        }
        base.attributeSelect($('#'+attr+'-1 option:nth-of-type('+($(this).parent().index()+2)+')').val(), $productContainer);
        $(this).parent().toggleClass('selected').siblings().removeClass('selected');
    });
}
base.arrows= function (){
    $('.arrow-attr-left, .arrow-attr-right').addClass('disabled');
    for(var i=0; i<$('.attr-list').length; i++){
        arrowSize($('.attr-list:eq('+i+')'),1, 'root');
    }
    $(document).on('click', '.arrow-attr-right', function (){
        var count = parseFloat($(this).siblings('.attr-list').attr('data-count'));
        arrowSize($(this).siblings('.attr-list'),count, 'right');
        count = count+1;
        $(this).siblings('.attr-list').attr('data-count', count);
        if(count===$(this).siblings('.attr-list').children('li').length){
            $(this).addClass('disabled');
        }
    });
    $(document).on('click', '.arrow-attr-left', function (){
        var count = parseFloat($(this).siblings('.attr-list').attr('data-count'));
        arrowSize($(this).siblings('.attr-list'),count, 'left');
        count = count-1;
        $(this).siblings('.attr-list').attr('data-count', count);
        if(count===1){
            $(this).addClass('disabled');
        }
    });
}
base.attributeSelect= function (selectedValueUrl, $productContainer) {
    if (selectedValueUrl) {
        $('body').trigger('product:beforeAttributeSelect',
            { url: selectedValueUrl, container: $productContainer });

        $.ajax({
            url: selectedValueUrl,
            method: 'GET',
            success: function (data) {
                base.handleVariantResponse(data, $productContainer);
                updateOptions(data.product.optionsHtml, $productContainer);
                updateQuantities(data.product.quantities, $productContainer);
                $('body').trigger('product:afterAttributeSelect',
                    { data: data, container: $productContainer });
                $.spinner().stop();
            },
            error: function () {
                $.spinner().stop();
            }
        });
    }
}
base.arrows();
base.colorAttribute();
base.selectAttribute();
module.exports = base;