'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
    processInclude(require('./search/search'));
    processInclude(require('wishlists/product/wishlistHeart'));
    processInclude(require('./product/quickView'));
    processInclude(require('./product/base'));
});
