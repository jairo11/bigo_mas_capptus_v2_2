//ValidateZip.js
'use strict';

var server = require('server');
var userLoggedIn = require("*/cartridge/scripts/middleware/userLoggedIn");
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');

var CustomerMgr = require('dw/customer/CustomerMgr');
var addressHelpers = require('*/cartridge/scripts/helpers/addressHelpers');


server.post('GetAddress', server.middleware.post, server.middleware.https, userLoggedIn.validateLoggedIn, csrfProtection.validateAjaxRequest, function (req, res, next) {
    var hookMgr = require("dw/system/HookMgr");

    var currentCustomer = req.currentCustomer || {};
    var profile = currentCustomer.profile || {};
    var customerNo = profile.customerNo || '';
    var customer = CustomerMgr.getCustomerByCustomerNumber(customerNo) || {};
    var addressBook = customer.addressBook || {};
    var addresses = addressBook.addresses || [];

    var form = req.form;
    var idSelected = form.addressSelected;
    var selectedAddress = {};
    var size = addresses.length;
    for (var i = 0; i < size; i++) {
        var address = addresses[i];
        if (('ab_' + address.ID) === idSelected) {
            selectedAddress = addressHelpers.copyShippingAddress(address);
            break;
        }
    }

    var suburbsResponse = {};
    var suburbsArray = [];
    if (hookMgr.hasHook("app.services.cya.brokerDelivery")) {
        suburbsResponse = hookMgr.callHook("app.services.cya.brokerDelivery", "getValidatePostalCode", selectedAddress.postalCode);
    }
    if (suburbsResponse.ResponseCode && suburbsResponse.ResponseCode == 200){
        suburbsArray = suburbsResponse.postalCode[0].suburbs;
    }

    res.json({ success: true, message: 'response from EditAdress', form: req.form, addresses: addresses, selectedAddress: selectedAddress, suburbsArray: suburbsArray });
    return next();
});

module.exports = server.exports();
