'use strict';

var server = require('server');
server.extend(module.superModule);

var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var serviceOrderTracking = require("*/cartridge/scripts/services/apiOrder");


/**
 * Order-Details : This endpoint is called to get Track Order Details
 * @name Base/Order-Details
 * @function
 * @memberof Order
 * @param {middleware} - consentTracking.consent
 * @param {middleware} - server.middleware.https
 * @param {middleware} - userLoggedIn.validateLoggedIn
 * @param {querystringparameter} - orderID - Order ID
 * @param {querystringparameter} - orderFilter - Order Filter ID
 * @param {category} - sensitive
 * @param {serverfunction} - get
 */
server.get(
    'Tracking',
    consentTracking.consent,
    server.middleware.https,
    function (req, res, next) {
        var collections = require('*/cartridge/scripts/util/collections');
        var URLUtils = require('dw/web/URLUtils');
        var order = {
            orderNo:req.querystring.orderID
        };
        var service = serviceOrderTracking.getTrackingOrder(order);
        service.delivered=false;
        
        if(service.history){
            for (let index = 0; index < service.history.length; index++) {
                let element = service.history[index].date;
                service.history[index].dateShort=element.substring(0, 10);
                service.history[index].time=element.substring(11, 19);;
                if(service.history[index].currentSituation === "ENTREGADO")    service.delivered=true;

            }
        }

        res.render('account/order/orderTracking', {
            tracking: service
        });
        next();
    }
);

module.exports = server.exports();
