'use strict';

/**
 * @namespace Stores
 */

var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var storeHelpers = require('*/cartridge/scripts/helpers/storeHelpers');
var Site = require("dw/system/Site");

server.extend(module.superModule);

/**
 * Stores-Find : This endpoint is used to load the Find Stores page
 * @name Base/Stores-Find
 * @function
 * @memberof Stores
 * @param {middleware} - server.middleware.https
 * @param {middleware} - cache.applyDefaultCache
 * @param {middleware} - consentTracking
 * @param {querystringparameter} - radius - The radius that the shopper selected to refine the search
 * @param {querystringparameter} - postalCode - The postal code that the shopper used to search
 * @param {querystringparameter} - lat - The latitude of the shopper position
 * @param {querystringparameter} - long - The longitude of the shopper position
 * @param {querystringparameter} - showMap - A flag indicating whether or not map is to be shown
 * @param {querystringparameter} - horizontalView - Boolean value to show map in Horizontal View
 * @param {querystringparameter} - isForm - Boolean value to show (or not) the form to Find Stores
 * @param {category} - non-sensitive
 * @param {serverfunction} - get
 */
server.replace('Find', server.middleware.https, cache.applyDefaultCache, consentTracking.consent, function (req, res, next) {
    var radius = Site.getCurrent().getCustomPreferenceValue("storeFocusKmRadius");
    var postalCode = req.querystring.postalCode;
    var lat = req.querystring.lat;
    var long = req.querystring.long;
    var showMap = req.querystring.showMap || false;
    var horizontalView = req.querystring.horizontalView || false;
    var isForm = req.querystring.isForm || false;

    var stores = storeHelpers.getStores(radius, postalCode, lat, long, req.geolocation, showMap);

    var viewData = {
        stores: stores,
        horizontalView: horizontalView,
        isForm: isForm,
        showMap: showMap
    };

    res.render('storeLocator/storeLocator', viewData);
    next();
});

module.exports = server.exports();
