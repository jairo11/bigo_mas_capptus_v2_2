'use strict';

var server = require('server');

var Logger = require('dw/system/Logger').getLogger('EmailTest', 'EmailTest');


// eslint-disable-next-line require-jsdoc
function sendOrderEmailTest(order, locale, type) {
    var Resource = require('dw/web/Resource');
    var Site = require('dw/system/Site');
    var OrderModel = require('*/cartridge/models/order');
    var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');
    var Locale = require('dw/util/Locale');

    var currentLocale = Locale.getLocale(locale);

    var templateType = '';

    var orderModel = new OrderModel(order, { countryCode: currentLocale.country, containerView: 'order' });

    var orderObject = { order: orderModel };

    var emailObj = {
        to: order.customerEmail,
        subject: Resource.msg('subject.order.confirmation.email', 'order', null),
        from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@testorganization.com',
        type: emailHelpers.emailTypes.orderConfirmation
    };

    switch (type) {
        case 4:
            emailObj.subject = Resource.msg('subjec.order.TDC.email', 'order', null);
            templateType = 'checkout/confirmation/confirmationEmail';
            break;
        case 7:
            emailObj.subject = Resource.msg('subject.order.confirmed.title', 'order', null); // change resource message properties
            templateType = 'email/confirmedPaymentOrderEmail';
            break;
        case 8:
            emailObj.subject = Resource.msg('subject.cancelled.order.title', 'order', null); // change resource message properties
            templateType = 'email/canceledCashPaymentOrderEmail';
            break;
        case 9:
            emailObj.subject = Resource.msg('subject.order.in.process', 'order', null); // change resource message properties
            templateType = 'email/onRouteOrderEmail';
            break;
        case 10:
            emailObj.subject = Resource.msg('subject.order.in.arrived', 'order', null); // change resource message properties
            templateType = 'email/deliveredOrderEmail';
            break;
        case 11:
            emailObj.subject = Resource.msg('subjec.order.return.email', 'order', null); // change resource message properties
            templateType = 'email/confirmedReturnEmail';
            break;
        case 12:
            emailObj.subject = Resource.msg('subjec.order.rejected.returned.email', 'order', null); // change resource message properties
            templateType = 'email/rejectedReturnEmail';
            break;
        case 13:
            emailObj.subject = Resource.msg('subjec.order.completed.returned.email', 'order', null); // change resource message properties
            templateType = 'email/completedReturnEmail';
            break;
        case 14:
            emailObj.subject = Resource.msg('subjec.order.accepted.returned.email', 'order', null); // change resource message properties
            templateType = 'email/acceptedReturnEmail';
            break;
        case 16:
            emailObj.subject = Resource.msg('subject.order.confirmed.order.broker.email', 'order', null); // change resource message properties
            templateType = 'email/confirmedOrderBrokerEmail';
            break;
        case 17:
            emailObj.subject = Resource.msg('subject.cancelled.order.title', 'order', null); // change resource message properties
            templateType = 'email/canceledOrderBrokerEmail';
            break;
        case 18:
            emailObj.subject = Resource.msg('subject.order.processed.email', 'order', null); // change resource message properties
            templateType = 'email/processedOrderEmail';
            break;
        case 19:
            emailObj.subject = Resource.msg('subject.cancelled.order.title', 'order', null); // change resource message properties
            templateType = 'email/canceledOrderEmail';
            break;
        case 20:
            emailObj.subject = Resource.msg('subject.order.validation.email', 'order', null); // change resource message properties
            templateType = 'email/inValidationOrderEmail';
            break;
        case 21:
            emailObj.subject = Resource.msg('subject.order.confirmed.validation.email', 'order', null); // change resource message properties
            templateType = 'email/confirmedValidationOrderEmail';
            break;
        case 22:
            emailObj.subject = Resource.msg('subject.canceled.validation.order.email', 'order', null); // change resource message properties
            templateType = 'email/canceledValidationOrderEmail';
            break;
        default:
            break;
    }

    emailHelpers.sendEmail(emailObj, templateType, orderObject);
}

server.post('Test', function (req, res, next) {
    var OrderMgr = require('dw/order/OrderMgr');
    var payload = {};

    try {
        payload = JSON.parse(request.httpParameterMap.requestBodyAsString);
    } catch (error) {
        Logger.error('Unable parse payload: ' + error.message);
        res.setStatusCode(500);
        return;
        // eslint-disable-next-line no-unreachable
        next();
    }

    var orderNo = payload.orderNo;
    var locale = payload.locale;
    var type = payload.type;

    var order = OrderMgr.getOrder(orderNo);

//    try {
        sendOrderEmailTest(order, locale, type);
//    } catch (error) {
//        Logger.error('Unable send email ' + error.message);
//        res.setStatusCode(500);
//        return;
        // eslint-disable-next-line no-unreachable
//        next();
//    }
    response.setStatus(200);
    res.render("checkout/moNotification.isml");
    next();
});


module.exports = server.exports();
