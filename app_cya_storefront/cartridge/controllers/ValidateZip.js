//ValidateZip.js
'use strict';

var server = require('server');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');

server.post(
    'ValidateCodePostal',
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var Resource = require("dw/web/Resource");
        var hookMgr = require("dw/system/HookMgr");
        var serverErrors = [];
        var fieldErrors = {};
        var error = false;

        try{
            var response;
            if (hookMgr.hasHook("app.services.cya.brokerDelivery")) {
                response = hookMgr.callHook("app.services.cya.brokerDelivery", "getValidatePostalCode", req.form.zip);
            }

            if(response.ResponseCode != 200){
                error = true;
                serverErrors.push(
                    Resource.msg('error.technical', 'checkout', null)
                );
                return { fieldErrors: fieldErrors, serverErrors: serverErrors, error: error };
            }
            //* ialvarez@ts4.mx validate if we have coverage in the postal code entered
            var postalCodes = response.postalCode || [];
            var postalCode = postalCodes[0] || {};
            var hasCoverage = postalCode.haveCoverage || false;
            var messageZip = '';

            if (!hasCoverage) {
                postalCode = {};
                messageZip = Resource.msg('error.zip.not.coverage', 'checkout', null);
            }
            res.json({
                success: 'success',
                info: postalCode,
                hasCoverage: hasCoverage,
                message: messageZip
            });

            return next();
        } catch (e) {
            error = true;
            serverErrors.push(
                Resource.msg('error.technical', 'checkout', null)
            );
            return { fieldErrors: fieldErrors, serverErrors: serverErrors, error: error };
        }
    }
);

module.exports = server.exports();
