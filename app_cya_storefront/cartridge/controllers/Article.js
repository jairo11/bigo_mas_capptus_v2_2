'use strict';

/**
 * @namespace Articles
 */

var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var categoriesMiddleware = require('*/cartridge/scripts/middleware/kb/categories');
var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');

/**
 * Any customization on this endpoint, also requires update for Default-Start endpoint
 */
/**
 * Home-Show : This endpoint is called when a shopper navigates to the home page
 * @name Base/Articles-Show
 * @function
 * @memberof Articles
 * @param {middleware} - categoriesMiddleware.categories
 * @param {middleware} - consentTracking.consent
 * @param {middleware} - cache.applyDefaultCache
 * @param {category} - non-sensitive
 * @param {renders} - isml
 * @param {serverfunction} - get
 */
server.get(
    'Show',
    categoriesMiddleware.categories,
    consentTracking.consent,
    cache.applyDefaultCache,
    function (req, res, next) {
        var Site = require('dw/system/Site');
        var PageMgr = require('dw/experience/PageMgr');
        var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');
        var articleSlug = req.querystring.slug;

        var HookMgr = require('dw/system/HookMgr');
        var article = HookMgr.callHook('app.hook.services.kb.category.article', 'getCategoryArticle', articleSlug)
        article = JSON.parse(article.object);

        if (!article) {
            article = {};
        }

        if (!article.hasOwnProperty('layoutItems')) {
            res.render('error/notFound');
            next();
        } else {
            article.layoutItems.forEach(element => {
                if (element.name === 'Detalle_del_articulo__c') {
                    article.content = element.value
                }
            });

            pageMetaHelper.setPageMetaTags(req.pageMetaData, Site.current);
            res.render('articles/article', {
                article: article
            });
            next();
        }

    }, pageMetaData.computedPageMetaData);

server.get('ErrorNotFound', function (req, res, next) {
    res.setStatusCode(404);
    res.render('error/notFound');
    next();
});

module.exports = server.exports();
