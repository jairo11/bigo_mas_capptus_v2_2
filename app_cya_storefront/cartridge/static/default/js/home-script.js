/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./cartridges/app_cya_storefront/cartridge/client/default/js/home-script.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./cartridges/app_cya_storefront/cartridge/client/default/js/home-script.js":
/*!**********************************************************************************!*\
  !*** ./cartridges/app_cya_storefront/cartridge/client/default/js/home-script.js ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

let searchIcon= document.querySelector('.search-mobile .only-button'),
    headerElements = document.querySelector('.header-elements'),
    searchDesktop = document.querySelector('.search-bar .search-field'),
    closeSearchDesk = document.querySelector('.header-elements .icon-close'),
    closeSearch= document.querySelector('.search-mobile .reset-button'),
    elements= document.querySelector('.mobile-elements'),
    title= document.querySelectorAll('.custom-text-title'),
    paragraph= document.querySelectorAll('.custom-text-paragraph');

if(searchIcon){
    searchIcon.onclick = ()=>{
        elements.classList.add('hide-mobile');
        document.getElementById('logo-mobile').style.display = "none";
        document.getElementById("brand-logo").style.zIndex = "3";
    }
    closeSearch.onclick = ()=>{
        elements.classList.remove('hide-mobile');
        document.getElementById('logo-mobile').style.display = "initial"; 
        document.getElementById("brand-logo").style.zIndex = "4";
    }
    for (const [a,b] of title.entries()) {
        title[a].onclick = ()=>{
            if(title[a].classList.contains('show-mobile')){
                title[a].classList.remove('show-mobile');
                paragraph[a].classList.remove('hide-mobile');
            }else{
                title[a].classList.add('show-mobile');
                paragraph[a].classList.add('hide-mobile');
            }
        }

    }
}
if(searchDesktop){
    searchDesktop.onclick = ()=>{
        headerElements.classList.add('open-search');
        document.querySelector('.navbar-header.icons').classList.add('col-lg-11');
        document.querySelector('.search-bar').classList.add('col-lg-10');
        document.querySelector('.login-header').classList.remove('col-lg-5');
        document.querySelector('.login-header').classList.add('col-lg-1');
        $('#maincontent').addClass('blur');
    }
    closeSearchDesk.onclick = ()=>{
        headerElements.classList.remove('open-search');
        document.querySelector('.navbar-header.icons').classList.remove('col-lg-11');
        document.querySelector('.search-bar').classList.remove('col-lg-10');
        document.querySelector('.login-header').classList.add('col-lg-5');
        document.querySelector('.login-header').classList.remove('col-lg-1');
        $('#maincontent').removeClass('blur');
    }
}

// set email at homePage to modal Newsletter
$(document).on('click', '#btnNewsletter', function () {
    var valor=document.getElementById("hpEmailSignUp").value;
    $("#newsletter-email").val(valor);
});

$(document).on('click', '#btnNewsletterFooter', function () {
    var valor=document.getElementById("emailNewslettter").value;
    $("#newsletter-email").val(valor);
});

$(document).on('click', '#btnSaveNewsLetter', function () {
    campoVacio();
});

function campoVacio(obj){
    var correcto = true;
    var email=document.getElementById("newsletter-email").value;
    var date=document.getElementById("newsletter-birthdate").value;
    var interest=document.getElementById("newsletter-preferences").value
    if(email && date && interest){
        $('#gratefulnessModal').modal('show');
        $('#newsletterModal').modal('hide');
    }
};

/***/ })

/******/ });
//# sourceMappingURL=home-script.js.map