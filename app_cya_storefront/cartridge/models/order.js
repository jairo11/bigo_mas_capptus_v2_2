'use strict';

var base = module.superModule;

/**
 * Order class that represents the current order
 * @param {dw.order.LineItemCtnr} lineItemContainer - Current users's basket/order
 * @param {Object} options - The current order's line items
 * @param {Object} options.config - Object to help configure the orderModel
 * @param {string} options.config.numberOfLineItems - helps determine the number of lineitems needed
 * @param {string} options.countryCode - the current request country code
 * @constructor
 */
function OrderModel(lineItemContainer, options) {
    var statusOrder= [
        {id:0, valor:"Creado"},
        {id:1, valor:""},
        {id:2, valor:""},
        {id:3, valor:"Nuevo"},
        {id:4, valor:"En proceso"},
        {id:5, valor:"Completado"},
        {id:6, valor:"Cancelado"},
        {id:7, valor:"Reemplazado"},
        {id:8, valor:"Fallido"}
    ];
    base.call(this, lineItemContainer, options);
    this.guideShipping = lineItemContainer.shipments[0].trackingNumber
    ? lineItemContainer.shipments[0].trackingNumber
    : null;
    this.cancelDescription = Object.hasOwnProperty.call(lineItemContainer, 'cancelDescription')
        ? lineItemContainer.cancelDescription
        : null;

        var status= this.orderStatus;
    
    if(status){
        this.paymentStatus= lineItemContainer.paymentStatus ? lineItemContainer.paymentStatus : null;
        if(lineItemContainer.shippingStatus == 0 && (status == 4 || status == 3 || status == 0 ) && lineItemContainer.paymentStatus == 2){
            this.orderStatus = {displayValue: "En proceso",
            value: status.value};
        }else
        if(lineItemContainer.shippingStatus == 0 && (status == 4 || status == 3 || status == 0 ) && lineItemContainer.paymentStatus == 1){
            this.orderStatus = {displayValue: "Por pagar",
            value: status.value};
        }else
        if(lineItemContainer.shippingStatus == 2 && (status == 5 ) && lineItemContainer.paymentStatus == 2){
            this.orderStatus = {displayValue: "Entregado",
            value: status.value};
        }else{
            this.orderStatus = {displayValue:statusOrder[status].valor,
            value: status.value};
        }
    }
    
}

module.exports = OrderModel;
