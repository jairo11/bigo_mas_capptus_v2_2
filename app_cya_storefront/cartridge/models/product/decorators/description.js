'use strict';

var base = module.superModule;

module.exports = function (object, product) {
    base.call(this, object, product);
    Object.defineProperty(object, 'productModel', {
        enumerable: true,
        value: product.custom.productModel ? product.custom.productModel : null
    });

    Object.defineProperty(object, 'modelCintura', {
        enumerable: true,
        value: product.custom.modelCintura ? product.custom.modelCintura : null
    });


    Object.defineProperty(object, 'modelForro', {
        enumerable: true,
        value: product.custom.modelForro ? product.custom.modelForro : null
    });


    Object.defineProperty(object, 'modelSilueta', {
        enumerable: true,
        value: product.custom.modelSilueta ? product.custom.modelSilueta : null
    });


    // Object.defineProperty(object, 'SiluetAttributes', {
    //     enumerable: true,
    //     value: product.custom.SiluetAttributes ? product.custom.SiluetAttributes : null
    // });

    Object.defineProperty(object, 'siluetLength', {
        enumerable: true,
        value: product.custom.SiluetLength ? product.custom.SiluetLength : null
    });

    Object.defineProperty(object, 'modelBolsillos', {
        enumerable: true,
        value: product.custom.modelBolsillos ? product.custom.modelBolsillos : null
    });


    Object.defineProperty(object, 'modelCuello', {
        enumerable: true,
        value: product.custom.modelCuello ? product.custom.modelCuello : null
    });


    Object.defineProperty(object, 'modelManga', {
        enumerable: true,
        value: product.custom.modelManga ? product.custom.modelManga : null
    });


    Object.defineProperty(object, 'modelPack', {
        enumerable: true,
        value: product.custom.modelPack ? product.custom.modelPack : null
    });


    Object.defineProperty(object, 'modelPierna', {
        enumerable: true,
        value: product.custom.modelPierna ? product.custom.modelPierna : null
    });


    Object.defineProperty(object, 'modelSuela', {
        enumerable: true,
        value: product.custom.modelSuela ? product.custom.modelSuela : null
    });


    Object.defineProperty(object, 'modelTacon', {
        enumerable: true,
        value: product.custom.modelTacon ? product.custom.modelTacon : null
    });


    Object.defineProperty(object, 'TaconAltura', {
        enumerable: true,
        value: product.custom.TaconAltura ? product.custom.TaconAltura : null
    });


    Object.defineProperty(object, 'SiluetLength', {
        enumerable: true,
        value: product.custom.SiluetLength ? product.custom.SiluetLength : null
    });


    Object.defineProperty(object, 'RoyaltyMarca', {
        enumerable: true,
        value: product.custom.RoyaltyMarca ? product.custom.RoyaltyMarca : null
    });


    Object.defineProperty(object, 'RoyaltySubmarca', {
        enumerable: true,
        value: product.custom.RoyaltySubmarca ? product.custom.RoyaltySubmarca : null
    });


    Object.defineProperty(object, 'PlataformaCampania', {
        enumerable: true,
        value: product.custom.PlataformaCampania ? product.custom.PlataformaCampania : null
    });


    Object.defineProperty(object, 'TamanoModelo', {
        enumerable: true,
        value: product.custom.TamanoModelo ? product.custom.TamanoModelo : null
    });


    Object.defineProperty(object, 'modelCintura', {
        enumerable: true,
        value: product.custom.modelCintura ? product.custom.modelCintura : null
    });


    Object.defineProperty(object, 'materialSustentable', {
        enumerable: true,
        value: product.custom.materialSustentable ? product.custom.materialSustentable : null
    });


    Object.defineProperty(object, 'modelPuno', {
        enumerable: true,
        value: product.custom.modelPuno ? product.custom.modelPuno : null
    });

    Object.defineProperty(object, 'cuidadosPrenda', {
        enumerable: true,
        value: product.custom.cuidadosPrenda ? product.custom.cuidadosPrenda : null
    });

    Object.defineProperty(object, 'Composicion', {
        enumerable: true,
        value: product.custom.Composicion ? product.custom.Composicion : null
    });

};
