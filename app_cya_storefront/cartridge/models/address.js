'use strict';

/**
 * creates a plain object that contains address information
 * @param {dw.order.OrderAddress} addressObject - User's address
 * @returns {Object} an object that contains information about the users address
 */
function createAddressObject(addressObject) {
    var result;
    if (addressObject) {
        result = {
            address1: addressObject.address1,
            address2: addressObject.address2,
            city: addressObject.city,
            firstName: addressObject.firstName,
            lastName: addressObject.lastName,
            ID: Object.hasOwnProperty.call(addressObject, 'ID')
                ? addressObject.ID : (Object.hasOwnProperty.call(addressObject, 'custom') ? addressObject.custom.customerAddresId : '' ),
            addressId: Object.hasOwnProperty.call(addressObject, 'ID')
                ? addressObject.ID : (Object.hasOwnProperty.call(addressObject, 'custom') ? addressObject.custom.customerAddresId : '' ),
            phone: addressObject.phone,
            postalCode: addressObject.postalCode,
            stateCode: addressObject.stateCode,
            jobTitle: addressObject.jobTitle,
            postBox: addressObject.postBox,
            salutation: addressObject.salutation,
            secondName: addressObject.secondName,
            companyName: addressObject.companyName,
            suffix: addressObject.suffix,
            suite: addressObject.suite,
            title: addressObject.title,
            UUID: addressObject.UUID,
            county: addressObject.hasOwnProperty('county') ? addressObject.county : (Object.hasOwnProperty.call(addressObject, 'custom') ? addressObject.custom.county : '' ),
            numberInt: addressObject.hasOwnProperty('numberInt') ? addressObject.numberInt : (Object.hasOwnProperty.call(addressObject, 'custom') ? addressObject.custom.numberInt : '' ),
            numberExt: addressObject.hasOwnProperty('numberExt') ? addressObject.numberExt : (Object.hasOwnProperty.call(addressObject, 'custom') ? addressObject.custom.numberExt : '' ),
            suburb: addressObject.hasOwnProperty('suburb') ? addressObject.suburb : (Object.hasOwnProperty.call(addressObject, 'custom') ? addressObject.custom.suburb : '' ),
            saveAddress:(Object.hasOwnProperty.call(addressObject, 'custom') && Object.hasOwnProperty.call(addressObject.custom, 'saveAddress') ? addressObject.custom.saveAddress : '' ),
        };

        if (result.stateCode === 'undefined') {
            result.stateCode = '';
        }

        if (Object.hasOwnProperty.call(addressObject, 'countryCode')) {
            result.countryCode = {
                displayValue: addressObject.countryCode.displayValue,
                value: addressObject.countryCode.value.toUpperCase()
            };
        }
    } else {
        result = null;
    }
    return result;
}

/**
 * Address class that represents an orderAddress
 * @param {dw.order.OrderAddress} addressObject - User's address
 * @constructor
 */
function address(addressObject) {
    this.address = createAddressObject(addressObject);
}

module.exports = address;
