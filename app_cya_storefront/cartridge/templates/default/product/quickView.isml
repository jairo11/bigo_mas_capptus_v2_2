<isscript>
        var assets = require('*/cartridge/scripts/assets');
        assets.addJs('/js/customProductDetails.js');
</isscript>
<isset name="productCopy" value="${pdict.product}" scope="page"/>
<isset name="product" value="${pdict.product}" scope="page"/>
<isset name="isBundle" value="${false}" scope="page"/>
<isset name="isQuickView" value="${true}" scope="page"/>
<isset name="isProductSet" value="${false}" scope="page" />
<isset name="loopState" value="${{count: 1}}" scope="page" />
<isscript>
    var customProductLink = product.selectedProductUrl.split("&quantity");
</isscript>
<div class="product-quickview product-${product.productType} col product-wrapper product-detail" data-pid="${product.id}">
    <button type="button" class="close pull-right" data-dismiss="modal"><span class="fa fa-times" aria-hidden="true"></span><span class="sr-only">Cerrar</span></button>
    <div class="row">
        <!-- Product Images Carousel -->
        <div class="primary-images col-12 col-sm-6 product-quickview__gallery">
            <div id="pdpCarousel-${product.id}" class="carousel slide" data-interval="0" data-prev="${Resource.msg('button.previous', 'common', null)}" data-next="${Resource.msg('button.next', 'common', null)}">
                <div class="thumbnails">
                    <isif condition="${product.images['small'].length > 1}">
                        <ol class="thumbnails__items" aria-hidden="true">
                            <isloop items="${product.images['small']}" var="image" status="loopStatus">
                                <li data-target="${"#"}pdpCarousel-${product.id}" data-slide-to="${loopStatus.index}" class='thumbnails__item <isif condition="${loopStatus.index == 0}">active</isif>'>
                                    <img src="${image.url}" alt="${image.alt} image number ${image.index}" itemprop="image" />
                                </li>
                            </isloop>
                        </ol>
                    </isif>
                </div>

                <div class="carousel-inner image-large" role="listbox">
                    <isloop items="${product.images['large']}" var="image" status="loopStatus">
                        <div class='carousel-item image-large__item <isif condition="${loopStatus.index == 0}">active</isif>'>
                            <img src="${image.url}" class="d-block img-fluid" alt="${image.alt} image number ${image.index}" itemprop="image" />
                        </div>
                    </isloop>
                </div>
            </div>
        </div>

        <!-- Product Name and Number -->
        <div class="col-sm-6 product-quickview__info">
            <div class="row">
                <section>
                    <h1 class="product-quickview__product-name">${product.productName}</h1>
                </section>
            </div>
            <iscomment> <div style="background-color: purple" class="ratings pull-right"> </iscomment>
            <div class="ratings">
                <span class="sr-only">${Resource.msgf('label.product.ratings', 'common', null, product.rating)}</span>
                <isinclude template="product/components/productRating" />
                <span class="reviews-value">${product.rating}</span>
                <a href="${customProductLink[0]+"#tab-B"}" class="write-review">${Resource.msgf('label.product.write.review', 'common', null, product.rating)}</a>
            </div>

            <!-- Prices -->
            <div class="product-quickview__prices">
                <span>${Resource.msg('text.price', 'search', null)}</span>
                <isset name="price" value="${product.price}" scope="page" />
                <isinclude template="product/components/pricing/main" />
            </div>

            <div class="product-number pull-left">
                <isinclude template="product/components/productNumber" />
            </div>

            <div class="detail-panel">
                <!-- Attributes -->
                <section class="product-quickview__attributes">
                    <!-- Applicable Promotions -->
                    <div class="product-quickview__attributes--promotions">
                        <div class="col-12 promotions">
                            <div class="align-self-center">
                                <isinclude template="product/components/promotions" />
                            </div>
                        </div>
                    </div>

                    <isinclude template="product/components/mainAttributes" />

                        <isloop items="${product.variationAttributes}" var="attr" status="attributeStatus">
                            <div data-attr="${attr.id}" class="swatch row">
                                <div class="col-8 product-quickview__attribute">
                                    <isinclude template="product/components/variationAttribute" />
                                </div>

                                <isif condition="${attributeStatus.last}">
                                    <!-- Quantity Drop Down Menu -->
                                    <div class="attribute quantity col-4 d-sm-none">
                                        <isif condition="${pdict.addToCartUrl || pdict.updateCartUrl}">
                                            <isinclude template="product/components/quantity" />
                                        </isif>
                                    </div>
                                </isif>
                            </div>
                        </isloop>

                        <div class="d-sm-none row availability align-self-end " data-ready-to-order="${product.readyToOrder}" data-available="${product.available}">
                            <isinclude template="product/components/availability" />
                        </div>
                </section>

                <isif condition="${product.productType !== 'bundle'}">
                    <!-- Options -->
                    <isif condition="${product.options && product.options.length > 0}">
                            <isinclude template="product/components/options" />
                    </isif>
                </isif>
            </div>
        </div>
    </div>

    <isif condition="${product.productType === 'bundle'}">
        <div class="hidden-xs-down">
        </div>
        <isinclude template="product/components/bundleItems" />

        <!-- Quantity Drop Down Menu -->
        <div class="row d-sm-none">
            <div class="quantity col-10 mx-auto">
                <isinclude template="product/components/quantity" />
            </div>
        </div>

        <!-- Availability -->
        <div class="row d-sm-none">
            <div class="col-11 mx-auto availability" data-ready-to-order="${product.readyToOrder}" data-available="${product.available}">
                <isinclude template="product/components/availability" />
            </div>
        </div>
    </isif>

    <isset name="product" value="${productCopy}" scope="page"/>
</div>
<div class="modal-footer row align-items-end">
    <isset name="loopState" value="${{count: 1}}" scope="page" />

    <div class="hidden-xs-down col availability align-self-end global-availability product-quickview__availability" data-ready-to-order="${product.readyToOrder}" data-available="${product.available}">
        <div class="row">
            <isinclude template="product/components/availability" />
        </div>
    </div>

    <div class="col-6 product-quickview__quantity">
        <div class="row align-items-end">
            <!-- Quantity -->
            <div class="hidden-xs-down col-12">
                <isif condition="${pdict.addToCartUrl || pdict.updateCartUrl}">
                    <isinclude template="product/components/quantity" />
                </isif>
            </div>

            <div class="col-12 mx-auto">
                <!-- Cart and [Optionally] Apple Pay -->
                <isif condition="${pdict.addToCartUrl}">
                    <isinclude template="product/components/addToCartGlobal" />
                <iselse>
                    <isinclude template="product/components/updateProduct" />
                </isif>
            </div>
        </div>
    </div>
</div>
