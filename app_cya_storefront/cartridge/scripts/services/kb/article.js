'use strict'

var Logger = require('dw/system/Logger');
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

var kbCategoryArticle = LocalServiceRegistry.createService('cya.kb.api.category.article', {
    createRequest: function (svc, args) {
        var article = args;
        var url = svc.configuration.credential.URL.replace('{article}', encodeURIComponent(article));
        var token = svc.configuration.credential.password;
        svc.setRequestMethod('GET');
        svc.setURL(url);
        svc.addHeader('Authorization', 'Bearer ' + token);
        svc.addHeader('Content-Type', 'application/json');
        svc.addHeader('Accept-language', 'es-MX');
    },

    parseResponse: function (svc, client) {
        return client.text
    },

    mockCall: function () {
        return {
            statusCode: 200,
            statusMessage: 'Success'
        }
    }
});

module.exports.kbCategoryArticle = kbCategoryArticle;
