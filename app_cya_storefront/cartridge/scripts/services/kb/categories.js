'use strict'

var Logger = require('dw/system/Logger');
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

var kbCategories = LocalServiceRegistry.createService('cya.kb.api.categories', {
    createRequest: function (svc, args) {
        var url = svc.configuration.credential.URL;
        var token = svc.configuration.credential.password;
        svc.setRequestMethod('GET');
        svc.setURL(url);
        svc.addHeader('Authorization', 'Bearer ' + token);
        svc.addHeader('Content-Type', 'application/json');
    },

    parseResponse: function (svc, client) {
        return client.text
    },

    mockCall: function () {
        return {
            statusCode: 200,
            statusMessage: 'Success'
        }
    }
});

module.exports.kbCategories = kbCategories;
