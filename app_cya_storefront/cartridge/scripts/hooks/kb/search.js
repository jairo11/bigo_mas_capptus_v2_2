'use strict';

var Rest = require('*/cartridge/scripts/services/kb/search');

function search(q) {
    return Rest.search.call(q);
}

exports.search = search;
