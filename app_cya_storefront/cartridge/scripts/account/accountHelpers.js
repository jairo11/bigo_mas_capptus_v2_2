'use strict';

/**
 * Creates an account model for the current customer
 * @param {Object} req - local instance of request object
 * @returns {Object} a plain object of the current customer's account
 */
function getAccountModel(req) {
    var AccountModel = require('*/cartridge/models/account');
    var AddressModel = require('*/cartridge/models/address');
    var orderHelpers = require('*/cartridge/scripts/order/orderHelpers');

    var preferredAddressModel;

    if (!req.currentCustomer.profile) {
        return null;
    }

    var orderModel = orderHelpers.getLastOrder(req);

    if (req.currentCustomer.addressBook.preferredAddress) {
        preferredAddressModel = new AddressModel(req.currentCustomer.addressBook.preferredAddress.raw);
    } else {
        preferredAddressModel = null;
    }
    var x=req.currentCustomer;
    if(x.raw.profile.birthday){
        var birthday=x.raw.profile.birthday;
        var cumple=birthday.toUTCString();
        var cumple2=cumple.split(' ');
        if(cumple2[2]=="Jan"){
            cumple2.splice(2,0, '01');
        }
        else if(cumple2[2]=="Feb"){
            cumple2.splice(2,0, '02');
        }
        else if(cumple2[2]=="Mar"){
            cumple2.splice(2,0, '03');
        }
        else if(cumple2[2]=="Apr"){
            cumple2.splice(2,0, '04');
        }
        else if(cumple2[2]=="May"){
            cumple2.splice(2,0, '05');
        }
        else if(cumple2[2]=="Jun"){
            cumple2.splice(2,0, '06');
        }
        else if(cumple2[2]=="Jul"){
            cumple2.splice(2,0, '07');
        }
        else if(cumple2[2]=="Aug"){
            cumple2.splice(2,0, '08');
        }
        else if(cumple2[2]=="Sep"){
            cumple2.splice(2,0, '09');
        }
        else if(cumple2[2]=="Oct"){
            cumple2.splice(2,0, '10');
        }
        else if(cumple2[2]=="Nov"){
            cumple2.splice(2,0, '11');
        }
        else if(cumple2[2]=="Dec"){
            cumple2.splice(2,0, '12');
        }
    }
    else{
        var cumple2=' '
    }
    if(x.raw.profile.custom.secondLastName){
        var secondLastName=x.raw.profile.custom.secondLastName;
    }
    else{
        var secondLastName=" "
    }
    if(x.raw.profile.custom){
        var preferencias=x.raw.profile.custom;
    }
    if (x.profile.phone){
        var tel=x.profile.phone;
    }
    else{
        var tel=" "
    }

    var modelDta= new AccountModel(req.currentCustomer, preferredAddressModel, orderModel);
    modelDta.profile={
        firstName:modelDta.profile.firstName ,
        lastName:modelDta.profile.lastName,
        secondLastName:secondLastName,
        email:modelDta.profile.email,
        birthday:cumple2[4]+"-"+cumple2[2]+"-"+cumple2[1],
        phone:tel,
        clothesWoman:preferencias.clothesWoman,
        clothesMan:preferencias.clothesMan,
        clothesBabys:preferencias.clothesBabys,
        clothesBoys:preferencias.clothesBoys,
        clothesDenim:preferencias.clothesDenim,
        clothesGirls:preferencias.clothesGirls,
        password:"",

    }
    return modelDta;
}

module.exports = {
    getAccountModel: getAccountModel
};

