'use strict';

/**
 * Middleware to use consent tracking check
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
function categories(req, res,next) {
    var HookMgr = require('dw/system/HookMgr');
    var categories = HookMgr.callHook('app.hook.services.kb.categories', 'getCategories', null)
    categories = JSON.parse(categories.object);

    if (!categories) {
        categories = {
            childCategories: []
        }
    }

    res.setViewData({
        categories: categories.childCategories
    });
    next();
}

module.exports = {
    categories: categories
};
