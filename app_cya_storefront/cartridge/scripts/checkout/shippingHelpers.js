"use strict";


var base = module.superModule;

/**
 * Retrieve raw address JSON object from request.form
 * @param {Request} req - the DW Request object
 * @returns {Object} - raw JSON representing address form data
 */
function getAddressFromRequest(req) {
    return {
        firstName: req.form.firstName,
        lastName: req.form.lastName,
        address1: req.form.address1,
        address2: req.form.address2,
        city: req.form.city,
        stateCode: req.form.stateCode,
        postalCode: req.form.postalCode,
        countryCode: req.form.countryCode,
        phone: req.form.phone,
        suburb: req.form.suburb,
        county: req.form.county,
        numberExt: req.form.numberExt,
        numberInt: req.form.numberInt,
        customerAddresId: req.form.addressId,
    };
}

module.exports = {
    getAddressFromRequest: getAddressFromRequest
};


Object.keys(base).forEach(function (prop) {
    // eslint-disable-next-line no-prototype-builtins
    if (!module.exports.hasOwnProperty(prop)) {
        module.exports[prop] = base[prop];
    }
});

