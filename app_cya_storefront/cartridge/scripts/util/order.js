
"use strict";

function getOrderStatus(order) {
    var status = "";
    var Resource = require("dw/web/Resource");
    switch (order.orderStatus.value) {
        case 0 :
            status = Resource.msg("label.orderhistory.status.CREATED", "account", null);
            break;
        case 3 :
            status = Resource.msg("label.orderhistory.status.NEW", "account", null);
            break;
        case 4 :
            status = Resource.msg("label.orderhistory.status.OPEN", "account", null);
            break;
        case 5 :
            status = Resource.msg("label.orderhistory.status.COMPLETED", "account", null);
            break;
        case 6 :
            status = Resource.msg("label.orderhistory.status.CANCELED", "account", null);
            break;
        case 8 :
            status = Resource.msg("label.orderhistory.status.FAILED", "account", null);
            break;
    }

    return status;
}

module.exports = {
    getOrderStatus: getOrderStatus
};
